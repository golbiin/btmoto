import { apiUrl } from "../config.json";
const axios = require("axios").default;

export function getSingleCForm(form_name) {
  return axios.post(apiUrl + "/admin/cforms/fetchDetailsByName", {
    form_name: form_name
  });
}

export function createEntries(data) {
  return axios.post(apiUrl + "/admin/cforms/create_entries", {
    data: data
  });
}
