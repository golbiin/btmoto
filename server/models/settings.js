const mongoose = require("mongoose");
const settings = new mongoose.Schema({
  name: { type: String, required: true },
  slug: { type: String, required: true },
  value: {type: Object , default: {} },
  status: { type: Number },
  created_on: { type: Date }
});

module.exports = mongoose.model("settings", settings);