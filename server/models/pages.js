const mongoose = require("mongoose");
const pages = new mongoose.Schema({
  title: { type: String },
  page_title: { type: String },
  meta_tag: { type: String },
  meta_desc: { type: String },
  slug: { type: String, required: true ,unique: true  },
  content: {  type: Object ,default: {}},
  preview: {  type: Object ,default: {}},
  template_name: { type: String },
  is_editable: { type: Boolean},
  is_duplicate: { type:Boolean},
  is_preview: { type:Boolean},
  is_delete: { type:Boolean},
  page_status :  { type: String, default: 1},
});
pages.index({ title: "text", content: "text"});
module.exports = mongoose.model("pages", pages);