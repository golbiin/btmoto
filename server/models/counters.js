const mongoose = require("mongoose");
const counters = new mongoose.Schema({
    slug: { type: String, required: true  },
    sequence_value :  { type: Number , required: true},
});
module.exports = mongoose.model("counters", counters);
