const mongoose = require("mongoose");
const integrator_distributor= new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
});
module.exports = mongoose.model("integrator_distributor", integrator_distributor);