const mongoose = require("mongoose");
const news = new mongoose.Schema({

    title:{ type: String, required: true  },
    description:{ type: String },
    content:{ type: String },
    image:{ type: String },
    image_thumb:{ type: String },
    date:{ type: Date },
    is_top_news:{ type: String, default: 0 },
    slug: { type: String, required: true  },
    page_status :  { type: String, default: 1},
    meta_tag: { type: String },
    meta_desc: { type: String },
    news_order: { type: Number },
});
news.index({ title: "text", description: "text", content: "text"});
module.exports = mongoose.model("news", news);
