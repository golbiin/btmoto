const mongoose = require("mongoose");
const orders = new mongoose.Schema({

    user_id: { type: String, required: true  },
    order_no: {type: String, required: true} ,
    products: { },
    created_on: { type: Date },
    status :  { type: String , required: true},
    page_status :  { type: String, default: 1},
    invoice_url :  { type: String },
    order_details : {
        product : {
            type: Array
        },
        tax : {
            type: Object
        },
        shipping : {
            type: Object
        },        
        total : {
            tax_total: { type: Number },
            shipping_total: { type: Number },
            product_total: { type: Number },
            discount: { type: Number },
            total: { type: Number },
        },
        refund : {
            type: Array
        }
    },
    payment_details : {
        id: { type: String},
        otherdetails : {
            type: Object
        }
    },
    shipping_details: {
        first_name: { type: String, required: true  },
        last_name: { type: String, required: true  },
        email: { type: String, required: true  },
        company: { type: String  },

        address: { type: String, required: true  },
        appartment: { type: String  },
        city: { type: String, required: true  },
        country:{ type: String, required: true  },
        region: { type: String, required: true  },
        zipcode: { type: String, required: true  },
        phone:{ type: String  },

    },
    shipping_method: {
        service: { type: String, required: true  },
        code: { type: String, required: true  },
        currency: { type: String, required: true  },
        charge: { type: String, required: true  },
    },
    tax: {
        tax_name: { type: String  },
        shipping: { type: Boolean },
        country: { type: String  },
        state: { type: String  },
        zipcode: { type: String  },
        rate_percent: { type: String },
        priority: { type: Number  },        
    },
    ups_options: {
        type: Object
    },
    shipping_digest : {
        type: Object   
    },
    shipmentresults : {
        type: Object   
    },
    coupon_details: {
        couponcode:{ type: String  },
        slug: { type: String  },
        amount: {type: Number   },
        discounttype : { type: String  },
        exiprydate: { type: Date },
        products : {  type: Object },
        users :  {  type: Object },
        couponstatus  : { type: String  },
        guestusers : { type: String}
    },
});
module.exports = mongoose.model("orders", orders);
