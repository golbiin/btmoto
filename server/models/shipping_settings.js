const mongoose = require("mongoose");
const shipping_settings = new mongoose.Schema({
    country: {
        type: String,
        required: true
    },
    region: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },

    zipcode: {
        type: String,
        required: true
    },
    street_address: {
        type: String,
    },
    slug: {
        type: String,
        required: true
    },
});
module.exports = mongoose.model("shipping_settings", shipping_settings);