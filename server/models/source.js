const mongoose = require("mongoose");
const source= new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
});
module.exports = mongoose.model("source", source);