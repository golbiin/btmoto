const mongoose = require("mongoose");
const products = new mongoose.Schema({
    product_name:{ type: String, required: true  },
    slug: { type: String, required: true  },
    url: {type: String},
    banner_description:{ type: String },
    product_sub_text : { type: String },
    model_number : { type: String },
    short_description:{ type: String },
    description:{ type: String },
    type:{ type: String , default: 'Simple'}, // (Simple/Bundled)
    price: {  type: Object },
    inventory: {  type: Object },
    images: {  type: Object },
    tabdata: {  type: Object },

    primary_category:{ type: String },
    categories: [mongoose.Types.ObjectId],
    productAttribute: [mongoose.Types.ObjectId],
    // categories: {  type: Object },
    related_products: {  type: Object }, // array of product ids
    downloads: {  type: Object },
    attributes: {  type: Object },  
    bundled_products: {  type: Object }, // array of product ids
    specifications: {  type: String },   // html content
    created_on: { type: Date },
    page_status :  { type: String, default: 1},
    meta_tag: {  type: String },
    meta_description: {  type: String },
    product_tag : {   type: Array },
    downloadbtn :  {  type: String },
    downloadcontent1 :  {  type: String },
    orderno : { type: Number , default: 0 },
    packages : {
        weights :  { type: String, default: 0},
        lengths :  { type: String, default: 0},
        widths : { type: String, default: 0},
        heights : { type: String, default: 0},
    }
});

products.index({ product_name: "text", short_description: "text",
                 description: "text", specifications: "text", 
                 banner_description: "text", product_sub_text: "text" });
module.exports = mongoose.model("products", products);
