const mongoose = require("mongoose");
const categories = new mongoose.Schema({
  category_name: { type: String, required: true },
  user_type: { type: Array },
 // attributes: { type: Array },
  short_description: { type: String},
  descrption: { type: String},
  banner_sub_description: { type: String},
  banner_main_description: { type: String},
  status: { type: Number },
  created_on: { type: Date },
  parent_id: { type: String},
  content: { type: Array },
  slug: { type: String, required: true },
  page_type: { type: String},
  page_layout: { type: String},
  image:{ type: String },
  manualUrl:{ type: String },
  downloadUrl:{ type: String },
  order : { type: Number , default: 0 },
});
//type: mongoose.Schema.ObjectId
module.exports = mongoose.model("categories", categories);
