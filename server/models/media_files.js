const mongoose = require("mongoose");
const media_files = new mongoose.Schema({
    page_type: {
        type: String
    },
    created_on: {
        type: Date
    },
    file_name: {
        type: String
    },
    file_url: {
        type: String
    },
    order_number : {
         type: Number 
    },
    status: {
        type: String,
        default: 1
    },
    file_type : {
        type: String
    },

    file_info : {
        originalname: {
            type: String
        },
        encoding: {
            type: String
        },    
        mimetype: {
            type: String
        },    
        filename: {
            type: String
        },    
        size: {
            type: String
        },    
    }


});
module.exports = mongoose.model("media_files", media_files);