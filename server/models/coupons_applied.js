const mongoose = require("mongoose");
var Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;

const coupons_applied = new mongoose.Schema({
    couponcode:{ type: String, required: true  },
    order_id:  {type: ObjectId },
    user_id: {type: ObjectId },
    amount: {type: Number , required: true  },    
    created_on: { type: Date , required: true},    
});
module.exports = mongoose.model("coupons_applied", coupons_applied);
