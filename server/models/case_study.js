const mongoose = require("mongoose");
const case_study = new mongoose.Schema({

    title:{ type: String, required: true  },
    sub_title:{ type: String},
    description:{ type: String },
    content:{ type: String },
    image:{ type: String },
    date:{ type: Date },
    relatedcontent : { type: String },
    is_top_case_study:{ type: String, default: 0 },
    slug: { type: String, required: true  },
    page_status :  { type: String, default: 1},
    related_products: {  type: Object }, // array of product ids
    related_cat: {  type: Object },// array of product cat,
    releasedate: { type: String , default: 0 },
    data_order : { type: Number }
});
case_study.index({ title: "text", description: "text", content: "text"});
module.exports = mongoose.model("case_study", case_study);
