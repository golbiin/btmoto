const mongoose = require("mongoose");
const feedback = new mongoose.Schema({
  users: {  type: Object , default: {} },
  feedback: { type: String },
  description : { type: String },
  review : { type: String },
});
module.exports = mongoose.model("feedback", feedback);
