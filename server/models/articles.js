const mongoose = require("mongoose");
const articles = new mongoose.Schema({

    title:{ type: String, required: true  },
    description:{ type: String },
    content:{ type: String },
    image:{ type: String },
    image_thumb:{ type: String },
    date:{ type: Date },
    is_top_articles:{ type: String, default: 0 },
    slug: { type: String, required: true  },
   // page_title: { type: String },
    meta_tag: { type: String },
    meta_desc: { type: String },
    page_status :  { type: String, default: 1},
    topic: { type: String , default: ''  },
    industry: { type: String , default: ''  },
    source: { type: String , default: ''  },
    // related_cat:{ type: String , default: ''  },
    releasedate: { type: String },
    article_order: { type: Number },
});
articles.index({ title: "text", description: "text", content: "text"});
module.exports = mongoose.model("articles", articles);
