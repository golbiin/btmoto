const mongoose = require("mongoose");
const attributes = new mongoose.Schema({
  name: { type: String, required: true },
  slug: { type: String, required: true },
  parent_id: { type: String},
  status: { type: Number },
  created_on: { type: Date },
  category: [mongoose.Types.ObjectId],
  order :{ type: Number , default: 0 }
});
//type: mongoose.Schema.ObjectId
module.exports = mongoose.model("attributes", attributes);