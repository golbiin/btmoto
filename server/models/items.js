const mongoose = require("mongoose");
const items = new mongoose.Schema({
  item_name: { type: String, required: true },
  slug: { type: String, required: true },
  parent_slug: { type: String, required: true },
  parent_id: { type: String, required: true },
  attributes: { type: Object },
  status: { type: Number },
  created_on: { type: Date }
});

module.exports = mongoose.model("items", items);
