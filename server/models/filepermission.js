const mongoose = require("mongoose");
const { array } = require("@hapi/joi");
const menus = new mongoose.Schema({
  name: { type: String, required: true  },
  items: {  type: Array },
  slug: { type: String, required: true },
  position : { type: String, default: 0 },
});
module.exports = mongoose.model("filepermission", menus);
