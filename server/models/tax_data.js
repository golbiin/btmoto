const mongoose = require("mongoose");
const tax_data = new mongoose.Schema({

    country:{ type: String, required: true  },
    state:{ type: String, required: true  },
    zipcode:{ type: String },
    city:{ type: String },
    rate_percent:{ type: String, required: true  },
    tax_name:{ type: String, default: 0, required: true  },
    priority: { type: Number, required: true  },
    shipping :  { type: Boolean, default: false},
});
module.exports = mongoose.model("tax_data", tax_data);
