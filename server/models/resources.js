const mongoose = require("mongoose");
const resources = new mongoose.Schema({
  category_id: { type: String, required: true },
  product_id: { type: String, required: true },
  resource_category: { type: String },
  file_url: { type: String},
  filename:{type: String },
  version:{type: String },
  created_on: {type: Date },
  resource_type: {type: String},
});
module.exports = mongoose.model("resources", resources);