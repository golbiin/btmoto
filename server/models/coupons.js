const mongoose = require("mongoose");
const coupons = new mongoose.Schema({
    couponcode:{ type: String, required: true  },
    slug: { type: String, required: true  },
    amount: {type: Number , required: true  },
    discounttype : { type: String, required: true  },
    exiprydate: { type: Date , required: true},
    products : {  type: Object },
    users :  {  type: Object },
    couponstatus  : { type: String, required: true  },
    guestusers : { type: String}
});
module.exports = mongoose.model("coupons", coupons);
