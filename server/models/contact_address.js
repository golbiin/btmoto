const mongoose = require("mongoose");
const contact_address  = new mongoose.Schema({
    name:{ type: String, required: true  },
    address1:{ type: String },
    address2:{ type: String },
    phone:{ type: String },
    email:{ type: String },
    latitude:{ type: String },
    longitude:{ type: String },
    created_on: { type: Date },
});
module.exports = mongoose.model("contact_address", contact_address );
