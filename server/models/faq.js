const mongoose = require("mongoose");
const faq= new mongoose.Schema({
    title: {type: String, required: true},
    description: { type: String},
    date:{ type: Date },
    categories: [mongoose.Types.ObjectId],
    page_status :  { type: String, default: 1},
    slug: { type: String, required: true  },
});
faq.index({ title: "text", description: "text"});
module.exports = mongoose.model("faq", faq);