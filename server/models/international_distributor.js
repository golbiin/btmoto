const mongoose = require("mongoose");
const international_distributor = new mongoose.Schema({
  location_name: { type: String, required: true },
  location_url: { type: String },
  latitude: { type: String },
  longitude: { type: String },
  storenumber :  { type: String, default: 1},
  created_on: { type: String },
  page_status: { type: String, default: 1 }
});

module.exports = mongoose.model(
  "international_distributor",
  international_distributor
);
