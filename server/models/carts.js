const mongoose = require("mongoose");
var Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;
const carts = new mongoose.Schema({
  products: {  type: Object },
  total: {  type: Object },
  createdon: {type: Date },
  user_id: {type: ObjectId },
});
module.exports = mongoose.model("carts", carts);
