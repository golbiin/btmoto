const mongoose = require("mongoose");
const order_failure = new mongoose.Schema({
    order_id: { type: String  },
    created_on: { type: Date },
    status:  { type: String , required: true},
    message: { type: String , required: true},
    error: { type: String , required: true},
    order_data: { type: Object }
});
module.exports = mongoose.model("order_failure", order_failure);
