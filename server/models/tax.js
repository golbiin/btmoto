const mongoose = require("mongoose");
const tax_datas= new mongoose.Schema({
  country: {
    type: String,
   // required: true
  },
  state: {
    type: String,
   // required: true
  },
  zipcode: {
    type: String,
  //  required: true
  },
  rate_percent: {
    type: String, 
  },
  tax_name: {
    type: String, 
  },
  priority: {
    type: String, 
  } ,
  shipping: {
    type: Boolean, 
  } 
});
module.exports = mongoose.model("tax_datas", tax_datas);