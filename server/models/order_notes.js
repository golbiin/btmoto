const mongoose = require("mongoose");
const order_notes = new mongoose.Schema({
    order_id: { type: String, required: true  },
    created_on: { type: Date },
    status :  { type: String , required: true},
    message : { type: String ,  default: ''},
});
module.exports = mongoose.model("order_notes", order_notes);
