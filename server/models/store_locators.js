const mongoose = require("mongoose");
const store_locators = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  store_category: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  address2: {
    type: String
    //  required: true
  },
  city: {
    type: String,
    required: true
  },
  state: {
    type: String
  },
  country: {
    type: String,
    required: true
  },
  zip: {
    type: String
    // required: true
  },
  date: {
    type: String,
    required: true
  },
  phone: {
    type: String
  },
  email: {
    type: String
  },

  service_areas: {
    type: Array,
    required: true
  },
  site_url: {
    type: String
  },
  logo_img: {
    type: String
  },
  fax: {
    type: String
  },
  latitude: {
    type: String
  },
  longitude: {
    type: String
  },
  headquaters: { type: Boolean },
  page_status: { type: String, default: 1 },
  marker_image: {type:String}
});
module.exports = mongoose.model("store_locator", store_locators);
