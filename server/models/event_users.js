const mongoose = require("mongoose");
var Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;
const event_users = new mongoose.Schema({
    event_id:{ type: ObjectId, required: true  },
    user_id:{ type: ObjectId, required: true  },    
    created_on: { type: Date }
});
module.exports = mongoose.model("event_users", event_users);
