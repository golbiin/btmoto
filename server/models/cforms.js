const mongoose = require("mongoose");
const cforms = new mongoose.Schema({
  form_name: { type: String, required: true },
  content: {type: Object , default: {} },
  status: { type: Number },
  created_on: { type: Date }
});

module.exports = mongoose.model("cforms", cforms);