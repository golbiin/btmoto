const mongoose = require("mongoose");
const distributorcategory= new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
});
module.exports = mongoose.model("distributorcategory", distributorcategory);