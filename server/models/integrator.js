const mongoose = require("mongoose");
var Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;
const integrator = new mongoose.Schema({

  category_id: { type: ObjectId, required: true },
  product_id: { type: ObjectId, required: true },
  file_name:{type: String },
  type: {type: String },
  version:{type: String },
  file_url: { type: String },
  created_on: {type: Date },

  category_name: { type: String, required: true },
  product_name: { type: String, required: true },
  software:{type: Object },
  images: {type: Object },
  document:{type: Object },
  // slug: { type: String, required: true },
  created_on: {type: Date }
});

module.exports = mongoose.model("integrator_resources", integrator);
