const mongoose = require("mongoose");
const distributor_resources = new mongoose.Schema({
    title:{ type: String, required: true  },
    file_url: { type: String },
    uploadFiles: {  type: Object },
    content:{ type: String },
    // image:{ type: String },
    date:{ type: Date },
    is_top_news:{ type: String, default: 0 },
    slug: { type: String, required: true  },
    page_status :  { type: String, default: 1},
    related_products: {  type: Object }, // array of product ids
    version :{ type: String },
    related_cat: { type: String , default: ''  },// array of product cat,
    releasedate: { type: String },
    data_order : { type : Number }
});
distributor_resources.index({ title: "text", content: "text", file_url: "text"});
module.exports = mongoose.model("distributor_resources", distributor_resources);
