const mongoose = require("mongoose");
const cforms_entries = new mongoose.Schema({
  form_id: { type: mongoose.Schema.ObjectId, required: true },
  data: { type: Object, default: {} },
  status: { type: Number },
  created_on: { type: Date }
});

module.exports = mongoose.model("cforms_entries", cforms_entries);
