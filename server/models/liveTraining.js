const mongoose = require("mongoose");
const livetraining = new mongoose.Schema({

    title:{ type: String, required: true  },
    description:{ type: String },
    content:{ type: String },
    image:{ type: String },
    date:{ type: Date },
    is_top_training:{ type: String, default: 0 },
    slug: { type: String, required: true  },
    page_status :  { type: String, default: 1},
    facebook :  { type: String, default: ''},
    twitter:  { type: String, default: ''},
    linkedin:  { type: String, default: ''},
    email:  { type: String, default: ''},
    eventStartDate: { type: Date , required: true},      
    eventStartTimeHour: { type: String },
    eventStartTimeMinutes:{ type: String },
    eventStartTimeMeridiem: { type: String },
    eventEndDate: { type: Date, required: true },   
    eventEndTimeHour: { type: String },
    eventEndTimeMinutes: { type: String },
    eventEndTimeMeridiem: { type: String },
    allDayEvent: { type: Boolean, default: false },
    hideEventTime: { type: Boolean, default: false },
    hideEventEndTime: { type: Boolean, default: false },
    eventLocation: { type: String },
    eventCost: { type: String },
    eventStatus: { type: Number },
    eventLink : {type: String},
    eventId : { type: String}
});
livetraining.index({ title: "text", description: "text", content: "text"});
module.exports = mongoose.model("livetraining", livetraining);
