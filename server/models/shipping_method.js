const mongoose = require("mongoose");
const shipping_method = new mongoose.Schema({
  fullname: { type: String },  
  sandbox: { type: Boolean , required: true}, 
  acount_no: { type: String , required: true},
  sandbox_account_no: { type: String , required: true},
  slug: { type: String, required: true},
  access_key: { type: String },
  sandbox_access_key: { type: String},
  ups_customer_classification: { type: String},
  status:{ type: String,required: true },
  ship_to_applicable_country:{ type: String ,required: true},
  ups_pickup_type: { type: String },
  packaging_type_code: {type: String },
  insure_package: { type: String},
  additional_handling_charge: { type: String },
  fullname: { type: String },
  sandbox: { type: String, required: true},
  sandbox_account_no:{ type: String, required: true},
  status: { type: Boolean},
  account_number:{ type: String,required: true },
  access_key:{ type: String ,required: true},
  sandbox_access_key:{ type: String ,required: true},
  slug:{ type: String ,required: true},
  account_number: { type: String},
  gateway_URL: { type: String },
  ups_customer_classification: { type: String, required: true },
  packing_type:{ type: String, required: true },
  pass_diamenions:{ type: String, required: true },
  weight_type: { type: String, required: true},
  diamension_type:{ type: String },
  tracing:{ type: String,required: true },
  username:{ type: String },
  sandbox_username:{ type: String },
  origin_of_the_shipment:{ type: String, required: true },
  password:{ type: String },
  sandbox_password:{ type: String, required: true },
  carrier_services: { type: Array, required: true}
});

//type: mongoose.Schema.ObjectId
module.exports = mongoose.model("shipping_method", shipping_method);