const mongoose = require("mongoose");
const redirection= new mongoose.Schema({
  from:{type: String },
  to:{type: String },
  created_on: { type: Date },
  status :  { type: String, default: 1},
});
module.exports = mongoose.model("redirection", redirection);