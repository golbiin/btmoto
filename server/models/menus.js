const mongoose = require("mongoose");
const menus = new mongoose.Schema({
  name: { type: String, required: true  },
  menu_items: {  type: Object , default: {} },
  slug: { type: String, required: true },
  position : { type: String, default: 0 },
});
module.exports = mongoose.model("menus", menus);
