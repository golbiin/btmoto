const mongoose = require("mongoose");
const facebook = new mongoose.Schema({
  name: { type: String, required: true  },
  value: {  type: Object , default: {} },
  slug: { type: String, required: true },
});
module.exports = mongoose.model("facebook", facebook);