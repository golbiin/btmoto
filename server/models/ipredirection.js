const mongoose = require("mongoose");
const ipredirection= new mongoose.Schema({
  from: mongoose.ObjectId,
  fromtitle:{type: String },
  to: mongoose.ObjectId,
  totitle:{type: String },
  created_on: { type: Date },
  status :  { type: String, default: 1},
  country :{  type: Object }, 
  state :{  type: Object }, 
  except : {type: String },
  ip: {type: String },
});
module.exports = mongoose.model("ipredirection", ipredirection);