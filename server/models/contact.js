const mongoose = require("mongoose");
const contact= new mongoose.Schema({
    fullname: {
    type: String,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  region: {
    type: String,
   
  },
  phone: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  comments: {
    type: String, 
  } 
});
module.exports = mongoose.model("contact_subscribers", contact);