const mongoose = require("mongoose");
const event_locations = new mongoose.Schema({

    location_name:{ type: String, required: true  },
    slug:{ type: String },
    description:{ type: String },
    address:{ type: String },
    latitude:{ type: String },
    longitude:{ type: String } ,
    created_on: { type: String },
    page_status: { type: String, default: 1 }
});

module.exports = mongoose.model("event_locations", event_locations);
