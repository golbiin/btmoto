const mongoose = require("mongoose");
const videotraining = new mongoose.Schema({
  video_title: { type: String, required: true },
  video_url: { type: String },
  category_name: { type: String },
  video_order: { type: Number },
  created_on: { type: Date },
  page_status: { type: String, default: 1 }
});

videotraining.index({ video_title: "text"});
module.exports = mongoose.model("videotraining", videotraining);
