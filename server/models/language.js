const mongoose = require("mongoose");
const language = new mongoose.Schema({

    language:{ type: String, required: true  },
    code:{ type: String },
    url:{ type: String },
    languagestatus  : { type: String, required: true  },
});
// language.index({ language: "text", code: "text", url: "text"});
module.exports = mongoose.model("language", language);
