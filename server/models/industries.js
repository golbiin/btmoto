const mongoose = require("mongoose");
const industries  = new mongoose.Schema({

    title:{ type: String, required: true  },
    description:{ type: String },
    content:{ type: String },
    relatedcontent : { type: String },
    image:{ type: String },
    date:{ type: Date },
 //   is_top_news:{ type: String, default: 0 },
    slug: { type: String, required: true  },
    page_status :  { type: String, default: 1},
});
industries.index({ title: "text", description: "text", content: "text"});
module.exports = mongoose.model("industries", industries );
