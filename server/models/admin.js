const mongoose = require("mongoose");
const admin = new mongoose.Schema({
  first_name: { type: String },
  last_name: { type: String },
  email: { type: String, required: true, unique: true },
  username: { type: String, required: true, unique: true },
  password: { type: String },
  image: { type: String },
  created_on: { type: Date },
  last_login: { type: Date },
  status: { type: Boolean },
  profile_image: {  type: Object },
});
module.exports = mongoose.model("admin", admin);
