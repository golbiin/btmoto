const mongoose = require("mongoose");
const footer_columns = new mongoose.Schema({
    col_number:{ type: String, required: true  },
    column_content:{ type: Object },  
    socket_number:{ type: String, required: true  },
    socket_content:{ type: Object },  
    updated_on: { type: Date },
});
module.exports = mongoose.model("footer_columns", footer_columns);
