const mongoose = require("mongoose");
const careers = new mongoose.Schema({
  job_name: {
    type: String,
    required: true
  },
  job_type: {
    type: String,
    required: true
  },
  meta_tag: { type: String, required: true },
  meta_desc: { type: String , required: true},
  location: {
    type: String,
    required: true
  },
  post_date: {
    type: Date,
    required: true
  },
  related_cat: {type: String},
  slug: {
    type: String,
    required: true
  },
  content: {
    type: String
  },
  page_status: { type: String, default: 1 }
});

careers.index({ job_name: "text", job_type: "text", content: "text"});
module.exports = mongoose.model("careers", careers);
