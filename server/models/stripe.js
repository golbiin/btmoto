const mongoose = require("mongoose");
const stripe = new mongoose.Schema({
  name: { type: String, required: true  },
  value: {  type: Object , default: {} },
  slug: { type: String, required: true },
});
module.exports = mongoose.model("stripe", stripe);