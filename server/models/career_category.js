const mongoose = require("mongoose");
const career_category= new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
});
module.exports = mongoose.model("career_category", career_category);