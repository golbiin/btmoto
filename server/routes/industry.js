/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require('async');

/* Include schema */
const News = require("../models/news");
const Industry = require("../models/industries");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;

/* http request  port set */
const router = express.Router();

/*  Get all types of News  */
router.post("/getAllIndustry", async (req, res) => {
  try {
    Industry.find( { page_status: 1 },
          async function (err, response) {
              if (err) {
                  res.send({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message,
                  });
              } else {
                  if (response) {
                      res.send({
                          status: 1,
                          message: "Fetched details successfully.",
                          data: response
                      });
                  } else {
                      res.send({
                          status: 0,
                          message: "No details available."
                      });
                  }
              }
          }
      ).sort({
        date: -1
      }).select('page_status slug title image');
  } catch (e) {
      res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
      });
  }

});


/*  Get Single getIndustry Details  */
router.post("/getIndustryBySlug", async (req, res) => {
   const slug  = req.body.slug;
   try {
    Industry.findOne(
       { slug: slug },
       async function (err, response) {
         if (err) {
           res.send({   
             status:0,         
             message: "Oops! " + err.name + ": " + err.message,
           });
         } else {
           if(response){
             res.send({ 
               status: 1,             
               message: "Fetched Getindustry details successfully.",
               data: response
             });
           }else{
             res.send({
               status: 0,
               message: "Details Unvailable."
             });
           }
         }
       }
     );
   } catch (e) {
     res.send({
       status: 0,
       message: "Oops! " + e.name + ": " + e.message,
     });
 }
 });
 

module.exports = router;







