/* load dependencies */

const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");
var mongoose = require("mongoose");
/* Include schema */

const LiveTraining = require("./../models/liveTraining");
const EventUsers = require("./../models/event_users");

const config = require("../config/config.json");
const { json } = require("body-parser");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();

/*  Get all scheduled live trainings  */
router.post("/getAllLiveTraining", async (req, res) => {
 
    try {
        LiveTraining.aggregate([

            { $match : 
              {
                $and: [ 
                  { "eventStartDate" : { $gte : new Date()}}, 
                  {"page_status": "1"}, 

                ]
              }
              ,           
            },
            {
              /* sort descending (latest subscriptions first) */
              $sort: {
                'eventStartDate': 1
              }
            },
            {
              /* group by year and month of the subscription event */
              $group: {
                _id: {
                  year: {
                    $year: '$eventStartDate'
                  },
                  month: {
                    $month: '$eventStartDate'
                  }
                },
                trainings: { $push: "$$ROOT" } 
              },
              
            },
            {
              /* sort descending (latest subscriptions first) */
              $sort: {
                  '_id.year': 1,
                  '_id.month': 1
              }
            },
            
        ], function(err, response) {
            if (err) {
                res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
                });
            } else {
                res.send({
                status: 1,
                message: "Success",
                trainings: response
                });
            }
        })
      
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
  }
});

/*  Get Single archive Details  */
router.post("/getScheduledAllLiveTraining", async (req, res) => {
 
    try {

  
      let d = new Date();
      let firstDay = new Date(d.getFullYear(), d.getMonth(), 0);
      let lastday = new Date(d.getFullYear(), d.getMonth(), 31);
      console.log('last30days -->',firstDay , lastday);
  
      LiveTraining.aggregate([{
        $match: {
          date: {
            $gt: firstDay,
            $lt: lastday
          }
        }
      }]).exec(
        (err, User) => {
          if (err) {
            // callback({
            //     status: 0,
            //     message: "Error fetching data",
            //   },
            //   null
            // );
  
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
  
          } else {
            
            res.send({ 
            status: 1,             
            message: "Fetched liveTraining details successfully.",
            data: User
            });
  
  
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
  }
});

  /* get training by id */
router.post("/getTrainingById", async (req, res) => {
const id  = req.body.id;
try {
    LiveTraining.find(
    { _id : id  },
    async function (err, response) {
        if (err) {
        res.send({   
            status:0,         
            message: "Oops! " + err.name + ": " + err.message,
        });
        } else {
        if(response){
            res.send({ 
            status: 1,             
            message: "Fetched liveTraining details successfully.",
            data: response
            });
        }else{
            res.send({
            status: 0,
            message: "Details Unvailable."
            });
        }
        }
    }
    );

    
} catch (e) {
    res.send({
    status: 0,
    message: "Oops! " + e.name + ": " + e.message,
    });
}
});

  /* get training by id */
router.post("/reserveEvent", async (req, res) => {
  const id  = req.body.id;
  try {
    const { event_id, token, user_id } = req.body;

    console.log('event_id user_id',event_id , user_id);
    if(!token) {
      res.send({
          status: 2,
          message: "You must be registered and logged In." ,
      })
    }

    payload = jwt.verify(token, jwtKey);
    EventUsers.find({
      user_id: req.body.user_id,
      event_id: req.body.event_id
    },async function(err, response) {

      console.log(err,response);
      if(err) {
        res.send({
          status: 2,
          message: "Oops! " + err.name + ": " + err.message,
        })
      } 
      else if(response.length > 0) {
        res.send({
          status: 2,
          message: "You have already reserved a seat for the event",
        })
      } else {
        let created_on = moment().format("MM-DD-YYYY hh:mm:ss");
        const newEventUser = new EventUsers({
          event_id: req.body.event_id,
          user_id: req.body.user_id,
          created_on: created_on,          
        });
        await newEventUser.save() 
        .then(() => {
          res.send({
            status: 1,
            message: "Your seat has been reserved successfully for the event",
          });
        }) 
        .catch((err) => {
          res.send({
              status: 2,
              message: "Oops! " + err.name + ": " + err.message,
          });
        });
      }

    })


  } catch (e) {
      res.send({
        status: 2,
        message: "Oops! " + e.name + ": " + e.message,
      });
  }
});


/* get training by id */
router.post("/getUserTraining", async (req, res) => {

  try {
    const { token, user_id } = req.body;
    if(!token) {
      res.send({
          status: 2,
          message: "You must be registered and logged In." ,
      })
    }

    payload = jwt.verify(token, jwtKey);
    
    LiveTraining.aggregate([
      // { $match: {'user_id' : mongoose.Types.ObjectId(user_id)}},
      { $lookup:
         {
           from: 'event_users',
           localField: '_id',
           foreignField: 'event_id',
           as: 'user_details'
         }
       },
       { $unwind: "$user_details" } ,
       { $match: {'user_details.user_id' : mongoose.Types.ObjectId(user_id)}},
      ],async function (err, response) {
        if (err) {
          res.send({   
              status:0,         
              message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
              res.send({ 
              status: 1,             
              message: "Fetched liveTraining details successfully.",
              data: response
              });
          }else{
              res.send({
              status: 0,
              message: "Details Unvailable."
              });
          }
        }
      });
    

  } catch (e) {
      res.send({
        status: 2,
        message: "Oops! " + e.name + ": " + e.message,
      });
  }
});




/* get training by id */
router.post("/updateTrainingById", async (req, res) => {


  try {
    RquestData =  req.body.training_id;

    async.waterfall([getTraningExitorNot , createTraining], function(error, results) {
      if (error) {
        res.send(error);
      } else {

        const { TraningIds } = results ;

        if(TraningIds.length > 0){
          res.send({
            status: 1,
            message: "Your seat has been reserved successfully for the events",
            results: results
          });
        
        } else {
            res.send({
            status: 0,
            message: "You have already reserved a seat for the events",
            results: results
          });
        }

      
      }
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }

});

function getTraningExitorNot(callback) {
  const results = {};
  
  try {
    let TraningAlreadyIds = [];
    let TraningIds = [];
    let id = 0;
    RquestData.forEach(function(dataVar){
    
      EventUsers.find({
        user_id: dataVar.user_id,
        event_id: dataVar.event_id,
      },async function(err, response) {
  
     
        if(err) {
          id ++;
          console.log('erroro');
            callback(
                {
                  status: 0,
                  message: "Sorry no Details found",
                  from: "EventUsers  Catch"
                },
                null
            );
        } 
        else if(response.length > 0) {
          TraningAlreadyIds.push( {user_id : dataVar.user_id, event_id : dataVar.event_id ,
            created_on : dataVar.created_on
          });
          id ++;

            if(id === RquestData.length){
              results.TraningAlreadyIds = TraningAlreadyIds;
              results.TraningIds = TraningIds;
              callback(null, results);
            } 

        } else {
          
          TraningIds.push( {user_id : dataVar.user_id, event_id : dataVar.event_id , created_on : dataVar.created_on});
          id ++;
          
          if(id === RquestData.length){
            results.TraningAlreadyIds = TraningAlreadyIds;
            results.TraningIds = TraningIds;
            callback(null, results);
          }
       
        }
 
      })
   
     });
 
  } catch (e) {
    callback(
      {
        status: 0,
        message: "Oops!",
        from: "getTraningExitorNot"
      },
      null
    );
  }
}


function createTraining(results, callback) {
  try {
    console.log('<-------------results-------->',results);
    const { TraningIds } = results ;

    EventUsers.insertMany(TraningIds)
    .then(result => {
      results.result = result;
      callback(null, results);
    })
    .catch(err => {
      callback(
        {
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
          from: "createTraning catch"
        },
        null
      );

    }) 
  } catch (e) {
    
    callback(
      {
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
        from: "createTraning catch"
      },
      null
    );
  }
}




module.exports = router;
  
  
  
  
  