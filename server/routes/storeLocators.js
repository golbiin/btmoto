//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");
//Include schema
const Store_locators = require("../models/store_locators");
// const ToothzenItems = require("../../modules/items");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();
var https = require("follow-redirects").https;
const { curly } = require('node-libcurl');
var curl = require('curlrequest');
/*-------------Get store details------------------*/

router.post("/getStoreDetailsBySlug", async (req, res) => {
  const store_category = req.body.slug;
  try {
    Store_locators.find(
      {
        $and: [
          { store_category: store_category },
          { page_status: { $ne: 0 } },
          { page_status: { $ne: 2 } }
        ]
      },
      async function(err, storeDetails) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
        if (storeDetails) {
          hQ_lat1 = 36.0051281;
          hQ_lon1 = -115.093628;
          var MapLocations = [];
          var result = storeDetails.map(function(el) {
            lat1 = el.latitude;
            lon1 = el.longitude;
            var distanceMiles = distance(lat1, lon1, hQ_lat1, hQ_lon1, "M");
            var o = JSON.parse(JSON.stringify(el));
            //  o.distance = Math.round((distanceMiles + Number.EPSILON) * 10) / 10;
            if (distanceMiles) {
              o.distance = distanceMiles.toFixed(1);
              return o;
            }
          });

          result1 = result.sort(function(a, b) {
            return a.distance - b.distance;
          });
          if (result1) {
            res.send({
              status: 1,
              message: "Fetched  details successfully.",
              locatorDetails: result
            });
          } else {
            res.send({
              status: 0,
              message: "No Results Found."
            });
          }
        }
      }
    ).sort({
      _id: -1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/*****search******/

router.post("/searchStoreLocation", async (req, res) => {
  let store_category1 = req.body.slug.selCategory;
  let distanceInMiles = req.body.slug.distance;
  const new_latitude = req.body.slug.latitude;
  const new_longitude = req.body.slug.longitude;
  console.log(5555, store_category1);
  try {
    Store_locators.find(
      {
        $and: [
          { store_category: store_category1 },
          { page_status: { $ne: 0 } },
          { page_status: { $ne: 2 } }
        ]
      },

      async function(err, storeDetails) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (storeDetails) {
            var MapLocations = [];

            if (new_latitude && new_longitude) {
              current_latitude = new_latitude;
              current_longitude = new_longitude;
            } else {
              current_latitude = 36.0051281;
              current_longitude = -115.093628;
            }
            var result = storeDetails.reduce(function(result, el) {
              lat1 = el.latitude;
              lon1 = el.longitude;

              var distance_new = distance(
                lat1,
                lon1,
                current_latitude,
                current_longitude,
                "M"
              );
              if (distance_new < distanceInMiles) {
                //var someNewValue = { distance:distance_new.toFixed(1) }
                // result.push(someNewValue);
                var NewValue = JSON.parse(JSON.stringify(el));
                NewValue.distance = distance_new.toFixed(1);
                result.push(NewValue);
              }

              return result;
            }, []);
            result = result.sort(function(a, b) {
              return a.distance - b.distance;
            });

            if (result.length > 0) {
              res.send({
                status: 1,
                message: "Fetched  details successfully.",
                storelocation: result
              });
            } else {
              res.send({
                status: 0,
                message: "No Results Found."
              });
            }
          } else {
            res.send({
              status: 0,
              message: "No Results Found."
            });
          }
        }
      }
    ).sort({
      _id: 1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

// Store location by distanceInMiles

// Distance from hQ
hQ_lat1 = 39.279611;
hQ_lon1 = -84.406969;
function distanceFromHq(lat1, lon1, lat2, lon2, unit) {
  if (lat1 == lat2 && lon1 == lon2) {
    return 0;
  } else {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit == "K") {
      dist = dist * 1.609344;
    }
    if (unit == "N") {
      dist = dist * 0.8684;
    }
    return dist;
  }
}

/*distancce function*/

function distance(lat1, lon1, lat2, lon2, unit) {
  if (lat1 == lat2 && lon1 == lon2) {
    return 0;
  } else {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit == "K") {
      dist = dist * 1.609344;
    }
    if (unit == "N") {
      dist = dist * 0.8684;
    }
    return dist;
  }
}


/* get training by id */
router.post("/getLocationDetails", async (req, res) => {

  try {
    const { address } = req.body;
   

   console.log('address', 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyDeYCp70l3DM3af-VzfqQBLSS1ud3jsC8c');

   var options = {
    url: 'https://maps.googleapis.com/maps/api/geocode/json?address=%'+address+'%&key=AIzaSyDeYCp70l3DM3af-VzfqQBLSS1ud3jsC8c',
    //method: "GET",
    verbose: true
    , stderr: true,
    headers: {
      'Content-Type': 'application/json',
    }
};
 
curl.request(options, function (err, data) {
  const {results} = JSON.parse(data);
 
  res.send({
    status: 1,
    data: results[0], 
  });
 
});
    
  
    
  } catch (e) {
      res.send({
        status: 2,
        message: "Oops! " + e.name + ": " + e.message,
      });
  }
});


module.exports = router;
