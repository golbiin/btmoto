/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require('async');

/* Include schema */
const News = require("../models/news");
const Archives = require("../models/archives");
const Articles = require("../models/articles");
const liveTraining = require("../models/liveTraining");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();

/*  Get all types of News  */
router.post("/getAllNews", async (req, res) => {
  try {
    async.parallel({
      latestNews: getLatestNews,
      recentNews: getRecentlyPublishedNews,
      topNews: getTopNews
    }, function (error, results) {
      if (error) {
        res.status(500).send(error);
        return;
      }
      res.send(results);
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

/*  Get the Latest News  */
router.post("/getLatestNews", async (req, res) => {
  const page = parseInt(req.body.page);
  const PAGE_SIZE = 10;  
  const skip = (page - 1) * PAGE_SIZE; 
  var totalPages  = 0;
  try {
    if(page < 0 || page === 0) {
      response = { status: 0, "message" : "Invalid page number, should start with 1"};
      return res.json(response)
    }
    /* News Count */
    News.count({  page_status: "1"}, async function(err,totalCount) {
      if(err) {
        response = {"status" : 0,"message" : "Error fetching data"}
      } else {
        totalPages = Math.ceil(totalCount / PAGE_SIZE);
      }
    });

    /* find all news */ 
    var query = News.find({  page_status: "1"});
    query.skip(skip);
    query.limit(PAGE_SIZE);
    query.sort({ news_order: 1 });
    query.sort({ date: -1 });

    query.exec(async function (err, response) {
      if (err) {         
        response = {"status" : 0,"message" : "Error fetching data"} 
      }
      res.send({
        status: 1,
        CurrentPage: page,
        latestNews: response,
        totalPages: totalPages
      });
    })
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
  
});

/*  Get the Top News  */
function getTopNews(callback) {
  News.find({is_top_news:"1" ,  page_status : "1"}).sort({  news_order: 1 }).sort({ _id: -1 }).exec(callback);
}

/*  Get the Latest News  */
function getLatestNews(callback) {
  News.find({page_status : "1"}).sort({ news_order: 1 }).limit(1).exec(callback); 
}

/*  Get the Recently Published News  */
function getRecentlyPublishedNews(callback) {
  News.find({page_status : "1"}).sort({  news_order: 1 }).sort({ _id: -1 }).limit(10).exec(callback);
}

/*  Get Single News Details  */
router.post("/getNewsBySlug", async (req, res) => {
   const slug  = req.body.slug;
   try {
     News.findOne(
       { slug: slug },
       async function (err, response) {
         if (err) {
           res.send({   
             status:0,         
             message: "Oops! " + err.name + ": " + err.message,
           });
         } else {
           if(response){
             res.send({ 
               status: 1,             
               message: "Fetched News details successfully.",
               data: response
             });
           }else{
             res.send({
               status: 0,
               message: "Details Unvailable."
             });
           }
         }
       }
     );
   } catch (e) {
     res.send({
       status: 0,
       message: "Oops! " + e.name + ": " + e.message,
     });
 }
 });
 

/*  Get all types of archives  */
router.post("/getArchives", async (req, res) => {
  try {
    async.parallel({
      latestArchives: getLatestArchives,
      recentArchives: getRecentlyPublishedArchives,
      topArchives: getTopArchives
    }, function (error, results) {
      if (error) {
        res.status(200).send(error);
        return;
      }
      res.send(results);
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

/*  Get the Top archives  */
function getTopArchives(callback) {
  Archives.find({is_top_news:"1" , page_status : "1"}).sort({ _id: -1 }).exec(callback);
}

/*  Get the Latest archives  */
function getLatestArchives(callback) {
  Archives.find({page_status : "1"}).sort({ _id: 1 }).limit(1).exec(callback); 
}

/*  Get the Recently Published archives  */
function getRecentlyPublishedArchives(callback) {
  Archives.find({page_status : "1"}).sort({ _id: -1 }).limit(10).exec(callback);
}

/*  Get Single archive Details  */
router.post("/getAllLiveTraining", async (req, res) => {
 
  try {
    // liveTraining.find(
    //   {   },
    //   async function (err, response) {
    //     if (err) {
    //       res.send({   
    //         status:0,         
    //         message: "Oops! " + err.name + ": " + err.message,
    //       });
    //     } else {
    //       if(response){
    //         res.send({ 
    //           status: 1,             
    //           message: "Fetched liveTraining details successfully.",
    //           data: response
    //         });
    //       }else{
    //         res.send({
    //           status: 0,
    //           message: "Details Unvailable."
    //         });
    //       }
    //     }
    //   }
    // );

    let d = new Date();
    let firstDay = new Date(d.getFullYear(), d.getMonth(), 0);
    let lastday = new Date(d.getFullYear(), d.getMonth(), 31);
    console.log('last30days -->',firstDay , lastday);

    liveTraining.aggregate([{
      $match: {
        date: {
          $gt: firstDay,
          $lt: lastday
        }
      }
    }]).exec(
      (err, User) => {
        if (err) {
          // callback({
          //     status: 0,
          //     message: "Error fetching data",
          //   },
          //   null
          // );

          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });

        } else {
          
          res.send({ 
          status: 1,             
          message: "Fetched liveTraining details successfully.",
          data: User
          });


        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});


/* Get all Trainig */

/* get training by id */
router.post("/getTrainingById", async (req, res) => {
  const id  = req.body.id;
  try {
    liveTraining.find(
      { _id : id  },
      async function (err, response) {
        if (err) {
          res.send({   
            status:0,         
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
            res.send({ 
              status: 1,             
              message: "Fetched liveTraining details successfully.",
              data: response
            });
          }else{
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );

    
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});

/*  Get all types of News  */
router.post("/getAllNewsAbout", async (req, res) => {
  try {
    async.parallel({
      news: getNewsAbout,
      articles: getArticlesAbout,
    }, function (error, results) {
      if (error) {
        res.send({
          status: 0,
          message: "Oops! " + error.name + ": " + error.message,
        });
        return;
      }
      res.send(results);
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

/*  Get the Top archives  */
function getNewsAbout(callback) {
  News.find({page_status : "1"}).sort({ date: -1 }).limit(6).exec(callback); 
}

/*  Get the Latest archives  */
function getArticlesAbout(callback) {
  Articles.find({page_status : "1"}).sort({ _id: -1 }).limit(3).exec(callback); 
}

 
module.exports = router;







