/* load dependencies */

const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");
var mongoose = require("mongoose");
/* Include schema */
var Zendesk = require("zendesk-node-api");
const LiveTraining = require("./../models/liveTraining");
const EventUsers = require("./../models/event_users");
const Users = require("./../models/users");

const config = require("../config/config.json");
const { json } = require("body-parser");
const jwtKey = config.jwsPrivateKey;
const zendeskUrl = config.zendeskUrl;
const zendeskEmail = config.zendeskEmail;
const zendeskToken = config.zendeskToken;
var axios = require('axios');
//const ZendeskAuth = "Basic " + Buffer.alloc(128, zendeskEmail+'/token:'+zendeskToken).toString('base64')
const ZendeskAuth =
  "Basic " +
  new Buffer(zendeskEmail + "/token:" + zendeskToken).toString("base64");
/* http request port set */
const router = express.Router();

 

router.get("/getFAQ", async (req, res) => {
  try {
     
      
      
      const sectionsRequest = async () => {
        try {
            
            var config = {
              method: 'get',
              url: zendeskUrl+'/api/v2/help_center/en-us/categories/360005958132/sections',
              headers: { 
                'Authorization': ZendeskAuth  
              }
            }; 
            return await axios(config).then(function (response) { 
                    return response.data.sections;
            })
             
        } 
         catch (err) {
            // Handle Error Here
           return false
        }
      };
      
      const fetchPosts = async (id) => {
          try {
              var articleconfig = {
                  method: 'get',
                  url: zendeskUrl+'/api/v2/help_center/en-us/sections/'+id+'/articles',
                  headers: { 
                    'Authorization': ZendeskAuth 
                  }
              };  
            return await axios(articleconfig)
                .then(function (response) {  
                   return response.data.articles;   
               }) 
          } catch (error) {
            console.log(error);
          }
        };
      
      const articlesRequest = async (sections) => {
        try {   
            var posts = []; 
            
            for (const section of sections) { 
                var singlepost={'title':section.name,'id':section.id,'questions':[]}; 
                const post = await fetchPosts(section.id); 
                var j=0;
                 for (const pos of post) {
                     if(j<5)
                         {
                            singlepost.questions.push({'title':pos.title,'id':pos.id});  
                         }
                     j++;
                 }
                posts.push(singlepost);
            } 
            console.log(posts);
            return posts;
        } 
         catch (err) {
            // Handle Error Here
           return false
        }
      };
      var sections=await sectionsRequest();
      var articles=await articlesRequest(sections);  
      res.send({
                    status: 1,
                    data: articles,
                  });  
   
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

/*  Get Single Fa quetsions Details  */
router.post("/getFaqByCategoryID", async (req, res) => {
  const slug  = req.body.slug;
  try {
      
      const sectionsRequest = async () => {
        try {
            
            var config = {
              method: 'get',
              url: zendeskUrl+'/api/v2/help_center/en-us/sections/'+slug+'',
              headers: { 
                'Authorization': ZendeskAuth  
              }
            }; 
            return await axios(config).then(function (response) { 
                    return response.data.section;
            })
             
        } 
         catch (err) {
            // Handle Error Here
           return false
        }
      };
      
      const fetchPosts = async (id) => {
          try {
              var articleconfig = {
                  method: 'get',
                  url: zendeskUrl+'/api/v2/help_center/en-us/sections/'+id+'/articles',
                  headers: { 
                    'Authorization': ZendeskAuth 
                  }
              };  
            return await axios(articleconfig)
                .then(function (response) {  
                   return response.data.articles;   
               }) 
          } catch (error) {
            console.log(error);
          }
        };
      
      const articlesRequest = async (sections) => {
        try {   
            var posts = []; 
            
            
                const post = await fetchPosts(sections.id); 
                var j=0;
                 for (const pos of post) {  
                            posts.push({'title':pos.title,'id':pos.id}); 
                     
                     j++;
                 }  
            
            console.log(posts);
            return posts;
        } 
         catch (err) {
            // Handle Error Here
           return false
        }
      };
      var sections=await sectionsRequest();
      var articles=await articlesRequest(sections);  
      res.send({ 
                  status: 1,             
                  message: "Fetched Faq details successfully.",
                  data: articles,
                  category:sections.name
                });
      
    
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});


/*  Get Single News Details  */
router.post("/getsingleFaqByID", async (req, res) => {
   const slug  = req.body.slug;
   try {
       const fetchPosts = async (id) => {
          try {
              var articleconfig = {
                  method: 'get',
                  url: zendeskUrl+'/api/v2/help_center/en-us/articles/'+id+'/',
                  headers: { 
                    'Authorization': ZendeskAuth 
                  }
              };  
            return await axios(articleconfig)
                .then(function (response) {  
                   return response.data.article;   
               }) 
          } catch (error) {
            console.log(error);
          }
        };
      
      const articlesRequest = async (slug) => {
        try {   
           
                const post = await fetchPosts(slug); 
                  
            return post;
        } 
         catch (err) {
            // Handle Error Here
           return false
        }
      };
     
      var article=await articlesRequest(slug);  
      res.send({ 
                  status: 1,             
                  message: "Fetched Faq details successfully.",
                  data: article
                });
      
   } catch (e) {
     res.send({
       status: 0,
       message: "Oops! " + e.name + ": " + e.message,
     });
 }
 });
 
 router.post("/searchFaq", async (req, res) => {
 
  let query =  '';
  let searchtext  = req.body.data.searchkey;
  // console.log(searchtext);
  try { 
  
      const sectionsRequest = async () => {
        try {
            
            var config = {
              method: 'get',
              url: zendeskUrl+'/api/v2/help_center/en-us/categories/360005958132/sections',
              headers: { 
                'Authorization': ZendeskAuth  
              }
            }; 
            return await axios(config).then(function (response) { 
                    return response.data.sections;
            })
             
        } 
         catch (err) {
            // Handle Error Here
           return false
        }
      };
      
      const fetchPosts = async (id,searchtext) => {
          try {
              var articleconfig = {
                  method: 'get',
                  url:zendeskUrl+'/api/v2/help_center/articles/search.json?section='+id+'&query='+searchtext,
                  headers: { 
                    'Authorization': ZendeskAuth 
                  }
              };  
            return await axios(articleconfig)
                .then(function (response) {  
                   return response.data.results;   
               }) 
          } catch (error) {
            console.log(error);
          }
        };
      
      const articlesRequest = async (sections,searchtext) => {
        try {   
            var posts = []; 
            
            for (const section of sections) { 
                var singlepost={'title':section.name,'id':section.id,'questions':[]}; 
                const post = await fetchPosts(section.id,searchtext); 
                var j=0;
                 for (const pos of post) {
                     if(j<5)
                         {
                            singlepost.questions.push({'title':pos.title,'id':pos.id});  
                         }
                     j++;
                 }
                posts.push(singlepost);
            } 
            console.log(posts);
            return posts;
        } 
         catch (err) {
            // Handle Error Here
           return []
        }
      };
      var sections=await sectionsRequest();
      var articles=await articlesRequest(sections,searchtext);  
      res.send({
                    status: 1,
                    data: articles,
                  }); 
      
  } catch (e) {
    res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
    });
}

});



/*  Get  Faq quetsions Details  */
router.post("/searchFaqQuestions", async (req, res) => {
  const slug  = req.body.faq_slug;
  let searchtext  = req.body.data.searchkey;
  try {
    const sectionsRequest = async () => {
        try {
            
            var config = {
              method: 'get',
              url: zendeskUrl+'/api/v2/help_center/en-us/sections/'+slug+'',
              headers: { 
                'Authorization': ZendeskAuth  
              }
            }; 
            return await axios(config).then(function (response) { 
                    return response.data.section;
            })
             
        } 
         catch (err) {
            // Handle Error Here
           return false
        }
      };
      
      const fetchPosts = async (id,searchtext) => {
          try {
              var articleconfig = {
                  method: 'get',
                  url:zendeskUrl+'/api/v2/help_center/articles/search.json?section='+id+'&query='+searchtext,
                  headers: { 
                    'Authorization': ZendeskAuth 
                  }
              };  
            return await axios(articleconfig)
                .then(function (response) {  
                   return response.data.results;   
               }) 
          } catch (error) {
            console.log(error);
          }
        };
      
      const articlesRequest = async (sections,searchtext) => {
        try {   
            var posts = []; 
            
            
                const post = await fetchPosts(sections.id,searchtext); 
                var j=0;
                 for (const pos of post) {  
                            posts.push({'title':pos.title,'id':pos.id}); 
                     
                     j++;
                 }  
            
            console.log(posts);
            return posts;
        } 
         catch (err) {
            // Handle Error Here
           return false
        }
      };
      var sections=await sectionsRequest();
      var articles=await articlesRequest(sections,searchtext);  
      res.send({ 
                  status: 1,             
                  message: "Fetched Faq details successfully.",
                  data: articles,
                  category:sections.name
                });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});



module.exports = router;







