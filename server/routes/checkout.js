//load dependencies
require('dotenv').config();
const CURRENCY    = process.env.CURRENCY;
const WC_PROCESSING =   process.env.WC_PROCESSING;


const bcrypt = require("bcryptjs");
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const { v4: uuidv4 } = require('uuid');
var async = require('async');

const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
const path = require("path");
var mongoose = require('mongoose');

var utils = require('../config/utils');
//Include schema
const Orders = require("../models/orders");
const OrderNotes = require("../models/order_notes");
const Products = require("../models/products");
const Users = require("../models/users");
const ShippingSettings = require("../models/shipping_settings");
const ShippingMethod = require("../models/shipping_method");
const StripeModel = require("../models/stripe");
const OrderFailure = require("../models/order_failure");
const TaxData = require("../models/tax_data");
const Carts = require("../models/carts");
const Coupons = require("../models/coupons");
const couponsApplied =  require("../models/coupons_applied");


const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;

const upsCOnfig   =    config.ups;
//http request port set
const router = express.Router();

router.post("/getOrder", async (req, res) => {

    token = req.body.token;

    if(!token) {
        res.send({
            message: "Sorry! We could not find any orders " ,
            from: 'token check'
        })
    }

    payload = jwt.verify(token, jwtKey);

    try {
        let id = mongoose.Types.ObjectId(req.body.id);
        Orders.findOne(
            {_id: id, user_id: payload._id},
            async function (err, response) {
                if (err) {
                res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                });
                } else {
                    
                    if (response) {

                        res.send({
                            status: 1,
                            message: "Fetched order  details successfully.",
                            order: response,
                        });
                    } else {
                        //onsole.log(66);
                        res.send({
                            status: 0,
                            message: "Details Unavailable.",
                        });
                    }
                }
          }
        )
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
});

router.post("/upsValidateAddress", async (req, res) => {

    shipping = req.body.shipping;
    products = req.body.cartProducts;
    selectedProducts = products;
    cartProducts = [];    
    productIds = []; 

    products.forEach(product => { 
        if(product.quantity > 0 ) {            
            cartProducts[product._id] = product.quantity;
            productIds.push(product._id);
        }
    });

    if( productIds.length <= 0 ) {
        res.send({
            status: 0 ,
            message: "Oops! cart is empty" ,
            from: 'empty cart top'
        })
    }

    if(!shipping.zipcode || !shipping.city || !shipping.region || !shipping.country || !shipping.address) {
        res.send({status: 0 , message : "Please enter valid address"});
    }

    async.waterfall([  
        getUPSConfig,
        upsAddressValidation,          
        getShipmentOrigin,
        getTax,
        getCartProducts,
        getUPSRatings,    
    ], function (error, results) {

        if(error) {
            res.send(error);
        } else {

            res.send({
                status: 1,
                message: "Success",
                rating: results.upsRating,
                tax: results.tax,
                results: results
            });
        }
    });    
    
});

router.post("/payment", async (req, res) => {

    const { products, total, cartId, token, authUser } = req.body;
    couponDetails = req.body.couponDetails;
    couponCode = couponDetails.couponcode;
    selectedProducts = products;
    cartProducts = [];    
    productIds = [];    
    paymentId   =   req.body.id;
    shipping = req.body.shippingDetails;
    cartUniqueId = cartId;

    if(!token) {
        res.send({
            message: "Plaese login to process checkout" ,
            from: 'empty cart top'
        })
    }

    payload = jwt.verify(token, jwtKey);

    products.forEach(product => { 
        if(product.quantity > 0 ) {            
            cartProducts[product._id] = product.quantity;
            productIds.push(product._id);
        }
    });

    if( productIds.length <= 0 ) {
        res.send({
            message: "Oops! cart is empty" ,
            from: 'empty cart top'
        })
    }

    try {

        async.waterfall([    
            getUPSConfig, 
            validateShipping,  
            upsAddressValidation, 
            getShipmentOrigin,  
            getStripeConfig,  
            getUser,
            getTax,
            getCartProducts,
            getCoupon,
            checkCouponApplied, 
            processCoupon,
            getUPSRatingsPayment,
            createPayment,
            createOrder,             
            createOrderNotes,            
            manageStock,
            saveCouponApplied,
            clearCart,
            sendOrderMail,


        ], function (error, results) {
            if(error) {
                res.send(error);
            } else {

                res.send({
                    status: 1,
                    message: "Success",
                    order: results
                });
            }
        });
   
    } catch (e) {
        res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
        });
    }
});
function getUPSConfig (callback) {

    const results = {};
    try{
        ShippingMethod.findOne({ slug : 'ups'} , 
            async function (err, response) {
                if (err ) {
                    callback({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                        from: 'shippment_UPS_config'
                    },null);
                } 
                if(response) {
                    results.upsSettings = response;
                    callback(null, results);                     
                } else {
                    callback({
                        status: 0,
                        message: "Sorry no shipping methods found" ,
                        from: 'shippment_UPS_config'
                    }, null); 
                }
                
            }
        )
    } catch (e) {
        callback({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
            from: 'shippment_UPS_config'
        }, null);
    }
}
function validateShipping( results, callback) {
    var validate = utils.validateShippingDetails(shipping);        
    if(validate.status === false) {
        callback({
            status: 2,                
            message: validate.errors,
            from: 'User data validation'
        },null);  
        return;          
    } else {
        callback(null, results);
    }
}
function upsAddressValidation(results, callback) {

    const { upsSettings } = results;
    const USE_SANDBOX = upsSettings.sandbox;
    const UPS_ACCESS_KEY = upsSettings.sandbox == true ? upsSettings.sandbox_access_key : upsSettings.access_key;
    const UPS_USER_ID = upsSettings.sandbox == true ? upsSettings.sandbox_username : upsSettings.username;
    const UPS_PASSWORD = upsSettings.sandbox == true ? upsSettings.sandbox_password : upsSettings.password;

    var AddressValidation = require('../lib/addressValidation');
    var validateAddress = new AddressValidation(UPS_ACCESS_KEY, UPS_USER_ID, UPS_PASSWORD);
    validateAddress.useSandbox(USE_SANDBOX);
    validateAddress.setJsonResponse(true);

    validateAddress.makeRequest({
        CountryCode: shipping.country,
        customerContext: shipping.address,
        city: shipping.city,
        stateProvinceCode: shipping.region,
        PostalCode: shipping.zipcode
    }, function (result, data) {

        if (result) {

            if (result.AddressValidationResponse.Response[0].ResponseStatusCode[0] == 1) {
                results.validAddress = {
                    status: result.AddressValidationResponse.Response[0].ResponseStatusCode[0],
                    message: result.AddressValidationResponse.Response[0].ResponseStatusDescription[0],
                    
                    result: result
                };
                callback(null, results);
            }
            else {
                callback({
                    status: result.AddressValidationResponse.Response[0].ResponseStatusCode[0],
                    message: result.AddressValidationResponse.Response[0].Error[0].ErrorDescription[0],                    
                    result: result
                }, null);
            }

        }
        else {
            callback({
                status: 0,
                message: "Please enter valid address"
            }, null);
        }

    });

}
function getShipmentOrigin (results, callback ) {

    try{
        ShippingSettings.findOne({},
            async function (err, response) {

                if (err ) {
                    callback({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                        from: 'shippment_origin'
                    },null);
                } 
                if(response.length == 0) {
                    callback({
                        status: 0,
                        message: "Sorry no shipping methods found" ,
                        from: 'shippment_origin'
                    }, null); 
                }
                results.shipmentOrigin = response;
                callback(null, results); 
            }
        )
    } catch (e) {
        callback({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
            from: 'shippment_origin'
        }, null);
    }
}
function getStripeConfig (results, callback) {

    try{
        StripeModel.findOne({ slug : 'stripe_payment'} , 
            async function (err, response) {
                if (err ) {
                    callback({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                        from: 'stripe_payment'
                    },null);
                } 
                if(response) {
                    results.stripeSettings = response;
                    callback(null, results);                     
                } else {
                    callback({
                        status: 0,
                        message: "Sorry no shipping methods found" ,
                        from: 'stripe_payment'
                    }, null); 
                }
                
            }
        )
    } catch (e) {
        callback({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
            from: 'shippment_UPS_config'
        }, null);
    }
}
function getTax( results, callback ){

    async.series({
        tax1 : function(callbackTax) {
            try {
                TaxData.findOne({ 
                    country : shipping.country,
                    state : shipping.region,
                    zipcode: shipping.zipcode
                } , 
                async function (err, response) {
                    if (err ) {
                        callbackTax({
                            status: 0,
                            message: "Oops! " + err.name + ": " + err.message,
                            from: 'tax'
                        },null);
                    }                      
                    callbackTax(null, (response ? response : {} ));                     
                    
                })
            } catch ( err ) {
                callbackTax({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                    from: 'tax'
                },null);
            }
        },
        tax2 : function( callbackTax){

            try {
                TaxData.findOne({ 
                    country : shipping.country,
                    state : shipping.region,
                    zipcode : '*'
                } , 
                async function (err, response) {
                    if (err ) {
                        callbackTax({
                            status: 0,
                            message: "Oops! " + err.name + ": " + err.message,
                            from: 'tax'
                        },null);
                    } 
                    callbackTax(null, (response ? response : {} ));                                            
                })
            } catch ( err ) {
                callbackTax({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                    from: 'tax'
                },null);
            }
        },
        tax3 : function( callbackTax){

            try {
                TaxData.findOne({ 
                    country : shipping.country,
                    state : '*',
                    zipcode : '*'
                } , 
                async function (err, response) {
                    if (err ) {
                        callbackTax({
                            status: 0,
                            message: "Oops! " + err.name + ": " + err.message,
                            from: 'tax'
                        },null);
                    } 
                    callbackTax(null, (response ? response : {} ));                                            
                })
            } catch ( err ) {
                callbackTax({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                    from: 'tax'
                },null);
            }
        }

    }, function(err, taxResults) {
        if(err) {
            callback(err, null);
        }
        if(taxResults) {

            let tax = {};
            if(Object.keys(taxResults.tax1).length > 0 ) {
                tax = taxResults.tax1;
            } else if(Object.keys(taxResults.tax2).length > 0 ) {
                tax = taxResults.tax2;
            }
            else if(Object.keys(taxResults.tax3).length > 0 ) {
                tax = taxResults.tax3;
            }
            results.tax = tax ;
            callback(null, results);
        }
    });
        
}
function getUser(results, callback) {

    try {
        var validate = utils.validateShippingDetails(shipping);
        
        if(validate.status === false) {
            callback({
                status: 2,                
                message: validate.errors,
                from: 'User data validation'
            },null);  
            return;          
        }
        if(validate.status === true) {

            //{ email: shipping.email }
            Users.findOne( {_id : payload._id }, async function (error, user) {
                
                if (error ) {
                    callback({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                        from: 'User Query fetch'
                    },null);
                } 
                if ( user ) { 

                    results.user = user;
                    results.isNewUser = false;            
                    callback(null, results);

                }  else {

                    callback({
                        status: 0,
                        message: "Sorry no user found",
                        from: 'User Query fetch'
                    },null);

                    //decrypt password
                    // const salt = bcrypt.genSaltSync(10);
                    // let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
                    // bcryptPassword = bcrypt.hashSync(utils.generatePassword(), salt);  

                    // newUserData = JSON.parse(JSON.stringify(shipping))            ;
                    // newUserData.user_name = shipping.email;
                    // newUserData.password = bcryptPassword;
                    // newUserData.created_on = createdon;
                    // newUserData.status = 1;
                    // newUserData.profile_image = '';  
                    // newUserData.usertype = 'subscriber' 
                    // newUserData.verify = true;         
                    // const newUser = new Users(newUserData);
                    
                    // //save user to mongoo db
                    // try {
                    //     await newUser
                    //     .save()
                    //     .then(() => {

                    //         results.user = newUser;
                    //         results.isNewUser = true;
                    //         callback(null, results);
                    //     })
                    //     .catch((err) => {
                    //         callback({
                    //             status: 0,
                    //             message: "Oops! " + err.name + ": " + err.message,
                    //             from: 'User Query save'
                    //         },null);
                    //     });
                    // } catch (err) {
                    //     callback({
                    //         status: 0,
                    //         message: "Oops! " + err.name + ": " + err.message,
                    //         from: 'User Query save catch'
                    //     },null);
                    
                    // }
                }
            });
        }
    } catch (e) {
        callback({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
            from: 'User Query fetch catch'
        }, null);
    }
}
function getCartProducts(results,callback) {
 
    const { upsSettings } = results;
    try{
        Products.find({
            _id: {
                $in: productIds,
            },
            page_status: "1"

            },
            async function (err, response) {

                if (err ) {
                    callback({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                        from: 'cart products fetch'
                    },null);
                } 
                if(response.length == 0) {
                    callback({
                        status: 0,
                        message: "Oops! cart is empty" ,
                        from: 'empty cart'
                    }, null); 
                }
                if (response.length > 0) {  

                    let cartFlag    =   true;
                    let message     =   "";
                    let total = 0;
                    let orderItems  =   [];
                    let productStock  =   [];

                    let totalWeight = 0;
                    let packages = [];

                    for(let product of response) {

                        if(utils.productAvailable(product,cartProducts[product._id])) {


                            
                            let price = utils.getPrice(product.price);
                            let lineItemTotal =  utils.lineItemTotal(price, cartProducts[product._id]);
                            total = total +lineItemTotal;

                            let weight = product.packages.weights;
                            let lineItemWeightTotal =  cartProducts[product._id] * weight;
                            totalWeight = totalWeight + lineItemWeightTotal;
                            orderItems.push({
                                'product_id': product._id,
                                'product_name': product.product_name,
                                'quantity': cartProducts[product._id],
                                'price': price,
                                'line_item_total': lineItemTotal,
                                'line_item_weight_total': lineItemWeightTotal,
                                'packages': product.packages,
                                'product_url': product.url
                            }) ;

                            
                            productStock.push( product._id )

                            if(upsSettings.packing_type == '01') {

                                for(i = 0; i < cartProducts[product._id]; i++) {
                                    packages.push ( {
                                        description: product.product_name,
                                        code: upsSettings.packaging_type_code,
                                        weight: weight,
                                        length: product.packages.lengths,
                                        width: product.packages.widths,
                                        height: product.packages.heights,
                                    });
                                }
                            } else if( upsSettings.packing_type == '02'){
                                packages.push ( {
                                    description: product.product_name,
                                    code: upsSettings.packaging_type_code,
                                    weight: product.lineItemWeightTotal
                                });
                            }
                        } else {
                            cartFlag = false;
                            message = "Oops! sorry the requested quantity of product "+product.product_name+" is not available" ;
                            break;
                        }
                    };

                    if(cartFlag == false ) {
                        callback({
                            status: 0,
                            message: message ,
                            from: 'qunatity check'
                        }, null); 
                    }
                    if(total == 0 ) {
                        callback({
                            status: 0,
                            message: message ,
                            from: 'qunatity check'
                        }, null); 
                    }

                    results.products = orderItems;
                    results.productStock   =   productStock;
                    results.total = total;
                    results.packages = packages;
                    results.totalWeight = totalWeight;

                    callback(null,results); 
                } 
            }
        )
    } catch (e) {
        callback({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
            from: 'cart products fetch'
        }, null);
    }

}
function getUPSRatingsPayment(results, callback) {

    const { upsSettings } = results;  
    if(upsSettings.carrier_services.length <= 0) {
        callback( {
            status: 0,
            message: "Sorry no shipping methods found"
        }, null)
    } else {
        try{
            results.carrier_services    =   [shipping.shipping_methods];
            ratingAndServices(results).then( upsresponse => {
            
                let ratings = [];
                let error = {};
                let isValid =  false; 
                upsresponse.forEach ( upsRate => {
                    
                    if(upsRate.RatingServiceSelectionResponse.Response[0].ResponseStatusCode[0] == 1) {

                        isValid = true;
                        let code = upsRate.RatingServiceSelectionResponse.RatedShipment[0].Service[0].Code[0];
                        let service = upsCOnfig.services[code];                    
                        let currency = upsRate.RatingServiceSelectionResponse.RatedShipment[0].TotalCharges[0].CurrencyCode[0] ;
                        let charge = upsRate.RatingServiceSelectionResponse.RatedShipment[0].TotalCharges[0].MonetaryValue[0] ;
                        
                        ratings.push ( {
                            code: code,
                            service : service,
                            currency: currency,
                            charge: charge
                        })
                    } else {
                        error = {
                            status: upsRate.RatingServiceSelectionResponse.Response[0].ResponseStatusCode[0],
                            message: upsRate.RatingServiceSelectionResponse.Response[0].Error[0].ErrorDescription[0]
                        }
                    }
                })

                if(isValid) {
                    results.upsRating = ratings;
                    callback( null, results);
                } else {
                    callback (error, null);
                }
            });
        } catch (err) {
            callback( {
                status: 0,
                message: "Oops! " + e.name + ": " + e.message,
            }, null)
        }
    }
}
async function createPayment(results, callback) {

    

    /***********PROCEESS PAYMENT ********* */
    const stripeSettings = results.stripeSettings;

    const USE_SANDBOX   = stripeSettings.value.live == false ? true : false;
    const SECRET_KEY    = USE_SANDBOX == true ? stripeSettings.value.Testsecretkey : stripeSettings.value.Livesecretkey;
    const stripe        = require('stripe')(SECRET_KEY);
    

    const productTotal = parseFloat(results.total) ;

    let discount = results.discount;
    if( productTotal < results.discount ) {
        discount = productTotal;
    } 

    const productDiscountTotal = ( parseFloat(productTotal) - parseFloat(discount) ) > 0 ? (parseFloat(productTotal) - parseFloat(discount) ): 0;
    const shippingTotal = parseFloat(results.upsRating[0].charge);
    let taxTotal = 0;

    if(Object.keys(results.tax).length > 0 ) {
        if(results.tax.shipping) {
            taxTotal =     ( (productTotal+shippingTotal)* parseFloat(results.tax.rate_percent) ) / 100  ;
        } else {
            taxTotal    =  ( productTotal * parseFloat(results.tax.rate_percent)) / 100 ;
        }
    }
    
    const amount    =   ((parseFloat(productDiscountTotal)+parseFloat(shippingTotal)+parseFloat(taxTotal)) * 100 ). toFixed(0);
    const idempotencyKey    =   uuidv4();               
    try {
        const paymentIntent = await stripe.paymentIntents.create({
             amount,
             currency: CURRENCY,
             payment_method:paymentId,
             confirm:true,
             description: "Cimon Product Purchase",
        }, {idempotencyKey}); 

        if (paymentIntent) {

            if(paymentIntent.status != "succeeded") {
                callback({
                    status: 0,
                    mesaage:"Sorry! Failed to make a payment. Please try again",
                    from: 'make payment'
                }, null);
            } 
            results.paymentDetails = paymentIntent;
            return results;
            // callback(null, results) ;
        } else {
            callback({
                status: 0,
                message: "Failed to make payment. Please try again",
                from: 'make payment catch'
            }, null);
        }

    } catch (e) {
        callback({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
            from: 'make payment catch'
        }, null);
    } 
}
function createOrder(results, callback) {

    const { upsSettings, shipmentOrigin } = results;
    utils.getValueForNextSequence("order_id").then( order_no => {
        try {        
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const productTotal = parseFloat(results.total) ;
            
            let discount = results.discount;
            if( productTotal < results.discount ) {
                discount = productTotal;
            } 

            const productDiscountTotal =  ( parseFloat(productTotal) - parseFloat(discount) ) > 0 ? ( parseFloat(productTotal) - parseFloat(discount) ) : 0 ;

            const shippingTotal = parseFloat(results.upsRating[0].charge);
            let taxTotal = 0;

            if(Object.keys(results.tax).length > 0 ) {
                if(results.tax.shipping) {
                    taxTotal =     ( (productTotal+shippingTotal)* parseFloat(results.tax.rate_percent) ) / 100  ;
                } else {
                    taxTotal    =  ( productTotal * parseFloat(results.tax.rate_percent)) / 100 ;
                }
            }
            const amount    =   (productDiscountTotal+shippingTotal+taxTotal).toFixed(2);
            const taxDetails = {
                "tax_name": results.tax.tax_name,
                "shipping": results.tax.shipping,
                "country": results.tax.country,
                "state": results.tax.state,
                "zipcode": results.tax.zipcode,
                "rate_percent": results.tax.rate_percent,
                "priority": results.tax.priority,
            }

            const couponInfo = {
                "couponcode": results.coupon.couponcode,
                "slug": results.coupon.slug,
                "amount": results.coupon.amount,
                "discounttype" : results.coupon.discounttype,
                "exiprydate": results.coupon.exiprydate,
                "products" : results.coupon.products,
                "users" :  results.coupon.users,
                "couponstatus"  : results.coupon.couponstatus,
                "guestusers" : results.coupon.guestusers,
            };


            let upsOptions = createUpsOptions(results);
            const newOrder = new Orders({
                order_no: utils.padWithZeroes(order_no,4),
                created_on: createdon,
                page_status: 1,
                user_id: results.user._id,
                status: WC_PROCESSING,
                shipping_details: shipping,
                shipping_method: results.upsRating[0],
                tax: taxDetails,
                coupon_details : couponInfo,
                order_details: {
                    
                    product: results.products,                    
                    total : {
                        tax_total: parseFloat(taxTotal.toFixed(2)),
                        shipping_total: shippingTotal,
                        product_total: results.total,
                        discount: parseFloat(discount),
                        total: parseFloat(amount)
                    }
                },
                payment_details: {
                    id:  results.paymentDetails.id,
                    otherdetails: results.paymentDetails
                    
                },
                ups_options: upsOptions
        
            });

            newOrder
            .save()
            .then((result) => {
                results.order = newOrder;            
                callback(null, results);
            })
        } catch (err) {
            let e = "Oops! " + err.name + ": " + err.message;
            results.order = {_id : '-1', status: 'failure'};
            
            handleOrderFailure(results, e, 1).then (() => {
                callback({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                    from: 'save order catch'
                }, null);
            })

        }

    });
    
    
}
function createOrderNotes(results, callback) {

    let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
    const newOrderNotes = new OrderNotes({
        created_on: createdon,
        order_id: results.order._id,
        status: results.order.status,
        message: "Order status changed to "+ results.order.status + ". Stripe charge completed (Id : "+ results.paymentDetails.id +")"
    });

    try {
        newOrderNotes
        .save()
        .then((result) => {
            results.orderNotes = newOrderNotes;            
            callback(null, results);
        })
        .catch((err) => {
            let e = "Oops! " + err.name + ": " + err.message;
            handleOrderFailure(results, e, 2).then (() => {
                callback(null, results);
            })

        });

    } catch (err) {
        let e = "Oops! " + err.name + ": " + err.message;
        handleOrderFailure(results, e, 2).then (() => {
            callback(null, results);
        })

    }
}
function manageStock(results, callback) {

    let productStock = results.productStock;
    try{
        Products.find({
            _id: {
                $in: productStock
            },
            page_status: "1"
            },
            async function (err, products) {

                if (err ) {
                    let e = "Oops! " + err.name + ": " + err.message;
                    handleOrderFailure(results, e, 3).then (() => {
                        callback(null, results);
                    })

                } 
                if(products.length == 0) {
                    let e = "Failed stock updation" ;
                    handleOrderFailure(results, e, 3).then (() => {
                        callback(null, results);
                    })                    
                }
                if (products.length > 0) {  

                    products.forEach(product => {
                        
                        if(product.inventory.manage_stock == true) {

                            let stockQuantity = parseInt(product.inventory.manage_inventory.stock_quantity)
                            letQuantitySelected = parseInt(cartProducts[product._id]);
                            let availableQuantity = (stockQuantity - letQuantitySelected) > 0 ? (stockQuantity - letQuantitySelected) : 0                                

                            Products.updateOne (
                                {_id: product._id},
                                { '$set': {"inventory.manage_inventory.stock_quantity" : availableQuantity} },
                                {multi: true},
                                function (err,log) {
                                    if(err){
                                        
                                    }                                        
                                }
                            );

                        }
                    });

                    callback(null, results);
                } 
            }
        )
    } catch (err) {
        let e = "Oops! " + err.name + ": " + err.message;
        handleOrderFailure(results, e, 3).then (() => {
            callback(null, results);
        })
    }



}
function clearCart(results, callback) {
    
    try {
        Carts.findByIdAndDelete({ _id: mongoose.Types.ObjectId(cartUniqueId)  }, async function (err,log) {
            callback(null, results);
        });
    } catch (err) {

        let e = "Oops! failed to clear cart";
        handleOrderFailure(results, e, 5).then (() => {
            callback(null, results);
        })
    }

}
function saveCouponApplied( results, callback){
    
    if(couponCode == '' ) { 
        callback(null, results);
    } else {
        let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
        const newCouponApplied = new couponsApplied({
            created_on: createdon,
            order_id: results.order._id,
            user_id: results.order.user_id,
            couponcode: results.order.coupon_details.couponcode,
            amount: results.order.order_details.total.discount
        });
    
        try {
            newCouponApplied
            .save()
            .then((result) => {
                results.couponAPplied = newCouponApplied;            
                callback(null, results);
            })
            .catch((err) => {
                let e = "Oops! " + err.name + ": " + err.message;
                handleOrderFailure(results, e, 2).then (() => {
                    callback(null, results);
                })
    
            });
    
        } catch (err) {
            
            let e = "Oops! failed to save coupon details";
            handleOrderFailure(results, e, ).then (() => {
                callback(null, results);
            })
        }
    }

}
function sendOrderMail(results, callback) {


    let header = fs
    .readFileSync(path.join(__dirname + "/emails/header.html"), {
      encootding: "utf-8"
    })
    .toString();
    var view = {
        siteurl: config.siteurl,
        imageUrl:config.imageUrl,     
    };
    var createHeader = mustache.render(header, view);


    let footer = fs
    .readFileSync(path.join(__dirname + "/emails/footer.html"), {
      encootding: "utf-8"
    })
    .toString();
    var view = {
        imageUrl:config.imageUrl,       
    };
    var createFooter = mustache.render(footer, view);


    let template = fs
    .readFileSync(path.join(__dirname + "/emails/customer-processing-order.html"), {
      encootding: "utf-8"
    })
    .toString();

    let total = {
            product_total : results.order.order_details.total.product_total,
            shipping_total: results.order.order_details.total.shipping_total,
            shipping_method: results.upsRating[0],
            tax : results.order.order_details.total.tax_total, 
            discount : results.order.order_details.total.discount,
            coupon: results.order.coupon_details.couponcode,
            payment_method: 'Stripe' ,
            grand_total: results.order.order_details.total.total
        };
    var orderHtml = utils.generateOrderHtml(selectedProducts, total);

    var view = {
        first_name:shipping.first_name,
        last_name:shipping.last_name,
        email:shipping.email,
        address: shipping.address,
        appartment: shipping.appartment,
        city: shipping.city,
        state: shipping.state,
        country: shipping.country,
        zipcode: shipping.zipcode,
        phone: shipping.phone,
        order: orderHtml,        
    };
    var createTemplate = mustache.render(template, view);
    var htmlContent = createHeader+createTemplate+createFooter;

    sgMail.setApiKey(sendGridKey);
    const message = {
        to:shipping.email,
        // from: config.fromEmail,
        from : { email : config.fromEmail , name: 'Cimon'},
        subject: "Order Received",
        html: htmlContent
    };

    try {
        sgMail
            .send(message)
            .then(() => {
                callback(null, results);
            })
            .catch(error => {
                const { message, code, response } = error;
                let e = "Oops! failed to send order email";
                handleOrderFailure(results, e, 4).then (() => {
                    callback(null, results);
                })
            });
    } catch (err) {
        let e = "Oops! " + err.name + ": " + err.message;
        handleOrderFailure(results, e, 4).then (() => {
            callback(null, results);
        })
    }
}
function getUPSRatings(results, callback) {
    const { upsSettings } = results;  
    if(upsSettings.carrier_services.length <= 0) {
        callback( {
            status: 0,
            message: "Sorry no shipping methods found"
        }, null)
    } else {
        try{
            results.carrier_services    =   upsSettings.carrier_services;
            ratingAndServices(results).then( upsresponse => {
            
                let ratings = [];
                let error = {};
                let isValid =  false; 
                upsresponse.forEach ( upsRate => {
                    
                    if(upsRate.RatingServiceSelectionResponse.Response[0].ResponseStatusCode[0] == 1) {

                        isValid = true;
                        let code = upsRate.RatingServiceSelectionResponse.RatedShipment[0].Service[0].Code[0];
                        let service = upsCOnfig.services[code];                    
                        let currency = upsRate.RatingServiceSelectionResponse.RatedShipment[0].TotalCharges[0].CurrencyCode[0] ;
                        let charge = upsRate.RatingServiceSelectionResponse.RatedShipment[0].TotalCharges[0].MonetaryValue[0] ;
                        
                        ratings.push ( {
                            code: code,
                            service : service,
                            currency: currency,
                            charge: charge
                        })
                    } else {
                        error = {
                            status: upsRate.RatingServiceSelectionResponse.Response[0].ResponseStatusCode[0],
                            message: upsRate.RatingServiceSelectionResponse.Response[0].Error[0].ErrorDescription[0]
                        }
                    }
                })

                if(isValid) {
                    results.upsRating = ratings;
                    callback( null, results);
                } else {
                    callback (error, null);
                }
            });
        } catch (err) {
            callback( {
                status: 0,
                message: "Oops! " + e.name + ": " + e.message,
            }, null)
        }
    }
}
async function ratingAndServices( results ) {

    const { upsSettings, shipmentOrigin } = results;
    const USE_SANDBOX   =   upsSettings.sandbox;
    const UPS_ACCESS_KEY = (USE_SANDBOX == true)  ? upsSettings.sandbox_access_key : upsSettings.access_key;
    const UPS_ACCOUNT_NUMBER = (USE_SANDBOX == true)  ? upsSettings.sandbox_account_no : upsSettings.acount_no;
    const UPS_USER_ID    = (USE_SANDBOX == true)  ? upsSettings.sandbox_username : upsSettings.username;
    const UPS_PASSWORD    = (USE_SANDBOX == true)  ? upsSettings.sandbox_password : upsSettings.password;

    var Rating = require('../lib/rating');
    var getRating = new Rating(UPS_ACCESS_KEY, UPS_USER_ID, UPS_PASSWORD);
    getRating.useSandbox(USE_SANDBOX);
    getRating.setJsonResponse(true);

    var promiseArray = [];

    let upsOptions = createUpsOptions(results);

    /*
    let upsOptions = {
        customerContext: "Rating and Service",
        pickUpType: {
            code: upsSettings.ups_pickup_type ? upsSettings.ups_pickup_type : upsCOnfig.default_pickup_type,
            description: "Rate"
        },
        CustomerClassification: {
            code: upsSettings.ups_customer_classification
        },
        shipment: {
            description: "Rate Description",
            name: upsCOnfig.name,
            phoneNumber: upsCOnfig.phone,
            shipperNumber: UPS_ACCOUNT_NUMBER,
            shipper: {
                address: {
                    addressLine1: shipmentOrigin.street_address,
                    addressLine2: shipmentOrigin.street_address1,
                    city: shipmentOrigin.city,
                    StateProvinceCode: shipmentOrigin.region,
                    PostalCode: shipmentOrigin.zipcode,
                    countryCode: shipmentOrigin.country
                }
            },
            shipTo: {
                companyName: shipping.company,
                phoneNumber: shipping.phone,
                address: {
                    addressLine1: shipping.address.trim(),
                    addressLine2: shipping.appartment.trim(),
                    city: shipping.city,
                    StateProvinceCode: shipping.region,
                    postalCode: shipping.zipcode,
                    countryCode: shipping.country
                }
            },
            shipFrom: {
                companyName: upsCOnfig.name,
                attentionName: upsCOnfig.name,
                phoneNumber: upsCOnfig.phone,
                faxNumber: upsCOnfig.fax,
                address: {
                    addressLine1: shipmentOrigin.street_address,
                    addressLine2: shipmentOrigin.street_address1,
                    city: shipmentOrigin.city,
                    StateProvinceCode: shipmentOrigin.region,
                    PostalCode: shipmentOrigin.zipcode,
                    countryCode: shipmentOrigin.country
                }
            },            
            paymentInformation: {
                accountNumber: UPS_ACCOUNT_NUMBER
            },
            package: results.packages,
        }
    };
    */
    
    results.carrier_services.forEach( code => {
        upsOptions.shipment.service = { code: code };
        promiseArray.push(new Promise((resolve, reject) => {
            
            getRating.makeRequest(upsOptions, function(response) {
                if (response) {
                    resolve(response)
                }
            });
        }))
    })

    return (await Promise.all(promiseArray));

}
function createUpsOptions (results) {

    const { upsSettings, shipmentOrigin } = results;
    const USE_SANDBOX   =   upsSettings.sandbox;
    const UPS_ACCESS_KEY = (USE_SANDBOX == true)  ? upsSettings.sandbox_access_key : upsSettings.access_key;
    const UPS_ACCOUNT_NUMBER = (USE_SANDBOX == true)  ? upsSettings.sandbox_account_no : upsSettings.acount_no;
    const UPS_USER_ID    = (USE_SANDBOX == true)  ? upsSettings.sandbox_username : upsSettings.username;
    const UPS_PASSWORD    = (USE_SANDBOX == true)  ? upsSettings.sandbox_password : upsSettings.password;

    let upsOptions = {
        customerContext: "Rating and Service",
        pickUpType: {
            code: upsSettings.ups_pickup_type ? upsSettings.ups_pickup_type : upsCOnfig.default_pickup_type,
            description: "Rate"
        },
        CustomerClassification: {
            code: upsSettings.ups_customer_classification
        },
        shipment: {
            description: "Rate Description",
            name: upsCOnfig.name,
            phoneNumber: upsCOnfig.phone,
            shipperNumber: UPS_ACCOUNT_NUMBER,
            shipper: {
                address: {
                    addressLine1: shipmentOrigin.street_address,
                    addressLine2: shipmentOrigin.street_address1,
                    city: shipmentOrigin.city,
                    StateProvinceCode: shipmentOrigin.region,
                    PostalCode: shipmentOrigin.zipcode,
                    countryCode: shipmentOrigin.country
                }
            },
            shipTo: {
                companyName: shipping.company,
                phoneNumber: shipping.phone,
                address: {
                    addressLine1: shipping.address.trim(),
                    addressLine2: shipping.appartment.trim(),
                    city: shipping.city,
                    StateProvinceCode: shipping.region,
                    postalCode: shipping.zipcode,
                    countryCode: shipping.country
                }
            },
            shipFrom: {
                companyName: upsCOnfig.name,
                attentionName: upsCOnfig.name,
                phoneNumber: upsCOnfig.phone,
                faxNumber: upsCOnfig.fax,
                address: {
                    addressLine1: shipmentOrigin.street_address,
                    addressLine2: shipmentOrigin.street_address1,
                    city: shipmentOrigin.city,
                    StateProvinceCode: shipmentOrigin.region,
                    PostalCode: shipmentOrigin.zipcode,
                    countryCode: shipmentOrigin.country
                }
            },            
            paymentInformation: {
                accountNumber: UPS_ACCOUNT_NUMBER
            },
            package: results.packages,
        }
    };

    return upsOptions;

}
function handleOrderFailure(results, error, type) {
    switch( type ) {
        case 1 : 
            message = "Payment successful with payment id : "+ results.paymentDetails.id+" .Failed to save order";
            break;
        case 2 :
            message = "Payment successful with payment id : "+ results.paymentDetails.id+" Order created successfully with order id :"+results.order._id+". Failed to save order notes";
            break; 
        case 3 :
            message = "Payment successful with payment id : "+ results.paymentDetails.id+" Order created successfully with order id :"+results.order._id+". Failed to update stock";
            break; 
        case 4 :
            message = "Payment successful with payment id : "+ results.paymentDetails.id+" Order created successfully with order id :"+results.order._id+". Failed to send order email";
            break; 
        case 5 :
            message = "Payment successful with payment id : "+ results.paymentDetails.id+" Order created successfully with order id :"+results.order._id+". Failed to clear cart";
            break;
        case 6 :
            message = "Payment successful with payment id : "+ results.paymentDetails.id+" Order created successfully with order id :"+results.order._id+". Failed to save coupon applied";
            break; 
    }
    return new Promise((resolve,reject)=>{
        let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
        let data =   {
            user_id: results.user._id ,
            products: results.products,
            total: results.total,
            packages: results.packages,
            shipping_method: results.upsRating[0],
            payment_details: results.paymentDetails,
            order: results.order
        }
        const orderFail= new OrderFailure({
            created_on: createdon,
            order_id: results.order._id ? results.order._id : '',
            status: results.order.status ? results.order.status : '',
            message: message,
            error: error,
            order_data: data
        });
        orderFail
        .save()
        .then(() => {
            return resolve( results );
        })
        .catch((err) => {
            return reject(results);
        });

    });
}
function getCoupon(results, callback) {

    if(couponCode == '' ) {
        results.coupon = setCouponData();
        results.discount = 0;
        callback( null, results);
    } else {

        try {
            Coupons.findOne( {couponcode: couponCode, couponstatus: 1  },
                async function ( err, response ) {

                    if(err || !response) {

                        const error = {
                            status: 0,
                            message:  "Please enter valid coupon code",
                            from:'fetch coupon'
                        }
                        callback( error, null);


                    } else if(response) {

                        const expiryValid = checkCouponExpiry(response)
                        const productValid = checkCouponValidWithProducts(response, results);
                        const userValid = checkCouponValidWIthUser( response, results ); 
                        if ( expiryValid && userValid && productValid.isValid) {

                            results.couponProductSpecific = productValid.productCoupon;
                            results.coupon = response;
                            callback(null, results);

                        } else {

                            const error = {
                                status: 0,
                                message: "Please enter valid coupon code",
                                from:'fetch coupon'
                            }
                            callback(error, null);
                        }
                        
                    } 

                }
            )
        }
        catch (err) {
            const error = {
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
                from: 'get coupon'
            }
            callback(error, null);
            
        }

    }

}
function checkCouponApplied(results, callback) {

    const user_id = results.user._id ? results.user._id : '' ; 
    results.discountApplied = 0;
    if ( couponCode == '' ) {
        results.coupon = setCouponData();
        results.discount = 0;
        callback(null, results) ; 
    } else {
        
        try {


            couponsApplied.aggregate( [
                {$match: { "couponcode" : couponCode, "user_id": user_id  } },
                { $group: { _id: "$user_id", discountApplied: { $sum: "$amount" } } }
                ],
                async function ( err, response ) {

                    if(err) {
                        const error = {
                            status: 0,
                            message: "Please enter valid coupon code",
                            from:'order coupon'
                        };
                         
                        callback(error, null) ;                      
                    } else if(response.length > 0) {

                        const discountApplied = response[0].discountApplied;
                        if(results.coupon.discounttype == 1) {
                            if( (results.coupon.amount - discountApplied ) > 0 ) {

                                results.discountApplied = discountApplied;
                                callback(null, results);
                            } else {

                                const error = {
                                    status: 0,
                                    message: "Sorry you have already applied the coupon",
                                    from:'order coupon applied'
                                };
                                callback(error, null) ;  

                            }
                        } else {
                            const error = {
                                status: 0,
                                message: "Sorry you have already applied the coupon",
                                from:'order coupon applied'
                            };
                            callback(error, null) ;  
                        }                        
                    } else {
                        callback(null, results);
                    }
                }
            )
        } catch ( err ) {
            const error = {
                status: 0,
                message: "Oops! " + e.name + ": " + e.message,
                from: 'check coupon applied',
            };
            callback(error, null) ;              
        }
    }
}
function checkCouponExpiry( response ) {

    let isValidCoupon = true;
    if(response.exiprydate) {

        var currentDate = new Date(new Date().toDateString());
        var exipryDate = new Date(new Date(response.exiprydate).toDateString());
        if(currentDate.getTime() > exipryDate.getTime()){
            isValidCoupon = false;                                    
        }                  
    } 
    return isValidCoupon;
}
function checkCouponValidWithProducts( response, results ) {

    
    let isValid = false;
    let productCoupon = false;
    if(response.products.length > 0 ) {

        for(var x in response.products) {

            results.products.forEach( item => {

                if(item.product_id.toString() === response.products[x]) {
                    isValid = true;
                    productCoupon = true;
                }
            })                            
        }
    } else  {
        isValid = true;
    }
    return {isValid : isValid, productCoupon : productCoupon };
}
function checkCouponValidWIthUser( response, results ) {
        
    let isValid = false;
    if(response.users.length > 0) {

        const user_id = results.user._id ? results.user._id : -1 ;
        response.users.forEach( user => {
            if(user.toString() === user_id.toString() ) {
                isValid = true;
            }
        }) 

    } else {
        isValid= true;
    }
    return isValid;
}
function processCoupon(results, callback) {

    if(couponCode == '' ) {
        results.coupon = setCouponData();
        results.discount = 0;
        callback(null, results);        
    } else {

        if( results.couponProductSpecific == true ) {

            let discount = 0;
            if(results.coupon.products.length > 0 ) {

                for(var x in results.coupon.products) {
                    results.products.forEach( item => {
                        if(item.product_id.toString() === results.coupon.products[x]) {
                            if(results.coupon.discounttype == 1) {
                                discount = ( results.coupon.amount - results.discountApplied ) > 0 ? ( results.coupon.amount - results.discountApplied ) : 0;
                            } else {
        
                                const total     =  parseFloat(item.price) * item.quantity;;
                                const percent   =  results.coupon.amount;  
                                discount = discount + total * percent /100;        
                            }
                        }
                    })                            
                }
            }
            results.discount = discount;
            callback(null, results);

        } else {

            let discount = 0;
            if(results.coupon.discounttype == 1) {
                discount = ( results.coupon.amount - results.discountApplied ) > 0 ? ( results.coupon.amount - results.discountApplied ) : 0;
            } else {
                const total = results.total; 
                const percent =  results.coupon.amount;  
                discount = total * percent /100;        
            }
            results.discount = discount;
            callback(null, results);
        }
    }
}
function setCouponData( ) {

    const couponInfo = {
        "couponcode": '',
        "slug": '',
        "amount": '',
        "discounttype" : '',
        "exiprydate": '',
        "products" : '',
        "users" :  '',
        "couponstatus"  : '',
        "guestusers" : '',
    };
    return couponInfo;
}

module.exports = router;
