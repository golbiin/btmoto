/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");

/* Include schema */
const News = require("../models/news");
const Archives = require("../models/archives");
const CaseStudy = require("../models/case_study");
const Products = require("../models/products");
const Categories = require("../models/categories");
const Menus = require("../models/menus");
const config = require("../config/config.json");

const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();

// /*  Get all types of News  */
// router.post("/getAllNews", async (req, res) => {
//   try {
//     async.parallel({
//       latestNews: getLatestNews,
//       recentNews: getRecentlyPublishedNews,
//       topNews: getTopNews
//     }, function (error, results) {
//       if (error) {
//         res.status(500).send(error);
//         return;
//       }
//       res.send(results);
//     });
//   } catch (e) {
//     res.send({
//       status: 0,
//       message: "Oops! " + e.name + ": " + e.message,
//     });
//   }
// });

/*  Get the Latest Casestudy  */
router.post("/getLatestCasestudy", async (req, res) => {
  console.log("req.body", req.body);

  const page = parseInt(req.body.page);
  const PAGE_SIZE = 9;
  const skip = (page - 1) * PAGE_SIZE;
  var totalPages = 0;
  try {
    if (page < 0 || page === 0) {
      response = {
        status: 0,
        message: "Invalid page number, should start with 1"
      };
      return res.json(response);
    }
    /* News Count */
    CaseStudy.count({ page_status: "1" }, async function(err, totalCount) {
      if (err) {
        response = { status: 0, message: "Error fetching data" };
      } else {
        totalPages = Math.ceil(totalCount / PAGE_SIZE);
      }
    });

    let query1 = (searchtext = searchproduct = searchcat = date = "");
    // searchtext  = req.body.serach.search ? req.body.serach.search : '';
    searchproduct = req.body.serach.product ? req.body.serach.product : "";
    searchcat = req.body.serach.category ? req.body.serach.category : "";
    date = req.body.serach.date ? req.body.serach.date : "";
    if (
      searchcat === "" &&
      searchproduct === "" &&
      searchtext === "" &&
      date === ""
    ) {
      /* find all Casestudy */

      console.log("srach 1");
      var query = CaseStudy.find({ page_status: "1" });
      query.skip(skip);
      query.limit(PAGE_SIZE);
      query.sort({ data_order: 1 });
      query.exec(async function(err, response) {
        if (err) {
          response = { status: 0, message: "Error fetching data" };
        }
        res.send({
          status: 1,
          CurrentPage: page,
          latestNews: response,
          totalPages: totalPages
        });
      });
    } else {
      console.log("srach 2");

      if (searchproduct && searchcat === "") {
        console.log("searchproduct");
        query1 = {
          $or: [
            { title: { $regex: searchtext, $options: "i" } },
            { sub_title: { $regex: searchtext, $options: "i" } },
            { description: { $regex: searchtext, $options: "i" } },
            { releasedate: { $regex: date, $options: "i" } }
          ],
          $and: [
            { related_products: { $regex: searchproduct, $options: "i" } },
            { releasedate: { $regex: date, $options: "i" } }
          ]
        };
      } else if (searchproduct === "" && searchcat) {
        console.log("searchcat");
        query1 = {
          $or: [
            { title: { $regex: searchtext, $options: "i" } },
            { sub_title: { $regex: searchtext, $options: "i" } },
            { description: { $regex: searchtext, $options: "i" } },
            { releasedate: { $regex: date, $options: "i" } }
          ],
          $and: [{ related_cat: { $regex: searchcat, $options: "i" } }]
        };
      } else if (searchproduct && searchcat) {
        console.log("both");
        query1 = {
          $or: [
            { title: { $regex: searchtext, $options: "i" } },
            { sub_title: { $regex: searchtext, $options: "i" } },
            { description: { $regex: searchtext, $options: "i" } },
            { releasedate: { $regex: date, $options: "i" } }
          ],
          $and: [
            { related_cat: { $regex: searchcat, $options: "i" } },
            { related_products: { $regex: searchproduct, $options: "i" } }
          ]
        };
      } else {
        console.log("else ");
        query1 = {
          $or: [
            { title: { $regex: searchtext, $options: "i" } },
            { sub_title: { $regex: searchtext, $options: "i" } },
            { description: { $regex: searchtext, $options: "i" } }
          ]
          // "$and": [
          //   { "releasedate": {  "$regex":date , '$options' : 'i'} },
          //  ],
        };
      }

      var query = CaseStudy.find(query1);
      query.skip(skip);
      query.limit(PAGE_SIZE);
      query.sort({ date: -1 });
      query.exec(async function(err, response) {
        if (err) {
          response = { status: 0, message: "Error fetching data" };
        }
        res.send({
          status: 1,
          CurrentPage: page,
          latestNews: response,
          totalPages: totalPages
        });
      });
    }
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/*  Get the Top Casestudy  */
function getTopCaseStudy(callback) {
  News.find({ is_top_case_study: "1", page_status: "1" })
    .sort({ _id: -1 })
    .exec(callback);
}

/* get top Casestudy */

router.post("/getTopCaseStudy", async (req, res) => {
  try {
    CaseStudy.find(
      {
        $and: [{ is_top_case_study: "1" }, { page_status: "1" }]
      },
      async function(err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (response.length > 0) {
            res.send({
              status: 1,
              message: "Fetched details successfully.",
              result: response
            });
          } else {
            res.send({
              result: response,
              status: 0,
              message: "No details available."
            });
          }
        }
      }
    )
      .sort({
        _id: -1
      })
      .limit(1);
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/*  Get Single Casestudy Details  */
router.post("/getCasestudyBySlug", async (req, res) => {
  const slug = req.body.slug;
  try {
    CaseStudy.findOne({ slug: slug }, async function(err, response) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (response) {
          res.send({
            status: 1,
            message: "Fetched CaseStudy details successfully.",
            data: response
          });
        } else {
          res.send({
            status: 0,
            message: "Details Unvailable."
          });
        }
      }
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/* get 3 casestudy */

router.post("/get3casestudy", async (req, res) => {
  try {
    CaseStudy.find({ page_status: 1 }, async function(err, response) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (response.length > 0) {
          res.send({
            status: 1,
            message: "Fetched details successfully.",
            result: response
          });
        } else {
          res.send({
            result: response,
            status: 0,
            message: "No details available."
          });
        }
      }
    })
      .sort({
        date: -1
      })
      .limit(3);
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/getAllProductsIdsandNames", async (req, res) => {
  const token = req.body.token;

  try {
    Products.find({
      page_status: "1"
    }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (log.length > 0) {
          res.send({
            status: 1,
            message: "Fetched Products successfully.",
            data: log
          });
        } else {
          res.send({
            status: 0,
            message: "No Product available."
          });
        }
      }
    })
      .sort({ date: -1 })
      .select("product_name _id");
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/serachFunction", async (req, res) => {
  try {
    let query = "";
    let searchtext = req.body.text.search;
    let searchproduct = req.body.text.product;
    let searchcat = req.body.text.category;
    let date = req.body.text.date;
    console.log("searchtext sdsd", req.body.text.date);

    if (
      searchcat === "" &&
      searchproduct === "" &&
      searchtext === "" &&
      date === ""
    ) {
      res.send({
        status: 2,
        message: "Fetched CaseStudy successfully."
      });
    }

    if (searchproduct && searchcat === "") {
      console.log("searchproduct 123");
      query = {
        $or: [
          { title: { $regex: searchtext, $options: "i" } },
          { sub_title: { $regex: searchtext, $options: "i" } },
          { description: { $regex: searchtext, $options: "i" } },
          { releasedate: { $regex: date, $options: "i" } }
        ],
        $and: [
          { related_products: { $regex: searchproduct, $options: "i" } },
          { releasedate: { $regex: date, $options: "i" } },
          { page_status: "1" }
        ]
      };
    } else if (searchproduct === "" && searchcat) {
      console.log("searchcat");
      query = {
        $or: [
          { title: { $regex: searchtext, $options: "i" } },
          { sub_title: { $regex: searchtext, $options: "i" } },
          { description: { $regex: searchtext, $options: "i" } },
          { releasedate: { $regex: date, $options: "i" } }
        ],
        $and: [
          { related_cat: { $regex: searchcat, $options: "i" } },
          { page_status: "1" }
        ]
      };
    } else if (searchproduct && searchcat) {
      console.log("both");
      query = {
        $or: [
          { title: { $regex: searchtext, $options: "i" } },
          { sub_title: { $regex: searchtext, $options: "i" } },
          { description: { $regex: searchtext, $options: "i" } },
          { releasedate: { $regex: date, $options: "i" } }
        ],
        $and: [
          { related_cat: { $regex: searchcat, $options: "i" } },
          { related_products: { $regex: searchproduct, $options: "i" } },
          { page_status: "1" }
        ]
      };
    } else {
      console.log("else ");
      query = {
        $or: [
          { title: { $regex: searchtext, $options: "i" } },
          { sub_title: { $regex: searchtext, $options: "i" } },
          { description: { $regex: searchtext, $options: "i" } }
        ],
        $and: [{ page_status: "1" }]
      };
    }

    CaseStudy.find(query, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (log.length > 0) {
          res.send({
            status: 1,
            message: "Fetched CaseStudy successfully.",
            data: log
          });
        } else {
          res.send({
            status: 0,
            message: "No CaseStudy available."
          });
        }
      }
    }).sort({ date: -1 });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/getAllCatsIdsandNames", async (req, res) => {
  try {
    Categories.find(
      {
        status: 1
      },
      async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Fetched categories successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No categories available."
            });
          }
        }
      }
    )
      .sort({ category_name: 1 })
      .select("category_name _id");
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/getIndustryMenu", async (req, res) => {
  try {
    Menus.find({ slug: "industries-casestudy" }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (log.length > 0) {
          res.send({
            status: 1,
            message: "Fetched menus successfully.",
            data: log[0].menu_items
          });
        } else {
          res.send({
            status: 0,
            message: "No menus available."
          });
        }
      }
    }).sort({ date: -1 });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/getProductMenu", async (req, res) => {
  try {
    Menus.find({ slug: "industries-products" }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (log.length > 0) {
          res.send({
            status: 1,
            message: "Fetched menus successfully.",
            data: log[0].menu_items
          });
        } else {
          res.send({
            status: 0,
            message: "No menus available."
          });
        }
      }
    }).sort({ date: -1 });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

module.exports = router;
