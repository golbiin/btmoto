//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
var async = require("async");
var mongoose = require("mongoose");
var Zendesk = require("zendesk-node-api");
const multer = require("multer");
const https = require("https");
//const request = require('request');
var curl = require("curlrequest");
const { Curl } = require("node-libcurl");

//Include schema
var utils = require("../config/utils");
const Users = require("../models/users");
const Carts = require("../models/carts");
const Products = require("../models/products");
const News = require("../models/news");
const Language = require("../models/language");
const Pages = require("../models/pages");

const Faq = require("../models/faq");
const Careers = require("../models/careers");
const Articles = require("../models/articles");
const LiveTraining = require("../models/liveTraining");
const VideoTraining = require("../models/videoTraining");

const ProfileTab = require("../models/profile_tab");
const FeedBack = require("../models/feedBack");
const Menus = require("../models/menus");
const Settings = require("../models/settings");
//include joi validation methods
const Validate = require("./validations/validate");
const uploadPath = "upload/";
const config = require("./../config/config.json");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const userTypes = config.userTypes;
const fromMail = config.fromEmail;
const USER_TYPES = config.userTypes;
const siteurl = config.siteurl;
const hubspotKey = config.hubspotKey;
const zendeskUrl = config.zendeskUrl;
const zendeskEmail = config.zendeskEmail;
const zendeskToken = config.zendeskToken;
//const ZendeskAuth = "Basic " + Buffer.alloc(128, zendeskEmail+'/token:'+zendeskToken).toString('base64')
const ZendeskAuth =
  "Basic " +
  new Buffer(zendeskEmail + "/token:" + zendeskToken).toString("base64");
const Hubspot = require("hubspot");
const hubspot = new Hubspot({ apiKey: hubspotKey });

//http request port set
const router = express.Router();
const url = config.uploadURL;
/* Login Authentication */
router.post("/authenticate", async (req, res) => {
  try {
    Validate.validateLogin(req, result => {
      if (result.error) {
        //validation error
        res.status(400).send(result.error.details[0].message);
      } else {
        async.waterfall(
          [
            async.apply(setUserLoginParams, req.body),
            userLogin,
            getUserCart,
            getGuestCart,
            mergeCart,
            removeGuestCart
          ],
          function(err, results) {
            if (err) {
              res.send(err);
            } else {
              const userInfo = {
                _id: results.user._id,
                email: results.user.email,
                first_name: results.user.first_name,
                last_name: results.user.last_name,
                usertype: results.user.usertype,
                uniq_id: results.user.uniq_id,
                company_name: results.user.company_name,
                country: results.user.country,
                phone_number: results.user.phone_number,
                city: results.user.city,
                zipcode: results.user.zipcode,
              };

              res.send({
                status: 1,
                message: "Login success!You will be redirected now",
                data: {
                  token: results.token,
                  user: userInfo,
                  cartId: results.cartId,
                  guestCart: results.guestCart
                }
              });
            }
          }
        );
      }
    });
  } catch (err) {
    res.status(400).send(ex);
  }
});

function setUserLoginParams(params, callback) {
  let results = {};
  const userType = params.usertype;
  var condition = { email: -1 };
  if (userType == USER_TYPES.SUBSCRIBER) {
    condition = {
      $or: [{ username: params.username }, { email: params.username }]
    };
  } else if (
    userType == USER_TYPES.INTEGRATOR ||
    userType == USER_TYPES.DISTRIBUTOR
  ) {
    condition = { uniq_id: params.username, usertype: params.usertype };
  }
  results.params = params;
  results.condition = condition;
  callback(null, results);
}
function userLogin(results, callback) {
  //user login check
  try {
    Users.findOne(results.condition, function(err, log) {
      if (err) {
        callback(
          {
            status: 0,
            message: "Oops! Something went wrong,please try later!"
          },
          null
        );
      } else if (!log) {
        callback(
          {
            status: 0,
            message:
              "There is no existing user with the details that you’ve provided."
          },
          null
        );
      } else {
        var validPassword = true;
        if (log.usertype == USER_TYPES.SUBSCRIBER) {
          const validatepassword = bcrypt.compareSync(
            results.params.password,
            log.password
          );
          if (!validatepassword) {
            validPassword = false;
          }
        }
        const verifiedEmail = log.verify;
        const last_login = log.last_login;
        if (validPassword && verifiedEmail) {
          log.last_login = moment().format("MM-DD-YYYY hh:mm:ss");
          log.save();
          const token = jwt.sign(
            {
              _id: log._id,
              email: log.email,
              first_name: log.first_name,
              last_name: log.last_name,
              usertype: log.usertype,
              uniq_id: log.uniq_id
            },
            jwtKey
          );

          results.token = token;
          results.user = log;
          results.lastlogin = last_login;
          callback(null, results);
        } else {
          const error =
            validPassword == false
              ? {
                  status: 0,
                  message:
                    "You have entered an invalid username and/or password."
                }
              : {
                  status: 0,
                  message: "Please verify your email to login"
                };
          callback(error);
        }
      }
    });
  } catch (ex) {
    callback(
      {
        status: 0,
        message: "Oops! Something went wrong,please try later!"
      },
      null
    );
  }
}

function getUserCart(results, callback) {
  try {
    Carts.findOne({ user_id: results.user._id }, async function(err, response) {
      let cartId = "";
      if (response) {
        cartId = response._id;
      }
      results.cartId = cartId;
      results.userCart = response;
      callback(null, results);
    });
  } catch (ex) {
    results.cartId = "";
    callback(null, results);
  }
}
function getGuestCart(results, callback) {
  if (results.params.cartId == "") {
    results.guestCartId = "";
    results.guestCart = {};
    callback(null, results);
  } else {
    try {
      Carts.findOne(
        { _id: mongoose.Types.ObjectId(results.params.cartId) },
        async function(err, response) {
          let guestCart = {};
          guestCartId = "";
          if (response) {
            guestCart = response;
            guestCartId = response._id;
          }
          results.guestCartId = guestCartId;
          results.guestCart = guestCart;
          callback(null, results);
        }
      );
    } catch (ex) {
      results.guestCartId = "";
      results.guestCart = {};
      callback(null, results);
    }
  }
}

function mergeCart(results, callback) {
  const CURRENCY_CODE = process.env.CURRENCY;
  const CURRENCY_SYMBOL = process.env.CURRENCY_SYMBOL;
  results.removeGuestCart = false;

  if (
    (results.cartId == "" && results.guestCartId == "") ||
    (results.cartId !== "" && results.guestCartId == "")
  ) {
    callback(null, results);
  } else if (results.cartId == "" && results.guestCartId !== "") {
    try {
      Carts.findByIdAndUpdate(
        { _id: results.guestCartId },
        {
          user_id: results.user._id
        },
        async function(err, response) {
          results.cartId = results.guestCartId;
          callback(null, results);
        }
      );
    } catch (err) {
      callback(null, results);
    }
  } else if (results.cartId !== "" && results.guestCartId !== "") {
    const cartProducts = results.userCart.products;

    results.guestCart.products.forEach(product => {
      let productAlreadyInCart = false;
      cartProducts.forEach(cp => {
        if (cp._id.equals(product._id)) {
          cp.quantity = cp.quantity + product.quantity;
          productAlreadyInCart = true;
        }
      });
      if (!productAlreadyInCart) {
        let productDetails = {
          _id: product._id,
          product_name: product.product_name,
          images: product.images,
          inventory: product.inventory,
          quantity: product.quantity,
          price: product.price,
          slug: product.slug,
          url: product.url
        };
        cartProducts.push(productDetails);
      }
    });

    let productQuantity = cartProducts.reduce((sum, p) => {
      sum += parseInt(p.quantity);
      return sum;
    }, 0);

    let totalPrice = cartProducts.reduce((sum, p) => {
      let price = utils.getPrice(p.price);
      sum += price * p.quantity;
      return sum;
    }, 0);

    let cartTotal = {
      productQuantity,
      totalPrice,
      currencyId: CURRENCY_CODE,
      currencyFormat: CURRENCY_SYMBOL
    };
    try {
      Carts.findByIdAndUpdate(
        { _id: results.cartId },
        {
          products: cartProducts,
          total: cartTotal
        },
        async function(err, response) {
          results.removeGuestCart = true;
          callback(null, results);
        }
      );
    } catch (err) {
      callback(null, results);
    }
  }
}

function removeGuestCart(results, callback) {
  if (!results.removeGuestCart) {
    callback(null, results);
  }
  try {
    if (results.removeGuestCart) {
      Carts.findByIdAndDelete({ _id: results.guestCartId }, async function(
        err,
        log
      ) {
        callback(null, results);
      });
    }
  } catch (err) {
    callback(null, results);
  }
}
/* Get User details */
router.post("/getUserDetails", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Users.findOne(
        { email: req.body.email },
        {
          profile_image: 1,
          first_name: 1,
          last_name: 1,
          email: 1,
          usertype: 1,
          uniq_id: 1,
          company_name: 1,
          country: 1,
          phone_number: 1,
          city: 1,
          zipcode: 1,
          recovery_email: 1,
          drivelink: 1,
          bussiness_type: 1,
          industry: 1,
          interested_products: 1,
          agree_newsletter: 1
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log) {
              res.send({
                status: 1,
                message: "Fetched user details successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/* insert user */
router.post("/register", async (req, res) => {
  const data = req.body.data;
  try {
    Validate.RegisterUser(data, result => {
      if (result.error) {
        res.send({
          status: 0,
          message: result.error.details[0].message
        });
      } else {
        //user existance check
        Users.findOne({ email: data.email }, async function(error, log) {
          if (log != null) {
            res.send({
              status: 2,
              message:
                "This email has already been used to register. Please use another."
            });
          } else {
            //decrypt password
            const uniqid = require("uniqid");
            const salt = bcrypt.genSaltSync(10);
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            bcryptPassword = bcrypt.hashSync(data.password, salt);

            const verifyToken64 = jwt.sign(
              { createdon: createdon, email: data.email },
              jwtKey
            );

            let bufferObj = Buffer.from(verifyToken64, "utf8");
            let verify_token = bufferObj.toString("base64");

            const newUser = new Users({
              user_name: data.email,
              first_name: data.first_name,
              last_name: data.last_name,
              email: data.email,
              password: bcryptPassword,
              created_on: createdon,
              status: 0,
              profile_image: "",
              usertype: userTypes.SUBSCRIBER,
              uniq_id: uniqid(),
              verify: false,
              verify_token: verify_token,
              agree_newsletter: data.agree_newsletter == 1 ? true : false,
              agree_terms: data.agree_terms == 1 ? true : false,
              bussiness_type: data.bussiness_type,
              city: data.city,
              company_name: data.company_name,
              country: data.country,
              industry: data.industry,
              interested_products: data.interested_products,
              phone_number: data.phone_number,
              recovery_email: data.recovery_email,
              zipcode: data.zipcode
            });

            //save user to mongoo db
            try {
              await newUser
                .save()
                .then(() => {
                  const contactObj = {
                    properties: [
                      { property: "firstname", value: data.first_name },
                      { property: "email", value: data.email },
                      { property: "lastname", value: data.last_name },
                      { property: "phone", value: data.phone_number },
                      { property: "company", value: data.company_name },
                      { property: "state", value: data.last_name },
                      { property: "zip", value: data.zipcode },
                      { property: "city", value: data.city }
                    ]
                  };
                  const hubspotContact = hubspot.contacts.create(contactObj);
                  console.log(hubspotContact);
                  sendRegisterMail(newUser, result => {
                    sendAdminRegisterMail(newUser, result => {
                      console.log(result);
                    });
                    if (result == "success") {
                      res.send({
                        status: 1,
                        message:
                          "You have successfully registered. Please verify your email address."
                      });
                    } else if (result == "failed") {
                      res.send({
                        status: 0,
                        type: "error",
                        message: "Mail Delivery failed"
                      });
                    }
                  });
                })
                .catch(err => {
                  res.send({
                    status: 0,
                    type: "error catch",
                    message: "Oops! " + err.name + ": " + err.message
                  });
                });
            } catch (ex) {
              res.send({
                status: 0,
                type: "error catch",
                message: "Oops! Something went wrong with sending  mail...!"
              });
            }
          }
        });
      }
    });
  } catch (ex) {
    res.status(400).send(ex);
  }
});

function sendRegisterMail(results, callback) {
  let header = fs
    .readFileSync(path.join(__dirname + "/emails/header.html"), {
      encootding: "utf-8"
    })
    .toString();
  var view = {
    siteurl: config.siteurl,
    imageUrl: config.imageUrl
  };
  var createHeader = mustache.render(header, view);

  let footer = fs
    .readFileSync(path.join(__dirname + "/emails/footer.html"), {
      encootding: "utf-8"
    })
    .toString();
  var view = {
    imageUrl: config.imageUrl
  };
  var createFooter = mustache.render(footer, view);

  let template = fs
    .readFileSync(path.join(__dirname + "/emails/register.html"), {
      encootding: "utf-8"
    })
    .toString();

  const verifyUrl = config.siteurl + "verify/" + results.verify_token;

  var view = {
    first_name: results.first_name,
    last_name: results.last_name,
    verify_url: verifyUrl
  };
  var createTemplate = mustache.render(template, view);
  var htmlContent = createHeader + createTemplate + createFooter;

  sgMail.setApiKey(sendGridKey);
  const message = {
    to: results.email,
    from: { email: config.fromEmail, name: "CIMON" },
    subject: "Registration Completed. Verify Your Email",
    html: htmlContent
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        callback("success");
      })
      .catch(error => {
        callback("failed");
      });
  } catch (err) {
    callback("failed");
  }
}



function sendAdminRegisterMail(results, callback) {
  let header = fs
    .readFileSync(path.join(__dirname + "/emails/header.html"), {
      encootding: "utf-8"
    })
    .toString();
  var view = {
    siteurl: config.siteurl,
    imageUrl: config.imageUrl
  };
  var createHeader = mustache.render(header, view);

  let footer = fs
    .readFileSync(path.join(__dirname + "/emails/footer.html"), {
      encootding: "utf-8"
    })
    .toString();
  var view = {
    imageUrl: config.imageUrl
  };
  var createFooter = mustache.render(footer, view);

  let template = fs
    .readFileSync(path.join(__dirname + "/emails/register-adminemail.html"), {
      encootding: "utf-8"
    })
    .toString();

  var view = {
    name: results.first_name + " " + results.last_name,
    email: results.email,
    company: results.company_name,
    country: results.country,
    city: results.city,
    zip: results.zipcode,
    phone: results.phone_number,
    interested: results.interested_products,
    busines_type: results.bussiness_type,
    industry: results.industry,
    recovery: results.recovery_email
  };

  var createTemplate = mustache.render(template, view);
  var htmlContent = createHeader + createTemplate + createFooter;

  sgMail.setApiKey(sendGridKey);
  const message = {
    to: config.admin_email,
    from: { email: config.fromEmail, name: "CIMON" },
    subject: "New submission from Registration",
    html: htmlContent
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        callback("success");
      })
      .catch(error => {
        callback("failed");
      });
  } catch (err) {
    callback("failed");
  }
}

/* Token Verification */
router.post("/verifyToken", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid request"
    });
  }
  try {
    // Create a buffer from the string
    let bufferObj = Buffer.from(token, "base64");
    // Encode the Buffer as a utf8 string
    let emailToken = bufferObj.toString("utf8");
    payload = jwt.verify(emailToken, jwtKey);

    Users.findOne({ email: payload.email, verify_token: token }, async function(
      error,
      log
    ) {
      if (log != null) {
        if (log.verify == true) {
          res.send({
            status: 2,
            message:
              "You have already verified your email. Please click here to "
          });
        } else {
          Users.findByIdAndUpdate(
            { _id: log._id },
            { verify: true },
            async function(err, log) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              } else {
                res.send({
                  status: 1,
                  message:
                    "You have successfully validated your email. Please click here to ",
                  data: { user_data: log }
                });
              }
            }
          );
        }
      } else {
        res.send({
          status: 0,
          message: "Invalid verification request"
        });
      }
    });
  } catch (ex) {
    res.send({
      status: 0,
      message: "Invalid verification request"
    });
  }
});

/* forgot password */
router.post("/forgotPassword", async (req, res) => {
  if (req.body.data.forgot_email) {
    try {
      Users.findOne(
        { email: req.body.data.forgot_email },
        await function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (!log) {
            res.send({
              status: 0,
              message:
                "There is no existing user with the details that you’ve provided."
            });
          } else {
            try {
              sendForgotpwdMail(log, result => {
                if (result.status == "success") {
                  var update_value = {
                    reset_token: result.token,
                    reset_status: false
                  };
                  var condition = { _id: log._id };
                  Users.findByIdAndUpdate(
                    condition,
                    update_value,
                    async function(err, log) {
                      if (err) {
                        res.send({
                          status: 0,
                          message:
                            "Oops! Something went wrong with sending Email."
                        });
                      } else {
                        res.send({
                          status: 1,
                          message:
                            "We have sent you a confirmation email. Please follow the instructions in the email."
                        });
                      }
                    }
                  );
                } else {
                  res.send({
                    status: 0,
                    message: "Oops! Something went wrong with sending Email."
                  });
                }
              });
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            }
          }
        }
      );
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  } else {
    res.send({ status: 0, message: "Please enter a properly formatted email" });
  }
});

function sendForgotpwdMail(results, callback) {
  let header = fs
    .readFileSync(path.join(__dirname + "/emails/header.html"), {
      encootding: "utf-8"
    })
    .toString();

  var view = {
    siteurl: config.siteurl,
    imageUrl: config.imageUrl
  };
  var createHeader = mustache.render(header, view);

  let footer = fs
    .readFileSync(path.join(__dirname + "/emails/footer.html"), {
      encootding: "utf-8"
    })
    .toString();

  var view = {
    imageUrl: config.imageUrl
  };
  var createFooter = mustache.render(footer, view);

  let template = fs
    .readFileSync(path.join(__dirname + "/emails/forgotpassword.html"), {
      encootding: "utf-8"
    })
    .toString();

  const token = jwt.sign({ _id: results._id, email: results.email }, jwtKey);

  let urlpass = config.siteurl + "reset-password/" + token + "";
  var view = {
    username: results.email,
    siteuUrl: urlpass
  };

  var createTemplate = mustache.render(template, view);
  var htmlContent = createHeader + createTemplate + createFooter;
  sgMail.setApiKey(sendGridKey);
  const message = {
    to: results.email,
    from: { email: config.fromEmail, name: "CIMON" },
    subject: "Forgot Password",
    html: htmlContent
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        callback({
          status: "success",
          token: token
        });
      })
      .catch(error => {
        const { message, code, response } = error;
        callback({
          status: "failed"
        });
      });
  } catch (err) {
    callback({
      status: "failed"
    });
  }
}

/* forgot Recovery email */

router.post("/forgotRecoveryEmail", async (req, res) => {
  if (req.body.data.forgot_email) {
    try {
      Users.findOne(
        { recovery_email: req.body.data.forgot_email },
        await function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (!log) {
            res.send({
              status: 0,
              message:
                "There is no existing user with the details that you’ve provided."
            });
          } else {
            try {
              sendForgotrecoMail(log, result => {
                if (result.status == "success") {
                  if (err) {
                    res.send({
                      status: 0,
                      message:
                        "Oops! Something went wrong with sending verification mail."
                    });
                  } else {
                    res.send({
                      status: 1,
                      message: "Please check your email "
                    });
                  }
                } else {
                  res.send({
                    status: 0,
                    message:
                      "Oops! Something went wrong with sending verification mail."
                  });
                }
              });
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            }
          }
        }
      );
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  } else {
    res.send({ status: 0, message: "Please enter a properly formatted email" });
  }
});

function sendForgotrecoMail(results, callback) {
  let header = fs
    .readFileSync(path.join(__dirname + "/emails/header.html"), {
      encootding: "utf-8"
    })
    .toString();

  var view = {
    siteurl: config.siteurl,
    imageUrl: config.imageUrl
  };
  var createHeader = mustache.render(header, view);

  let footer = fs
    .readFileSync(path.join(__dirname + "/emails/footer.html"), {
      encootding: "utf-8"
    })
    .toString();

  var view = {
    imageUrl: config.imageUrl
  };
  var createFooter = mustache.render(footer, view);

  let template = fs
    .readFileSync(path.join(__dirname + "/emails/recoveryemail.html"), {
      encootding: "utf-8"
    })
    .toString();

  const token = jwt.sign({ _id: results._id, email: results.email }, jwtKey);

  var view = {
    email: results.email
  };

  var createTemplate = mustache.render(template, view);
  var htmlContent = createHeader + createTemplate + createFooter;
  sgMail.setApiKey(sendGridKey);
  const message = {
    to: results.recovery_email,
    from: { email: config.fromEmail, name: "CIMON" },
    subject: "Recovery Mail",
    html: htmlContent
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        callback({
          status: "success",
          token: token
        });
      })

      .catch(error => {
        const { message, code, response } = error;
        callback({
          status: "failed"
        });
      });
  } catch (err) {
    callback({
      status: "failed"
    });
  }
}

/* Reset password */
router.post("/resetPassword", async (req, res) => {
  if (req.body.user_id) {
    const token = req.body.user_id;
    payload = jwt.verify(token, jwtKey);
    try {
      Users.findOne(
        { _id: payload._id, email: payload.email, reset_token: token },
        await function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (!log) {
            res.send({
              status: 0,
              message:
                "There is no existing user with the details that you’ve provided."
            });
          } else {
            try {
              if (log.reset_status == true) {
                res.send({
                  status: 0,
                  message: "The reset password link expired"
                });
              } else {
                //decrypt password
                const salt = bcrypt.genSaltSync(10);
                let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
                bcryptPassword = bcrypt.hashSync(req.body.data.password, salt);
                log.password = bcryptPassword;
                log.reset_status = true;
                log.save();
                res.send({
                  status: 1,
                  message:
                    "You have successfully updated your password. Please continue to "
                });
              }
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            }
          }
        }
      );
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  } else {
    res.send({ status: 0, message: "Invalid request." });
  }
});

/* Update profile */

router.post("/updateProfile", async (req, res) => {
  if (req.body.token) {
    const token = req.body.token;
    const data = req.body.data;
    payload = jwt.verify(token, jwtKey);
    console.log("updated value", data);
    try {
      Validate.updateUser(data, result => {
        if (result.error) {
          res.send({
            status: 0,
            message: result.error.details[0].message,
            from: "validate"
          });
        } else {
          try {
            Users.findByIdAndUpdate(
              { _id: mongoose.Types.ObjectId(data._id) },
              {
                first_name: data.first_name,
                last_name: data.last_name,
                company_name: data.company_name,
                city: data.city,
                zipcode: data.zipcode,
                phone_number: data.phone_number,
                country: data.country,
                bussiness_type: data.bussiness_type,
                industry: data.industry,
            //    agree_newsletter: data.agree_newsletter,
                profile_image : data.profile_image
              },
              async function(err, response) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                    from: "err"
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "You have successfully updated your profile.",
                    data: data
                  });
                }
              }
            );
          } catch (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
              from: "catch"
            });
          }
        }
      });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  } else {
    res.send({ status: 0, message: "Invalid request." });
  }
});

router.post("/updateRecoveryEmail", async (req, res) => {
  if (req.body.token) {
    const token = req.body.token;
    const data = req.body.data;
    payload = jwt.verify(token, jwtKey);

    try {
      Validate.updateRecoveryEmail(data, result => {
        if (result.error) {
          res.send({
            status: 0,
            message: result.error.details[0].message,
            from: "validate"
          });
        } else {
          try {
            Users.findByIdAndUpdate(
              { _id: mongoose.Types.ObjectId(data._id) },
              {
                recovery_email: data.recovery_email
              },
              async function(err, response) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                    from: "err"
                  });
                } else {
                  res.send({
                    status: 1,
                    message:
                      "You have successfully updated your recovery email",
                    data: data
                  });
                }
              }
            );
          } catch (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
              from: "catch"
            });
          }
        }
      });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  } else {
    res.send({ status: 0, message: "Invalid request." });
  }
});

/* end reset password */

router.post("/verfiyUserWithId", async (req, res) => {
  try {
    Users.findOne({ _id: req.body.id }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (log) {
          res.send({
            status: 1,
            message: "Fetched user details successfully."
            // data: log
          });
        } else {
          res.send({
            status: 0,
            message: "Details Unvailable."
          });
        }
      }
    }).sort({ created_on: -1 });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/UpdatePassword", async (req, res) => {
  //user login check
  try {
    Admin.findOne(
      {
        $or: [
          { username: req.body.data.username },
          { email: req.body.data.username }
        ]
      },
      function(err, log) {
        if (err) {
          res.status(400).send("Oops! Something went wrong,please try later!");
        } else if (!log) {
          res.send({
            status: 1,
            type: "error",
            message:
              "There is no existing admin with the details that you’ve provided."
          });
        } else {
          const salt = bcrypt.genSaltSync(10);
          bcryptPassword = bcrypt.hashSync(req.body.data.password, salt);
          if (
            req.body.data.password == req.body.data.repassword &&
            log._id == req.body.data._id
          ) {
            var update_value = {
              password: bcryptPassword
            };
            var condition = { _id: log._id };
            Admin.findByIdAndUpdate(condition, update_value, async function(
              err,
              log
            ) {
              if (err) {
                res.send({
                  status: 0,
                  type: "error",
                  message: "Oops! " + err.name + ": " + err.message
                });
              } else {
                res.send({
                  status: 1,
                  message: "Password changed successfully.",
                  type: "sucess"
                });
              }
            });
          }
        }
      }
    );
  } catch (ex) {
    res.status(400).send("Oops! Something went wrong please try again later");
  }
});

/* upload profile image */

router.post("/updateUser", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        if (req.body.data.profile_image) {
          profiledata = req.body.data.profile_image;
          var update_value = {
            first_name: req.body.data.first_name,
            last_name: req.body.data.last_name,
            user_name: req.body.data.user_name,
            profile_image: profiledata,
            usertype: req.body.data.usertype,
            uniq_id: req.body.data.uniq_id
          };
        } else {
          var update_value = {
            first_name: req.body.data.first_name,
            last_name: req.body.data.last_name,
            user_name: req.body.data.user_name,
            usertype: req.body.data.usertype,
            uniq_id: req.body.data.uniq_id
          };
        }
        var condition = {
          _id: req.body.data._id
        };

        Users.findByIdAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "You have successfully uploaded your profile image.",
              data: {
                user_data: log
              }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/uploadData", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    res.send({
      status: 1,
      message: "Valid"
    });
  }
});

/* Search Functionality */
router.post("/search", async (req, res) => {
  searchSlug = req.body.slug;
  if (searchSlug == "") {
    res.send({
      status: 0,
      message: "Sorry! no search results found.",
      from: "empty slug"
    });
  } else {
    try {
      async.parallel(
        {
          products: getProductsSearch,
          news: getNewsSearch,
          video: getVideoSearch,
          training : getTrainingSearch,
          articles : getArticlesSearch,
          career : getCareerSearch,
          faq : getFaqSearch,
          pages : getPagesSearch,
        },
        function(error, results) {
          if (error) {
            res.send({
              status: 0,
              message: "Sorry!| no search results found.",
              from: error
            });
          } else {
            res.send({
              status: 1,
              search: results
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Sorry!!! no search results found.",
        from: "catch"
      });
    }
  }
});


function getPagesSearch(callback) {
  console.log('getPagesSearch', searchSlug);
  try {
    Pages.find(

      {

        $or: [
          { title: { $regex:  new RegExp(searchSlug, "i") } },
          { content: { $regex:  new RegExp(searchSlug, "i") } },
        //  { $text: { $search: "\\" + searchSlug + "\\" }}
        ],
         
    },
     // { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
      { title: 1, slug: 1, content: 1 , page_status: 1 }
    ).exec(callback);
  } catch (err) {
    callback({
      status: 0,
      error : err,
      message: "Sorry! no search results found."
    });
  }
}

function getProductsSearch(callback) {
  try {
  //   Products.find(
  // //    { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
  // { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
  //     { product_name: 1, url: 1, slug: 1, short_description: 1 }
  //   ).exec(callback);


  Products.find(

    {

      $or: [
        { product_name: { $regex:  new RegExp(searchSlug, "i") } },
        { description: { $regex:  new RegExp(searchSlug, "i") } },
        { product_tag: { $regex:  new RegExp(searchSlug, "i") } },
      ],
      page_status: 1
  },
   // { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
    { product_name: 1, url: 1, slug: 1 , short_description: 1   }
  ).exec(callback);

  } catch (err) {
    callback({
      status: 0,
      error : err,
      message: "Sorry! "+searchSlug +" no search results found."
    });
  }
}

function getNewsSearch(callback) {
  try {
    News.find(
      { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
      { title: 1, slug: 1, description: 1 }
    ).exec(callback);
  } catch (err) {
    callback({
      status: 0,
      message: "Sorry! no search results found."
    });
  }
}

function getVideoSearch(callback) {
  try {
    VideoTraining.find(
      { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
      { video_title: 1, video_url: 1 }
    ).exec(callback);
  } catch (err) {
    callback({
      status: 0,
      message: "Sorry! no search results found."
    });
  }
}

function getTrainingSearch(callback) {
  try {
    LiveTraining.find(
      { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
      { title: 1, slug: 1, description: 1 }
    ).exec(callback);
  } catch (err) {
    callback({
      status: 0,
      message: "Sorry! no search results found."
    });
  }
}

function getArticlesSearch(callback) {
  try {
    Articles.find(
      { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
      { title: 1, slug: 1, description: 1 }
    ).exec(callback);
  } catch (err) {
    callback({
      status: 0,
      message: "Sorry! no search results found."
    });
  }
}

function getCareerSearch(callback) {
  try {
    Careers.find(
      { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
      { job_name: 1, job_type: 1, content: 1  , slug: 1}
    ).exec(callback);
  } catch (err) {
    callback({
      status: 0,
      message: "Sorry! no search results found."
    });
  }
}

function getFaqSearch(callback) {
  try {
    Faq.find(

      {

        $or: [
          { title: { $regex:  new RegExp(searchSlug, "i") } },
          { description: { $regex:  new RegExp(searchSlug, "i") } },
        //  { $text: { $search: "\\" + searchSlug + "\\" }}
        ],
      },
      // { $text: { $search: "\\" + searchSlug + "\\" }, page_status: 1 },
      { title: 1, slug: 1, description: 1 }
    ).exec(callback);
  } catch (err) {
    callback({
      status: 0,
      message: "Sorry! no search results found."
    });
  }
}






/* Get all languages*/
router.post("/getAlllanguage", async (req, res) => {
  try {
    Language.find({}, async function(err, response) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (response.length > 0) {
          res.send({
            status: 1,
            message: "Fetched details successfully.",
            language: response
          });
        } else {
          res.send({
            langDetails: response,
            status: 0,
            message: "No details available."
          });
        }
      }
    }).sort({
      _id: 1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/* Get All Ticket Support  */
router.post("/getTicketSupport", async (req, res) => {
  try {
    let email = req.body.data.email;
    let pagenumber = req.body.data.page;
    var zendesk = new Zendesk({
      url: zendeskUrl, // https://example.zendesk.com
      email: zendeskEmail, // me@example.com
      token: zendeskToken // hfkUny3vgHCcV3UfuqMFZWDrLKms4z3W2f6ftjPT
    });

    let Query = "query=type:user email:" + email;

    zendesk.search
      .list(Query)
      .then(function(result) {
        if (result.length > 0) {
        //  console.log("user details", result);
          let Query2 = "query=type:ticket requester_id:" + result[0].id + "&per_page=10&page="+ pagenumber;

          console.log('Query2' ,Query2);
          zendesk.search.list(Query2).then(function(result) {
            res.send({
              status: 1,
              message: "Sucess ",
              Data: result
            });
          });
        } else {
          res.send({
            status: 1,
            message: "No Data Found",
            Data: []
          });
        }
      })
      .catch(function(err) {
        res.send({
          status: 0,
          message: "No Data Found"
        });
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});



/* Get All Ticket Count  */
router.post("/getTicketCount", async (req, res) => {
  try {
    let email = req.body.data.email;
    let pagenumber = req.body.data.page;
    var zendesk = new Zendesk({
      url: zendeskUrl, // https://example.zendesk.com
      email: zendeskEmail, // me@example.com
      token: zendeskToken // hfkUny3vgHCcV3UfuqMFZWDrLKms4z3W2f6ftjPT
    });

    let Query = "query=type:user email:" + email;

    zendesk.search
      .list(Query)
      .then(function(result) {
        if (result.length > 0) {
        //  console.log("user details", result);
          let Query2 = "query=type:ticket requester_id:" + result[0].id;

          console.log('Query2' ,Query2);
          zendesk.search.list(Query2).then(function(result) {
            res.send({
              status: 1,
              message: "Sucess ",
              Data: result
            });
          });
        } else {
          res.send({
            status: 1,
            message: "No Data Found",
            Data: []
          });
        }
      })
      .catch(function(err) {
        res.send({
          status: 0,
          message: "No Data Found"
        });
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});



router.post("/updateTicket", async (req, res) => {
  try {
    RquestData = req.body.data;

    async.waterfall([getUserExitorNot, createTicket], function(error, results) {
      if (error) {
        res.send(error);
      } else {
        res.send({
          status: 1,
          message: "New Ticket Created",
          results: results
        });
      }
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

function getUserExitorNot(callback) {
  const results = {};
  results.zendesk = {
    url: zendeskUrl, // https://example.zendesk.com
    email: zendeskEmail, // me@example.com
    token: zendeskToken // hfkUny3vgHCcV3UfuqMFZWDrLKms4z3W2f6ftjPT
  };
  try {
    const { email, first_name, last_name } = RquestData.users;
    //  console.log('RquestData' ,RquestData);
    var zendesk = new Zendesk(results.zendesk);
    let Query = "query=type:user email:" + email;
    // console.log('getUserExitorNot' , Query);
    zendesk.search
      .list(Query)
      .then(function(result) {
        if (result.length > 0) {
          //  console.log('result' ,result);
          //  console.log('result' ,result ,zendesk);
          results.User = result;
          results.ZenUserid = result[0].id;
          callback(null, results);
        } else {
          zendesk.users
            .create({
              name: first_name + " " + last_name,
              email: email,
              result_type: "user",
              role: "end-user"
            })
            .then(function(result) {
              zendesk.search
                .list(Query)
                .then(function(result) {
                  if (result.length > 0) {
                    results.User = result;
                    results.ZenUserid = result[0].id;
                    callback(null, results);
                  }
                })
                .catch(function(err) {
                  callback(
                    {
                      status: 0,
                      message: "Sorry no Details found on zendesk",
                      from: "getUserExitorNot Catch"
                    },
                    null
                  );
                });

              // if(result.length > 0){

              //    results.User = result;
              //    results.ZenUserid = result.id;
              //    callback(null, results);
              //   } else {
              //     console.log('create' ,first_name + ' ' + last_name, email);
              //     callback({
              //       status: 0,
              //       message: "Sorry no Details found on zendesk" ,
              //       from: 'getUserExitorNot else'
              //   }, null);
              //   }
            })
            .catch(function(err) {
              callback(
                {
                  status: 0,
                  message: "Sorry no Details found on zendesk",
                  from: "getUserExitorNot Catch"
                },
                null
              );
            });

          //   callback({
          //     status: 0,
          //     message: "Sorry no Details found on zendesk" ,
          //     from: 'getUserExitorNot'
          // }, null);
        }
      })
      .catch(function(err) {
        callback(
          {
            status: 0,
            message: err,
            from: "getUserExitorNot"
          },
          null
        );
      });
  } catch (e) {
    callback(
      {
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
        from: "getUserExitorNot"
      },
      null
    );
  }
}

function createTicket(results, callback) {
  try {
    const { email, first_name, last_name } = RquestData.users;
    const { subject, description, profile_image, token } = RquestData;
    var zendesk = new Zendesk(results.zendesk);
    let Query = "query=type:user email:" + email;
    console.log(
      "createTicket profile_image",
      email,
      first_name,
      last_name,
      profile_image
    );

    zendesk.tickets
      .create({
        subject: subject,
        comment: {
          body: description,
          uploads: token
          //   uploads: profile_image
        },
        status: "new",
        requester_id: results.ZenUserid,
        priority: "normal",
        via: {
          channel: "api",
          source: {
            to: {
              name: "CIMON Support",
              address: results.zendesk.email
            },
            from: {
              address: email,
              name: first_name + " " + last_name
            },
            rel: ""
          }
        }
      })
      .then(function(result) {
        console.log("sucess createTicket", result);
        results.ticket = result;
        callback(null, results);
      })
      .catch(function(err) {
        callback(
          {
            status: 0,
            message: "something went wrong on createTicket",
            from: "createTicket catch"
          },
          results
        );
      });
  } catch (e) {
    callback(
      {
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
        from: "createTicket"
      },
      null
    );
  }
}

/* Get  Ticket Support by id  */
router.post("/getTicketId", async (req, res) => {
  try {
    let TICKET_ID = req.body.id;
    var zendesk = new Zendesk({
      url: zendeskUrl, // https://example.zendesk.com
      email: zendeskEmail, // me@example.com
      token: zendeskToken // hfkUny3vgHCcV3UfuqMFZWDrLKms4z3W2f6ftjPT
    });

    //console.log('zendesk',zendesk);
    zendesk.tickets
      .show(TICKET_ID)
      .then(function(result) {
        //  console.log('TICKET_ID',result);
        res.send({
          status: 1,
          message: "Sucess ",
          data: result
        });
      })
      .catch(function(err) {
        res.send({
          status: 0,
          message: "No Data Found",
          data: []
        });
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/* Get  Ticket Support by id  */
router.post("/editTicket", async (req, res) => {
  try {
    let TICKET_ID = req.body.data.id;
    let token = req.body.data.token ? req.body.data.token : "";

    console.log("token", token, req.body.data);
    var zendesk = new Zendesk({
      url: zendeskUrl, // https://example.zendesk.com
      email: zendeskEmail, // me@example.com
      token: zendeskToken // hfkUny3vgHCcV3UfuqMFZWDrLKms4z3W2f6ftjPT
    });

    zendesk.tickets
      .update(TICKET_ID, {
        subject: req.body.data.subject,
        description: req.body.data.description,
        comment: {
          body: req.body.data.description,
          author_id: req.body.data.submitter_id,
          uploads: token
          //   description: req.body.data.description ,
          //   uploads: profile_image
        }
        // assignee_id : 409304787372
        //status : "new",
      })
      .then(function(result) {
        console.log(
          "result TICKET_ID",
          result.ticket.ticket_form_id,
          req.body.data.description
        );
        res.send({
          status: 1,
          message: "Ticket updated Sucessfully ",
          data: result
        });
      })
      .catch(function(err) {
        res.send({
          status: 0,
          message: err,
          data: []
        });
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/* Get Ticket audit by Ticket id*/

router.post("/getTicketAudit", async (req, res) => {
  try {
    // console.log('ZendeskAuth', ZendeskAuth);
    let Ticket_Audit_Id = req.body.id;
    let pagenumber = req.body.pagenumber;
    var zendesk = new Zendesk({
      url: zendeskUrl, // https://example.zendesk.com
      email: zendeskEmail, // me@example.com
      token: zendeskToken // hfkUny3vgHCcV3UfuqMFZWDrLKms4z3W2f6ftjPT
    });

    const options = {
      url:
        "https://cimon.zendesk.com/api/v2/tickets/" +
        Ticket_Audit_Id +
        "/audits.json?per_page=10&page="+pagenumber,
      method: "GET",
      headers: {
        //'Content-Type': 'application/json',
        Authorization: ZendeskAuth
      }
    };

    curl.request(options, function(err, data) {
      if (err) {
        res.send({
          status: 0,
          message: "error" + err,
          data: []
        });
      } else {
        res.send({
          status: 1,
          message: "Sucess",
          data: JSON.parse(data)
        });
      }
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/* Get User by Authorised id  */
router.post("/getTicketUserDetails", async (req, res) => {
  try {
    let id = req.body.id;
    var zendesk = new Zendesk({
      url: zendeskUrl, // https://example.zendesk.com
      email: zendeskEmail, // me@example.com
      token: zendeskToken // hfkUny3vgHCcV3UfuqMFZWDrLKms4z3W2f6ftjPT
    });

    let Query = "query=type:user id:" + id;
    let USER_ID = id;
    zendesk.users
      .show(USER_ID)
      .then(function(result) {
        //    console.log('result123', result ,Query);
        if (result) {
          res.send({
            status: 1,
            message: "Sucess ",
            data: result
          });
        } else {
          res.send({
            status: 0,
            message: "No Data Found",
            data: []
          });
        }
      })
      .catch(function(err) {
        res.send({
          status: 0,
          message: "No Data Found"
        });
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/* uploadImageAttachment */

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, url);
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  }
});
var upload = multer({
  storage: storage
}).array("file");

router.post("/uploadImageAttachment", async (req, res) => {
  try {
    await upload(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        res.send({
          status: 0,
          message: "Oops! something went wrong with uploading profile image."
        });
      } else if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        try {
          var zendesk = require("node-zendesk");

          var client = zendesk.createClient({
            username: "christian.heilman@cimoninc.com",
            token: "p9A0vLNG0Hb3Vk16RWBR9QoRX5WOxYfz9cEI91ek",
            remoteUri: "https://cimon.zendesk.com/api/v2"
          });

          console.log(
            "req.files",
            req.files[0].destination + req.files[0].filename
          );

          client.attachments.upload(
            path.resolve(req.files[0].destination + req.files[0].filename),
            {
              filename: req.files[0].filename
            },
            function(err, req, result) {
              // if (err) {
              //   console.log(err);
              //   return;
              // }
              if (result) {
                res.send({
                  status: 1,
                  message: "sucess",
                  data: result
                });
              } else {
                res.send({
                  status: 1,
                  message: "error",
                  data: err
                });
              }
            }
          );

          //    const filePathData  =  './uploads/2020_9_1603741571125_Auto_mobile_icon.png'
          // const filePathData2  = fs.readFileSync(req.files[0]);

          //     console.log(' req.filePathData2',filePathData2);

          // //    console.log(' req.file', req.file.filename);
          //     var zendesk = new Zendesk({
          //       url: 'https://cimon.zendesk.com', // https://example.zendesk.com
          //       email: 'christian.heilman@cimoninc.com', // me@example.com
          //       token: 'p9A0vLNG0Hb3Vk16RWBR9QoRX5WOxYfz9cEI91ek' // hfkUny3vgHCcV3UfuqMFZWDrLKms4z3W2f6ftjPT
          //     });

          //     const options = {
          //       url: 'https://cimon.zendesk.com/api/v2/uploads.json?filename='+req.files[0].filename+'&token=p9A0vLNG0Hb3Vk16RWBR9QoRX5WOxYfz9cEI91ek&binary=false',
          //      method: 'POST',
          //      body : filePathData2,
          //       headers: {
          //         'Content-Type' : 'application/binary',
          //         //'Content-Type': 'application/json',
          //         'Authorization' :  ZendeskAuth
          //       }
          //   };

          //     curl.request(options, function (err, data) {
          //       if(err){
          //         res.send({
          //           status: 0,
          //           message: 'error 123' + err,
          //           data : []
          //         });
          //       } else {
          //         res.send({
          //           status: 1,
          //           message: 'Sucess',
          //           data : JSON.parse(data)
          //         });
          //       }

          //     });
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
      }
    });
  } catch (err) {
    res.send({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message
    });
  }
});

/* Get Attachments by  id*/

router.post("/getAttachementDetails", async (req, res) => {
  try {
    let Attachments_Id = req.body.data[0].id;

    const options = {
      url: req.body.data[0].url,
      method: "GET",
      headers: {
        //'Content-Type': 'application/json',
        Authorization: ZendeskAuth
      }
    };

    curl.request(options, function(err, data) {
      if (err) {
        console.log("ZendeskAuth err", err);
        res.send({
          status: 0,
          message: "error" + err,
          data: []
        });
      } else {
        console.log("ZendeskAuth err", data);
        res.send({
          status: 1,
          message: "Sucess",
          data: JSON.parse(data)
        });
      }
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});


/* update user Password */

router.post("/updatepasswordId", async (req, res) => {
  //user login check
  try {
    Users.findOne(
      {
        _id : req.body.data._id
      },
      function(err, log) {
        if (err) {
          res.send({
            status: 0,
            type: "error",
            message:
              "Oops! Something went wrong,please try later!"
          });
          
        } else if (!log) {
          res.send({
            status: 0,
            type: "error",
            message:
              "There is no existing User with the details that you’ve provided."
          });
        } else {
          const salt = bcrypt.genSaltSync(10);
          bcryptPassword = bcrypt.hashSync(req.body.data.password, salt);
         

          if (
            req.body.data.password == req.body.data.repassword 
          ) {


            var update_value = {
              password: bcryptPassword
            };
            var condition = { _id: log._id };
            Users.findByIdAndUpdate(condition, update_value, async function(
              err,
              log
            ) {
              if (err) {
                res.send({
                  status: 0,
                  type: "error",
                  message: "Oops! " + err.name + ": " + err.message
                });
              } else {
                res.send({
                  status: 1,
                  message: "Password changed successfully.",
                  type: "sucess"
                });
              }
            });

          } else {
            res.send({
              status: 0,
              type: "error",
              message:
                "The passwords you’ve entered does not match."
            });
          }
          
        }
      }
    );
  } catch (ex) {
    res.send({
      status: 0,
      type: "error",
      message:
        "Oops! Something went wrong,please try later!" + ex
    });
  }
});

/* End update user Password */



/* update user Password */

router.post("/confirmPasswordById", async (req, res) => {
  //user login check
  try {
    Users.findOne(
      {
        _id : req.body.data._id
      },
      function(err, log) {
        if (err) {
          res.send({
            status: 0,
            type: "error",
            message:
              "Oops! Something went wrong,please try later!"
          });
          
        } else if (!log) {
          res.send({
            status: 0,
            type: "error",
            message:
              "There is no existing User with the details that you’ve provided."
          });
        } else {
         
          const validatepassword = bcrypt.compareSync(
            req.body.data.password,
            log.password
          );
          if(validatepassword){
            res.send({
              status: 1,
              type: "sucess",
              message:
                "Password Confirmed. Redirecting... "
            });
          } else {
            res.send({
              status: 0,
              type: "error",
              message:
                "You have entered an invalid password."
            });
          }
          
        }
      }
    );
  } catch (ex) {
    res.send({
      status: 0,
      type: "error",
      message:
        "Oops! Something went wrong,please try later!" + ex
    });
  }
});

/* End update user Password */



router.post("/getUserSidebar", async (req, res) => {
  try {
     
    ProfileTab.findOne(
        {  },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            }); 
          } else {
  
            if(log){
              
              res.send({
                status: 1,
                message: "Fetched admin details successfully.",
                data: log
              });
  
            }else{
  
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
  
            }
          }
        }
      ).sort({ created_on: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
});



/* create Feedback*/

router.post("/createFeedback", async (req, res) => {
  const token = req.body.token;
    console.log('enter this section');
    request = req.body.data
    async.waterfall([getFeedbackEmail
      
      , getCreateFeedback
    
    ], function(error, results) {
      if (error) {
        res.send(error);
      } else {
        res.send({
          status: 1,
          message: "You have successfully submitted your feedback.",
          results: results
        });
      }
    });
    // try {




    //   Settings.find({
    //     slug : 'feedbackEmail'
    //   }, async function(error, log) {
    //     if (log != null) {
    //       console.log('else option')
    //       res.send({
    //         status: 2,
    //         message:
    //           "Feedback Email Issue"
    //       });
    //     } else {

    //       console.log('log--->', log);
          
    //     const newFeedBack = new FeedBack({
    //       feedback: req.body.data.feedback,
    //       description: req.body.data.description,
    //       review: req.body.data.review,
    //       users: req.body.data.users,
    //     });

    
     
    //       try {
    //         await newFeedBack
    //           .save()
    //           .then(() => {
              
    //             sendFeedbackMail(newFeedBack, result => {
    //               if (result == "success") {
    //                 res.send({
    //                   status: 1,
    //                   message:
    //                     "You have successfully submitted your feedback."
    //                 });
    //               } else if (result == "failed") {
    //                 res.send({
    //                   status: 0,
    //                   type: "error",
    //                   message: "Mail Delivery failed"
    //                 });
    //               }
    //             });
    //           })
    //           .catch(err => {
    //             res.send({
    //               status: 0,
    //               type: "error catch",
    //               message: "Oops! " + err.name + ": " + err.message
    //             });
    //           });
    //       } catch (ex) {
    //         res.send({
    //           status: 0,
    //           type: "error catch",
    //           message: "Oops! Something went wrong with sending  mail...!"
    //         });
    //       }


    //     }
    //   });

    // } catch (e) {
    //   res.send({
    //     status: 0,
    //     message: "Oops! " + e.name + ": " + e.message,
    //   });
    // }
});


function getFeedbackEmail(callback) {
  const results = {};
 
  try {
       Settings.find({
          slug : 'feedbackEmail'
        }, async function(error, log) {
          if (log != null) {
            results
            results.feedbackEmail = log[0].value;
            callback(null, results);
          } else {

            callback(
              {
                status: 0,
                message: "Sorry no Details found",
                from: "feedbackEmail Catch"
              },
              null
            );
          
            }
          });
  } catch (e) {
    callback(
      {
        status: 0,
        message: "Sorry no Details found",
        from: "feedbackEmail Catch"
      },
      null
    );
  }
}


/* sent mail and feeback */

function getCreateFeedback(results, callback) {
  
    const newFeedBack = new FeedBack({
          feedback: request.feedback,
          description: request.description,
          review: request.review,
          users: request.users,
        }); 

    
     
          try {
             newFeedBack
              .save()
              .then(() => {
                results.feedback = newFeedBack;
                sendFeedbackMail(results, result => {
                  if (result == "success") {
                    callback(null, results);
                  } else if (result == "failed") {
                   
                    callback(
                      {
                        status: 0,
                        message: "Mail Delivery failed",
                        from: "getCreateFeedback failed"
                      },
                      null
                    );


                  }
                });
              })
              .catch(err => {
                callback(
                  {
                    status: 0,
                    message: "Mail Delivery failed",
                    from: "getCreateFeedback Catch"
                  },
                  null
                );
              });
          } catch (ex) {
            callback(
              {
                status: 0,
                message: "Sorry no Details found",
                from: "getCreateFeedback Catch"
              },
              null
            );
          }

 
 
}


/* Send Feedback mail */

function sendFeedbackMail(results, callback) {
  let header = fs
    .readFileSync(path.join(__dirname + "/emails/header.html"), {
      encootding: "utf-8"
    })
    .toString();
  var view = {
    siteurl: config.siteurl,
    imageUrl: config.imageUrl
  };
  var createHeader = mustache.render(header, view);

  let footer = fs
    .readFileSync(path.join(__dirname + "/emails/footer.html"), {
      encootding: "utf-8"
    })
    .toString();
  var view = {
    imageUrl: config.imageUrl
  };
  var createFooter = mustache.render(footer, view);

  let template = fs
    .readFileSync(path.join(__dirname + "/emails/feedback.html"), {
      encootding: "utf-8"
    })
    .toString();

  //const verifyUrl = config.siteurl + "verify/" + results.verify_token;

  var view = {
    first_name: results.first_name,
    last_name: results.last_name,
    Viewurl: siteurl + 'cimon-gate/admin/feedback#' + results._id
  };
  var createTemplate = mustache.render(template, view);
  var htmlContent = createHeader + createTemplate + createFooter;

  sgMail.setApiKey(sendGridKey);
  const message = {
    to: results.feedbackEmail,
    from: { email: config.fromEmail, name: "CIMON" },
    subject: "Feedback",
    html: htmlContent
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        callback("success");
      })
      .catch(error => {
        callback("failed");
      });
  } catch (err) {
    callback("failed");
  }
}

/*Get all FeedBack  Menu*/
router.post("/getAllFeedBackMenu", async (req, res) => {
  try {
    
    Menus.find({slug : 'feedback' }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (log.length > 0) {
          res.send({
            status: 1,
            message: "Fetched FeedBack Successfully.",
            data: log
          });
        } else {
          res.send({
            status: 0,
            message: "No FeedBack available."
          });
        }
      }
    }).sort({
      _id: -1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});
































/* Product sendEmail */

router.post("/sendEmail", async (req, res) => {

  
  sendProductMail(req.body.data, result => {
   
    if (result.status == "success") {
      res.send({
        status: 1,
        message: "Your message has been sent successfully."
      });
    
    } else {
      res.send({
        status: 0,
        message: "Oops! Something went wrong with sending Email."
      });
    }
  });
});

function sendProductMail(results, callback) {

  
  let header = fs
    .readFileSync(path.join(__dirname + "/emails/header.html"), {
      encootding: "utf-8"
    })
    .toString();

  var view = {
    siteurl: config.siteurl,
    imageUrl: config.imageUrl
  };
  var createHeader = mustache.render(header, view);

  let footer = fs
    .readFileSync(path.join(__dirname + "/emails/footer.html"), {
      encootding: "utf-8"
    })
    .toString();

  var view = {
    imageUrl: config.imageUrl
  };
  var createFooter = mustache.render(footer, view);



  var view = {
    username: results.email,
    Url: results.url,
    message : results.message,
    product : results.product
  };

  let template = fs
  .readFileSync(path.join(__dirname + "/emails/emailShare.html"), {
    encootding: "utf-8"
  })
  .toString();

  var createTemplate = mustache.render(template, view);
  var htmlContent = createHeader + createTemplate + createFooter;
  sgMail.setApiKey(sendGridKey);
  const message = {
    to: results.email,
    from: { email: config.fromEmail, name: "CIMON" },
    subject: "CIMON "+ results.product +" Email Share",
    html: htmlContent
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        callback({
          status: "success",
        });
      })
      .catch(error => {
        const { message, code, response } = error;
        console.log('sendProductMail error',error);
        callback({
          status: "failed"
        });
      });
  } catch (err) {
    callback({
      status: "failed"
    });
  }
}









module.exports = router;
