/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");
const mongoose = require("mongoose");
/* Include schema */
const News = require("../models/news");
const DistributorResources = require("../models/integrator_resources");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();


/* category list */


router.post("/getAllCategoryOptions", async (req, res) => {
  const token = req.body.token;
  try {
   
    DistributorResources.find({},
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Distributor Successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Distributor available."
            });
          }

        }
      }
    ).sort({
      title: -1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

  
/*  Get all types of archives  */
router.post("/getArchives", async (req, res) => {
  try {
    async.parallel(
      {
        latestArchives: getLatestArchives,
        recentArchives: getRecentlyPublishedArchives,
        topArchives: getTopArchives
      },
      function(error, results) {
        if (error) {
          res.status(200).send(error);
          return;
        }
        res.send(results);
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/*  Get the Top archives  */
function getTopArchives(callback) {
  DistributorResources.find({ is_top_news: "1", page_status: "1" })
    .sort({ _id: -1 })
    .exec(callback);
}

/*  Get the Latest archives  */
function getLatestArchives(callback) {
  DistributorResources.find({ page_status: "1" })
    .sort({ _id: 1 })
    .limit(1)
    .exec(callback);
}

/*  Get the Recently Published archives  */
function getRecentlyPublishedArchives(callback) {
  DistributorResources.find({ page_status: "1" })
    .sort({ _id: -1 })
    .limit(10)
    .exec(callback);
}

/*  Get Single archive Details  */

router.post("/getAllArchives", async (req, res) => {
  try {
    async.parallel(
      {
        Software: getSoftwareArchives,
        Manual: getMannualArchives,
        Document: getDocumentArchives
      },
      function(error, results) {
        if (error) {
          res.status(500).send(error);
          return;
        }
        res.send(results);
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/*  Get the Top News  */
function getSoftwareArchives(callback) {
  DistributorResources.find({ related_cat: "software", page_status: "1" })
    .sort({ _id: -1 })
    .exec(callback);
}

/*  Get the Latest News  */
function getMannualArchives(callback) {
  DistributorResources.find({ related_cat: "manual", page_status: "1" })
    .sort({ _id: -1 })
    .exec(callback);
}

/*  Get the Recently Published News  */
function getDocumentArchives(callback) {
  DistributorResources.find({
    $and: [
      { related_cat: { $ne: "software" } },
      { related_cat: { $ne: "manual" } }
    ]
  })
    .sort({ _id: -1 })
    .exec(callback);
}

/*Search Archive */

router.post("/searchArchive", async (req, res) => {
  let query = "";
  let searchtext = req.body.data.searchkey;
  let searchproduct = req.body.data.product;
  let searchcat = req.body.data.category;
  let date1 = req.body.data.filedate;

  const page = req.body.page;
  const limit = 10;
  const skip = limit * (page - 1);
  var totalPages = 0;
  // let limit = parseInt(req.body.page);

  //console.log(444,req.body.data);

  try {
    if (page < 0 || page === 0) {
      response = {
        status: 0,
        message: "Invalid page number, should start with 1"
      };
      return res.json(response);
    }

    const andCondition = [{ _id: { $ne: -1 } }];
    if (searchproduct) {
      andCondition.push({ related_products: searchproduct });
    }
    if (searchcat) {
      andCondition.push({ related_cat: searchcat });
    }
    if (searchtext) {
      andCondition.push({ title: new RegExp(searchtext, "i") });
      /* andCondition.push({
          $or: [{
            title: new RegExp(searchtext, "i")
        }, {
            version: new RegExp(searchtext, "i")
        }]
         } ) */
    }
    if (date1) {
      console.log("date1)", moment(date1).format("YYYY-MM-DD"));
      andCondition.push({ releasedate: moment(date1).format("YYYY-MM-DD") });
    }

    andCondition.push({ page_status: "1" });
   // console.log(434, andCondition);
    // let match = { $match: { "_id": { $ne: -1 } }};
    // if(andCondition.length > 0) {
    //    match =  { $match: { $and: andCondition }}
    //}

    DistributorResources.aggregate(
      [
        { $match: { $and: andCondition } },
        { $limit: skip + limit },
        { $skip: skip },
        { $sort: { data_order: 1 } }
      ],
      function(err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (response.length > 0) {
            //    console.log(55,response);
            res.send({
              status: 1,
              message: "Fetched details successfully.",
              archivedata: response,
              CurrentPage: page,
              totalPages: totalPages
            });
          } else {
            res.send({
              status: 0,
              message: "Sorry! No records found."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});
module.exports = router;

/* */

/* Total archive count */

router.post("/getArchiveCount", async (req, res) => {
  try {
    let query = "";
    let searchtext = req.body.data.searchkey;
    let searchproduct = req.body.data.product;
    let searchcat = req.body.data.category;
    let date1 = req.body.data.filedate;

    const andCondition = [{ _id: { $ne: mongoose.Types.ObjectId(-1) } }];
    if (searchproduct) {
      andCondition.push({ related_products: searchproduct });
    }
    if (searchcat) {
      andCondition.push({ related_cat: searchcat });
    }
    if (searchtext) {
      andCondition.push({ title: new RegExp(searchtext, "i") });
      /* andCondition.push({
      $or: [{
        title: new RegExp(searchtext, "i")
    }, {
        version: new RegExp(searchtext, "i")
    }]
     } ) */
    }
    if (date1) {
      andCondition.push({ releasedate: moment(date1).format("YYYY-MM-DD") });
    }
    andCondition.push({ page_status: "1" });
    //console.log(434,andCondition);

    DistributorResources.countDocuments({ $and: andCondition }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else if (!log) {
        res.send({
          status: 1,
          message: "no data found",
          totalCount: 0
        });
      } else {
        res.send({
          status: 1,
          message: "success",
          totalCount: log
        });
      }
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/*  Get downloadfiles   */
router.post("/downloadfiles", async (req, res) => {
  const id = req.body.id;
  try {
    DistributorResources.findOne(
      { _id: id },
      { file_url: 1, uploadFiles: 1 },
      async function(err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (response) {
            res.send({
              status: 1,
              message: "Fetched Archives details successfully.",
              data: response
            });
          } else {
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/*date list*/
router.post("/getDateList", async (req, res) => {
  try {
    DistributorResources.find(
      { page_status: "1" },
      {
        releasedate: 1
      },
      async function(err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (response) {
            res.send({
              status: 1,
              message: "Fetched Archives details successfully.",
              data: response
            });
          } else {
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    ).sort({ _id: -1 });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});



/*  Get Single Archieve Details  */
router.post("/getsingleArchiveBySlug", async (req, res) => {
  const slug  = req.body.slug;
  try {
    DistributorResources.findOne(
      { slug: slug },
      async function (err, response) {
        if (err) {
          res.send({   
            status:0,         
            message: "Oops! " + err.name + ": " + err.message,
          }); 
        } else {
          if(response){
            res.send({ 
              status: 1,             
              message: "Fetched Archives details successfully.",
              data: response
            });
          }else{
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});



module.exports = router;
