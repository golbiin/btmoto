/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
/*Include schema */
const Resources = require("../models/resources");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
/* http request port set */
const router = express.Router();

/* Get all integrator and distributor Resources */ 
router.post("/getAllresources", async (req, res) => {
    const {token, pid, cid, s} = req.body;
    try {

        if(!token) {
            res.send({
                status: 0,
                message: "Oops! Invalid request",
            });
            return;
        }

        payload = jwt.verify(token, jwtKey);
        const andCondition = [ ];
        if(pid) {
            andCondition.push( {"product_id": pid});
        }
        if(cid) {
            andCondition.push( {"category_id": cid});
        }
        if(s) {
            andCondition.push( {"filename": s } )
        }
        let match = { $match: { "_id": { $ne: -1 } }};
        if(andCondition.length > 0) {
            match =  { $match: { $and: andCondition }}
        }
        Resources.aggregate([
            match
        ], function (err, response) {
            
            if (err) {
                res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                });
            } else {
                if (response.length > 0) {
                    res.send({
                        status: 1,
                        message: "Fetched details successfully.",
                        resourcesDetails: response
                    });
                } else {
                    res.send({
                        status: 0,
                        message: "Sorry! No records found."
                    });
                }
            }

        });
    } catch (e) {
        res.send({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
        });
    }

});


module.exports = router;