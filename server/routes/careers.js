/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");

/* Include schema */
const Careers = require("../models/careers");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();

/* List all Jobs */
// router.post("/getAlljobs", async (req, res) => {
//   try {
//     Careers.find(
//       {
//         $and: [{ page_status: { $ne: 0 } }, { page_status: { $ne: 2 } }]
//       },
//       async function(err, response) {
//         if (err) {
//           res.send({
//             status: 0,
//             message: "Oops! " + err.name + ": " + err.message
//           });
//         } else {
//           if (response) {
//             res.send({
//               status: 1,
//               message: "Fetched details successfully.",
//               jobDetails: response
//             });
//           } else {
//             res.send({
//               status: 0,
//               message: "No details available."
//             });
//           }
//         }
//       }
//     ).sort({ _id: -1 });
//   } catch (e) {
//     res.send({
//       status: 0,
//       message: "Oops! " + e.name + ": " + e.message
//     });
//   }
// });

router.post("/getAlljobs", async (req, res) => {
const page = parseInt(req.body.page);
  const PAGE_SIZE = 10;  
  const skip = (page - 1) * PAGE_SIZE; 
  var totalPages  = 0;
  const { filter } = req.body;
  let filterCondition = {page_status: "1"};  
  const search = filter.search;
  const date = filter.date;
  const relatedcat = filter.related_cat;
  if(relatedcat) {
    filterCondition.related_cat  =  relatedcat 
  } 
  if(date) {

   // console.log('current date',date);
   // console.log('moment date', moment(new Date(date)).format('YYYY-MM-DD h:mm:ss')   );
    let nextdate = new Date(date);
    nextdate.setDate(nextdate.getDate() + 1);
 //   console.log('nextdate date', nextdate  );
    filterCondition.post_date  =  {
      $gte: new Date(date), 
      $lte : nextdate, 
    
    //  $lt: nextdate
    } 
  } 
  if(search) {
    filterCondition.job_name = new RegExp(search, "i") ;
    // filterCondition.$text = { $search: search } 
  }

  console.log('filterCondition' , filterCondition);
  try {
    if(page < 0 || page === 0) {
      response = { status: 0, "message" : "Invalid page number, should start with 1"};
      return res.json(response)
    }
    /* Articles Count */
    Careers.count(filterCondition, async function(err,totalCount) {
      if(err) {
        response = {"status" : 0,"message" : "Error fetching data"}
      } else {
        totalPages = Math.ceil(totalCount / PAGE_SIZE);
      }
    });

    /* find all Articles */ 
    var query = Careers.find(filterCondition);
    query.skip(skip);
    query.limit(PAGE_SIZE);
    query.sort({ date: -1 });
    query.exec(async function (err, response) {
      if (err) {         
        response = {"status" : 0,"message" : "Error fetching data"} 
      }
      res.send({
        status: 1,
        CurrentPage: page,
        jobDetails: response,
        totalPages: totalPages
      });
    })
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
  
});














/* Get Job Description */
router.post("/getsinglejobs", async (req, res) => {
  const slug = req.body.slug;
  try {
    Careers.findOne(
      {
        slug: slug
      },
      async function(err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (response) {
            res.send({
              status: 1,
              message: "Fetched  details successfully.",
              singleDetails: response
            });
          } else {
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

/* Search Jobs */
router.post("/searchJobs", async (req, res) => {
  const job_key = req.body.data.job_key;
  const country = req.body.data.country;
  let condition = {};
  if (country) {
    condition = {
      $and: [
        { page_status: { $ne: 0 } },
        { page_status: { $ne: 2 } },
        {
          $and: [
            {
              $or: [
                {
                  job_name: new RegExp(job_key, "i")
                },
                {
                  job_type: new RegExp(job_key, "i")
                }
              ]
            },
            {
              location: country
            }
          ]
        },
        {
          location: country
        }
      ]
    };
  }
  try {
    Careers.find(condition, async function(err, response) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (response.length > 0) {
          res.send({
            status: 1,
            message: "Fetched  details successfully.",
            singleDetails: response
          });
        } else {
          res.send({
            status: 0,
            message: "No Results Found."
          });
        }
      }
    }).sort({ _id: -1 });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});


router.post("/getCareersCount", async (req, res) => {
  try {
    let query = "";
    let searchtext = req.body.data.searchkey;
    // let searchproduct = req.body.data.product;
    // let searchcat = req.body.data.category;
    let date1 = req.body.data.filedate;

    const andCondition = [{ _id: { $ne: mongoose.Types.ObjectId(-1) } }];
    // if (searchproduct) {
    //   andCondition.push({ job_type: searchproduct });
    // }
    // if (searchcat) {
    //   andCondition.push({ job_type: searchcat });
    // }
    if (searchtext) {
      andCondition.push({ job_name: new RegExp(searchtext, "i") });
    //    andCondition.push({
    //   $or: [{
    //     job_name: new RegExp(searchtext, "i")
    // }, {
    //     //version: new RegExp(searchtext, "i")
    // }]
    //  } ) 
     }
    if (date1) {
      andCondition.push({ post_date: moment(date1).format("YYYY-MM-DD") });
    }
    andCondition.push({ page_status: "1" });
    //console.log(434,andCondition);

    Careers.countDocuments({ $and: andCondition }, async function(err, log) {
      console.log(222,log);
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else if (!log) {
        res.send({
          status: 1,
          message: "no data found",
          totalCount: 0
        });
      } else {
        res.send({
          status: 1,
          message: "success",
          totalCount: log
        });
      }
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops!1222 " + e.name + ": " + e.message
    });
  }
});

/*Search Careers */

router.post("/searchCareers", async (req, res) => {
  let query = "";
  let searchtext = req.body.data.searchkey;
  //let searchproduct = req.body.data.product;
  //let searchcat = req.body.data.category;
  let date1 = req.body.data.filedate;

  const page = req.body.page;
  const limit = 2;
  const skip = limit * (page - 1);
  var totalPages = 0;
  // let limit = parseInt(req.body.page);

  //console.log(444,req.body.data);

  try {
    if (page < 0 || page === 0) {
      response = {
        status: 0,
        message: "Invalid page number, should start with 1"
      };
      return res.json(response);
    }

    const andCondition = [{ _id: { $ne: -1 } }];
    // if (searchproduct) {
    //   andCondition.push({ related_products: searchproduct });
    // }
    // if (searchcat) {
    //   andCondition.push({ job_type: searchcat });
    // }
    if (searchtext) {
      andCondition.push({ job_name: new RegExp(searchtext, "i") });
      //  andCondition.push({
      //     $or: [{
      //       job_name: new RegExp(searchtext, "i")
      //   }, {
      //      /// version: new RegExp(searchtext, "i")
      //   }]
      //    } )
    }
    if (date1) {
      console.log("date1)", moment(date1).format("YYYY-MM-DD"));
      andCondition.push({ post_date: moment(date1).format("YYYY-MM-DD") });
    }

    andCondition.push({ page_status: "1" });
    console.log(430, andCondition);
    // let match = { $match: { "_id": { $ne: -1 } }};
    // if(andCondition.length > 0) {
    //    match =  { $match: { $and: andCondition }}
    //}

    Careers.aggregate(
      [
        { $match: { $and: andCondition } },
        { $limit: skip + limit },
        { $skip: skip },
        { $sort: { date: -1 } }
      ],
      function(err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (response.length > 0) {
            //    console.log(55,response);
            res.send({
              status: 1,
              message: "Fetched details successfully.",
              archivedata: response,
              CurrentPage: page,
              totalPages: totalPages
            });
          } else {
            res.send({
              status: 0,
              message: "Sorry! No records found."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});


/*date list*/
router.post("/getDateList", async (req, res) => {
  try {
    Careers.find(
      { page_status: "1" },
      {
        post_date: 1
      },
      async function(err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (response) {
            res.send({
              status: 1,
              message: "Fetched Archives details successfully.",
              data: response
            });
          } else {
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    ).sort({ _id: -1 });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});



module.exports = router;
