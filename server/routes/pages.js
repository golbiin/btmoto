/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require('async');

/* Include schema */
const Pages = require("../models/pages");
const Distributor = require("../models/international_distributor");
const MediaFiles = require("../models/media_files");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
/* http request port set */
const router = express.Router();

/* Get Page Contents */
router.post("/getPageContent", async (req, res) => {
  const slug = req.body.slug;
  try {
    Pages.findOne(
        { slug : slug },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if(log){
              res.send({
                status: 1,
                message: "Fetched  Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      ).sort({ created_on: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
  }
});


/* Get Preview Contents */
router.post("/getPreviewContent", async (req, res) => {
  const id = req.body.slug;
  try {
    Pages.findOne(
        { _id : id },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if(log){
              res.send({
                status: 1,
                message: "Fetched  Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "Details Unavailable."
              });
            }
          }
        }
      ).sort({ created_on: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
  }
});

/* List all Logos */
router.post("/getcustomerLogos", async (req, res) => {
  try {
    MediaFiles.find(
       { 
         
        page_type : 'customers'
        
      
      },
      
          async function (err, response) {
              if (err) {
                  res.send({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message,
                  });
              } else {
                  if (response) {
                      res.send({
                          status: 1,
                          message: "Fetched details successfully.",
                          customerLogos: response
                      });
                  } else {
                      res.send({
                          status: 0,
                          message: "No details available."
                      });
                  }
              }
          }
      ).sort({
        order_number: 1
      });
  } catch (e) {
      res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
      });
  }

});


/* Get Page international distributor  */
router.post("/getIntDistLocations", async (req, res) => {
 console.log('skp');
  try {
    Distributor.find({  },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if(log){
              res.send({
                status: 1,
                message: "Fetched  Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      ).sort({ created_on: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
  }
});


module.exports = router;







