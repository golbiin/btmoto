/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require('async');

/* Include schema */
const Faq = require("../models/faq");
const Categories = require("../models/categories");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
/* http request port set */
const router = express.Router();
/*  Get all types of News  */


router.post("/getAllFaq", async (req, res) => {
  try {
    async.parallel({
        faqDetails: function (callback) {
          Categories.find({
            parent_id: 0
            }, {
              _id: 1,
              category_name: 1,
              slug: 1,
            },
            async function (err, categoryInfo) {
              if (err || !categoryInfo) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              }
              if (categoryInfo) {
                var faqarray = [];
                categoryInfo.forEach(function (category) {
                  Faq.find({
                      page_status: "1",
                      categories: {
                        $in: [category._id]
                      },
                    },
                    async function (err, faq) {
                      if (err) {
                        callback({
                            status: 0,
                            message: "Error fetching data",
                          },
                          null
                        );
                      }else{
                        var questions = [];
                        faq.forEach(function (question) {
                          // questions.push(question.title);
                          questions.push( {"title" : question.title, "slug" : question.slug});
  
                        }); 
                        if(questions.length > 0){
                          faqarray.push( {"title" : category.category_name , "slug" : category.slug ,"questions" : questions});
                        }
                    }
                    }
                  ).limit(5).sort({ created_on: 1 });

                }); // loop close

                setTimeout(function() {
                  callback(null, faqarray);
                }, 2000);


              }
            }
          ).sort({ created_on: 1 });
        },
      },
      function (err, results) {
        if (err !== null) {
          res.send({
            status: 0,
            message: "Oops! " + err,
          });
        } else {
          res.send({
            status: 1,
            data: results.faqDetails,
          });
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});



/*  Get Single Fa quetsions Details  */
router.post("/getFaqByCategorySlug", async (req, res) => {
  const slug  = req.body.slug;
  try {
    Categories.findOne(
      { slug: slug },
      async function (err, response) {
        if (err) {
          res.send({   
            status:0,         
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
            const cat_id  = response._id;
            const cat_name = response.category_name;
            Faq.find({
              page_status: "1",
              categories: {
                $in: [cat_id]
              },
            },
            async function (err, faq) {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              }else{
                var faqarray = [];
                faq.forEach(function (question) {
                  faqarray.push( {"title" : question.title , "slug" : question.slug});
                }); 
                res.send({ 
                  status: 1,             
                  message: "Fetched Faq details successfully.",
                  data: faqarray,
                  category:cat_name
                });
              }
            }
          ).sort({ created_on: 1 });

          }else{
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});



/*  Get Single News Details  */
router.post("/getsingleFaqBySlug", async (req, res) => {
   const slug  = req.body.slug;
   try {
     Faq.findOne(
       { slug: slug },
       async function (err, response) {
         if (err) {
           res.send({   
             status:0,         
             message: "Oops! " + err.name + ": " + err.message,
           });
         } else {
           if(response){
             res.send({ 
               status: 1,             
               message: "Fetched Faq details successfully.",
               data: response
             });
           }else{
             res.send({
               status: 0,
               message: "Details Unvailable."
             });
           }
         }
       }
     );
   } catch (e) {
     res.send({
       status: 0,
       message: "Oops! " + e.name + ": " + e.message,
     });
 }
 });
 

 
router.post("/searchFaq", async (req, res) => {
 
  let query =  '';
  let searchtext  = req.body.data.searchkey;
  // console.log(searchtext);
  try {
    async.parallel({
      faqDetails: function (callback) {
        Categories.find({
          parent_id: 0,
         // category_name: { $regex: '.*' + searchtext + '.*' }
         // category_name: { $regex: new RegExp("^" + searchtext.toLowerCase(), "i") }

          }, {
            _id: 1,
            category_name: 1,
            slug: 1,
            
          },
          async function (err, categoryInfo) {
            if (err || !categoryInfo) {
              callback({
                  status: 0,
                  message: "Error fetching data",
                },
                null
              );
            }
            if (categoryInfo) {
              var faqarray = [];
              categoryInfo.forEach(function (category) {
                Faq.find({
                    page_status: "1",
                    categories: {
                      $in: [category._id]
                    },
                    // title: { $regex: new RegExp("^" + searchtext.toLowerCase(), "i") }
                    title: { $regex:  new RegExp(searchtext, "i") }
                    // title: { $regex: '.*' + searchtext + '.*' }
                  },
                  async function (err, faq) {
                    if (err) {
                      callback({
                          status: 0,
                          message: "Error fetching data",
                        },
                        null
                      );
                    }else{
                      var questions = [];
                      faq.forEach(function (question) {
                        // questions.push(question.title);
                        questions.push( {"title" : question.title, "slug" : question.slug});

                      }); 
                      if(questions.length > 0){
                          faqarray.push( {"title" : category.category_name , "slug" : category.slug ,"questions" : questions});
                      }
                    
                    }
                  }
                ).limit(5).sort({ created_on: 1 });

              }); // loop close

              setTimeout(function() {
                callback(null, faqarray);
              }, 2000);


            }
          }
        ).sort({ created_on: 1 });
      },
    },
    function (err, results) {
      if (err !== null) {
        res.send({
          status: 0,
          message: "Oops! " + err,
        });
      } else {
          res.send({
            status: 1,
            data: results.faqDetails,
          });
        }
      }
   );
   
} catch (e) {
    res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
    });
}

});



/*  Get  Faq quetsions Details  */
router.post("/searchFaqQuestions", async (req, res) => {
  const slug  = req.body.faq_slug;
  let searchtext  = req.body.data.searchkey;
  try {
    Categories.findOne(
      { slug: slug },
      async function (err, response) {
        if (err) {
          res.send({   
            status:0,         
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
            const cat_id  = response._id;
            const cat_name = response.category_name;
            Faq.find({
              page_status: "1",
              categories: {
                $in: [cat_id]
              },
              title: { $regex:  new RegExp(searchtext, "i") }
            },
            async function (err, faq) {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              }else{
                var faqarray = [];
                faq.forEach(function (question) {
                  faqarray.push( {"title" : question.title , "slug" : question.slug});
                }); 
                res.send({ 
                  status: 1,             
                  message: "Fetched Faq details successfully.",
                  data: faqarray,
                  category:cat_name
                });
              }
            }
          ).sort({ created_on: 1 });

          }else{
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});



module.exports = router;







