/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require('async');

/* Include schema */
const Articles = require("../models/articles");
const Archives = require("../models/archives");
const News = require("../models/news");
const liveTraining = require("../models/liveTraining");
const Source = require("../models/source");
const Categories = require("../models/categories");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();

/*  Get all types of Articles  */
router.post("/getAllArticles", async (req, res) => {
  try {
    async.parallel({
      latestNews: getLatestArticles,
      recentNews: getRecentlyPublishedArticles,
      topNews: getTopArticles
    }, function (error, results) {
      if (error) {
        res.status(500).send(error);
        return;
      }
      res.send(results);
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

/*  Get the Latest Articles  */
router.post("/getLatestArticles", async (req, res) => {
  const page = parseInt(req.body.page);
  const PAGE_SIZE = 10;  
  const skip = (page - 1) * PAGE_SIZE; 
  var totalPages  = 0;
  const { filter } = req.body;
  let filterCondition = {page_status: "1"};  
  const search = filter.search;
  const industry = filter.industry;
  const source = filter.source;
  const date = filter.date;
  const topic = filter.topic;
  if(industry) {
    filterCondition.industry = industry
  } 
  if(source) {
    filterCondition.source = source
  } 
  if(topic) {
    filterCondition.topic = topic
  } 
  if(date) {
    filterCondition.releasedate =  moment(date).format("YYYY-MM-DD") 
  } 
  if(search) {
    filterCondition.title = new RegExp(search, "i") ;
    // filterCondition.$text = { $search: search } 
  }


  try {
    if(page < 0 || page === 0) {
      response = { status: 0, "message" : "Invalid page number, should start with 1"};
      return res.json(response)
    }
    /* Articles Count */
    Articles.count(filterCondition, async function(err,totalCount) {
      if(err) {
        response = {"status" : 0,"message" : "Error fetching data"}
      } else {
        totalPages = Math.ceil(totalCount / PAGE_SIZE);
      }
    });

    /* find all Articles */ 
    var query = Articles.find(filterCondition);
    query.skip(skip);
    query.limit(PAGE_SIZE);
    query.sort({ article_order: 1 });
    query.exec(async function (err, response) {
      if (err) {         
        response = {"status" : 0,"message" : "Error fetching data"} 
      }
      res.send({
        status: 1,
        CurrentPage: page,
        latestNews: response,
        totalPages: totalPages
      });
    })
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
  
});



/*  Get the Latest Articles  */
// router.post("/getLatestArticles", async (req, res) => {
//   const page = parseInt(req.body.page);
//   const PAGE_SIZE = 10;  
//   const skip = (page - 1) * PAGE_SIZE; 
//   var totalPages  = 0;
//   try {
//     if(page < 0 || page === 0) {
//       response = { status: 0, "message" : "Invalid page number, should start with 1"};
//       return res.json(response)
//     }
//     /* Articles Count */
//     Articles.count({  page_status: "1"}, async function(err,totalCount) {
//       if(err) {
//         response = {"status" : 0,"message" : "Error fetching data"}
//       } else {
//         totalPages = Math.ceil(totalCount / PAGE_SIZE);
//       }
//     });
//     let query1 = (searchtext = searchtopic = searchindustry = searchsource = date = "");
//     // searchtext  = req.body.serach.search ? req.body.serach.search : '';
//     searchtopic = req.body.serach.topic ? req.body.serach.topic : "";
//     searchindustry = req.body.serach.industry ? req.body.serach.industry : "";
//     searchsource = req.body.serach.source ? req.body.serach.source : "";
//     date = req.body.serach.date ? req.body.serach.date : "";
//     if (
//       searchtopic === "" &&
//       searchindustry === "" &&
//       searchsource === "" &&
//       searchtext === "" &&

//       date === ""
//     ) {
//     /* find all Articles */ 
//     var query = Articles.find({page_status: "1"});
//     query.skip(skip);
//     query.limit(PAGE_SIZE);
//     query.sort({ date: -1 });
//     query.exec(async function (err, response) {
//       if (err) {         
//         response = {"status" : 0,"message" : "Error fetching data"} 
//       }
//       res.send({
//         status: 1,
//         CurrentPage: page,
//         latestNews: response,
//         totalPages: totalPages
//       });
//     })
     

//   } else {
//     console.log("srach 2");

//     if (searchtopic && searchindustry && searchsource === "") {
//       console.log("searchproduct 123");
//       query1 = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           //{ related_products: { $regex: searchproduct, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     } else if (searchtopic === "" && searchindustry && searchsource) {
//       console.log("searchsource");
//       query1 = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           { industry: { $regex: searchindustry, $options: "i" } },
//           { source: { $regex: searchsource, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     } else if (searchtopic && searchindustry === "" && searchsource) {
//       console.log("searchsource");
//       query1 = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           { topic: { $regex: searchtopic, $options: "i" } },
//           { source: { $regex: searchsource, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     }else if (searchtopic && searchindustry && searchsource === "") {
//       console.log("searchsource");
//       query1 = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           { topic: { $regex: searchtopic, $options: "i" } },
//           { industry: { $regex: searchindustry, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     }
//     else if (searchtopic && searchindustry && searchsource) {
//       console.log("both");
//       query1 = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//           //{ sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           { topic: { $regex: searchtopic, $options: "i" } },
//           { industry: { $regex: searchindustry, $options: "i" } },
//           { source: { $regex: searchsource, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     } else {
//       console.log("else ");
//       query1 = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } }
//         ],
//         $and: [{ page_status: "1" }]
//       };
//     }

//     var query = Articles.find(query1);
//     query.skip(skip);
//     query.limit(PAGE_SIZE);
//     query.sort({ date: -1 });
//     query.exec(async function(err, response) {
//       if (err) {
//         response = { status: 0, message: "Error fetching data" };
//       }
//       res.send({
//         status: 1,
//         CurrentPage: page,
//         latestNews: response,
//         totalPages: totalPages
//       });
//     });
//   }


//   } catch (e) {
//     res.send({
//       status: 0,
//       message: "Oops! " + e.name + ": " + e.message,
//     });
//   }
  
// });

// router.post("/serachFunction1", async (req, res) => {
//   try {
//     let query = "";
//     let searchtext = req.body.text.search;
//     let searchtopic = req.body.text.topic;
//     let searchindustry = req.body.text.industry;
//     let searchsource = req.body.text.source;
//     let date = req.body.text.date;
//     console.log("searchtext sdsd", req.body.text.date);

//     if (
//       searchtopic === "" &&
//       searchindustry === "" &&
//       searchsource === "" &&
//       searchtext === "" &&
//       date === ""
//     ) {
//       res.send({
//         status: 2,
//         message: "Fetched articles successfully."
//       });
//     }

//     if (searchtopic && searchindustry && searchsource === "") {
//       console.log("searchproduct 123");
//       query = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           //{ related_products: { $regex: searchproduct, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     } else if (searchtopic === "" && searchindustry && searchsource) {
//       console.log("searchsource");
//       query = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           { industry: { $regex: searchindustry, $options: "i" } },
//           { source: { $regex: searchsource, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     } else if (searchtopic && searchindustry === "" && searchsource) {
//       console.log("searchsource");
//       query = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           { topic: { $regex: searchtopic, $options: "i" } },
//           { source: { $regex: searchsource, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     }else if (searchtopic && searchindustry && searchsource === "") {
//       console.log("searchsource");
//       query = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           { topic: { $regex: searchtopic, $options: "i" } },
//           { industry: { $regex: searchindustry, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     }
//     else if (searchtopic && searchindustry && searchsource) {
//       console.log("both");
//       query = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//           //{ sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } },
//           { releasedate: { $regex: date, $options: "i" } }
//         ],
//         $and: [
//           { topic: { $regex: searchtopic, $options: "i" } },
//           { industry: { $regex: searchindustry, $options: "i" } },
//           { source: { $regex: searchsource, $options: "i" } },
//           { page_status: "1" }
//         ]
//       };
//     } else {
//       console.log("else ");
//       query = {
//         $or: [
//           { title: { $regex: searchtext, $options: "i" } },
//          // { sub_title: { $regex: searchtext, $options: "i" } },
//           { description: { $regex: searchtext, $options: "i" } }
//         ],
//         $and: [{ page_status: "1" }]
//       };
//     }

//     Articles.find(query, async function(err, log) {
//       if (err) {
//         res.send({
//           status: 0,
//           message: "Oops! " + err.name + ": " + err.message
//         });
//       } else {
//         if (log.length > 0) {
//           res.send({
//             status: 1,
//             message: "Fetched articles successfully.",
//             data: log
//           });
//         } else {
//           res.send({
//             status: 0,
//             message: "No articles available."
//           });
//         }
//       }
//     }).sort({ date: -1 });
//   } catch (e) {
//     res.send({
//       status: 0,
//       message: "Oops! " + e.name + ": " + e.message
//     });
//   }
// });

/*  Get the Top Articles  */
function getTopArticles(callback) {
  Articles.find({is_top_news:"1" ,  page_status : "1"}).sort({ _id: -1 }).exec(callback);
}

/*  Get the Latest Articles  */
function getLatestArticles(callback) {
  Articles.find({page_status : "1"}).sort({ date: 1 }).limit(1).exec(callback); 
}

/*  Get the Recently Published Articles  */
function getRecentlyPublishedArticles(callback) {
  Articles.find({page_status : "1"}).sort({ _id: -1 }).limit(10).exec(callback);
}

/*  Get Single Articles Details  */
router.post("/getArticlesBySlug", async (req, res) => {
   const slug  = req.body.slug;
   try {
     Articles.findOne(
       { slug: slug },
       async function (err, response) {
         if (err) {
           res.send({   
             status:0,         
             message: "Oops! " + err.name + ": " + err.message,
           });
         } else {
           if(response){
             res.send({ 
               status: 1,             
               message: "Fetched Articles details successfully.",
               data: response
             });
           }else{
             res.send({
               status: 0,
               message: "Details Unvailable."
             });
           }
         }
       }
     );
   } catch (e) {
     res.send({
       status: 0,
       message: "Oops! " + e.name + ": " + e.message,
     });
 }
 });
 







 /*  Get the Latest Articles  */
router.post("/getLatestMoreNews", async (req, res) => {
  const page = parseInt(req.body.page);
  const PAGE_SIZE = 10;  
  const skip = (page - 1) * PAGE_SIZE; 
  var totalPages  = 0;
  try {
    if(page < 0 || page === 0) {
      response = { status: 0, "message" : "Invalid page number, should start with 1"};
      return res.json(response)
    }
    /* Articles Count */
    News.count({  page_status: "1"}, async function(err,totalCount) {
      if(err) {
        response = {"status" : 0,"message" : "Error fetching data"}
      } else {
        totalPages = Math.ceil(totalCount / PAGE_SIZE);
      }
    });

    /* find all Articles */ 
    var query = News.find({page_status: "1"});
    query.skip(skip);
    query.limit(PAGE_SIZE);
    query.sort({ news_order: 1 });
    query.exec(async function (err, response) {
      if (err) {         
        response = {"status" : 0,"message" : "Error fetching data"} 
      }
      res.send({
        status: 1,
        CurrentPage: page,
        latestNews: response,
        totalPages: totalPages
      });
    })
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
  
});


/*  Get the Latest Articles  */
router.post("/getLatestNewsByPagenumber", async (req, res) => {
  const page = parseInt(req.body.page);
  const PAGE_SIZE = 10;  
  const skip = (page - 1) * PAGE_SIZE; 
  var totalPages  = 0;
  try {
    if(page < 0 || page === 0) {
      response = { status: 0, "message" : "Invalid page number, should start with 1"};
      return res.json(response)
    }
    /* Articles Count */
    News.count({  page_status: "1"}, async function(err,totalCount) {
      if(err) {
        response = {"status" : 0,"message" : "Error fetching data"}
      } else {
        totalPages = Math.ceil(totalCount / PAGE_SIZE);
      }
    });

    /* find all Articles */ 
    var query = News.find({page_status: "1"});
    query.skip(skip);
    query.limit(PAGE_SIZE);
    query.sort({ news_order: 1 });
    query.exec(async function (err, response) {
      if (err) {         
        response = {"status" : 0,"message" : "Error fetching data"} 
      }
      res.send({
        status: 1,
        CurrentPage: page,
        latestNews: response,
        totalPages: totalPages
      });
    })
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
  
});


/*  Get the Latest More News count  */
router.post("/getMoreNewsCount", async (req, res) => {
  const PAGE_SIZE = 10;  
  try {
    News.findOne(
      {  page_status: "1"},
      async function (err, response) {
        if (err) {
          res.send({   
            status:0,         
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
            res.send({ 
              status: 1,             
              message: "Fetched Articles details successfully.",
              totalPages: Math.ceil(response.length / PAGE_SIZE)
            });
          }else{
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});
 



/* getSource */
router.post("/getSource", async (req, res) => {
  const token = req.body.token;
  try {
 
    Source.find(
        {  },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            
            if(log.length>0){
              res.send({
                status: 1,
                message: "Fetched Source Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "No Source available.",
                data: log
              });
            }

          }
        }
      )
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
});


/* getAllCatsIdsandNames*/

router.post("/getAllCatsIdsandNames", async (req, res) => {
  try {
    Categories.find(
      {
        status: 1
      },
      async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Fetched categories successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No categories available."
            });
          }
        }
      }
    )
      .sort({ category_name: 1 })
      .select("category_name _id");
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});
module.exports = router;







