//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require('async');
//Include schema


const Shipping = require("../../models/shipping_settings");
const Shipping_Method = require("../../models/shipping_method");
const config = require("../../config/config.json");
const Stripe = require("../../models/stripe");
const jwtKey = config.jwsPrivateKey;
const mongoose = require("mongoose");


//http request port set
const router = express.Router();


/***********shipping******************/


//getShippingData



router.post("/getShippingData", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Shipping.findOne({
            // slug: 'shipping_info'
          },

          async function (err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                status: 1,
                shippingDetails: log
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});


//update data

router.post("/updateShippingData", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);


      var condition = {
        slug: req.body.data.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '')
      };
      

      var update_value = {
        country: req.body.data.country,
        region: req.body.data.region,
        city: req.body.data.city,
        zipcode: req.body.data.zipcode,
        street_address: req.body.data.street_address,
        // slug: req.body.data.slug.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,''),
      };
      Shipping.findOneAndUpdate(condition, update_value, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          res.send({
            status: 1,
            message: "Shipping Data Updated successfully"
          });
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


//getShippingMethod



router.post("/getShippingMethod", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Shipping_Method.findOne({
            slug: 'ups-1'
          },

          async function (err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                status: 1,
                shippingMethodDetails: log
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});


//update shipping method

router.post("/updateShippingMethod", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);


      var condition = {
        slug: req.body.data.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '')
      };
   
      var shipping_update_value = {
                //status: 1,
                sandbox: req.body.data.sandbox,
                sandbox_account_no: req.body.data.sandbox_account_no,
                acount_no: req.body.data.acount_no,
                sandbox_access_key: req.body.data.sandbox_access_key,
                access_key: req.body.data.access_key,
                ups_customer_classification: req.body.data.ups_customer_classification,
                ups_pickup_type:req.body.data.ups_pickup_type,
                sandbox_username: req.body.data.sandbox_username,
                username: req.body.data.username,
                packaging_type_code: req.body.data.packaging_type_code,
                sandbox_password: req.body.data.sandbox_password,
                password: req.body.data.password,
                packing_type:req.body.data.packing_type,
                diamension_type:req.body.data.diamension_type,
                weight_type:req.body.data.weight_type,
                carrier_services:req.body.data.crOptions,
                
      };
      
      Shipping_Method.findOneAndUpdate(condition, shipping_update_value, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          res.send({
            status: 1,
            message: "Shipping Data Updated successfully"
          });
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


module.exports = router;



/***********shipping ****************/