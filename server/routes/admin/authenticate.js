//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
//Include schema
const Admin = require("../../models/admin");


//include joi validation methods
const Validate = require("../validations/validate");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;

//http request port set
const router = express.Router();

router.post("/authenticate", async (req, res) => {
  try {
    Validate.validateadminLogin(req, (result) => {
      if (result.error) {
        //validation error
        res.status(400).send(result.error.details[0].message);
      } else {
        //user login check
        try {
          Admin.findOne(
            {
              $or: [
                { username: req.body.username },
                { email: req.body.username },
              ],
            },
            function (err, log) {
              if (err) {
                res
                  .status(400)
                  .send("Oops! Something went wrong,please try later!");
              } else if (!log) {
                res.send({
                  status: 0,
                  message:
                    "There is no existing admin with the details that you’ve provided.",
                });
              } else {
                const validatepassword = bcrypt.compareSync(
                  req.body.password,
                  log.password
                );

                if (!validatepassword) {
                  res.send({
                    status: 0,
                    message:
                      "You have entered an invalid username and/or password.",
                  });
                } else {
                  log.last_login = moment().format("MM-DD-YYYY hh:mm:ss");
                  log.save();
                  const token = jwt.sign(
                    { _id: log._id, email: log.email },
                    jwtKey
                  );
                  res.send({
                    status: 1,
                    message: "Login success!You will be redirected now",
                    data: { token: token },
                  });
                }
              }
            }
          );
        } catch (ex) {
          res
            .status(400)
            .send("Oops! Something went wrong please try again later");
        }
      }
    });
  } catch (err) {
    
    res.status(400).send(ex);
  }
});
router.post("/create_admin", async (req, res) => {
  try {
    const salt = bcrypt.genSaltSync(10);
    let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
    bcryptPassword = bcrypt.hashSync(req.body.password, salt);
    const newUser = new Admin({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      username: req.body.username,
      password: bcryptPassword,
      created_on: createdon,
      status: 0,
    });
    newUser
      .save()
      .then(() => {
        res.send("success");
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  } catch (err) {
    res.send(err);
  }
});




router.post("/forgotpassword", async (req, res) => {
    //user login check
    try {
      Admin.findOne(
        {
          $or: [
            { username: req.body.data.username },
            { email: req.body.data.username },
          ],
        },
        function (err, log) {
          if (err) {
            res
              .status(400)
              .send("Oops! Something went wrong,please try later!");
          } else if (!log) {
            res.send({
              status: 1,
              type : 'error',
              message:
                "There is no existing admin with the details that you’ve provided.",
            });
          } else {

            

            sendForgotMail(log, (result) => {
              if (result == "success") {
                res.send({
                  status: 1,
                  type : 'sucess',
                  message: "Message will be sent to your email address.",
                });
              } else if (result == "failed") {
                res.send({
                  status: 1,
                  type : 'error',
                  message:
                    "Oops! Something went wrong with sending  mail...!",
                });
              }
            });
          

            // res.send({
            //   status: 1,
            //   type : 'sucess',
            //   message:
            //     "A message will be sent to your email address.",
            // });


          }
        }
      );
    } catch (ex) {
      res
        .status(400)
        .send("Oops! Something went wrong please try again later");
    }
});


router.post("/getAdminDetails", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {
  payload = jwt.verify(token, jwtKey);
  Admin.findOne(
      { email: req.body.email },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          }); 
        } else {

          if(log){
            
            res.send({
              status: 1,
              message: "Fetched admin details successfully.",
              data: log
            });

          }else{

            res.send({
              status: 0,
              message: "Details Unvailable."
            });

          }
        }
      }
    ).sort({ created_on: -1 });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
}
});



router.post("/verfiyUserWithId", async (req, res) => {
   
  try {
  
  Admin.findOne(
      { _id: req.body.id },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          }); 
        } else {

          if(log){
            
            res.send({
              status: 1,
              message: "Fetched admin details successfully.",
             // data: log
            });

          }else{

            res.send({
              status: 0,
              message: "Details Unvailable."
            });

          }
        }
      }
    ).sort({ created_on: -1 });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
 
});



function sendForgotMail(results, callback) {

 
  let header = fs
  .readFileSync(path.join(__dirname + "/email-templates/header.html"), {
    encootding: "utf-8"
  })
  .toString();

   
  var view = {
      imageUrl:config.imageUrl,     
  };
  var createHeader = mustache.render(header, view);


  let footer = fs
  .readFileSync(path.join(__dirname + '/email-templates/footer.html'), {
    encootding: "utf-8"
  })
  .toString();

   
  var view = {
      imageUrl:config.imageUrl,       
  };
  var createFooter = mustache.render(footer, view);
  
  let template = fs
  .readFileSync(path.join(__dirname + "/email-templates/forgotpassword.html"), {
    encootding: "utf-8"
  })
  .toString();

  let urlpass = config.siteurl + "admin/forgot-password/" + results._id + "";
  var view = {
      username: results.email,
      siteuUrl :urlpass,  
  };
  
  var createTemplate = mustache.render(template, view);
  //var htmlContent = createHeader+createTemplate+createFooter;
  var htmlContent = createHeader+ createTemplate +createFooter;
  sgMail.setApiKey(sendGridKey);
  const message = {
      to:results.email,
      // from: config.fromEmail,
      from : { email : config.fromEmail , name: 'Cimon'},
      subject: "Forgot Password",
      html: htmlContent
  };

  try {
      sgMail
          .send(message)
          .then(() => {
         //     callback(null, results);
         callback("success");
          })
          .catch(error => {
            
              const { message, code, response } = error;
              callback("failed");
              callback({
                  status: 0,
                  message: "Oops! failed to send order email" ,
                  from: 'mail'
              });
          });
  } catch (err) {
    
    callback("failed");
      callback({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
          from: 'mail catch'
      }, null);
  }
}



router.post("/UpdatePassword", async (req, res) => {
  //user login check
  try {
    Admin.findOne(
      {
        $or: [
          { username: req.body.data.username },
          { email: req.body.data.username },
        ],
      },
      function (err, log) {
        if (err) {
          res
            .status(400)
            .send("Oops! Something went wrong,please try later!");
        } else if (!log) {
          res.send({
            status: 1,
            type : 'error',
            message:
              "There is no existing admin with the details that you’ve provided.",
          });
        } else {

          
          const salt = bcrypt.genSaltSync(10);
          bcryptPassword = bcrypt.hashSync(req.body.data.password, salt);
          if((req.body.data.password ==  req.body.data.repassword) && (log._id == req.body.data._id)){



            var update_value = {
              password: bcryptPassword,
            };
            var condition = { _id: log._id };
            Admin.findByIdAndUpdate(condition, update_value, async function (
              err,
              log
            ) {
              if (err) {
                res.send({
                  status: 0,
                  type : 'error',
                  message: "Oops! " + err.name + ": " + err.message,
                });
              } else {
                res.send({
                  status: 1,
                  message: "Password changed successfully.",
                  type : 'sucess',
                });
              }
            });


           
          }
          // res.send({
          //   status: 1,
          //   type : 'sucess',
          //   message:
          //     "A message will be sent to your email address.",
          // });


        }
      }
    );
  } catch (ex) {
    res
      .status(400)
      .send("Oops! Something went wrong please try again later");
  }
});






module.exports = router;
