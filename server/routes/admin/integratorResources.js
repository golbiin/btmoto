//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");
//Include schema

const IntegratorResource = require("../../models/integrator_resources");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;

//http request port set
const router = express.Router();

/********************************************************* */


/*   Create Distributors */
router.post("/createDistributors", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      IntegratorResource.find({ slug: req.body.data.slug }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const newResources = new IntegratorResource(({
              title: req.body.data.title,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              date: createdon,
              //description: req.body.data.description,
              file_url: req.body.data.file_url,
              uploadFiles: req.body.data.uploadFiles,
              content : req.body.data.content,
              //        is_top_news : req.body.data.checked,
              related_products: req.body.data.related_products,
              related_cat: req.body.data.related_cat,
              releasedate: moment(req.body.data.releasedate).format(
                "YYYY-MM-DD"
              ),
              version: req.body.data.version,
              data_order : req.body.data.data_order
            }));

            await newResources
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have successfully uploaded an IntegratorResource."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*  Get all Distributors */
router.post("/getAllDistributors", async (req, res) => {
  const token = req.body.token;
  const filter = req.body.filter;
  let filterCondition = {};
  // if (filter.date) {
  //   const selectedDate = filter.date.split("/");
  //   const start = new Date(selectedDate[1], selectedDate[0] - 1, 1);
  //   const end = new Date(selectedDate[1], selectedDate[0] - 1, 31);
  //   filterCondition = { date: { $gte: start, $lte: end } };
  // }

  if (filter.type) {
    filterCondition.related_cat = filter.type;
  }
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      IntegratorResource.find(filterCondition, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched IntegratorResource successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No IntegratorResource available."
            });
          }
        }
      }).sort({ _id: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/* Delete Distributors */
router.post("/deleteDistributors", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        IntegratorResource.findByIdAndDelete({ _id: req.body.news_id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "IntegratorResource deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

/* trash archives */

router.post("/trashDistributors", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    payload = jwt.verify(token, jwtKey);

    try {
      var condition = { _id: req.body.id };
      var update_value = {
        page_status: 0
      };
      IntegratorResource.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "IntegratorResource moved to trash successfully"
          });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/* Get Single Distributors */
router.post("/getSingleDistributors", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        IntegratorResource.findOne(
          { _id: req.body.news_id },
          {
            _id: 1,
            is_top_news: 1,
            title: 1,
            slug: 1,
            file_url: 1,
            content: 1,
            description: 1,
            related_products: 1,
            related_cat: 1,
            releasedate: 1,
            version: 1,
            uploadFiles: 1,
            data_order : 1
          },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: { news_data: log }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

/*Update Distributors*/
router.post("/updateDistributors", async (req, res) => {
  console.log("testtttt");
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      console.log(
        "dattttttttttttttteeeee",
        moment(req.body.data.releasedate).format("YYYY-MM-DD")
      );
      //console.log(4343, req.body.data);
      IntegratorResource.find(
        {
          slug: req.body.data.slug
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, ""),
          _id: { $ne: req.body.data._id }
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "This slug already exists, try another."
              });
            } else {
              var condition = { _id: req.body.data._id };
              var update_value = {
                title: req.body.data.title,
                slug: req.body.data.slug
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, ""),
                description: req.body.data.description,
                content: req.body.data.content,
                //       is_top_news : req.body.data.checked,
                file_url: req.body.data.file_url,
                uploadFiles: req.body.data.uploadFiles,
                version: req.body.data.version,
                related_products: req.body.data.related_products,
                releasedate: moment(req.body.data.releasedate).format(
                  "YYYY-MM-DD"
                ),
                related_cat: req.body.data.related_cat,
                data_order : req.body.data.data_order
              };

              IntegratorResource.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "IntegratorResource Updated successfully"
                  });
                }
              });
            }
          }
        }
      ).sort({ created_on: 1 });
      payload = jwt.verify(token, jwtKey);
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*action handler archives */

router.post("/actionHandlerDistributors", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.archiveids };
        if (req.body.action == "3") {
          IntegratorResource.deleteMany(
            { _id: { $in: req.body.archiveids } },
            async function(err, log) {
              if (err) {
                res.send({ status: 1, message: "Something went wrong" + err });
              } else {
                res.send({
                  status: 1,
                  message: "IntegratorResource Trash deleted successfully"
                });
              }
            }
          );
        } else {
          IntegratorResource.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "IntegratorResource Updated successfully" });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*date filter archive */
router.post("/getDistributorDateFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      IntegratorResource.aggregate(
        [
          {
            $project: {
              year: { $year: "$date" },
              month: { $month: "$date" }
            }
          },
          {
            $group: {
              _id: null,
              date: { $addToSet: { year: "$year", month: "$month" } }
            }
          },
          { $sort: { _id: -1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*category filter archive */

router.post("/getDistributorTypeFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      IntegratorResource.aggregate(
        [ 
          {
            $project: {
              related_cat: 1
            }
          },
          {
            $group: {
              _id: "$related_cat"
            }
          },
          { $sort: { related_cat: 1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
/********************************************************** */

module.exports = router;
