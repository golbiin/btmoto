//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const mongoose = require('mongoose');
//Include schema
const Source = require("../../models/source");
const Users = require("../../models/users");
const config = require("../../config/config.json");
const Validate = require("../validations/validate");

const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();


router.post("/getAllSource", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Source.find({},
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 1,
                message: "Fetched Source Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "No Source available."
              });
            }

          }
        }
      ).sort({
        _id: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/createSource", async (req, res) => {
  const token = req.body.token;
    console.log('enter this section');
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      Source.find({
        title: req.body.data.title
      },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This Source already exists."
            });

          } else {
            

            const newSource = new Source({
              title: req.body.data.title,
            //  slug : req.body.data.title.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, ''),
            });

          
            await newSource.save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have successfully created a Source.",
                });
              })
              .catch((err) => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
              });

          }

        }
      }
    ).sort({
      //created_on: 1
    });

    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


 


router.post("/removeSource", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Source.findByIdAndDelete({
        _id: req.body.data._id
      }, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Your Source has been deleted successfully."
            });
          } else {
            res.send({
              status: 1,
              message: "Something went wrong"
            });
          }
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});




router.post("/getSourceById", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Source.findOne({
          _id: req.body.data.id
        },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if (log) {
              res.send({
                status: 1,
                message: "Fetched Source Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      ).sort({
        // created_on: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/updateSource", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {

      payload = jwt.verify(token, jwtKey);

      Source.find({
        title: req.body.data.title,
        _id: { $ne: mongoose.Types.ObjectId(req.body.data.id) }
      },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This Source already exists."
            });

          } else {

            var condition = {
              _id: req.body.data.id
            };
            var update_value = {
              title: req.body.data.title,
               
            };
            Source.findOneAndUpdate(condition, update_value, async function (
              err,
              log
            ) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
              } else {
                res.send({
                  status: 1,
                  message: "You have updated the Source successfully."
                });
              }
            })
          }
        }
      });
    
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});




module.exports = router;