//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const mongoose = require('mongoose');
//Include schema
const ProfileTab = require("../../models/profile_tab");
const Users = require("../../models/users");
const config = require("../../config/config.json");
const Validate = require("../validations/validate");

const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();



/***************** Menu *************** */
router.post("/createProfileTab", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      ProfileTab.find(
        {
          slug: req.body.data.name
            .trim()
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, "")
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "Menu Name already exist, please use another"
              });
            } else {
              let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
              const newProfileTab = new ProfileTab({
                name: req.body.data.name,
                slug: req.body.data.name
                  .trim()
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, "")
              });
              await newProfileTab
                .save()
                .then(() => {
                  res.send({
                    status: 1,
                    message: "You have created a Tab to the Cimon"
                  });
                })
                .catch(err => {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                });
            }
          }
        }
      ).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getAllTabs", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      ProfileTab.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched careers successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No careers available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/updateProfileTab", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var condition = { _id: req.body.data.menunid };
      var update_value = { menu_items: req.body.data.menu_items };
      ProfileTab.findOneAndUpdate(condition, update_value, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({ status: 1, message: "Tabs Updated successfully" });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
 



module.exports = router;