//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
//Include schema
const storelocation = require("../../models/store_locators");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();

router.post("/getAllStoreLocation", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      storelocation
        .find({}, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 1,
                message: "Fetched Store Locator Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "No Store Locator available."
              });
            }
          }
        })
        .sort({ date: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getStoreDetailsbyId", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      storelocation.findOne({ _id: req.body.data }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Fetched Store Locator Successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "Details Unvailable." + err
            });
          }
        }
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/createStore", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
      const newstorelocation = new storelocation({
        title: req.body.data.title,
        address: req.body.data.address,
        address2: req.body.data.address2,
        city: req.body.data.city,
        state: req.body.data.state,
        country: req.body.data.country,
        email: req.body.data.email,
        fax: req.body.data.fax,
        latitude: req.body.data.latitude,
        longitude: req.body.data.longitude,
        phone: req.body.data.phone,
        service_areas: req.body.data.servicearea,
        logo_img: req.body.data.images.featured_image,
        site_url: req.body.data.url,
        zip: req.body.data.zip,
        date: createdon,
        store_category: req.body.data.store_category,
        marker_image: req.body.data.marker_image,
      });
      await newstorelocation
        .save()
        .then(() => {
          res.send({
            status: 1,
            message: "You have created a Store Locator to the Cimon"
          });
        })
        .catch(err => {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/updateStoreLocator", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      var condition = { _id: req.body.data._id };
      var update_value = {
        address: req.body.data.address,
        address2: req.body.data.address2,
        city: req.body.data.city,
        country: req.body.data.country,
        email: req.body.data.email,
        fax: req.body.data.fax,
        latitude: req.body.data.latitude,
        logo_img: req.body.data.logo_img,
        longitude: req.body.data.longitude,
        phone: req.body.data.phone,
        service_areas: req.body.data.service_areas,
        site_url: req.body.data.site_url,
        state: req.body.data.state,
        store_category: req.body.data.store_category,
        title: req.body.data.title,
        zip: req.body.data.zip,
        marker_image: req.body.data.marker_image,
      };

      storelocation.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Store Locator Updated successfully"
          });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteStore", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      storelocation.findByIdAndDelete({ _id: req.body.data }, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log) {
            res.send({ status: 1, message: "Store deleted successfully" });
          } else {
            res.send({ status: 1, message: "Something went wrong" });
          }
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/trashStore", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      storelocation.findByIdAndUpdate(
        { _id: req.body.data },
        { page_status: 0 },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log) {
              res.send({ status: 1, message: "Store trashed successfully" });
            } else {
              res.send({ status: 1, message: "Something went wrong" });
            }
          }
        }
      );
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
router.post("/actionHandlerstore", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.ids };
        if (req.body.action == "3") {
          storelocation.deleteMany(
            { _id: { $in: req.body.ids } },
            async function(err, log) {
              if (err) {
                res.send({ status: 1, message: "Something went wrong" + err });
              } else {
                res.send({
                  status: 1,
                  message: "Store Trash deleted successfully"
                });
              }
            }
          );
        } else {
          storelocation.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "stores Updated successfully" });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

module.exports = router;
