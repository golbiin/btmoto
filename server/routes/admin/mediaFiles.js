//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");;
const multer = require("multer");
const AWS = require("aws-sdk");
var fs = require("fs");
const config = require("../../config/config.json");
const url = config.uploadURL;
const jwtKey = config.jwsPrivateKey;
const Mediafiles = require("../../models/media_files");
var multerS3 = require('multer-s3');
//http request port set
const router = express.Router();

/* upload path setting */
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, url);
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + "-" + file.originalname);
    },
  });
  

  var upload = multer({
    storage: storage,
  }).array("file");
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
  });
var s3 = new AWS.S3();
 
var uploads3 = multer({
  storage: multerS3({
    s3: s3,
    bucket: config.BucketName,
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, "profile_cimon/" + Date.now().toString() + "-" + file.originalname)
    }
  })
})
  /* upload path setting ends */

  /* Multiple file upload */
router.post('/uploadFiles/:token', uploads3.array('file', 70), function(req, res, next) { 
    const token = req.params.token;
      if (!token) {
        res.send({
          status: 0,
          message: "invalid request",
        });
      }
    const imageTypes = config.fileTypes.image;
    const audioTypes = config.fileTypes.audio;
    const videoTypes = config.fileTypes.video;
    const zipTypes = config.fileTypes.archives;
    const docTypes = config.fileTypes.documents;
    
    var ResponseData = [];
    isFileUploaded = false;
    req.files.map((item) => {

      const fileinfo = {
        originalname: item.originalname,
        encoding: item.encoding,
        mimetype: item.mimetype,
        filename: item.originalname,
        size: item.size,
      }

      let validFile = true;
      let filetype = 'image';

      if(imageTypes.includes(item.mimetype)) {
        filetype = 'image'
      } else if(audioTypes.includes(item.mimetype)) {
        filetype = 'audio'
      } else if(videoTypes.includes(item.mimetype)) {
        filetype = 'video'
      } else if(zipTypes.includes(item.mimetype)) {
        filetype = 'archive'
      } else if(docTypes.includes(item.mimetype)) {
        filetype = 'document';
      }
 
      item.file_info = fileinfo;
      item.status = 1;
      item.filetype = filetype;
      ResponseData.push(item);
    });
     console.log("ResponseData",ResponseData);
      res.send({
        status: 1,
        message: "File uploaded",
        data: { file_location: ResponseData},
      });
}) 

  router.post("/uploadFiles-old/:token", async (req, res) => {
      const token = req.params.token;
      if (!token) {
        res.send({
          status: 0,
          message: "invalid request",
        });
      } else {
        try {
          payload = jwt.verify(token, jwtKey);
          const user_id = payload._id;
          try {
            await upload(req, res, async function (err) {
              if (err instanceof multer.MulterError) {
                res.send({
                  status: 0,
                  message:
                    "Oops! something went wrong with uploading files.",
                });
              } else if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
              } else {
                try {
                  await awsUpload(req, (result) => {
                    if (result.message) {
                      return res
                        .status(500)
                        .json({ status: 0, message: result.message });
                    } else {
                      //success
                      req.files.map((item) => {
                        if(url + item.filename){
                          fs.unlinkSync(url + item.filename);
                          
                        }
                      })
                      res.send({
                        status: 1,
                        message: "File uploaded",
                        data: { file_location: result },
                      });
                    }
                  });
                } catch (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                  });
                }
              }
            });
          } catch (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          }
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        }
      }
    });
    
    
  
  function awsUpload(req, callback) {

    AWS.config.update({
      accessKeyId: config.accessKeyId,
      secretAccessKey: config.secretAccessKey,
    });
    var s3 = new AWS.S3();
  
    const file = req.files;
    const imageTypes = config.fileTypes.image;
    const audioTypes = config.fileTypes.audio;
    const videoTypes = config.fileTypes.video;
    const zipTypes = config.fileTypes.archives;
    const docTypes = config.fileTypes.documents;
    
    var ResponseData = [];
    isFileUploaded = false;
    file.map((item) => {

      const fileinfo = {
        originalname: item.originalname,
        encoding: item.encoding,
        mimetype: item.mimetype,
        filename: item.filename,
        size: item.size,
      }

      let validFile = true;
      let filetype = 'image';
      var filePath = url + item.filename + "";
      if(imageTypes.includes(item.mimetype)) {
        filetype = 'image'
      } else if(audioTypes.includes(item.mimetype)) {
        filetype = 'audio'
      } else if(videoTypes.includes(item.mimetype)) {
        filetype = 'video'
      } else if(zipTypes.includes(item.mimetype)) {
        filetype = 'archive'
      } else if(docTypes.includes(item.mimetype)) {
        filetype = 'document';
      }
      else {

        validFile = false;
        ResponseData.push({
          status: 0,
          file_name : item.filename,
          file_url : '',
          Location : '',
          filetype : filetype,
          file_info : fileinfo,
          message: "Sorry, this file type is not permitted for security reasons (" + item.filename + ")"
        })
      } 

      if(validFile) {
        isFileUploaded = true;
        const file_name =  Date.now() + "_" + item.filename;
        var params = {
          Bucket: config.BucketName,
          Body: fs.createReadStream(filePath),
          Key: "profile_cimon/" + file_name,
        };
        s3.upload(params, function (err, data) {
          //handle error
          if (err) {
            callback(err);
          }
          //success
          if (data) {
            data.file_info = fileinfo;
            data.status = 1;
            data.filetype = filetype;
            data.file_name = file_name;
            ResponseData.push(data);
            if(ResponseData.length == file.length){
              callback(ResponseData);
            } 
          }
        });
      }

      if(ResponseData.length == file.length && isFileUploaded == false) {
        callback(ResponseData);
      }
    });
  }

  /* List files */
  router.post("/getAllfiles", async (req, res) => {
    const token = req.body.token;
    const filter = req.body.filter;
    let filterCondition = {};
    if(filter.date) {
      const selectedDate  = filter.date.split('/');
      const start = new Date(selectedDate[1], selectedDate[0]-1, 1);
      const end = new Date(selectedDate[1], selectedDate[0]-1, 31);
      filterCondition = { created_on : {$gte: start, $lte: end}};
    }

    if(filter.type) {
      filterCondition.file_type = filter.type;
    }
    // console.log(filterCondition);
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
    try {
    payload = jwt.verify(token, jwtKey);
    Mediafiles.find(
        filterCondition,
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if(log.length>0){
              res.send({
                status: 1,
                message: "Fetched data successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "No data available."
              });
            }
          }
        }
      ).sort({ _id: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
  });


  /* Save files */
  
  router.post("/saveFiles", async (req, res) => {
    const token = req.body.token;   

    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } 
    else {

      payload = jwt.verify(token, jwtKey);
      let createdOn = moment().format("MM-DD-YYYY hh:mm:ss");
      let saveData  = req.body.data;
      let docs = [];
      let failed_files = [];
      saveData.forEach(function (item, index) {
        console.log(item);
        if(item.location !== "") {
          docs.push({ file_url: item.location, file_name: item.originalname,created_on: createdOn, 
            file_type: item.filetype, file_info: item.file_info});
        } else {
          failed_files.push({ file_url: item.location, file_name: item.originalname,message: "item.message", file_type: item.file_type});
        }
      });

      console.log(docs);

      if( docs.length > 0 ) {
        Mediafiles.insertMany(docs)
        .then(result => {
          res.send({
            "status":1,
            "failed_files" : failed_files,
            "msg":(`You have uploaded successfully.`)
          });
          return result
        })
        .catch(err => {
          res.send({
            "status":0,
            "msg":(`Failed to insert documents: ${err}`)
          });
        })  
      } else {
        res.send({
          "status":0,
          "failed_files" : failed_files,
          "msg":(`Failed to upload files!`)
        });
      }
    }
  });

/* Delete Files */

router.post("/deleteEntry", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Mediafiles.findByIdAndDelete({ _id: req.body.id }, async function (
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "You have deleted this entry." });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});

router.post("/bulkDelete", async (req, res) => {

  const token = req.body.token;
  const ids = req.body.id;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Mediafiles.find({ _id: {$in: ids} }, async function (
          err,
          response
        ) {
          if (err) {
            res.send(err);
          } else {
            let awskeys = [];
            for(let media of response) {
              awskeys.push({Key : media.file_name});            
            };
            await awsDelete(awskeys, (result) => {
              if (result.message) {
                return res
                  .send({ status: 0, message: result.message });
              } else {

                Mediafiles.deleteMany({ _id: { $in: ids} }, 
                  async function (err, result) {
                    if (err) {
                      res.send(err);
                    } else {
                      //success
                      res.send({
                        status: 1,
                        message: "File Deleted",
                        data: { file_location: result },
                      });
                    }
                });                            
              }
            });
          }        
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }

});


  /* Get name Description */
router.post("/getNameById", async (req, res) => {
  const id = req.body.id;
  try {
    Mediafiles.findOne({
              _id: id
          },
          async function (err, response) {
              if (err) {
                  res.send({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message,
                  });
              } else {
                  if (response) {

                      let file_details = {
                        originalname: response.file_info.originalname ? response.file_info.originalname : '' ,
                        encoding: response.file_info.encoding ? response.file_info.encoding : '',
                        mimetype: response.file_info.mimetype ? response.file_info.mimetype : '',
                        filename: response.file_info.filename ? response.file_info.filename : '',
                        size: response.file_info.size ? response.file_info.size : ''
                      }
                      res.send({
                          status: 1,
                          message: "Fetched  details successfully.",
                          singleDetails: response,
                          size: response.file_info.size,
                          file_details:  file_details
                      });
                  } else {
                      res.send({
                          status: 0,
                          message: "Details Unvailable."
                      });
                  }
              }
          }
      )
  } catch (e) {
      res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
      });
  }
})

/* Update Name */
router.post("/updateTitle", async (req, res) => {
 
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      // console.log('-->',req.body.data);
      payload = jwt.verify(token, jwtKey);
      try {
        var update_value = {
     
          file_name: req.body.data.file_name,
    
        };
       // const slug = req.body.data.slug.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
      //  const slug = req.body.data.slug;
      const id = req.body.data.id;
        var condition = {
          _id: id
        };
        Mediafiles.findOneAndUpdate(condition,update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            res.send({
              status: 1,
              message: "Media Title changed successfully.",
              data: {
                media_data: log
              },
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});


/* List date filter options */
router.post("/getDateFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {
    payload = jwt.verify(token, jwtKey);
    Mediafiles.aggregate([
      { "$project": { 
          "year": { "$year": "$created_on" }, 
          "month": { "$month": "$created_on" } 
      }},
      { "$group": { 
          "_id": null, 
          "created_on": { "$addToSet": { "year": "$year", "month": "$month" }}
      }},
      { $sort : { _id : -1 } }
    ], function( err, data ) {
      if ( err )
        res.send(err);
      else{
        res.send({
          status: 1,
          options: data
        });
      }
    });
  
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
}
});

/* List type filter options */
router.post("/getTypeFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      
      Mediafiles.aggregate([
        { "$project": { 
            "file_type": 1, 

        }},
        { "$group": { 
            "_id": '$file_type'
        }},
        { $sort : { file_type : 1 } }
      ], function( err, data ) {
        if ( err )
          res.send(err);
        else{
          res.send({
            status: 1,
            options: data
          });
        }
      });
    
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


function awsDelete(objects, callback) {
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
  });
  var s3 = new AWS.S3();
  var options = {
    Bucket: config.BucketName,
    Delete: {
      Objects: objects
    }
  };

  s3.deleteObjects(options, function(err, data){
    if(data){
      callback(data);
    } else {
      callback(err);
    }
  });
}



module.exports = router;



