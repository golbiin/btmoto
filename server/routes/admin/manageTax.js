//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
//Include schema
const Tax = require("../../models/tax_data"); 
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();
var async = require('async');

router.post("/getAllTax", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {

   

    payload = jwt.verify(token, jwtKey);
    Tax.find(
        {  },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            
            if(log.length>0){
              res.send({
                status: 1,
                message: "Fetched Taxs Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "No Taxs available.",
                data: log
              });
            }

          }
        }
      )
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});




router.post("/getTaxById", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {

   

    payload = jwt.verify(token, jwtKey);
    Tax.find(
      { _id: req.body.data.id },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            
            if(log.length>0){
              res.send({
                status: 1,
                message: "Fetched Taxs Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "No Taxs available.",
                data: log
              });
            }

          }
        }
      )
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/createTax", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
      try {
        payload = jwt.verify(token, jwtKey);
      
        
        Tax.find(
          { tax_name: req.body.data.tax_name },
          async function (err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {
              if(log.length>0){
                res.send({
                  status: 0,
                  message: "Tax already exist"
                });
  
              } else {
                const newTax = new Tax({
                  country: req.body.data.country,
                  state: req.body.data.state_code,
                  zipcode: req.body.data.zipcode,
                  rate_percent: req.body.data.rate_percent,
                  tax_name: req.body.data.tax_name.trim(),
                  priority: req.body.data.priority,
                  shipping : req.body.data.shipping
                });
                await newTax.save()  
                .then(() => {
                  res.send({
                    status: 1,
                    message: "You have created a Tax to the Cimon",
                  });
                }) 
                .catch((err) => {
                  res.send({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message,
                  });
                });

              }

            }
          }
        ).sort({ created_on: 1 });

      } catch (e) {
        res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
        });
      }
    }
  });
  


// router.post("/updateTax", async (req, res) => {
//   const token = req.body.token;
//   const data = req.body.data;
 
//   if (!token) {
//     res.send({ status: 0, message: "Invalid Request" });
//   } else {
//   try {
//     payload = jwt.verify(token, jwtKey);

//     async.waterfall([
//       function(callback) {

//         data.forEach(function(items) {


//         var condition = { tax_name: items.tax_name.trim() };
        
//         Tax.find(
//          condition,
//          async function (err, log) {
//            if (err) {
//              // res.send({
//              //   status: 0,
//              //   message: "Oops! " + err.name + ": " + err.message,
//              // });

//            } else {
//              if(log.length>0){
               
//               var update_value = {  
//                 country: items.country,
//                 state: items.state,
//                 zipcode: items.zipcode,
//                 rate_percent: items.rate_percent,
//                // tax_name: items.tax_name,
//                 priority: items.priority,
//                 shipping : false };
//               Tax.findByIdAndUpdate(condition, update_value, async function (
//                 err,
//                 log
//               ) {
//                 if (err) {
//                   callback(null, 'Update ----------->');
              
//                 } else {
//                   callback(null, 'Update error----------->' + err);
//                 }
//               });
     

//              }else{
          
//               const newTax = new Tax({
//                 country: items.country,
//                 state: items.state,
//                 zipcode: items.zipcode,
//                 rate_percent: items.rate_percent,
//                 tax_name: items.tax_name.trim(),
//                 priority: items.priority,
//                 shipping : false
//               });
//               await newTax.save() 
//               .then(() => {
//                 callback(null, '----------->');
              
//               }) 
//               .catch((err) => {
//                 callback(null, 'error----------->' + err);
               
//               });
               
//              }
//            }
//          }
//        ).sort({ order_no: -1 });


//       });

//      //   callback(null, '----------->');
         
//       }
   
//   ], function (err, result) {
//       // result now equals 'done'
//       if (err) {
        
//         res.send({
//           status:0,
//           message: "No Tax available.",
//         });
        
//       } else {

        
//         res.send({
//           status: 1,
//           message: "Fetched Tax successfully.",
//           data: result
//         });


//       }
//   });

  
//     } catch (e) {
//       res.send({
//         status: 0,
//         message: "Oops! " + e.name + ": " + e.message,
//       });
//     }
//   }
// });


router.post("/removeTax", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Tax.findByIdAndDelete({ _id: req.body.data._id }, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(log){
            res.send({ status: 1, message: "This tax code has been deleted succcessfully." });
          } else {
            res.send({ status: 1, message: "Something went wrong" });
          }
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
}); 



router.post("/updateTax", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      var condition = { _id: req.body.data.id };
      var update_value = { 
        country: req.body.data.country,
        state: req.body.data.state_code,
        zipcode: req.body.data.zipcode,
        rate_percent: req.body.data.rate_percent,
 //       tax_name: req.body.data.tax_name.trim(),
        priority: req.body.data.priority,
        shipping : req.body.data.shipping
      };
      Tax.findOneAndUpdate(condition, update_value, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          res.send({ status: 1, message: "Tax Updated successfully" });
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});
 
module.exports = router;