const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const mustache = require("mustache");
const multer = require("multer");
const AWS = require("aws-sdk");
var fs = require("fs");
const config = require("../../config/config.json");
const url = config.uploadURL;
const jwtKey = config.jwsPrivateKey;
var multerS3 = require('multer-s3');

/*******************upload path setting******/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, url);
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
var upload = multer({
  storage: storage,
}).single("file");


AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
  });
var s3 = new AWS.S3();
 
var uploads3 = multer({
  storage: multerS3({
    s3: s3,
    bucket: config.BucketName,
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, "profile_cimon/" + Date.now().toString() + "-" + file.originalname)
    }
  })
})
/*******************end upload path setting*/
//http request port set
const router = express.Router();

router.post('/uploadData/:token', uploads3.array('file', 70), function(req, res, next) { 
    const token = req.params.token;
      if (!token) {
        res.send({
          status: 0,
          message: "invalid request",
        });
      } 
    res.send({
                    status: 1,
                    message: "File uploaded",
                    data: { file_location: req.files[0].location },
                  });
}) 
router.post("/uploadData-old/:token", async (req, res) => {
  const token = req.params.token;
  if (!token) {
    res.send({
      status: 0,
      message: "invalid request",
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        await upload(req, res, async function (err) {
          if (err instanceof multer.MulterError) {
            res.send({
              status: 0,
              message:
                "Oops! something went wrong with uploading profile image.",
            });
          } else if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            try {
              await awsUpload(req, (result) => {
                if (result.message) {
                  //error
                  return res
                    .status(500)
                    .json({ status: 0, message: result.message });
                } else {
                  //sucess
                  fs.unlinkSync(url + req.file.filename);
                  res.send({
                    status: 1,
                    message: "File uploaded",
                    data: { file_location: result },
                  });
                }
              });
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            }
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message,
      });
    }
  }
});



function awsUpload(req, callback) {
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
  });
  var s3 = new AWS.S3();
  var filePath = url + req.file.filename + "";

  //configuring parameters
  var params = {
    Bucket: config.BucketName,
    Body: fs.createReadStream(filePath),
    Key: "profile_cimon/" + Date.now() + "_" + req.file.filename,
  };
  s3.upload(params, function (err, data) {
    //handle error
    if (err) {
      callback(err);
    }
    //success
    if (data) {
      
      callback(data.Location);
    }
  });
}


function awsUploadbase64(req, callback) {
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
  });
  var s3 = new AWS.S3();
  
 // var buf = req.body.data.base64image.replace(/^data:image\/png;base64,/, "");
  buf = new Buffer(req.body.data.base64image.replace(/^data:image\/\w+;base64,/, ""),'base64')
  const type = req.body.data.base64image.split(';')[0].split('/')[1];
  //configuring parameters
  var params = {
    Bucket: config.BucketName,
    Body: buf,
    ContentEncoding: 'base64', // required
    ContentType: `image/${type}`,
    Key: "profile_cimon/" + Date.now() + "_" + req.body.data.image_id,
  };
  
  s3.upload(params, function (err, data) {
    //handle error
    if (err) {
      callback(err);
    }
    //success
    if (data) {
      
      callback(data.Location);
    }
  });
}

 



router.post("/imageupload", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "invalid request",
    });
  } else {
    var base64Data = req.body.data.base64image.replace(/^data:image\/png;base64,/, "");
   
  //  res.send({ status: 0, message: token  , sate: base64Data , image_id  : req.body.data.image_id , ssss:'ffsddf'});
 

          try {
            await awsUploadbase64(req, (result) => {
              if (result.message) {
                //error
                res.send({ uploaded: 0, url : '' });

              } else {
                //sucess
               // fs.unlinkSync(url + req.body.data.image_id);
                res.send({ uploaded: 1, url : result });
              }
            });
          } catch (err) {
            res.send({ uploaded: 0, url : '' });
          } 



  }

 
});



function awsUploadEditor(req, callback) {
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
  });
  var s3 = new AWS.S3();
  var filePath = url + req.file.filename + "";
  var d = new Date();
  
  //configuring parameters
  var params = {
    Bucket: config.BucketName,
    Body: fs.createReadStream(filePath),
    Key: "profile_cimon/"   + req.file.filename,
  };
  s3.upload(params, function (err, data) {
    //handle error
    if (err) {
      
      callback(err);
    }
    //success
    if (data) {
      
      callback(data.Location);
    }
  });
}


router.post('/imageupload-new', uploads3.array('upload', 70), function(req, res, next) { 
    
    res.send({
                    uploaded: 1,
                    message: "File uploaded",
                    url: req.files[0].location ,
                  });
}) 
router.post('/imageupload-new-old', getUpload().single('upload'), (req, res) => {
  
  

  try {
     awsUploadEditor(req, (result) => {
      if (result.message) {
        
        //error
        res.send({ uploaded: 0, url : '' });
      } else {
        //sucess
        res.send({ uploaded: 1, url : result });
      }
    });
  } catch (err) {
    res.send({ uploaded: 0, url : '' });
  }

 
});



module.exports = router;





 
function getUpload() {
  // file upload config using multer

 


  var upload = multer({
    storage: multer.diskStorage({
      destination: function (req, file, cb) {
        var d = new Date();
        
        const  newurl  = url +  d.getFullYear() + '/' + d.getMonth();
        // if (!fs.existsSync(newurl)){
        //       // fs.mkdirSync(newurl);
        //       fs.mkdir(newurl, {recursive: true}, err => {
        //         if (err)  newurl = url;
               
        //       })
        // }
         

        cb(null, url);
      },
      filename: function (req, file, cb) {
        var d = new Date();
        var fileExtension = path.extname( Date.now() + "-" + file.originalname);
              var fileBase = path.basename(file.originalname, fileExtension);
              var fileSlug = d.getFullYear() + '_' + d.getMonth() + "_" + Date.now() + "_" + fileBase + fileExtension;
              
        cb(null,fileSlug);
      },  
    })
   
  });

  
 
  return upload;
}
