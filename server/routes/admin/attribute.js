//load dependencies
const express = require('express')
const jwt = require('jsonwebtoken')
const moment = require('moment')
var async = require('async')
//Include schema

const News = require('../../models/news')
const Attributes = require('../../models/attributes')
const Categories = require('../../models/categories')
const config = require('../../config/config.json')
const jwtKey = config.jwsPrivateKey

//http request port set
const router = express.Router()

/********************************************************* */

router.post('/createAttributes', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      //   console.log('last value are ', req.body);
      // payload = jwt.verify(token, jwtKey);

      Attributes.find({ slug: req.body.data.slug.trim() }, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: 'This slug already exists, try another.'
            })
          } else {
            let createdon = moment().format('MM-DD-YYYY hh:mm:ss')
            const newAttributes = new Attributes({
              name: req.body.data.name,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, '-')
                .replace(/[^\w-]+/g, ''),
              // attributes: req.body.data.attributes,
              status: 1,
              created_on: createdon,
              parent_id: req.body.data.parent_category,
              category: req.body.data.category,
              order: req.body.data.ordernumber
            })
            await newAttributes
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: 'You have created a new Attributes.'
                })
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: 'Oops! ' + err.name + ': ' + err.message
                })
              })
          }
        }
      }).sort({ created_on: 1 })
    } catch (e) {
      res.send({
        status: 0,
        message: 'Oops! ' + e.name + ': ' + e.message
      })
    }
  }
})

router.post('/getAllAttributes', async (req, res) => {
  try {
    async.waterfall(
      [
        function (callback) {
          Attributes.find({ parent_id: 0 }, async function (err, Parent_cat) {
            if (err) {
              callback(null, 'a', 'b')
            } else {
              if (Parent_cat.length > 0) {
                Attributes.find({ parent_id: { $ne: 0 } }, async function (
                  err,
                  child_cat
                ) {
                  if (err) {
                    callback(null, Parent_cat, [])
                  } else {
                    if (child_cat.length > 0) {
                      callback(null, Parent_cat, child_cat)
                    } else {
                      callback(null, Parent_cat, [])
                    }
                  }
                })
                  .collation({ locale: 'en' })
                  .sort({ order: 1 })
              } else {
                callback(null, [], [])
              }
            }
          })
            .collation({ locale: 'en' })
            .sort({ order: 1 })
        },
        function (ParentCat, ChildCat, callback) {
          // arg1 now equals 'one' and arg2 now equals 'two'

          const Items = []

          ParentCat.forEach(function (item) {
            item.parent_id = 'level1'
            item.userrrrr = 'level1'
            Items.push(item)
            ChildCat.forEach(function (childitem) {
              if (item._id == childitem.parent_id) {
                childitem.parent_id = 'level2'
                childitem.userrrrr = 'level3'
                Items.push(childitem)
              }
            })
          })

          const Thrid_child_left = ChildCat

          Items.forEach(function (sortableitem) {
            for (var i = 0; i < Thrid_child_left.length; i++) {
              var obj = Thrid_child_left[i]
              if (sortableitem._id === obj._id) {
                Thrid_child_left.splice(i, 1)
              }
            }
          })

          callback(null, Items, Thrid_child_left)
        },
        function (Items, Thrid_child_left, callback) {
          // arg1 now equals 'three'

          const AllItems = []

          Items.forEach(function (item) {
            AllItems.push(item)
            Thrid_child_left.forEach(function (childitem) {
              if (item._id == childitem.parent_id) {
                // const source = { b: 4, c: 5 };
                // const returnedTarget = {...childitem , user_type : source};

                //childitem.push({country: IN});

                childitem.parent_id = 'level3'
                childitem.userrrrr = 'level3'

                AllItems.push(childitem)
              }
            })
          })

          callback(null, AllItems)
        }
      ],
      function (err, result) {
        // result now equals 'done'

        if (err) {
          res.send({
            status: 0,
            message: 'No Attributes available.'
          })
        } else {
          res.send({
            status: 1,
            message: 'Fetched Attributes successfully.',
            data: result
          })
        }
      }
    )
  } catch (e) {
    res.send({
      status: 0,
      message: 'Oops! ' + e.name + ': ' + e.message
    })
  }
})

router.post('/getAllAttributesFrontend', async (req, res) => {
  try {
    let category_items = req.body.data
   
    async.waterfall(
      [
        function (callback) {
          Attributes.find({ parent_id: 0 ,
            category: {
              $in: category_items
            }
          }, async function (err, Parent_cat) {
            if (err) {
              callback(null, 'a', 'b')
            } else {
              if (Parent_cat.length > 0) {
                Attributes.find(
                  {
                    parent_id: { $ne: 0 },
                    category: {
                      $in: category_items
                    }
                  },
                  async function (err, child_cat) {

                 //   console.log('getAllAttributes xxx', child_cat)
                    if (err) {
                      callback(null, Parent_cat, [])
                    } else {
                      if (child_cat.length > 0) {
                        callback(null, Parent_cat, child_cat)
                      } else {
                        callback(null, Parent_cat, [])
                      }
                    }
                  }
                )
                  .collation({ locale: 'en' })
                  .sort({ order: 1 })
              } else {
                callback(null, [], [])
              }
            }
          })
            .collation({ locale: 'en' })
            .sort({ order: 1 })
        },
        function (ParentCat, ChildCat, callback) {
          // arg1 now equals 'one' and arg2 now equals 'two'

          const Items = []

          ParentCat.forEach(function (item) {
          
            item.parent_id = 'level1'
            item.userrrrr = 'level1'
          
            Items.push(item)
            ChildCat.forEach(function (childitem) {
              if (item._id == childitem.parent_id) {
               
                childitem.parent_id = 'level2'
                childitem.userrrrr = 'level3'
                Items.push(childitem)
              }
            })
          })

          const Thrid_child_left = ChildCat

          Items.forEach(function (sortableitem) {
            for (var i = 0; i < Thrid_child_left.length; i++) {
              var obj = Thrid_child_left[i]
              if (sortableitem._id === obj._id) {
                Thrid_child_left.splice(i, 1)
              }
            }
          })

          callback(null, Items, Thrid_child_left)
        },
        function (Items, Thrid_child_left, callback) {
          // arg1 now equals 'three'

          const AllItems = []

          Items.forEach(function (item) {
            AllItems.push(item)
            Thrid_child_left.forEach(function (childitem) {
              if (item._id == childitem.parent_id) {
                // const source = { b: 4, c: 5 };
                // const returnedTarget = {...childitem , user_type : source};

                //childitem.push({country: IN});

                childitem.parent_id = 'level3'
                childitem.userrrrr = 'level3'

                AllItems.push(childitem)
              }
            })
          })

          callback(null, AllItems)
        }
      ],
      function (err, result) {
        // result now equals 'done'

        if (err) {
          res.send({
            status: 0,
            message: 'No Attributes available.'
          })
        } else {
          res.send({
            status: 1,
            message: 'Fetched Attributes successfully.',
            data: result
          })
        }
      }
    )
  } catch (e) {
    res.send({
      status: 0,
      message: 'Oops! ' + e.name + ': ' + e.message
    })
  }
})

router.post('/getAllParentAttributes', async (req, res) => {
  const token = req.body.token

  try {
    async.parallel(
      {
        ParentCat: getParentCateogery
        //     ChildCat: getChildCategory
      },
      function (error, results) {
        if (error) {
          res.send({
            status: 0,
            message: 'No Attributes available.'
          })
        } else {
          const Items = []

          results.ParentCat.forEach(function (item) {
            Items.push(item)
            // results.ChildCat.forEach(function(childitem) {
            //   if (item._id == childitem.parent_id) {
            //     let changename = childitem.name;
            //     childitem.name = " - " + changename;

            //     Items.push(childitem);
            //   }
            // });
          })

          res.send({
            status: 1,
            message: 'Fetched Attributes successfully.',
            data: Items
          })
        }
      }
    )
  } catch (e) {
    res.send({
      status: 0,
      message: 'Oops! ' + e.name + ': ' + e.message
    })
  }
})

function getParentCateogery (callback) {
  Attributes.find({ parent_id: 0 })
    .collation({ locale: 'en' })
    .sort({ name: 1 })
    .exec(callback)
}
function getChildCategory (callback) {
  Attributes.find({ parent_id: { $ne: 0 } })
    .collation({ locale: 'en' })
    .sort({ name: 1 })
    .exec(callback)
}

router.post('/removeAttributes', async (req, res) => {
  console.log('removeAttributes')
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)
      Attributes.findByIdAndDelete({ _id: req.body.data._id }, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        } else {
          var update_value = {
            parent_id: 0,
            category: []
          }

          try {
            var condition = { parent_id: req.body.data._id }
            Attributes.updateMany(condition, update_value, async function (
              err,
              log
            ) {
              if (err) {
                res.send({ status: 1, message: 'Something went wrong' })
              } else {
                res.send({
                  status: 1,
                  message: 'You have successfully deleted this Attribute.'
                })
              }
            })
          } catch (e) {
            res.send({ status: 1, message: 'Something went wrong' })
          }
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: 'Token verification failed.' })
      }
      res.send({
        status: 0,
        message: 'Oops! ' + e.name + ': ' + e.message
      })
    }
  }
})

router.post('/getAttributeBySlug', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)
      Attributes.findOne(
        { status: 1, _id: req.body.data.slug.trim() },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            if (log) {
              res.send({
                status: 1,
                message: 'Fetched Attributes details successfully.',
                data: log
              })
            } else {
              res.send({
                status: 0,
                message: 'Details Unvailable.'
              })
            }
          }
        }
      ).sort({ created_on: -1 })
    } catch (e) {
      res.send({
        status: 0,
        message: 'Oops! ' + e.name + ': ' + e.message
      })
    }
  }
})

router.post('/updateAttribute', async (req, res) => {
  console.log('updateAttribute')
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)

      var condition = { _id: req.body.data._id }
      var update_value = {
        name: req.body.data.name,
        parent_id: req.body.data.parent_category,
        category: req.body.data.category,
        slug: req.body.data.slug
          .trim()
          .toLowerCase()
          .replace(/ /g, '-')
          .replace(/[^\w-]+/g, ''),
        order: req.body.data.ordernumber
      }

      Attributes.find({ _id: req.body.data._id }, async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        } else {
          Attributes.findOneAndUpdate(condition, update_value, async function (
            err,
            log
          ) {
            if (err) {
              res.send({
                status: 0,
                message: 'Oops! ' + err.name + ': ' + err.message
              })
            } else {
              res.send({
                status: 1,
                data : log,
                message: 'You have updated this attribute succesfully.'
              })
            }
          })
          console.log(
            'updateAttribute',
            req.body.data._id,
            log[0].slug,
            req.body.data.slug.trim()
          )
          //   if (log.length > 0) {

          //     if(log[0].slug == req.body.data.slug.trim()){
          //       Attributes.findOneAndUpdate(condition, update_value, async function(
          //         err,
          //         log
          //       ) {
          //         if (err) {
          //           res.send({
          //             status: 0,
          //             message: "Oops! " + err.name + ": " + err.message
          //           });
          //         } else {
          //           res.send({ status: 1, message: "You have updated this attribute succesfully." });
          //         }
          //       });
          //     } else {
          //       res.send({
          //         status: 0,
          //         message: "This slug already exists, try another."
          //       });
          //     }

          //   } else {

          // }
        }
      }).sort({ created_on: 1 })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: 'Token verification failed.' })
      }
      res.send({
        status: 0,
        message: 'Oops! ' + e.name + ': ' + e.message
      })
    }
  }
})

router.post('/getAllCategory', async (req, res) => {
  try {
    Categories.find(
      {
        status: 1
      },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        } else {
          if (log) {
            res.send({
              status: 1,
              message: 'Fetched categories successfully.',
              data: log
            })
          } else {
            res.send({
              status: 0,
              message: 'No categories available.'
            })
          }
        }
      }
    )
  } catch (e) {
    res.send({
      status: 0,
      message: 'Oops! ' + e.name + ': ' + e.message
    })
  }
})

/* getCurrentCategoryIdbySlug*/
router.post('/getCurrentCategoryIdbySlug', async (req, res) => {
  try {
    Categories.find(
      {
        slug: {
          $in: req.body.data
        }
      },
      {
        _id: 1
      },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        } else {
          if (log) {
            res.send({
              status: 1,
              message: 'Fetched categories successfully.',
              data: log
            })
          } else {
            res.send({
              status: 0,
              message: 'No categories available.'
            })
          }
        }
      }
    )
  } catch (e) {
    res.send({
      status: 0,
      message: 'Oops! ' + e.name + ': ' + e.message
    })
  }
})

/********************************************************** */

module.exports = router
