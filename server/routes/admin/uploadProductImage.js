const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const mustache = require("mustache");
const multer = require("multer");
const AWS = require("aws-sdk");
var fs = require("fs");
const config = require("../../config/config.json");
const url = config.uploadURL;
const jwtKey = config.jwsPrivateKey;
var multerS3 = require('multer-s3');

/*******************upload path setting******/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, url);
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

var uploadSingle = multer({
  storage: storage,
}).single("file");

var upload = multer({
  storage: storage,
}).array("file");

/*******************upload path setting ends******/
AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
  });
var s3 = new AWS.S3();
 
var uploads3 = multer({
  storage: multerS3({
    s3: s3,
    bucket: config.BucketName,
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, "profile_cimon/" + Date.now().toString() + "-" + file.originalname)
    }
  })
})
const router = express.Router();

router.post('/uploadFeaturedImage/:token', uploads3.array('file', 70), function(req, res, next) { 
    const token = req.params.token;
      if (!token) {
        res.send({
          status: 0,
          message: "invalid request",
        });
      } 
     res.send({
                    status: 1,
                    message: "File uploaded",
                    data: { file_location: req.files[0].location , file_name: req.files[0].originalname },
                  });
    
}) 
router.post("/uploadFeaturedImage-old/:token", async (req, res) => {
  const token = req.params.token;
  if (!token) {
    res.send({
      status: 0,
      message: "invalid request",
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        await uploadSingle(req, res, async function (err) {
          if (err instanceof multer.MulterError) {
            res.send({
              status: 0,
              message:
                "Oops! something went wrong with uploading profile image.",
            });
          } else if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            try {
              await awsUploadSingle(req, (result) => {
                if (result.message) {
                  //error
                  return res
                    .status(500)
                    .json({ status: 0, message: result.message });
                } else {
                  //sucess
                  fs.unlinkSync(url + req.file.filename);
                  res.send({
                    status: 1,
                    message: "File uploaded",
                    data: { file_location: result , file_name: req.file.filename },
                  });
                }
              });
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } 
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message,
      });
    }
  }
});


function awsUploadSingle(req, callback) {
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
  });
  var s3 = new AWS.S3();
  var filePath = url + req.file.filename + "";

  //configuring parameters
  var params = {
    Bucket: config.BucketName,
    Body: fs.createReadStream(filePath),
    Key: "profile_cimon/" + Date.now() + "_" + req.file.filename,
  };
  s3.upload(params, function (err, data) {
    //handle error
    if (err) {
      callback(err);
    }
    //success
    if (data) {
      callback(data.Location);
    }
  });
}


// Multiple file uplaod
router.post('/uploadData/:token', uploads3.array('file', 70), function(req, res, next) { 
    const token = req.params.token;
    console.log('uploadDatamain');
      if (!token) {
        res.send({
          status: 0,
          message: "invalid request",
        });
      }
    const newArray = req.files.map(item => {
      return { Location: item.location,name: item.originalname };
    });
     
    res.send({
                    status: 1,
                    message: "File uploaded",
                    data: { file_location: newArray },
                  });
}) 

router.post("/uploadData-s/:token", async (req, res) => {

  console.log('uploadData-s');
  const token = req.params.token;
  if (!token) {
    res.send({
      status: 0,
      message: "invalid request",
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        await upload(req, res, async function (err) {
          if (err instanceof multer.MulterError) {
            res.send({
              status: 0,
              message:
                "Oops! something went wrong with uploading profile image.",
            });
          } else if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            try {
              await awsUpload(req, (result) => {
                if (result.message) {
                  return res
                    .status(500)
                    .json({ status: 0, message: result.message });
                } else {
                  //sucess
                  req.files.map((item) => {
                    if(url + item.filename){
                      fs.unlinkSync(url + item.filename);
                    }
                  })
                  res.send({
                    status: 1,
                    message: "File uploaded",
                    data: { file_location: result },
                  });
                }
              });
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            }
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message,
      });
    }
  }
});

function awsUpload(req, callback) {
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
  });
  var s3 = new AWS.S3();

  const file = req.files;
  var ResponseData = [];
  file.map((item) => {
    var filePath = url + item.filename + "";
    var params = {
      Bucket: config.BucketName,
      Body: fs.createReadStream(filePath),
      Key: "profile_cimon/" + Date.now() + "_" + item.filename,
    };
    s3.upload(params, function (err, data) {
      //handle error
      if (err) {
        callback(err);
      }
      //success
      if (data) {
        ResponseData.push(data);
        if(ResponseData.length == file.length){
          callback(ResponseData);
        } 
      }
    });
  });
}


module.exports = router;
