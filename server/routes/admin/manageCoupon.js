//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const mongoose = require('mongoose');
//Include schema
const Coupons = require("../../models/coupons");
const Users = require("../../models/users");
const config = require("../../config/config.json");
const Validate = require("../validations/validate");

const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();


router.post("/getAllCoupons", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Coupons.find({},
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 1,
                message: "Fetched Coupons Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "No Coupons available."
              });
            }

          }
        }
      ).sort({
        _id: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/createCoupon", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      Validate.AddCoupon(req.body.data, (result) => {
        if (result.error) {
          //validation error
          res.status(200).send({
            status: 2,
            message: result.error.details[0].message
          })

        } else {

          Coupons.find({
              slug: req.body.data.couponcode.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '')
            },
            async function (err, log) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
              } else {
                if (log.length > 0) {
                  res.send({
                    status: 0,
                    message: "This coupon already exists."
                  });

                } else {
                  const newCoupon = new Coupons({
                    couponcode: req.body.data.couponcode,
                    slug: req.body.data.couponcode.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, ''),
                    amount: req.body.data.amount,
                    discounttype: req.body.data.discounttype,
                    exiprydate: req.body.data.exiprydate,
                    products: req.body.data.products,
                    users: req.body.data.users,
                    couponstatus: req.body.data.couponstatus,
                    guestusers: req.body.data.guestusers,
                  });
                  await newCoupon.save()
                    .then(() => {
                      res.send({
                        status: 1,
                        message: "You have successfully created a coupon.",
                      });
                    })
                    .catch((err) => {
                      res.send({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                      });
                    });

                }

              }
            }
          ).sort({
            //created_on: 1
          });
        }
      });

    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/getAllUsersIdsandNames", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Users.find({},
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 1,
                message: "Fetched Products successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "No Product available."
              });
            }
          }
        }
      ).sort({
        date: -1
      }).select('email _id');
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/removeCoupon", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Coupons.findByIdAndDelete({
        _id: req.body.data._id
      }, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Your coupon has been deleted successfully."
            });
          } else {
            res.send({
              status: 1,
              message: "Something went wrong"
            });
          }
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});




router.post("/getCouponById", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Coupons.findOne({
          _id: req.body.data.id
        },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if (log) {
              res.send({
                status: 1,
                message: "Fetched Contact Address Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      ).sort({
        // created_on: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/updateCoupon", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {

      payload = jwt.verify(token, jwtKey);

      Coupons.find({
        slug: req.body.data.couponcode.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, ''),
        _id: { $ne: mongoose.Types.ObjectId(req.body.data.id) }
      },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This coupon already exists."
            });

          } else {

            var condition = {
              _id: req.body.data.id
            };
            var update_value = {
              couponcode: req.body.data.couponcode,
              slug: req.body.data.couponcode.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, ''),
              amount: req.body.data.amount,
              discounttype: req.body.data.discounttype,
              exiprydate: req.body.data.exiprydate,
              products: req.body.data.products,
              users: req.body.data.users,
              couponstatus: req.body.data.couponstatus,
              guestusers: req.body.data.guestusers,
            };
            Coupons.findOneAndUpdate(condition, update_value, async function (
              err,
              log
            ) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
              } else {
                res.send({
                  status: 1,
                  message: "You have updated the coupon successfully."
                });
              }
            })
          }
        }
      });
    
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});




module.exports = router;