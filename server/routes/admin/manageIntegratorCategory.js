//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const mongoose = require('mongoose');
//Include schema
const Integrator = require("../../models/categoryIntegrator");
const config = require("../../config/config.json");
const Validate = require("../validations/validate");

const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();




/* category list */


router.post("/getAllCategoryOptions", async (req, res) => {
  console.log('getAllCategoryOptions');
  const token = req.body.token;
  try {
   
    Integrator.find({},
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Distributor Successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Distributor available."
            });
          }

        }
      }
    ).sort({
      title: -1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});


router.post("/getAllIntegrator", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Integrator.find({},
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 1,
                message: "Fetched Integrator Category  Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "No Integrator Category  available."
              });
            }

          }
        }
      ).sort({
        _id: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/createIntegrator", async (req, res) => {
  const token = req.body.token;
    console.log('enter this section');
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      Integrator.find({
        title: req.body.data.title
      },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This Integrator already exists."
            });

          } else {
            

            const newIntegrator = new Integrator({
              title: req.body.data.title,
            //  slug : req.body.data.title.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, ''),
            });

          
            await newIntegrator.save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have successfully created a Integrator.",
                });
              })
              .catch((err) => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
              });

          }

        }
      }
    ).sort({
      //created_on: 1
    });

    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


 


router.post("/removeIntegrator", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Integrator.findByIdAndDelete({
        _id: req.body.data._id
      }, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Your Integrator Category  has been deleted successfully."
            });
          } else {
            res.send({
              status: 1,
              message: "Something went wrong"
            });
          }
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});




router.post("/getIntegratorById", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Integrator.findOne({
          _id: req.body.data.id
        },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if (log) {
              res.send({
                status: 1,
                message: "Fetched Integrator Category  Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      ).sort({
        // created_on: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/updateIntegrator", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {

      payload = jwt.verify(token, jwtKey);

      Integrator.find({
        title: req.body.data.title,
        _id: { $ne: mongoose.Types.ObjectId(req.body.data.id) }
      },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This Integrator Category  already exists."
            });

          } else {

            var condition = {
              _id: req.body.data.id
            };
            var update_value = {
              title: req.body.data.title,
               
            };
            Integrator.findOneAndUpdate(condition, update_value, async function (
              err,
              log
            ) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
              } else {
                res.send({
                  status: 1,
                  message: "You have updated the Integrator Category  successfully."
                });
              }
            })
          }
        }
      });
    
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});




module.exports = router;