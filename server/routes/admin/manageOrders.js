//load dependencies
const SECRET_KEY    = process.env.SECRET_KEY;
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const fs = require("fs");
const PDFDocument = require("pdfkit");
const path = require("path");
const mustache = require("mustache");
const sgMail = require("@sendgrid/mail");
//Include schema
const Orders = require("../../models/orders");
const Ordersnotes = require("../../models/order_notes");
const config = require("../../config/config.json");
const AWS = require("aws-sdk");
const { v4: uuidv4 } = require('uuid');
const stripe = require('stripe')(SECRET_KEY);
const jwtKey = config.jwsPrivateKey;
var async = require('async');


const OrderNotes = require("../../models/order_notes");
const Products = require("../../models/products");
const ShippingMethod = require("../../models/shipping_method");
const ShippingSettings = require("../../models/shipping_settings");
var utils = require('../../config/utils');
//http request port set
const router = express.Router();
const url = config.uploadURL;
const sendGridKey = config.sendGridKey;
const upsCOnfig   =    config.ups;


router.post("/createOrder", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
      try {
        payload = jwt.verify(token, jwtKey);
      
        let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
        const newOrder = new Orders({
          user_id : '5ed4eb9041dafd182ccf5d75',
          created_on: createdon,
          status : 'completed',
          order_details : {
            order_items : {
              product_id: 'ddd'
            },
            tax : {
              name: 'ssss'
            },
            shipping : {
              method : 'Flat Shipping Rate',
              type : 'Internet Banking'
            },
            total :{
              tax_total  : '500',
              shipping_total: '700',
              total: '1300'
            }
          },
          payment_details:{
            id : '123455',
            otherdetails :{
              method : 'stripe',
              card : 'visa' 
            }
          },
          shipping_details : {
            first_name: 'elvee',
            last_name: 't',
            email: 'elvee@webeteer.com',
            company: 'webeteer.com',
            address:{
              line1: 'testline',
              appartment: 'test apartment',
              city:   'city',
              country:'country',
              state: 'state',
              zipcode: 'zipcode',
              phone:'9567795262',
            } 
          }
        });
        await newOrder.save() 
        .then(() => {
          res.send({
            status: 1,
            message: "You have created a order to the Cimon",
          });
        }) 
        .catch((err) => {
          res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
          });
        });
  
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/getAllOrders", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {
    payload = jwt.verify(token, jwtKey);
    Orders.find(
        {  },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if(log.length>0){
              res.send({
                status: 1,
                message: "Fetched Orders Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "No Orders available."
              });
            }
          }
        }
      ).sort({ order_no: -1 }).select('page_status _id order_no created_on status order_details shipping_details');
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/trashOrder", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        var condition = { _id: req.body.order_id };
        var update_value = { page_status: 0 };
        Orders.findByIdAndUpdate(condition, update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            res.send({
              status: 1,
              message: "Order Trashed",
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});
router.post("/movetoPublish", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        var condition = { _id: req.body.order_id };
        var update_value = { page_status:1 };
        Orders.findByIdAndUpdate(condition, update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            res.send({
              status: 1,
              message: "Order Published",
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});



router.post("/actionHandlerOrder", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      
      var update_value = {  
        page_status: req.body.action,
      //  status : req.body.status
      };
 
      try {
          const  condition = { _id: req.body.orderids };
          Orders.updateMany(condition, update_value, async function (
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "Orders Updated successfully"   });
            }
          })

    } catch (e) {
         res.send({ status: 1, message: "Something went wrong" +e });
    }



    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
}); 



router.post("/actionTrashHandlerOrder", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      

      if(req.body.action == 'untrash'){
        var update_value = {  
          page_status: 1,
        };
        try {
                const  condition = { _id: req.body.orderids };
                Orders.updateMany(condition, update_value, async function (
                  err,
                  log
                ) {
                  if (err) {
                    res.send({ status: 1, message: "Something went wrong" + err });
                  } else {
                    res.send({ status: 1, message: "Orders Updated successfully" });
                  }
                })

          } catch (e) {
                res.send({ status: 1, message: "Something went wrong" +e });
          }




      } else if(req.body.action == 'delete'){

    try {
          const  condition = { _id: req.body.orderids };
          Orders.deleteMany(condition, async function (
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "Orders Updated successfully" });
            }
          })

    } catch (e) {
          res.send({ status: 1, message: "Something went wrong" +e });
    }


      } else {
        res.send({ status: 1, message: "Something went wrong" });
      }

     
 



    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
}); 


router.post("/deleteOrder", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Orders.findByIdAndDelete({ _id: req.body.order_id }, async function (
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Order deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});


router.post("/updateOrder", async (req, res) => {
    const token = req.body.token;
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
      try {
        payload = jwt.verify(token, jwtKey);
        var condition = { _id: req.body._id };

        
        // var update_value = { 
        //   status: req.body.data.status, 
        //   created_on: req.body.data.created_on,
        //   shipping_details : req.body.data.shipping_details
        // };

        
        // res.send({ status: 0, message: update_value });
        
        Orders.findOneAndUpdate(condition, 
          
          { 
            $set: { 
              "status" : req.body.data.status,
              "created_on": req.body.data.created_on,
              "shipping_details.zipcode": req.body.data.zipcode
            },
        }
          
          , async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            res.send({ status: 1, message: "Order Updated successfully" });
          }
        })
      } catch (e) {
        if (e instanceof jwt.JsonWebTokenError) {
          res.send({ status: 0, message: "Token verification failed." });
        }
        res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
        });
      }
    }
  });


function getRefundvalue(refundarray) {
  let refundvalue = 0
  if (refundarray.length > 0) {
    refundarray.map(
      (singlerefund, idx) =>
        (refundvalue = refundvalue + (singlerefund.other.amount * 1) / 100)
    )
  }
  return refundvalue
}


function getshippingRefundvalue(refundarray) {
  let refunded_shipping_amt = 0
  if (refundarray.length > 0) {
    refundarray.map(
      (singlerefund, idx) =>
      (refunded_shipping_amt = refunded_shipping_amt + (singlerefund.shipping * 1))
    )
  }
  return refunded_shipping_amt
}

function gettaxRefundvalue(refundarray) {
  let refunded_tax_amt = 0
  if (refundarray.length > 0) {
    refundarray.map(
      (singlerefund, idx) =>
      (refunded_tax_amt = refunded_tax_amt + (singlerefund.tax * 1)),
    )
  }
  return refunded_tax_amt
}



function getRemovedItemsfunction (refundarray){
  let Removeditems = [];
  let removedproducts = [];
  if(refundarray.length){
    refundarray.map(
      (singlerefund, idx) =>
      Removeditems.push(singlerefund)
    )
  }
   
   removedproducts = getRemovedItems(Removeditems);
   return removedproducts;
}


function getRemovedItems  (Removeditems){
  
  const data_table_row = [];
  if(Removeditems.length > 0){
  var holder = {};
    Removeditems.forEach(function(valuesare) {
      if(valuesare){
      if(valuesare.removedstock){
        valuesare.removedstock.forEach(function(removedstocksingle) {
          if((removedstocksingle.count * 1) > 0){
            if(removedstocksingle._id != '' ){
              if (holder.hasOwnProperty(removedstocksingle._id)) {
                holder[removedstocksingle._id] = holder[removedstocksingle._id] + removedstocksingle.count * 1;
              } else {

                holder[removedstocksingle._id] = removedstocksingle.count *1;
              }
            }
          }
        
        })
      }

    }
      // }
      
    })
  }
  
  var obj2 = [];
       if(holder){
          for (var prop in holder) {
            obj2.push({ _id : prop, count: holder[prop] });
          }
       }
  
     return obj2;
      
    
    
      
  }

  function getQunatity(Removedproducts , productid){
 
    let  rowData =  Removedproducts.filter(l => {
      return (
        l._id.match(productid)  
      )
    });

    if(rowData.length > 0) {
      return (rowData[0].count ? rowData[0].count : 0)
    } else {
      return 0;
    }
    
  }


  router.post("/generateInvoice", async (req, res) => {
    const token = req.body.token;
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
    try {
      payload = jwt.verify(token, jwtKey);
     
   

      let refundamount = 0
      let Removedproducts = [];
      
      refundamount = getRefundvalue(req.body.data.order_details.refund);

      refundshipping_amt= getshippingRefundvalue(req.body.data.order_details.refund);

     

      refundtax_amt= gettaxRefundvalue(req.body.data.order_details.refund);


      Removedproducts  =  getRemovedItemsfunction(req.body.data.order_details.refund);


     


      let orderid = req.body.data._id;
      let invoiceid = req.body.data.order_no ? req.body.data.order_no : req.body.data._id;
      itemsarray= [];
      refundarray = [];
      
      req.body.data.order_details.product.forEach(function(item){

        let count =  getQunatity(Removedproducts ,item.product_id);
      

      
        const Setdata = {};
        const Setrefunddata = {};
        Setdata.item =  item.product_name
        Setdata.description = 'Note that in order to Browserify a project using PDFKit, you need to install the brfs ... The first page of a PDFKit document is added for you automatically when you';
        Setdata.quantity = (item.quantity)
        Setdata.amount = item.line_item_total
        itemsarray.push(Setdata);
        if(count > 0){  // refund products array


           
          Setrefunddata.item =  item.product_name
          Setrefunddata.quantity = (count *1)
          Setrefunddata.amount = (item.price *  (count *1))
          refundarray.push(Setrefunddata);

        }
       
      });


      
      
      let TrackingNumber = '';
      if (req.body.data.hasOwnProperty('shipmentresults')) {
        req.body.data.shipmentresults.PackageResults.PackageResults.forEach(function (item) {
          TrackingNumber = TrackingNumber + item.TrackingNumber[0] + ', '
        }); 
      }


      



      const invoice = {
        otherdetails :req.body.data.shipping_details, 
        shipping: {
          name:  req.body.data.shipping_details.email,
          address: req.body.data.shipping_details.appartment,
          city: req.body.data.shipping_details.city,
          state: req.body.data.shipping_details.region,
          country: req.body.data.shipping_details.country,
          postal_code: req.body.data.shipping_details.zipcode
        },
        TrackingNumber : TrackingNumber, 
        items:  itemsarray,
        refundarray : refundarray,
        subtotal: req.body.data.order_details.total.product_total ? req.body.data.order_details.total.product_total : 0,
        shipping_charge : req.body.data.order_details.total.shipping_total ? req.body.data.order_details.total.shipping_total : 0,
        tax_charge : req.body.data.order_details.total.tax_total ? req.body.data.order_details.total.tax_total : 0,
        coupon : req.body.data.order_details.total.discount ? req.body.data.order_details.total.discount : 0, 
        paid: refundamount ? refundamount : 0,
        date : req.body.data.created_on,
        refund_shipping : refundshipping_amt ? refundshipping_amt : 0,
        refund_tax : refundtax_amt ? refundtax_amt : 0,
        invoice_nr: invoiceid ,
        total : req.body.data.order_details.total.total ? req.body.data.order_details.total.total : 0,
      };

    
      let returnvalue = createInvoice(invoice, url ,invoiceid);
      

     
      try {
        awsUploadEditor(returnvalue ,orderid, (result) => {
         if (result.message) {
           
           //error
           res.send({ status: 0, url : '' });
         } else {
           //sucess
           try {
            var condition = { _id: orderid };
            var update_value = { invoice_url: result };
            Orders.findByIdAndUpdate(condition, update_value, async function (
            err,
            log
            ) {
            if (err) {
              res.send({ status: 0, url : '' });
            } else {
             
              sendInvoiceMail(req.body.data.shipping_details , result , invoiceid, returnvalue, (results) => {
                if (results == "success") {
                  res.send({ status: 1,  mail : 'success' , url : result});
                } else if (results == "failed") {
                  res.send({ status: 1, mail : 'failed' , url : result});
                }
              });
              
            }
            });
            } catch (err) {
              res.send({ status: 0, url : '' });
            }
          
         }
       });
     } catch (err) {
      
       res.send({ status: 0, url : '' });
     }

     
      
      } catch (e) {
        res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
        });
      }
    }
  });
  
  function awsUploadEditor(returnvalue ,orderid, callback) {
    AWS.config.update({
      accessKeyId: config.accessKeyId,
      secretAccessKey: config.secretAccessKey,
    });
    var s3 = new AWS.S3();
   // var filePath = url + req.file.filename + "";
    var d = new Date();
    
    //configuring parameters
    var params = {
      Bucket: config.BucketName,
      Body: returnvalue,
      ContentEncoding: "buffer",
      ContentType: "application/pdf",
      Key: "invoice/"   + orderid,
    };
    s3.upload(params, function (err, data) {
      //handle error
      if (err) {
        
        callback(err);
      }
      //success
      if (data) {
        
        callback(data.Location);
      }
    });
  }

  function createInvoice(invoice, paths ,orderid) {
    
    let doc = new PDFDocument({ margin: 50 });
  
    generateHeader(doc);
    generateCustomerInformation(doc, invoice , orderid);
    generateInvoiceTable(doc, invoice);    
    doc.pipe(fs.createWriteStream(path.join(process.cwd() + '/uploads/invoice/'+orderid+'.pdf')) , {flags: 'w'});
    doc.end();
  
    return doc;
  }

  function generateHeader(doc) {
    doc
      .image(path.join(process.cwd() + '/uploads/logo.png'), 50, 45, { width: 100  }).fill('#6600FF')
      .fillColor("#000000")
      .fontSize(20)
      .text("Cimon Inc.", 200, 40 ,{ align: "right" })
      .fontSize(10)
      .text("2435 W Horizon Ridge Pkwy,", 200, 65, { align: "right" })
      .text("#100 Henderson, NV 89052", 200, 80, { align: "right" })
      .moveDown();
  }
  
  function generateFooter(doc) {
    doc
      .fontSize(10)
      .text(
        "Payment is due within 15 days. Thank you for your business.",
        50,
        600,
        { align: "center", width: 500 }
      );
  }

  function generateHr(doc, y) {
    doc
      .strokeColor("#aaaaaa")
      .lineWidth(1)
      .moveTo(50, y)
      .lineTo(550, y)
      .stroke();
  }
  function formatCurrency(cents) {
    return "$" + (cents).toFixed(2);
  }
  
  function formatDate(date) {
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
  
    return day + "/" + month + "/" + year;
  }


  function generateCustomerInformation(doc, invoice , orderid) {
    doc
      .fillColor("#444444")
      .fontSize(20)
      .text("Invoice", 50, 160);
  
    generateHr(doc, 185);
  
    const customerInformationTop = 200;
  
    doc
      .fontSize(10)
      .text(`Invoice Number: `, 50, customerInformationTop)
      .font("Helvetica-Bold")
      .text(invoice.invoice_nr, 150, customerInformationTop)
      .font("Helvetica")
      .text("Invoice Date:", 50, customerInformationTop + 15)
      .text(formatDate(new Date()), 150, customerInformationTop + 15)
      .text("Balance Due:", 50, customerInformationTop + 30)
      .text(
        formatCurrency(invoice.total - invoice.paid),
        150,
        customerInformationTop + 30
      )
  
      .font("Helvetica-Bold")
      .text(invoice.shipping.name, 300, customerInformationTop)
      .font("Helvetica")
      //.text(invoice.shipping.address, 300, customerInformationTop + 15)
      // .text(
      //   invoice.shipping.city +
      //     ", " +
      //     invoice.shipping.state +
      //     ", " +
      //     invoice.shipping.country,
      //   300,
      //   customerInformationTop + 30
      // )
      .text( 'Tracking Number : ' + invoice.TrackingNumber, 300, customerInformationTop + 15)
      .moveDown();
  
    generateHr(doc, customerInformationTop + 50);
  }

  function generateInvoiceTable(doc, invoice) {
    let i;
    const invoiceTableTop = 280;
  
    doc.font("Helvetica-Bold");
    generateTableRow(
      doc,
      invoiceTableTop,
      "Item",
      "Description",
      "Unit Cost",
      "Quantity",
      "Line Total"
    );
    generateHr(doc, invoiceTableTop + 20);
    doc.font("Helvetica");
  
    for (i = 0; i < invoice.items.length; i++) {
      const item = invoice.items[i];
      const position = invoiceTableTop + (i + 1) * 30;

      if (position > 630){ doc.addPage(); position = invoiceTableTop + (i + 1) * 30; }

      generateTableRow(
        doc,
        position,
        item.item,
        item.description,
        formatCurrency(item.amount / item.quantity),
        item.quantity,
        formatCurrency(item.amount)
      );
  
      generateHr(doc, position + 20);
    }


    const subtotalPosition = invoiceTableTop + (i + 1) * 30;

   let  paidToDatePosition =  getNewpageorNot(doc,subtotalPosition);
    generateTableRow(
      doc,
      paidToDatePosition,
      "",
      "",
      "Shipping",
      "",
      formatCurrency(invoice.shipping_charge),
    0
    );

    paidToDatePosition =  getNewpageorNot(doc,paidToDatePosition + 20);
    generateTableRow(
      doc,
      paidToDatePosition,
      "",
      "",
      "Tax",
      "",
      formatCurrency(invoice.tax_charge),
    0
    );

     

    paidToDatePosition =  getNewpageorNot(doc,paidToDatePosition + 20);
  
    generateTableRow(
      doc,
      paidToDatePosition,
      "",
      "",
      "Subtotal",
      "",
      formatCurrency(invoice.subtotal) ,
      0
    );



    
   // generateRefundTable(doc ,invoice ,position + 20);
/**************** refund section **************** */

let newRefundTableTop =   getNewpageorNot(doc,paidToDatePosition+10)

if(invoice.refundarray.length){


   newRefundTableTop =   getNewpageorNot(doc,newRefundTableTop + 30)
   doc.font("Helvetica-Bold");
   generateTableRow(
     doc,
     newRefundTableTop,
     "Refund",
     "",
     "",
     "",
     "",
     1
   );
   generateHr(doc, newRefundTableTop = getNewpageorNot(doc,newRefundTableTop + 20));
   doc.font("Helvetica");

   for (i = 0; i < invoice.refundarray.length; i++) {
    const item = invoice.refundarray[i];
    newRefundTableTop =  getNewpageorNot(doc,newRefundTableTop + 15);
//    let positionnew  =  getNewpageorNot(doc ,positionnew);
    generateTableRow(
      doc,
      newRefundTableTop,
      item.item,
      item.description,
      formatCurrency(item.amount / item.quantity),
      item.quantity,
      formatCurrency(item.amount),
      1
    );
    generateHr(doc, newRefundTableTop =   getNewpageorNot(doc,newRefundTableTop + 20));
   }

  }  

/**************** End refund section **************** */
 
paidToDatePosition =  getNewpageorNot(doc,newRefundTableTop + 20);

if(invoice.refund_shipping ) {
    generateTableRow(
      doc,
      paidToDatePosition,
      "",
      "",
      "Shipping",
      "",
      formatCurrency(invoice.refund_shipping),
    1
    ); 
}

    if(invoice.refund_tax ) {
    paidToDatePosition =  getNewpageorNot(doc,paidToDatePosition + 20);
    generateTableRow(
      doc,
      paidToDatePosition,
      "",
      "",
      "Tax",
      "",
      formatCurrency(invoice.refund_tax),
    1
    );
    }



    if(invoice.coupon ) {
      paidToDatePosition =  getNewpageorNot(doc,paidToDatePosition + 20);
    generateTableRow(
      doc,
      paidToDatePosition,
      "",
      "",
      "Coupon",
      "",
      formatCurrency(invoice.coupon),
    1
    );
    }
    



     
    // if(invoice.paid ) {
    // paidToDatePosition =  getNewpageorNot(doc, paidToDatePosition+ 20);
    // generateTableRow(
    //   doc,
    //   paidToDatePosition,
    //   "",
    //   "",
    //   "Refund",
    //   "",
    //   formatCurrency(invoice.paid),
    // 1
    // );
    // }

  
    const duePosition =  getNewpageorNot(doc,paidToDatePosition + 20);
    doc.font("Helvetica-Bold");
    generateTableRow(
      doc,
      duePosition,
      "",
      "",
      "Balance",
      "",
      formatCurrency(invoice.total - invoice.paid),
      0
    );
    doc.font("Helvetica");

   generateInvoicebillingaddress(doc, invoice ,duePosition);

  }



  function getNewpageorNot(doc ,duePosition) {
    if (duePosition > 700){ 
      doc.addPage(); duePosition = 20;
    }  
     return duePosition;
  }

  function generateInvoicebillingaddress(doc, invoice ,duePosition) {
    
    duePosition = getNewpageorNot(doc ,duePosition);

  
    
    // if (duePosition > 600){ doc.addPage(); duePosition = 0; }
 
    const paidToDatePositionnew =  doc
    .fillColor("#444444")
    .fontSize(15)
    .text("Billing address", 50,  duePosition =  getNewpageorNot(doc ,duePosition+80));

    let hrpos = getNewpageorNot(doc ,duePosition+20);
    generateHr(doc,  hrpos);
     let customerInformationTop  = hrpos;
    doc
    .fontSize(10)
    .text(invoice.otherdetails.first_name + ' '+ invoice.otherdetails.last_name + ',', 50, customerInformationTop =   getNewpageorNot(doc ,customerInformationTop+15))
    .font("Helvetica")
    doc.text(invoice.otherdetails.address, 50, customerInformationTop = getNewpageorNot(doc ,customerInformationTop+15))
  
    doc.text(invoice.otherdetails.appartment, 50, customerInformationTop = getNewpageorNot(doc ,customerInformationTop+15))

    doc.text(invoice.otherdetails.city, 50, customerInformationTop = getNewpageorNot(doc ,customerInformationTop+15))
  
    doc.text(invoice.otherdetails.region, 50, customerInformationTop =  getNewpageorNot(doc ,customerInformationTop+15))
    
    doc.text(invoice.otherdetails.country, 50, customerInformationTop =  getNewpageorNot(doc ,customerInformationTop+15))
  
    doc.text(invoice.otherdetails.zipcode, 50,customerInformationTop = getNewpageorNot(doc ,customerInformationTop+15))
 
    doc.text(invoice.otherdetails.phone,50,customerInformationTop =  getNewpageorNot(doc ,customerInformationTop+15))

    doc.text(invoice.otherdetails.email,50,customerInformationTop =  getNewpageorNot(doc ,customerInformationTop+15))
  }

  function generateTableRow(doc, y, c1, c2, c3, c4, c5 , refund) {

    if(refund) {
      doc
      .fontSize(10).fillColor('red')
      .text(c1, 50, y  ,{ width: 150,  display: "inline-block","word-break": "break-all"})
    //  .text(c2, 150, y +10 ,{ width: 90,  display: "inline-block","word-break": "break-all"})
      .text(c3, 280, y, { width: 90, align: "right" , display: "inline-block","word-break": "break-all"})
      .text(c4, 370, y, { width: 90, align: "right" ,    display: "inline-block","word-break": "break-all" })
      .text(c5, 0, y, { align: "right" });
    
    } else {
      doc
      .fontSize(10).fillColor('black')
      .text(c1, 50, y  ,{ width: 150,  display: "inline-block","word-break": "break-all"})
    //  .text(c2, 150, y +10 ,{ width: 90,  display: "inline-block","word-break": "break-all"})
      .text(c3, 280, y, { width: 90, align: "right" , display: "inline-block","word-break": "break-all"})
      .text(c4, 370, y, { width: 90, align: "right" ,    display: "inline-block","word-break": "break-all" })
      .text(c5, 0, y, { align: "right" });
    }
  
  }

router.post("/getCurrentOders", async (req, res) => {
    const token = req.body.token;
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
    try {
   
      payload = jwt.verify(token, jwtKey);
      Orders.find(
        { _id: req.body.data },
          async function (err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {
              if(log.length>0){
  
                res.send({
                  status: 1,
                  message: "Fetched Orders Successfully.",
                  data: log
                });
              }else{
                res.send({
                  status: 0,
                  message: "No Orders available."
                });
              }
  
            }
          }
        ).sort({ name: -1 });
      } catch (e) {
        res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
        });
      }
    }
  });

  router.post("/getCurrentOdersnotes", async (req, res) => {
      const token = req.body.token;
      if (!token) {
        res.send({ status: 0, message: "Invalid Request" });
      } else {
      try {
     
        payload = jwt.verify(token, jwtKey);
        Ordersnotes.find(
          { order_id: req.body.data },
            async function (err, log) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
              } else {
                if(log.length>0){
    
                  res.send({
                    status: 1,
                    message: "Fetched Orders Successfully.",
                    data: log
                  });
                }else{
                  res.send({
                    status: 0,
                    message: "No Orders available."
                  });
                }
    
              }
            }
          ).sort({ created_on: -1 });
        } catch (e) {
          res.send({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
          });
        }
      }
    });




    router.post("/refundOrder", async (req, res) => {
        const token = req.body.token;
     
      
        if (!token) {
          res.send({ status: 0, message: "Invalid Request" });
        } else {
        try {
        const idempotencyKey    =   uuidv4();  
        const stripenew = require('stripe')(process.env.SECRET_KEY);  
        const amount = req.body.data.refund_amount *100;
        const refund = await stripenew.refunds.create({
          amount: amount,
          payment_intent: req.body.data.payment_id,
     //     reason :  req.body.data.refund_note 
        } );
        
        if (refund) {
         
          if(refund.status == "succeeded") {


           let returnvalue = updateOrderitems(req.body.data,refund ,req.body.data.orderid ,req.body.data.refund_amount ,req.body.data.removestock_products);
            
            
            var condition = { _id: req.body.data.orderid };
            var update_value = { status: 'Refund' }; 
            Orders.updateOne(condition, 
              
              { $push: {
                "order_details.refund":  
                 {data :req.body.data , order_id : req.body.data.orderid, amount : req.body.data.refund_amount , other  : refund ,removedstock :  req.body.data.removestock_products , shipping : req.body.data.shipping_refund_amount, tax : req.body.data.tax_refund_amount },
                
              } ,
              $set: {status: 'Refund'},
          }
              
              
              , async function (
              err,
              log
            ) {
              if (err) {
                res.send({
                          status: 0,
                     //     data :  {data :req.body.data , order_id : req.body.data.orderid, amount : req.body.data.refund_amount , other  : refund  },
                          message: "Something went wrong" + err,
                });
              } else {
               
                res.send({
                  status: 1,
               //   refund : log.order_details,
                  data :  {data :req.body.data , order_id : req.body.data.orderid, amount : req.body.data.refund_amount , other  : refund ,removedstock :  req.body.data.removestock_products , shipping : req.body.data.shipping_refund_amount, tax : req.body.data.tax_refund_amount },
                          message: "refund successful",
                });
              }
            });
          
              // callback({
              //     status: 0,
              //     mesaage:"Sorry! Failed to make a payment. Please try again",
              //     from: 'make payment'
              // }, null);
          } else {
            res.send({
                    status: 0,
                    message: "something went wronge" ,
                  });
          }
          
      }



          } catch (e) {
            res.send({
              status: 0,
              message: "Oops! " + e.name + ": " + e.message,
            });
          }
        }
      });



      
router.post("/getRefundArray", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {
    
      Orders.find(
            { _id: req.body.data },
              async function (err, log) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                  });
                } else {
                  if(log.length>0){
      
                    res.send({
                      status: 1,
                      message: "Fetched Orders Successfully.",
                      data: log[0].order_details.refund
                    });
                  }else{
                    res.send({
                      status: 0,
                      message: "No Orders available."
                    });
                  }
      
                }
              }
            ).sort({ name: -1 });



    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/shippingConfirm", async (req, res) => {
  const token = req.body.token;
  order_id =  req.body.data;
   
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {

    
    async.waterfall([    
      getUPSConfig, 
      getOrderbyId, 
      upsAddressValidation, 
      getShipmentOrigin,  
      getconfirmShipment,
      getshipmentaccept,
      updateOrder,
      sendShipConfirmMail

  ], function (error, results) {
      if(error) {
          res.send(error);
      } else {
        
          res.send({
              status: 1,
              message: "Success",
              order: results
          });
      }
  });
    
    
    
    
    }
  
});



function updateOrder(results, callback) {
   
  
  try {
    var condition = { _id: order_id };
    var update_value = { 
      shipping_digest : results.shipmentConfirm[0] ,
      shipmentresults : results.packageresults[0] };
    Orders.findByIdAndUpdate(condition, update_value, async function (
      err,
      log
    ) {
      if (err) {
          callback({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
            from: 'save order catch'
          }, null);

      } else {
     
        results.order = log;            
        callback(null, results);
      }
    });
  } catch (err) {
    callback({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message,
      from: 'save order catch'
    }, null);
  }


}


function validateShipping( results, callback) {
  shipping = results.order.shipping_details;
 

  var validate = utils.validateShippingDetails(shipping);        
  if(validate.status === false) {
    
      callback({
          status: 2,                
          message: validate.errors,
          from: 'User data validation'
      },null);  
      return;          
  } else {
    
      callback(null, results);
  }
}

function getOrderbyId (results ,callback) {
   
  try{
    Orders.findOne({ _id : order_id} , 
          async function (err, response) {
              if (err ) {
                  callback({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message,
                      from: 'getOrderbyId'
                  },null);
              } 
              if(response) {
              
                  results.order = response;
                  callback(null, results);                     
              } else {
                  callback({
                      status: 0,
                      message: "Sorry no order found" ,
                      from: 'getOrderbyId'
                  }, null); 
              }
              
          }
      )
  } catch (e) {
      callback({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
          from: 'shippment_UPS_config'
      }, null);
  }
}
function getUPSConfig (callback) {
  
  const results = {};
  try{
      ShippingMethod.findOne({ slug : 'ups'} , 
          async function (err, response) {
              if (err ) {
                  callback({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message,
                      from: 'shippment_UPS_config'
                  },null);
              } 
              if(response) {
                  results.upsSettings = response;
                  callback(null, results);                     
              } else {
                  callback({
                      status: 0,
                      message: "Sorry no shipping methods found" ,
                      from: 'shippment_UPS_config'
                  }, null); 
              }
              
          }
      )
  } catch (e) {
      callback({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
          from: 'shippment_UPS_config'
      }, null);
  }
}


function upsAddressValidation(results, callback) {

  shipping = results.order.shipping_details;
  
  const { upsSettings } = results;

 
  const USE_SANDBOX = upsSettings.sandbox;
  const UPS_ACCESS_KEY = upsSettings.sandbox == true ? upsSettings.sandbox_access_key : upsSettings.access_key;
  const UPS_USER_ID = upsSettings.sandbox == true ? upsSettings.sandbox_username : upsSettings.username;
  const UPS_PASSWORD = upsSettings.sandbox == true ? upsSettings.sandbox_password : upsSettings.password;

  var AddressValidation = require('../../lib/addressValidation');
  var validateAddress = new AddressValidation(UPS_ACCESS_KEY, UPS_USER_ID, UPS_PASSWORD);
  validateAddress.useSandbox(USE_SANDBOX);
  validateAddress.setJsonResponse(true);

  validateAddress.makeRequest({
      CountryCode: shipping.country,
      customerContext: shipping.address,
      city: shipping.city,
      stateProvinceCode: shipping.region,
      PostalCode: shipping.zipcode
  }, function (result, data) {

      if (result) { 
         
          if (result.AddressValidationResponse.Response[0].ResponseStatusCode[0] == 1) {
              results.validAddress = {
                  status: result.AddressValidationResponse.Response[0].ResponseStatusCode[0],
                  message: result.AddressValidationResponse.Response[0].ResponseStatusDescription[0],
                  
                  result: result
              };
              callback(null, results);
          }
          else {
              callback({
                  status: result.AddressValidationResponse.Response[0].ResponseStatusCode[0],
                  message: result.AddressValidationResponse.Response[0].Error[0].ErrorDescription[0],                    
                  result: result
              }, null);
          }

      }
      else {
    
          callback({
              status: 0,
              message: "Please enter valid address"
          }, null);
      }

  });

}


function getShipmentOrigin (results, callback ) {

  try{
      ShippingSettings.findOne({},
          async function (err, response) {

              if (err ) {
                  callback({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message,
                      from: 'shippment_origin'
                  },null);
              } 
              if(response.length == 0) {
                  callback({
                      status: 0,
                      message: "Sorry no shipping methods found" ,
                      from: 'shippment_origin'
                  }, null); 
              }
              results.shipmentOrigin = response;
              callback(null, results); 
          }
      )
  } catch (e) {
      callback({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
          from: 'shippment_origin'
      }, null);
  }
}
 

function getconfirmShipment(results, callback) {
  
  const { upsSettings } = results;  
  if(upsSettings.carrier_services.length <= 0) {
      callback( {
          status: 0,
          message: "Sorry no shipping methods found",
          from: 'service empty'
      }, null)
  } else {
      try{
          results.carrier_services    =   [shipping.shipping_methods];
          confirmShipmentAndServices(results).then( upsresponse => {
            
              let shipment = [];
              let error = {};
              let isValid =  false; 
              upsresponse.forEach ( shipmentconfirm => {
                  
                  if(shipmentconfirm.ShipmentConfirmResponse.Response[0].ResponseStatusCode[0] == 1) {

                      isValid = true;
                      let code = shipmentconfirm.ShipmentConfirmResponse.Response[0];
                      let ShipmentCharges = shipmentconfirm.ShipmentConfirmResponse.ShipmentCharges;                    
                      let BillingWeight  = shipmentconfirm.ShipmentConfirmResponse.BillingWeight;
                      let ShipmentDigest = shipmentconfirm.ShipmentConfirmResponse.ShipmentDigest;
                      let ShipmentIdentificationNumber =  shipmentconfirm.ShipmentConfirmResponse.ShipmentIdentificationNumber
                      shipment.push ( {
                          code: code,
                          ShipmentCharges : ShipmentCharges,
                          BillingWeight: BillingWeight,
                          ShipmentDigest: ShipmentDigest,
                          ShipmentIdentificationNumber : ShipmentIdentificationNumber
                      })
                  } else {
                      error = {
                          status: shipmentconfirm.ShipmentConfirmResponse.Response[0].ResponseStatusCode[0],
                          message: shipmentconfirm.ShipmentConfirmResponse.Response[0].Error[0].ErrorDescription[0]
                      }
                  }
              })

              if(isValid) {
                  results.shipmentConfirm = shipment;
                 
                  callback( null, results);
              } else {
                  callback (error, null);
              }
          });
      } catch (err) {
          callback( {
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
              from: 'service empty'
          }, null)
      }
  }
}

async function confirmShipmentAndServices( results ) {

  const { upsSettings, shipmentOrigin } = results;
  shipping = results.order.shipping_details;
  shipping_method = results.order.shipping_method.service;

  
  

  Removedproducts =  getRemovedItemsfunction(results.order.order_details.refund); // refunded products

   
  product = results.order.user_id.ups_options ? results.order.ups_options.shipment.package : results.order.order_details.product ;
  pickUpType = results.order.ups_options ? results.order.ups_options.pickUpType : ''
  let packages = [];



 

  const USE_SANDBOX   =   upsSettings.sandbox;
  const UPS_ACCESS_KEY = (USE_SANDBOX == true)  ? upsSettings.sandbox_access_key : upsSettings.access_key;
  const UPS_ACCOUNT_NUMBER = (USE_SANDBOX == true)  ? upsSettings.sandbox_account_no : upsSettings.acount_no;
  const UPS_USER_ID    = (USE_SANDBOX == true)  ? upsSettings.sandbox_username : upsSettings.username;
  const UPS_PASSWORD    = (USE_SANDBOX == true)  ? upsSettings.sandbox_password : upsSettings.password;

  var ShipConfirm  = require('../../lib/shipConfirm');
  var confirmShipment  = new ShipConfirm(UPS_ACCESS_KEY, UPS_USER_ID, UPS_PASSWORD);
  confirmShipment.useSandbox(USE_SANDBOX);
  confirmShipment.setJsonResponse(true);

  var promiseArray = [];

  let servicecode = results.order.shipping_method.service ? results.order.shipping_method.service : 'Ground';
 
  for(let single of product) {

    let count =  getQunatity(Removedproducts ,single.product_id);
    


    for(i = 0; i < ((single.quantity *1) - count ); i++) {
    
    if(single.code == '01') {

        packages.push ( {
          description: single.product_name,
          code: single.code,
          weight: single.packages.weights,
          length: single.packages.lengths,
          width: single.packages.widths,
          height: single.packages.heights,
        });
  
    } else if(single.code == '02') {
        packages.push ( {
          description: single.product_name,
          code: single.code,
          weight: single.lineItemWeightTotal
        });
    } else {
      packages.push ( {
        description:  single.product_name,
        code: '02',
        weight: single.packages.weights
      });
    }
  }
   


  }

  

  let upsOptions = {
    validate: "nonvalidate",
      pickUpType: pickUpType,
      // CustomerClassification: {
      //     code: upsSettings.ups_customer_classification
      // },
      shipment: {
          name: upsCOnfig.name,
          description: "Shipment to Us",
          // phoneNumber: upsCOnfig.phone,
          // shipperNumber: UPS_ACCOUNT_NUMBER,
          shipper: {
              name: upsCOnfig.name,
              attentionName: upsCOnfig.name,
              shipperNumber: UPS_ACCOUNT_NUMBER,
              phone: upsCOnfig.phone,
                address: {
                    address1: shipmentOrigin.street_address,
                    address2: shipmentOrigin.street_address1,
                    address3 : '',
                    city: shipmentOrigin.city,
                    state: shipmentOrigin.region,
                    zip: shipmentOrigin.zipcode,
                    country: shipmentOrigin.country
                }
          },
          shipTo: {
                  companyName: shipping.company,
                  attentionName : shipping.first_name + ' ' + shipping.last_name,
                  phone: shipping.phone,
                    address: {
                      address1: shipping.address.trim(),
                      address2: shipping.appartment.trim(),
                      address3 : '',
                      city: shipping.city,
                      state: shipping.region,
                      zip: shipping.zipcode,
                      country: shipping.country
                    }
          },
                  
          payment : {
            accountNumber : UPS_ACCOUNT_NUMBER
          },
          service : {
            code : servicecode 
          },
          package :  packages
         // package: results.packages,
      }
  };


  

promiseArray.push(new Promise((resolve, reject) => {
    confirmShipment.makeRequest(upsOptions, function(response) {
        
        if (response) {
            resolve(response)
        }
    });
}))

 

 return (await Promise.all(promiseArray));

}







function getshipmentaccept(results, callback) {
 
  const { upsSettings } = results;  
  if(upsSettings.carrier_services.length <= 0) {
      callback( {
          status: 0,
          message: "Sorry no shipping methods found"
      }, null)
  } else {
      try{
          results.carrier_services    =   [shipping.shipping_methods];
          shipmentAcceptAndServices(results).then( response => {
            
                

                

                
           
           
              let shipment = [];
              let error = {};
              let isValid =  false; 
              response.forEach ( shipmentresults => {
                  
                  if(shipmentresults.ShipmentAcceptResponse.Response[0].ResponseStatusCode[0] == 1) {

                      isValid = true;
                      let code = shipmentresults.ShipmentAcceptResponse.Response[0];
                      let ShipmentCharges = shipmentresults.ShipmentAcceptResponse.ShipmentResults[0].ShipmentCharges[0];                    
                      let BillingWeight  = shipmentresults.ShipmentAcceptResponse.ShipmentResults[0].BillingWeight;
                      let PackageResults = shipmentresults.ShipmentAcceptResponse.ShipmentResults[0];
                      let ShipmentIdentificationNumber =  shipmentresults.ShipmentAcceptResponse.ShipmentResults[0].ShipmentIdentificationNumber
                      shipment.push ( {
                          code: code,
                          ShipmentCharges : ShipmentCharges,
                          BillingWeight: BillingWeight,
                          PackageResults: PackageResults,
                          ShipmentIdentificationNumber : ShipmentIdentificationNumber
                      })
                  } else {
                      error = {
                          status: shipmentconfirm.ShipmentAcceptResponse.Response[0].ResponseStatusCode[0],
                          message: shipmentconfirm.ShipmentAcceptResponse.Response[0].Error[0].ErrorDescription[0]
                      }
                  }
              })

              if(isValid) {
                  results.packageresults = shipment;
                  callback( null, results);
              } else {
                  callback (error, null);
              }
          });
      } catch (err) {
          callback( {
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
          }, null)
      }
  }
}

async function shipmentAcceptAndServices( results ) {

  const { upsSettings, shipmentOrigin } = results;
  ShipmentDigest = results.shipmentConfirm;
  

  
  const USE_SANDBOX   =   upsSettings.sandbox;
  const UPS_ACCESS_KEY = (USE_SANDBOX == true)  ? upsSettings.sandbox_access_key : upsSettings.access_key;
  const UPS_ACCOUNT_NUMBER = (USE_SANDBOX == true)  ? upsSettings.sandbox_account_no : upsSettings.acount_no;
  const UPS_USER_ID    = (USE_SANDBOX == true)  ? upsSettings.sandbox_username : upsSettings.username;
  const UPS_PASSWORD    = (USE_SANDBOX == true)  ? upsSettings.sandbox_password : upsSettings.password;

  var ShipAccept  = require('../../lib/shipAccept');
  var acceptShipment  = new ShipAccept(UPS_ACCESS_KEY, UPS_USER_ID, UPS_PASSWORD);
  acceptShipment.useSandbox(USE_SANDBOX);
  acceptShipment.setJsonResponse(true);

  var promiseArray = [];
 


// promiseArray.push(new Promise((resolve, reject) => {
//   // acceptShipment.makeRequest(upsOptions, function(response) {
//   //       if (response) {
//   //           resolve(response)
//   //       }
//   //   });

//     acceptShipment.makeRequest({
//       digest : ShipmentDigest[0].ShipmentDigest
//  }, function(err, response) {
//    if (err) {
//      console.error(err);
//    }
 
//    if (response) {
//      //Enjoy playing the data :)
//      resolve(response)




  
//    }
//  });


// }))

promiseArray.push(new Promise((resolve, reject) => {
  acceptShipment.makeRequest({
           digest : ShipmentDigest[0].ShipmentDigest
       }, function(response) {
      if (response) {
          resolve(response)
      }
  });
}))



return (await Promise.all(promiseArray));


 

  

}






function updateOrderitems(data, refundfromstripe ,orderid , amount , removestock) {
  try {


    if(data.restockitems === true){
      updateProductStock(removestock);
    }
    
    let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
    const newOrderNotes = new OrderNotes({
        created_on: createdon,
        order_id: orderid,
        status: refundfromstripe.status,
        message: amount + " refund "+ refundfromstripe.status + ". Stripe refund completed (Id : "+ refundfromstripe.id +")"
    });

    newOrderNotes
    .save()
    .then((result) => {
      return 1
    })
    .catch((err) => {
      return 0
    });
  } catch (err) {
    return 0
  }

}


    function updateProductStock(removestock) {
      
      try {
 
       removestock.forEach(function(key) {


          if(key._id !== '' && key.count !=0 ) {


            var condition = { _id: key._id };
            Products.find(
             condition,
             async function (err, log) {
               if (err) {
                 // res.send({
                 //   status: 0,
                 //   message: "Oops! " + err.name + ": " + err.message,
                 // });
               } else {
                 if(log.length>0){
                    
                   
         
            Products.updateOne(condition, 
                   { 
                     $set: { 
                       //"inventory.manage_inventory.available_stock_quntity":removestock.count,
                       "inventory.manage_inventory.stock_quantity":    (log[0].inventory.manage_inventory.stock_quantity * 1 + key.count * 1 )
                     },
                 }
                   , async function (
                   err,
                   log
                 ) {
                   if (err) {
                     
                     return 0
                   } else {
                     
                     return 1
                   }
                 });
         
         
                 }else{
                   // res.send({
                   //   status: 0,
                   //   message: "No Orders available."
                   // });
                 }
               }
             }
           ).sort({ order_no: -1 });

          }
         
          
        });


       


       
        
      } catch (err) {
       return 0
      }

    }

    function sendInvoiceMail(results , url, orderid,pdfvalue, callback) {

      
       

    

       let header = fs
       .readFileSync(path.join(process.cwd() + "/routes/emails/header.html"), {
         encootding: "utf-8"
       })
       .toString();
     
        
       var view = {
           imageUrl:config.imageUrl,     
       };
       var createHeader = mustache.render(header, view);
     
     
       let footer = fs
       .readFileSync(path.join( process.cwd() + '/routes/emails/footer.html'), {
         encootding: "utf-8"
       })
       .toString();
     
        
       var view = {
           imageUrl:config.imageUrl,       
       };
       var createFooter = mustache.render(footer, view);
       
       let template = fs
       .readFileSync(path.join(process.cwd() + "/routes/emails/invoice-template.html"), {
         encootding: "utf-8"
       })
       .toString();
     
       let urlpass = url;
       var view = {
           username: results.email,
           siteuUrl :urlpass,  
           invoicenumber :orderid 
       };
       
       var createTemplate = mustache.render(template, view);
       //var htmlContent = createHeader+createTemplate+createFooter;
       var htmlContent = createHeader+ createTemplate +createFooter;
       sgMail.setApiKey(sendGridKey);
       const message = {
           to:results.email,
          //  from: config.fromEmail,
          from : { email : config.fromEmail , name: 'Cimon'},
           subject: "Invoice Mail",
          // files     : [{filename: orderid +'.pdf', content: pdfvalue}],
           html: htmlContent,
           attachments: [
            {
              content: fs.readFileSync(path.join(process.cwd() + '/uploads/invoice/'+orderid+'.pdf')).toString("base64"),
              filename: "attachment.pdf",
              type: "application/pdf",
              disposition: "attachment"
            }
          ]
       };
     
       try {
           sgMail
               .send(message)
               .then(() => {
              //     callback(null, results);
              callback("success");
               })
               .catch(error => {
                 
                   const { message, code, response } = error;
                   callback("failed");
                   callback({
                       status: 0,
                       message: "Oops! failed to send order email" ,
                       from: 'mail'
                   });
               });
       } catch (err) {
         
         callback("failed");
           callback({
               status: 0,
               message: "Oops! " + err.name + ": " + err.message,
               from: 'mail catch'
           }, null);
       }
     }

  


     function sendShipConfirmMail(results, callback) {

     
     let TrackingNumber = '';
      
      
      if(results.packageresults[0]){
         
        results.packageresults[0].PackageResults.PackageResults.forEach(function(item) {
         
          TrackingNumber = TrackingNumber + item.TrackingNumber + ', '
        });
      }


       
       let header = fs
       .readFileSync(path.join(process.cwd() + "/routes/emails/header.html"), {
         encootding: "utf-8"
       })
       .toString();
     
        
       var view = {
           imageUrl:config.imageUrl,     
       };
       var createHeader = mustache.render(header, view);
     
     
       let footer = fs
       .readFileSync(path.join( process.cwd() + '/routes/emails/footer.html'), {
         encootding: "utf-8"
       })
       .toString();
     
        
       var view = {
           imageUrl:config.imageUrl,       
       };
       var createFooter = mustache.render(footer, view);
       
       let template = fs
       .readFileSync(path.join(process.cwd() + "/routes/emails/tracking-template.html"), {
         encootding: "utf-8"
       })
       .toString();
     
        
       var view = {
           username: results.order.shipping_details.email, 
           trackingnumber : TrackingNumber.replace(/,\s*$/, "")
       };
       
       var createTemplate = mustache.render(template, view);
       //var htmlContent = createHeader+createTemplate+createFooter;
       var htmlContent = createHeader+ createTemplate +createFooter;
       sgMail.setApiKey(sendGridKey);
       const message = {
        to:results.order.shipping_details.email,
     //  to: 'elvee@webeteer.com',
        // from: config.fromEmail,
        from : { email : config.fromEmail , name: 'Cimon'},
        subject: "Shipping Confirmation",
        html: htmlContent
        };
     
       try {
           sgMail
               .send(message)
               .then(() => {
                   callback(null, results);
               })
               .catch(error => {
                 
                   const { message, code, response } = error;
                   
                   callback({
                       status: 0,
                       message: "Oops! failed to send order email" ,
                       from: 'mail'
                   });
               });
       } catch (err) {
         
         callback("failed");
           callback({
               status: 0,
               message: "Oops! " + err.name + ": " + err.message,
               from: 'mail catch'
           }, null);
       }
     }
  
  
  
  
  
     module.exports = router;