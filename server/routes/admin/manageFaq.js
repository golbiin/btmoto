//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
//Include schema
const Faq = require("../../models/faq");
const Categories = require("../../models/categories");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();

/*  Get all Faq  */

// /*  Get all videos  */
// router.get("/getAllFaq", async (req, res) => {
//   let filterCondition = {};
//   try {
//   Faq.find(
//       filterCondition,
//       async function (err, log) {
//         if (err) {
//           res.send({
//             status: 0,
//             message: "Oops! " + err.name + ": " + err.message,
//           });
//         } else {
//           if(log.length>0){
//             res.send({
//               status: 1,
//               message: "Fetched Faq successfully.",
//               data: log
//             });
//           }else{
//             res.send({
//               status: 0,
//               message: "No Video Training available."
//             });
//           }
//         }
//       }
//     ).sort({ date: -1 });
//   } catch (e) {
//     res.send({
//       status: 0,
//       message: "Oops! " + e.name + ": " + e.message,
//     });
//   }
// });

router.post("/getAllFaq", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Faq.aggregate([
        {
          $lookup: {
            from: "categories",
            localField: "categories",
            foreignField: "_id",
            as: "category_info"
          }
        }
      ]).exec((err, posts) => {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Success!",
            data: posts
          });
        }
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

// router.post("/getAllFaq", async (req, res) => {
//   const token = req.body.token;
//   if (!token) {
//     res.send({ status: 0, message: "Invalid Request" });
//     } else {
//     try {
//     payload = jwt.verify(token, jwtKey);
//     Faq.find(
//         filterCondition,
//         async function (err, log) {
//           if (err) {
//             res.send({
//               status: 0,
//               message: "Oops! " + err.name + ": " + err.message,
//             });
//           } else {
//             if(log.length>0){
//               res.send({
//                 status: 1,
//                 message: "Fetched Faq successfully.",
//                 data: log
//               });
//             }else{
//               res.send({
//                 status: 0,
//                 message: "No Faq available."
//               });
//             }
//           }
//         }
//       ).sort({ date: -1 });
//     } catch (e) {
//       res.send({
//         status: 0,
//         message: "Oops! " + e.name + ": " + e.message,
//       });
//     }
//   }
// });

router.post("/createFaq", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      //   console.log('req.body.data',req.body.data);
      payload = jwt.verify(token, jwtKey);
      Faq.find({ slug: req.body.data.slug }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            console.log("req.body.data", req.body.data);
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const faq = new Faq({
              title: req.body.data.title,
              date: createdon,
              description: req.body.data.description,
              categories: req.body.data.categories,
              page_status: 1,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, "")
            });
            await faq
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have successfully created a FAQ."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getCategoryParent", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Categories.find({ status: 1, parent_id: 0 }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched categories successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No categories available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteFaq", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Faq.findByIdAndDelete({ _id: req.body.faq_id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Faq deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/trashFaq", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Faq.findByIdAndUpdate(
          { _id: req.body.faq_id },
          { page_status: 0 },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "Faq deleted" });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/getSinglefaq", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Faq.findOne(
          { _id: req.body.faq_id },
          { _id: 1, title: 1, description: 1, categories: 1, slug: 1 },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: { faq_data: log }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/updatefaq", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      Faq.find(
        {
          slug: req.body.data.slug
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, ""),
          _id: { $ne: req.body.data._id }
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "This slug already exists, try another."
              });
            } else {
              Faq.find({ _id: req.body.data._id }, async function(err, log) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  if (log.length > 0) {
                    var condition = { _id: req.body.data._id };
                    var update_value = {
                      title: req.body.data.title,
                      description: req.body.data.description,
                      categories: req.body.data.categories,
                      slug: req.body.data.slug
                        .toLowerCase()
                        .replace(/ /g, "-")
                        .replace(/[^\w-]+/g, "")
                    };
                    Faq.findOneAndUpdate(
                      condition,
                      update_value,
                      async function(err, log) {
                        if (err) {
                          res.send({
                            status: 0,
                            message: "Oops! " + err.name + ": " + err.message
                          });
                        } else {
                          res.send({
                            status: 1,
                            message: "Your FAQ updated successfully."
                          });
                        }
                      }
                    );
                  } else {
                    res.send({
                      status: 1,
                      message: "Your FAQ updated successfully."
                    });
                  }
                }
              }).sort({ created_on: 1 });
            }
          }
        }
      );

      ///////////////////
      payload = jwt.verify(token, jwtKey);

      ////////////////
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/actionHandlerCasestudy", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.case_id };
        if (req.body.action == "3") {
          Faq.deleteMany({ _id: { $in: req.body.case_id } }, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({
                status: 1,
                message: "Faq Trash deleted successfully"
              });
            }
          });
        } else {
          Faq.updateMany(condition, update_value, async function(err, log) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "Your FAQ updated successfully." });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

module.exports = router;
