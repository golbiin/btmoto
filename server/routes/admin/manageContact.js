//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
//Include schema
const Contactus = require("../../models/contact_address");
const ContactSubscription = require("../../models/contact");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();


router.post("/getAllAddress", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {
    payload = jwt.verify(token, jwtKey);
    Contactus.find(
        {  },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if(log.length>0){
              res.send({
                status: 1,
                message: "Fetched Address Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "No address available."
              });
            }

          }
        }
      ).sort({ name: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/createContact", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
      try {
        payload = jwt.verify(token, jwtKey);
      
        let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
        const newContact = new Contactus({
          name: req.body.data.name,
          address1: req.body.data.address1,
          address2: req.body.data.address2,
          phone: req.body.data.phone,
          email: req.body.data.email,
          latitude: req.body.data.latitude,
          longitude: req.body.data.longitude,
          created_on: createdon,
        });
        await newContact.save() 
        .then(() => {
          res.send({
            status: 1,
            message: "You have added a new addresss.",
          });
        }) 
        .catch((err) => {
          res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
          });
        });
  
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/removeContact", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Contactus.findByIdAndDelete({ _id: req.body.data._id }, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(log){
            res.send({ status: 1, message: "You have deleted the address successfully." });
          } else {
            res.send({ status: 1, message: "Something went wrong" });
          }
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
}); 




router.post("/getContactById", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
    payload = jwt.verify(token, jwtKey);
    Contactus.findOne(
        { _id: req.body.data.id },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if(log){
              res.send({
                status: 1,
                message: "Fetched Contact Address Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      ).sort({ created_on: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/updateContact", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      var condition = { _id: req.body.data.id };
      var update_value = { 
        name: req.body.data.name, 
        address1: req.body.data.address1, 
        address2: req.body.data.address2, 
        email: req.body.data.email,
        phone: req.body.data.phone,
        latitude: req.body.data.latitude,
        longitude: req.body.data.longitude,
      };
      Contactus.findOneAndUpdate(condition, update_value, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          res.send({ status: 1, message: "You have updated this contact successfully." });
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});

// ****************************  SUBSCRIPTIONS  ***************************************8

router.post("/getAllSubcriptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {
    payload = jwt.verify(token, jwtKey);
    ContactSubscription.find(
        {  },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if(log.length>0){
              res.send({
                status: 1,
                message: "Fetched Subscription Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "No Subscription available."
              });
            }

          }
        }
      ).sort({ name: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/removeSubscription", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      ContactSubscription.findByIdAndDelete({ _id: req.body.id }, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(log){
            res.send({ status: 1, message: "Subscription deleted successfully" });
          } else {
            res.send({ status: 1, message: "Something went wrong" });
          }
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
}); 


module.exports = router;