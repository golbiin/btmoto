//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
//Include schema
const CaseStudy = require("../../models/case_study");
const Products = require("../../models/products");
const Categories = require("../../models/categories");
const Menus = require("../../models/menus");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();

router.post("/getAllcasestudy", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      CaseStudy.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Case Study successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Case Study available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getIndustryMenu", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Menus.find({ slug: "industries-casestudy" }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched menus successfully.",
              data: log[0].menu_items
            });
          } else {
            res.send({
              status: 0,
              message: "No menus available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
router.post("/getProductMenu", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Menus.find({ slug: "industries-products" }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched menus successfully.",
              data: log[0].menu_items
            });
          } else {
            res.send({
              status: 0,
              message: "No menus available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/createcasestudy", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      //   console.log('req.body.data',req.body.data);
      payload = jwt.verify(token, jwtKey);
      CaseStudy.find({ slug: req.body.data.slug }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            console.log("req.body.data", req.body.data);
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const newCasestudy = new CaseStudy({
              title: req.body.data.title,
              sub_title: req.body.data.sub_title,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              date: createdon,
              description: req.body.data.description,
              image: req.body.data.profile_image,
              content: req.body.data.content,
              is_top_case_study: req.body.data.checked,
              relatedcontent: req.body.data.relatedcontent,
              related_products: req.body.data.related_products,
              related_cat: req.body.data.related_cat,
              releasedate: req.body.data.releasedate,
              data_order : req.body.data.data_order
            });
            await newCasestudy
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have created a Case Study to the Cimon"
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deletecasestudy", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        CaseStudy.findByIdAndDelete({ _id: req.body.case_id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Case Study deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/trashcasestudy", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        CaseStudy.findByIdAndUpdate(
          { _id: req.body.case_id },
          { page_status: 0 },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "Case Study Updated" });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/getSinglecasestudy", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        CaseStudy.findOne(
          { _id: req.body.case_id },
          {
            _id: 1,
            is_top_case_study: 1,
            relatedcontent: 1,
            title: 1,
            sub_title: 1,
            slug: 1,
            image: 1,
            content: 1,
            description: 1,
            related_products: 1,
            related_cat: 1,
            releasedate: 1,
            date: 1,
            data_order : 1
          },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: { Casestudy_data: log }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/updatecasestudy", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      CaseStudy.find(
        //{ slug: req.body.data.slug.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'') , _id : {$ne: req.body.data._id }},
        { _id: req.body.data._id },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              var condition = { _id: req.body.data._id };
              var update_value = {
                title: req.body.data.title,
                sub_title: req.body.data.sub_title,
                slug: req.body.data.slug
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, ""),
                description: req.body.data.description,
                content: req.body.data.content,
                is_top_news: req.body.data.checked,
                image: req.body.data.profile_image,
                relatedcontent: req.body.data.relatedcontent,
                related_products: req.body.data.related_products,
                related_cat: req.body.data.related_cat,
                releasedate: req.body.data.releasedate,
                date: req.body.data.date,
                data_order : Number(req.body.data.data_order)
              };
              CaseStudy.findOneAndUpdate(
                condition,
                update_value,
                async function(err, log) {
                  if (err) {
                    res.send({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message
                    });
                  } else {
                    res.send({
                      status: 1,
                      message: "Case Study Updated successfully"
                    });
                  }
                }
              );
            } else {
              res.send({
                status: 1,
                message: "-->Case Study Updated successfully"
              });
            }
          }
        }
      ).sort({ created_on: 1 });

      ///////////////////
      payload = jwt.verify(token, jwtKey);

      ////////////////
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/actionHandlerCasestudy", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.case_id };
        if (req.body.action == "3") {
          CaseStudy.deleteMany(
            { _id: { $in: req.body.case_id } },
            async function(err, log) {
              if (err) {
                res.send({ status: 1, message: "Something went wrong" + err });
              } else {
                res.send({
                  status: 1,
                  message: "Casestudy Trash deleted successfully"
                });
              }
            }
          );
        } else {
          CaseStudy.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "Article Updated successfully" });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getAllCatsIdsandNames", async (req, res) => {
  try {
    Categories.find(
      {
        status: 1
      },
      async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Fetched categories successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No categories available."
            });
          }
        }
      }
    )
      .sort({ category_name: 1 })
      .select("category_name _id");
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

module.exports = router;
