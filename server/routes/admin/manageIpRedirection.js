//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const mongoose = require('mongoose');
//Include schema
const IpRedirection = require("../../models/ipredirection");
const Pages = require("../../models/pages");
const config = require("../../config/config.json");
const Validate = require("../validations/validate");

const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();


router.post("/getAllRedirection", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      IpRedirection.find({},
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 1,
                message: "Fetched IpRedirection Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "No IpRedirection available."
              });
            }

          }
        }
      ).sort({
        _id: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/createRedirection", async (req, res) => {
  const token = req.body.token;
    console.log('enter this section');
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      console.log('req.body.data', req.body.data);
      let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
      const newIpRedirection = new IpRedirection({
        from: req.body.data.fromurl,
        to: req.body.data.tourl,
        created_on : createdon,
        status : 1 ,
        country : req.body.data.country,
        state : req.body.data.state,
        fromtitle : req.body.data.fromtitle,
        totitle : req.body.data.totitle,
        except : req.body.data.except,
        ip : req.body.data.ip
      //  slug : req.body.data.title.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, ''),
      });

    
      await newIpRedirection.save()
        .then(() => {
          res.send({
            status: 1,
            message: "You have successfully created a IpRedirection.",
          });
        })
        .catch((err) => {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        });

    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


 


router.post("/removeRedirection", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      IpRedirection.findByIdAndDelete({
        _id: req.body.data._id
      }, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Your IpRedirection has been deleted successfully."
            });
          } else {
            res.send({
              status: 1,
              message: "Something went wrong"
            });
          }
        }
      })
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});




router.post("/getRedirectionById", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      IpRedirection.findOne({
          _id: req.body.data.id
        },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if (log) {
              res.send({
                status: 1,
                message: "Fetched IpRedirection Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      ).sort({
        // created_on: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/updateRedirection", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {

      payload = jwt.verify(token, jwtKey);
      var condition = {
        _id: req.body.data.id
      };
      var update_value = {
        from: req.body.data.fromurl ,
        to: req.body.data.tourl ,
        country : req.body.data.country,
        fromtitle : req.body.data.fromtitle,
        totitle : req.body.data.totitle,
        except : req.body.data.except,
        state : req.body.data.state,
        ip : req.body.data.ip
      };
      IpRedirection.findOneAndUpdate(condition, update_value, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          res.send({
            status: 1,
            message: "You have updated the IpRedirection successfully."
          });
        }
      })
    
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



router.post("/updateRedirectionStatus", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      console.log('inside updateIpRedirectionStatus');
      payload = jwt.verify(token, jwtKey);
      var condition = {
        _id: req.body._id
      };
      var update_value = {
        status: req.body.status == true ? 1 : 0 ,
      };

      console.log('update_value', update_value , condition);
      IpRedirection.findOneAndUpdate(condition, update_value, async function (
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          res.send({
            status: 1,
            message: "You have updated the IpRedirection successfully."
          });
        }
      })
    
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/getAllpages", async (req, res) => {

  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Pages.find(
          { $or: [  {  page_status : "1" , page_status : { $exists: false }} ] },
          "title  slug  _id  page_status",
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "Sucess!",
                data: log
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});


module.exports = router;