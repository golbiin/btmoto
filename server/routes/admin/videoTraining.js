/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");

/* Include schema */
const Training = require("../../models/videoTraining");
const config = require("../../config/config.json");
const Validate = require("../validations/validate");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();

router.post("/createVideoTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    Validate.createVideoTraining(req, result => {
      if (result.error) {
        //validation error
        res.send({
          status: 0,
          message: result.error.details[0].message
        });
      } else {
        try {
          payload = jwt.verify(token, jwtKey);
          let created_on = moment().format("MM-DD-YYYY hh:mm:ss");
          const videoTraining = new Training({
            video_title: req.body.data.video_title,
            video_url: req.body.data.video_url,
            video_order: req.body.data.video_order,
            created_on: created_on,
            category_name: req.body.data.category_name,
            page_status: 1
          });
          videoTraining
            .save()
            .then(() => {
              res.send({
                status: 1,
                message: "You have successfully added a video training."
              });
            })
            .catch(err => {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            });
        } catch (e) {
          res.send({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message
          });
        }
      }
    });
  }
});

router.post("/updateVideoTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    Validate.updateVideoTraining(req, result => {
      if (result.error) {
        //validation error
        res.send({
          status: 0,
          message: result.error.details[0].message
        });
      } else {
        try {
          payload = jwt.verify(token, jwtKey);
          var condition = { _id: req.body.data._id };
          var update_value = {
            video_title: req.body.data.video_title,
            category_name: req.body.data.category_name,
            video_url: req.body.data.video_url,
            video_order: req.body.data.video_order
          };
          Training.findOneAndUpdate(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "You have updated this video training successfully."
              });
            }
          });
        } catch (e) {
          if (e instanceof jwt.JsonWebTokenError) {
            res.send({ status: 0, message: "Token verification failed." });
          }
          res.send({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message
          });
        }
      }
    });
  }
});

/*  Get all videos  */
router.post("/getAllVideoTraining", async (req, res) => {
  const token = req.body.token;
  const filter = req.body.filter;
  let filterCondition = {};
  if (filter.date) {
    const selectedDate = filter.date.split("/");
    const start = new Date(selectedDate[1], selectedDate[0] - 1, 1);
    const end = new Date(selectedDate[1], selectedDate[0] - 1, 31);
    filterCondition = { created_on: { $gte: start, $lte: end } };
  }

  if (filter.type) {
    filterCondition.category_name = filter.type;
  }

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Training.find(filterCondition, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Video Training successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Video Training available."
            });
          }
        }
      }).sort({ date: -1, video_order: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/trashVideoTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Training.findByIdAndUpdate(
          { _id: req.body.id },
          { page_status: 0 },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "Video Training Trashed" });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/actionHandlerTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      var update_value = {
        page_status: req.body.action
      };
      try {
        const condition = { _id: req.body.ids };
        if (req.body.action == "3") {
          Training.deleteMany({ _id: { $in: req.body.ids } }, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({
                status: 1,
                message: "Video training trash deleted successfully"
              });
            }
          });
        } else {
          Training.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({
                status: 1,
                message: "You have updated this video training successfully."
              });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getSingleVideoTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Training.findOne({ _id: req.body.id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: { data: log }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

/* List date filter options */
router.post("/getDateFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Training.aggregate(
        [
          {
            $project: {
              year: { $year: "$created_on" },
              month: { $month: "$created_on" }
            }
          },
          {
            $group: {
              _id: null,
              created_on: { $addToSet: { year: "$year", month: "$month" } }
            }
          },
          { $sort: { _id: -1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/* List type filter options */
router.post("/getTypeFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Training.aggregate(
        [
          {
            $project: {
              category_name: 1
            }
          },
          {
            $group: {
              _id: "$category_name"
            }
          },
          { $sort: { category_name: 1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteVideoTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Training.findByIdAndDelete({ _id: req.body.id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Video Training deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

module.exports = router;
