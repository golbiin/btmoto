//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
var async = require('async');
//Include schema
const Users = require("../../models/users");
const Admins = require("../../models/admin");
const Orders = require("../../models/orders");
const Filepermission = require("../../models/filepermission");
//include joi validation methods
const Validate = require("../validations/validate");

const uploadPath = 'uploads/'

const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;
//http request port set
const router = express.Router();




router.post("/getSingleProfile", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Admins.findOne({
            _id: req.body.user_id
          }, {
            _id: 1,
            first_name: 1,
            last_name: 1,
            email: 1,
            username: 1,
            password: 1
          },
          async function (err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {

              res.send({
                status: 1,
                message: "success",
                data: {
                  user_data: log
                },
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});


router.post("/updateProfile", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {

        if (req.body.data.old_password) {
          Admins.findOne({
              $or: [{
                  email: req.body.data.email
                },
                {
                  _id: req.body.data._id
                },
              ],
            },
            function (err, log) {
              if (err) {
                res
                  .status(400)
                  .send("Oops! Something went wrong,please try later!");
              } else if (!log) {
                res.send({
                  status: 0,
                  message: "There is no existing admin with the details that you’ve provided.",
                });
              } else {
                const validatepassword = bcrypt.compareSync(
                  req.body.data.old_password,
                  log.password
                );

                if (!validatepassword) {
                  res.send({
                    status: 0,
                    message: "You have entered an invalid  password.",
                  });
                } else {
                  const salt = bcrypt.genSaltSync(10);
                  bcryptPassword = bcrypt.hashSync(req.body.data.new_password, salt);
                  var update_value = {
                    first_name: req.body.data.first_name,
                    last_name: req.body.data.last_name,
                    email: req.body.data.email,
                    username: req.body.data.username,
                    password: bcryptPassword,

                  };
                  var condition = {
                    _id: req.body.data._id
                  };
                  Admins.findByIdAndUpdate(condition, update_value, async function (
                    err,
                    log
                  ) {
                    if (err) {
                      res.send({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                      });
                    } else {
                      res.send({
                        status: 1,
                        message: "Profile changed successfully.",
                        data: {
                          user_data: log
                        },
                      });
                    }
                  });
                }
              }
            }
          );
        } else {
          var update_value = {
            first_name: req.body.data.first_name,
            last_name: req.body.data.last_name,
            email: req.body.data.email,
            username: req.body.data.username,

          };
          var condition = {
            _id: req.body.data._id
          };
          Admins.findByIdAndUpdate(condition, update_value, async function (
            err,
            log
          ) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {
              res.send({
                status: 1,
                message: "Profile changed successfully.",
                data: {
                  user_data: log
                },
              });
            }
          });
        }

      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});


// admin insert user
router.post("/insertAdmin", async (req, res) => {
  // res.send({ status: req.body.password, message: "Invalid Request" });
  try {
    Validate.adminUsercreate(req, (result) => {
      if (result.error) {
        //validation error
        res.status(200).send(result.error.details[0].message);
      } else {
        //user existance check
        Admins.findOne({
          email: req.body.email
        }, async function (error, log) {
          if (log != null) {
            res.send({
              status: 0,
              message: "An admin user with this email already exists.",
            });
          } else {
            //decrypt password
            const salt = bcrypt.genSaltSync(10);
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            bcryptPassword = bcrypt.hashSync(req.body.password, salt);

            const newUser = new Admins({
              username: req.body.userName,
              first_name: req.body.firstName,
              last_name: req.body.lastName,
              email: req.body.email,
              password: bcryptPassword,
              created_on: createdon,
              status: 0,
              profile_image: req.body.profile_image,
            });

            //save user to mongoo db
            try {
              await newUser
                .save()
                .then(() => {
                  let user_token = jwt.sign({
                      _id: newUser._id,
                      email: newUser.email
                    },
                    jwtKey
                  );
                  res.send({
                    status: 1,
                    message: "You have succesfully created an admin user, you'll now be redirected.",
                    data: {
                      token: user_token,

                    },
                  });
                })
                .catch((err) => {
                  res.status(200).send(err);
                });
            } catch (ex) {
              
            }
          }
        });
      }
    });
  } catch (ex) {
    res.status(400).send(ex);
  }

});


router.post("/getAlladmins", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Admins.find({
          status: 0
        }, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: log,
            });
          }
        })
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});



router.post("/getSingleAdmin", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Admins.findOne({
            _id: req.body.user_id
          }, {
            _id: 1,
            username: 1,
            email: 1,
            first_name: 1,
            last_name: 1,
            profile_image: 1
          },
          async function (err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: {
                  user_data: log
                },
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});


router.post("/updateAdmin", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    // res.send({ status: req.body.data.profile_image, message: "Invalid Request" });
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        if (req.body.data.profile_image) {
          profiledata = req.body.data.profile_image;
          var update_value = {
            first_name: req.body.data.first_name,
            last_name: req.body.data.last_name,
            username: req.body.data.user_name,
            profile_image: profiledata,
          };
        } else {
          var update_value = {
            first_name: req.body.data.first_name,
            last_name: req.body.data.last_name,
            username: req.body.data.user_name,
          };
        }
        var condition = {
          _id: req.body.data._id
        };

        Admins.findByIdAndUpdate(condition, update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {

            res.send({
              status: 1,
              message: "You have changed this administrator details successfully.",
              data: {
                user_data: log
              },
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});

router.post("/deleteAdmin", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {

        Admins.findByIdAndDelete({
          _id: req.body.user_id
        }, async function (
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({
              status: 1,
              message: "Admin deleted"
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});


// admin insert user
router.post("/insertUser", async (req, res) => {
  // res.send({ status: req.body.password, message: "Invalid Request" });
  try {
    console.log('123result.error');
    Validate.Usercreate(req, (result) => {
      console.log('result.error' ,result.error , typeof result.error)
      if (result.error) {
        console.log('result.error' ,result.error , typeof result.error)
        //validation error
        res.status(400).send(result.error);
      } else {
        //user existance check
        Users.findOne({
          email: req.body.email
        }, async function (error, log) {
          if (log != null) {
            res.send({
              status: 0,
              message: "User with provided email already exist.",
            });
          } else {
            //decrypt password
            const uniqid = require('uniqid');
            const salt = bcrypt.genSaltSync(10);
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            bcryptPassword = bcrypt.hashSync(req.body.password, salt);
                console.log(55555555555555555,req.body)
            const newUser = new Users({
              user_name: req.body.email,
              first_name: req.body.firstName,
              last_name: req.body.lastName,
              email: req.body.email,
              password: bcryptPassword,
              created_on: createdon,
              status: 0,
              profile_image: req.body.profile_image,
              usertype: req.body.usertype,
              country:req.body.country,
              company_name: req.body.company_name,
              city: req.body.city,
              zipcode: req.body.zipcode,
              phone_number: req.body.phone_number,
              recovery_email: req.body.recovery_email,
              uniq_id: uniqid(),
              drivelink: req.body.drivelink,
              bussiness_type : req.body.bussiness_type,
              industry : req.body.industry,
              agree_newsletter : req.body.agree_newsletter,
              interested_products :req.body.interested_products
            });
              
            //save user to mongoo db
            try {
              await newUser
                .save()
                .then(() => {
                  
                  let user_token = jwt.sign({
                      _id: newUser._id,
                      email: newUser.email
                    },
                    jwtKey
                  );
                  res.send({
                    status: 1,
                    message: "You have successfully created user, you’ll be redirected.",
                    data: {
                      token: user_token,

                    },
                  });
                })
                .catch((err) => {
                  res.status(400).send(err);
                });
            } catch (ex) {
              
            }
          }
        });
      }
    });
  } catch (ex) {
    console.log('123result.error',ex);
    res.status(400).send(ex);
  }

});

router.post("/getAllUser", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Users.find({}, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: log,
            });
          }
        })
        .sort({
          _id: -1
      });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});

router.post("/changeStatus", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        var condition = {
          _id: req.body.user_id
        };
        var update_value = {
          admin_approve: req.body.status
        };
        Users.findByIdAndUpdate(condition, update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});

/*verify user status */
router.post("/changeVerifyStatus", async (req, res) => {
  
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        var condition = {
          _id: req.body.user_id
        };
        var update_value = {
          verify: req.body.status
        };
        Users.findByIdAndUpdate(condition, update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            res.send({
              status: 1,
              message: "User Verified!",
              data:log,
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});



router.post("/deleteUser", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {

        Users.findByIdAndDelete({
          _id: req.body.user_id
        }, async function (
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({
              status: 1,
              message: "User deleted"
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});
router.post("/getCount", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Users.find({
          account_type: req.body.type
        }, async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: {
                total: log.length
              },
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});

router.post("/getSingleUser", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Users.findOne({
            _id: req.body.user_id
          }, {
            _id: 1,
            user_name: 1,
            email: 1,
            first_name: 1,
            last_name: 1,
            profile_image: 1,
            usertype: 1,
            company_name: 1,
            city: 1,
            zipcode: 1,
            phone_number: 1,
            recovery_email: 1,
            uniq_id: 1,
            country:1,
            drivelink:1,
            agree_newsletter : 1,
            bussiness_type : 1,
            industry : 1,
            interested_products : 1
          },
          async function (err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: {
                  user_data: log
                },
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});

router.post("/updateUser", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    // res.send({ status: req.body.data.profile_image, message: "Invalid Request" });
    try {
      console.log('enter 1');
      payload = jwt.verify(token, jwtKey);
      try {
        console.log('enter 2');
            let bcryptPassword = '';
           const salt = bcrypt.genSaltSync(10);
           //const newPassword =  req.body.data.newPassword;
           if(req.body.data.newPassword)
           {
            bcryptPassword =   bcrypt.hashSync(req.body.data.newPassword, salt) ;
           }
           console.log('enter 3');
      //  console.log(444,bcryptPassword, req.body.data.newPassword);
        if (req.body.data.profile_image) {
          console.log('enter 4');
          profiledata = req.body.data.profile_image;
          var update_value = {
            first_name: req.body.data.first_name,
            last_name: req.body.data.last_name,
            user_name: req.body.data.user_name,
            profile_image: profiledata,
            usertype: req.body.data.usertype,
            company_name: req.body.data.company_name,
            country:req.body.data.country,
            city: req.body.data.city,
            zipcode: req.body.data.zipcode,
            phone_number: req.body.data.phone_number,
            recovery_email: req.body.data.recovery_email,
            uniq_id: req.body.data.uniq_id,
            drivelink: req.body.data.drivelink,
            password: bcryptPassword,
            agree_newsletter:  req.body.data.agree_newsletter,
            industry :  req.body.data.industry,
            bussiness_type :  req.body.data.bussiness_type,
            interested_products : req.body.data.interested_products
          };
        } else {
          
          var update_value = {
            first_name: req.body.data.first_name,
            last_name: req.body.data.last_name,
            user_name: req.body.data.user_name,
            usertype: req.body.data.usertype,
            uniq_id: req.body.data.uniq_id,
            country:req.body.data.country,
            company_name: req.body.data.company_name,
            city: req.body.data.city,
            zipcode: req.body.data.zipcode,
            phone_number: req.body.data.phone_number,
            recovery_email: req.body.data.recovery_email,
            drivelink: req.body.data.drivelink,
             password: bcryptPassword,
            agree_newsletter:  req.body.data.agree_newsletter,
            industry :  req.body.data.industry,
            bussiness_type :  req.body.data.bussiness_type,
            interested_products : req.body.data.interested_products
          };
        }
        var condition = {
          _id: req.body.data._id
        };
        console.log('enter 5' ,update_value);
        Users.findByIdAndUpdate(condition, update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            //  fs.unlinkSync(uploadPath + profile_image);
            res.send({
              status: 1,
              message: "User details changed successfully.",
              data: {
                user_data: log
              },
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong.",
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!",
      });
    }
  }
});


router.post("/uploadData", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    res.send({
      status: 1,
      message: "Valid"
    });
  }
});


router.post("/getDashboardDetails", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {



    try {
      async.parallel({
        UserCount: function getUserCount(callback) {
          Users.find().count().exec(callback);
        },
        AdminCount: function getAdminCount(callback) {
          Admins.find().count().exec(callback);
        },
        OrderCount: function getUserCount(callback) {
          Orders.find().count().exec(callback);
        },
        LastMonthCount: function getLastMonthCount(callback) {
          let d = new Date();
          let last30days = d.setDate(d.getDate() - 30);

          
          Users.aggregate([{
            $match: {
              created_on: {
                $gte: last30days,
                $lte: d
              }
            }
          }]).exec(
            (err, User) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {
                callback(null, User.length);
              }
            }
          );
        },
        CurrentMonthCount: function getCurrentMonthCount(callback) {
          let lastdate = new Date();
          let firstDay = new Date(lastdate.getFullYear(), lastdate.getMonth(), 1);
          Users.aggregate([{
            $match: {
              created_on: {
                $gt: firstDay,
                $lte: lastdate
              }
            }
          }]).exec(
            (err, User) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {
                callback(null, User.length);
              }
            }
          );
        },
        UserWeekCount: function getCurrentMonthCount(callback) {

          var today = new Date();
          var sundaylastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
          var satlastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate());

          Users.aggregate([{
            $match: {
              created_on: {
                $gte: sundaylastWeek,
                $lte: satlastWeek
              }
            }
          }]).exec(
            (err, User) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {
                callback(null, User.length);
              }
            }
          );
        },
        UserGraphdata: function getUserGraphdata(callback) {

          var today = new Date();
          lastyearDate = new Date(today.getFullYear(), 0, 1);
          ThisyearLastDate = new Date(today.getFullYear(), 12, 1);
          Users.aggregate([

            {
              $match: {
                created_on: {
                  $gt: lastyearDate,
                  $lte: ThisyearLastDate
                }
              }
            },

            {
              $project: {
                month: {
                  $month: "$created_on"
                },
                year: {
                  $year: "$created_on"
                },
              },

            },

            {
              $group: {
                _id: '$month',
                count: {
                  $sum: 1
                }
              }
            },

            {
              $sort: {
                _id: -1
              }
            }



          ]).exec(
            (err, Results) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {
                


                // if(Results.UserGraphdata){
                //   const Items =[];
                //   for(i = 0;i<=12;i++){
                //     Results.UserGraphdata.forEach(function(item){
                //       if(item._id == i){
                //         Items.push(item);
                //       }
                //     });
                //   }
                //   callback(null, Items);
                // } else {
                //   callback(null, Results);
                // }          

                callback(null, Results);

              }
            }
          );
        },
        LastMonthOrderCount: function getLastMonthCount(callback) {
          // var today = new Date();
          // var last30days = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30);
          // var d = today;

          let lastdate = new Date();
          let last30days = new Date(lastdate.getFullYear(), lastdate.getMonth(), 1);

          Orders.aggregate([{
            $match: {
              created_on: {
                $gte: last30days,
                $lt: lastdate
              }
            }
          }]).exec(
            (err, Orders) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {

                callback(null, Orders.length);
              }
            }
          );
        },
        SalesGraphDataCurrentYear: function getsalesGraphdata(callback) {
          var today = new Date();
          lastyearDate = new Date(today.getFullYear(), 0, 1);
          ThisyearLastDate = new Date(today.getFullYear(), 12, 1);
          Orders.aggregate([

            {
              $match: {
                created_on: {
                  $gt: lastyearDate,
                  $lte: ThisyearLastDate
                }
              }
            },

            {
              $project: {
                month: {
                  $month: "$created_on"
                },
                year: {
                  $year: "$created_on"
                },
              },

            },

            {
              $group: {
                _id: '$month',
                count: {
                  $sum: 1
                }
              }
            },

            {
              $sort: {
                _id: -1
              }
            }
          ]).exec(
            (err, Results) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {


                callback(null, Results);

              }
            }
          );



        },
        SalesGraphDataPrevieousYear: function getsalesGraphdataPrev(callback) {
          var today = new Date();
          lastyearDate = new Date(today.getFullYear(), -12, 1);
          ThisyearLastDate = new Date(today.getFullYear(), 0, 1);
          
          
          Orders.aggregate([

            {
              $match: {
                created_on: {
                  $gt: lastyearDate,
                  $lte: ThisyearLastDate
                }
              }
            },

            {
              $project: {
                month: {
                  $month: "$created_on"
                },
                year: {
                  $year: "$created_on"
                },
              },

            },

            {
              $group: {
                _id: '$month',
                count: {
                  $sum: 1
                }
              }
            },

            {
              $sort: {
                _id: -1
              }
            }
          ]).exec(
            (err, Results) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {


                callback(null, Results);

              }
            }
          );

        },
        MonthlyRevenue: function getLastMonthCount(callback) {
          var today = new Date();
          var last30days = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30);
          var d = today;
          let lastdate = new Date();
          let firstDay = new Date(lastdate.getFullYear(), lastdate.getMonth(), 1);

          Orders.aggregate([{
            $match: {
              $or: [{
                status: 'Done'
              }, {
                status: 'succeeded'
              }, {
                status: 'Completed'
              }],
              $and: [{
                  created_on: {
                    $gt: last30days
                  }
                },
                {
                  created_on: {
                    $lte: d
                  }
                },
                {
                  page_status: {
                    $eq: '1'
                  }
                },

              ]
            }
          }]).exec(
            (err, Orders) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {
                
                let Ordertotal = 0;
                Orders.forEach(function (Orderitem) {
                  let refundvalue = 0
                  refundvalue =
                    (Orderitem.order_details.refund.length > 0 ?
                      (Orderitem.order_details.refund.reduce((refundvalue, Qty) => refundvalue * 1 + (Qty.other.amount * 1) / 100, 0)) : 0
                    )

                  if (Orderitem.order_details.total.total) {
                    Ordertotal = Ordertotal + (Orderitem.order_details.total.total * 1 - refundvalue * 1)
                  }


                })

                callback(null, Ordertotal);
              }
            }
          );
        },
        ProjectGraphData: function getProjectGraphData(callback) {
          var today = new Date();
          lastyearDate = new Date(today.getFullYear(), 0, 1);
          ThisyearLastDate = new Date(today.getFullYear(), 12, 1);
          
          
          Orders.aggregate([

            {
              $match: {
                created_on: {
                  $gt: lastyearDate,
                  $lte: ThisyearLastDate
                }
              }
            },

            // {
            //     $project : { 
            //       month : {$month : "$created_on"}, 

            //     },

            // },

            {
              $group: {
                _id: '$status',
                count: {
                  $sum: 1
                }
              }
            },

            // { $sort : { _id : -1 } }



          ]).exec(
            (err, Results) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {


                let bgcolor = '#999999';
                allStatusarray = []
                Results.forEach(function (item) {
                  const setData = {};
                  if (item._id.toLowerCase() === 'succeeded' || item._id.toLowerCase() === 'done') {
                    bgcolor = "#2DCE98";
                  } else if (item._id.toLowerCase() === 'processing') {
                    bgcolor = "#11cdef";
                  } else if (item._id.toLowerCase() === 'canceled') {
                    bgcolor = "#F53C56";
                  } else if (item._id.toLowerCase() === 'delivery') {
                    bgcolor = "#ffa500";
                  } else if (item._id.toLowerCase() === 'ongoing') {
                    bgcolor = "#8a5cfa";
                  } else if (item._id.toLowerCase() === 'refund') {
                    bgcolor = "#FF4E50";
                  } else {
                    bgcolor = "#7579FF";
                  }
                  setData.data = [item.count];
                  setData.backgroundColor = [bgcolor];
                  setData.label = item._id;

                  allStatusarray.push(setData);
                });

                callback(null, allStatusarray);

              }
            }
          );

        },

        DevliveryLastmonthCount: function DevliverymonthCountfns(callback) {
          var today = new Date();
          var last30days = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30);
          var d = today;
          let lastdate = new Date();
          let firstDay = new Date(lastdate.getFullYear(), lastdate.getMonth(), 1);
          Orders.aggregate([{
            $match: {
              $or: [{
                status: 'delivery'
              }, {
                status: 'Delivery'
              }],
              $and: [{
                  created_on: {
                    $gt: last30days
                  }
                },
                {
                  created_on: {
                    $lte: d
                  }
                },
                {
                  page_status: {
                    $eq: '1'
                  }
                },

              ]
            }
          }]).exec(
            (err, Orders) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {
                callback(null, Orders.length);
              }
            }
          );
        },


        DevliveryTotalCount: function DevliveryTotalCountfns(callback) {
          var today = new Date();
          var last30days = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30);
          var d = today;
          let lastdate = new Date();
          let firstDay = new Date(lastdate.getFullYear(), lastdate.getMonth(), 1);
          Orders.aggregate([{
              $match: {
                $or: [{
                  status: 'delivery'
                }, {
                  status: 'Delivery'
                }],
                $and: [{
                  page_status: {
                    $eq: '1'
                  }
                }, ]
              }
            }

          ]).exec(
            (err, Orders) => {
              if (err) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              } else {
                callback(null, Orders.length);
              }
            }
          );
        },






      }, function (error, results) {
        if (error) {

          res.send({
            status: 0,
            message: error,
          });

        } else {
          


          res.send({
            status: 1,
            message: "Fetched  successfully.",
            data: results
          });


        }

      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }

  }


});



router.post('/getDungraphDetails', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({
      status: 0,
      message: 'Invalid Request'
    })
  } else {
    try {
      async.parallel({
          CountryGraphData: function getProjectGraphData(callback) {
            var today = new Date()
            lastyearDate = new Date(today.getFullYear(), 0, 1)
            ThisyearLastDate = new Date(today.getFullYear(), 12, 1)
            
            
            Orders.aggregate([{
                $unwind: '$order_details.total.total'
              },
              {
                $unwind: '$shipping_details.country'
              },
              //    { $unwind: '$order_details.refund' },

              {
                $match: {
                  created_on: {
                    $gt: lastyearDate,
                    $lte: ThisyearLastDate
                  },
                  page_status: {
                    $eq: '1'
                  },
                  $or: [{
                    status: 'succeeded'
                  }],
                }
              },
              {
                $group: {
                  total: {
                    $sum: '$order_details.total.total'
                  },
                  _id: '$shipping_details.country',
                  //       refund : { "$sum": "$order_details.refund.amount" } ,


                }
              }
              // { $sort : { _id : -1 } }
            ]).exec((err, Results) => {
              if (err) {
                callback({
                  status: 0,
                  message: 'Error fetching data'
                }, null)
              } else {
                
                Resultsarray = [];
                Results.map((cat, index) => {
                  const Setdata = {};
                  Setdata._id = cat._id;
                  Setdata.total = (cat.total);
                  Setdata.refund = 0;
                  Resultsarray.push(Setdata);
                });
                callback(null, Resultsarray)
              }
            })
          },

          refundedCountryGraphData: function getProjectGraphData(callback) {
            var today = new Date()
            lastyearDate = new Date(today.getFullYear(), 0, 1)
            ThisyearLastDate = new Date(today.getFullYear(), 12, 1)
            
            
            Orders.aggregate([
              //   { $unwind: '$order_details.refund' },
              {
                $unwind: '$shipping_details.country'
              },
              {
                $unwind: '$order_details.refund'
              },

              {
                $match: {
                  created_on: {
                    $gt: lastyearDate,
                    $lte: ThisyearLastDate
                  },
                  page_status: {
                    $eq: '1'
                  },
                  $or: [{
                    status: 'succeeded'
                  }],
                }
              },
              {
                $group: {
                  //  total: { $sum: '$order_details.refund.amount' },
                  _id: '$shipping_details.country',
                  refund: {
                    "$sum": "$order_details.refund.amount"
                  },
                  "probabilityArr": {
                    $push: "$order_details.refund.amount"
                  }

                }
              },
              {
                $project: {

                  "results": {
                    $reduce: {
                      input: "$probabilityArr",
                      initialValue: 1,
                      in: {
                        $multiply: ["$$value", "$$this.amount"]
                      }
                    }
                  }
                }
              }
              // { $sort : { _id : -1 } }
            ]).exec((err, Results) => {
              if (err) {
                callback({
                  status: 0,
                  message: 'Error fetching data'
                }, null)
              } else {
                
                Resultsarray = [];
                Results.map((cat, index) => {
                  const Setdata = {};
                  Setdata._id = cat._id;
                  Setdata.total = (cat.total);
                  Setdata.refund = 0;
                  Resultsarray.push(Setdata);
                });
                callback(null, Resultsarray)
              }
            })
          },
          CurrentMonthOrderCount: function getCurrentMonthCount(callback) {
            let lastdate = new Date();
            let firstDay = new Date(lastdate.getFullYear(), lastdate.getMonth(), 1);
            Orders.aggregate([{
              $match: {
                created_on: {
                  $gt: firstDay,
                  $lte: lastdate,

                },
                page_status: {
                  $eq: '1'
                },
                //  $or: [{ status: 'Done' }, { status: 'succeeded' }],
              }
            }]).exec(
              (err, User) => {
                if (err) {
                  callback({
                      status: 0,
                      message: "Error fetching data",
                    },
                    null
                  );
                } else {
                  callback(null, User.length);
                }
              }
            );
          },
          UserWeekOrderCount: function getCurrentMonthCount(callback) {

            var today = new Date();
            var sundaylastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
            var satlastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
            
            Orders.aggregate([{
              $match: {
                created_on: {
                  $gte: sundaylastWeek,
                  $lte: satlastWeek,

                },
                page_status: {
                  $eq: '1'
                },
                //   $or: [{ status: 'Done' }, { status: 'succeeded' }],
              }
            }]).exec(
              (err, User) => {
                if (err) {
                  callback({
                      status: 0,
                      message: "Error fetching data",
                    },
                    null
                  );
                } else {
                  callback(null, User.length);
                }
              }
            );
          },
        },
        function (error, results) {
          if (error) {
            res.send({
              status: 0,
              message: error
            })
          } else {
            
            res.send({
              status: 1,
              message: 'Fetched successfully.',
              data: results
            })
          }
        }
      )
    } catch (e) {
      res.send({
        status: 0,
        message: 'Oops! ' + e.name + ': ' + e.message
      })
    }
  }
})




// admin update updateFilePermission
router.post("/updateFilePermission", async (req, res) => {
  // res.send({ status: req.body.password, message: "Invalid Request" });
  try {
    const token = req.body.token;
  
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
        try {
          if (!token) {
            res.send({ status: 0, message: "Invalid Request" });
          } else {
            try {
              payload = jwt.verify(token, jwtKey);
              var condition = { slug : 'filepermission' };
              var update_value = { 
                items: req.body.data,
              };
              Filepermission.findOneAndUpdate(condition, update_value, async function (
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                  });
                } else {
                  res.send({ status: 1, message: "Filepermission Updated successfully" });
                }
              })
            } catch (e) {
              if (e instanceof jwt.JsonWebTokenError) {
                res.send({ status: 0, message: "Token verification failed." });
              }
              res.send({
                status: 0,
                message: "Oops! " + e.name + ": " + e.message,
              });
            }
          }


          
        } catch (e) {
          res.send({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
          });
        }
      }
  } catch (ex) {
    res.send({
      status: 0,
      message: "Oops! " + ex.name + ": " + ex.message,
    });
  }

});


router.post("/getFilepermissionById", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {

   

    payload = jwt.verify(token, jwtKey);
    Filepermission.find(
      { slug: 'filepermission' },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            
            if(log.length>0){
              res.send({
                status: 1,
                message: "Fetched File permission Successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "No File permission available.",
                data: log
              });
            }

          }
        }
      )
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/actionHandlerUser", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      
        var update_value = {  
          page_status: req.body.action,
        };

        try {

          if(req.body.action == '1'){
           
            Users.deleteMany({ _id: { $in: req.body.userids} }, 
              async function (err, result) {
                if (err) {
                  res.send({ status: 0, message: "Something went wrong" + err });
                } else {
                  //success
                  res.send({ status: 1, message: "Users Deleted successfully"});
                }
            });  


          } 
          else if(req.body.action == '2'){
        //    console.log("req.body.action == '2'" ,req.body.status);
              var update_value = {  
                usertype: req.body.status,
              };
              const  condition = { _id: req.body.userids };
              Users.updateMany(condition, update_value, async function (
                err,
                log
              ) {
                if (err) {
                  res.send({ status: 1, message: "Something went wrong" + err });
                } else {
                  res.send({ status: 1, message: "Users Updated successfully"});
                }
              })
  
          }
          else {
            res.send({ status: 1, message: "Something went wrong"   });
          }
         
            // const  condition = { _id: req.body.newsids };
            // Users.updateMany(condition, update_value, async function (
            //   err,
            //   log
            // ) {
            //   if (err) {
            //     res.send({ status: 1, message: "Something went wrong" + err });
            //   } else {
            //     res.send({ status: 1, message: "Pages Updated successfully"});
            //   }
            // })

        } catch (e) {
            res.send({ status: 1, message: "Something went wrong" +e });
        }



    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});


router.post("/exportcsv", async (req, res) => {
  const token = req.body.token;
  
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {
    payload = jwt.verify(token, jwtKey);
    const role = req.body.role;
    startdate = req.body.start ? new Date(req.body.start) :  new Date() ;
    enddate = req.body.end ? new Date(req.body.end) : new Date(); 
    if(role === '') {
     
      Users.find({
        created_on: {
          $gte: new Date(startdate.getFullYear(), startdate.getMonth(), startdate.getDate()), 
          $lte: new Date(enddate.getFullYear(), enddate.getMonth(), enddate.getDate()) ,
      }
    }, 'first_name  last_name  user_name email usertype company_name  bussiness_type city country drivelink profile_image phone_number verify', async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          res.send({
            status: 1,
            message: "success",
            data: log
          });
        }
      });


    } else {

      console.log('role', role)
      Users.find({
        $and: [{
          created_on: {
            $gte: new Date(startdate.getFullYear(), startdate.getMonth(), startdate.getDate()), 
            $lte: new Date(enddate.getFullYear(), enddate.getMonth(), enddate.getDate()) ,
          }
        },
        {
          usertype: {
            $eq: role
          }
        },
      
      

      ]
         
    }, 'first_name  last_name  user_name email usertype company_name  bussiness_type city country drivelink profile_image phone_number verify' , async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          res.send({
            status: 1,
            message: "success",
            data: log
          });
        }
      });
    }
  
  } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



module.exports = router;