//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
//Include schema
const Integrator = require("../../models/integrator"); 
const Resources = require("../../models/resources");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();
var async = require('async');


  router.post("/createIntegrator", async (req, res) => {
    const token = req.body.token;   

    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
        try {
         
          payload = jwt.verify(token, jwtKey);
          
         
        
          let createdon = moment().format("MM-DD-YYYY hh:mm:ss");

         
          const newResources= new Resources({
            category_id: req.body.data.category_id,
            product_id: req.body.data.product_id,
            resource_category: req.body.data.resource_category,
            file_url : req.body.data.file_url,
            filename : req.body.data.filename, 
            version: req.body.data.version,
            resource_type : req.body.data.resource_type,
            created_on: createdon, 
          });
          newResources.save() 
          .then(() => {
            res.send({
              status: 1,
              message: "You have created ",
            });
          }) 
          .catch((err) => {
            res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
            });
          });
    
      } catch (e) {
        res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
        });
      }
   }
  });



  router.post("/getAllIntegratorResources", async (req, res) => {
    const token = req.body.token;
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
    try {
    payload = jwt.verify(token, jwtKey);
    Resources.find(
        { },
        async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            });
          } else {
            if(log.length>0){
              res.send({
                status: 1,
                message: "Fetched careers successfully.",
                data: log
              });
            }else{
              res.send({
                status: 0,
                message: "No careers available."
              });
            }
          }
        }
      ).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
  });


  router.post("/deleteIntegrator", async (req, res) => {
    const token = req.body.token;
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
      try {
        payload = jwt.verify(token, jwtKey);
        try {
          Resources.findByIdAndDelete({ _id: req.body.id }, async function (
            err,
            log
          ) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "Resources deleted" });
            }
          });
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops!Something went wrong.",
          });
        }
      } catch (e) {
        if (e instanceof jwt.JsonWebTokenError) {
          res.send({ status: 0, message: "Token verification failed." });
        }
        res.send({
          status: 0,
          message: "Oops! Something went wrong with JWT token verification...!",
        });
      }
    }
  });


  router.post("/getSingleIntegrator", async (req, res) => {
    const token = req.body.token;
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
      try {
        payload = jwt.verify(token, jwtKey);
        try {
          Resources.findOne(
            { _id: req.body.user_id },
            { _id: 1, category_id: 1, product_id:1, file_url: 1, filename: 1, version: 1, resource_category:1, resource_type:1 },
            async function (err, log) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
                
              } else {
               
                res.send({
                  status: 1,
                  message: "success",
                  data: { user_data: log }, 
                });
                
              }
            }
          );
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops!Something went wrong.",
          });
        }
      } catch (e) {
        if (e instanceof jwt.JsonWebTokenError) {
          res.send({ status: 0, message: "Oops!jwt token malformed." });
        }
        res.send({
          status: 0,
          message: "Oops! Something went wrong with JWT token verification...!",
        });
      }
    }
  });


  
  router.post("/updateResource", async (req, res) => {
    const token = req.body.token;
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
      try {
        payload = jwt.verify(token, jwtKey);
        try {
          
            var update_value = {
              category_id: req.body.data.category_id,
              product_id: req.body.data.product_id,
              resource_category: req.body.data.resource_category,
              filename:req.body.data.filename,
              file_url: req.body.data.file_url,
              version:req.body.data.version,
              resource_type: req.body.data.resource_type,
            };
          
           var condition = { _id: req.body.data._id };
  
          Resources.findByIdAndUpdate(condition, update_value, async function ( err, log ) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {
              res.send({
                status: 1,
                message: "User details changed successfully.",
                data: { user_data: log },
              });
            }
          });
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops!Something went wrong.",
          });
        }
      } catch (e) {
        if (e instanceof jwt.JsonWebTokenError) {
          res.send({ status: 0, message: "Oops!jwt token malformed." });
        }
        res.send({
          status: 0,
          message: "Oops! Something went wrong with JWT token verification...!",
        });
      }
    }
  });

  
 
module.exports = router;