//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");

const multer = require("multer");
const AWS = require("aws-sdk");
var multerS3 = require('multer-s3');
//Include schema
const Pages = require("../../models/pages");
//include joi validation methods
const Validate = require("../validations/validate");
const config = require("../../config/config.json");
const url = config.uploadURL;
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;
const router = express.Router();

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, url);
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  }
});

router.post("/getAllpages", async (req, res) => {
  // res.send({
  //   status: 1,
  //   message: "Sucess!",
  // });
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Pages.find(
          {},
          "title  slug  _id is_duplicate is_editable is_delete page_status",
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "Sucess!",
                data: log
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

// admin insert page
router.post("/insertPages", async (req, res) => {
  // res.send({ status: req.body.title, message: "Invalid Request" });
  const token = req.body.token;
  //res.send({ status: imagarray, message: "Invalid Request" });
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
    //  console.log('enter data',req.body );
      Pages.findOne(
        {
          slug: req.body.data.slug.toLowerCase()
          .replace(/ /g, "-")
          .replace(/[^\w-]+/g, ""),
        },
        async function(error, log) {
          if (log != null) {
            res.send({
              status: 0,
              message: "Page with provided slug already exist."
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const newPage = new Pages({
               
                 title: req.body.data.title,
                 slug:  req.body.data.slug
                 .toLowerCase()
                 .replace(/ /g, "-")
                 .replace(/[^\w-]+/g, ""),
                 page_title: req.body.data.page_title,
                 content: req.body.data.content,
                 meta_tag : req.body.data.meta_tag,
                 meta_desc: req.body.data.meta_desc,
                 created_on: createdon,
                 template_name: 'dynamic',
                 is_editable: false,
                 is_duplicate: true,
                 is_preview: true,
                 is_delete: true
            });

            // console.log('newPage' , newPage);
            //save page to mongoo db
            try {
              await newPage
                .save()
                .then(() => {
                  
                  res.send({
                    status: 1,
                    message:
                      "You have successfully created page, you’ll be redirected.",
                  });
                })
                .catch(err => {
                  res.status(400).send(err);
                });
            } catch (ex) {}
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }



 
});

router.post("/deletePage", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {
        Pages.findByIdAndDelete(
          {
            _id: req.body.page_id,
            is_delete: true
          },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({
                status: 1,
                message: "Page deleted"
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/trashPage", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {
        Pages.findByIdAndUpdate(
          {
            _id: req.body.page_id
          },
          {
            _id: req.body.page_id
          },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({
                status: 1,
                message: "Page deleted"
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/duplicatePage", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      Pages.findOne({ _id: req.body.page_id }, async function(err, page) {
        if (err) {
          res.send({
            from: "err",
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          const createdon = moment().format("MM-DD-YYYY hh:mm:ss");
          const title = page.title + " copy";

          const slug = title
            .toLowerCase()
            .replace(/[^\w ]+/g, "")
            .replace(/ +/g, "-");
          Pages.find({ slug: new RegExp(slug, "i") }, async function(
            err,
            response
          ) {
            if (err) {
              res.send({
                from: "err",
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              const newSlug =
                slug + (response.length > 0 ? response.length : "");
              const newTitle =
                title + (response.length > 0 ? response.length : "");

              const newPage = new Pages({
                title: newTitle,
                slug: newSlug,
                page_title: page.page_title,
                content: page.content,
                created_on: createdon,
                template_name: page.template_name,
                is_editable: page.is_editable,
                is_duplicate: page.is_duplicate,
                is_preview: page.is_preview,
                is_delete: true
              });

              await newPage
                .save()
                .then(() => {
                  let page_token = jwt.sign(
                    {
                      _id: newPage._id,
                      title: newPage.title
                    },
                    jwtKey
                  );
                  res.send({
                    status: 1,
                    message: "You have successfully duplicated the page.",
                    data: {
                      token: page_token
                    }
                  });
                })
                .catch(err => {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                });
            }
          });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      } else {
        res.send({
          status: 0,
          message: "Oops! Something went wrong with JWT token verification...!"
        });
      }
    }
  }
});

router.post("/getSinglePageById", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Pages.findOne(
        {
          _id: req.body.slug
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: {
                page_data: log
              }
            });
          }
        }
      );
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

 
router.post("/getSinglePage", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Pages.findOne(
          {
            slug: req.body.slug
          },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: {
                  page_data: log
                }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/updatePage", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      console.log("-->", req.body.data.slug, req.body.data.id);
      payload = jwt.verify(token, jwtKey);
      try {
        var update_value = {
          title: req.body.data.title,
          page_title: req.body.data.page_title,
          meta_tag: req.body.data.meta_tag,
          meta_desc: req.body.data.meta_desc,
          slug: req.body.data.slug,
          content: req.body.data.content
        };
        // const slug = req.body.data.slug.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
        const slug = req.body.data.slug;
        const id = req.body.data.id;
        var condition = {
          _id: id
        };
        Pages.findOneAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Page details changed successfully.",
              data: {
                user_data: log
              }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/previewPage", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        var update_value = {
          // title: req.body.data.title,
          // page_title: req.body.data.page_title,
          // //    slug: req.body.data.slug,
          preview: req.body.data.preview
        };
        const slug = req.body.data.slug
          .toLowerCase()
          .replace(/ /g, "-")
          .replace(/[^\w-]+/g, "");
        var condition = {
          slug: slug
        };
        Pages.findOneAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Preview changed successfully.",
              data: {
                user_data: log
              }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

// Page iamge upload

// router.use('/static', express.static(__dirname + '/images'))
// const storage = multer.diskStorage({
//     destination: function(req, file, cb){
//     cb(null, './images')
//     },
//     filename: function(req, file, cb){
//     cb(null, new Date().toISOString().split(':')[0] + file.originalname)
//     }
// })
// const upload = multer({storage:storage})
// router.post('/uploadPageImage', upload.single('file'), (req, res) => {

//     res.json('http://localhost:4000/images/' + req.file.filename)
// });

//--------------------------------------------------------

router.post("/uploadVideo-old/:token", async (req, res) => {
  const token = req.params.token;
  if (!token) {
    res.send({
      status: 0,
      message: "invalid request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        await uploadSingle(req, res, async function(err) {
          if (err instanceof multer.MulterError) {
            res.send({
              status: 0,
              message: "Oops! something went wrong with uploading ."
            });
          } else if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            try {
              await awsUploadSingle(req, result => {
                if (result.message) {
                  //error
                  return res.status(500).json({
                    status: 0,
                    message: result.message
                  });
                } else {
                  //sucess
                  fs.unlinkSync(url + req.file.filename);
                  res.send({
                    status: 1,
                    message: "File uploaded",
                    data: {
                      file_location: result,
                      file_name: req.file.filename
                    }
                  });
                }
              });
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            }
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});


  /* upload path setting ends */
var s3 = new AWS.S3();
var uploads3 = multer({
  storage: multerS3({
    s3: s3,
    bucket: config.BucketName,
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, "profile_cimon/" + Date.now().toString() + "-" + file.originalname)
    }
  })
})
  /* upload path setting ends */
  /* Multiple file upload */
  router.post('/uploadVideo/:token', uploads3.array('file', 70), function(req, res, next) { 
    console.log('entered uploadVideo');


    try {

          const token = req.params.token;
          if (!token) {
            res.send({
              status: 0,
              message: "invalid request",
            });
          }
          const imageTypes = config.fileTypes.image;
          const audioTypes = config.fileTypes.audio;
          const videoTypes = config.fileTypes.video;
          const zipTypes = config.fileTypes.archives;
          const docTypes = config.fileTypes.documents;

          var ResponseData = [];
          isFileUploaded = false;
          req.files.map((item) => {

          const fileinfo = {
            originalname: item.originalname,
            encoding: item.encoding,
            mimetype: item.mimetype,
            filename: item.originalname,
            size: item.size,
          }

          let validFile = true;
          let filetype = 'image';

          if(imageTypes.includes(item.mimetype)) {
            filetype = 'image'
          } else if(audioTypes.includes(item.mimetype)) {
            filetype = 'audio'
          } else if(videoTypes.includes(item.mimetype)) {
            filetype = 'video'
          } else if(zipTypes.includes(item.mimetype)) {
            filetype = 'archive'
          } else if(docTypes.includes(item.mimetype)) {
            filetype = 'document';
          }

          item.file_info = fileinfo;
          item.status = 1;
          item.filetype = filetype;
          ResponseData.push(item);
          });
          console.log("ResponseData",ResponseData);

          res.send({
          status: 1,
          message: "File uploaded",
          data: {
            file_location: ResponseData[0].location,
            file_name: ResponseData[0].originalname
          }
          });

      } catch (err) {
        res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
        });
      }
    
}) 

var uploadSingle = multer({
  storage: storage
}).single("file");

function awsUploadSingle(req, callback) {
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey
  });
  var s3 = new AWS.S3();
  var filePath = url + req.file.filename + "";

  var d = new Date();
  //configuring parameters
  var params = {
    Bucket: config.BucketName,
    Body: fs.createReadStream(filePath),
    // Key: "video/" + Date.now() + "_" + req.file.filename,
    Key:
      "upload/" + d.getFullYear() + "/" + d.getMonth() + "/" + req.file.filename
  };
  s3.upload(params, function(err, data) {
    //handle error
    if (err) {
      callback(err);
    }
    //success
    if (data) {
      callback(data.Location);
    }
  });
}

router.post("/actionHandlerPage", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.newsids };
        Pages.updateMany(condition, update_value, async function(err, log) {
          if (err) {
            res.send({ status: 1, message: "Something went wrong" + err });
          } else {
            res.send({ status: 1, message: "Pages Updated successfully" });
          }
        });
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

//--------------------------------------------------------


router.post("/updatePagedatabyId", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      console.log("-->", req.body, req.body.data.id);
      payload = jwt.verify(token, jwtKey);
      try {
        var update_value = {
          title: req.body.data.title,
          page_title: req.body.data.page_title,
          meta_tag: req.body.data.meta_tag,
          meta_desc: req.body.data.meta_desc,
          slug: req.body.data.slug,
          content: req.body.data.content
        };
        // const slug = req.body.data.slug.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
        const slug = req.body.data.slug;
        const id = req.body.data.id;
        var condition = {
          _id: id
        };
        Pages.findOneAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Page details changed successfully.",
              data: {
                user_data: log
              }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});


router.post("/getSinglePageByIdNew", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      console.log('id', req.body.id);
      payload = jwt.verify(token, jwtKey);
      try {
        Pages.findOne(
          {
            _id: req.body.id
          },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              console.log('logs are', log)
              res.send({
                status: 1,
                message: "success",
                data:  log
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Oops!jwt token malformed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});


module.exports = router;
