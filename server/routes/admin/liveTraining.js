/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");

/* Include schema */
const Location = require("../../models/event_locations");
const Training = require("../../models/liveTraining");
const Validate = require("../validations/validate");
const config = require("../../config/config.json");
const Settings = require("../../models/settings");
const EventUsers = require("../../models/event_users");
const LiveTraining = require("../../models/liveTraining");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();

/*  Get all types of News  */
router.post("/getAllTraining", async (req, res) => {
  const token = req.body.token;
  const filter = req.body.filter;
  let filterCondition = {};
  if (filter.date) {
    const selectedDate = filter.date.split("/");
    const start = new Date(selectedDate[1], selectedDate[0] - 1, 1);
    const end = new Date(selectedDate[1], selectedDate[0] - 1, 31);
    filterCondition = { eventStartDate: { $gte: start, $lte: end } };
  }

  if (filter.type) {
    filterCondition.eventStatus = filter.type;
  }
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Training.find(filterCondition, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Live Training successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Live Training available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/createTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Training.find({ slug: req.body.data.slug }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const newTraining = new Training({
              title: req.body.data.title,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              createdon: createdon,
              description: req.body.data.description,
              image: req.body.data.profile_image,
              content: req.body.data.content,
              facebook: req.body.data.facebook,
              twitter: req.body.data.twitter,
              linkedin: req.body.data.linkedin,
              email: req.body.data.email,
              date: req.body.data.date,
              eventStartDate: req.body.data.eventStartDate,
              eventStartTimeHour: req.body.data.eventStartTimeHour,
              eventStartTimeMinutes: req.body.data.eventStartTimeMinutes,
              eventStartTimeMeridiem: req.body.data.eventStartTimeMeridiem,
              eventEndDate: req.body.data.eventEndDate,
              eventEndTimeHour: req.body.data.eventEndTimeHour,
              eventEndTimeMinutes: req.body.data.eventEndTimeMinutes,
              eventEndTimeMeridiem: req.body.data.eventEndTimeMeridiem,
              allDayEvent: req.body.data.allDayEvent,
              hideEventTime: req.body.data.hideEventTime,
              hideEventEndTime: req.body.data.hideEventEndTime,
              eventLocation: req.body.data.eventLocation,
              eventCost: req.body.data.eventCost,
              eventStatus: req.body.data.eventStatus,
              eventLink: req.body.data.eventLink,
              eventId: req.body.data.eventId,
              page_status: 1
            });
            await newTraining
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have created a live training."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Training.findByIdAndDelete({ _id: req.body.id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Live Training deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});











router.post("/duplicateTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      
      Training.findOne({ _id: req.body.id }, async function(err, singleTraining) {
        if (err) {
          res.send({
            from: "err",
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          const createdon = moment().format("MM-DD-YYYY hh:mm:ss");
          const title = singleTraining.title + " copy";

          const slug = title
            .toLowerCase()
            .replace(/[^\w ]+/g, "")
            .replace(/ +/g, "-");
            Training.find({ slug: new RegExp(slug, "i") }, async function(
            err,
            response
          ) {
            if (err) {
              res.send({
                from: "err",
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {

              let createdon = moment().format("MM-DD-YYYY hh:mm:ss");


              const newSlug =
                slug + (response.length > 0 ? response.length : "");
              const newTitle =
                title + (response.length > 0 ? response.length : "");

              const newTraining = new Training({
                title: newTitle,
                slug: newSlug,
                page_title: singleTraining.page_title,
                content: singleTraining.content,
                created_on: createdon,
                description: singleTraining.description,
                image: singleTraining.image,
                content: singleTraining.content,
                facebook: singleTraining.facebook,
                twitter: singleTraining.twitter,
                linkedin: singleTraining.linkedin,
                email: singleTraining.email,
                date: singleTraining.date,
                eventStartDate: singleTraining.eventStartDate,
                eventStartTimeHour: singleTraining.eventStartTimeHour,
                eventStartTimeMinutes: singleTraining.eventStartTimeMinutes,
                eventStartTimeMeridiem:singleTraining.eventStartTimeMeridiem,
                eventEndDate: singleTraining.eventEndDate,
                eventEndTimeHour: singleTraining.eventEndTimeHour,
                eventEndTimeMinutes: singleTraining.eventEndTimeMinutes,
                eventEndTimeMeridiem:singleTraining.eventEndTimeMeridiem,
                allDayEvent: singleTraining.allDayEvent,
                hideEventTime: singleTraining.hideEventTime,
                hideEventEndTime: singleTraining.hideEventEndTime,
                eventLocation: singleTraining.eventLocation,
                eventCost: singleTraining.eventCost,
                eventStatus: singleTraining.eventStatus,
                eventLink: singleTraining.eventLink,
                eventId: singleTraining.eventId,
                page_status: 1
              });

              await newTraining
                .save()
                .then(() => {
                  let page_token = jwt.sign(
                    {
                      _id: newTraining._id,
                      title: newTraining.title
                    },
                    jwtKey
                  );
                  res.send({
                    status: 1,
                    message: "You have successfully duplicated Training.",
                    data: { 
                      token: page_token
                    }
                  });
                })
                .catch(err => {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                });
            }
          });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      } else {
        res.send({
          status: 0,
          message: "Oops! Something went wrong with JWT token verification...!"
        });
      }
    }
  }
});
















router.post("/getSingleTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Training.findOne({ _id: req.body.id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: { data: log }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/updateTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      Training.find(
        {
          slug: req.body.data.slug
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, ""),
          _id: { $ne: req.body.data._id }
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "This slug already exists, try another."
              });
            } else {
              var condition = { _id: req.body.data._id };
              var update_value = {
                title: req.body.data.title,
                slug: req.body.data.slug
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, ""),
                description: req.body.data.description,
                content: req.body.data.content,
                is_top_news: req.body.data.checked,
                image: req.body.data.profile_image,
                facebook: req.body.data.facebook,
                twitter: req.body.data.twitter,
                linkedin: req.body.data.linkedin,
                email: req.body.data.email,
                date: req.body.data.date,
                eventStartDate: req.body.data.eventStartDate,
                eventStartTimeHour: req.body.data.eventStartTimeHour,
                eventStartTimeMinutes: req.body.data.eventStartTimeMinutes,
                eventStartTimeMeridiem: req.body.data.eventStartTimeMeridiem,
                eventEndDate: req.body.data.eventEndDate,
                eventEndTimeHour: req.body.data.eventEndTimeHour,
                eventEndTimeMinutes: req.body.data.eventEndTimeMinutes,
                eventEndTimeMeridiem: req.body.data.eventEndTimeMeridiem,
                allDayEvent: req.body.data.allDayEvent,
                hideEventTime: req.body.data.hideEventTime,
                hideEventEndTime: req.body.data.hideEventEndTime,
                eventLocation: req.body.data.eventLocation,
                eventCost: req.body.data.eventCost,
                eventStatus: req.body.data.eventStatus,
                eventLink: req.body.data.eventLink,
                eventId: req.body.data.eventId
              };
              Training.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "This live training has updated successfully."
                  });
                }
              });
            }
          }
        }
      ).sort({ created_on: 1 });

      ///////////////////
      payload = jwt.verify(token, jwtKey);

      ////////////////
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/trashTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    payload = jwt.verify(token, jwtKey);

    try {
      var condition = { _id: req.body.id };
      var update_value = {
        page_status: 0
      };
      Training.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Event Location moved to trash successfully"
          });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/actionHandlerTraining", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.newsids };
        Training.updateMany(condition, update_value, async function(err, log) {
          if (err) {
            res.send({ status: 1, message: "Something went wrong" + err });
          } else {
            res.send({
              status: 1,
              message: "This live training has updated successfully."
            });
          }
        });
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/**Event Locations */
router.post("/createEventLocation", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    Validate.createEventLocation(req, result => {
      if (result.error) {
        //validation error
        res.send({
          status: 0,
          message: result.error.details[0].message
        });
      } else {
        try {
          payload = jwt.verify(token, jwtKey);
          Location.find(
            {
              slug: req.body.data.location_name
                .trim()
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, "")
            },
            async function(err, log) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              } else {
                if (log.length > 0) {
                  res.send({
                    status: 0,
                    message: "Location already exist, please enter another"
                  });
                } else {
                  let created_on = moment().format("MM-DD-YYYY hh:mm:ss");
                  const newLocation = new Location({
                    location_name: req.body.data.location_name,
                    slug: req.body.data.location_name
                      .trim()
                      .toLowerCase()
                      .replace(/ /g, "-")
                      .replace(/[^\w-]+/g, ""),
                    created_on: created_on,
                    description: req.body.data.description,
                    address: req.body.data.address,
                    latitude: req.body.data.latitude,
                    longitude: req.body.data.longitude,
                    page_status: 1
                  });
                  await newLocation
                    .save()
                    .then(() => {
                      res.send({
                        status: 1,
                        message:
                          "You have successfully created an event location."
                      });
                    })
                    .catch(err => {
                      res.send({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message
                      });
                    });
                }
              }
            }
          ).sort({ created_on: 1 });
        } catch (e) {
          res.send({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message
          });
        }
      }
    });
  }
});

router.post("/getAllEventLocation", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Location.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched event locations successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No event locations available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteEventLocation", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Location.findByIdAndDelete({ _id: req.body.id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({
              status: 1,
              message: "Event Location deleted successfully"
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/actionHandlerEventLocation", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.ids };
        Location.updateMany(condition, update_value, async function(err, log) {
          if (err) {
            res.send({ status: 1, message: "Something went wrong" + err });
          } else {
            res.send({
              status: 1,
              message: "Event Locations Updated successfully"
            });
          }
        });
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getSingleEventLocation", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Location.findOne({ _id: req.body.id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: { data: log }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/updateEventLocation", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    payload = jwt.verify(token, jwtKey);

    Validate.updateEventLocation(req, result => {
      if (result.error) {
        //validation error
        res.send({
          status: 0,
          message: result.error.details[0].message
        });
      } else {
        try {
          Location.find(
            {
              slug: req.body.data.location_name
                .trim()
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              _id: { $ne: req.body.data._id }
            },
            async function(err, log) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              } else {
                if (log.length > 0) {
                  res.send({
                    status: 0,
                    message: "Location already exist."
                  });
                } else {
                  var condition = { _id: req.body.data._id };
                  var update_value = {
                    location_name: req.body.data.location_name,
                    slug: req.body.data.location_name
                      .toLowerCase()
                      .replace(/ /g, "-")
                      .replace(/[^\w-]+/g, ""),
                    description: req.body.data.description,
                    address: req.body.data.address,
                    latitude: req.body.data.latitude,
                    longitude: req.body.data.longitude
                  };
                  Location.findOneAndUpdate(
                    condition,
                    update_value,
                    async function(err, log) {
                      if (err) {
                        res.send({
                          status: 0,
                          message: "Oops! " + err.name + ": " + err.message
                        });
                      } else {
                        res.send({
                          status: 1,
                          message: "The event location has been updated successfully."
                        });
                      }
                    }
                  );
                }
              }
            }
          ).sort({ created_on: 1 });
        } catch (e) {
          if (e instanceof jwt.JsonWebTokenError) {
            res.send({ status: 0, message: "Token verification failed." });
          }
          res.send({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message
          });
        }
      }
    });
  }
});
router.post("/trashEventLocation", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    payload = jwt.verify(token, jwtKey);

    try {
      var condition = { _id: req.body.id };
      var update_value = {
        page_status: 0
      };
      Location.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Event Location moved to trash successfully"
          });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getAllActiveEventLocation", async (req, res) => {
  const token = req.body.token;
  const status = req.body.status;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Location.find({ page_status: status }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched event locations successfully.",
              locations: log
            });
          } else {
            res.send({
              status: 0,
              message: "No event locations available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/* List date filter options */
router.post("/getDateFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Training.aggregate(
        [
          {
            $project: {
              year: { $year: "$eventStartDate" },
              month: { $month: "$eventStartDate" }
            }
          },
          {
            $group: {
              _id: null,
              eventStartDate: { $addToSet: { year: "$year", month: "$month" } }
            }
          },
          { $sort: { _id: -1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/* List type filter options */
router.post("/getTypeFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Training.aggregate(
        [
          {
            $project: {
              eventStatus: 1
            }
          },
          {
            $group: {
              _id: "$eventStatus"
            }
          },
          { $sort: { eventStatus: 1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
router.post("/trashTraining", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    payload = jwt.verify(token, jwtKey);

    try {
      var condition = { _id: req.body.id };
      var update_value = {
        page_status: 0
      };
      Training.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Event Location moved to trash successfully"
          });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/actionHandlerTraining", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.newsids };
        if (req.body.action == "3") {
          Training.deleteMany(
            { _id: { $in: req.body.newsids } },
            async function(err, log) {
              if (err) {
                res.send({ status: 1, message: "Something went wrong" + err });
              } else {
                res.send({
                  status: 1,
                  message: "Live Training Trash deleted successfully."
                });
              }
            }
          );
        } else {
          Training.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({
                status: 1,
                message: "This live training has updated successfully."
              });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});







/*  Get all types of News  */
router.post("/getAllTrainingPublished", async (req, res) => {
  const token = req.body.token;
  const filter = req.body.filter;
  let filterCondition = {};
  Training.find({ page_status: 1 }, async function(err, log) {
    if (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    } else {
      if (log.length > 0) {
        res.send({
          status: 1,
          message: "Fetched event Training successfully.",
          data: log
        });
      } else {
        res.send({
          status: 0,
          message: "No event Training available."
        });
      }
    }
  }).sort({ date: -1 });

   
});



/* get all Event traning s */

/*Get all  Feeback email  */
router.post("/getEventTranining", async (req, res) => {
  try {
    Settings.find({
      slug : 'eventtraining'
    }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (log.length > 0) {
          res.send({
            status: 1,
            message: "Event Training Successfully.",
            data: log
          });
        } else {
          res.send({
            status: 0,
            message: "No FeedBack available."
          });
        }
      }
    }).sort({
      _id: -1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});


/* update updateEventTraining  */
router.post("/updateEventTraining", async (req, res) => {
        const token = req.body.token;
        if (!token) {
        res.send({ status: 0, message: "Invalid Request" });
        } else {
        try {

              var condition = { slug: 'eventtraining' };

  
              var update_value = {
                value: req.body.data,                
              };

              console.log('update_value', req.body.data);
            
              Settings.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "Live Training section updated successfully."
                  });
                }
              }); 
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});




/*Get all  Get User Training  */
router.post("/getUserTraining", async (req, res) => {
  try {
 
    EventUsers.aggregate([
      // { $match: {'user_id' : mongoose.Types.ObjectId(user_id)}},
      { $lookup:
         {
           from: 'livetrainings',
           localField: 'event_id',
           foreignField: "_id",
           as: 'Trainings'
         },
         
       },
       {
        $unwind: {
            path: "$Trainings",
            preserveNullAndEmptyArrays: false
        }
       },
       { $lookup:
        {
          from: 'users',
          localField: 'user_id',
          foreignField: "_id",
          as: 'users'
        },
        
      },
      { $sort : { "livetrainings.created_on" : 1 } }
      //  { $match: {'user_details.user_id' : mongoose.Types.ObjectId(user_id)}},
      ],async function (err, response) {
        if (err) {
          res.send({   
              status:0,         
              message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
              res.send({ 
              status: 1,             
              message: "Fetched liveTraining details successfully.",
              data: response
              });
          }else{
              res.send({
              status: 0,
              message: "Details Unvailable."
              });
          }
        }
      });
    
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});




/*Get all  Get User Training  */
router.post("/getUsersByTraningId", async (req, res) => {
  try {
    console.log('getUsersByTraningId', req.body.data);
    EventUsers.aggregate([
      // { $match: {'user_id' : mongoose.Types.ObjectId(user_id)}},
      { $lookup:
         {
           from: 'livetrainings',
           localField: 'event_id',
           foreignField: "_id",
           as: 'Trainings'
         },
         
       },
      //  {
      //   $unwind: {
      //       path: "$Trainings",
      //       preserveNullAndEmptyArrays: false
      //   }
      //  },
       { $lookup:
        {
          from: 'users',
          localField: 'user_id',
          foreignField: "_id",
          as: 'users'
        },
        
      },
      { $sort : { "livetrainings.created_on" : 1 } },
    //  { $match: {'Trainings._id' : req.body.data}},
      ],async function (err, response) {
        if (err) {
          res.send({   
              status:0,         
              message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
            

            var result = response.filter(function(e, i) {
              console.log('response',e.Trainings[0]);
              if(e.Trainings.length > 0){
                return e.event_id == req.body.data
              }
             
           
       //       return res[i] == 'y'
            })

            res.send({ 
              status: 1,             
              message: "Fetched liveTraining details successfully.",
              data: result
              });
            // if(result){
            
            // }
          }else{
              res.send({
              status: 0,
              message: "Details Unvailable."
              });
          }
        }
      });
    
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});



module.exports = router;
