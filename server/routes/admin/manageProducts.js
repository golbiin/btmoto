//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const Categories = require("../../models/categories");
//Include schema
const Products = require("../../models/products");

const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
var async = require("async");
//http request port set
const router = express.Router();

router.post("/getAllProducts", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      Products.aggregate([
        {
          $lookup: {
            from: "categories",
            localField: "categories",
            foreignField: "_id",
            as: "category_info"
          }
        }
      ]).exec((err, posts) => {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Success!",
            data: posts
          });
        }
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/createProduct", async (req, res) => {
  const token = req.body.token;

  //res.send({ status: imagarray, message: "Invalid Request" });
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Products.find({ slug: req.body.data.slug }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");

            const newProducts = new Products({
              product_name: req.body.data.product_name,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              created_on: createdon,
              short_description: req.body.data.short_description,
              description: req.body.data.description,
              specifications: req.body.data.specifications,
              categories: req.body.data.categories,
              price: req.body.data.price,
              inventory: req.body.data.inventory,
              images: req.body.data.images,
              downloads: req.body.data.downloads,
              related_products: req.body.data.related_products,
              primary_category: req.body.data.primary_category,
              attributes: req.body.data.attributes,
              tabdata: req.body.data.tabdata,
              url: req.body.data.url,
              banner_description: req.body.data.banner_description,
              product_sub_text: req.body.data.product_sub_text,
              model_number: req.body.data.model_number,
              meta_tag: req.body.data.meta_tag,
              meta_description: req.body.data.meta_description,
              orderno : req.body.data.orderno,
              product_tag: req.body.data.product_tag,
              packages: req.body.data.packages,
              page_status: 1,
              productAttribute : req.body.data.productAttribute,
              downloadbtn : req.body.data.downloadbtn 
            });
            await newProducts
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have successfully added a new product."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteProduct", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Products.findByIdAndDelete({ _id: req.body.product_id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Product deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/duplicateProduct", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {
        Products.findOne({ _id: req.body.product_id }, async function(
          err,
          product
        ) {
          if (err) {
            res.send({
              from: "err",
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const product_name = product.product_name + " copy";
            const slug = product_name
              .toLowerCase()
              .replace(/ /g, "-")
              .replace(/[^\w-]+/g, "");
            console.log(slug);
            Products.find({ slug: new RegExp(slug, "i") }, async function(
              err,
              response
            ) {
              if (err) {
                res.send({
                  from: "err",
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              } else {
                const newSlug =
                  slug + (response.length > 0 ? response.length : "");
                const newProductName =
                  product_name + (response.length > 0 ? response.length : "");
                const newProducts = new Products({
                  product_name: newProductName,
                  slug: newSlug,
                  created_on: createdon,
                  short_description: product.short_description,
                  description: product.description,
                  specifications: product.specifications,
                  categories: product.categories,
                  price: product.price,
                  inventory: product.inventory,
                  images: product.images,
                  downloads: product.downloads,
                  related_products: product.related_products,
                  primary_category: product.primary_category,
                  attributes: product.attributes,
                  url: product.url,
                  banner_description: product.banner_description,
                  product_sub_text: product.product_sub_text,
                  model_number: product.model_number,
                  packages: product.packages,
                  page_status: 1,
                  duplicate_count: 0
                });
                await newProducts
                  .save()
                  .then(() => {
                    res.send({
                      status: 1,
                      message: "You have successfully duplicated a product.",
                      product: newProducts
                    });
                  })
                  .catch(err => {
                    res.send({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message,
                      from: "create_product catch"
                    });
                  });
              }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      } else {
        res.send({
          status: 0,
          message: "Oops! Something went wrong with JWT token verification...!"
        });
      }
    }
  }
});

router.post("/getAllProductsIdsandNames", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Products.find({
        page_status: "1"
      }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Products successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Product available."
            });
          }
        }
      })
        .sort({ date: -1 })
        .select("product_name _id");
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getListCatNames", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Products.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Products successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Product available."
            });
          }
        }
      })
        .sort({ date: -1 })
        .select("product_name _id");
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getSingleProduct", async (req, res) => {
  console.log('getSingleproduct--->123');
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Products.findOne({ _id: req.body.p_id }, {}, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: { product_data: log }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/updateProduct", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      Products.find(
        {
          slug: req.body.data.slug
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, ""),
          _id: { $ne: req.body.data._id }
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "This slug already exists, try another."
              });
            } else {
              var condition = { _id: req.body.data._id };
              var update_value = {
                product_name: req.body.data.product_name,
                slug: req.body.data.slug
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, ""),
                url: req.body.data.url,
                short_description: req.body.data.short_description,
                description: req.body.data.description,
                specifications: req.body.data.specifications,
                images: req.body.data.images,
                downloads: req.body.data.downloads,
                price: req.body.data.price,
                inventory: req.body.data.inventory,
                categories: req.body.data.categories,
                tabdata: req.body.data.tabdata,
                inventory: req.body.data.inventory,
                related_products: req.body.data.related_products,
                primary_category: req.body.data.primary_category,
                attributes: req.body.data.attributes,
                banner_description: req.body.data.banner_description,
                product_sub_text: req.body.data.product_sub_text,
                model_number: req.body.data.model_number,
                packages: req.body.data.packages,
                meta_tag: req.body.data.meta_tag,
                meta_description: req.body.data.meta_description,
                product_tag: req.body.data.product_tag,
                productAttribute : req.body.data.productAttribute,
                downloadbtn : req.body.data.downloadbtn,
                downloadcontent1 : req.body.data.downloadcontent1,
                orderno : req.body.data.orderno,
              };

              Products.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "You have successfully updated this product."
                  });
                }
              });
            }
          }
        }
      ).sort({ created_on: 1 });
      payload = jwt.verify(token, jwtKey);
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getCurrentproductUrl", async (req, res) => {
  const token = req.body.token;
  const cat_id = req.body.cat_id;
  const location = req.body.location;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      async.waterfall(
        [
          function(callback) {
            Categories.find({ _id: cat_id }, async function(err, Current_cat) {
              if (err) {
                callback(null, "", "");
              } else {
                if (Current_cat.length > 0) {
                  const parent_id = Current_cat[0].parent_id;

                  if (parent_id) {
                    Categories.find({ _id: parent_id }, async function(
                      err,
                      Parent_cat
                    ) {
                      if (err) {
                        if (location === "category") {
                          callback(
                            null,
                            "introduction/" + Current_cat[0].slug,
                            null
                          );
                        } else {
                          callback(null, Current_cat[0].slug, null);
                        }
                      } else {
                        if (Parent_cat.length > 0) {
                          callback(null, Current_cat[0].slug, Parent_cat);
                        } else {
                          callback(null, Current_cat[0].slug, null);
                        }
                      }
                    })
                      .collation({ locale: "en" })
                      .sort({ category_name: 1 });
                  } else {
                    callback(null, Current_cat[0].slug, null);
                  }
                } else {
                  callback(null, null, null);
                }
              }
            })
              .collation({ locale: "en" })
              .sort({ category_name: 1 });
          },
          function(Current_cat, Parent_cat, callback) {
            let Finalurl = "/" + Current_cat;

            if (Parent_cat) {
              if (Parent_cat.length > 0) {
                Finalurl = "/" + Parent_cat[0].slug + Finalurl;
                let SuperParent = Parent_cat[0].parent_id;
                if (SuperParent) {
                  Categories.find({ _id: SuperParent }, async function(
                    err,
                    SuperParent_cat
                  ) {
                    if (err) {
                      callback(null, Finalurl);
                    } else {
                      if (SuperParent_cat.length > 0) {
                        Finalurl = "/" + SuperParent_cat[0].slug + Finalurl;
                        callback(null, Finalurl);
                      } else {
                        callback(null, Finalurl);
                      }
                    }
                  })
                    .collation({ locale: "en" })
                    .sort({ category_name: 1 });
                }
              } else {
                callback(null, Finalurl);
              }
            } else {
              callback(null, Finalurl);
            }
          }
        ],
        function(err, result) {
          // result now equals 'done'
          if (err) {
            res.send({
              status: 0,
              message: "No categories available."
            });
          } else {
            res.send({
              status: 1,
              message: "Fetched categories successfully.",
              data: result
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/actionHandlerProduct", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.productids };
        Products.updateMany(condition, update_value, async function(err, log) {
          if (err) {
            res.send({ status: 1, message: "Something went wrong" + err });
          } else {
            res.send({
              status: 1,
              message: "You have successfully updated this product."
            });
          }
        });
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

// product get

router.post("/getAllProductsCat", async (req, res) => {
  const token = req.body.token;
  const category = req.body.category;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      Products.find(
        {
          categories: {
            $in: category
          }
        },
        async function(err, products) {
          if (err) {
            res.send({
              status: 0,
              message: "Error fetching data"
            });
          } else {
            res.send({
              status: 1,
              message: "Success!",
              data: products
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

// get category

function getParentCateogery(callback) {
  Categories.find({ parent_id: 0 })
    .collation({ locale: "en" })
    .sort({ category_name: 1 })
    .exec(callback);
}
function getChildCategory(callback) {
  Categories.find({ parent_id: { $ne: 0 } })
    .collation({ locale: "en" })
    .sort({ category_name: 1 })
    .exec(callback);
}

function getChildCategoryNew(Parentid, callback) {
  Categories.find({ parent_id: { $eq: Parentid } })
    .sort({ _id: 1 })
    .exec(callback);
}

function getChildCategorylast(Parentid, callback) {
  Categories.find({ parent_id: Parentid }, async function(err, log) {
    if (err) {
      callback(err);
    } else {
      if (log.length > 0) {
        callback(log);
      } else {
        callback(err);
      }
    }
  }).sort({ name: -1 });
}

router.post("/getAllCategory", async (req, res) => {
  const token = req.body.token;
  try {
    async.parallel(
      {
        ParentCat: getParentCateogery,
        ChildCat: getChildCategory
      },
      function(error, results) {
        if (error) {
          res.send({
            status: 0,
            message: "No categories available."
          });
        } else {
          //
          const Items = [];

          results.ParentCat.forEach(function(item) {
            Items.push(item);
            results.ChildCat.forEach(function(childitem) {
              if (item._id == childitem.parent_id) {
                let changename = childitem.category_name;
                childitem.category_name = " - " + changename;

                Items.push(childitem);
              }
            });
          });

          res.send({
            status: 1,
            message: "Fetched categories successfully.",
            data: Items
          });
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/updateCategoryShort", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    console.log("req.body.data", req.body.data);
    try {
      Products.find(
        {
          slug: req.body.data.slug
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, ""),
          _id: { $ne: req.body.data._id }
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "This slug already exists, try another."
              });
            } else {
              var condition = { _id: req.body.data._id };
              var update_value = {
                url: req.body.data.url,
                categories: req.body.data.categories,
                primary_category: req.body.data.primary_category
              };

              Products.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "You have updated this category succesfully."
                  });
                }
              });
            }
          }
        }
      ).sort({ created_on: 1 });
      payload = jwt.verify(token, jwtKey);
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
router.post("/trashProduct", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Products.findByIdAndUpdate(
          { _id: req.body.product_id },
          { page_status: 0 },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "Product Trashed" });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/actionHandlerProduct", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.productids };
        if (req.body.action == "3") {
          Products.deleteMany(
            { _id: { $in: req.body.productids } },
            async function(err, log) {
              if (err) {
                res.send({ status: 1, message: "Something went wrong" + err });
              } else {
                res.send({
                  status: 1,
                  message: "Trashed products deleted successfully."
                });
              }
            }
          );
        } else {
          Products.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({
                status: 1,
                message: "You have successfully updated this product."
              });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

module.exports = router;
