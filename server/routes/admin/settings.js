//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");

//Include schema

const Categories = require("../../models/categories");
const News = require("../../models/news");
const Articles = require("../../models/articles");

const Archives = require("../../models/archives");
const Language = require("../../models/language");
const Careers = require("../../models/careers");
const Pages = require("../../models/pages");
const Footer = require("../../models/footer_columns");
const Menus = require("../../models/menus");
const config = require("../../config/config.json");
const Stripe = require("../../models/stripe");
const Facebook = require("../../models/facebook");
const jwtKey = config.jwsPrivateKey;
const Validate = require("../validations/validate");
const Distributor = require("../../models/international_distributor");

const Orders = require("../../models/orders");
const FeedBack = require("../../models/feedBack");
const Settings = require("../../models/settings");
//http request port set
const router = express.Router();

router.post("/createCategory", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      Categories.find({ slug: req.body.data.slug.trim() }, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const newCategory = new Categories({
              category_name: req.body.data.category_name,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              // attributes: req.body.data.attributes,
              status: 1,
              created_on: createdon,
              descrption: req.body.data.descrption,
              short_description: req.body.data.short_description,
              parent_id: req.body.data.parent_category,
              page_type: req.body.data.page_type,
              page_layout: req.body.data.page_layout,
              image: req.body.data.profile_image,
              order : req.body.data.order
            });
            await newCategory
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have created a new category."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getAllCategories-old", async (req, res) => {
  try {
    async.parallel(
      {
        ParentCat: getParentCateogery,
        ChildCat: getChildCategory
      },
      function(error, results) {
        if (error) {
          res.send({
            status: 0,
            message: "No categories available."
          });
        } else {
          const Items = [];

          results.ParentCat.forEach(function(item) {
            Items.push(item);
            results.ChildCat.forEach(function(childitem) {
              if (item._id == childitem.parent_id) {
                Items.push(childitem);
              }
            });
          });

          res.send({
            status: 1,
            message: "Fetched categories successfully.",
            data: Items
          });
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/getAllCategories", async (req, res) => {
  try {
    async.waterfall(
      [
        function(callback) {
          Categories.find({ parent_id: 0 }, async function(err, Parent_cat) {
            if (err) {
              callback(null, "a", "b");
            } else {
              if (Parent_cat.length > 0) {
                Categories.find({ parent_id: { $ne: 0 } }, async function(
                  err,
                  child_cat
                ) {
                  if (err) {
                    callback(null, Parent_cat, []);
                  } else {
                    if (child_cat.length > 0) {
                      callback(null, Parent_cat, child_cat);
                    } else {
                      callback(null, Parent_cat, []);
                    }
                  }
                })
                  .collation({ locale: "en" })
                  .sort({ order: 1  ,category_name: 1});
              } else {
                callback(null, [], []);
              }
            }
          })
            .collation({ locale: "en" })
            .sort({ order: 1 ,category_name: 1 });
        },
        function(ParentCat, ChildCat, callback) {
          // arg1 now equals 'one' and arg2 now equals 'two'

          const Items = [];

          ParentCat.forEach(function(item) {
            item.parent_id = "level1";
            item.userrrrr = "level1";
            Items.push(item);
            ChildCat.forEach(function(childitem) {
              if (item._id == childitem.parent_id) {
                childitem.parent_id = "level2";
                childitem.userrrrr = "level3";
                Items.push(childitem);
              }
            });
          });

          const Thrid_child_left = ChildCat;

          Items.forEach(function(sortableitem) {
            for (var i = 0; i < Thrid_child_left.length; i++) {
              var obj = Thrid_child_left[i];
              if (sortableitem._id === obj._id) {
                Thrid_child_left.splice(i, 1);
              }
            }
          });

          callback(null, Items, Thrid_child_left);
        },
        function(Items, Thrid_child_left, callback) {
          // arg1 now equals 'three'

          const AllItems = [];

          Items.forEach(function(item) {
            AllItems.push(item);
            Thrid_child_left.forEach(function(childitem) {
              if (item._id == childitem.parent_id) {
                // const source = { b: 4, c: 5 };
                // const returnedTarget = {...childitem , user_type : source};

                //childitem.push({country: IN});

                childitem.parent_id = "level3";
                childitem.userrrrr = "level3";

                AllItems.push(childitem);
              }
            });
          });

          callback(null, AllItems);
        }
      ],
      function(err, result) {
        // result now equals 'done'
        if (err) {
          res.send({
            status: 0,
            message: "No categories available."
          });
        } else {
          res.send({
            status: 1,
            message: "Fetched categories successfully.",
            data: result
          });
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/getAllProductCategories", async (req, res) => {
  // try {
  //   async.parallel({
  //     ParentCat: getParentCateogery,
  //     ChildCat: getChildCategory
  //   }, function (error, results) {
  //     if (error) {

  //       res.send({
  //         status:0,
  //         message: "No categories available.",
  //       });

  //     } else {

  //       const Items =[];

  //       results.ParentCat.forEach(function(item){
  //          Items.push(item);
  //           results.ChildCat.forEach(function(childitem){

  //             if(item._id == childitem.parent_id){
  //               Items.push(childitem);
  //             }

  //           });
  //       });

  //         res.send({
  //           status: 1,
  //           message: "Fetched categories successfully.",
  //           data: Items
  //         });

  //     }

  //   });
  // } catch (e) {
  //   res.send({
  //     status: 0,
  //     message: "Oops! " + e.name + ": " + e.message,
  //   });
  // }

  try {
    async.waterfall(
      [
        function(callback) {
          Categories.find({ parent_id: 0 }, async function(err, Parent_cat) {
            if (err) {
              callback(null, "a", "b");
            } else {
              if (Parent_cat.length > 0) {
                Categories.find({ parent_id: { $ne: 0 } }, async function(
                  err,
                  child_cat
                ) {
                  if (err) {
                    callback(null, Parent_cat, []);
                  } else {
                    if (child_cat.length > 0) {
                      callback(null, Parent_cat, child_cat);
                    } else {
                      callback(null, Parent_cat, []);
                    }
                  }
                })
                  .collation({ locale: "en" })
                  .sort({ category_name: 1 });
              } else {
                callback(null, [], []);
              }
            }
          })
            .collation({ locale: "en" })
            .sort({ category_name: 1 });
        },
        function(ParentCat, ChildCat, callback) {
          // arg1 now equals 'one' and arg2 now equals 'two'

          const Items = [];

          ParentCat.forEach(function(item) {
            item.parent_id = "level1";
            item.userrrrr = "level1";
            Items.push(item);
            ChildCat.forEach(function(childitem) {
              if (item._id == childitem.parent_id) {
                childitem.parent_id = "level2";
                childitem.userrrrr = "level3";
                Items.push(childitem);
              }
            });
          });

          const Thrid_child_left = ChildCat;

          Items.forEach(function(sortableitem) {
            for (var i = 0; i < Thrid_child_left.length; i++) {
              var obj = Thrid_child_left[i];
              if (sortableitem._id === obj._id) {
                Thrid_child_left.splice(i, 1);
              }
            }
          });

          callback(null, Items, Thrid_child_left);
        },
        function(Items, Thrid_child_left, callback) {
          // arg1 now equals 'three'

          const AllItems = [];

          Items.forEach(function(item) {
            AllItems.push(item);
            Thrid_child_left.forEach(function(childitem) {
              if (item._id == childitem.parent_id) {
                // const source = { b: 4, c: 5 };
                // const returnedTarget = {...childitem , user_type : source};

                //childitem.push({country: IN});

                childitem.parent_id = "level3";
                childitem.userrrrr = "level3";

                AllItems.push(childitem);
              }
            });
          });

          callback(null, AllItems);
        }
      ],
      function(err, result) {
        // result now equals 'done'
        if (err) {
          res.send({
            status: 0,
            message: "No categories available."
          });
        } else {
          res.send({
            status: 1,
            message: "Fetched categories successfully.",
            data: result
          });
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/getselctedproductCatogery", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Categories.find({ status: 1 }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched categories successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No categories available."
            });
          }
        }
      })
        .sort({ name: -1 })
        .distinct("_id");
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

function getParentCateogery(callback) {
  Categories.find({ parent_id: 0 })
    .collation({ locale: "en" })
    .sort({ category_name: 1 })
    .exec(callback);
}
function getChildCategory(callback) {
  Categories.find({ parent_id: { $ne: 0 } })
    .collation({ locale: "en" })
    .sort({ category_name: 1 })
    .exec(callback);
}

function getChildCategoryNew(Parentid, callback) {
  Categories.find({ parent_id: { $eq: Parentid } })
    .sort({ _id: 1 })
    .exec(callback);
}

function getChildCategorylast(Parentid, callback) {
  Categories.find({ parent_id: Parentid }, async function(err, log) {
    if (err) {
      callback(err);
    } else {
      if (log.length > 0) {
        callback(log);
      } else {
        callback(err);
      }
    }
  }).sort({ name: -1 });
}

router.post("/getAllParentCategory", async (req, res) => {
  const token = req.body.token;

  try {
    async.parallel(
      {
        ParentCat: getParentCateogery,
        ChildCat: getChildCategory
      },
      function(error, results) {
        if (error) {
          res.send({
            status: 0,
            message: "No categories available."
          });
        } else {
          const Items = [];

          results.ParentCat.forEach(function(item) {
            Items.push(item);
            results.ChildCat.forEach(function(childitem) {
              if (item._id == childitem.parent_id) {
                let changename = childitem.category_name;
                childitem.category_name = " - " + changename;

                Items.push(childitem);
              }
            });
          });

          res.send({
            status: 1,
            message: "Fetched categories successfully.",
            data: Items
          });
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
  //     if (!token) {
  //       res.send({ status: 0, message: "Invalid Request" });
  //     } else {
  //     try {
  //     payload = jwt.verify(token, jwtKey);
  //     Categories.find(
  //         { status: 1 },
  //         async function (err, log) {
  //           if (err) {
  //             res.send({
  //               status: 0,
  //               message: "Oops! " + err.name + ": " + err.message,
  //             });
  //           } else {
  //             if(log.length>0){
  //               res.send({
  //                 status: 1,
  //                 message: "Fetched categories successfully.",
  //                 data: log
  //               });
  //             }else{
  //               res.send({
  //                 status: 0,
  //                 message: "No categories available."
  //               });
  //             }
  //           }
  //         }
  //       ).sort({ _id: -1 });
  //     } catch (e) {
  //       res.send({
  //         status: 0,
  //         message: "Oops! " + e.name + ": " + e.message,
  //       });
  //     }
  // }
});

router.post("/removeCategory", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Categories.findByIdAndDelete({ _id: req.body.data._id }, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          var update_value = {
            parent_id: 0
          };

          try {
            var condition = { parent_id: req.body.data._id };
            Categories.updateMany(condition, update_value, async function(
              err,
              log
            ) {
              if (err) {
                res.send({ status: 1, message: "Something went wrong" });
              } else {
                res.send({
                  status: 1,
                  message: "You have successfully deleted this category."
                });
              }
            });
          } catch (e) {
            res.send({ status: 1, message: "Something went wrong" });
          }

          // if(log){
          //   res.send({ status: 1, message: "You have successfully deleted this category." });
          // } else {
          //   res.send({ status: 1, message: "Something went wrong" });
          // }
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getCategoryBySlug", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Categories.findOne(
        { status: 1, slug: req.body.data.slug.trim() },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log) {
              res.send({
                status: 1,
                message: "Fetched Category details successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      ).sort({ created_on: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/updateCategory", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var condition = { slug: req.body.data.slug };
      var update_value = {
        category_name: req.body.data.category_name,
        parent_id: req.body.data.parent_category,
        descrption: req.body.data.descrption,
        content: req.body.data.content,
        page_type: req.body.data.page_type,
        page_layout: req.body.data.page_layout,
        image: req.body.data.profile_image,
        banner_sub_description: req.body.data.banner_sub_description,
        manualUrl: req.body.data.manualUrl,
        downloadUrl: req.body.data.downloadUrl,
        banner_main_description: req.body.data.banner_main_description,
        short_description: req.body.data.short_description,
        order : req.body.data.order 
      };
      Categories.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({ status: 1, message: "You have updated this category succesfully." });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/********************************************************* */

router.post("/getAllNews", async (req, res) => {
  const token = req.body.token;
  const filter = req.body.filter;
  let filterCondition = {};
  if (filter.date) {
    const selectedDate = filter.date.split("/");
    const start = new Date(selectedDate[1], selectedDate[0] - 1, 1);
    const end = new Date(selectedDate[1], selectedDate[0] - 1, 31);
    filterCondition = { date: { $gte: start, $lte: end } };
  }
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      News.find(filterCondition, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched news successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No news available."
            });
          }
        }
      }).sort({ _id: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/createNews", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      News.find({ slug: req.body.data.slug }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const newNews = new News({
              title: req.body.data.title,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              date: createdon,
              description: req.body.data.description,
              image: req.body.data.profile_image,
              content: req.body.data.content,
              is_top_news: req.body.data.checked,
              meta_tag: req.body.data.meta_tag,
              meta_desc: req.body.data.meta_desc,
              image_thumb :   req.body.data.profile_image_thumb,
              news_order: req.body.data.news_order,
            });
            await newNews
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have successfully added a new News."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ date: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteNews", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        News.findByIdAndDelete({ _id: req.body.news_id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "News deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/trashNews", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        News.findByIdAndUpdate(
          { _id: req.body.news_id },
          { page_status: 0 },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "News Trashed" });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/getSingleNews", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        News.findOne(
          { _id: req.body.news_id },
          {
            _id: 1,
            is_top_news: 1,
            title: 1,
            slug: 1,
            image: 1,
            image_thumb : 1,
            content: 1,
            description: 1,
            meta_tag: 1,
            meta_desc: 1,
            news_order: 1
          },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: { news_data: log }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/updateNews", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      News.find(
        {
          slug: req.body.data.slug
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, ""),
          _id: { $ne: req.body.data._id }
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "This slug already exists, try another."
              });
            } else {
              var condition = { _id: req.body.data._id };
              var update_value = {
                title: req.body.data.title,
                slug: req.body.data.slug
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, ""),
                description: req.body.data.description,
                content: req.body.data.content,
                is_top_news: req.body.data.checked,
                image: req.body.data.profile_image,
                meta_tag: req.body.data.metatag,
                meta_desc: req.body.data.metadescription,
                image_thumb :   req.body.data.profile_image_thumb,
                news_order: req.body.data.news_order,
              };
              News.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({ status: 1, message: "News Updated successfully" });
                }
              });
            }
          }
        }
      ).sort({ date: 1 });

      ///////////////////
      payload = jwt.verify(token, jwtKey);

      ////////////////
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
router.post("/actionHandlerNews", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.newsids };
        if (req.body.action == "3") {
          News.deleteMany({ _id: { $in: req.body.newsids } }, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({
                status: 1,
                message: "News Trash deleted successfully."
              });
            }
          });
        } else {
          News.updateMany(condition, update_value, async function(err, log) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "News Updated successfully" });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
/* List date filter options */
router.post("/getDateFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      News.aggregate(
        [
          {
            $project: {
              year: { $year: "$date" },
              month: { $month: "$date" }
            }
          },
          {
            $group: {
              _id: null,
              date: { $addToSet: { year: "$year", month: "$month" } }
            }
          },
          { $sort: { _id: -1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/********************************************************** */

router.post("/getAllCareers", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Careers.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched careers successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No careers available."
            });
          }
        }
      }).sort({ _id: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteCareer", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Careers.findByIdAndDelete({ _id: req.body.id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Career deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/actionHandlercareer", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.ids };
        if (req.body.action == "3") {
          Careers.deleteMany({ _id: { $in: req.body.ids } }, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({
                status: 1,
                message: "Careers Trash deleted successfully"
              });
            }
          });
        } else {
          Careers.updateMany(condition, update_value, async function(err, log) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "Careers Updated successfully" });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
router.post("/trashCareer", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Careers.findByIdAndUpdate(
          { _id: req.body.id },
          { page_status: 0 },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "Career Trashed" });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/createCareer", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Careers.find({ slug: req.body.data.slug }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const newCareer = new Careers({
              job_name: req.body.data.job_name,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              meta_tag: req.body.data.meta_tag,
              meta_desc: req.body.data.meta_desc,
              post_date: createdon,
              job_type: req.body.data.job_type,
              location: req.body.data.job_location,
              related_cat: req.body.data.related_cat,
              content: req.body.data.content
            });
            await newCareer
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have created a Career to the Cimon"
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getSingleCareer", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Careers.findOne(
          { _id: req.body.id },
          {
            _id: 1,
            job_name: 1,
            slug: 1,
            meta_tag: 1,
            meta_desc: 1,
            related_cat: 1,
            job_type: 1,
            content: 1,
            location: 1,
            post_date: 1
          },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: { career_data: log }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/updateCareer", async (req, res) => {
  // res.send({ status: req.body.data.job_location, message: "Invalid Request" });
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      Careers.find(
        {
          slug: req.body.data.slug
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, ""),
          _id: { $ne: req.body.data._id }
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "This slug already exists, try another."
              });
            } else {
              var condition = { _id: req.body.data._id };

              console.log('post_date', req.body.data.post_date);
              var update_value = {
                job_name: req.body.data.job_name,
                slug: req.body.data.slug
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, ""),
                meta_tag: req.body.data.meta_tag,
                meta_desc: req.body.data.meta_desc,
                job_type: req.body.data.job_type,
                location: req.body.data.job_location,
                related_cat: req.body.data.related_cat,
                content: req.body.data.content , 
                post_date : req.body.data.post_date
              };
              Careers.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "Career Updated successfully"
                  });
                }
              });
            }
          }
        }
      ).sort({ created_on: 1 });

      ///////////////////
      payload = jwt.verify(token, jwtKey);

      ////////////////
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/********************************************************** */
router.post("/getAllPagesIdsandNames", async (req, res) => {
  // Pages Dropdown for Category page
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Pages.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Pages successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Pages available."
            });
          }
        }
      })
        .sort({ date: -1 })
        .select("title _id");
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/***************** Footer *************** */
router.post("/updateFooter", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      // Footer.find({}, function(err, log) {
      Footer.find({}, async function(error, log) {
        if (log.length > 0) {
          var id = log[0]._id;
          // res.send({ status: 1, message: log[0]._id });
          var condition = { _id: id };
          let updated_on = moment().format("MM-DD-YYYY hh:mm:ss");
          var update_value = {
            col_number: req.body.data.col_number,
            socket_number: req.body.data.socket_number,
            column_content: req.body.data.column_content,
            socket_content: req.body.data.socket_content,
            updated_on: updated_on
          };
          Footer.findOneAndUpdate(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({ status: 1, message: "Footer Updated successfully" });
            }
          });
        } else {
          let updated_on = moment().format("MM-DD-YYYY hh:mm:ss");
          const newFooter = new Footer({
            col_number: req.body.data.col_number,
            socket_number: req.body.data.socket_number,
            column_content: req.body.data.column_content,
            socket_content: req.body.data.socket_content,
            updated_on: updated_on
          });
          try {
            await newFooter
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "Updated Footer Details."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          } catch (ex) {}
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getAllFooterDetails", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Footer.findOne(
        {},
        {
          _id: 1,
          col_number: 1,
          socket_number: 1,
          column_content: 1,
          socket_content: 1
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Fetched footer details successfully.",
              data: log
            });
          }
        }
      ).sort({ date: -1 });
      //   Footer.find(
      //       { },
      //       async function (err, log) {
      //         if (err) {
      //           res.send({
      //             status: 0,
      //             message: "Oops! " + err.name + ": " + err.message,
      //           });
      //         } else {
      //           if(log.length>0){
      //             res.send({
      //               status: 1,
      //               message: "Fetched footer details successfully.",
      //               data: log
      //             });
      //           }else{
      //             res.send({
      //               status: 0,
      //               message: "No footer details available."
      //             });
      //           }
      //         }
      //       }
      //     ).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getFooterMenus", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Menus.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched menus successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No menus available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/***************** Menu *************** */
router.post("/createMenu", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      Menus.find(
        {
          slug: req.body.data.name
            .trim()
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, "")
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "Menu Name already exist, please use another"
              });
            } else {
              let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
              const newMenus = new Menus({
                name: req.body.data.name,
                slug: req.body.data.name
                  .trim()
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, "")
              });
              await newMenus
                .save()
                .then(() => {
                  res.send({
                    status: 1,
                    message: "You have created a Menu to the Cimon"
                  });
                })
                .catch(err => {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                });
            }
          }
        }
      ).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getAllMenus", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Menus.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched careers successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No careers available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/updateMenu", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var condition = { _id: req.body.data.menunid };
      var update_value = { menu_items: req.body.data.menu_items };
      Menus.findOneAndUpdate(condition, update_value, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({ status: 1, message: "Menu Updated successfully" });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteMenu", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Menus.findByIdAndDelete({ _id: req.body.data.menunid }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Menu deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

/******************************** */

/***************** Dashboard ****************/

router.post("/getAllMenus", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Menus.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched careers successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No careers available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/***************** Dashboard ****************/

/***************** Stripe *************** */

router.post("/updateStripe", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var condition = { slug: "stripe_payment" };
      var update_value = {
        name: "stripe_payment",
        value: req.body.data.value
      };
      Stripe.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({ status: 1, message: "Your Stripe details have been updated successfully." });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getStripe", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Stripe.findOne({ slug: "stripe_payment" }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Fetched Category details successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }).sort({ created_on: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/***************** Stripe *************** */



/***************** facebbok *************** */

router.post("/updateFacebook", async (req, res) => {
  const token = req.body.token;
  console.log('updateFacebook');
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {

   
      payload = jwt.verify(token, jwtKey);

      var condition = { slug: "facebook" };
      var update_value = {
        name: "facebook",
        value: req.body.data.value
      };
      Facebook.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({ status: 1, message: "Your Facebook details have been updated successfully." });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getFacebook", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Facebook.findOne({ slug: "facebook" }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Fetched Facebook details successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }).sort({ created_on: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/***************** facebbok *************** */

router.post("/getReport", async (req, res) => {
  const token = req.body.token;
  var today = new Date();
  lastyearDate = new Date(today.getFullYear(), 0, 1);
  ThisyearLastDate = new Date(today.getFullYear(), 12, 1);

  let months = 0;
  months = (ThisyearLastDate.getFullYear() - lastyearDate.getFullYear()) * 12;
  months = months + lastyearDate.getMonth();
  months += ThisyearLastDate.getMonth();

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      async.parallel(
        {
          LastMonthOrderCount: function getLastMonthCount(callback) {
            var today = new Date();
            var last30days = new Date(
              today.getFullYear(),
              today.getMonth(),
              today.getDate() - 30
            );
            var d = today;

            Orders.aggregate([
              {
                $match: {
                  created_on: {
                    $gte: last30days,
                    $lt: d
                  }
                }
              }
            ]).exec((err, Orders) => {
              if (err) {
                callback(
                  {
                    status: 0,
                    message: "Error fetching data"
                  },
                  null
                );
              } else {
                callback(null, Orders.length);
              }
            });
          },
          SalesGraphDataCurrentYear: function getsalesGraphdata(callback) {
            var today = new Date();
            lastyearDate = new Date(today.getFullYear(), 0, 1);
            ThisyearLastDate = new Date(today.getFullYear(), 12, 1);
            Orders.aggregate([
              {
                $match: {
                  created_on: {
                    $gt: lastyearDate,
                    $lte: ThisyearLastDate
                  }
                }
              },

              {
                $project: {
                  month: { $month: "$created_on" },
                  year: { $year: "$created_on" }
                }
              },

              {
                $group: {
                  _id: "$month",
                  count: { $sum: 1 }
                }
              },

              { $sort: { _id: -1 } }
            ]).exec((err, Results) => {
              if (err) {
                callback(
                  {
                    status: 0,
                    message: "Error fetching data"
                  },
                  null
                );
              } else {
                callback(null, Results);
              }
            });
          },
          OrderGraphdata: function getUserGraphdata(callback) {
            var today = new Date();
            lastyearDate = new Date(today.getFullYear(), 0, 1);
            ThisyearLastDate = new Date(today.getFullYear(), 12, 1);
            Orders.aggregate([
              {
                $match: {
                  created_on: {
                    $gt: lastyearDate,
                    $lte: ThisyearLastDate
                  }
                }
              },

              {
                $project: {
                  month: { $month: "$created_on" },
                  year: { $year: "$created_on" }
                }
              },

              {
                $group: {
                  _id: "$month",
                  count: { $sum: 1 }
                }
              },

              { $sort: { _id: -1 } }
            ]).exec((err, Results) => {
              if (err) {
                callback(
                  {
                    status: 0,
                    message: "Error fetching data"
                  },
                  null
                );
              } else {
                //

                if (Results.length) {
                  const Items = [];
                  for (i = 1; i <= months; ++i) {
                    let count = 1;
                    Results.forEach(function(item) {
                      if (item._id == i) {
                        Items.push(item.count);
                        return;
                      } else if (count == Results.length) {
                        Items.push(0);
                      }
                      count++;
                    });
                  }

                  callback(null, Items);
                } else {
                  callback(null, Results);
                }

                //    callback(null, Results);
              }
            });
          },

          CountryGraphData: function getProjectGraphData(callback) {
            var today = new Date();
            lastyearDate = new Date(today.getFullYear(), 0, 1);
            ThisyearLastDate = new Date(today.getFullYear(), 12, 1);

            Orders.aggregate([
              // { $unwind: '$order_details.total.product_total' },
              { $unwind: "$shipping_details.country" },
              { $unwind: "$order_details.product" },

              {
                $match: {
                  created_on: { $gt: lastyearDate, $lte: ThisyearLastDate },
                  page_status: { $eq: "1" },
                  $or: [{ status: "succeeded" }]
                }
              },
              {
                $project: {
                  month: { $month: "$created_on" },
                  year: { $year: "$created_on" },
                  TotalMarks: {
                    $sum: "$order_details.product.quantity"
                  }
                }
              },
              {
                $group: {
                  //  total: { $sum: "$order_details.product.quantity" },
                  _id: "$month",
                  total_new: { $sum: "$order_details.product.quantity" },
                  count: { $sum: 1 }
                  //       refund : { "$sum": "$order_details.refund.amount" } ,
                }
              }
              // { $sort : { _id : -1 } }
            ]).exec((err, Results) => {
              if (err) {
                callback(
                  { status: 0, message: "Error fetching data" + err },
                  null
                );
              } else {
                callback(null, Results);
              }
            });
          },
          MonthlyRevenue: function getLastMonthCount(callback) {
            var today = new Date();
            var last30days = new Date(
              today.getFullYear(),
              today.getMonth(),
              today.getDate() - 30
            );
            var d = today;
            let lastdate = new Date();
            let firstDay = new Date(
              lastdate.getFullYear(),
              lastdate.getMonth(),
              1
            );

            Orders.aggregate([
              {
                $match: {
                  $or: [
                    { status: "Done" },
                    { status: "succeeded" },
                    { status: "Completed" }
                  ],
                  $and: [
                    { created_on: { $gt: last30days } },
                    { created_on: { $lte: d } },
                    { page_status: { $eq: "1" } }
                  ]
                }
              }
            ]).exec((err, Orders) => {
              if (err) {
                callback(
                  {
                    status: 0,
                    message: "Error fetching data"
                  },
                  null
                );
              } else {
                let Ordertotal = 0;
                Orders.forEach(function(Orderitem) {
                  let refundvalue = 0;
                  refundvalue =
                    Orderitem.order_details.refund.length > 0
                      ? Orderitem.order_details.refund.reduce(
                          (refundvalue, Qty) =>
                            refundvalue * 1 + (Qty.other.amount * 1) / 100,
                          0
                        )
                      : 0;

                  if (Orderitem.order_details.total.total) {
                    Ordertotal =
                      Ordertotal +
                      (Orderitem.order_details.total.total * 1 -
                        refundvalue * 1);
                  }
                });

                callback(null, Ordertotal);
              }
            });
          }
        },
        function(error, results) {
          if (error) {
            res.send({
              status: 0,
              message: error
            });
          } else {
            res.send({
              status: 1,
              message: "Fetched  successfully.",
              data: results
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*   Create Archives */
router.post("/createArchives", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Archives.find({ slug: req.body.data.slug }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            const newArchives = new Archives({
              title: req.body.data.title,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              date: createdon,
              //description: req.body.data.description,
              file_url: req.body.data.file_url,
              uploadFiles: req.body.data.uploadFiles,
              content : req.body.data.content,
              //        is_top_news : req.body.data.checked,
              related_products: req.body.data.related_products,
              related_cat: req.body.data.related_cat,
              releasedate: moment(req.body.data.releasedate).format(
                "YYYY-MM-DD"
              ),
              version: req.body.data.version,
              data_order : req.body.data.data_order
            });
            await newArchives
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have successfully uploaded an Archive."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*  Get all Archives */
router.post("/getAllArchives", async (req, res) => {
  const token = req.body.token;
  const filter = req.body.filter;
  let filterCondition = {};
  // if (filter.date) {
  //   const selectedDate = filter.date.split("/");
  //   const start = new Date(selectedDate[1], selectedDate[0] - 1, 1);
  //   const end = new Date(selectedDate[1], selectedDate[0] - 1, 31);
  //   filterCondition = { date: { $gte: start, $lte: end } };
  // }

  if (filter.type) {
    filterCondition.related_cat = filter.type;
  }
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Archives.find(filterCondition, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Archives successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Archives available."
            });
          }
        }
      }).sort({ _id: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/* Delete Archives */
router.post("/deleteArchives", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Archives.findByIdAndDelete({ _id: req.body.news_id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Archives deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

/* trash archives */

router.post("/trashArchives", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    payload = jwt.verify(token, jwtKey);

    try {
      var condition = { _id: req.body.id };
      var update_value = {
        page_status: 0
      };
      Archives.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Archive moved to trash successfully"
          });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/* Get Single Archives */
router.post("/getSingleArchives", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Archives.findOne(
          { _id: req.body.news_id },
          {
            _id: 1,
            is_top_news: 1,
            title: 1,
            slug: 1,
            file_url: 1,
            content: 1,
            description: 1,
            related_products: 1,
            related_cat: 1,
            releasedate: 1,
            version: 1,
            uploadFiles: 1,
            data_order : 1
          },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: { news_data: log }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

/*Update Archives*/
router.post("/updateArchives", async (req, res) => {
  console.log("testtttt");
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      console.log(
        "dattttttttttttttteeeee",
        moment(req.body.data.releasedate).format("YYYY-MM-DD")
      );
      //console.log(4343, req.body.data);
      Archives.find(
        {
          slug: req.body.data.slug
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, ""),
          _id: { $ne: req.body.data._id }
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              res.send({
                status: 0,
                message: "This slug already exists, try another."
              });
            } else {
              var condition = { _id: req.body.data._id };
              var update_value = {
                title: req.body.data.title,
                slug: req.body.data.slug
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, ""),
                description: req.body.data.description,
                content: req.body.data.content,
                //       is_top_news : req.body.data.checked,
                file_url: req.body.data.file_url,
                uploadFiles: req.body.data.uploadFiles,
                version: req.body.data.version,
                related_products: req.body.data.related_products,
                releasedate: moment(req.body.data.releasedate).format(
                  "YYYY-MM-DD"
                ),
                related_cat: req.body.data.related_cat,
                data_order : req.body.data.data_order
              };

              Archives.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "Archives Updated successfully"
                  });
                }
              });
            }
          }
        }
      ).sort({ created_on: 1 });
      payload = jwt.verify(token, jwtKey);
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*action handler archives */

router.post("/actionHandlerArchives", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.archiveids };
        if (req.body.action == "3") {
          Archives.deleteMany(
            { _id: { $in: req.body.archiveids } },
            async function(err, log) {
              if (err) {
                res.send({ status: 1, message: "Something went wrong" + err });
              } else {
                res.send({
                  status: 1,
                  message: "Archives Trash deleted successfully"
                });
              }
            }
          );
        } else {
          Archives.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "Archives Updated successfully" });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*date filter archive */
router.post("/getArchiveDateFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Archives.aggregate(
        [
          {
            $project: {
              year: { $year: "$date" },
              month: { $month: "$date" }
            }
          },
          {
            $group: {
              _id: null,
              date: { $addToSet: { year: "$year", month: "$month" } }
            }
          },
          { $sort: { _id: -1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*category filter archive */

router.post("/getArchiveTypeFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Archives.aggregate(
        [
          {
            $project: {
              related_cat: 1
            }
          },
          {
            $group: {
              _id: "$related_cat"
            }
          },
          { $sort: { related_cat: 1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/createLanguages", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Language.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          const newLanguages = new Language({
            language: req.body.data.language,
            code: req.body.data.code,
            url: req.body.data.url,
            languagestatus: req.body.data.languagestatus
          });
          await newLanguages
            .save()
            .then(() => {
              res.send({
                status: 1,
                message: "You have created a Language to the Cimon"
              });
            })
            .catch(err => {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            });
        }
      }).sort({ created_on: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*Get all languages */
router.post("/getAllLanguages", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Language.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched Language Successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No Language available."
            });
          }
        }
      }).sort({
        _id: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getLanguageById", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Language.findOne(
        {
          _id: req.body.data.id
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log) {
              res.send({
                status: 1,
                message: "Fetched Language Successfully.",
                data: log
              });
            } else {
              res.send({
                status: 0,
                message: "Details Unvailable."
              });
            }
          }
        }
      ).sort({
        // created_on: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/* Delete language */
router.post("/removeLanguage", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Language.findByIdAndDelete(
        {
          _id: req.body.data._id
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log) {
              res.send({
                status: 1,
                message: "Language Deleted Successfully"
              });
            } else {
              res.send({
                status: 1,
                message: "Something went wrong"
              });
            }
          }
        }
      );
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/*language Updated */
router.post("/updateLanguage", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      var condition = { _id: req.body.data.id };
      var update_value = {
        language: req.body.data.language,
        code: req.body.data.code,
        url: req.body.data.url,
        languagestatus: req.body.data.languagestatus
      };
      Language.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({ status: 1, message: "Language Updated successfully" });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/createInternationalDistributor", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
      const newDistributor = new Distributor({
        location_name: req.body.data.location_name,
        location_url: req.body.data.location_url,
        latitude: req.body.data.latitude,
        longitude: req.body.data.longitude,
        storenumber: req.body.data.storenumber,
        created_on: createdon
      });
      await newDistributor
        .save()
        .then(() => {
          res.send({
            status: 1,
            message: "You have created a new record successfully."
          });
        })
        .catch(err => {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getAllInternationalDistributor", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Distributor.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched  successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No careers available."
            });
          }
        }
      }).sort({ date: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/actionHandlerId", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.ids };
        if (req.body.action == "3") {
          Distributor.deleteMany({ _id: { $in: req.body.ids } }, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({
                status: 1,
                message: "Distributor Trash deleted successfully"
              });
            }
          });
        } else {
          Distributor.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({
                status: 1,
                message: "Distributor Updated successfully"
              });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});
router.post("/deleteInternationalDistributor", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Distributor.findByIdAndDelete({ _id: req.body.id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Resources deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/trashInternationalDistributor", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Distributor.findByIdAndUpdate(
          { _id: req.body.id },
          { page_status: 0 },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "Resources deleted" });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/getSingleInternationalDistributor", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Distributor.findOne({ _id: req.body.id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: { data: log }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/updateInternationalDistributor", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      var condition = { _id: req.body.data._id };
      var update_value = {
        location_name: req.body.data.location_name,
        location_url: req.body.data.location_url,
        latitude: req.body.data.latitude,
        longitude: req.body.data.longitude,
        storenumber: req.body.data.storenumber
      };
      Distributor.findOneAndUpdate(condition, update_value, async function(
        err,
        log
      ) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({ status: 1, message: "You have updated this record successfully." });
        }
      });
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

/********** Aticles *******************/

router.post("/createArticles", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Articles.find({ slug: req.body.data.slug }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 0,
              message: "This slug already exists, try another."
            });
          } else {
            let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
            console.log(req.body.data);
            const newNews = new Articles({
              title: req.body.data.title,
              slug: req.body.data.slug
                .toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""),
              date: createdon,
              description: req.body.data.description,
              image: req.body.data.profile_image,
              image_thumb :   req.body.data.profile_image_thumb,
              content: req.body.data.content,
              is_top_articles: req.body.data.checked,
              meta_tag: req.body.data.meta_tag,
              meta_desc: req.body.data.meta_desc,
              related_products: req.body.data.related_products,
              topic: req.body.data.topic,
              industry: req.body.data.industry, 
              source: req.body.data.source,
              // related_cat: req.body.data.related_cat,
              releasedate: moment(req.body.data.releasedate).format(
                "YYYY-MM-DD"
              ),
              article_order : Number(req.body.data.article_order)
            });

            console.log(newNews);
            await newNews
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "You have successfully added a new article."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          }
        }
      }).sort({ date: 1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getAllArticles", async (req, res) => {
  const token = req.body.token;
  const filter = req.body.filter;
  let filterCondition = {};
  if (filter.date) {
    const selectedDate = filter.date.split("/");
    const start = new Date(selectedDate[1], selectedDate[0] - 1, 1);
    const end = new Date(selectedDate[1], selectedDate[0] - 1, 31);
    filterCondition = { date: { $gte: start, $lte: end } };
  }
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Articles.find(filterCondition, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched news successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No news available."
            });
          }
        }
      }).sort({ _id: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getArticleDateFilterOptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Articles.aggregate(
        [
          {
            $project: {
              year: { $year: "$date" },
              month: { $month: "$date" }
            }
          },
          {
            $group: {
              _id: null,
              date: { $addToSet: { year: "$year", month: "$month" } }
            }
          },
          { $sort: { _id: -1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/deleteArticles", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Articles.findByIdAndDelete({ _id: req.body.news_id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "News deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/trashArticles", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Articles.findByIdAndUpdate(
          { _id: req.body.news_id },
          { page_status: 0 },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "Articles trashed." });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/actionHandlerArticles", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        page_status: req.body.action
      };

      try {
        const condition = { _id: req.body.newsids };
        if (req.body.action == "3") {
          Articles.deleteMany(
            { _id: { $in: req.body.newsids } },
            async function(err, log) {
              if (err) {
                res.send({ status: 1, message: "Something went wrong" + err });
              } else {
                res.send({
                  status: 1,
                  message: "Articles Trash deleted successfully."
                });
              }
            }
          );
        } else {
          Articles.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "Articles Updated successfully" });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/getSingleArticles", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Articles.findOne(
          { _id: req.body.news_id },
          {
            _id: 1,
            is_top_news: 1,
            title: 1,
            slug: 1,
            image: 1,
            content: 1,
            description: 1,
            meta_tag: 1,
            meta_desc: 1,
            image_thumb: 1,
            // page_title :1 ,
            is_top_articles: 1,
            topic: 1,
            industry: 1, 
            source: 1,
            article_order: 1
          },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: { news_data: log }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/updateArticles", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      Articles.find(
        {
          slug: req.body.data.slug
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, ""),
          _id: { $ne: req.body.data._id }
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log.length > 0) {
              var condition = { _id: req.body.data._id };
              var update_value = {
                title: req.body.data.title,
                slug: req.body.data.slug,
                description: req.body.data.description,
                content: req.body.data.content,
                is_top_news: req.body.data.checked,
                image: req.body.data.profile_image,
                image_thumb :   req.body.data.profile_image_thumb,
                //     page_title:  req.body.data.pagetitle,
                meta_tag: req.body.data.metatag,
                meta_desc: req.body.data.metadescription,
                topic: req.body.data.topic,
                industry: req.body.data.industry, 
                source: req.body.data.source,
                releasedate: moment(req.body.data.releasedate).format(
                  "YYYY-MM-DD"
                ),
                article_order : req.body.data.article_order
              };
              Articles.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "Articles Updated successfully"
                  });
                }
              });
            } else {
              var condition = { _id: req.body.data._id };
              var update_value = {
                title: req.body.data.title,
                slug: req.body.data.slug,
                description: req.body.data.description,
                content: req.body.data.content,
                is_top_news: req.body.data.checked,
                image: req.body.data.profile_image,
                image_thumb :   req.body.data.profile_image_thumb,
                //     page_title:  req.body.data.pagetitle,
                meta_tag: req.body.data.metatag,
                meta_desc: req.body.data.metadescription,
                topic: req.body.data.topic,
                industry: req.body.data.industry, 
                source: req.body.data.source,
                releasedate: moment(req.body.data.releasedate).format(
                  "YYYY-MM-DD"
                ),
                article_order : req.body.data.article_order
              };
              Articles.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "Articles Updated successfully"
                  });
                }
              });
            }
          }
        }
      ).sort({ date: 1 });

      ///////////////////
      payload = jwt.verify(token, jwtKey);

      ////////////////
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});



/*Get all FeedBack */
router.post("/getAllFeedBack", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      FeedBack.find({}, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched FeedBack Successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No FeedBack available."
            });
          }
        }
      }).sort({
        _id: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});


/* Delete feedback */
router.post("/removeFeedback", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      console.log(' req.body.data',  req.body.data);
      payload = jwt.verify(token, jwtKey);
      FeedBack.findByIdAndDelete(
        {
          _id: req.body.data._id
        },
        async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (log) {
              res.send({
                status: 1,
                message: "Feedback Deleted Successfully"
              });
            } else {
              res.send({
                status: 1,
                message: "Something went wrong"
              });
            }
          }
        }
      );
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({
          status: 0,
          message: "Token verification failed."
        });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});




/*Get all  Feeback email  */
router.post("/getFeedbackEmail", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Settings.find({
        slug : 'feedbackEmail'
      }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched FeedBack Successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No FeedBack available."
            });
          }
        }
      }).sort({
        _id: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});



/* update Feeback email  */
router.post("/updatefeedbacksettings", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {

              var condition = { slug: 'feedbackEmail' };

  
              var update_value = {
                value: req.body.data.email,                
              };
            
              Settings.findOneAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  res.send({
                    status: 1,
                    message: "Feedback email Updated successfully"
                  });
                }
              }); 
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});



/* update updateEventTraining  */
router.post("/updateSettngs", async (req, res) => {
  const token = req.body.token;
  if (!token) {
  res.send({ status: 0, message: "Invalid Request" });
  } else {
  try {


    console.log('elveeeeeeeeeeeeeee');
        var condition = { slug: 'slidersettings' };
        var update_value = {
          value: req.body.data,                
        };
        Settings.findOneAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Updated Successfully."
            });
          }
        }); 
} catch (e) {
res.send({
  status: 0,
  message: "Oops! " + e.name + ": " + e.message
});
}
}
});



/*  get slider Options */
router.post("/getSliderSettngs", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({
      status: 0,   
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Settings.find({
        slug : 'slidersettings'
      }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched SliderOptions Successfully.",
              data: log[0].value
            });
          } else {
            res.send({
              status: 0,
              message: "Not available."
            });
          }
        }
      }).sort({
        _id: -1
      });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});




module.exports = router;
