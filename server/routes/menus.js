/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require('async');

/* Include schema */
const Menus = require("../models/menus");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();
const request = require('request');

/* Get Header Menus */
router.post("/getHeaderMenus", async (req, res) => {
    try {
        Menus.findOne({
                slug: 'main-menu',
            },
            async function (err, response) {
                if (err) {
                    res.send({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                    });
                } else {
                    if (response) {
                        res.send({
                            status: 1,
                            message: "Fetched details successfully.",
                            headermenu: response
                        });
                    } else {
                        res.send({
                            status: 0,
                            message: "No details available."
                        });
                    }
                }
            }
        ).sort({
            _id: 1
        });
    } catch (e) {
        res.send({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
        });
    }

});
module.exports = router;