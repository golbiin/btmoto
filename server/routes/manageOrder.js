//load dependencies
const SECRET_KEY    = process.env.SECRET_KEY;
const WC_PROCESSING = process.env.WC_PROCESSING;
const WC_CANCELLED = process.env.WC_CANCELLED;
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const fs = require("fs");
const PDFDocument = require("pdfkit");
const path = require("path");
const mustache = require("mustache");
const sgMail = require("@sendgrid/mail");
var mongoose = require('mongoose');

//const Ordersnot = require("../models/orders");
const Ordersnotes = require("../models/order_notes");
const AWS = require("aws-sdk");
const { v4: uuidv4 } = require('uuid');
const stripe = require('stripe')(SECRET_KEY);
var async = require('async');


const OrderNotes = require("../models/order_notes");
const Products = require("../models/products");
const ShippingMethod = require("../models/shipping_method");
const ShippingSettings = require("../models/shipping_settings");
const Orders = require("../models/orders");
var utils = require('../config/utils');
//http request port set
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;

const upsCOnfig   =    config.ups;
//http request port set
const router = express.Router();

/* Guest user orders */

router.post("/getUserOrders", async (req, res) => {
    const token = req.body.token;
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
    try {
      payload = jwt.verify(token, jwtKey);
      Orders.find(
          { user_id: payload._id  }, 
          async function (err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              });
            } else {
              if(log.length>0){
                res.send({
                  status: 1,
                  message: "Fetched Orders Successfully.",
                  data: log
                });
              }else{
                res.send({
                  status: 0,
                  message: "No Orders available."
                });
              }
            }
          }
        ).sort({ order_no: -1 });
      } catch (e) {
        res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
        });
      }
    }
  });

  /* Cancel Order */
  router.post("/cancelOrder", async (req, res) => {
    const token = req.body.token;
    const order_id = req.body.order_id;
    
    if (!token) {
      res.send({ status: 0, message: "Invalid Request" });
    } else {
      try {
        payload = jwt.verify(token, jwtKey);

        try {
          var condition = { _id: mongoose.Types.ObjectId( order_id ) };

          Orders.findOne (condition, async function (err, order) {

            if(err) {
              res.send({
                status: 0,
                message: "Order not found",
              });
            }            
            if(order.status == WC_PROCESSING){
              
              var update_value = { status: WC_CANCELLED };

              Orders.findByIdAndUpdate(condition, update_value, async function (
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                  });
                } else {
                  sendCancelOrderMail(log, (result) => {
                    if (result == "success") {
                      res.send({
                        status: 1,
                        type : 'sucess',
                        message: "Order cancelled.",
                      });
                    } else if (result == "failed") {
                      res.send({
                        status: 1,
                        type : 'error',
                        message:
                          "Oops! Something went wrong with sending  mail...!",
                      });
                    }
                  });
                  
                 
                }
              });
            } else {
              res.send({
                status: 0,
                message: "Not allowed to cancel order. shippment started",
              });
            }

          })

        } catch (err) {
          res.send({
            status: 0,
            message: "Oops!Something went wrong.",
          });
        }
      } catch (e) {
        if (e instanceof jwt.JsonWebTokenError) {
          res.send({ status: 0, message: "Token verification failed." });
        }
        res.send({
          status: 0,
          message: "Oops! Something went wrong with JWT token verification...!",
        });
      }
    }
  });

  function sendCancelOrderMail(order, callback) {
     let header = fs
     .readFileSync(path.join(__dirname + "/emails/header.html"), {
       encootding: "utf-8"
     })
     .toString();
   
      
     var view = {
         imageUrl:config.imageUrl, 
         siteuUrl :config.siteurl,    
     };
     var createHeader = mustache.render(header, view);
   
   
     let footer = fs
     .readFileSync(path.join(__dirname + '/emails/footer.html'), {
       encootding: "utf-8"
     })
     .toString();
     var createFooter = mustache.render(footer, view);
     
     let template = fs
     .readFileSync(path.join(__dirname + "/emails/cancelorder.html"), {
       encootding: "utf-8"
     })
     .toString();

     let total = {
      product_total : order.order_details.total.product_total,
      shipping_total: order.order_details.total.shipping_total,
      shipping_method: order.shipping_method.service,
      tax : 0, 
      payment_method: 'Stripe' ,
      grand_total: order.order_details.total.total
      };
    var orderHtml = utils.generateOrderCancelHtml(order.order_details.product, total);

    var view = {
      first_name:order.shipping_details.first_name,
      last_name:order.shipping_details.last_name,
      company:order.shipping_details.company,
      email:order.shipping_details.email,
      address: order.shipping_details.address,
      appartment: order.shipping_details.appartment,
      city: order.shipping_details.city,
      country: order.shipping_details.country,
      zipcode: order.shipping_details.zipcode,
      phone: order.shipping_details.phone,
      order: orderHtml, 
      siteuUrl :config.siteurl,       
    };
     
     var createTemplate = mustache.render(template, view);
     var htmlContent = createHeader+ createTemplate +createFooter;
     sgMail.setApiKey(sendGridKey);
     const message = {
         to:[config.admin_email , order.shipping_details.email], 
         from : { email : config.fromEmail , name: 'Cimon'},
         subject: "Cancel Order",
         html: htmlContent
     };
      try {
      sgMail
          .send(message)
          .then(() => {
         callback("success");
          })
          .catch(error => {
            
              const { message, code, response } = error;
              callback("failed");
              callback({
                  status: 0,
                  message: "Oops! failed to send order email" ,
                  from: 'mail'
              });
          });
  } catch (err) {
    
    callback("failed");
      callback({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
          from: 'mail catch'
      }, null);
  }
}

  module.exports = router;