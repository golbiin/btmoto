//load dependencies
const express = require("express");
var async = require("async");
const { createWriteStream } = require('fs');
//Include schema
const Footer_columns = require("../models/footer_columns");
const Menus = require("../models/menus");
const Redirection = require("../models/redirection");
const Ipredirection = require("../models/ipredirection");
const Pages = require("../models/pages");
const Facebook = require("../models/facebook");

const Settings = require("../models/settings");

const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();
const mongoose = require("mongoose");
const { SitemapStream, streamToPromise } = require('sitemap');
/* Get Footer Menu */
var curl = require("curlrequest");
const { curly } = require('node-libcurl');
const publicIp = require('public-ip');
var ip = require('ip'); 


router.post("/footer", async (req, res) => {

  try {
    async.waterfall([  
          getFooter,  
          getFooterMenu,
      ], function (error, response) {

          if (error) {
            res.send(error);              
          } else  {

            res.send({
              status: 1,
              message: "Fetched menu details successfully.",
              data: response.data,
              FooterDetails: response.footerMenu,
            });

          }
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

});


function getFooter(callback) {
  let results = {}
  try{
    Footer_columns.findOne({},
    async function (err, response) {
      if (err) {
        callback({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
        }, null);
      } else {
        if(response) {   
          results.data = response;  
          callback(null, results);
        } else {
          callback({
            status: 0,
            message: "No footer details available.",
          });
        }
      }
    });
  } catch (err) {
    callback({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message,
    }, null);
  }
}
function getFooterMenu(results, callback) {
  let menuIds = [];
  if(results.data.column_content) {
    for (let [key, column] of Object.entries(results.data.column_content)) {
      menuIds.push(column.menu);
    }
  }
  try{
    Menus.find({
      _id: {
        $in: menuIds 
      }
    },
    async function (err, menus) {
      
      if (err) {
        callback({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
        }, null);
      } else {
        if (menus) {
          results.footerMenu = menus;
          
          callback(null, results);
        } else {
          callback({
            status: 0,
            message: "Footer menu details unavailable.",
          });
        }
      }
    });
  } catch(err) {
    callback({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message,
    }, null);
  }
}



router.post("/getSitemap", async (req, res) => {
  try {
   
     

    
  const options = {
    // let's assume id below is 1458722400807259,
    // which is my user id
      method: 'GET',
      url : `https://graph.facebook.com/v10.0/344152559342803/photos?type=uploaded&access_token=EAAUhozSUNoQBAKEpdOUKog4ozqU8OPHFGjTZBAhmCUIUhb4WFxhsU5dXZB1fBNHYMGs9E4YaLmVjtueh7R1nZBJEvjclLl6yts3CGsNJ5BRTgoSkqDZA2Ni31ZC8txRgLk2pyXdV8sNkZBEtV4JoGAOTyiL0eVHKCSKEJjMgbeZCwmeKoiK39WWsrUFyOpLlcIZD`
    };


    curl.request(options, function(err, data) {
      if (err) {
        res.send({
          status: 0,
          message: "error" + err,
          data: []
        });
      } else {
        res.send({
          status: 1,
          message: "Sucess",
          data: JSON.parse(data)
        });
      }
    });
    // const sitemap = new SitemapStream({ hostname: 'http://3.93.223.71/' });

    // const writeStream = createWriteStream('../sitemap.xml');
    // sitemap.pipe(writeStream);
     
    // Menus.find( { slug : 'sitemap' },
    //       async function (err, response) {
    //           if (err) {
    //               res.send({
    //                   status: 0,
    //                   message: "Oops! " + err.name + ": " + err.message,
    //               });
    //           } else {
    //               if (response) {
                     
    //                   const response_details = response;
                    
    //                   response_details[0].menu_items.map((urls, index) => {
    //                     console.log('response_details',);
    //                     sitemap.write(urls.url);
                        
    //                   });
    //                   sitemap.end();
    //                    res.send({
    //                       status: 1,
    //                       message: "Fetched details successfully.",
    //                       data: response
    //                   });
                     
    //               } else {
    //                   res.send({
    //                       status: 0,
    //                       message: "No details available."
    //                   });
    //               }
    //           }
    //       }
    //   ).sort({
    //     date: -1
    //   })
  } catch (e) {
      res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
      });
  }

});

router.post("/getFacebookImageFeeds", async (req, res) => {
  try {
   
     
    pageId = '344152559342803';
    accessToken = 'EAAUhozSUNoQBAFUy19atZCIxOsoapP2nXZCKKhYyXMdNyZAjljG0kxyTvIOidI3DfocO4DjZBGPuY9x4sBfZChokDrSqUC5h6PphMsoFQMRQlp5BGiZBJQWK6oIc6eZC94HultVK7JrqGZBTepokE1ZCmA5ZCmPgxpmC4s2hyPi2jAKQZDZD';

   


    try {
      async.waterfall([  
            getFacebookDetails,
            getImageList,  
            getImagePath,
        ], function (error, response) {
  
            if (error) {
              res.send(error);              
            } else  {
  
              res.send({
                status: 1,
                message: "Fetched menu details successfully.",
                data: response,
               
              });
  
            }
        });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }


  } catch (e) {
      res.send({
          status: 0,
          message: "Oops! " + e.name + ": " + e.message,
      });
  }

});


function getFacebookDetails(callback) {
  
  let results = {}
  try{ 
    Facebook.find( { slug: 'facebook' },
      async function (err, response) {
        if (err) {
          callback({
            status: 0,
            message: "No Facebook Details",
          }, null);
        } else {
          if(response){
            results.facebook = response[0].value;  
            callback(null, results);
          }else{
            callback({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message,
            }, null);
          }
        }
      }
    );
  } catch (err) {
    callback({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message,
    }, null);
  }
}



function getImageList(results,callback) {

  pageId  = results.facebook.PageId;
  accessToken = results.facebook.AccessToken;

  options = {
    method: 'GET',
    url : `https://graph.facebook.com/v10.0/`+pageId+`/photos?type=uploaded&access_token=`+accessToken,
  };
   console.log('results', results);
  try{ 
    curl.request(options, function(err, data) {
      if (err) {
        callback({
          status: 0,
          message: "No images",
        }, null);
      } else {
       // console.log('curl result', JSON.parse(data));
        results.data = JSON.parse(data);  
        callback(null, results);
       
      }
    });
  } catch (err) {
    callback({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message,
    }, null);
  }
}


function getImagePath(results, callback) {
  
  try{
  
    const imagesarray = [];
    results.data.data.forEach(element => { 
      // const imagesarrayPath = retunimagurl(element.id);
      
       imagesarray.push(element.id);
 
     }); 

    
     results.imageids = imagesarray;
     callback(null, results);
    

  } catch(err) {
    callback({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message,
    }, null);
  }
}


/*  getFacebookImage  by id  */
router.post("/getFacebookImage", async (req, res) => {
  
   
  try {
    const ids = req.body.id
   
    const imagesarray = [];
     //  accessToken = 'EAAUhozSUNoQBAFUy19atZCIxOsoapP2nXZCKKhYyXMdNyZAjljG0kxyTvIOidI3DfocO4DjZBGPuY9x4sBfZChokDrSqUC5h6PphMsoFQMRQlp5BGiZBJQWK6oIc6eZC94HultVK7JrqGZBTepokE1ZCmA5ZCmPgxpmC4s2hyPi2jAKQZDZD';
        optionsimagepath = {
          method: 'GET',
          url : `https://graph.facebook.com/v10.0/`+ids+`/picture?&access_token=`+accessToken+`&redirect=0`,
         
        };
        curl.request(optionsimagepath, function(err, data) {
          if (err) {
            console.log('err result-->', err);
          } else {
            const ImageData =  JSON.parse(data); 
            
            res.send({ 
              status: 1,             
              message: "Fetched Pages successfully.",
              data: ImageData.data.url
            });
           
          }
        });

  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});


//  function retunimagurl (ids) {
//   console.log('retunimagurl' ,ids); 
//        const imagesarray = [];
//        accessToken = 'EAAUhozSUNoQBAMxgJQzthaBEnFCbpVnozpHJLMPJ79ZAhla9omUGocO1ZBYNzKp6DC36LZCNNs1uwAYbiACKRlssiVGRuKgsJHIGrjl1AhkZA5srF4ZBB9qToPHSfGgz08R5uXFcxCMi6958sArx49kznkm8fXhTZB0ED2kL6M9ZBZANcKAJsG8HAKfZA1Sq1hkcBBQWEezpYuQZDZD';
//         optionsimagepath = {
//           method: 'GET',
//           url : `https://graph.facebook.com/v10.0/`+ids+`/picture?&access_token=`+accessToken+`&redirect=0`,
//           //access_token: accessToken
//         };
//         curl.request(optionsimagepath, function(err, data) {
//           if (err) {
//             console.log('err result-->', err);
//           } else {
//             const ImageData =  JSON.parse(data);
           
//             imagesarray.push(ImageData.data.url);
//             callback(imagesarray);
           
//           }
//         });


//         console.log('imagesarray-->', imagesarray);
        
//         // return imagesarray;
// }


/*  getRedirectUrl Details  */
router.post("/getRedirectUrl", async (req, res) => {
   
  try {
    console.log('enter getRedirectUrl');
      Redirection.find( { status: 1 },
      async function (err, response) {
        if (err) {
          res.send({   
            status:0,         
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
            res.send({ 
              status: 1,             
              message: "Fetched Redirection successfully.",
              data: response
            });
          }else{
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});


/*  getRedirectUrl Details  */
router.post("/getcontent", async (req, res) => {
   
  try {
   // console.log('getcontent',req.body );
    Pages.find( { slug: req.body.slug },
      async function (err, response) {
        if (err) {
          res.send({   
            status:0,         
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
            res.send({ 
              status: 1,             
              message: "Fetched Pages successfully.",
              data: response
            });
          }else{
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});


/*  getRedirectUrl Details  */
router.post("/getRedirection", async (req, res) => {


  _id = req.body.id ;
  ipv4 = req.body.ipaddress
  const {data} = await curly.get('http://www.geoplugin.net/json.gp?ip='+ipv4);
  console.log('data--->',_id);
  reposnedata = data,
  Geocountry = data.geoplugin_countryCode,
  Georegion = data.geoplugin_regionCode
  try {
    async.waterfall([  
      getRedirection,  
      getRedirectionUrl,
    //  getIpaddress
      ], function (error, response) {

          if (error) {

            res.send(error);

                          
          } else  {

            res.send({
              status: 1,
              message: "Fetched Redirection Successfully.",
              data: response,
              userip : reposnedata ,
              ipv4 : ipv4
            });

          }
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

 
});


function getRedirection(callback) {
  let results = {}
  try{
    Ipredirection.findOne( { 
      
      from : _id , 
      status : '1' 
      ,  country: { $regex:  new RegExp(Geocountry, "i")}
    },
      async function (err, response) {
        if (err) {
          callback({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          }, null);
        } else {
         console.log('--->response',Geocountry , response , _id);
          if(response) {
              
            results.data = response;
            results.country = response.country ? response.country : ''; 
            results.state = response.state ? response.state : ''; 
            results.except = response.except ? response.except  : false;  
            results.ip = response.ip ? response.ip  : '';
            results.Currentcountry = Geocountry ? Geocountry : '';
            callback(null, results);
          } else {
           
      
            callback({
              status: 0,
              message: "Oops! ",
              data : results,
              from: 'getRedirection'
          },null);
           // callback(null, results);
          }
        }
      }
    ).sort({ created_on: -1 });
  } catch (err) {
    callback(null ,{
      status: 0,
      message: "Oops! " + err.name + ": " + err.message,
    });
  }
}

function getRedirectionUrl(results, callback) {
  
  
  console.log('getRedirectionUrl',results);
  try{
    Pages.find({ _id: results.data.to },
    async function (err, response) {
      
      if (err) {
        callback({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
          from: 'getRedirectionUrl'
      },null);
      } else {
        if (response.length) {

        console.log('results.country.indexOf(Geocountry)', Geocountry, response[0].slug, results.country.indexOf(Geocountry), results.except , results.data.ip !== null ? results.data.ip.includes(ipv4) : 0)
        let slug = '';
        if(response[0].template_name == 'scada' || response[0].template_name ==  'ipc' || 
        response[0].template_name ==  'hmi' || 
        response[0].template_name == 'hybrid-hmi' ||
        response[0].template_name ==  'ipc'){
          slug = '/introduction/' + response[0].slug
        } else {
          slug  = '/' + response[0].slug
        }

      
       if( (results.data.ip == ipv4 || (results.data.ip !== null ? results.data.ip.includes(ipv4) : 0))){ // checking ip address 
        results.redirect = 0;
       }
       else if (

          (results.state.indexOf(Georegion) > -1 && results.country.indexOf(Geocountry) > -1 && results.except == "false")
          ||
          (results.state.indexOf(Georegion) > -1 && results.country.indexOf(Geocountry) == -1 && results.except == "true")
         

        ){  // Region based redirection
          results.redirect =  slug ? (slug) : 0;
        }
        else if (

        (results.country.indexOf(Geocountry) > -1 && results.except == "false")
                              ||
        (results.country.indexOf(Geocountry) == -1 && results.except == "true")

        ) {  // Country based redirection
          results.redirect =  slug ? (slug) : 0;
        }  else {
          results.redirect = 0;
        }
          
  
          callback(null, results);
        } else {
          callback(null,results);
        }
      }
    }).sort({ created_on: -1 });
  } catch(err) {
    callback(null,{
      status: 0,
      message:err,
    });
  }
}




/*  get slider Options */
router.post("/getSliderOption", async (req, res) => {
  try { 
    Settings.find({
      slug : 'slidersettings'
    }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        if (log.length > 0) {
          res.send({
            status: 1,
            message: "Fetched SliderOptions Successfully.",
            data: log[0].value
          });
        } else {
          res.send({
            status: 0,
            message: "Not available."
          });
        }
      }
    }).sort({
      _id: -1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});
module.exports = router;