const CURRENCY    = process.env.CURRENCY;
const CURRENCY_SYMBOL = process.env.CURRENCY_SYMBOL;
//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require('async');
var mongoose = require('mongoose');

//Include schema
var utils = require('../config/utils');
const Carts = require("../models/carts");
const Products = require("../models/products");
const Coupons = require("../models/coupons");
const Orders = require("../models/orders");
const couponsApplied =  require("../models/coupons_applied");
const ShippingMethod = require("../models/shipping_method");

const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
const router = express.Router();

router.post("/getAvailableShippingMethod", async (req, res) => {
    
    try{
        ShippingMethod.findOne({ slug : 'ups'} , 
            async function (err, response) {
                if (err ) {
                    res.send({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                        from: 'shippment_UPS_config'
                    });
                }else if(response) {
                    
                    res.send({
                        status: 1,
                        message: "Success! ",
                        methods: response.carrier_services
                    });                    
                } else {
                    res.send({
                        status: 0,
                        message: "Sorry no shipping methods found" ,
                        from: 'shippment_UPS_config'
                    }); 
                }
                
            }
        )
    } catch (e) {
        res.send({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
            from: 'shippment_UPS_config'
        });
    }
});

router.post("/clearCart", async (req, res) => {
    
    const { cartId } = req.body;  

    if(!cartId) {
        res.send({
            status: 1,
            cartProducts: [],
            cartTotal: {
                productQuantity: 0,
                totalPrice: 0,
                currencyId: CURRENCY,
                currencyFormat: CURRENCY_SYMBOL
            }
        });
    }

    try {
        Carts.findByIdAndDelete({ _id: cartId  }, async function (err,log) {
            res.send({
                status: 1,
                cartProducts: [],
                cartTotal: {
                    productQuantity: 0,
                    totalPrice: 0,
                    currencyId: CURRENCY,
                    currencyFormat: CURRENCY_SYMBOL
                }
            });
        });
    } catch (err) {
        res.send({
            status: 1,
            cartProducts: [],
            cartTotal: {
                productQuantity: 0,
                totalPrice: 0,
                currencyId: CURRENCY,
                currencyFormat: CURRENCY_SYMBOL
            }
        });
    }

});
router.post("/saveCart", async (req, res) => {

    action = "save_cart"; 
    
    const { cartId, cartTotal, cartProducts, } = req.body;  
    userId =    req.body.userId;
    couponCode = req.body.coupon_code;
    productIds = [];    
    products = []; 
    cartUniqueId = cartId;   
    cartProducts.forEach(product => { 
        if(product.quantity > 0 ) {            
            products[product._id] = parseInt(product.quantity);
            productIds.push(mongoose.Types.ObjectId(product._id));
        }
    });

    if( productIds.length <= 0 ) {

        if(!cartUniqueId) {
            res.send({
                status: 1,
                cartProducts: [],
                cartTotal: {
                    productQuantity: 0,
                    totalPrice: 0,
                    currencyId: CURRENCY,
                    currencyFormat: CURRENCY_SYMBOL
                },
                couponDetails: {
                    couponcode: '',
                    discount: 0
                }
            });
        }

        try {
            Carts.findByIdAndDelete({ _id: cartUniqueId  }, async function (err,log) {
                res.send({
                    status: 1,
                    cartProducts: [],
                    cartTotal: {
                        productQuantity: 0,
                        totalPrice: 0,
                        currencyId: CURRENCY,
                        currencyFormat: CURRENCY_SYMBOL
                    },
                    couponDetails: {
                        couponcode: '',
                        discount: 0
                    }
                });
            });
        } catch (err) {
            res.send({
                status: 1,
                cartProducts: [],
                cartTotal: {
                    productQuantity: 0,
                    totalPrice: 0,
                    currencyId: CURRENCY,
                    currencyFormat: CURRENCY_SYMBOL
                },
                couponDetails: {
                    couponcode: '',
                    discount: 0
                }
            });
        }

        return;
    }

    try {
        async.waterfall([  
            getCartId,  
            getCartProducts,
            saveCartItems,
            getCoupon,
            checkCouponApplied, 
            processCoupon
    
        ], function (error, response) {
            if (error) {
                res.send({
                    status: 1,
                    cartId: '',
                    cartProducts: [],
                    cartTotal: {
                        productQuantity: 0,
                        totalPrice: 0,
                        currencyId: CURRENCY,
                        currencyFormat: CURRENCY_SYMBOL
                    },
                    couponDetails: {
                        couponcode: '',
                        discount: 0
                    }
                });
                return;
            } else  {
    
                res.send({
                    status: 1,
                    cartId: response.cartId,
                    cartProducts: response.cartItems,
                    cartTotal: response.cartTotal,
                    couponDetails: {
                        couponcode : response.coupon.couponcode,
                        discount: response.discount
                    },
                    results: response
                })
            }
        });
    } catch (e) {
        res.send({
            status: 1,
            cartId: '',
            cartProducts: [],
            cartTotal: {
                productQuantity: 0,
                totalPrice: 0,
                currencyId: CURRENCY,
                currencyFormat: CURRENCY_SYMBOL
            },
            couponDetails: {
                couponcode: '',
                discount: 0
            }
        });
    }
});
function getCartId(callback) {

    let results = {};
    if(!cartUniqueId) {
        results.isNew = true;
        results.cartId = '';
        results.cartDetails = {};
        callback(null, results);
    } else {
        Carts.findOne( {_id: mongoose.Types.ObjectId(cartUniqueId) },
            async function ( err, response ) {

                
                if(err) {
                    results.isNew = true;
                    results.cartId = '';
                    results.cartDetails = {};
                    callback(null, results);
                }
                if(response) {
                    results.isNew = false;
                    results.cartId = response._id;
                    results.cartDetails = response
                    callback(null, results);
                } 
                else {
                    results.isNew = true;
                    results.cartId = '';
                    results.cartDetails = {};
                    callback(null, results); 
                }
            }
        )
    }
}
function getCartProducts(results, callback) {
 
    try{
        Products.find({
            _id: {$in: productIds},
            page_status: "1"
            },
            async function (err, response) {
                
                if (err ) {
                    callback({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message,
                        from: 'cart products fetch'
                    },null);
                } 
                if(!response) {
                    callback({
                        status: 0,
                        message: "Oops! cart is empty" ,
                        from: 'empty cart'
                    }, null); 
                }
                if (response.length > 0) {  

                    let total = 0;
                    let totalQuantity = 0;
                    let cartItems  =   [];

                    for(let product of response) {

                        if(utils.productAvailable(product,products[product._id])) {

                            let price = utils.getPrice(product.price);
                            let lineItemTotal =  utils.lineItemTotal(price, products[product._id]);
                            total = total +lineItemTotal;
                            totalQuantity += products[product._id]
                            cartItems.push({
                                '_id': product._id,
                                'product_name': product.product_name,
                                'images': product.images,
                                'inventory': product.inventory,
                                'quantity': products[product._id],
                                'price': product.price,
                                'slug': product.slug,
                                'url': product.url,
                            }) ;
                        } 
                    };
                    results.cartItems = cartItems;
                    results.cartTotal = {
                        productQuantity: totalQuantity,
                        totalPrice: total,
                        currencyId: CURRENCY,
                        currencyFormat: CURRENCY_SYMBOL
                    }
                    callback(null,results); 
                } 
            }
        )
    } catch (e) {
        callback({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
            from: 'cart products fetch'
        }, null);
    }

}
function saveCartItems(results, callback) {

    
    let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
    if(results.isNew) {
        
        const newCart = new Carts({
            products: results.cartItems,
            total: results.cartTotal,
            createdon: createdon,
            user_id: userId
        });

        try {
            newCart
            .save()
            .then(() => {
                results.cartId = newCart._id;
                callback(null, results);
            })
            .catch((err) => {
                callback({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                    from: 'cart Query save'
                },null);
            });
        } catch (err) {
            callback({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
                from: 'cart Query save catch'
            },null);
        
        }

    } else {

        Carts.findByIdAndUpdate(
            {_id:results.cartId}, 
            {
                products: results.cartItems,
                total: results.cartTotal,
                createdon: createdon 
            },            
            async function (err,response) {
            if (err) {
              callback({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
              }, null);
            } else {
              callback(null, results);
            }
        });

    }
}
router.post("/getCartItems", async (req, res) => {
    
    cartUniqueId =  req.body.id ; 
    couponCode = req.body.coupon_code;
    action = "get_cart"; 
    token = req.body.token;
    payload = { _id : ''};
    if(token) {
        payload = jwt.verify(token, jwtKey);
    }
    try {
        async.waterfall([ 
            getCartId,
            processCartItems ,
            getCoupon, 
            checkCouponApplied, 
            processCoupon  

        ], function (error, response) {
            if (error) {
                res.send({
                    status: 1,
                    cartId: '',
                    cartProducts: [],
                    cartTotal: {
                        productQuantity: 0,
                        totalPrice: 0,
                        currencyId: CURRENCY,
                        currencyFormat: CURRENCY_SYMBOL
                    },
                    couponDetails: {
                        couponcode : '',
                        discount: 0
                    }
                });
                return;
            } else  {
    
                res.send({
                    status: 1,
                    cartId: response.cartId,
                    cartProducts: response.cartItems,
                    cartTotal: response.cartTotal,
                    couponDetails: {
                        couponcode : response.coupon.couponcode,
                        discount: response.discount
                    }
                })
            }
        });
    } catch (e) {
        res.send({
            status: 1,
            cartId: '',
            cartProducts: [],
            cartTotal: {
                productQuantity: 0,
                totalPrice: 0,
                currencyId: CURRENCY,
                currencyFormat: CURRENCY_SYMBOL
            },
            couponDetails: {
                couponcode : '',
                discount: 0
            }
        });
    }    
});
function processCartItems(results, callback) {

    if(results.isNew) {
        callback({
            status: 0,
            message: "Oops! cart is empty" ,
            from: 'empty cart'
        }, null); 
    } else {

        let productIds = [];    
        let products = [];  
        results.cartDetails.products.forEach(product => { 
            if(product.quantity > 0 ) {            
                products[product._id] = parseInt(product.quantity);
                productIds.push(product._id);
            }
        });

        if( productIds.length <= 0 ) {
            callback({
                status: 0,
                message: "Oops! cart is empty" ,
                from: 'empty cart'
            }, null); 
        }

        try{
            Products.find({
                _id: {$in: productIds},
                page_status: "1"
                },
                async function (err, response) {

                    if (err ) {
                        callback({
                            status: 0,
                            message: "Oops! " + err.name + ": " + err.message,
                            from: 'cart products fetch'
                        },null);
                    } 
                    if(!response) {
                        callback({
                            status: 0,
                            message: "Oops! cart is empty" ,
                            from: 'empty cart'
                        }, null); 
                    }
                    if (response.length > 0) {  
    
                        let total = 0;
                        let totalQuantity = 0;
                        let cartItems  =   [];
    
                        for(let product of response) {
    
                            if(utils.productAvailable(product,products[product._id])) {
    
                                let price = utils.getPrice(product.price);
                                let lineItemTotal =  utils.lineItemTotal(price, products[product._id]);
                                total = total +lineItemTotal;
                                totalQuantity += products[product._id]
                                cartItems.push({
                                    '_id': product._id,
                                    'product_name': product.product_name,
                                    'images': product.images,
                                    'inventory': product.inventory,
                                    'quantity': products[product._id],
                                    'price': product.price,
                                    'slug': product.slug,
                                    'url': product.url,
                                }) ;
                            } 
                        };
                        results.cartItems = cartItems;
                        results.cartTotal = {
                            productQuantity: totalQuantity,
                            totalPrice: total,
                            currencyId: CURRENCY,
                            currencyFormat: CURRENCY_SYMBOL
                        }
                        callback(null,results); 
                    } 
                }
            )
        } catch (e) {
            callback({
                status: 0,
                message: "Oops! " + e.name + ": " + e.message,
                from: 'cart products fetch'
            }, null);
        }

    }
}
router.post("/applyCoupon", async (req, res) => {

    action = "apply_coupon";
    couponCode=  req.body.coupon_code;  
    cartUniqueId    =   req.body.cartId;
    token = req.body.token;

    if(!couponCode) {
        res.send({
            status: 0,
            message: "Please enter coupon code",
            from: "empty coupon"
        });
        return;
    }
    try {
        async.waterfall([ 
            getCartId,
            processCartItems   ,
            getCoupon,
            checkCouponApplied, 
            processCoupon

        ], function (error, response) {
            if (error) {
                res.send(error);
            } else  {
    
                res.send({
                    status: 1,
                    coupon: {
                        couponcode : response.coupon.couponcode,
                        discount: response.discount
                    }
                })
            }
        });
    } catch (e) {
        callback({
            status: 0,
            message: "Oops! " + e.name + ": " + e.message,
            from: 'async cart'
        }, null);
    }    
});
function getCoupon(results, callback) {

    if( couponCode == ''  ) {
        processCallback(results,callback, action,{
            status: 0,
            message: "Please enter coupon code",
            from: "empty coupon"
        });
    } else {

        try {

            Coupons.findOne( {couponcode: couponCode, couponstatus: 1  },
                async function ( err, response ) {

                    if(err || !response) {

                        const error = {
                            status: 0,
                            message: action == "save_cart" ? "The coupon code applied is removed due to some error": "Please enter valid coupon code",
                            from:'fetch coupon'
                        }
                        processCallback(results,callback,action,error)
                        
                    } else if(response) {

                        const expiryValid = checkCouponExpiry(response)
                        const productValid = checkCouponValidWithProducts(response, results);
                        const userValid = checkCouponValidWIthUser( response, results ); 
                        if ( expiryValid && userValid && productValid.isValid) {

                            results.couponProductSpecific = productValid.productCoupon;
                            results.coupon = response;
                            callback(null, results);

                        } else {

                            const error = {
                                status: 0,
                                message: action == "save_cart" ? "Your coupon code is now invalid": "Please enter valid coupon code",
                                from:'fetch coupon'
                            }
                            processCallback(results,callback,action,error)
                        }
                        
                    } 

                }
            )
        }
        catch (err) {
            const error = {
                status: 0,
                message: "Oops! " + e.name + ": " + e.message,
                from: 'get coupon'
            }
            processCallback(results,callback,action,error) ; 
            
        }

    }

}
function checkCouponApplied(results, callback) {

    results.discountApplied = 0;
    const user_id = results.cartDetails.user_id ? results.cartDetails.user_id : '' ;
    if ( (couponCode == '' && user_id == '') || ( couponCode == '' && user_id !== '' )  ) {
        processCallback(results,callback,action,{}) ; 
    } else if( couponCode !== '' && user_id == '' ) {
        callback(null, results);
    } else {
        
        try {
            couponsApplied.aggregate( [
                {$match: { "couponcode" : couponCode, "user_id": user_id  } },
                { $group: { _id: "$user_id", discountApplied: { $sum: "$amount" } } }
                ],
                async function ( err, response ) {

                    
                    if(err) {
                        const error = {
                            status: 0,
                            message: action == "save_cart" ? "The coupon code applied is removed due to some error":"Please enter valid coupon code",
                            from:'order coupon'
                        };
                        processCallback(results,callback,action,error) ;                        
                    } else if(response.length > 0) {

                        const discountApplied = response[0].discountApplied;

                        if(results.coupon.discounttype == 1) {

                            if( (results.coupon.amount - discountApplied ) > 0 ) {

                                results.discountApplied = discountApplied;
                                callback(null, results);
                            } else {

                                const error = {
                                    status: 0,
                                    message: "Sorry you have already applied the coupon",
                                    from:'order coupon applied'
                                };
                                processCallback(results,callback,action,error) ; 

                            }


                        } else {

                            const error = {
                                status: 0,
                                message: "Sorry you have already applied the coupon",
                                from:'order coupon applied'
                            };
                            processCallback(results,callback,action,error) ; 
                        }                           
                    } 
                    else {
                        callback(null, results);
                    }
                }
            )
        } catch ( err ) {
            const error = {
                status: 0,
                message: "Oops! " + err.name + ": " + err.message,
                from: 'check coupon applied',
            };
            processCallback(results,callback,action,error) ;                 
        }
    }
}
function checkCouponExpiry( response ) {

    let isValidCoupon = true;
    if(response.exiprydate) {

        var currentDate = new Date(new Date().toDateString());
        var exipryDate = new Date(new Date(response.exiprydate).toDateString());
        if(currentDate.getTime() > exipryDate.getTime()){
            isValidCoupon = false;                                    
        }                  
    } 
    return isValidCoupon;
}
function checkCouponValidWithProducts( response, results ) {

    let isValid = false;
    let productCoupon = false;
    if(response.products.length > 0 ) {

        for(var x in response.products) {
            results.cartItems.forEach( item => {
                if(item._id.toString() === response.products[x]) {
                    isValid = true;
                    productCoupon = true;
                }
            })                            
        }
    } else  {
        isValid = true;
    }
    return {isValid : isValid, productCoupon : productCoupon };
}
function checkCouponValidWIthUser( response, results ) {
        
    let isValid = false;
    if(response.users.length > 0) {

        const user_id = results.cartDetails.user_id ? results.cartDetails.user_id : -1 ;
        response.users.forEach( user => {
            if(user.toString() === user_id.toString() ) {
                isValid = true;
            }
        }) 

    } else {
        isValid= true;
    }
    return isValid;
}
function resetCouponData( ) {

    const coupon = {
        couponcode: '',
        amount: 0,
        discounttype: 1,        
    }
    return coupon;
}
function processCoupon(results, callback) {

    if(couponCode == '' && ( action == "get_cart" || action == "save_cart") ) {
        processCallback(results,callback,action,{}) ; 
    } else {

        if( results.couponProductSpecific == true ) {

            let discount = 0;
            if(results.coupon.products ) {

                for(var x in results.coupon.products) {
                    results.cartItems.forEach( item => {
   
                        if(item._id.toString() === results.coupon.products[x]) {

                            if(results.coupon.discounttype == 1) {

                                discount = ( results.coupon.amount - results.discountApplied ) > 0 ? ( results.coupon.amount - results.discountApplied ) : 0;

                            } else {
        
                                const total     =  parseFloat(utils.getPrice(item.price)) * item.quantity;
                                const percent   =  results.coupon.amount;  
                                discount = discount + total * percent /100;       
                            }

                        }
                    })                            
                }
            }

            if( results.cartTotal.totalPrice < discount ) {
                discount = results.cartTotal.totalPrice;
            } 

            results.discount = discount;
            callback(null, results);

        } else {

            let discount = 0;
            if(results.coupon.discounttype == 1) {
                discount = ( results.coupon.amount - results.discountApplied ) > 0 ? ( results.coupon.amount - results.discountApplied ) : 0;
            } else {
                const total = results.cartTotal.totalPrice 
                const percent =  results.coupon.amount;  
                discount = total * percent /100;        
            }

            if( results.cartTotal.totalPrice < discount ) {
                discount = results.cartTotal.totalPrice;
            } 
            results.discount = discount;
            callback(null, results);

          
        }
    }
}
function processCallback(results, callback, action, error) {

    if(action == "get_cart" || action == "save_cart") {
        results.coupon = resetCouponData();
        results.discount = 0;
        results.coupon_error = error;
        callback(null, results);

    } else {
        callback(error, null);
    }

}


module.exports = router;







