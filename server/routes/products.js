//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");
var mongoose = require("mongoose");


var utils = require('../config/utils');

//Include schema
const Products = require("../models/products");
const Categories = require("../models/categories");

const Integrator = require("../models/integrator");
// const ToothzenItems = require("../../modules/items");
const config = require("../config/config.json");
const {
  response
} = require("express");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();



/*-------------Get Single product details------------------*/
router.post("/getSingleproduct", async (req, res) => {
  
  const slug = req.body.slug;
  console.log('getSingleproduct12212');
  try {
    //payload = jwt.verify(token, jwtKey);
    Products.findOne({
        slug: slug,
        page_status: "1"
      },
      async function (err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          
          if (response) {

            let relatedProductIds = response.related_products;
            console.log('relatedProductIds',relatedProductIds);
            Products.find({
                _id: {
                  $in: relatedProductIds
                },
                page_status: "1"
              }, {
                _id: 1,
                product_name: 1,
                images: 1,
                slug: 1,
                url: 1,
                tabdata:1,
                short_description:1,
                product_sub_text:1,
                meta_tag : 1,
                meta_description : 1,
                product_tag : 1,
                downloadbtn : 1,
                downloadcontent1 : 1
              },
              async function (err, relatedProducts) {
                console.log('relatedProducts',relatedProducts);
                Categories.find({
                  _id: {
                    $in: response.categories
                  },
                  status: "1"
                }, 
                async function (err, categoryDetails) {
  
                  res.send({
                    status: 1,
                    message: "Fetched Product  details successfully.",
                    singleDetails: response,
                    relatedProducts: relatedProducts,
                    categoryDetails: categoryDetails
                  });
  
                });




                /* res.send({
                  status: 1,
                  message: "Fetched Product  details successfully.",
                  singleDetails: response,
                  relatedProducts: relatedProducts
                }); */

              });


              
          } else {

            res.send({
              status: 0,
              message: "Details Unavailable.",
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});



/*------------- getallproduct------------------*/
router.post("/getAllProducts", async (req, res) => {
  try {

 
    categorySlugs = req.body.categories;
    attributeIds = req.body.attributes;
    var parentId = "";

    console.log('categorySlugs',categorySlugs);


    async.parallel({
        categoryDetails: function (callback) {
          const childCategorySlug = categorySlugs[categorySlugs.length - 1];

          console.log('childCategorySlug',childCategorySlug);
          // get parent id
          Categories.findOne({
              slug: childCategorySlug,
            },
            async function (err, childCategory) {
              if (err || !childCategory) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              }
              if (childCategory) {
                var parentId = childCategory.parent_id;
                // get child categories
                Categories.find({
                    parent_id: parentId,
                  }, {
                    category_name: 1,
                    slug: 1,
                  },
                  async function (err, subCategories) {
                    if (err) {
                      callback({
                          status: 0,
                          message: "Error fetching data",
                        },
                        null
                      );
                    }
                    result = {
                      childCategory: childCategory,
                      subCaregories: subCategories,
                    };

                    callback(null, result);
                  }
                ) .collation({ locale: 'en' })
                .sort({ order: 1 ,category_name: 1});
              }
            }
          );
        },


        categorySubDetails: function (callback) {
          const childCategorySlug = categorySlugs[categorySlugs.length - 1];

          console.log('childCategorySlug',childCategorySlug);
          // get parent id
          Categories.findOne({
              slug: childCategorySlug,
            },
            async function (err, childCategory) {
              if (err || !childCategory) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              }
              if (childCategory) {
                var parentId = childCategory._id;
                // get child categories
                Categories.find({
                    parent_id: parentId,
                  }, {
                    category_name: 1,
                    slug: 1,
                  },
                  async function (err, subCategories) {
                    if (err) {
                      callback({
                          status: 0,
                          message: "Error fetching data",
                        },
                        null
                      );
                    }

                    
                      result.subCat = subCategories ? subCategories : [];
                     
               
                    callback(null, result);
                  }
                ) .collation({ locale: "en" })
                .sort({ category_name: 1 });
              }
            }
          ).collation({ locale: 'en' })
          .sort({ order: 1 ,category_name: 1 })
        },
        productDetails: function (callback) {




          const childCategorySlug = categorySlugs[categorySlugs.length - 1];

          console.log('childCategorySlug',childCategorySlug);
          // get parent id
          Categories.findOne({
              slug: childCategorySlug,
            },
            async function (err, childCategory) {
              if (err || !childCategory) {
                callback({
                    status: 0,
                    message: "Error fetching data",
                  },
                  null
                );
              }
              if (childCategory) {
                var parentId = childCategory._id;
                // get child categories
                Categories.find({
                    parent_id: parentId,
                  }, {
                    category_name: 1,
                    slug: 1,
                  },
                  async function (err, subCategories) {
                    if (err) {
                      callback({
                          status: 0,
                          message: "Error fetching data",
                        },
                        null
                      );
                    }

                    
                  //    result.subCat = subCategories ? subCategories : [];
                     
                  let CatSlugArray = [];
                  console.log('before categorySlugs 2',categorySlugs , subCategories );
                  if(categorySlugs[categorySlugs.length - 1]){  // current category slug
                    CatSlugArray.push(categorySlugs[categorySlugs.length - 1]);
                  }
        
        
                  if(result.subCat != 'undefined' && subCategories != null && subCategories.length){
                    subCategories.forEach(function(entry) { // pushing child category slug
                      console.log('entry',entry.slug);
                      CatSlugArray.push(entry.slug);
                  });
        
                  }
                 
                  console.log('after categorySlugs 2',CatSlugArray);
        
                  Categories.find({
                      slug: {
                        $in: CatSlugArray,
                      },
                    }, {
                      _id: 1,
                      category_name: 1,
                    },
                    async function (err, categoryInfo) {
                      if (err || !categoryInfo) {
                        callback({
                            status: 0,
                            message: "Error fetching data",
                          },
                          null
                        );
                      }
                      if (categoryInfo) {
                        var catgoryIds = [];
                        categoryInfo.forEach(function (category) {
                          catgoryIds.push(category._id);
                        });
        
                        console.log('catgoryIds',catgoryIds);
                        // const andCondition = [{
                        //   categories: {
                        //     $all: catgoryIds
                        //   }}];
                        let  andCondition = {}
                        if (attributeIds.length) {
        
                            andCondition =  {
                              categories: {
                                $in: catgoryIds
                              },
                              productAttribute: {
                                $in: attributeIds
                              },
                              page_status: "1" 
                            }
                        } else {
                            andCondition =  {
                            categories: {
                              $in: catgoryIds
                            },
                            page_status: "1" 
                          }
                        }
                        
                        // andCondition.push({ page_status: "1" });
        
                         console.log('all ids',attributeIds , andCondition);
        
                        
                        Products.find(andCondition,
                          async function (err, products) {
                            if (err) {
                              callback({
                                  status: 0,
                                  message: "Error fetching data",
                                },
                                null
                              );
                            }
                            callback(null, products);
                          }
                        )
                        .collation({ locale: 'en' })
                        .sort({ orderno: 1 })
                      }
                    }
                  ).collation({ locale: 'en' })
                  .sort({ order: 1 ,category_name: 1 })
              
                  }
                ) .collation({ locale: 'en' })
                .sort({ order: 1  ,category_name: 1})
              }
            }
          ).collation({ locale: 'en' })
          .sort({ order: 1  ,category_name: 1})



          
          // let CatSlugArray = [];
          // console.log('before categorySlugs 2',categorySlugs , result.subCat );
          // if(categorySlugs[categorySlugs.length - 1]){  // current category slug
          //   CatSlugArray.push(categorySlugs[categorySlugs.length - 1]);
          // }


          // if(result.subCat != 'undefined' && result.subCat != null && result.subCat.length){
          //   result.subCat.forEach(function(entry) { // pushing child category slug
          //     console.log('entry',entry.slug);
          //     CatSlugArray.push(entry.slug);
          // });

          // }
         
          // console.log('after categorySlugs 2',CatSlugArray);

          // Categories.find({
          //     slug: {
          //       $in: CatSlugArray,
          //     },
          //   }, {
          //     _id: 1,
          //     category_name: 1,
          //   },
          //   async function (err, categoryInfo) {
          //     if (err || !categoryInfo) {
          //       callback({
          //           status: 0,
          //           message: "Error fetching data",
          //         },
          //         null
          //       );
          //     }
          //     if (categoryInfo) {
          //       var catgoryIds = [];
          //       categoryInfo.forEach(function (category) {
          //         catgoryIds.push(category._id);
          //       });

          //       console.log('catgoryIds',catgoryIds);
          //       // const andCondition = [{
          //       //   categories: {
          //       //     $all: catgoryIds
          //       //   }}];
          //       let  andCondition = {}
          //       if (attributeIds.length) {

          //           andCondition =  {
          //             categories: {
          //               $all: catgoryIds
          //             },
          //             productAttribute: {
          //               $in: attributeIds
          //             },
          //             page_status: "1" 
          //           }
          //       } else {
          //           andCondition =  {
          //           categories: {
          //             $in: catgoryIds
          //           },
          //           page_status: "1" 
          //         }
          //       }
                
          //       // andCondition.push({ page_status: "1" });

          //        console.log('all ids',attributeIds , andCondition);

                
          //       Products.find(andCondition,
          //         async function (err, products) {
          //           if (err) {
          //             callback({
          //                 status: 0,
          //                 message: "Error fetching data",
          //               },
          //               null
          //             );
          //           }
          //           callback(null, products);
          //         }
          //       );
          //     }
          //   }
          // );
        },
      },
      function (err, results) {
        
        if (err !== null) {
          res.send({
            status: 0,
            message: "Oops! " + err,
          });
        } else {
          res.send({
            status: 1,
            subCategories: results.categoryDetails.subCaregories,
            categoryInfo: results.categoryDetails.childCategory,
            productDetails: results.productDetails,
          });
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

/*------------------ get products of scada categories -------------------*/

router.post("/getAllScadaproducts", async (req, res) => {
  const categoryslug = req.body.slug;

  try {
    Categories.findOne({
        slug: categoryslug,
      },
      async function (err, mainCategory) {
        if (err || !mainCategory) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          var parentId = mainCategory._id; //(scada id)
          // get child categories
          Categories.find({
              parent_id: parentId,
            }, {
              parent_id: 1,
              _id: 1,
              slug:1,
            },
            async function (err, subCategories) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message,
                });
              } else {
                if (subCategories.length > 0) {
                  var categories1 = [];
                  var categories2 = [];
                  categories1.push(
                    subCategories[0].parent_id,
                    subCategories[0]._id
                  );
                  categories2.push(
                    subCategories[1].parent_id,
                    subCategories[1]._id
                  );

                  async.parallel({
                      Category1Data: function (callback) {

                        Products.find({
                            categories: {
                              $all: categories1
                            },
                            page_status: "1"
                          },
                          async function (err, cat1Data) {
                            if (err || !cat1Data) {
                              callback({
                                  status: 0,
                                  message: "Error fetching data",
                                },
                                null
                              );
                            }
                            result = {
                              cat1Data: cat1Data,

                            };
                            callback(null, result);
                          }
                        );
                      },
                      Category2Data: function (callback) {
                        Products.find({
                            categories: {
                              $all: categories2,
                            },
                            page_status: "1"
                          },

                          async function (err, cat2Data) {
                            if (err || !cat2Data) {
                              callback({
                                  status: 0,
                                  message: "Error fetching data",
                                },
                                null
                              );
                            }
                            result = {
                              cat2Data: cat2Data,
                            };
                            callback(null, result);
                          }
                        );
                      },
                    },
                    function (err, results) {

                      if (err !== null) {
                        res.send({
                          status: 0,
                          message: "Oops! " + err,
                        });
                      } else {
                        res.send({
                          status: 1,
                          subCategories:subCategories,
                          category1Data: results.Category1Data.cat1Data,
                          category2Data: results.Category2Data.cat2Data,
                        });
                      }
                    }
                  );
                } else {
                  res.send({
                    status: 0,
                    message: "Category Details Unavailable.",
                  });
                }
              }
            }
          );
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

router.post("/checkAddToCart", async (req, res) => {
  const productSelected = req.body.product;
  const quantitySelected = req.body.quantity;

  if (!productSelected) {
    res.send({
      status: 0,
      message: "Oops! No product found",
    });
  }
  try {

    var id = mongoose.Types.ObjectId(productSelected);
    var slug = productSelected.slug;
    Products.findOne({
        _id: id,
        page_status: "1"
      },
      async function (err, product) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          
          if (product) {
            var cartFlag = utils.productAvailable(product, quantitySelected);
            res.send({
              status: cartFlag,
              message: (cartFlag ? "Product available" : "Product out of stock"),
            });
          } else {
            
            res.send({
              status: 0,
              message: "Details Unavailable.",
            });
          }
        }
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

router.post("/checkAddToCartMultiple", async (req, res) => {
  let productsSelected = req.body.products;
  let productIds = [];
  let cartProducts = [];
  productsSelected.forEach(product => {

    if (parseInt(product.quantity) > 0) {
      cartProducts[product._id] = product.quantity;
      productIds.push(product._id);
    }
  });

  if (productIds.length <= 0) {
    res.send({
      status: 0,
      message: "Oops! No product selected",
    });
  }
  try {

    Products.find({
        _id: {
          $in: productIds,
        },
        page_status: "1"
      },
      async function (err, products) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          
          if (products.length > 0) {

            let isNotProductAvaialble = false;
            let msg = "";
            for (var x in products) {

              let isProductAvaialble = utils.productAvailable(products[x], cartProducts[products[x]._id]);
              if (isProductAvaialble == false) {
                isNotProductAvaialble = true;
                msg = " Sorry the selected quantity of product '" + products[x].product_name + "' is not available";
              }
            }

            res.send({
              status: (isNotProductAvaialble == true ? false : true),
              message: (isNotProductAvaialble == true ? msg : "Products are available"),
            });
          } else {
            
            res.send({
              status: 0,
              message: "Sorry no products found",
            });
          }
        }
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});


/// get all product for integrator resource

router.post("/getAllProductsIntegrator", async (req, res) => {
  try {

    Products.find({
        page_status: "1"
      },
      async function (err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (response.length > 0) {
            res.send({
              status: 1,
              message: "Fetched details successfully.",
              data: response
            });
          } else {
            res.send({
              productsDetails: response,
              status: 0,
              message: "No details available."
            });
          }
        }
      }
    ).sort({
      _id: 1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

});



// get all category for integrator resources

router.post("/getAllCategoryIntegrator", async (req, res) => {
  try {

    Categories.find({},
      async function (err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (response.length > 0) {
            res.send({
              status: 1,
              message: "Fetched details successfully.",
              data: response
            });
          } else {
            res.send({
              categoryDetails: response,
              status: 0,
              message: "No details available."
            });
          }
        }
      }
    ).sort({
      _id: 1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

});





// get document, softare, image for integrator resource


router.post("/getAllIntegrator", async (req, res) => {
  const token = req.body.token;
  const category = req.body.category;

  if (!token) {
    res.send({
      status: 0,
      message: "Invalid Request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      Integrator.find({
          category_name: category,
        },
        async function (err, products) {
          
          if (err) {
            res.send({
              status: 0,
              message: "Error fetching data",
            });
          } else {
            res.send({
              status: 1,
              message: "Success!",
              data: products,
            });
          }
        });

    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message,
      });
    }
  }
});



// router.post("/getAllIntegrator", async (req, res) => {
//   const token = req.body.token;
//   const category = req.body.category;

//   if (!token) {
//     res.send({ status: 0, message: "Invalid Request" });
//   } else {
//   try {
//     payload = jwt.verify(token, jwtKey);

//     Integrator.aggregate([{

//       $unwind : "$category_name",
//       // $unwind : "$product_name",
//       //   $unwind : "images",
//       //     $unwind: "document",
//         //   $match:{ "$and":[ {"product_name":"1"},
//         //  {"category_name":"1"} ] }
//           }],
//     async function (err, products) { 

//       if (err) {
//         res.send({
//             status: 0,
//             message: "Error fetching data",
//           });
//       } else {
//       res.send({
//         status: 1,
//         message: "Success!",
//         data: products,
//       });
//       }
//     });

//   } catch (e) {
//     res.send({
//       status: 0,
//       message: "Oops! " + e.name + ": " + e.message,
//     });
//   }
// }
// });









// router.post("/getAllIntegrator", async (req, res) => {
//   const token = req.body.token;
//   if (!token) {
//     res.send({ status: 0, message: "Invalid Request" });
//   } else {
//   try {
//   payload = jwt.verify(token, jwtKey);

// const product_name =req.body.product_name;
// const category_name =req.body.category_name;
// const file_name =req.body.file_name;

//   Integrator.aggregate([
//     {

//       $unwind : "software",
//         $unwind : "images",
//           $unwind: "document",
//           $match:{ "$and":[ {"product_name": "5ef5d1de7b08912c14fbbf6b"},
//           {"category_name":"5f02b262416010206830a412"}, 
//           {"file_name":"Rectangle 89.zip"} ] }

//     },

//   ]).exec((err, result) => {

//     if (err) {
//       res.send({
//         status: 0,
//         message: "Oops! " + err.name + ": " + err.message,
//       });
//     } else {
//       res.send({
//         status: 1,
//         message: "Success!",
//         data: result,
//       });
//     }
//   });


//   } catch (e) {
//     res.send({
//       status: 0,
//       message: "Oops! " + e.name + ": " + e.message,
//     });
//   }
// }
// });




router.post("/searchFilename", async (req, res) => {
  const file_key = req.body.data.file_key;
  //const country = req.body.data.country;

  try {

    Integrator.find({
        $and: [{
          $or: [{
            file_name: new RegExp(file_key, "i")
          }]
        }, ],


      },

      async function (err, response) {
        
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {

          if (response) {

            res.send({
              status: 1,
              message: "Fetched  details successfully.",
              singleDetails: response
            });

          } else {

            res.send({
              status: 0,
              message: "No Results Found."
            });

          }
        }
      }
    )
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

});




/// get all product for integrator resource

router.post("/getAllRecommanedProducts", async (req, res) => {
  try {
    console.log('ids are ', req.body);
    Products.find({
        _id: req.body.ids
      },
      async function (err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (response.length > 0) {
            res.send({
              status: 1,
              message: "Fetched details successfully.",
              data: response
            });
          } else {
            res.send({
             
              status: 0,
              message: "No details available."
            });
          }
        }
      }
    ).sort({
      _id: 1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

});


module.exports = router;