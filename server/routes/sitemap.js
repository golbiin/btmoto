/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");
const mongoose = require("mongoose");
/* Include schema */
const News = require("../models/news");
const Articles = require("../models/articles");
const Careers = require("../models/careers");
const Training = require("../models/videoTraining");
const Industry = require("../models/industries");
const CaseStudy = require("../models/case_study");
const Archives = require("../models/archives");
const Distributor = require("../models/international_distributor");
const Faq = require("../models/faq");
const Categories = require("../models/categories");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();

/*  Get all types of News  */
router.post("/getAllSiteMapData", async (req, res) => {
  try {
    async.parallel(
      {
        news: getSMNews, 
        articles: getSMArticles, 
        careers: getSMCareers,
        videos:getSMVideos,
        industry:getSMIndustry,
        casestudy: getSMCaseStudy,
        archives: getSMArchives,
        distributor: getSMDistributor, 
        faq: getSMFaq,
        faqtype: getSMCategories    
      },
      function(error, results) {
        if (error) {
          res.status(500).send(error);
          return;
        }
        res.send(results);
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});
 

function getSMNews(callback) {
  News.find({page_status: "1"},{slug:1, _id:0})
    .sort({ date: 1 }) 
    .exec(callback);
} 
function getSMArticles(callback) {
  Articles.find({page_status: "1"},{slug:1, _id:0})
    .sort({ date: 1 }) 
    .exec(callback);
} 
function getSMCareers(callback) {
  Careers.find({page_status: "1"},{slug:1, _id:0})
    .sort({ date: 1 }) 
    .exec(callback);
} 
function getSMVideos(callback) {
  Training.find({page_status: "1"},{"id": "$_id", _id:0})
    .sort({ date: 1 }) 
    .exec(callback);
} 
function getSMIndustry(callback) {
  Industry.find({page_status: "1"},{slug:1, _id:0})
    .sort({ date: 1 }) 
    .exec(callback);
} 
function getSMCaseStudy(callback) {
  CaseStudy.find({page_status: "1"},{slug:1, _id:0})
    .sort({ date: 1 }) 
    .exec(callback);
} 
function getSMArchives(callback) {
  Archives.find({page_status: "1"},{slug:1, _id:0})
    .sort({ date: 1 }) 
    .exec(callback);
} 
function getSMDistributor(callback) {
  Distributor.find({},{location_url:1, _id:0}) 
    .exec(callback);
} 
function getSMFaq(callback) {
  Faq.find({page_status: "1"},{slug:1, _id:0}) 
    .exec(callback);
} 
function getSMCategories(callback) {
  Categories.find({parent_id: 0},{slug:1, _id:0}) 
    .exec(callback);
} 

module.exports = router;
