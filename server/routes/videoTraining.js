/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");

/* Include schema */

const Training = require("../models/videoTraining");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();

/*  Get all types of News  */
router.post("/getAllVideoTraining", async (req, res) => {
  try {
    Training.aggregate(
      [
        // First Stage
        { $match: { page_status: "1" } },
        { $sort: { video_order: 1 } },
        {
          $group: { _id: "$category_name", videos: { $push: "$$ROOT" } }
        },
        { $sort: { _id: -1 } }
      ],
      function(err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Success",
            videos: response
          });
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/getSingleVideoTraining", async (req, res) => {
  try {
    Training.findOne({ _id: req.body.id }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        res.send({
          status: 1,
          message: "success",
          data: log
        });
      }
    });
  } catch (err) {
    res.send({
      status: 0,
      message: "Oops!Something went wrong."
    });
  }
});


/* get training by id */
router.post("/getTechVideo", async (req, res) => {
  //const id  = req.body.id;
  try {
    Training.find(
      { category_name: "Webinars"  },
      async function (err, response) {
        if (err) {
          res.send({   
            status:0,         
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if(response){
            res.send({ 
              status: 1,             
              message: "Fetched liveTraining details successfully.",
              data: response
            });
          }else{
            res.send({
              status: 0,
              message: "Details Unvailable."
            });
          }
        }
      }
    );

    
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
}
});

module.exports = router;
