const Joi = require("joi");
const config = require("./../../config/config.json");
const USER_TYPES = config.userTypes;

//admin user create validation
module.exports.RegisterUser = function(req, callback) {
  const insertvalidate = {
    first_name: Joi.string()
      .required()
      .label("First name"),
    last_name: Joi.string()
      .required()
      .label("Last name"),
    email: Joi.string()
      .required()
      .email()
      .label("Email"),
    password: Joi.string()
      .required()
      .min(8)
      .regex(/^[a-zA-Z0-9!@#$%^&*()]{8,20}$/)
      .label("Password"),
    confirm_password: Joi.any()
      .valid(Joi.ref("password"))
      .required()
      .options({ language: { any: { allowOnly: "Password do not match" } } }),
    agree_terms: Joi.string()
      .required()
      .label("Agree terms of service and privacy policy"),
    agree_newsletter: Joi.string()
      .allow(null)
      .label("Agree news letters"),
    company_name: Joi.string()
      .required()
      .label("Company name"),
    country: Joi.string()
      .required()
      .label("Country"),
    city: Joi.string()
      .required()
      .label("City"),
    zipcode: Joi.string()
      .allow("")
      .label("Zipcode"),
    
    phone_number: Joi.string()
      .required()
      .label("Phone Number"),
    interested_products: Joi.string()
      .allow(null)
      .label("Interested products"),
    bussiness_type: Joi.string()
      .allow("")
      .label("Bussiness type"),
    industry: Joi.string()
      .allow("")
      .label("Industry"),

    recovery_email: Joi.string()
      .email()
      .allow(null)
      .label("Recovery Email"),
    confirm_recovery_email: Joi.string()
      .email()
      .valid(Joi.ref("recovery_email"))
      .options({
        language: { any: { allowOnly: "Recovery email do not match" } }
      })
  };
  result = Joi.validate(req, insertvalidate);

  callback(result);
};

// user update validation
module.exports.updateUser = function(req, callback) {
  const insertvalidate = {
    _id: Joi.string()
      .required()
      .label("Invalid request"),
    first_name: Joi.string()
      .required()
      .label("First name"),
    last_name: Joi.string()
      .required()
      .label("Last name"),
    company_name: Joi.string()
      .required()
      .label("Company name"),
    country: Joi.string()
      .required()
      .label("Country"),
    city: Joi.string()
      .required()
      .label("City"),
    zipcode: Joi.string()
      .allow("")
      .label("Zipcode"),
    phone_number: Joi.string()
      .required()
      .label("Phone Number"),
    bussiness_type: Joi.string()
      .allow("")
      .label("bussiness_type"),
    industry: Joi.string()
      .allow("")
      .label("industry"),
    agree_newsletter: Joi.boolean(),
    profile_image: Joi.string()
    .allow("")
    .label("profile_image"),
  };
  result = Joi.validate(req, insertvalidate);

  callback(result);
};

//admin user recovery email validation
module.exports.updateRecoveryEmail = function(req, callback) {
  const insertvalidate = {
    _id: Joi.string()
      .required()
      .label("Invalid request"),
    recovery_email: Joi.string()
      .required()
      .email()
      .label("First name")
  };
  result = Joi.validate(req, insertvalidate);

  callback(result);
};
module.exports.adminUsercreate = function(req, callback) {
  const insertvalidate = {
    
    firstName: Joi.string()
      .required()
      .label("First name"),
    lastName: Joi.string()
      .required()
      .label("Last name"),
    email: Joi.string()
      .required()
      .email()
      .label("Email"),
    profile_image: Joi.string().label("Profile Image"),
    password: Joi.string()
      .required()
      .min(8)
      .regex(/^[a-zA-Z0-9!@#$%^&*()]{8,20}$/)
      .label("Password"),   
    userName: Joi.string()
      .required()
      .label("User name"), 
  };
  result = Joi.validate(req.body, insertvalidate);
  callback(result);
};

//admin user create validation
module.exports.Usercreate = function(req, callback) {
  const insertvalidate = {
    firstName: Joi.string()
      .required()
      .label("First name"),
    lastName: Joi.string()
      .required()
      .label("Last name"),
    email: Joi.string()
      .required()
      .email()
      .label("Email"),
    profile_image: Joi.string().label("Profile Image"),
    password: Joi.string()
      .required()
      .min(8)
      .regex(/^[a-zA-Z0-9!@#$%^&*()]{8,20}$/)
      .label("Password"),
    usertype: Joi.string()
      .required()
      .label("User Type"),
    company_name: Joi.string()
      .required()
      .label("Company name"),
    city: Joi.string()
      .required()
      .label("City"),
    zipcode: Joi.string()
      .allow("")
      .label("Zipcode"),
    phone_number: Joi.string()
      .required()
      .label("Phone Number"),
    country: Joi.string()
      .allow("")
      .label("Country"),
    drivelink: Joi.string()
      .allow(null)
      .allow("")
      .label("Drive link"),
    recovery_email: Joi.string()
      .email()
      .allow(null)
      .label("Recovery Email"),
    agree_newsletter: Joi.boolean(),
    bussiness_type: Joi.string()
      .allow("")
      .label("bussiness_type"),
    industry: Joi.string()
      .allow("")
      .label("industry"),
    interested_products: Joi.string()
      .allow("")
      .label("interested_products")
  };
  result = Joi.validate(req.body, insertvalidate);
  callback(result);
};

//signup validation
module.exports.Usersignup = function(req, callback) {
  const Signupvalidate = {
    account_type: Joi.string().required(),
    first_name: Joi.string()
      .required()
      .label("First name"),
    last_name: Joi.string()
      .required()
      .label("Last name"),
    email: Joi.string()
      .required()
      .email()
      .label("Email"),
    password: Joi.string()
      .required()
      .min(8)
      .regex(/^[a-zA-Z0-9!@#$%^&*()]{8,20}$/)
      .label("Password")
  };
  result = Joi.validate(req.body, Signupvalidate);
  callback(result);
};
//login validation
module.exports.validateLogin = function(req, callback) {
  switch (req.body.usertype) {
    case USER_TYPES.SUBSCRIBER:
      const loginSchema = {
        username: Joi.string()
          .required()
          .email()
          .label("Email"),
        password: Joi.string()
          .required()
          .label("Password"),
        usertype: Joi.string()
          .required()
          .label("User Type"),
        cartId: Joi.string().allow("")
      };
      result = Joi.validate(req.body, loginSchema);
      break;
    case USER_TYPES.INTEGRATOR:
      const loginIntegratorSchema = {
        username: Joi.string()
          .required()
          .label("Integrator Id"),
        usertype: Joi.string()
          .required()
          .label("User Type"),
        cartId: Joi.string().allow("")
      };
      result = Joi.validate(req.body, loginIntegratorSchema);
      break;
    case USER_TYPES.DISTRIBUTOR:
      const loginDistributorSchema = {
        username: Joi.string()
          .required()
          .label("Integrator Id"),
        usertype: Joi.string()
          .required()
          .label("User Type"),
        cartId: Joi.string().allow("")
      };
      result = Joi.validate(req.body, loginDistributorSchema);
      break;
  }
  callback(result);
};
//adminlogin validation
module.exports.validateadminLogin = function(req, callback) {
  const loginSchema = {
    username: Joi.string()
      .required()
      .label("username"),
    password: Joi.string()
      .required()
      .label("Password")
  };
  result = Joi.validate(req.body, loginSchema);
  callback(result);
};

//admin cms page create validation
module.exports.Pagecreate = function(req, callback) {
  const insertvalidatepage = {
    title: Joi.string()
      .required()
      .label("Title"),
    slug: Joi.string()
    .required()
    .label("Slug"),
    editor_content: Joi.any()
  };
  result = Joi.validate(req.body, insertvalidatepage);
  callback(result);
};

module.exports.AddCoupon = function(req, callback) {
  const insertvalidate = {
    amount: Joi.number()
      .required()
      .label("Amount "),
    couponcode: Joi.string()
      .required()
      .label("Coupon code"),
    couponstatus: Joi.string()
      .required()
      .label("Coupon status"),
    discounttype: Joi.string()
      .required()
      .label("Discount type"),
    exiprydate: Joi.string()
      .required()
      .label("Exipry date"),
    guestusers: Joi.string()
      .allow(null)
      .label("Guest users"),
    products: Joi.array()
      .allow(null)
      .label("Products"),
    users: Joi.array()
      .allow(null)
      .label("Users"),
    id: Joi.string().allow("")
  };
  result = Joi.validate(req, insertvalidate);

  callback(result);
};

module.exports.createVideoTraining = function(req, callback) {
  const insertvalidate = {
    video_title: Joi.string()
      .required()
      .label("Video title"),
    video_url: Joi.string()
      .required()
      .label("Video Url"),
    category_name: Joi.string()
      .required()
      .label("Category"),
    video_order: Joi.any()
      .optional()
      .label("video order")
  };
  result = Joi.validate(req.body.data, insertvalidate);
  callback(result);
};

module.exports.updateVideoTraining = function(req, callback) {
  const insertvalidate = {
    _id: Joi.string()
      .required()
      .label("Id"),
    video_title: Joi.string()
      .required()
      .label("Video title"),
    video_url: Joi.string()
      .required()
      .label("Video Url"),
    category_name: Joi.string()
      .required()
      .label("Category"),
    video_order: Joi.any()
      .optional()
      .label("video order")
  };
  result = Joi.validate(req.body.data, insertvalidate);
  callback(result);
};

module.exports.createEventLocation = function(req, callback) {
  const insertvalidate = {
    location_name: Joi.string()
    .required()
    .label("Location Name"),
    address: Joi.string()
    .required()
    .label("Address"),
    description: Joi.string()
    .required()
    .label("Description"),
    longitude: Joi.string()
    .required()
    .label("Longitude"),
    latitude: Joi.any()
    .required()
    .label("Latitude")
  };
  result = Joi.validate(req.body.data, insertvalidate);
  callback(result);
};

module.exports.updateEventLocation = function(req, callback) {
  const insertvalidate = {
    _id: Joi.string()
    .required()
    .label("Location Name"),
    location_name: Joi.string()
    .required()
    .label("Location Name"),
    address: Joi.string()
    .required()
    .label("Address"),
    description: Joi.string()
    .required()
    .label("Description"),
    longitude: Joi.string()
    .required()
    .label("Longitude"),
    latitude: Joi.any()
    .required()
    .label("Latitude")
  };
  result = Joi.validate(req.body.data, insertvalidate);
  callback(result);
};