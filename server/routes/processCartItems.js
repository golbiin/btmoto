var utils = require('../utils');
const Products = require("../models/products");
const { CURRENCY, CURRENCY_SYMBOL } = require("./cart");
function processCartItems(results, callback) {

    if (results.isNew) {
        callback({
            status: 0,
            message: "Oops! cart is empty",
            from: 'empty cart'
        }, null);
    }
    else {

        let productIds = [];
        let products = [];
        results.cartDetails.products.forEach(product => {
            if (product.quantity > 0) {
                products[product._id] = parseInt(product.quantity);
                productIds.push(product._id);
            }
        });

        if (productIds.length <= 0) {
            callback({
                status: 0,
                message: "Oops! cart is empty",
                from: 'empty cart'
            }, null);
        }

        try {
            Products.find({
                _id: { $in: productIds },
                page_status: "1"
            },
                async function (err, response) {

                    if (err) {
                        callback({
                            status: 0,
                            message: "Oops! " + err.name + ": " + err.message,
                            from: 'cart products fetch'
                        }, null);
                    }
                    if (!response) {
                        callback({
                            status: 0,
                            message: "Oops! cart is empty",
                            from: 'empty cart'
                        }, null);
                    }
                    if (response.length > 0) {

                        let total = 0;
                        let totalQuantity = 0;
                        let cartItems = [];

                        for (let product of response) {

                            if (utils.productAvailable(product, products[product._id])) {

                                let price = utils.getPrice(product.price);
                                let lineItemTotal = utils.lineItemTotal(price, products[product._id]);
                                total = total + lineItemTotal;
                                totalQuantity += products[product._id];
                                cartItems.push({
                                    '_id': product._id,
                                    'product_name': product.product_name,
                                    'images': product.images,
                                    'inventory': product.inventory,
                                    'quantity': products[product._id],
                                    'price': product.price,
                                    'slug': product.slug,
                                    'url': product.url,
                                });
                            }
                        };
                        results.cartItems = cartItems;
                        results.cartTotal = {
                            productQuantity: totalQuantity,
                            totalPrice: total,
                            currencyId: CURRENCY,
                            currencyFormat: CURRENCY_SYMBOL
                        };
                        callback(null, results);
                    }
                }
            );
        }
        catch (e) {
            callback({
                status: 0,
                message: "Oops! " + e.name + ": " + e.message,
                from: 'cart products fetch'
            }, null);
        }

    }
}
exports.processCartItems = processCartItems;
