/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
const path = require("path");
const joi = require("joi");
const bcrypt = require("bcryptjs");
const config = require("../config/config.json");

/* Include schema */
const CimonForms = require("../models/cforms");
const CformEntries = require("../models/cform_entries");
const querystring = require("querystring");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;
const siteurl = config.siteurl;

/* Create Cform Details */
router.post("/create", async (req, res) => {
  const token = req.body.token;
  try {
    //Form existance check
    CimonForms.findOne({ form_name: req.body.data.form_name }, async function(
      error,
      log
    ) {
      if (log != null) {
        res.send({
          status: 0,
          message: "Form name already exist."
        });
      } else {
        //  payload = jwt.verify(token, jwtKey);
        let created_on = moment().format("MM-DD-YYYY hh:mm:ss");
        const newCform = new CimonForms({
          form_name: req.body.data.form_name,
          content: req.body.data.content,
          status: 1,
          created_on: created_on
        });
        await newCform
          .save()
          .then(() => {
            res.send({
              status: 1,
              message: "You have added a Cimon Form."
            });
          })
          .catch(err => {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          });
      }
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

router.post("/list", async (req, res) => {
  const token = req.body.token;
  const filter = req.body.filter;
  let filterCondition = {};
  if (filter.date) {
    const selectedDate = filter.date.split("/");
    const start = new Date(selectedDate[1], selectedDate[0] - 1, 1);
    const end = new Date(selectedDate[1], selectedDate[0] - 1, 31);
    filterCondition = { created_on: { $gte: start, $lte: end } };
  }
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      CimonForms.find(filterCondition, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: "Fetched forms successfully.",
              data: log
            });
          } else {
            res.send({
              status: 0,
              message: "No forms available."
            });
          }
        }
      }).sort({ _id: -1 });
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/listByDate", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      CimonForms.aggregate(
        [
          {
            $project: {
              year: { $year: "$created_on" },
              month: { $month: "$created_on" }
            }
          },
          {
            $group: {
              _id: null,
              created_on: { $addToSet: { year: "$year", month: "$month" } }
            }
          },
          { $sort: { _id: -1 } }
        ],
        function(err, data) {
          if (err) res.send(err);
          else {
            res.send({
              status: 1,
              options: data
            });
          }
        }
      );
    } catch (e) {
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/delete", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        CimonForms.findByIdAndDelete({ _id: req.body.form_id }, async function(
          err,
          log
        ) {
          if (err) {
            res.send(err);
          } else {
            res.send({ status: 1, message: "Forms deleted" });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.post("/trash", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        CimonForms.findByIdAndUpdate(
          { _id: req.body.form_id },
          { status: 0 },
          async function(err, log) {
            if (err) {
              res.send(err);
            } else {
              res.send({ status: 1, message: "Forms trashed." });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});
router.post("/actionHandler", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      var update_value = {
        status: req.body.action
      };

      try {
        const condition = { _id: req.body.formids };
        if (req.body.action == "3") {
          CimonForms.deleteMany(
            { _id: { $in: req.body.formids } },
            async function(err, log) {
              if (err) {
                res.send({ status: 1, message: "Something went wrong" + err });
              } else {
                res.send({
                  status: 1,
                  message: "Forms Trash deleted successfully."
                });
              }
            }
          );
        } else {
          CimonForms.updateMany(condition, update_value, async function(
            err,
            log
          ) {
            if (err) {
              res.send({ status: 1, message: "Something went wrong" + err });
            } else {
              res.send({ status: 1, message: "Forms Updated successfully" });
            }
          });
        }
      } catch (e) {
        res.send({ status: 1, message: "Something went wrong" + e });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.post("/fetchDetails", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        CimonForms.findOne({ _id: req.body.form_id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: log
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops!Something went wrong."
        });
      }
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Oops!jwt token malformed." });
      }
      res.send({
        status: 0,
        message: "Oops! Something went wrong with JWT token verification...!"
      });
    }
  }
});

router.get("/content", async (req, res) => {
  try {
    let form_id = req.query.form_id;
    CimonForms.findOne({ _id: form_id }, async function(err, log) {
      if (err) {
        res.send([]);
      } else {
        res.send(log.content);
      }
    });
  } catch (err) {
    res.send([]);
  }
});

router.post("/fetchDetailsByName", async (req, res) => {
  try {
    CimonForms.findOne({ form_name: req.body.form_name }, async function(
      err,
      log
    ) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        res.send({
          status: 1,
          message: "success",
          data: log
        });
      }
    });
  } catch (err) {
    res.send({
      status: 0,
      message: "Oops!Something went wrong."
    });
  }
});

router.post("/edit", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      //Form existance check
      CimonForms.findOne(
        { form_name: req.body.data.form_name, _id: { $ne: req.body.data._id } },
        async function(error, log) {
          if (log != null) {
            res.send({
              status: 0,
              message: "Form name already exist."
            });
          } else {
            CimonForms.find(
              {
                _id: { $ne: req.body.data._id }
              },
              async function(err, log) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  if (log.length > 0) {
                    var condition = { _id: req.body.data._id };
                    var update_value = {
                      form_name: req.body.data.form_name,
                      content: req.body.data.content
                    };
                    CimonForms.findOneAndUpdate(
                      condition,
                      update_value,
                      async function(err, log) {
                        if (err) {
                          res.send({
                            status: 0,
                            message: "Oops! " + err.name + ": " + err.message
                          });
                        } else {
                          res.send({
                            status: 1,
                            message: "Forms Updated successfully"
                          });
                        }
                      }
                    );
                  } else {
                    var condition = { _id: req.body.data._id };
                    var update_value = {
                      form_name: req.body.data.form_name,
                      content: req.body.data.content
                    };
                    CimonForms.findOneAndUpdate(
                      condition,
                      update_value,
                      async function(err, log) {
                        if (err) {
                          res.send({
                            status: 0,
                            message: "Oops! " + err.name + ": " + err.message
                          });
                        } else {
                          res.send({
                            status: 1,
                            message: "Forms Updated successfully"
                          });
                        }
                      }
                    );
                  }
                }
              }
            ).sort({ date: 1 });

            ///////////////////
            payload = jwt.verify(token, jwtKey);

            ////////////////
          }
        }
      );
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        res.send({ status: 0, message: "Token verification failed." });
      }
      res.send({
        status: 0,
        message: "Oops! " + e.name + ": " + e.message
      });
    }
  }
});

router.get("/countries", async (req, res) => {
  var optionsData = [
    { text: "Select country", value: " " },
    { text: "Afghanistan", value: "Afghanistan" },
    { text: "Åland Islands", value: "Åland Islands" },
    { text: "Albania", value: "Albania" },
    { text: "Algeria", value: "Algeria" },
    { text: "American Samoa", value: "American Samoa" },
    { text: "Andorra", value: "Andorra" },
    { text: "Angola", value: "Angola" },
    { text: "Anguilla", value: "Anguilla" },
    { text: "Antarctica", value: "Antarctica" },
    { text: "Antigua and Barbuda", value: "Antigua and Barbuda" },
    { text: "Argentina", value: "Argentina" },
    { text: "Armenia", value: "Armenia" },
    { text: "Aruba", value: "Aruba" },
    { text: "Australia", value: "Australia" },
    { text: "Austria", value: "Austria" },
    { text: "Azerbaijan", value: "Azerbaijan" },
    { text: "Bahamas", value: "Bahamas" },
    { text: "Bahrain", value: "Bahrain" },
    { text: "Bangladesh", value: "Bangladesh" },
    { text: "Barbados", value: "Barbados" },
    { text: "Belarus", value: "Belarus" },
    { text: "Belgium", value: "Belgium" },
    { text: "Belize", value: "Belize" },
    { text: "Benin", value: "Benin" },
    { text: "Bermuda", value: "Bermuda" },
    { text: "Bhutan", value: "Bhutan" },
    { text: "Bolivia", value: "Bolivia" },
    {
      text: "Bonaire, Sint Eustatius and Saba",
      value: "Bonaire, Sint Eustatius and Saba"
    },
    { text: "Bosnia and Herzegovina", value: "Bosnia and Herzegovina" },
    { text: "Botswana", value: "Botswana" },
    { text: "Bouvet Island", value: "Bouvet Island" },
    { text: "Brazil", value: "Brazil" },
    {
      text: "British Indian Ocean Territory",
      value: "British Indian Ocean Territory"
    },
    { text: "Brunei Darussalam", value: "Brunei Darussalam" },
    { text: "Bulgaria", value: "Bulgaria" },
    { text: "Burkina Faso", value: "Burkina Faso" },
    { text: "Burundi", value: "Burundi" },
    { text: "Cambodia", value: "Cambodia" },
    { text: "Cameroon", value: "Cameroon" },
    { text: "Canada", value: "Canada" },
    { text: "Cape Verde", value: "Cape Verde" },
    { text: "Cayman Islands", value: "Cayman Islands" },
    { text: "Central African Republic", value: "Central African Republic" },
    { text: "Chad", value: "Chad" },
    { text: "Chile", value: "Chile" },
    { text: "China", value: "China" },
    { text: "Christmas Island", value: "Christmas Island" },
    { text: "Cocos (Keeling) Islands", value: "Cocos (Keeling) Islands" },
    { text: "Colombia", value: "Colombia" },
    { text: "Comoros", value: "Comoros" },
    {
      text: "Congo, Republic of the (Brazzaville)",
      value: "Congo, Republic of the (Brazzaville)"
    },
    {
      text: "Congo, the Democratic Republic of the (Kinshasa)",
      value: "Congo, the Democratic Republic of the (Kinshasa)"
    },
    { text: "Cook Islands", value: "Cook Islands" },
    { text: "Costa Rica", value: "Costa Rica" },
    { text: "Côte d'Ivoire, Republic of", value: "Côte d'Ivoire, Republic of" },
    { text: "Croatia", value: "Croatia" },
    { text: "Cuba", value: "Cuba" },
    { text: "Curaçao", value: "Curaçao" },
    { text: "Cyprus", value: "Cyprus" },
    { text: "Czech Republic", value: "Czech Republic" },
    { text: "Denmark", value: "Denmark" },
    { text: "Djibouti", value: "Djibouti" },
    { text: "Dominica", value: "Dominica" },
    { text: "Dominican Republic", value: "Dominican Republic" },
    { text: "Ecuador", value: "Ecuador" },
    { text: "Egypt", value: "Egypt" },
    { text: "El Salvador", value: "El Salvador" },
    { text: "Equatorial Guinea", value: "Equatorial Guinea" },
    { text: "Eritrea", value: "Eritrea" },
    { text: "Estonia", value: "Estonia" },
    { text: "Ethiopia", value: "Ethiopia" },
    {
      text: "Falkland Islands (Islas Malvinas",
      value: "Falkland Islands (Islas Malvinas"
    },
    { text: "Faroe Islands", value: "Faroe Islands" },
    { text: "Fiji", value: "Fiji" },
    { text: "Finland", value: "Finland" },
    { text: "France", value: "France" },
    { text: "French Guiana", value: "French Guiana" },
    { text: "French Polynesia", value: "French Polynesia" },
    {
      text: "French Southern and Antarctic Lands",
      value: "French Southern and Antarctic Lands"
    },
    { text: "Gabon", value: "Gabon" },
    { text: "Gambia, The", value: "Gambia, The" },
    { text: "Georgia", value: "Georgia" },
    { text: "Germany", value: "Germany" },
    { text: "Ghana", value: "Ghana" },
    { text: "Gibraltar", value: "Gibraltar" },
    { text: "Greece", value: "Greece" },
    { text: "Greenland", value: "Greenland" },
    { text: "Grenada", value: "Grenada" },
    { text: "Guadeloupe", value: "Guadeloupe" },
    { text: "Guam", value: "Guam" },
    { text: "Guatemala", value: "Guatemala" },
    { text: "Guernsey", value: "Guernsey" },
    { text: "Guinea", value: "Guinea" },
    { text: "Guinea-Bissau", value: "Guinea-Bissau" },
    { text: "Guyana", value: "Guyana" },
    { text: "Haiti", value: "Haiti" },
    {
      text: "Heard Island and McDonald Islands",
      value: "Heard Island and McDonald Islands"
    },
    { text: "Holy See (Vatican City)", value: "Holy See (Vatican City)" },
    { text: "Honduras", value: "Honduras" },
    { text: "Hong Kong", value: "Hong Kong" },
    { text: "Hungary", value: "Hungary" },
    { text: "Iceland", value: "Iceland" },
    { text: "India", value: "India" },
    { text: "Indonesia", value: "Indonesia" },
    { text: "Iran, Islamic Republic of", value: "Iran, Islamic Republic of" },
    { text: "Iraq", value: "Iraq" },
    { text: "Ireland", value: "Ireland" },
    { text: "Isle of Man", value: "Isle of Man" },
    { text: "Israel", value: "Israel" },
    { text: "Italy", value: "Italy" },
    { text: "Jamaica", value: "Jamaica" },
    { text: "Japan", value: "Japan" },
    { text: "Jersey", value: "Jersey" },
    { text: "Jordan", value: "Jordan" },
    { text: "Kazakhstan", value: "Kazakhstan" },
    { text: "Kenya", value: "Kenya" },
    { text: "Kiribati", value: "Kiribati" },
    {
      text: "Korea, Democratic People's Republic of",
      value: "Korea, Democratic People's Republic of"
    },
    { text: "Korea, Republic of", value: "Korea, Republic of" },
    { text: "Kuwait", value: "Kuwait" },
    { text: "Kyrgyzstan", value: "Kyrgyzstan" },
    { text: "Laos", value: "Laos" },
    { text: "Latvia", value: "Latvia" },
    { text: "Lebanon", value: "Lebanon" },
    { text: "Lesotho", value: "Lesotho" },
    { text: "Liberia", value: "Liberia" },
    { text: "Libya", value: "Libya" },
    { text: "Liechtenstein", value: "Liechtenstein" },
    { text: "Lithuania", value: "Lithuania" },
    { text: "Luxembourg", value: "Luxembourg" },
    { text: "Macao", value: "Macao" },
    { text: "Macedonia, Republic of", value: "Macedonia, Republic of" },
    { text: "Madagascar", value: "Madagascar" },
    { text: "Malawi", value: "Malawi" },
    { text: "Malaysia", value: "Malaysia" },
    { text: "Maldives", value: "Maldives" },
    { text: "Mali", value: "Mali" },
    { text: "Malta", value: "Malta" },
    { text: "Marshall Islands", value: "Marshall Islands" },
    { text: "Martinique", value: "Martinique" },
    { text: "Mauritania", value: "Mauritania" },
    { text: "Mauritius", value: "Mauritius" },
    { text: "Mayotte", value: "Mayotte" },
    { text: "Mexico", value: "Mexico" },
    {
      text: "Micronesia, Federated States of",
      value: "Micronesia, Federated States of"
    },
    { text: "Moldova", value: "Moldova" },
    { text: "Monaco", value: "Monaco" },
    { text: "Mongolia", value: "Mongolia" },
    { text: "Montenegro", value: "Montenegro" },
    { text: "Montserrat", value: "Montserrat" },
    { text: "Morocco", value: "Morocco" },
    { text: "Mozambique", value: "Mozambique" },
    { text: "Myanmar", value: "Myanmar" },
    { text: "Namibia", value: "Namibia" },
    { text: "Nauru", value: "Nauru" },
    { text: "Nepal", value: "Nepal" },
    { text: "Netherlands", value: "Netherlands" },
    { text: "New Caledonia", value: "New Caledonia" },
    { text: "New Zealand", value: "New Zealand" },
    { text: "Nicaragua", value: "Nicaragua" },
    { text: "Niger", value: "Niger" },
    { text: "Nigeria", value: "Nigeria" },
    { text: "Niue", value: "Niue" },
    { text: "Norfolk Island", value: "Norfolk Island" },
    { text: "Northern Mariana Islands", value: "Northern Mariana Islands" },
    { text: "Norway", value: "Norway" },
    { text: "Oman", value: "Oman" },
    { text: "Pakistan", value: "Pakistan" },
    { text: "Palau", value: "Palau" },
    { text: "Palestine, State of", value: "Palestine, State of" },
    { text: "Panama", value: "Panama" },
    { text: "Papua New Guinea", value: "Papua New Guinea" },
    { text: "Paraguay", value: "Paraguay" },
    { text: "Peru", value: "Peru" },
    { text: "Philippines", value: "Philippines" },
    { text: "Pitcairn", value: "Pitcairn" },
    { text: "Poland", value: "Poland" },
    { text: "Portugal", value: "Portugal" },
    { text: "Puerto Rico", value: "Puerto Rico" },
    { text: "Qatar", value: "Qatar" },
    { text: "Réunion", value: "Réunion" },
    { text: "Romania", value: "Romania" },
    { text: "Russian Federation", value: "Russian Federation" },
    { text: "Rwanda", value: "Rwanda" },
    { text: "Saint Barthélemy", value: "Saint Barthélemy" },
    {
      text: "Saint Helena, Ascension and Tristan da Cunha",
      value: "Saint Helena, Ascension and Tristan da Cunha"
    },
    { text: "Saint Kitts and Nevis", value: "Saint Kitts and Nevis" },
    { text: "Saint Lucia", value: "Saint Lucia" },
    { text: "Saint Martin", value: "Saint Martin" },
    { text: "Saint Pierre and Miquelon", value: "Saint Pierre and Miquelon" },
    {
      text: "Saint Vincent and the Grenadines",
      value: "Saint Vincent and the Grenadines"
    },
    { text: "Samoa", value: "Samoa" },
    { text: "San Marino", value: "San Marino" },
    { text: "Sao Tome and Principe", value: "Sao Tome and Principe" },
    { text: "Saudi Arabia", value: "Saudi Arabia" },
    { text: "Senegal", value: "Senegal" },
    { text: "Serbia", value: "Serbia" },
    { text: "Seychelles", value: "Seychelles" },
    { text: "Sierra Leone", value: "Sierra Leone" },
    { text: "Singapore", value: "Singapore" },
    { text: "Sint Maarten (Dutch part)", value: "Sint Maarten (Dutch part)" },
    { text: "Slovakia", value: "Slovakia" },
    { text: "Slovenia", value: "Slovenia" },
    { text: "Solomon Islands", value: "Solomon Islands" },
    { text: "Somalia", value: "Somalia" },
    { text: "South Africa", value: "South Africa" },
    {
      text: "South Georgia and South Sandwich Islands",
      value: "South Georgia and South Sandwich Islands"
    },
    { text: "South Sudan", value: "South Sudan" },
    { text: "Spain", value: "Spain" },
    { text: "Sri Lanka", value: "Sri Lanka" },
    { text: "Sudan", value: "Sudan" },
    { text: "Suriname", value: "Suriname" },
    { text: "Swaziland", value: "Swaziland" },
    { text: "Sweden", value: "Sweden" },
    { text: "Switzerland", value: "Switzerland" },
    { text: "Syrian Arab Republic", value: "Syrian Arab Republic" },
    { text: "Taiwan", value: "Taiwan" },
    { text: "Tajikistan", value: "Tajikistan" },
    {
      text: "Tanzania, United Republic of",
      value: "Tanzania, United Republic of"
    },
    { text: "Thailand", value: "Thailand" },
    { text: "Timor-Leste", value: "Timor-Leste" },
    { text: "Togo", value: "Togo" },
    { text: "Tokelau", value: "Tokelau" },
    { text: "Tonga", value: "Tonga" },
    { text: "Trinidad and Tobago", value: "Trinidad and Tobago" },
    { text: "Tunisia", value: "Tunisia" },
    { text: "Turkey", value: "Turkey" },
    { text: "Turkmenistan", value: "Turkmenistan" },
    { text: "Turks and Caicos Islands", value: "Turks and Caicos Islands" },
    { text: "Tuvalu", value: "Tuvalu" },
    { text: "Uganda", value: "Uganda" },
    { text: "Ukraine", value: "Ukraine" },
    { text: "United Arab Emirates", value: "United Arab Emirates" },
    { text: "United Kingdom", value: "United Kingdom" },
    { text: "United States", value: "United States" },
    {
      text: "United States Minor Outlying Islands",
      value: "United States Minor Outlying Islands"
    },
    { text: "Uruguay", value: "Uruguay" },
    { text: "Uzbekistan", value: "Uzbekistan" },
    { text: "Vanuatu", value: "Vanuatu" },
    {
      text: "Venezuela, Bolivarian Republic of",
      value: "Venezuela, Bolivarian Republic of"
    },
    { text: "Vietnam", value: "Vietnam" },
    { text: "Virgin Islands, British", value: "Virgin Islands, British" },
    { text: "Virgin Islands, U.S.", value: "Virgin Islands, U.S." },
    { text: "Wallis and Futuna", value: "Wallis and Futuna" },
    { text: "Western Sahara", value: "Western Sahara" },
    { text: "Yemen", value: "Yemen" },
    { text: "Zambia", value: "Zambia" },
    { text: "Zimbabwe", value: "Zimbabwe" }
  ];
  res.send(optionsData);
});

/* Push Cform Entries */
router.post("/create_entries", async (req, res) => {
  try {
    let created_on = moment().format("MM-DD-YYYY hh:mm:ss");
    const newCformEntries = new CformEntries({
      form_id: req.body.data.form_id,
      data: req.body.data.data,
      status: 1,
      created_on: created_on
    });
    await newCformEntries
      .save()
      .then(() => {
        res.send({
          status: 1,
          message: "You have successfully submitted your entries."
        });
      })
      .catch(err => {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message
    });
  }
});

module.exports = router;
