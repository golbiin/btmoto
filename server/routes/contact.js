/* load dependencies */
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
const path = require("path");
const joi = require("joi");
const bcrypt = require("bcryptjs");


/* Include schema */
const ContactSubscriber = require("../models/contact");
const Contact = require("../models/contact_address");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;

/* http request port set */
const router = express.Router();
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;
const siteurl = config.siteurl;

/* Create Contact Details */
router.post("/createContact", async (req, res) => {
  const token = req.body.token;
  try {
  //  payload = jwt.verify(token, jwtKey);
    const newContact = new ContactSubscriber({
      fullname: req.body.data.fullname,
      country: req.body.data.country,
      phone: req.body.data.phone,
      email: req.body.data.email,
      comments: req.body.data.comments,
    });
    await newContact.save()
      .then(() => {
        res.send({
          status: 1,
          message: "You have added a new addresss.",
        });
      })
      .catch((err) => {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
        });
      });

  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
  // if (!token) {
  //   res.send({
  //     status: 0,
  //     message: "Invalid Request"
  //   });
  // } else {
  
  // }
});

/* Create Distributor Contact Details */
router.post("/createDistributorContact", async (req, res) => {
  const token = req.body.token;
 
  //  payload = jwt.verify(token, jwtKey);
    const newContact = new ContactSubscriber({
      fullname: req.body.data.fullname,
      country: req.body.data.country,
      region: req.body.data.region,
      phone: req.body.data.phone,
      email: req.body.data.email,
      comments: req.body.data.comments,
    });
  try {
    await newContact
    .save()
      .then(() => {
        sendContactMail(newContact, (result,err) => {
          if (result == "success") { 
            res.send({
              status: 1,
              message:
                "Your Subscribtion Completed Succesfully .",
            });
          } else if (result == "failed") {
            res.send({
              status: 0,
              type: "error",
              message:
                "Oops! Something went wrong with sending  mail...!"+ err,
            });
          }
        });
      })
      .catch((err) => {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message,
        });
      });
  } 
  catch (e) {
    res.send({
      status: 0,
      type: "error catch",
      message: "Oops! Something went wrong with sending  mail...!",
    });
  }
 

});


/* Subscriber mail in distributor contact pop up */

function sendContactMail(results, callback) {
  let header = fs
    .readFileSync(path.join(__dirname + "/emails/header.html"), {
      encootding: "utf-8",
    })
    .toString();

  var view = {
    siteurl: config.siteurl,
    imageUrl: config.imageUrl,
  };
  var createHeader = mustache.render(header, view);

  let footer = fs
    .readFileSync(path.join(__dirname + "/emails/footer.html"), {
      encootding: "utf-8",
    })
    .toString();

  var view = {
    imageUrl: config.imageUrl,
  };
  var createFooter = mustache.render(footer, view);

  let template = fs
    .readFileSync(path.join(__dirname + "/emails/distributor-contactemail.html"), {
      encootding: "utf-8",
    })
    .toString();
    console.log(43434,results);
  var view = {
    email: results.email,
    fullname: results.fullname,
    phone: results.phone,
    country: results.country,
    region: results.region,
    comments: results.comments,
  
  };

  var createTemplate = mustache.render(template, view);
  var htmlContent = createHeader + createTemplate + createFooter;

  sgMail.setApiKey(sendGridKey);
  const message = {
    to: config.admin_email,
    from : { email : config.fromEmail , name: 'Cimon'},
    subject: "Distributor Contact",
    html: htmlContent,
  };

  try {
    sgMail
      .send(message)
      .then(() => {       
        callback("success");
      })
      .catch((error) => {
        callback("failed");
        console.error(error.toString()); 
        

   
      });
  } catch (err) {
    callback({
      status: "failed",
    });
  }
}

/* Subscriber mail in distributor contact pop up */

/* Get all Contacts*/
router.post("/getAllcontact", async (req, res) => {
  try {
    Contact.find({
      },
      async function (err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (response.length > 0) {
            res.send({
              status: 1,
              message: "Fetched details successfully.",
              contact: response
            });
          } else {
            res.send({
              jobDetails: response,
              status: 0,
              message: "No details available."
            });
          }
        }
      }
    ).sort({
      _id: 1
    });
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

});


/* send mail */

function sendMailToLab(newContact, callback) {
  let template = fs
    .readFileSync(path.join(__dirname + "/mail-templates/labQuestion.html"), {
      encootding: "utf-8",
    })
    .toString();
  var view = {
    fullname: req.body.data.fullname,
    country: req.body.data.country,
    phone: req.body.data.phone,
    email: req.body.data.email,
    comments: req.body.data.comments,
  };
  var createTemplate = mustache.render(template, view);
  
  sgMail.setApiKey(sendGridKey);
  const message = {
    to: newContact.to,
    from : { email : config.fromEmail , name: 'Cimon'},
    subject: "Cimon: Ask a Question to the Lab",
    html: createTemplate,
  };
  try {
    sgMail
      .send(message)
      .then(() => {
        callback("success");
        
      })
      .catch((error) => {
        /*Log friendly error*/
        console.error("send err", error.toString());
        /*Extract error msg */
        const {
          message,
          code,
          response
        } = error;
        
        /*Extract response msg */
        callback("failed");
      });
  } catch (err) {
    callback("failed");
    
  }
}

module.exports = router;