//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
var async = require("async");

//Include schema
const Categories = require("../models/categories");
// const ToothzenItems = require("../../modules/items");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
//http request port set
const router = express.Router();
const mongoose = require("mongoose");
const Products = require("../models/products");

router.post("/getAllCategory", async (req, res) => {

  try {

    Categories.find({
      status: 1,
    },
      async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (log) {
            res.send({
              status: 1,
              message: "Fetched categories successfully.",
              data: log,
            });
          } else {
            res.send({
              status: 0,
              message: "No categories available.",
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

});

/***get all category data in ipc/plc/hmi/hybrid pages */

router.post("/getAllCategoryBySlug", async (req, res) => {
  const categoryslug = req.body.slug;


  try {
    Categories.findOne({
      slug: categoryslug,
    },
      async function (err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (response) {
            const cat_id = response._id;
            Categories.find({
              parent_id: cat_id,
            },
              async function (err, result) {
                if (err) {
                  result.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                  });
                } else {
                  if (result.length > 0) {

                    res.send({
                      status: 1,
                      message: "Fetched Category details successfully.",
                      categoryData: result,
                    });
                  } else {
                    res.send({
                      status: 0,
                      message: "Category Details Unavailable.",
                    });
                  }
                }
              }
            ).collation({ locale: 'en' })
            .sort({ order: 1 })

          } else {
            res.send({
              status: 0,
              message: "No such slug Exists.",
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});

/***get all category data in ipc/plc/hmi/hybrid pages */

/****Get plc-cm1 subcategories details****** */

router.post("/getAllCmCategories", async (req, res) => {

  const slug = req.body.slug; //cm1
  try {
    Categories.findOne({
      slug: slug
    },
      async function (err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });

        } else {

          if (response) {
            let row_id = response._id;
            Categories.find({
              parent_id: row_id,
            },
              async function (err, result) {
                if (err) {
                  result.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                  });
                } else {
                  if (result.length > 0) {

                    res.send({
                      status: 1,
                      message: "Fetched Category details successfully.",
                      categoryData: result,
                    });
                  } else {
                    res.send({
                      status: 0,
                      message: "Category Details Unavailable.",
                    });
                  }
                }
              }
            );

          } else {
            res.send({
              status: 0,
              message: "Details Unavailable."
            });

          }
        }
      }
    )
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

});



/****Get plc-cm1 subcategories details****** */

router.post("/getproductBannerData", async (req, res) => {

  console.log('------------>');
  const slug = req.body.slug;
  try {
    Categories.findOne({
      slug: slug
    },
      //  { banner_sub_description: 1},
      async function (err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });

        }
        else {

          if (response) {

            res.send({
              status: 1,
              message: "Fetched  details successfully.",
              singleDetails: response
            });

          } else {

            res.send({
              status: 0,
              message: "Details Unavailable."
            });

          }
        }
      }
    ).select('banner_sub_description banner_main_description');
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }

});




// recomended product ipc,plc,scada,hybrid-hmi,hmi page
router.post("/getAllCategoryProducts", async (req, res) => {
  const categoryslug = req.body.slug;
  try {
    Categories.findOne({
      slug: categoryslug,
    },
      async function (err, response) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (response) {
            const cat_id = response._id;
            Products.find({
              categories: {
                $in: cat_id,
              },
              page_status: "1"
            }, {
              product_name: 1,
              images: 1,
              url: 1,
              slug: 1,
            },

              async function (err, result) {
                if (err) {
                  result.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message,
                  });
                } else {
                  if (result) {
                    res.send({
                      status: 1,
                      message: "Fetched Category details successfully.",
                      categoryData: result,
                    });
                  } else {
                    res.send({
                      status: 0,
                      message: "Category Details Unavailable.",
                    });
                  }
                }
              }
            );

          } else {
            res.send({
              status: 0,
              message: "No such slug Exists.",
            });
          }
        }
      }
    );
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});




// RecomendedProduct Section

router.post("/getRecomandedProduct", async (req, res) => {
  console.log('req.body', req.body);
  let cat_id = 0;
  try {


    Products.find({
      _id: {
        $in: req.body.ids,
      },
      //page_status: "1"
    }, {
      product_name: 1,
      images: 1,
      url: 1,
      slug: 1,
    },

      async function (err, result) {
        if (err) {
          result.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message,
          });
        } else {
          if (result) {
            res.send({
              status: 1,
              message: "Fetched Category details successfully.",
              categoryData: result,
            });
          } else {
            res.send({
              status: 0,
              message: "Category Details Unavailable.",
            });
          }
        }
      }

    );

  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});




router.post("/getSubCatById", async (req, res) => {
  console.log('req.body', req.body.id);
  try {
    Categories.find({ parent_id: { $eq: req.body.id } }, async function (
      err,
      child_cat
    ) {
      res.send({
        status: 1,
        data: child_cat,
        message: "successful",
      });
    })  .collation({ locale: "en" })
    .sort({ order: 1 ,category_name: 1 });
      
  } catch (e) {
    res.send({
      status: 0,
      message: "Oops! " + e.name + ": " + e.message,
    });
  }
});



module.exports = router;

