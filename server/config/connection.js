function dbconnect(req, res, next) {
  const mongoose = require("mongoose");
  mongoose
    .connect(
      "mongodb+srv://cimon:cimon2020@cluster0-9cnav.mongodb.net/cimon",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      }
    )
    .then(() => "Connected successfully!!")
    .catch((err) => console.error("could not connect to db"));
  
}
module.exports = dbconnect;
