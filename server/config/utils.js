const config = require("./config.json");
const Counters = require("../models/counters");
require('dotenv').config();
const CURRENCY_SYMBOL    = process.env.CURRENCY_SYMBOL;
function generatePassword(){

    // Generate a random number.
    var number = Math.random()

    // Convert this number into a string.
    var string = number.toString(36)

    // Grab a section of the string as the password
    var password = string.slice(8);

    // Return the password back!
    return password;
}

function getPrice(price) {
    if(price.sales_price && price.sales_price != '' && price.sales_price != null && parseFloat(price.sales_price) > 0 ){
        return price.sales_price > 0 ? parseFloat(price.sales_price) : 0.00;
    } else{
        return price.regular_price > 0 ? parseFloat(price.regular_price) : 0.00;
    }
}

function lineItemTotal (price,quantity) {
    return parseInt(quantity)* parseFloat(price);
}

function productAvailable(product, quantity){
    if ( (
        product.inventory.manage_stock == false && 
        product.inventory.stock_availability == "In Stock"
      ) ||
      (
        (product.inventory.manage_stock == true && 
        product.inventory.manage_inventory.stock_quantity > 0 && 
        parseInt(quantity) <= parseInt(product.inventory.manage_inventory.stock_quantity) 
        ) ||
        ( product.inventory.manage_stock == true && product.inventory.manage_inventory.allow_backorders == "allow") ||
        ( product.inventory.manage_stock == true && product.inventory.manage_inventory.allow_backorders == "allow_but_notify") 
      )
      ) {
          return true;
      } 
      return false;
}

function generateOrderHtml( products, total, type=1) {

    var orderHtml = '<table cellspacing="0" cellpadding="6" border="1" style="width:100%;font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif" >';

    orderHtml += '    <thead>';
    orderHtml += '		<tr>';
    orderHtml += '			<th scope="col" style="text-align:center"></th>';
	orderHtml += '			<th scope="col" style="text-align:left">Product</th>';
	orderHtml += '			<th scope="col" style="text-align:center">Quantity</th>';
	orderHtml += '			<th scope="col" style="text-align:right">Price</th>';
    orderHtml += '		</tr>';
    orderHtml += '	</thead>';
    
    products.forEach ( product => {

        let formattedPrice =    0, productPrice = 0;
        let price = 0;
        let line_total = 0
        if(product.price) {
            price = (type=1) ? getPrice(product.price) : product.price;
            line_total = price*product.quantity;

        }

        orderHtml +=   '<tr>';
        orderHtml +=        '<td style="text-align-center;vertical-align:middle;">';
        orderHtml +=            '<img width="73px" height="63px" src='+(
                                            product.images.featured_image.image_url != null
                                            ? product.images.featured_image.image_url
                                            : config.imageUrl+'/images/placeholder.png')
                                +'>';
        orderHtml +=        '</td>';
        orderHtml +=        '<td style="text-align:left;vertical-align:middle;font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">'
        orderHtml +=                '<h4><a href="'+''+'">'+product.product_name+'</a></h4>';
        orderHtml +=        '</td>';
        orderHtml +=        '<td style="text-align:center;vertical-align:middle;font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif">'+product.quantity+'</td>';
        orderHtml +=        '<td style="text-align:right;vertical-align:middle;font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif">'+CURRENCY_SYMBOL+line_total+'</td>';
        orderHtml +=    '</tr>';
    })
    orderHtml +=   '<tfoot>';
    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left;border-top-width:4px">Subtotal:</th>';
    orderHtml +=   '        <td style="text-align:right;border-top-width:4px"><span><span>'+CURRENCY_SYMBOL+'</span>'+total.product_total+'</span></td>';
    orderHtml +=   '    </tr>';

    if(total.coupon !== '') {
        orderHtml +=   '    <tr>';
        orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left;">Discount: Coupon Applied  '+ total.coupon+'</th>';
        orderHtml +=   '        <td style="text-align:right;color:red"><span><span> - '+CURRENCY_SYMBOL+'</span>'+total.discount+'</span></td>';
        orderHtml +=   '    </tr>'; 
    }

    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left;">Shipping:</th>';
    orderHtml +=   '        <td style="text-align:right;"><span><span>'+CURRENCY_SYMBOL+'</span>'+total.shipping_total+'</span></td>';
    orderHtml +=   '    </tr>';
    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left;">Tax:</th>';
    orderHtml +=   '        <td style="text-align:right;"><span><span>'+CURRENCY_SYMBOL+'</span>'+total.tax+'</span></td>';
    orderHtml +=   '    </tr>';
    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left">Payment method:</th>';
    orderHtml +=   '        <td style="text-align:right">'+total.payment_method+'</td>';
    orderHtml +=   '    </tr>';
    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left">Total:</th>';
    orderHtml +=   '        <td style="text-align:right"><span><span>'+CURRENCY_SYMBOL+'</span>'+total.grand_total+'</span></td>';
    orderHtml +=   '    </tr>';
    orderHtml +=   '</tfoot>';
    orderHtml +=  '</table>';
    return orderHtml;

}

function generateOrderCancelHtml( products, total, type=1) {

    var orderHtml = '<table cellspacing="0" cellpadding="6" border="1" style="width:100%;font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif" >';

    orderHtml += '    <thead>';
    orderHtml += '		<tr>';
    orderHtml += '			<th scope="col" style="text-align:center"></th>';
	orderHtml += '			<th scope="col" style="text-align:left">Product</th>';
	orderHtml += '			<th scope="col" style="text-align:center">Quantity</th>';
	orderHtml += '			<th scope="col" style="text-align:right">Price</th>';
    orderHtml += '		</tr>';
    orderHtml += '	</thead>';
    
    products.forEach ( product => {

        let formattedPrice =    0, productPrice = 0;
        let price = 0;
        let line_total = 0
        if(product.price) {
            price = (type=1) ? getPrice(product.price) : product.price;
            line_total = price*product.quantity;

        }

        orderHtml +=   '<tr>';
        orderHtml +=        '<td style="text-align-center;vertical-align:middle;">';
       
        orderHtml +=        '</td>';
        orderHtml +=        '<td style="text-align:left;vertical-align:middle;font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">'
        orderHtml +=                '<h4>'+product.product_name+'</h4>';
        orderHtml +=        '</td>';
        orderHtml +=        '<td style="text-align:center;vertical-align:middle;font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif">'+product.quantity+'</td>';
        orderHtml +=        '<td style="text-align:right;vertical-align:middle;font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif">'+CURRENCY_SYMBOL+line_total+'</td>';
        orderHtml +=    '</tr>';
    })
    orderHtml +=   '<tfoot>';
    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left;border-top-width:4px">Subtotal:</th>';
    orderHtml +=   '        <td style="text-align:right;border-top-width:4px"><span><span>'+CURRENCY_SYMBOL+'</span>'+total.product_total+'</span></td>';
    orderHtml +=   '    </tr>';
    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left;">Shipping:</th>';
    orderHtml +=   '        <td style="text-align:right;"><span><span>'+CURRENCY_SYMBOL+'</span>'+total.shipping_total+'</span></td>';
    orderHtml +=   '    </tr>';
    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left;">Tax:</th>';
    orderHtml +=   '        <td style="text-align:right;"><span><span>'+CURRENCY_SYMBOL+'</span>'+total.tax+'</span></td>';
    orderHtml +=   '    </tr>';
    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left">Payment method:</th>';
    orderHtml +=   '        <td style="text-align:right">'+total.payment_method+'</td>';
    orderHtml +=   '    </tr>';
    orderHtml +=   '    <tr>';
    orderHtml +=   '        <th scope="row" colspan="3" style="text-align:left">Total:</th>';
    orderHtml +=   '        <td style="text-align:right"><span><span>'+CURRENCY_SYMBOL+'</span>'+total.grand_total+'</span></td>';
    orderHtml +=   '    </tr>';
    orderHtml +=   '</tfoot>';
    orderHtml +=  '</table>';
    return orderHtml;

}

function flattenObject(ob) {
	var toReturn = {};
	
	for (var i in ob) {
		if (!ob.hasOwnProperty(i)) continue;
		
		if ((typeof ob[i]) == 'object') {
			var flatObject = flattenObject(ob[i]);
			for (var x in flatObject) {
				if (!flatObject.hasOwnProperty(x)) continue;
				
				toReturn[x] = flatObject[x];
			}
		} else {
			toReturn[i] = ob[i];
		}
	}
	return toReturn;
};

function validateShippingDetails(object) {

    var data = flattenObject(object);

    // require the Joi module
    const Joi = require('joi');
    // define the validation schema

    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    const schema = Joi.object().keys({
        first_name: Joi.string().regex(/^[a-zA-Z ]*$/).min(3).max(50).required().label("First name"),
        last_name: Joi.string().regex(/^[a-zA-Z ]*$/).min(3).max(50).required().label("Last name"),
        email: Joi.string().email().min(5).max(50).required(),
        address: Joi.string().min(3).max(500).required().label("address"), 
        appartment : Joi.any().optional() ,
        city:Joi.string().min(3).max(200).required(),
        country: Joi.string().required(),
        region: Joi.string().required(),
        zipcode: Joi.string().required(), 
        phone: Joi.string().required().max(15).regex(phoneRegExp).label("Phone number"),
        company: Joi.any().optional()     ,
        shipping_methods:  Joi.string().required().label("Shipping methods"), 
    });
    let options = { abortEarly: false }; 
    const { error, value } = Joi.validate(data, schema, options);
    let errors = {};
    if (error) {
        for (var x in error.details) {
            let e = error.details[x];
            if ( !(e['path'] in errors)) {
                errors[e['path']] = e['message'];
            }                
        }
        return { status:false, errors: errors };
    } else {
        return {status:true};
    }

}
function padWithZeroes(number, length) {

    var myString = '' + number;
    while (myString.length < length) {
        myString = '0' + myString;
    }
    return myString;
}

function getValueForNextSequence(sequenceName){

    return new Promise((resolve,reject)=>{
        Counters.findOneAndUpdate(
            { "slug": sequenceName },
            { "$inc": { "sequence_value": 1 } },
            {new:true},
            function(err,response) {
            // work here
            if(response) {                
                return ( resolve (response.sequence_value) )
            }        
            }
        );
    });
}



const formatPrice = (x, currency) => {
    switch (currency) {
      case 'BRL':
        return x.toFixed(2).replace('.', ',');
      default:
        return x.toFixed(2);
    }
};

module.exports = {
    formatPrice,
    getPrice,
    generateOrderHtml,
    generateOrderCancelHtml,
    productAvailable,
    lineItemTotal,
    generatePassword,
    flattenObject,
    validateShippingDetails,
    getValueForNextSequence,
    padWithZeroes,
}