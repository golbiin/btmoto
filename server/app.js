//Load dependencies
var compression = require('compression');
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dbconnect = require("./config/connection");
const path = require("path");
var http = require("http");
var https = require("https");
//load routes
const manageusers = require("./routes/admin/manageUsers");
const adminauth = require("./routes/admin/authenticate");
const settings = require("./routes/admin/settings");
const upload = require("./routes/admin/uploadImage");
const productUpload = require("./routes/admin/uploadProductImage");
const manageproduct = require("./routes/admin/manageProducts");
const managepages = require("./routes/admin/managePages");
const managecontacts = require("./routes/admin/manageContact");
const adminorders = require("./routes/admin/manageOrders");
const shipping_settings =  require("./routes/admin/shipping_settings");
const coupons = require("./routes/admin/manageCoupon");
const manageTax = require("./routes/admin/manageTax");
const manageStore = require("./routes/admin/manageStore");
const integrator =  require("./routes/admin/integrator");
const mediafiles =  require("./routes/admin/mediaFiles");
const industry =  require("./routes/admin/industry");
const manageCasestudy = require("./routes/admin/manageCasestudy");
const manageFaq = require("./routes/admin/manageFaq");
const liveTraining = require("./routes/admin/liveTraining");
const videoTraining = require("./routes/admin/videoTraining");
const source = require("./routes/admin/manageSource");
const careerCategory = require("./routes/admin/manageCareerCategory");
const ProfileTab = require("./routes/admin/manageProfileTab");
const Redirection = require("./routes/admin/manageRedirection");
const IpRedirection = require("./routes/admin/manageIpRedirection");
const Attribute = require("./routes/admin/attribute");
const DistributorResources = require("./routes/admin/distributorResources");
const DistributorCategory = require("./routes/admin/manageDistributorCategory");
const IntegratorCategory = require("./routes/admin/manageIntegratorCategory");

const IntegratorResources = require("./routes/admin/integratorResources");

const auth = require("./routes/authenticate");
const news = require("./routes/news");
const articles = require("./routes/articles");
const products = require("./routes/products");
const categories = require("./routes/categories");
const careers = require("./routes/careers");
const contact = require("./routes/contact");
const checkout = require("./routes/checkout");
const cart = require("./routes/cart");
const resources = require("./routes/resources");
const footerColumns = require("./routes/footerColumns");
const menus = require("./routes/menus");
const store_locators = require("./routes/storeLocators");
const pages = require("./routes/pages");
const clientorder = require("./routes/manageOrder");
const industryfrontend = require("./routes/industry");
const casestudy = require("./routes/casestudy");
const archives = require("./routes/archives");
const userVideoTraining = require("./routes/videoTraining");
const userLiveTraining = require("./routes/liveTraining");
const faq = require("./routes/faq");
const cforms = require("./routes/cforms");
const zendeskFeeds=require("./routes/zendeskfeeds");
const distributorResources=require("./routes/distributorResources");
const integratorResources=require("./routes/integratorResources");
//const profile = require("./routes/uploadImages")
//use dependencies
const app = express();
app.use(compression());
app.use(cors());
app.use(express.json({
  limit: '50mb'
}));
dbconnect();
var publicDir = require("path").join(__dirname, "/mail-templates");
//default request method
app.get("/", (req, res) => res.send("API Haandle!"));

//api calls redirect to corresponding routes
app.use("/api/footerColumns", footerColumns);
app.use("/api/admin/manage/users", manageusers);
app.use("/api/admin", adminauth);
app.use("/api/admin/settings", settings);
app.use("/api/admin/upload", upload);
app.use("/api/admin/product-upload", productUpload);
app.use("/api/admin/product", manageproduct);
app.use("/api/admin/manage/pages", managepages);
app.use("/api/admin/orders", adminorders);
app.use("/api/admin/contacts", managecontacts);
app.use("/api/admin/shipping_settings", shipping_settings);
app.use("/api/admin/integrator", integrator);
app.use("/api/admin/coupons", coupons);
app.use("/api/admin/tax", manageTax);
app.use("/api/admin/storelocation", manageStore);
app.use("/api/admin/mediaFiles", mediafiles);
app.use("/api/admin/industry", industry);
app.use("/api/admin/casestudy", manageCasestudy);
app.use("/api/admin/faq", manageFaq);
app.use("/api/admin/livetraining", liveTraining);
app.use("/api/admin/videoTraining", videoTraining);
app.use("/api/admin/source", source);
app.use("/api/admin/category", careerCategory);
app.use("/api/admin/tab", ProfileTab);
app.use("/api/admin/redirection", Redirection);
app.use("/api/admin/ipredirection", IpRedirection);
app.use("/api/admin/attribute", Attribute);
app.use("/api/admin/cforms", cforms);
app.use("/api/admin/distributor-resources", DistributorResources);
app.use("/api/admin/distributor_category", DistributorCategory);
app.use("/api/admin/integrator-resources", IntegratorResources);
app.use("/api/admin/integrator_category", IntegratorCategory);


const sitemap = require("./routes/sitemap");
//app.use("/api/profile", profile);
app.use("/api/login", auth);
app.use("/api/news", news);
app.use("/api/articles", articles);
app.use("/api/products", products);
app.use("/api/careers", careers);
app.use("/api/contact", contact);
app.use("/api/checkout", checkout);
app.use("/api/cart", cart);
app.use("/api/categories", categories);
app.use("/api/menus", menus);
app.use("/api/resources", resources);
app.use("/api/storeLocators", store_locators);
app.use("/api/pages", pages);
app.use("/api/order", clientorder);
app.use("/api/industry", industryfrontend);
app.use("/api/casestudy", casestudy);
app.use("/api/archives", archives);
app.use("/api/videoTraining", userVideoTraining);
app.use("/api/liveTraining", userLiveTraining);
app.use("/api/faq", faq);
app.use("/api/sitemap", sitemap);
app.use("/api/zendeskFeeds", zendeskFeeds);
app.use("/api/distributorResources", distributorResources);
app.use("/api/integratorResources", integratorResources);


//setting port

var httpServer = http.createServer(app);
httpServer.listen(4000, function () {
  console.log("server is running on port :4000");
});