import { apiUrl } from "../../config.json";
import { FETCH_PRODUCTS, FETCH_PRODUCT_SINGLE, CHANGE_PRODUCT_QUANTITY } from '../type';

const axios = require("axios").default;


export const fetchProducts = (categories, callback) => dispatch => {

    return axios
    .post(apiUrl + "/products/getAllProducts", {
        categories: categories
    })
    .then(res => {
        
        if (!!callback) {
          callback();
        }
        return dispatch({
          type: FETCH_PRODUCTS,
          payload: res.data
        });
    })
    .catch(err => {
        
    });
};
export const fetchProductSingle = (slug, callback) => dispatch => {
  return axios
  .post(apiUrl + "/products/getSingleproduct", {
    slug: slug,
  })
  .then(response => {    

      let product = {
        _id: "",
        product_name: "",
        slug: "",
        short_description: "",
        description: "",
        specifications: "",
        price: {regular_price: "0", sales_price: "0"},
        images: {
          featured_image: {image_name:"",image_url:""}, 
          gallery_images: [], dimension_images: []
        },
        tabdata:[],
        image: "",
        gallery:[],
        dimensionImages:[],
        relatedProducts:[],
        cadfile_2D:"",
        cadfile_3D:"",
        xpaneldesigner:"",
        quantity: 1,
        inventory: {
          manage_stock:false,
          stock_availability:"",
          manage_inventory: {
            stock_quantity: 0,
            available_stock_quntity: 0,
            allow_backorders: "",
          }
        },
        status: 0
      
      };

      if (!!callback) {
        callback();
      }
      if (response.data.status !== 1) {
        callback();
      }
      if (response.data.status  === 1) {
        
        product.status = 1;
        if (response.data.singleDetails.product_name) {
          product.product_name = response.data.singleDetails.product_name.replace(/cimon/gi,"");
        }
        
        if (response.data.singleDetails.images) {
          product.images = response.data.singleDetails.images;
          if (response.data.singleDetails.images.featured_image) {
            product.image = response.data.singleDetails.images.featured_image.image_url;
          }
          if (response.data.singleDetails.images.gallery_images) {
            product.gallery = response.data.singleDetails.images.gallery_images;
          }
          if (response.data.singleDetails.images.dimension_images) {
            product.dimensionImages = response.data.singleDetails.images.dimension_images;            
          }
        }

        if (response.data.relatedProducts.length > 0) {
            product.relatedProducts = response.data.relatedProducts;
        }
        // if (response.data.singleDetails.tabdata) {
        //   product.tabdata = response.data.singleDetails.tabdata;
        // }

        if (response.data.singleDetails.downloads) {
          product.cadfile_2D = response.data.singleDetails.downloads.cadfile_2D
            ? response.data.singleDetails.downloads.cadfile_2D
            : "";

          product.cadfile_3D = response.data.singleDetails.downloads.cadfile_3D
            ? response.data.singleDetails.downloads.cadfile_3D
            : "";

          product.xpaneldesigner = response.data.singleDetails.downloads
            .xpaneldesigner
            ? response.data.singleDetails.downloads.xpaneldesigner
            : "";
        }
        product.quantity = 1;
        product._id = response.data.singleDetails._id;
        product.slug = response.data.singleDetails.slug;
        product.short_description = response.data.singleDetails.short_description;
        product.description = response.data.singleDetails.description;
        product.specifications =  response.data.singleDetails.specifications;
        product.price = response.data.singleDetails.price;
        product.tabdata = response.data.singleDetails.tabdata;
        product.inventory = response.data.singleDetails.inventory;
      } 
      return dispatch({
        type: FETCH_PRODUCT_SINGLE,
        payload: product
      });
  })
  .catch(err => {
      
  });
};

export const changeProductQuantity = (quantity, product, type) => {
  return (dispatch) => {
    let payload = {product: product, quantity: quantity, type: type}
    dispatch({
      type: CHANGE_PRODUCT_QUANTITY,
      payload: payload
    })
  }
}