import {
  LOAD_CART,
  UPDATE_CART,
  CLEAR_CART,
  SET_PROCESSING,
  SHOW_CART,
  APPLY_COUPON,
  REMOVE_COUPON
} from '../type';

import {
  CURRENCY_CODE,
  CURRENCY_SYMBOL,
  apiUrl
} from "../../config.json";
import util from '../util';
import * as ProductService from "../../ApiServices/products";
import * as CheckoutService from "../../ApiServices/checkout";
import alertify from "alertifyjs";
const axios = require("axios").default;
export const hideCart = showCartFlag => ( dispatch ) => {
  dispatch({
    type: SHOW_CART,
    payload: showCartFlag
  });
};

export const loadCart = (flag) => (dispatch, getState) => {
  const cartId = getState().cart.cartId;
  const coupon  = getState().cart.couponDetails ? getState().cart.couponDetails.couponcode : '';
  const token = getState().auth.token;
  /*Get cart products */
  ProductService.getCartProducts(cartId, token, coupon).then( async cartInfo => {

    if(cartInfo.status === 200 ) {
      const payload = {
        cartTotal: cartInfo.data.cartTotal,
        cartProducts: cartInfo.data.cartProducts,
        cartId: cartInfo.data.cartId,
        showCartFlag: flag,
        couponDetails: {
          couponcode: cartInfo.data.couponDetails.couponcode,
          discount: cartInfo.data.couponDetails.discount
        }
      }

      dispatch({
        type: LOAD_CART,
        payload: payload
      });    
    } else {
      dispatch({
        type: LOAD_CART,
        payload:  {
          cartTotal: {
            productQuantity: 0,
            totalPrice: 0,
            currencyFormat: CURRENCY_SYMBOL,
            currencyId: CURRENCY_CODE
          },
          cartProducts: [],
          cartId: '',
          couponDetails: {
            couponcode: '',
            discount: 0
          },
        }
      });   
      util.showMessage('load-cart', 'error');
    }
  });

}

/* Add product to cart */
export const addProduct = (product) => (dispatch, getState) => {

  dispatch({ type: SET_PROCESSING, payload: true });
  const cartId = getState().cart.cartId;
  const coupon  = getState().cart.couponDetails.couponcode;
  const token = getState().auth.token;
  /*Get cart products */
  ProductService.getCartProducts(cartId, token, coupon).then( async cartInfo => {
    if(cartInfo.status === 200 ) {
      const cartProducts = cartInfo.data.cartProducts;
      let productAlreadyInCart = false;
      let quantity = product.quantity;
      cartProducts.forEach((cp) => {
        if (cp._id === product._id) {
          quantity = parseInt(quantity)+parseInt(cp.quantity);
        }
      });
      const checkProductAvailable = await ProductService.checkAddToCart(product._id,quantity);
      if ( checkProductAvailable.data.status && checkProductAvailable.status === 200 ) {
          cartProducts.forEach((cp) => {
            if (cp._id === product._id) {
              cp.quantity = parseInt(cp.quantity) + parseInt(product.quantity);
              productAlreadyInCart = true;
            }
          });
          if (!productAlreadyInCart) {
            let productDetails = {
              _id: product._id,
              product_name: product.product_name,
              images: product.images,
              inventory: product.inventory,
              quantity: product.quantity,
              price: product.price,
              slug: product.slug,
              url: product.url,
            };
            cartProducts.push(productDetails);
          }
          dispatch(updateCart(cartProducts, cartId, 'add-to-cart-single')); 
                    
      } else {
        util.showMessage('add-to-cart-single-quantity', 'error');        
        dispatch({ type: SET_PROCESSING, payload: false });
      }
    } else {
      util.showMessage('add-to-cart-single', 'error');
      dispatch({ type: SET_PROCESSING, payload: false });
    }
  });
};

/* Add multiple products to cart */
export const multipleAddProduct = (products) => (dispatch, getState) => {
  dispatch({ type: SET_PROCESSING, payload: true });
  const cartId = getState().cart.cartId;
  const coupon  = getState().cart.couponDetails.couponcode;
  const token = getState().auth.token;
  /*Get cart products */
  ProductService.getCartProducts(cartId, token, coupon).then( async cartInfo => {
    if(cartInfo.status === 200 ) {
      const cartProducts = cartInfo.data.cartProducts;
      let selectedProducts = JSON.parse(JSON.stringify(products));
      selectedProducts.forEach( (product) => {       
        cartProducts.forEach((cp) => {
            if (cp._id === product._id) {
              product.quantity = parseInt(product.quantity)+parseInt(cp.quantity);
            }
        });
      });
      const checkProductAvailable = await ProductService.checkAddToCartMultiple(selectedProducts);
      if (checkProductAvailable.data.status &&checkProductAvailable.status === 200) {
        selectedProducts.forEach( (product) => {
          let productAlreadyInCart = false;
          cartProducts.forEach((cp) => {
            if (cp._id === product._id) {
              cp.quantity = product.quantity;
              productAlreadyInCart = true;
            }
          });
          if (!productAlreadyInCart) {
            let productDetails = {
              _id: product._id,
              product_name: product.product_name,
              images: product.images,
              inventory: product.inventory,
              quantity: product.quantity,
              price: product.price,
              slug: product.slug,
              url: product.url,
            };
            cartProducts.push(productDetails);
          }
        });
        dispatch(updateCart(cartProducts, cartId, 'add-to-cart-multiple')); 
      } else {
        util.showMessage('add-to-cart-single-quantity', 'error');        
        dispatch({ type: SET_PROCESSING, payload: false });
      }
    }
    else {
      util.showMessage('add-to-cart-multiple', 'error');
      dispatch({ type: SET_PROCESSING, payload: false });
    }

  });
};

/*Remove product from cart */
export const removeProduct = (product) => (dispatch, getState) => {
  dispatch({ type: SET_PROCESSING, payload: true });
  const cartId = getState().cart.cartId;
  const coupon  = getState().cart.couponDetails.couponcode;
  const token = getState().auth.token;
  /*Get cart products */
  ProductService.getCartProducts(cartId, token, coupon).then( async cartInfo => {
    if(cartInfo.status === 200 ) {
      const cartProducts = cartInfo.data.cartProducts;
      if(cartProducts.length > 0 ) {
        const index = cartProducts.findIndex((p) => p._id === product._id);
        if (index >= 0) {
          cartProducts.splice(index, 1);
          dispatch(updateCart(cartProducts, cartId, 'remove-cart-item'));
        }  else {
          dispatch({ type: SET_PROCESSING, payload: false });
        }
      } else {
        dispatch(updateCart(cartProducts, cartId, 'remove-cart-item'));
      }
    } else {
      util.showMessage('remove-cart-item', 'error');
      dispatch({ type: SET_PROCESSING, payload: false });
    }
  });
};

/* Clear cart */
export const clearCart = () => (dispatch, getState) => {
  dispatch({
    type: CLEAR_CART,
    payload:  {
      cartTotal: {
        productQuantity: 0,
        totalPrice: 0,
        currencyFormat: CURRENCY_SYMBOL,
        currencyId: CURRENCY_CODE
      },
      cartProducts: [],
      cartId: '',
    }
  });
  
};

/* Cart action processing state change */
export const setProcessing = (flag) => dispatch => {
  dispatch({
    type: SET_PROCESSING,
    payload: flag
  });
};

/* Change cart products quantity */
export const changeQuantity = (product, type ) => (dispatch, getState) => {
  dispatch({ type: SET_PROCESSING, payload: true });
  const cartId = getState().cart.cartId;
  const coupon  = getState().cart.couponDetails.couponcode;
  const token = getState().auth.token;

  ProductService.getCartProducts(cartId, token, coupon).then( async cartInfo => {
    if(cartInfo.status === 200 ) {
      const cartProducts = cartInfo.data.cartProducts;
      let productAlreadyInCart = false;
      let quantity = 1;
      cartProducts.forEach((cp) => {
        if (cp._id === product._id) {
          quantity =
            type === "increase"
              ? parseInt(cp.quantity) + 1
              : parseInt(cp.quantity) === 0
              ? 0
              : parseInt(cp.quantity) - 1;
        }
      });
      const checkProductAvailable = await ProductService.checkAddToCart(product._id,quantity);
      if (checkProductAvailable.data.status && checkProductAvailable.status === 200 ) {
        let cartProductQuantity = 0;
        cartProducts.forEach((cp) => {
          if (cp._id === product._id) {
            cartProductQuantity = cp.quantity =
              type === "increase"
                ? parseInt(cp.quantity) + 1
                : parseInt(cp.quantity) === 0
                ? 0
                : parseInt(cp.quantity) - 1;
            productAlreadyInCart = true;
          }
        });

        if (!productAlreadyInCart && cartProductQuantity > 0) {
          let productDetails = {
            _id: product._id,
            product_name: product.product_name,
            images: product.images,
            inventory: product.inventory,
            quantity: product.quantity,
            price: product.price,
            slug: product.slug,
            url: product.url,
          };
          cartProducts.push(productDetails);
        }

        if (cartProductQuantity <= 0) {
          const index = cartProducts.findIndex((p) => p._id === product._id);
          if (index >= 0) {
            cartProducts.splice(index, 1);
          }
        }

        dispatch(updateCart(cartProducts, cartId, type));

      } else {
        util.showMessage('add-to-cart-single-quantity', 'error');        
        dispatch({ type: SET_PROCESSING, payload: false });
      }
    } else {
      util.showMessage(type, 'error');
      dispatch({ type: SET_PROCESSING, payload: false });
    }
  });

};

/* Update cart */
export const updateCart = (cartProducts, cartId, type) => (dispatch, getState) => {

  const authUser = getState().auth.authUser;
  const userId = authUser._id;
  const token = getState().auth.token;
  const coupon  = getState().cart.couponDetails.couponcode;
  let productQuantity = cartProducts.reduce((sum, p) => {
    sum += parseInt(p.quantity);
    return sum;
  }, 0);
  let totalPrice = cartProducts.reduce((sum, p) => {
    let price = util.getPrice(p.price);
    sum += price * p.quantity;
    return sum;
  }, 0);
  let cartTotal = {
    productQuantity,
    totalPrice,
    currencyId: CURRENCY_CODE,
    currencyFormat: CURRENCY_SYMBOL,
    
  };
  return axios
  .post(apiUrl + "/cart/saveCart", {
    cartTotal: cartTotal,
    cartProducts: cartProducts,
    cartId: cartId ? cartId : '',
    userId: userId,
    token: token,
    coupon_code: coupon
  })
  .then(response => {    
      if(response.status === 200) {
        const cartData = {
          cartTotal: response.data.cartTotal,
          cartProducts: response.data.cartProducts,
          cartId: response.data.cartId,
          couponDetails: {
            couponcode: response.data.couponDetails.couponcode,
            discount: response.data.couponDetails.discount
          }
        }
        util.showMessage(type, 'success');
        dispatch({
          type: UPDATE_CART,
          payload: cartData,
        });
      } else {
        util.showMessage(type, 'error');
        dispatch({ type: SET_PROCESSING, payload: false });
      }
  })
  .catch(err => {
    util.showMessage(type, 'error');
    dispatch({ type: SET_PROCESSING, payload: false });

  }); 
};

/* Apply coupon to cart */
export const applyCoupon = (data) => (dispatch, getState) => {
  dispatch({ type: SET_PROCESSING, payload: true });
  const cartId = getState().cart.cartId;
  const token = getState().auth.token;
  CheckoutService.applyCoupon(data.coupon_code,cartId, token).then( async couponInfo => {
    if(couponInfo.status === 200 ) {
      if(couponInfo.data.status === 1) {
        dispatch({
          type: APPLY_COUPON,
          payload: couponInfo.data.coupon
        })
      } else  {
        alertify.set("notifier", "position", "top-right");
        alertify.error(couponInfo.data.message);
      }
    } 
    dispatch({ type: SET_PROCESSING, payload: false });
  });
}

/* Remove coupon from cart */
export const removeCoupon = () => (dispatch, getState) => {
    dispatch({ type: REMOVE_COUPON, payload: '' });
}
