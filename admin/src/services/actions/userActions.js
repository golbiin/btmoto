import { AUTH_USER, SIGN_OUT, LOGIN_PROCESSING, LOGIN_FAILURE, SET_CARTID, DISPLAY_LOGIN_POPUP, CHANGE_TAB,
     DISPLAY_EDIT_PROFILE, PROFILE_PROCESSING, HIDE_EDIT_PROFILE } from '../type';
import * as authServices from "../../ApiServices/login";
import {clearCart, loadCart} from './cartActions';
import alertify from "alertifyjs";

export const showHideLogin = (flag, activeTab) => ( dispatch ) => {

    dispatch({
      type: DISPLAY_LOGIN_POPUP,
      payload: { status: flag, activeTab: activeTab}
    });
    
};

export const hideEditProfile = (flag) => ( dispatch ) => {
    dispatch({
        type: HIDE_EDIT_PROFILE,
        payload:flag
    });
}

export const displayEditProfile = (flag) => ( dispatch ) => {

    dispatch({
        type: PROFILE_PROCESSING,
        payload:true
    });
    const response =  authServices.getCurrentUser();  

    if(response){
      if(response.email){
          authServices.getUserDetails(response.email)
          .then( async response1 => {
            if(response1){                                
              if(response1.data.data.hasOwnProperty('email')){

                
                    dispatch({
                        type: DISPLAY_EDIT_PROFILE,
                        payload: { 
                            profile: response1.data.data,
                            show: flag
                        }
                    });
                    dispatch({
                        type: PROFILE_PROCESSING,
                        payload:false
                    });
                    
              }
            } else{
                dispatch({
                    type: DISPLAY_EDIT_PROFILE,
                    payload: { 
                        profile: {
                            _id: '',
                            first_name: '',
                            last_name: '',
                            company_name: '',
                            city: '',
                            zipcode: '',
                            phone_number: '',
                            country: ''
                        },
                        show: flag
                    }
                });
                dispatch({
                    type: PROFILE_PROCESSING,
                    payload:false
                });

            }
          });
      }
    }    
};



export const changeTab = (tab) => ( dispatch ) => {

    dispatch({
      type: CHANGE_TAB,
      payload: tab
    });
    
};

export const login = ( data, remember ) => (dispatch, getState) => {

    dispatch({
        type: LOGIN_PROCESSING,
        payload: true
    })
    try {

        const cartId = getState().cart.cartId;
        data.cartId = cartId;
        authServices.authenticate(data)
        .then( async response => {

            if (response.data.status === 0) {
                dispatch({
                    type: LOGIN_FAILURE,
                    payload: {
                        message: response.data.message,
                        message_type: "error",
                        submit_status: false,
                        verification: true,
                    }
                })
            } else {
                alertify.set("notifier", "position", "top-right");
                alertify.success("You are now logged in.");
                const token = response.data.data["token"];
                const result = await authServices.loginWithJwt(
                    token,
                    remember
                );
                const user = response.data.data["user"];

                const payload = {
                    authUser: user,
                    token : token
                }

                dispatch({
                    type: SET_CARTID,
                    payload: response.data.data.cartId
                })
                dispatch({
                    type: AUTH_USER,
                    payload: payload
                });  
                dispatch(loadCart(false));
                                       
            }

        }).catch( err => {
            dispatch({
                type: LOGIN_PROCESSING,
                payload: false
            })
        })

      } catch (err) {
        dispatch({
            type: LOGIN_PROCESSING,
            payload: false
        })
      }

}
export const getCurrentUser = () => (dispatch, getState) => {

    const response =  authServices.getCurrentUser();   
    
    if(response){   
        if(response.email){
            authServices.getUserDetails(response.email)
            .then( async response1 => {
               
                if(response1.data.status === 1){    
                    
                    if(response1.data.data.hasOwnProperty('email')){
                        const token = authServices.getUserToken(); 
                        const payload = {
                            authUser: response1.data.data,
                            token : token
                        }
                        dispatch({
                            type: AUTH_USER,
                            payload: payload
                        });   
                    }
                    else{
                       
                        dispatch(
                         
                            signOut()
                            
                            );
                    }
                   
                }
                else
                {
                    
                    dispatch(signOut());
                }



            }).catch( err => {
                dispatch({
                    type: LOGIN_FAILURE,
                    payload: {
                        message: '',
                        message_type: "error",
                        submit_status: false,
                        verification: false,
                    },
                
                }); 
            })
        }

    } else {
        dispatch({
            type: LOGIN_FAILURE,
            payload: {
                message: '',
                message_type: "error",
                submit_status: false,
                verification: false,
            }
        }); 
    }
}
export const signOut = () => (dispatch, getState) => {

    authServices.logout();
    
    dispatch({
        type: SIGN_OUT,
        payload:''
    });
    dispatch(clearCart());
    
}




