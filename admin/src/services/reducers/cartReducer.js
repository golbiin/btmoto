import { LOAD_CART, 
  SET_PROCESSING, 
  UPDATE_CART, 
  CLEAR_CART, 
  SHOW_CART,
  SET_CARTID, 
  APPLY_COUPON,
  REMOVE_COUPON
} from '../type';


const initialState = {
  cartId: '',
  products: [],
  couponDetails: {
    couponcode: '',
    discount: 0
  },
  data: {
    productQuantity: 0,
    totalPrice: 0,
  },
  showCartFlag: false,
  isCartProcessing: false,
  shipping: {
    zipcode: '', 
    city: '',
    region: '',
    country: '',
    address: ''
  },
  shipping_method: '',
  tax: ''

};

export default function(state = initialState, action) {
 
  switch (action.type) {
    case SET_PROCESSING:
      return {
        ...state,
        isCartProcessing: action.payload,
        products: state.products,
        cartId: state.cartId,
        data: state.data,
        couponDetails:state.couponDetails
      };
      break;
    case SET_CARTID:
 
      return {
        ...state,
        cartId: action.payload,
      };
      break;
    case LOAD_CART:
      
      return {
        ...state,
        products: action.payload.cartProducts,
        cartId: action.payload.cartId,
        data: action.payload.cartTotal,
        showCartFlag: action.payload.showCartFlag,
        couponDetails: action.payload.couponDetails
      };
    
      break;
    case UPDATE_CART:
      return {
        ...state,
        isCartProcessing: false,
        cartId: action.payload.cartId,
        data: action.payload.cartTotal,
        products: action.payload.cartProducts,
        couponDetails: action.payload.couponDetails
      };
      break;
    case SHOW_CART:
      return {
        ...state,
        showCartFlag: action.payload
      };
      break;
    case CLEAR_CART: 
      return {
        ...state,
        products: action.payload.cartProducts,
        cartId: action.payload.cartId,
        data: action.payload.cartTotal,
        couponDetails: {
          couponcode: '',
          discount: 0
        },
      };
      break;
    case APPLY_COUPON:
      return {
        ...state,
        isCartProcessing: false,
        couponDetails: action.payload
      };
      break;
    case REMOVE_COUPON:
        return {
          ...state,
          isCartProcessing: false,
          couponDetails: {
            couponcode: '',
            discount: 0
          },
        };
        break;    
    default:
      return state;
  }
}
