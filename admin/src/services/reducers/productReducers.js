import { FETCH_PRODUCTS, FETCH_PRODUCT_SINGLE, CHANGE_PRODUCT_QUANTITY } from '../type';

const initialState = {
  product: {
    status: 0,
    _id: "",
    product_name: "",
    slug: "",
    short_description: "",
    description: "",
    specifications: "",
    price: {regular_price: "0.00", sales_price: "0.00"},
    images: {
      featured_image: {image_name:"",image_url:""}, 
      gallery_images: [], dimension_images: []
    },
    image: "",
    gallery:[],
    dimensionImages:[],
    relatedProducts: [],
    cadfile_2D:"",
    cadfile_3D:"",
    xpaneldesigner:"",
    quantity: 1,
    inventory: {
      manage_stock:false,
      stock_availability:"",
      manage_inventory: {
        stock_quantity: 0,
        available_stock_quntity: 0,
        allow_backorders: "",
      }
    },
  },
  products: [],
  categoryDetails:{},
  subCategories:[]
};

export default function(state = initialState, action) {
  
  
  switch (action.type) {
    case FETCH_PRODUCTS:
      
      return {
        ...state,
        products: action.payload.productDetails,
        categoryDetails:action.payload.categoryInfo,
        subCategories: action.payload.subCategories
      };
      break;
    case FETCH_PRODUCT_SINGLE:      
        return {
          ...state,
          product: action.payload,
        };
        break;
    case CHANGE_PRODUCT_QUANTITY:

        let count =  action.payload.quantity;
        let type = action.payload.type;
        if (count <= 0) {
          count  = 0;
        }  

        if(type === "plp" || type === "plp-multiple") {
          return {
            ...state,
            products: state.products.map((product, index) => {
              if (product._id === action.payload.product._id) {
                return Object.assign({}, product, {
                  quantity: count
                })
              }
              return product
            }),
            categoryDetails : { ...state.categoryDetails },
            subCategories : [ ...state.subCategories ]
          }
        } else {
          return {
            ...state,
            product: Object.assign({}, state.product, {
              quantity: count
            })
          }
        }
        break;
    default:
      return state;
  }
}
