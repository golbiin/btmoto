import { AUTH_USER, SIGN_OUT, LOGIN_PROCESSING, LOGIN_FAILURE, DISPLAY_LOGIN_POPUP, CHANGE_TAB, DISPLAY_EDIT_PROFILE, 
    SET_PROFILE, PROFILE_PROCESSING, HIDE_EDIT_PROFILE  } from '../type';
  
    const initialState = {
        token: '',
        authUser: {
            first_name: '',
            last_name: '',
            email: '',
            id: '',
            company_name: '',
            country: '',
            phone_number: '',
            city: '',
            zipcode: ''
        } ,
        loginProcessing: false,
        loginFailure: {
            message: '',
            message_type: "error",
            submit_status: false,
            verification: false,
        },
        displayLoginPopup: false,
        activeTab: 'login',
        displayEditProfileStatus: false,
        profile: {
            _id: '',
            first_name: '',
            last_name: '',
            company_name: '',
            city: '',
            zipcode: '',
            phone_number: '',
            country: ''
        },
        profileProcessing: false,
    };
  
    export default function(state = initialState, action) {

        
        const loginProcessing = false;
        const loginFailure =    {
                                    message: '',
                                    message_type: "error",
                                    submit_status: false,
                                    verification: false,
                                }
        const authUser = {
            first_name: '',
            last_name: '',
            email: '',
            _id: '',
            company_name: '',
            country: '',
            phone_number: '',
            city: '',
            zipcode: ''
        } 
        switch (action.type) {
        case DISPLAY_LOGIN_POPUP:
            return {
                ...state,
                displayLoginPopup: action.payload.status,
                activeTab:action.payload.activeTab
            };
            break;
        case DISPLAY_EDIT_PROFILE:
            return {
                ...state,
                displayEditProfileStatus: action.payload.show,
                profile: action.payload.profile
            };
            break; 
        case HIDE_EDIT_PROFILE:  
            return {
                ...state,
                displayEditProfileStatus: action.payload,
            }; 
            break;
        case SET_PROFILE:
            return {
                ...state,
                profile: action.payload.profile
            };  
            break;
        case PROFILE_PROCESSING:
            return {
                ...state,
                profileProcessing: action.payload,
            };
            break;
        case CHANGE_TAB:
            return {
                ...state,
                activeTab:action.payload
            };
            break;
        case LOGIN_PROCESSING:
            return {
                ...state,
                loginProcessing: action.payload,
                loginFailure: loginFailure
            };
            break;
        case LOGIN_FAILURE:
            return {
                ...state,
                loginProcessing: loginProcessing,
                loginFailure: action.payload
            };
            break;
        case AUTH_USER:
            return {
            ...state,
            authUser: action.payload.authUser,
            token: action.payload.token,
            loginProcessing: loginProcessing,
            loginFailure: loginFailure

            };
            break;
        case SIGN_OUT:
            return {
                ...state,
                authUser: authUser ,
                token: ''
            };
            break;
        default:
            return state;
        }
    }
    