import { combineReducers } from 'redux';
import { withReduxStateSync } from 'redux-state-sync';
import productsState from './reducers/productReducers';
import cart from './reducers/cartReducer';
import auth from './reducers/userReducer';

const rootReducer = combineReducers({
    auth: auth,
    productsState: productsState,
    cart: cart,  
});

export default withReduxStateSync(rootReducer);