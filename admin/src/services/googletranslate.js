import $ from "jquery";

const GtranslateElement = () => {
  // Change font family and color
  $("iframe")
    .contents()
    .find(
      ".goog-te-menu2-item div, .goog-te-menu2-item:link div, .goog-te-menu2-item:visited div, .goog-te-menu2-item:active div"
    ) //, .goog-te-menu2 *
    .css({
      color: "#fff",
      "background-color": "#262b33",
    });

  $("iframe")
    .contents()
    .find(".goog-te-menu2-item-selected")
    .css("display", "none");
  // Change hover effects
  $("iframe")
    .contents()
    .find(".goog-te-menu2-item div")
    .hover(
      function () {
        $(this)
          .css("background-color", "#262b33")
          .find("span.text")
          .css("color", "#fff");
      },
      function () {
        $(this)
          .css("background-color", "#262b33")
          .find("span.text")
          .css("color", "#fff");
      }
    );

  // Change Google's default blue border
  $("iframe")
    .contents()
    .find(".goog-te-menu2")
    .css("border", "1px solid #17548d");

  $("iframe")
    .contents()
    .find(".goog-te-menu2")
    .css("background-color", "#262b33");

  // Change the iframe's box shadow
  $(".goog-te-menu-frame").css({
    "box-shadow": "none",
    width: "112px",
  });
  // Change menu's padding
  $("iframe")
    .contents()
    .find(".goog-te-menu2-item-selected")
    .css("display", "none");

  // Change menu's padding
  $("iframe").contents().find(".goog-te-menu2").css({
    padding: "0px",
    width: "112px",
  });

  // Change the padding of the languages
  $("iframe").contents().find(".goog-te-menu2-item div").css({
    padding: "9px",
    color: "#fff",
    "font-size": "11.95px!important",
    cursor: "pointer!important",
    "font-family": "Arial Bold",
  });

  // Change the width of the languages
  $("iframe").contents().find(".goog-te-menu2-item").css("width", "100%");

  $("iframe").contents().find("td").css("width", "100%");

  $("iframe")
    .contents()
    .find(".goog-te-menu2-item div")
    .hover(
      function () {
        $(this).find("span.text").css("color", "#ff2a0c");
      },
      function () {
        $(this).find("span.text").css("color", "#fff");
      }
    );
    $("iframe")
    .contents()
    .find("a.goog-te-menu-value")
    .hover(
      function () {
        
        $(".goog-te-menu-frame").css({
         
          'display': "block",
        });
      },
    
    );
  
  // Change Google's default blue border
  $("iframe").contents().find(".goog-te-menu2").css("border", "none");

  $("body").css("top", "0px!important");

};

export default {
  GtranslateElement,
};