import alertify from "alertifyjs";
const formatPrice = (x, currency) => {
    switch (currency) {
      case 'BRL':
        return x ? x.toFixed(2).replace('.', ','): 0.00;
      default:
        return x ? x.toFixed(2): 0.00;
    }
};
const getPrice = (price) => {
  if(price.sales_price && price.sales_price !== '' && price.sales_price !== null && parseFloat(price.sales_price) > 0 ){
    return price.sales_price > 0 ? parseFloat(price.sales_price) : 0.00;
  } else{
    return price.regular_price > 0 ? parseFloat(price.regular_price) : 0.00;
  }
};
const productAvailable = (product) => {

  if ( (
    product.inventory.manage_stock === false && 
    product.inventory.stock_availability === "In Stock"
    ) ||
    (
      (product.inventory.manage_stock == true && 
      parseInt(product.inventory.manage_inventory.stock_quantity) > 0 ) ||
      ( product.inventory.manage_stock == true && product.inventory.manage_inventory.allow_backorders === "allow") ||
      ( product.inventory.manage_stock == true && product.inventory.manage_inventory.allow_backorders === "allow_but_notify") 
    )
  ) {
        return true;
    } 
    return false;
}
const showMessage = (type, msg_type) => {

  var msg = '';
  switch(type) {
    case 'add-to-cart-single': 
      msg = ( msg_type === 'success' )? 'Product added to cart successfully' :"Sorry! failed to add product to cart";
      break;
    case 'add-to-cart-multiple':
      msg = ( msg_type === 'success' )? 'Product added to cart successfully' :"Sorry! failed to add product to cart";
      break;
    case 'add-to-cart-single-quantity':      
      msg = 'Sorry! The requested quantity of product is not available';
      break;
    case 'remove-cart-item':
      msg = ( msg_type === 'success' )? 'Product removed from cart successfully' :"Sorry! failed to remove product from cart";
      break;
    case 'increase':
        msg = ( msg_type === 'success' )? 'Product quantity changed successfully' :"Sorry! Failed to update product quantity";
        break;
    case 'decrease':
      msg = ( msg_type === 'success' )? 'Product quantity changed successfully' :"Sorry! Failed to update product quantity";
      break;
    case 'load-cart':      
      msg = 'Sorry! failed to fetch cart products';
      break
    default:
      msg = '';

  }
  alertify.set("notifier", "position", "top-right");
  if(msg_type === 'success') {
      alertify.success(msg);
  } else{
    alertify.error(msg);
  }
};

  
  export default {
    formatPrice,
    getPrice,
    productAvailable,
    showMessage
  };
  