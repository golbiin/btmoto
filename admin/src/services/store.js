import { compose, createStore, applyMiddleware } from 'redux';
import { createStateSyncMiddleware, initStateWithPrevTab  } from 'redux-state-sync';
import thunkMiddleware from 'redux-thunk'
import rootReducer from './reducers';
import {
  SET_CARTID,
  ADD_PRODUCT,
  MULTIPLE_ADD_PRODUCT,
  UPDATE_CART,
  CLEAR_CART,
  REMOVE_PRODUCT,
  SIGN_OUT,
  AUTH_USER,
  LOAD_CART,
  APPLY_COUPON,
  REMOVE_COUPON
} from './type';

export default initialState => {
  initialState = 
    JSON.parse(window.localStorage.getItem('state')) || initialState;

  const config = {
    // Only below actions will be triggered in other tabs
    whitelist: [ADD_PRODUCT,
                MULTIPLE_ADD_PRODUCT,
                UPDATE_CART,
                CLEAR_CART,
                REMOVE_PRODUCT,
                SET_CARTID,
                SIGN_OUT,
                AUTH_USER,
                LOAD_CART,
                APPLY_COUPON,
                REMOVE_COUPON
              ],
  };
  const syncMiddleWare = createStateSyncMiddleware(config);
  
  const middlewares = [thunkMiddleware, syncMiddleWare ];

  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middlewares)
      /* window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__() */
    )
  );

  store.subscribe(() => {
    
    const state = store.getState();
  
    const persist = {
      cart: state.cart,
      auth: state.auth
    };

    window.localStorage.setItem('state', JSON.stringify(persist));
  });

  initStateWithPrevTab(store);

  return store;
};
