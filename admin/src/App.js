import React, {Component ,Suspense, lazy} from "react";
import {Route,Switch,BrowserRouter,Redirect} from "react-router-dom";
import {Provider} from 'react-redux';  
import store from './services/store';
import ScrollTop from './services/scrollTop';


/* admin */

const AdminDashboard = lazy(() => import('./admin/adminDashboard'));
const ManageUsers = lazy(() => import('./admin/manageUsers'));
const Adminlogin = lazy(() => import('./admin/adminLogin'));
const AdminForgot = lazy(() => import('./admin/adminForgot'));
const AdminPassword= lazy(() => import('./admin/adminPassword'));
const ProtectedRoutea = lazy(() => import('./admin/adminProtectedRoute'));
const EditUser = lazy(() => import('./admin/users/editUser'));
const AdminPages = lazy(() => import('./admin/cms/pages/adminPages'));
const AdminAddPage = lazy(() => import('./admin/cms/pages/AdminAddPage'));
const AdminEditPage = lazy(() => import('./admin/cms/pages/adminEditPage'));
const AdminProducts = lazy(() => import('./admin/products/adminProducts'));
const AdminAddFaq = lazy(() => import('./admin/cms/AdminAddFaq'));
const AdminEditFaq = lazy(() => import('./admin/cms/AdminEditFaq'));
const AdminFaqPage = lazy(() => import('./admin/cms/AdminFaqPage'));
const Orders = lazy(() => import('./admin/orders/orders'));
const AdminOrderDetails = lazy(() => import('./admin/orders/AdminOrderDetails'));
const UsersList= lazy(() => import('./admin/users/usersList'));
const UserAdd = lazy(() => import('./admin/users/userAdd'));
const UserProfile = lazy(() => import('./admin/users/userProfile'));
const ManageCategories = lazy(() => import('./admin/categories/manageCategories'));
const EditCateogry = lazy(() => import('./admin/categories/editCateogry'));
const PageBuilder = lazy(() => import('./admin/cms/basic/pages/landing'));
const PageBuilderBasic = lazy(() => import('./admin/cms/basic/pages/index'));
// const PageBuilderLanding = lazy(() => import('./admin/cms/landing/pages/index'));
const ScadabundleEditPage = lazy(() => import('./admin/cms/pages/scadabundlesEditPage'));
const HomeEditPage = lazy(() => import('./admin/cms/pages/homeEditPage'));
const AboutEditPage = lazy(() => import('./admin/cms/pages/aboutEditPage'));
const ScadaEditPage = lazy(() => import('./admin/cms/pages/scadaEditPage'));
const CimonCiconEditPage = lazy(() => import('./admin/cms/pages/cimonCiconEditPage'));
const IpcEditPage = lazy(() => import('./admin/cms/pages/ipcEditPage'));
const HybridHmiEditPage = lazy(() => import('./admin/cms/pages/hybridhmiEditPage'));
const PlcEditPage = lazy(() => import('./admin/cms/pages/plcEditPage'));
const HmiEditPage = lazy(() => import('./admin/cms/pages/hmiEditPage'));
const CareerEditPage = lazy(() => import('./admin/cms/pages/careerEditPage'));
const NewsEditPage = lazy(() => import('./admin/cms/pages/newsEditPage'));
const ContactEditPage = lazy(() => import('./admin/cms/pages/contactEditPage'));
const CareerCenterEditPage = lazy(() => import('./admin/cms/pages/careerCenterEditPage'));
const FooterEditPage  = lazy(() => import('./admin/cms/pages/footerEditPage'));
const DistributorEditPage =  lazy(() => import('./admin/cms/pages/distributorEditPage'));
const IntegratorPortalEditPage   =  lazy(() => import('./admin/cms/pages/integratorPortalEditPage'));
const DistributorLocatorEditPage  =  lazy(() => import('./admin/cms/pages/distributorLocatorEditPage'));

const DownloadEditPage = lazy(() => import('./admin/cms/pages/downloadEditPage'));
const ProductSplash = lazy(() => import('./admin/cms/pages/productSplash'));
const InternationalDistributorEditPage = lazy(() => import('./admin/cms/pages/internationalDistributorEditPage'));
const TechSupportEditPage = lazy(() => import('./admin/cms/pages/techSupportEditPage'));
const OurCompanyEditPage = lazy(() => import('./admin/cms/pages/ourCompanyEditPage'));
const OurCustomersEditPage = lazy(() => import('./admin/cms/pages/ourCustomersEditPage'));
const SIProgramEditPage = lazy(() => import('./admin/cms/pages/siProgramEditPage'));

const IndustryEditPage = lazy(() => import('./admin/cms/pages/industryEditPage'));
const CaseStudyEditPage = lazy(() => import('./admin/cms/pages/caseStudyEditPage'));
const LiveTrainingEditPage = lazy(() => import('./admin/cms/pages/liveTrainingEditPage'));
const VideoTutorialEditPage = lazy(() => import('./admin/cms/pages/videoTutorialEditPage'));
const ArchivesEditPage = lazy(() => import('./admin/cms/pages/archivesEditPage'));
const FaqEditPage = lazy(() => import('./admin/cms/pages/faqEditPage'));
const CertificatesEdit = lazy(() => import('./admin/cms/pages/certificatesEditPage'));

const AdminManageAddress = lazy(() => import('./admin/contacts/manageAddress')); 
const AdminListNews = lazy(() => import('./admin/news/adminListNews'));
const AdminAddNews = lazy(() => import('./admin/news/adminAddNews'));
const AdminEditNews = lazy(() => import('./admin/news/adminEditNews'));
const AdminListCareers = lazy(() => import('./admin/careers/adminListCareers'));
const AdminAddCareer = lazy(() => import('./admin/careers/adminAddCareer'));
const AdminEditCareer = lazy(() => import('./admin/careers/adminEditCareer'));
const AdminManageSubscribers = lazy(() => import('./admin/contacts/manageSubscribers'));
const AdminProductAdd = lazy(() => import('./admin/products/adminAddproducts'));
const AdminProductEdit = lazy(() => import('./admin/products/adminEditproducts'));
const AdminMenus = lazy(() => import('./admin/settings/manageMenus'));

const ManageProfile = lazy(() => import('./admin/settings/manageProfile'));

const ManageSlider = lazy(() => import('./admin/settings/manageSlider'));

const StripeSetting = lazy(() => import('./admin/settings/stripeSettings'));
const FacebookSetting = lazy(() => import('./admin/settings/facebookSetting'));
const LinkedinSetting = lazy(() => import('./admin/settings/linkedinSetting'));
const AdminFooter = lazy(() => import('./admin/footer/manageFooter'));
const AdminUsersList = lazy(() => import('./admin/users/adminList'));
const AdminsAdd = lazy(() => import('./admin/users/adminAdd'));
const AdminEdit = lazy(() => import('./admin/users/editAdmin'));
const ShippingOrigin = lazy(() => import('./admin/shipping/shippingOrigin'));
const IntegratorResources = lazy(() => import('./admin/integrator/addintegratorResources'));
const EditIntegratorResources = lazy(() => import('./admin/integrator/editintegratorResources'));
const ListIntegratorResources = lazy(() => import('./admin/integrator/listintegratorResources'));
const AdminCoupons = lazy(() => import('./admin/coupons/manageCoupons'));
const GeneralTax = lazy(() => import('./admin/shipping/generalTax'));
const StoreLocator = lazy(() => import('./admin/storeLocator/storeLocator'));
const StoreLocatorEdit =   lazy(() => import('./admin/storeLocator/storeLocatorEdit')); 
const StoreLocatorAdd =   lazy(() => import('./admin/storeLocator/storeLocatorAdd'));
const ListArchives = lazy(() => import('./admin/archives/listArchives'));
const AddArchives = lazy(() => import('./admin/archives/addArchives'));
const EditArchives = lazy(() => import('./admin/archives/editArchives'));
const Language = lazy(() => import('./admin/language/language'));
const EditLanguage = lazy(() => import('./admin/language/editLanguage'));
const PreviewPage = lazy(() => import('./admin/preview/preview'));
const MediaUpload = lazy(() => import('./admin/media/mediaUpload'));
const MediaAdd= lazy(() => import('./admin/media/newMedia'));
const BrowseMedia = lazy(() => import('./admin/media/browseMedia'));
const AddIndustry= lazy(() => import('./admin/industry/adminAddIndustry'));
const EditIndustry= lazy(() => import('./admin/industry/adminEditIndustry'));
const ListIndustry= lazy(() => import('./admin/industry/adminListIndustry'));


const AddcaseStudy= lazy(() => import('./admin/caseStudy/AddcaseStudy'));
const EditcaseStudy= lazy(() => import('./admin/caseStudy/EditcaseStudy'));
const ListcaseStudy= lazy(() => import('./admin/caseStudy/ListcaseStudy'));

const AddEventLocations = lazy(() => import('./admin/eventLocations/adminAddEventLocations'));
const EditEventLocations = lazy(() => import('./admin/eventLocations/adminEditEventLocations'));
const ListEventLocations= lazy(() => import('./admin/eventLocations/adminListEventLocations'));

const AddLiveTraining= lazy(() => import('./admin/liveTraining/adminAddLiveTraining'));
const EditLiveTraining= lazy(() => import('./admin/liveTraining/adminEditLiveTraining'));
const ListLiveTraining= lazy(() => import('./admin/liveTraining/adminListLiveTraining'));
const UsersListLiveTraning  = lazy(() => import('./admin/liveTraining/UsersList'));

const AddVideoTraining= lazy(() => import('./admin/videoTraining/adminAddVideoTraining'));
const EditVideoTraining= lazy(() => import('./admin/videoTraining/adminEditVideoTraining'));
const ListVideoTraining= lazy(() => import('./admin/videoTraining/adminListVideoTraining'));

const ListInternationalDistributor= lazy(() => import('./admin/internationalDistributor/listInterDistributor'));
const EditInternationalDistributor= lazy(() => import('./admin/internationalDistributor/editInterDistributor'));
const AddInternationalDistributor= lazy(() => import('./admin/internationalDistributor/addInterlDistributor'));

const ListArticles = lazy(() => import('./admin/articles/listArticles'));
const AddArticles = lazy(() => import('./admin/articles/addArticles'));
const EditArticles = lazy(() => import('./admin/articles/editArticles'));
const AdminSource = lazy(() => import('./admin/source/manageSource'));
const CareerCategory = lazy(() => import('./admin/careerCategory/manageCareerCategory'));

const Redirection = lazy(() => import('./admin/redirection/manageRedirection'));
const IpRedirection = lazy(() => import('./admin/ipRedirection/manageIpRedirection'));

const FeedBack = lazy(() => import('./admin/feedBack/feedBack'));
const AdminListAttendees = lazy(() => import('./admin/liveTraining/adminListAttendees'));

const ManageAttributes = lazy(() => import('./admin/attributes/manageAttributes'));
const EditAttributes = lazy(() => import('./admin/attributes/editAttributes'));



const CreateCforms = lazy(() => import('./admin/cimonForms/createCforms'));
const ListCforms = lazy(() => import('./admin/cimonForms/listcForms'));
const EditCforms = lazy(() => import('./admin/cimonForms/editCforms'));

const AddDistributorResources = lazy(() => import('./admin/distributorResources/addDistributorResources')); 
const EditDistributorResources = lazy(() => import('./admin/distributorResources/editDistributorResources')); 
const ListDistributorResources = lazy(() => import('./admin/distributorResources/listDistributorResources')); 
const DistributorManageCategory = lazy(() => import('./admin/distributorResources/category/manageCategory'));
 
const AddIntegratorResource = lazy(() => import('./admin/integratorResources/addIntegratorResources')); 
const EditIntegratorResource  = lazy(() => import('./admin/integratorResources/editIntegratorResources')); 
const ListIntegratorResource  = lazy(() => import('./admin/integratorResources/listIntegratorResources')); 
const IntegratorManageCategory = lazy(() => import('./admin/integratorResources/category/manageCategory'));
 


class App extends Component {
  state = {};

  /* sitelock submission */

  lockSubmit = (e) => {
    e.preventDefault();
    const password = document.getElementById("password").value;
    if (password === "Cimon2020") {
      localStorage.setItem("sitelock", true);
      window.location = "/";
    }
  };

  render() {
      const siteLock = localStorage.getItem("sitelock") === "true";
      const images = require.context("./assets/images", true);
      return (  
      <React.Fragment> 
        
            { /* router settings */ } 

         <Provider store = {store({})}>
          <BrowserRouter basename="btmoto-gate" >
          <ScrollTop/>   
          <Suspense fallback={<div style={{ 
            'height': '100vh',
            'display': 'flex',
            'position': 'absolute',
            'background': '#fff',
            'zIndex': '999',
            'width': '100%',
            'justifyContent': 'center',
            'alignItems': 'center',
          }}>Loading...</div>}>
          <Switch >


         
          <Route exact path = "/" component = {Adminlogin}/>                      
           { /* Admin */ } 
          <Route exact path = "/admin" component = {Adminlogin} />
          <Route exact path = "/admin/login" component = {Adminlogin} />            
          <Route exact path = "/admin/forgot-password" component = {AdminForgot} /> 
          <Route exact path = "/admin/forgot-password/:id" component = {AdminPassword } /> 
          <ProtectedRoutea exact path = "/admin/home" component = {AdminDashboard }/>    
          <ProtectedRoutea exact path = "/admin/pages" render = {props => < AdminPages parent = "cms"title = "Pages" {...props}/> }/> 
          {/* <ProtectedRoutea exact path = "/admin/page/page-builder-landing"render = {props => < PageBuilderLanding parent = "page builder"title = "Home" {...props } />  } />  */}
          <ProtectedRoutea exact path = "/admin/page/page-builder-basic"render = {props => < PageBuilderBasic parent = "page builder"title = "Home" {...props } />  } /> 
          <ProtectedRoutea exact path = "/admin/page/page-builder"render = {props => < PageBuilder parent = "page builder"title = "Home" {...props } />  } /> 
          <ProtectedRoutea exact path = "/admin/addpage" render = { props => < AdminAddPage parent = "cms"title = "Pages" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/edit-page/:slug" render = { props => < AdminEditPage parent = "cms"title = "Pages" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/home"render = {props => < HomeEditPage parent = "cms"title = "Home" {...props } />  } /> 
          <ProtectedRoutea exact path = "/admin/page/edit/about-us" render = {props => < AboutEditPage parent = "cms" title = "About Us" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/scada" render = {props => < ScadaEditPage parent = "cms"title = "Scada" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/scadabundle" render = {props => < ScadabundleEditPage parent = "cms"title = "Scadabundle" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/cimon-cicon" render = {props => < CimonCiconEditPage parent = "cms"title = "Cimon-Cicon" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/ipc" render = {props => < IpcEditPage parent = "cms"title = "ipc" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/hybrid-hmi" render = {props => < HybridHmiEditPage parent = "cms"title = "HMI + PLC" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/careers" render = {props => < CareerEditPage parent = "cms"title = "Career" { ...props} />  } /> 
          <ProtectedRoutea exact path = "/admin/page/edit/news" render = {props => < NewsEditPage parent = "cms"title = "News" { ...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/industry" render = {props => < IndustryEditPage parent = "cms"title = "Industry" { ...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/contact" render = {props => < ContactEditPage parent = "cms" title = "Contact" { ...props }/> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/career-center" render = {props => < CareerCenterEditPage parent = "cms"title = "career center" {...props} /> } /> 
          <ProtectedRoutea exact path = "/admin/page/edit/footer" render = {props => < FooterEditPage parent = "cms"title = "Footer" {...props } />  }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/distributor-portal" render = {props => < DistributorEditPage parent = "cms"title = "Distributor Portal" {...props}/>  }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/integrator-portal" render = { props => < IntegratorPortalEditPage parent = "cms" title = "Integrator Portal" { ...props}/> } /> 
          <ProtectedRoutea exact path = "/admin/page/edit/distributor-locator" render = {props => < DistributorLocatorEditPage parent = "cms"title = "Distributor Locator" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/plc"render = { props => < PlcEditPage parent = "cms" title = "PLC" {...props }/> } /> 
          <ProtectedRoutea exact path = "/admin/page/edit/hmi"render = {props => < HmiEditPage parent = "cms"title = "hmi" {...props} /> }/> 
         
          <ProtectedRoutea exact path = "/admin/page/edit/download"render = {props => < DownloadEditPage parent = "cms"title = "Download" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/products-splash"render = {props => < ProductSplash parent = "cms"title = "CIMON PRODUCTS" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/international-distributor"render = {props => < InternationalDistributorEditPage parent = "cms"title = "International Distributor" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/techsupport"render = {props => < TechSupportEditPage parent = "cms"title = "Tech Support " {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/our-company"render = {props => < OurCompanyEditPage parent = "cms"title = "Our Company " {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/our-customers"render = {props => < OurCustomersEditPage parent = "cms"title = "Our Customers " {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/si-program"render = {props => < SIProgramEditPage parent = "cms"title = "SI Program " {...props} /> }/> 
         
          <ProtectedRoutea exact path = "/admin/page/edit/case-study" render = {props => < CaseStudyEditPage parent = "cms"title = "Case Study" { ...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/live-training" render = {props => < LiveTrainingEditPage  parent = "cms"title = "Live Training " { ...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/video-tutorials" render = {props => < VideoTutorialEditPage  parent = "cms"title = "Video Tutorials " { ...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/archives" render = {props => < ArchivesEditPage  parent = "cms"title = "Archives " { ...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/faq" render = {props => < FaqEditPage  parent = "cms"title = "Faq " { ...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/page/edit/certificates" render = {props => < CertificatesEdit  parent = "cms"title = "Certificates" { ...props} /> }/> 
 
          <ProtectedRoutea exact path = "/admin/products" render = {props => < AdminProducts parent = ""title = "Products" {...props}/> } />
          <ProtectedRoutea exact path = "/admin/products/addnew" render = {props => < AdminProductAdd parent = ""title = "Products" {...props}/>}/> 
          <ProtectedRoutea exact path = "/admin/product/edit/:id" render = {props => < AdminProductEdit parent = "Products"title = "Products" {...props }/> } /> 
          <ProtectedRoutea exact path = "/admin/users" component = {ManageUsers}/>  
          <ProtectedRoutea exact path = "/admin/users/edit/:id" component = {EditUser}/> 
          <ProtectedRoutea exact path = "/admin/adminuser/edit/:id" component = {AdminEdit } />   
          <ProtectedRoutea exact path = "/admin/" component = {AdminDashboard}/>  
          <ProtectedRoutea exact path = "/admin/faq" render = {props => < AdminFaqPage parent = "cms"title = "Faq" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/addfaq" render = {props => < AdminAddFaq parent = "cms" title = "Faq" {...props }/> }/> 
          <ProtectedRoutea exact path = "/admin/editfaq/:id" render = {props => < AdminEditFaq parent = "cms" title = "Faq" {...props }/> }/> 

          <ProtectedRoutea exact path = "/admin/contact/address"  render = {props => < AdminManageAddress parent = "contact"  title = "Contact" {...props}/>  } /> 
          <ProtectedRoutea exact path = "/admin/contact/subscriptions"render = {props => < AdminManageSubscribers parent = "contact" title = "Contact" { ...props}/> } /> 
          <ProtectedRoutea exact path = "/admin/news" render = {props => < AdminListNews parent = ""title = "News" { ...props }/> }/>
          <ProtectedRoutea exact path = "/admin/news/add" render = {props => < AdminAddNews parent = ""title = "News" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/news/edit/:id" render = {props => < AdminEditNews parent = "" title = "News" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/careers" render = {props => < AdminListCareers parent = "" title = "Career" {...props}/> } />
          <ProtectedRoutea exact path = "/admin/career/add" render = {props => < AdminAddCareer parent = "" title = "Career" {...props }/>}/> 
          <ProtectedRoutea exact path = "/admin/career/edit/:id" render = {props => < AdminEditCareer parent = ""title = "Career" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/orders" render = {props => < Orders parent = "" title = "Orders" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/orders/:id" render = {props => < AdminOrderDetails parent = ""title = "Orders" {...props}/>}/> 
          <ProtectedRoutea exact path = "/admin/useradd"render = {props => < UserAdd parent = "" title = "Users" {...props}/> } />
          <ProtectedRoutea exact path = "/admin/adminadd" render = {props => < AdminsAdd parent = "" title = "Admins" { ...props}/> } />
          <ProtectedRoutea exact path = "/admin/user/:id" render = {props => < UserAdd parent = "" title = "Users" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/userslist" render = {props => < UsersList parent = "" title = "Users" {...props}/> }/>
          <ProtectedRoutea exact path = "/admin/adminusers" render = {props => < AdminUsersList parent = "" title = "Admins" {...props}/>}/> 
          <ProtectedRoutea exact path = "/admin/profile/:id" render = {props => < UserProfile parent = "" title = "Users" { ...props}/> }/>
          <ProtectedRoutea exact path = "/admin/settings/manage-categories" render = {props => < ManageCategories parent = "" title = "Categories" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/settings/manage-categories/:slug" render = {props => < EditCateogry parent = ""title = "Categories" {...props}/>}/>
          <ProtectedRoutea exact path = "/admin/menus" render = {props => < AdminMenus parent = ""title = "Menus" {...props} /> }/>


          <ProtectedRoutea exact path = "/admin/manage-profile" render = {props => < ManageProfile parent = ""title = "Profile" {...props} /> }/>
          <ProtectedRoutea exact path = "/admin/manage-slider" render = {props => < ManageSlider parent = ""title = "Slider" {...props} /> }/>

          
       
          <ProtectedRoutea exact path = "/admin/stripesettings" render = {props => < StripeSetting parent = ""title = "Stripe" {...props}/> } /> 
          <ProtectedRoutea exact path = "/admin/facebooksettings" render = {props => < FacebookSetting parent = ""title = "Facebook" {...props}/> } />
          <ProtectedRoutea exact path = "/admin/linkedinsettings" render = {props => < LinkedinSetting parent = ""title = "Linkedin" {...props}/> } /> 
          
         
          
          <ProtectedRoutea exact path = "/admin/manage-footer"render = {props => < AdminFooter parent = ""title = "Footer" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/shipping"render = {props => < ShippingOrigin parent = ""title = "Shipping" { ...props} /> } />
          <ProtectedRoutea exact path = "/admin/tax"render = {props => <GeneralTax parent = ""title = "Tax" { ...props}/> } />
          <ProtectedRoutea exact path = "/admin/resources"render = {props => < ListIntegratorResources parent = ""title = "Resources" {...props}/> }/>  
          <ProtectedRoutea exact path = "/admin/resources/add" render = {props => < IntegratorResources parent = ""title = "Resources" {...props}/> } /> 
          <ProtectedRoutea exact path = "/admin/resources/edit/:id"render = { props => < EditIntegratorResources parent = "" title = "Resources" {...props}/> } />  
          <ProtectedRoutea exact path = "/admin/storelocator" render = {props => <StoreLocator parent = ""title = "Store Locator" {...props} />  } />
          <ProtectedRoutea exact path = "/admin/storelocator/edit/:id"render = {props => < StoreLocatorEdit parent = "Store Locator"title = "Store Locator"{...props }/> }/>
          <ProtectedRoutea exact path = "/admin/storelocator/add" render = {props => < StoreLocatorAdd  parent = "Store Locator"title = "Store Locator"{...props}/>  } />
          <ProtectedRoutea exact path = "/admin/coupons"render = {props => < AdminCoupons parent = ""title = "Coupons" { ...props} />  }/>
        
        
          <ProtectedRoutea exact path = "/admin/source"render = {props => < AdminSource parent = ""title = "software" { ...props} />  }/>
          <ProtectedRoutea exact path = "/admin/careers/category"render = {props => < CareerCategory parent = ""title = "Career" { ...props} />  }/>
        
        
          <ProtectedRoutea exact path = "/admin/archives" render = {props => < ListArchives parent = "" title = "Archives" {...props}/> }/>
          <ProtectedRoutea exact path = "/admin/archives/add"render = { props => < AddArchives parent = ""title = "Archives" {...props} />} />
          <ProtectedRoutea exact path = "/admin/archives/edit/:id"render = { props => < EditArchives parent = "" title = "Archives" { ...props }/>}/>
          <ProtectedRoutea exact path = "/admin/language" render = {props => <Language parent = ""title = "Language" {...props} /> }/>
          <ProtectedRoutea exact path = "/admin/language/edit/:id" render = {props => <EditLanguage parent = ""title = "Language" {...props} /> }/>
          <ProtectedRoutea exact path = "/admin/preview/:slug" render = {props => < PreviewPage parent = "preview" title = "" {...props}/>}/>
          <ProtectedRoutea exact path = "/admin/uploads" render = {props => <MediaUpload parent = ""title = "Media Files" {...props}/> } /> 
          <ProtectedRoutea exact path = "/admin/uploads/addnew" render = {props => < MediaAdd parent = ""title = "Media Files" {...props}/>}/> 
          <ProtectedRoutea exact path = "/admin/browse/media" render = {props => <BrowseMedia parent = ""title = "Media Files" {...props}/> } /> 
          <ProtectedRoutea exact path = "/admin/feedback" render = {props => <FeedBack parent = ""title = "Feedback" {...props} /> }/>
         

          <ProtectedRoutea exact path = "/admin/industry/add" render = {props => < AddIndustry parent = ""title = "Industries" { ...props }/> }/>
          <ProtectedRoutea exact path = "/admin/industry/edit/:id" render = {props => < EditIndustry parent = ""title = "Industries" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/industry" render = {props => < ListIndustry parent = "" title = "Industries" {...props} /> }/> 
         
          <ProtectedRoutea exact path = "/admin/casestudy/add" render = {props => < AddcaseStudy parent = ""title = "Case Study" { ...props }/> }/>
          <ProtectedRoutea exact path = "/admin/casestudy/edit/:id" render = {props => < EditcaseStudy parent = ""title = "Case Study" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/casestudy" render = {props => < ListcaseStudy parent = "" title = "Case Study" {...props} /> }/> 

          <ProtectedRoutea exact path = "/admin/event-locations/add" render = {props => < AddEventLocations parent = ""title = "Event Locations" { ...props }/> }/>
          <ProtectedRoutea exact path = "/admin/event-locations/edit/:id" render = {props => < EditEventLocations parent = ""title = "Event Locations" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/event-locations" render = {props => < ListEventLocations parent = "" title = "Event Locations" {...props} /> }/> 
         
          <ProtectedRoutea exact path = "/admin/live-training/add" render = {props => < AddLiveTraining parent = ""title = "Live Training" { ...props }/> }/>
          <ProtectedRoutea exact path = "/admin/live-training/edit/:id" render = {props => < EditLiveTraining parent = ""title = "live Training" {...props}/> }/> 
         
          <ProtectedRoutea exact path = "/admin/live-training/training/:id" render = {props => < UsersListLiveTraning parent = ""title = "Users" {...props}/> }/> 
         
          <ProtectedRoutea exact path = "/admin/live-training" render = {props => < ListLiveTraining parent = "" title = "Live Training" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/attendee" render = {props => < AdminListAttendees parent = "" title = "Live Training" {...props} /> }/> 
         
          
          <ProtectedRoutea exact path = "/admin/video-training/add" render = {props => < AddVideoTraining parent = ""title = "Video Training" { ...props }/> }/>
          <ProtectedRoutea exact path = "/admin/video-training/edit/:id" render = {props => < EditVideoTraining parent = ""title = "Video Training" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/video-training" render = {props => < ListVideoTraining parent = "" title = "Video Training" {...props} /> }/> 

          <ProtectedRoutea exact path = "/admin/international-distributor/add" render = {props => < AddInternationalDistributor parent = "" title = "International Distributor" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/international-distributor/edit/:id" render = {props => < EditInternationalDistributor parent = "" title = "International Distributor" {...props} /> }/> 
          <ProtectedRoutea exact path = "/admin/international-distributor" render = {props => < ListInternationalDistributor parent = "" title = "International Distributor" {...props} /> }/> 


          <ProtectedRoutea exact path = "/admin/articles" render = {props => < ListArticles  parent = ""title = "Articles" { ...props }/> }/>
          <ProtectedRoutea exact path = "/admin/articles/add" render = {props => < AddArticles  parent = ""title = "Articles" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/articles/edit/:id" render = {props => < EditArticles  parent = "" title = "Articles" {...props} /> }/> 


          <ProtectedRoutea exact path = "/admin/redirection"render = {props => < Redirection parent = ""title = "Redirection" { ...props} />  }/>
          <ProtectedRoutea exact path = "/admin/ipredirection"render = {props => < IpRedirection parent = ""title = "IP Redirection" { ...props} />  }/>
        
        
          <ProtectedRoutea exact path = "/admin/settings/manage-attributes" render = {props => <ManageAttributes parent = "" title = "Attributes" {...props}/> }/> 
          <ProtectedRoutea exact path = "/admin/settings/manage-attributes/:slug" render = {props => <EditAttributes parent = ""title = "Attributes" {...props}/>}/>
        
          <ProtectedRoutea exact path = "/admin/add-cforms"render = {props => < CreateCforms parent = "" title = "Cimon Forms" {...props}/> }/> 
 <ProtectedRoutea exact path = "/admin/list-cforms"render = {props => < ListCforms parent = "" title = "Cimon Forms" {...props}/> }/>   
 <ProtectedRoutea exact path = "/admin/edit-cform/:id" render = {props => < EditCforms  parent = "" title = "Cimon Forms" {...props} /> }/> 

          <ProtectedRoutea exact path = "/admin/distributor-resources"render = {props => <ListDistributorResources parent = ""title = "Distributor Resources" { ...props} />  }/>
          <ProtectedRoutea exact path = "/admin/distributor-resources/add"render = { props => <AddDistributorResources parent = ""title = "Distributor Resources" {...props} />} />
          <ProtectedRoutea exact path = "/admin/distributor-resources/edit/:id"render = { props => <EditDistributorResources parent = "" title = "Distributor Resources" { ...props }/>}/>
          <ProtectedRoutea exact path = "/admin/distributor-category"render = {props => <DistributorManageCategory parent = ""title = "Distributor Category" { ...props} />  }/>
        
  

          <ProtectedRoutea exact path = "/admin/integrator-resources"render = {props => <ListIntegratorResource  parent = ""title = "Integrator Resources" { ...props} />  }/>
          <ProtectedRoutea exact path = "/admin/integrator-resources/add"render = { props => <AddIntegratorResource  parent = ""title = "Integrator Resources" {...props} />} />
          <ProtectedRoutea exact path = "/admin/integrator-resources/edit/:id"render = { props => <EditIntegratorResource  parent = "" title = "Integrator Resources" { ...props }/>}/>
          <ProtectedRoutea exact path = "/admin/integrator-category"render = {props => <IntegratorManageCategory parent = ""title = "Integrator Category" { ...props} />  }/>
        
          

          
          </Switch> 
          </Suspense> 
          </BrowserRouter> 
        </Provider> 

        { /* router settings */ } 
        
      </React.Fragment>
      );
  }
}
export default App;