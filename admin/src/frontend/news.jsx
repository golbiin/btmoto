import React, { Component } from 'react';
import {  Container, Row, Col } from 'react-bootstrap';
import { Link } from "react-router-dom";
import TextTruncate from 'react-text-truncate'; 
import Moment from 'react-moment';
import Header from "./common/header";
import Footer from "./common/footer";
import Banner from './common/banner';
import SubFooter from './common/subFooter';
import * as newsService from "../ApiServices/news";
import * as pageService from "../ApiServices/page";
import { Helmet } from 'react-helmet';


class News extends Component {
    constructor(props) {
        super(props);
         this.state = {
            content :{},
            categoryData:[{
                    "title": "CIMON NEWS",
                    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    "image": "news-banner.jpg"
                    },
                    {
                        "title": "CIMON NEWS",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                        "image": "news-banner.jpg"
                    },
                    {
                        "title": "CIMON NEWS",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                        "image": "news-banner.jpg"
                    }
                ],
                blocking: false,
                visibleRecent: 8,
                visibleArticle: 9,
                error: false,
                recentNews:[],
                latestNews:[],
                topNews:[],
                spinner:false,
                page_title:"",
        }
        this.loadmoreRecent  = this.loadmoreRecent.bind(this);
        this.loadmoreArticle = this.loadmoreArticle.bind(this);
        this.myRef = React.createRef();
    }
    scrollToMyRef = () => window.scrollTo(0, this.myRef.current.offsetTop);

    loadmoreRecent() {
        this.setState((prev) => {
          return {visibleRecent: prev.visibleRecent + 8};
        });
      }

      loadmoreArticle() {
        this.setState((prev) => {
          return {visibleArticle: prev.visibleArticle + 9};
        });  
      }

    /* Get Page Contents */
      getContent = async (slug) => {
        try {
          const response = await pageService.getPageContent(slug);
          if (response.data.status === 1) {
             /* Banner-Slider Contents */
            if (response.data.data.content.bannerSlider) {
              const banner_content = [];
            
              response.data.data.content.bannerSlider.map((banner, index) => {
                const Setdata = {};
                Setdata.title = banner.heading ? banner.heading : '';
                Setdata.description = banner.subheading ? banner.subheading : '';
                Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
                banner_content.push(Setdata);
              });
              this.setState({
                categoryData: banner_content,
              });   
            }
            this.setState({ content: response.data.data.content });
            
        if(response.data.data.page_title){
            this.setState({ 
              page_title: response.data.data.page_title
              });
  
          }
          }
        } catch (err) {
          this.setState({ spinner: false });
        }
      };

    componentDidMount = () => {
        const slug = this.props.match.path.substring(this.props.match.path.lastIndexOf('/') + 1);
        this.getNews();
        this.getContent(slug);    
    };
    
  /*  Get All types of News  */
    getNews = async () => {
        this.setState({ spinner: true});
        try {
            const response =  await newsService.getNews();
            if(response.status === 200 ) {
                if (response.data.latestNews.length > 0 ) {
                    this.setState({ latestNews: response.data.latestNews});
                }
                if (response.data.recentNews.length > 0 ) {
                    this.setState({ recentNews: response.data.recentNews});
                }
                if (response.data.topNews.length > 0) {
                    this.setState({ topNews: response.data.topNews});
                }
            }
            this.setState({ spinner: false});
        } catch (err) {
            this.setState({ spinner: false});
        }
      }
   
    render() { 
        const { categoryData,  latestNews,page_title } = this.state;
        const images = require.context("../assets/images", true);
        let newsImg;
        return ( 
            <React.Fragment>
            <Helmet>
            <title>{ page_title ? page_title:" " }</title>
            </Helmet>
             <Header />
             <div className="news-page-wrapper">
                <Banner categoryInfo={ categoryData } />
                <div className="latest-news-wrapper">
                    <Container className="news" data-aos="fade-up"  data-aos-duration="2000">
                        <h2>Latest Articles</h2>
                        {latestNews.map( ( news, index) => {
                            if(news.image) {
                                newsImg = news.image;
                            }  else {
                                newsImg =   images(`./frontend/news-placeholder1.png`);
                            }   
                            return (
                                <>
                                <Row className="no-gutters" key={index}>
                                    <Col lg={8} className="left-section">
                                        <div className="img-box">
                                            <img alt="News poster " src={newsImg}></img>
                                            <div className="over-text">
                                                <TextTruncate
                                                    line={1}
                                                    element="p"
                                                    truncateText="…"
                                                    text={news.title}                                                   
                                                />        
                                            </div> 
                                        </div>
                                    </Col>
                                    <Col lg={4} className="right-section">
                                        <TextTruncate
                                            line={1}
                                            element="h4"
                                            truncateText="…"
                                            text={news.title}     
                                        />
                                        <TextTruncate
                                            line={8}
                                            element="p"
                                            truncateText="…"
                                            text={news.description} 
                                        />
                                        <Link to={"/latest-news/" + news.slug} className="read-link">
                                        READ MORE
                                        </Link>
                                    </Col>
                                </Row>
                                </>
                            );
                        })}   
                    </Container>
                </div>
                <div className="resently-published-wrapper">
                    <Container className="news"  data-aos="fade-up"  data-aos-duration="2000">
                        <h2>Recently published</h2>
                        <ul>
                        {this.state.recentNews.slice(0, this.state.visibleRecent).map( ( news, index) => {
                            return (
                                <li key={index}>
                                    <span>
                                        <Moment format="MMM. D">
                                        {news.date}
                                        </Moment>
                                    </span>
                                    <span>
                                        <Link to={"/latest-news/" + news.slug}>
                                        {news.title}
                                        </Link>
                                    </span>
                                </li>
                            )
                        })}                         
                         </ul>
                        <div className="load-more-box">
                        {this.state.visibleRecent < this.state.recentNews.length &&                      
                        <Link to="#" onClick={this.loadmoreRecent} >
                                 LOAD MORE
                                 </Link>
                                }
                        </div>
                    </Container>
                </div>
                <div className="top-articles-wrapper">
                    <Container className="news"  data-aos="fade-up"  data-aos-duration="2000">
                        <h2>Top Articles</h2>
                        <Row>                     
                            {this.state.topNews.slice(0, this.state.visibleArticle).map( (news, index) => {
                                if(news.image) {
                                    newsImg = news.image;
                                }  else {
                                    newsImg =   images(`./frontend/news-placeholder.png`);
                                }  
                                return(
                                    <Col md={4} sm={6} className="box" key={index}>
                                        <div className="item">
                                            <Link to="#" className="img-link">
                                                <img alt="News poster" src={newsImg}></img>
                                            </Link>
                                            <div className="content-box">
                                                <h3>{news.title} </h3>
                                                <TextTruncate
                                                    line={4}
                                                    element="p"
                                                    truncateText="…"
                                                    text={news.description} 
                                                />
                                                <div className="link-box">
                                                    <Link to={"/latest-news/" + news.slug} className="read-link">
                                                    READ MORE
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </Col>                            
                                )
                            } ) }
                        </Row>
                        <div className="load-more-box">
                        {this.state.visibleArticle < this.state.topNews.length &&
                             <Link to="#" onClick={this.loadmoreArticle}>LOAD MORE</Link>
                        }
                        </div>
                    </Container>
                </div>
                <SubFooter />
             </div>
            <Footer />
            </React.Fragment>
        );
    }
}
 
export default News ;