import React, { Component } from "react";
import {Container,Row,Col} from "react-bootstrap";
import * as newsService from "../ApiServices/news";
import Header from "./common/header";
import Footer from "./common/footer";
import SubFooter from "./common/subFooter";
import { Helmet } from 'react-helmet';
import BlockUi from "react-block-ui";


class ArchiveSingle extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      blocking: false,
      archiveData: [],
    };
  }

  componentDidMount = () => {
    this.getArchiveBySlug();
    window.scrollTo(0, 0);
  };

  /*  Get Single Archive Details  */
  getArchiveBySlug = async () => {
    this.setState({blocking:true});
    const archiveSlug = this.props.match.params.slug;
    const response = await newsService.getArchiveBySlug(archiveSlug);
    this.setState({blocking:false});
    if (response.data.status === 1) {
      let newArchivearray = {
        title: response.data.data.title,
        image: response.data.data.image,
        date: response.data.data.date,
        content: response.data.data.content,
      };
      this.setState({ archiveData: newArchivearray });
      this.setState({ spinner: false });
    }
  };

  render() {
    const { archiveData } = this.state;
    const archiveSlug = this.props.match.params.slug;
 
    const TITLE = archiveSlug +' | Automation Company - Industrial Automation';
    return (
      <React.Fragment>
      <Helmet>
      <title>{ TITLE }</title>
      </Helmet>
        <Header />
        <div className="latest-news-single-wrapper">
          <Container className="single-news-container">
            <BlockUi tag="div" blocking={this.state.blocking}>
            <Row>
              <Col>
                <h2>{archiveData.title}</h2>
                <p dangerouslySetInnerHTML={{ __html: archiveData.content }} />
              </Col>
            </Row>
            </BlockUi>
            <Row className="single-news-row">
              <Col>
                <p className="author-data">Posted On: March 20, 2020</p>
              </Col>
            </Row>
            <Row className="share-row">
              <Col>
                <label>Share: </label>
                <ul class="social-network social-circle">
                  <li>
                    <a
                      onClick={() =>
                        window.open("https://twitter.com/CIMONsns", "_blank")
                      }
                      class="icoTwitter"
                      title="Twitter"
                      rel="noopener noreferrer"
                    >
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.facebook.com/cimoninc/"
                      target="_blank"
                      class="icoFacebook"
                      title="Facebook"
                      rel="noopener noreferrer"
                    >
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.linkedin.com/company/cimonautomation"
                      target="_blank"
                      class="icoLinkedin"
                      title="Linkedin"
                      rel="noopener noreferrer"
                    >
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.instagram.com/cimon_official/ "
                      target="_blank"
                      class="icoInstagram"
                      title="Instagram"
                      rel="noopener noreferrer"
                    >
                      <i class="fa fa-instagram"></i>
                    </a>
                  </li>
                </ul>
              </Col>
            </Row>
          </Container>
        </div>
        <SubFooter />
        <Footer />
      </React.Fragment>
    );
  }
}
export default ArchiveSingle;
