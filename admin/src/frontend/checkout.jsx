import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import { PUBLISHABLE_KEY } from "../config.json";
import CheckoutForm from "./checkout/checkoutForm";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {loadCart, hideCart,clearCart,removeCoupon} from "../services/actions/cartActions";
import { showHideLogin, changeTab } from "../services/actions/userActions";
import Header from "./common/header";
const stripePromise = loadStripe(PUBLISHABLE_KEY);

class Checkout extends Component {
  static propTypes = {
    loadCart: PropTypes.func.isRequired,
    clearCart: PropTypes.func.isRequired,
    cartProducts: PropTypes.array.isRequired,
    cartTotal: PropTypes.object.isRequired,
    hideCart: PropTypes.func.isRequired,
    showHideLogin: PropTypes.func.isRequired,
    changeTab: PropTypes.func.isRequired,
    removeCoupon: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      redirectCheckout: false,
      orderId: "",
    };
    this.props.hideCart(false);
  }
  callbackRedirect = (childData, orderId) => {
    this.setState({ orderId: orderId });
    this.setState({ redirectCheckout: childData });
  };

  render() {
    const images = require.context("../assets/images", true);
    const {
      cartTotal,
      cartProducts,
      showCartFlag,
      loadCart,
      clearCart,
      cartId,
      authUser,
      token,
      showHideLogin,      
      couponDetails,
      removeCoupon,      
    } = this.props;

    if (this.state.redirectCheckout === true) {
      let url = "/checkout-thank-you/" + this.state.orderId;
      return <Redirect to={url} />;
    }

    return (
      <React.Fragment>
        <div className="checkout-page-wrapper">
          <Header />
          <div className="page-box">
            <div className="logo-box">
              <Link className="logo" to="/">
                <img
                  className="logoimg"
                  alt=""
                  src={images(`./frontend/cimon-logo-checkout.png`)}
                ></img>
              </Link>
            </div>
            <div className="content-wrapper">
              {(() => {
                if (authUser.email === "") {
                  return (
                    <h4 className="empty-cart">
                      You must be logged in to checkout.
                      <Link to="#" onClick={() => showHideLogin(true, "login")}>
                         login
                      </Link>
                    </h4>
                  );
                } else if (cartProducts.length === 0) {
                  return <h4 className="empty-cart">Your cart is empty!</h4>;
                } else {
                  return (
                    <Elements stripe={stripePromise}>
                      <CheckoutForm
                        key="checkoutFom"
                        token={token}
                        authUser={authUser}
                        cartId={cartId}
                        cartProducts={cartProducts}
                        cartTotal={cartTotal}
                        loadCart={loadCart}
                        showCartFlag={showCartFlag}
                        clearCart={clearCart}
                        couponDetails={couponDetails}
                        removeCoupon={removeCoupon}
                        callbackRedirect={this.callbackRedirect}
                      />
                    </Elements>
                  );
                }
              })()}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  token: state.auth.token,
  authUser: state.auth.authUser,
  cartId: state.cart.cartId,
  cartProducts: state.cart.products,
  cartTotal: state.cart.data,
  showCartFlag: state.cart.showCartFlag,
  displayLoginPopup: state.auth.displayLoginPopup,
  activeTab: state.auth.activeTab,
  couponDetails: state.cart.couponDetails,
});

export default connect(mapStateToProps, {
  loadCart,
  hideCart,
  clearCart,
  showHideLogin,
  changeTab,
  removeCoupon,
})(Checkout);
