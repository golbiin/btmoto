import React, { Component } from "react";
import Header from "./common/header";
import Footer from "./common/footer";
import SubFooter from "./common/subFooter";
import {  Link } from "react-router-dom";
import SilckSilder from "./common/slickSilder";
import * as pageService from "../ApiServices/page";
import { Helmet } from 'react-helmet';
import  "../services/aos";
import { siteUrl } from '../config.json';


class Home extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  state = {
    content : {},
    homedata : {},
    page_title:"",
  }
  scrollToMyRef = () => window.scrollTo(0, this.myRef.current.offsetTop);


  getContent = async (slug) => {

    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status == 1) {
       /* Banner-Slider Contents */  
        if (response.data.data.content.bannerSlider) {
          const Setdata = {};
          response.data.data.content.bannerSlider.map((banner, index) => {
            Setdata.title = banner.heading ? banner.heading : '';
            Setdata.description = banner.subheading ? banner.subheading : '';
            Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
          });
          this.setState({
            homedata: Setdata,
          });
        }
        this.setState({ content: response.data.data.content });

        if(response.data.data.page_title){
          this.setState({ 
            page_title: response.data.data.page_title
            });

        }
      } else {

      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };
  componentDidMount = async () => {
    const slug = 'home';
    
    this.getContent(slug);
  };

  render() {
    const images = require.context("../assets/images", true);
    const {page_title} = this.state;
    return (
      <React.Fragment>
      <Helmet>
      <title>{ page_title ? page_title:" " }</title>
      </Helmet>
        <Header />
        <div className="container-fluid" id="home">
          <div className="home-video-section">
            <div className="videoWrapper">
              <video width="100%" height="auto">
              <source
                  src={siteUrl+"/images/Website.mp4"} type="video/mp4"
                />
              </video>
            </div>
            <div className="photoWrapper">
              <div className="home_banner_bg"></div>
            </div>
            <Link
              onClick={this.scrollToMyRef}
              to="#"
              className="scroll-down-link"
            ></Link>

            <div className="caption_fullwidth">
              <div className="container caption_container">
                <div className="slideshow_caption">
                  <h2 dangerouslySetInnerHTML={{ __html: this.state.homedata.title ? this.state.homedata.title : '' }} >                  
                  </h2>
                  <div dangerouslySetInnerHTML={{ __html: this.state.homedata.description ? this.state.homedata.description : '' }}>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div ref={this.myRef} className="future_auto" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="middle_wrapper">
               <div dangerouslySetInnerHTML={{ __html: this.state.content.section1 ? this.state.content.section1 : '' }}>
               </div>
                <div className="iconss_section">
                  <div className="single_icon_section row justify-content-between"  dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }}>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="break-down-sec" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="break_sec_full"  dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}>        
              </div>
            </div>
          </div>
          <div className="pixel_map" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="full_content"  dangerouslySetInnerHTML={{ __html: this.state.content.section4 ? this.state.content.section4 : '' }}>
              </div>
            </div>
          </div>
          <div className="success_slider" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <h2>CIMON NEWS</h2>
            </div>
          
            <SilckSilder banners = {this.state.content.newsSilder} />
            <div className="container">
              <div className="full_slidercontent">
                <div className="slider_section" dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}>
                </div>
              </div>
            </div>
          </div>
          <div id="home-section-bussiness-trust" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12" dangerouslySetInnerHTML={{ __html: this.state.content.section6 ? this.state.content.section6 : '' }}>
                </div>
                <div className="container my-4">
                  <div
                    id="logo_slider"
                    className="carousel slide carousel-multi-item"
                    data-ride="carousel"
                  >
                    <div className="carousel-inner" role="listbox">
                      <div className="carousel-item active" dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}>  
                      </div>
                      <div className="carousel-item" dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}>
                      </div>
                    </div>

                    <div className="controls-top">
                      <a
                        className="carousel-control-prev"
                        role="button"
                        href="#logo_slider"
                        data-slide="prev"
                      >
                        <i className="fa fa-chevron-left"></i>
                      </a>
                      <a
                        className="carousel-control-next"
                        role="button"
                        href="#logo_slider"
                        data-slide="next"
                      >
                        <i className="fa fa-chevron-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12" dangerouslySetInnerHTML={{ __html: this.state.content.section8 ? this.state.content.section8 : '' }}>
                </div>
              </div>
            </div>
          </div>
          <div id="home-section-join-industry" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12" dangerouslySetInnerHTML={{ __html: this.state.content.section9 ? this.state.content.section9 : '' }}>
                </div>
              </div>
            </div>
          </div>

          <SubFooter />
        </div>

        <Footer />
      </React.Fragment>
    );
  }
}

export default Home;
