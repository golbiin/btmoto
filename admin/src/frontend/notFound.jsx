import React  from "react";
import Header from "./common/header";
import Footer from "./common/footer";
import SubFooter from "./common/subFooter";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import { Helmet } from "react-helmet";
const TITLE = "Page not found | Automation Company - Industrial Automation";

const NotFound = (props) => {
  return (
    <React.Fragment>
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <Header />
      <div className="search-page">
        <div className="bannerWrapper">
          <div className="sip_banner_bg">
            <div className="sbg"></div>
            <div className="container">
              <div className="hedng">
                <h1>PAGE NOT FOUND</h1>
              </div>
              <Link to="#" className="scroll-down-link"></Link>
            </div>
          </div>
        </div>
        <Container className="search-results">
          <Row>
            <Col>
              <h4>{"Sorry! Page not found."}</h4>
            </Col>
          </Row>
        </Container>
      </div>
      <SubFooter />
      <Footer />
    </React.Fragment>
  );
};

export default NotFound;
