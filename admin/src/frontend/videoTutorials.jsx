import React, { Component } from "react";
import Header from "./common/header";
import Footer from "./common/footer";
import { Container, Row, Col } from "react-bootstrap";
import Banner from "./common/banner";
import SubFooter from "./common/subFooter";
import poster from '../assets/images/frontend/videotutplay.png';
import { Helmet } from 'react-helmet';
import { Link } from "react-router-dom";

const TITLE = 'Video Tutorials | Automation Company - Industrial Automation'

class VideoTutorials extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      getJobsDet: [],
      status: "",
      categoryData: [
        {
          title: "VIDEO TUTORIALS",
          sub_title: "Automation Tutorials, Tech Notes,Webinars and more",
          description:
            "Lorem ipsum dolor sit , consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit , consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat .",
          image: "banner-cimon-vidoe-tutorials.jpg",
        },
      ],
      content: {},   
    };
  }

  componentDidMount = async () => {
  };

  render() {
   const { categoryData } = this.state;
   let video = '';
   video= require('../assets/videos/IPC_TableTopVideo_0ct4.mp4');

    return (
      <React.Fragment>
         <Helmet>
          <title>{ TITLE }</title>
        </Helmet>
        <Header />
        <div className="container-fluid">
          <div id="videoTutorials">
            <Banner categoryInfo={categoryData} />
         
           <Container className="title-section text-center">
                    <Row className="justify-content-center">
                        <Col sm={12} md={12} >
                           <h2>WEBINARS</h2> 
                          
                        </Col>
                    </Row>
           </Container>

          <div className="video-wrapper" data-aos="fade-up"  data-aos-duration="2000">
              <Container className="tutorial">
                <Row
                  className="justify-content-md-between justify-content-sm-around ">
                      <Col md={3} className="pr-md-0">
                            <h3>CIMON Product Overview</h3>                           
                            <div className="scada-video-box">
                                <video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> 
                            </div> 
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>Using XpanelDesigner for HMI</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>Using CICON for PLC</h3>  
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                          
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>Using UltimateAccess SCADA</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>

                       <Col md={3} className="pr-md-0">
                            <h3>Handling Communications</h3>                           
                            <div className="scada-video-box">
                                <video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> 
                            </div> 
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>Handling Communications</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>COMING SOON</h3>  
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                          
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>COMING SOON</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>
                </Row> 
              </Container>
            </div>
            <Container className="title-section text-center">
                    <Row className="justify-content-center">
                        <Col sm={12} md={12} >
                           <h2>TUTORIALS/QUICK TIPS</h2> 
                          
                        </Col>
                    </Row>
           </Container>
          <div className="video-wrapper" data-aos="fade-up"  data-aos-duration="2000">
              <Container className="tutorial">
                <Row
                  className="justify-content-md-between justify-content-sm-around ">
                      <Col md={3} className="pr-md-0">
                            <h3>Xpanel HMI Basics</h3>                           
                            <div className="scada-video-box">
                                <video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> 
                            </div> 
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>HMI Software Installation</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>HMI Software on Windows OS</h3>  
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                          
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>CICON Installation</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>

                       <Col md={3} className="pr-md-0">
                            <h3>Tagging and Database</h3>                           
                            <div className="scada-video-box">
                                <video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> 
                            </div> 
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>Xpanel HMI Scripting</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>COMING SOON</h3>  
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                          
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>COMING SOON</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>
                </Row>                     
              </Container>
            </div>
            <Container className="title-section text-center">
                    <Row className="justify-content-center">
                        <Col sm={12} md={12} >
                           <h2>PRODUCT VIDEOS</h2> 
                          
                        </Col>
                    </Row>
           </Container>
          <div className="video-wrapper" data-aos="fade-up"  data-aos-duration="2000">
              <Container className="tutorial">
                <Row
                  className="justify-content-md-between justify-content-sm-around ">
                       <Col md={3} className="pr-md-0">
                            <h3>CIMON PLC</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>UltimateAccess SCADA</h3>  
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                          
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>CIMON Ultimate IPC</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>
                       <Col md={3} className="pr-md-0">
                            <h3>CIMON Xpanel HMI</h3>   
                            <div className="scada-video-box">
                                {<video controls poster={poster}>
                                    <source src={video} type="video/mp4" />
                               </video> }
                            </div>                         
                       </Col>
                </Row>                         
              </Container>
            </div>
          <div className="hr_outer">
          <Container className="tutorial">
             <Row className="hr_line"></Row>
            </Container> 
          </div>
      <div className="bottom_links">
      <Container className="tutorial">
                <Row
                  className="justify-content-md-between justify-content-sm-around">
                       <Col md={4} className="common_button ts text-md-left">
                            <Link to="/live-training">
                            <h3>Tech Support</h3>   
                            </Link>                       
                       </Col>
                       <Col md={4} className="common_button lt text-md-center"> 
                       <Link to="/live-training">
                            <h3>Live Training</h3>  
                        </Link>                      
                       </Col>
                       <Col md={4} className="common_button ds text-md-right">
                       <Link to="/download">
                            <h3>Download Software</h3>   
                      </Link>                           
                       </Col>
                </Row>
              </Container>
        </div>
         <SubFooter />
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

export default VideoTutorials;
