import React from "react";
import { Route, Redirect } from "react-router-dom";
import * as authServices from "../ApiServices/login";
import { USER_TYPES } from './../config.json';
const DistributorRoutea = ({ component: Component, render, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {

        const authUser = authServices.getCurrentUser();
        if (authUser && authUser.usertype == USER_TYPES.DISTRIBUTOR)
            return Component ? <Component {...props} /> : render(props);
        return <Redirect to="/distributor-portal" />;
        
      }}
    />
  );
};

export default DistributorRoutea;
