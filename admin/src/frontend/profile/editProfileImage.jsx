import React, { useState, useReducer } from "react";
import { Button, Modal, Form, Container, Row, Col } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers';
import * as yup from "yup";
import * as authServices from "./../../ApiServices/login";
import Profileupload from "../common/profileUpload";

const EditProfileImage = (props) => {
    const schema = yup.object().shape({   
      profile_image: yup.string().
            required("This filed is required"),        
    });
    
    const reload=()=>window.location.reload();
    const [setProcessingTo] = useState(false);
    const [setUserInfo] = useReducer(
      (state, newState) => ({...state, ...newState}),
      {
          profile_image: props.profileImg.profile_image,        
      }
  );
    const [serverError, setServerError] = useState('');
    const [success, setSuccess] = useState('');
    const { register, handleSubmit, errors } = useForm({
      resolver: yupResolver(schema)
  });

    const onSubmit = async (data, e) => {
      setProcessingTo(true);  
      setSuccess('');
      setServerError(''); 

      const userData = {
          _id : data._id,
          profile_image: data.profile_image,
      };
   
      try {
          const response = await authServices.updateUserdata(userData);  
          const data = response.data
          if(response.status == 200 && response.data.status == 1) {
              props.updateUserdata();                 
              setSuccess(response.data.message)  ;
              setServerError('');   
          } else {
              setServerError(response.data.message);
              setSuccess('');   
          }
          setProcessingTo(false);           
      } catch (error) {
          setProcessingTo(false);
      }

  };

    return (
      <div>
      <Modal 
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        className="edit-profile-modal"
        onHide={reload}
      > 
        <Modal.Header closeButton >
          <span className="close_popup"
           ></span>
        </Modal.Header>   
        <Modal.Body >
        <BlockUi tag="div"
         blocking={props.submit_status}  
         >
           <Form className="img-upload-form" method="post" onSubmit={handleSubmit(onSubmit)} >
          <Container>              
          <Row>
            <Col sm={12}>
            <p style={{color: "green"}} className={props.message_type}>{props.message}</p>
            <h3>Change Profile Picture</h3>
            </Col>
          </Row>
          <Row className="forgot-form">
              <Col sm= {12}>
              <div className="gallery-single profile-upload"> 
                  <div className="form-group">
                    <Profileupload
                     className="form-control "
                     onuplaodProfile={props.onuplaodProfile}
                     value= { props.profileImg.profile_image}
                  />
                  </div>
                  </div>
              </Col>
              <Col sm={12} md={6} xs={12}></Col>
              <Col sm={12} md={6} xs={12} className="profile-img-upload">
              <Button
                className="profile-img-button btn btn-primary"
                onClick={props.updateUser} variant="primary" type="submit" disabled={false}>
                UPLOAD PHOTO
              </Button>
              </Col>
          </Row>             
          </Container>
          </Form>
          </BlockUi>
        </Modal.Body>
      </Modal>
    </div>
    );
}

export default EditProfileImage;