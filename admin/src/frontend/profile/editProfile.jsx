import React, { useState, useEffect, useReducer } from "react";
import { useDispatch } from 'react-redux';
import { Button, Modal, Form, Container, Row, Col } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers';
import * as yup from "yup";
import { Countries } from './../../countries';
import * as authServices from "./../../ApiServices/login";
import { SET_PROFILE } from "../../services/type";
const EditProfile = (props) => {

    const dispatch = useDispatch();
    const schema = yup.object().shape({
        first_name: yup.string()
            .required("First name is required")
            .matches("^[a-zA-Z ]*$", "Please enter alphabets only")
            .min(3, "First name must contains atleast 3 characters")
            .max(50, "Max length of first name is 50 charcters"),
        last_name: yup.string()
            .required("Last name is required")
            .matches("^[a-zA-Z ]*$", "Please enter alphabets only")
            .min(3, "Last name must contains atleast 3 characters")
            .max(50, "Max length of last name is 50 charcters"),
        company_name: yup.string()
            .required("Company name is required")
            .min(2, "Company name must contains atleast 3 characters")
            .max(200, "Max length of company name is 200 charcters"),
        city: yup.string()
            .required("City is required")
            .min(3, "City must contains atleast 3 characters")
            .max(200, "Max length of city is 200 charcters"),
        zipcode: yup.string().required("Zipcode is required"),
        country: yup.string().required("Country is required"),
        phone_number: yup.string().required("Phone number is required"),
    });
    const [isProcessing, setProcessingTo] = useState(false);
    const [userInfo, setUserInfo] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {
            id: props.profile._id ? props.profile._id : '',
            first_name: props.profile.first_name  ? props.profile.first_name  : '',
            last_name: props.profile.last_name  ? props.profile.last_name  : '',
            city: props.profile.city ? props.profile.city : '',
            company_name: props.profile.company_name ?  props.profile.company_name : '',                                    
            city: props.profile.city ? props.profile.city : '',
            country: props.profile.country ? props.profile.country : '',
            zipcode: props.profile.zipcode ? props.profile.zipcode : '',
            phone_number: props.profile.phone_number ? props.profile.phone_number : '',
        }
    );
    const [serverError, setServerError] = useState('');
    const [success, setSuccess] = useState('');
    const { register, handleSubmit, errors } = useForm({
        resolver: yupResolver(schema)
    });
    const handleProfileChange = evt => {
        const name = evt.target.name;
        const newValue = evt.target.value;      
        setUserInfo({[name]: newValue});            
    }
    const onSubmit = async (data, e) => {
        setProcessingTo(true);  
        setSuccess('');
        setServerError(''); 
        const userData = {
            _id: data._id ? data._id : '',
            first_name: data.first_name ? data.first_name : '',
            last_name: data.last_name ? data.last_name : '',
            company_name: data.company_name ? data.company_name : '',
            city: data.city ? data.city : '',
            zipcode: data.zipcode ? data.zipcode : '',
            phone_number: data.phone_number ?  data.phone_number : '',
            country: data.country ? data.country : ''
        };
        try {
            const response = await authServices.updateProfile(userData);  
            const data = response.data
            if(response.status == 200 && response.data.status == 1) {
                dispatch({ type: SET_PROFILE, payload: {
                    profile: data.data
                }})   
                setSuccess(response.data.message)  ;
                setServerError(''); 
            } else {
                setServerError(response.data.message);
                setSuccess('');
            }
            setProcessingTo(false);           
        } catch (error) {
            setProcessingTo(false);
        }

    };
    useEffect( ()=> {
        setSuccess('');
        setServerError(''); 
    },[props.show])
    
    return (
        <Modal
            show ={ props.show }
            onHide = {props.onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            className="edit-profile-modal"
        > 
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
            { props.profileProcessing == true ? (
                <BlockUi blocking={ props.profileProcessing }>Loading..</BlockUi>
            ) : (
            <BlockUi tag="div" blocking={ isProcessing } >   
            <Form method="post" className="profile-form" onSubmit={handleSubmit(onSubmit)}>                
                <Container>
                    <Row>
                        <Col sm={12}><h4>Update Profile</h4>
                        { serverError && <p className="red">{serverError}</p> }
                        { success && <p className="green">{success}</p> }
                        </Col>                    
                        <Col sm= {6}>

                            <div className="profile-form-group">
                            <input
                                name = "_id"
                                placeholder="First Name"
                                id="_id"
                                type="hidden"                               
                                className="profile-input"
 
                                defaultValue= { props.profile._id}
                                onChange={handleProfileChange} 
                                ref={register}
                            />
                            <input
                                name = "first_name"
                                placeholder="First Name"
                                id="first_name"
                                type="text"                               
                                className="profile-input"
                                
                                defaultValue= { props.profile.first_name}
                                onChange={handleProfileChange} 
                                ref={register}
                            />
                            <p className="red">{errors.first_name?.message}</p>
                            </div>
                        </Col>
                        <Col sm= {6}>
                            <div className="profile-form-group">
                            <input
                                name = "last_name"
                                placeholder="Last Name"
                                id="last_name"
                                type="text"                               
                                className="profile-input"
                                defaultValue= { props.profile.last_name}
                                onChange={handleProfileChange} 
                                ref={register}
                            />
                            <p className="red">{errors.last_name?.message}</p>
                            </div>
                        </Col>
                        <Col sm= {12}>
                            <div className="profile-form-group">
                            <input
                                name = "company_name"
                                placeholder="Company Name"
                                id="company_name"
                                type="text"     
                                defaultValue= { props.profile.company_name}                          
                                className="profile-input"
                                onChange={handleProfileChange} 
                                ref={register}
                            />
                            <p className="red">{errors.company_name?.message}</p>
                            </div>
                        </Col>
                        <Col sm= {12}>
                            <div className="profile-form-group">
                            <input
                                name = "city"
                                placeholder="City"
                                id="city"
                                type="text" 
                                defaultValue= { props.profile.city}                              
                                className="profile-input"
                                onChange={handleProfileChange} 
                                ref={register}
                            />
                            <p className="red">{errors.city?.message}</p>
                            </div>
                        </Col>
                        <Col sm= {12}>
                            <div className="profile-form-group">
                            <input
                                name = "zipcode"
                                placeholder="Zipcode"
                                id="zipcode"
                                type="text"                               
                                className="profile-input"
                                defaultValue = { props.profile.zipcode}
                                onChange={handleProfileChange} 
                                ref={register}
                            />
                            <p className="red">{errors.zipcode?.message}</p>
                            </div>
                        </Col>
                        <Col sm= {12}>
                        <div className={`profile-form-group`}>
                        <select
                            id="country"                            
                            name="country"
                            className="profile-input"
                            ref={register}
                            defaultValue = { props.profile.country}
                            onChange={handleProfileChange} 
                        >
                            <option key={""} value={""}>
                                Select Country
                            </option>
                            {(Countries || []).map((item) => (
                            <option key={item.value} value={item.value}>
                                {item.label || item.value}
                            </option>
                            ))}
                        </select>
                        <p className="red">{errors.country?.message}</p>
                        </div>
                        </Col>
                        <Col sm= {12}>
                            <div className="profile-form-group">
                            <input
                                name = "phone_number"
                                placeholder="Phone Number"
                                id="phone_number"
                                type="text"                               
                                className="profile-input"
                                defaultValue = { props.profile.phone_number}
                                onChange={handleProfileChange} 
                                ref={register}
                            />
                            <p className="red">{errors.phone_number?.message}</p>
                            </div>
                        </Col>
                        <Col sm= {6}></Col>
                        <Col sm= {6}>
                            <Button className="profile-edit-button" variant="primary" type="submit" disabled={false}>
                                {isProcessing ? "Processing..." : `SUBMIT`}
                                </Button>
                        </Col>
                    
                    </Row>
                </Container>
            </Form>                
            </BlockUi>
            )

            }
            
            </Modal.Body>
            
        </Modal>
    );
}
export default EditProfile;
