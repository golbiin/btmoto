import React, { useState } from "react";
import { Button, Form, Container, Row, Col } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers';
import * as yup from "yup";
import * as authServices from "./../../ApiServices/login";

const RecoveryEmail = (props) => {
    const schema = yup.object().shape({
        _id: yup.string()
            .required("Recovery email is required"),
        recovery_email: yup.string()
            .required("Recovery email is required")
            .email("Please enter valid email")           
    });

    const [isProcessing, setProcessingTo] = useState(false);
    const [recoveryEmail, setRecoveryEmail] = useState('');
    const [serverError, setServerError] = useState('');
    const [success, setSuccess] = useState('');
    const { register, handleSubmit, errors } = useForm({
        resolver: yupResolver(schema)
    });

    const handleChange = evt => {
        const newValue = evt.target.value;      
        setRecoveryEmail(newValue);            
    }

    const onSubmit = async (data, e) => {
        setProcessingTo(true);  
        setSuccess('');
        setServerError(''); 
        const userData = {
            _id : data._id,
            recovery_email: data.recovery_email,
        };
     
        try {
            const response = await authServices.updateRecoveyEmail(userData);  
            if(response.status === 200 && response.data.status === 1) {
                props.getUserDetails();                 
                setSuccess(response.data.message)  ;
                setServerError(''); 
                 
            } else {
                setServerError(response.data.message);
                setSuccess('');
                
            }
            setProcessingTo(false);           
        } catch (error) {

            setProcessingTo(false);
        }

    };



    return (
            <BlockUi tag="div" blocking={ isProcessing } >                    
            <Form className="recovery-email-form" method="post" onSubmit={handleSubmit(onSubmit)} >                
                <Container>
                    <Row>                     
                        <Col className="recovery-email-col" sm= {7}>
                            <div className="profile-form-group">
                            <input
                                name = "_id"
                                id="_id"
                                type="hidden"                               
                                className="email-input"
 
                                defaultValue= { props.profile._id}
                                onChange={handleChange} 
                                ref={register}
                            />
                            <input
                                name = "recovery_email"
                                placeholder="Recovery Email"
                                id="recovery_email"
                                type="text"                               
                                className="email-input"
                                
                                defaultValue= { props.profile.recovery_email}
                                onChange={handleChange} 
                                ref={register}
                            />
                            <p className="red">{errors.recovery_email?.message}</p>
                            </div>
                        </Col>
                        <Col className="recovery-email-col" sm= {5}>
                            <Button className="recovery-email-edit-button" variant="primary" type="submit" disabled={false}>
                                {isProcessing ? "Processing..." : `SUBMIT`}
                                </Button>
                        </Col>
                        <Col className="recovery-email-col" sm={12}>
                        { serverError && <p className="red">{serverError}</p> }
                        { success && <p className="green">{success}</p> }
                        </Col> 
                    </Row>
                </Container>
            </Form>                
            </BlockUi>
    );
}

export default RecoveryEmail;