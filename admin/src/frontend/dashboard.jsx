import React, { Component } from 'react';
import Header from "./common/header";
import Footer from "./common/footer";
import EditProfileImage from "./profile/editProfileImage";
import { displayEditProfile } from './../services/actions/userActions';
import RecoveryEmail from './profile/recoveryEmail';
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import { connect } from "react-redux";
import DeleteConfirm from './deleteConfirm';
import * as OrdersApi from '../ApiServices/orders';
import * as authServices from "../ApiServices/login";
import Joi from "joi-browser";
import memoize from 'memoize-one';
import ReactSpinner from 'react-bootstrap-spinner';
import { CSVLink } from "react-csv";
import { Helmet } from 'react-helmet';
const TITLE = 'Dashboard | Automation Company - Industrial Automation';

class Dashboard extends Component {
  state = {
      profileModalShow: false,
      recoveryEmailShow: false,
      profile: {},
      search: '',
      data: [],
      dataTrash: [],
      contextActions_select: '0',
      contextActionsTrashselect: 'untrash',
      toggleCleared: false,
      spinner: true,
      profileModalImgShow: false,
      profileImg: {},
      upload_data: "",
      profile_image: "",
      sources:"",
      profile_data: null,
      errors: {},
      submit_status: false,
      message: '',
      message_type: '',
      columns: [
        {
          name: 'ID',
          cell: row => (
            <p> {row.order_no} </p>
          ),
          allowOverflow: true,
          button: true,
          width: '60px',
          hide: 'sm'
        },
        {
          name: 'PRODUCT',
          cell: row => (        
              <p>{row.title}</p>              
          ),
          selector: 'title',
          allowOverflow: true,
          sortable: true,            
        },  
        {
          name: 'Email',
          selector: 'email',
          sortable: true,
          cell: row => <div className='order_email'>{row.email}</div>,
          allowOverflow: false,
          left: true
        },    
        {
          name: 'Date',
          selector: 'date',
          sortable: true,
          left: true,
          hide: 'sm',
          maxWidth : '110px'
        },
        {
          name: 'QUANTITY',
          selector: 'qunatity',
          center: true,
          maxWidth : '30px'
        },
        {
          name: 'STATUS',
          selector: 'status',
          cell: row => (
            <i className={'orderstatus ' + row.status}>{row.status}</i>
          ),
          left: true,
          hide: 'sm',
          maxWidth : '50px'
        },
        {
          name: 'PRICE',
          selector: 'price',
          sortable: true,
          left: true,
          hide: 'sm',
          maxWidth : '30px'
        },
        {
          name: 'CANCEL ORDER',
          cell: row => (
            <div>
              <div>
                {row.status === 'Processing' ? (
                  <Link
                    onClick={() => this.saveAndtoogle(row.id)}
                    className='cancel-button'
                  >
                    Cancel
                  </Link>
                ) :  (
                  <Link
                    className='success-button'
                  >
                    {row.status} 
                  </Link>
                )}
              </div>
            </div>
          ),
          allowOverflow: false,
          button: true,
          width: '100px', /* custom width for icon button,*/
          maxWidth : '100px'
        },
        {
          name: 'VIEW',
          cell: row => (
            <div>
              <div>
                  <Link
                    to ={"/view-order/"+row.id}
                    className='cancel-button'
                  >
                    View
                  </Link>
              </div>
            </div>
          ),
          allowOverflow: false,
          button: true,
          width: '100px', /* custom width for icon button,*/
          maxWidth : '100px'
        }
      ]
  }

  isEmpty = (obj) => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };

  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors };
    let upload_data = { ...this.state.upload_data };
    let file = value.target.files[0];
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      upload_data = file;
      this.setState({ upload_data });
    }
  };

  /*upload profile image*/
  updateUser = async () => {
    this.setState({ submitStatus: true });
    let profile = { ...this.state.profile };
    const errors = { ...this.state.errors };
    let user_validate = Joi.validate(profile);
    if (user_validate.error) {      
      if (user_validate.error) {
        let path = user_validate.error.details[0].path[0];
        let errormessage = user_validate.error.details[0].message;
        errors[path] = errormessage;        
      }
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({ errors });
      this.setState({
        submitStatus: false,
        message: "",
        responsetype: "",
      });
      this.setState({ errors });      
    } else {
      if (this.isEmpty(errors)) {
        if (this.state.upload_data) {
          const response1 = await authServices.uploadProfile(
            this.state.upload_data
          );        
          if (response1.data.status === 1) {
            let filepath = response1.data.data.file_location;
            let profile = { ...this.state.profile };
            profile["profile_image"] = filepath;
            this.setState({ profile });
            this.updateUserdata();
          } else {
            window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error",
            });
          }
        } else {
          this.updateUserdata();
        }
      }
    }
  };

  
  updateUserdata = async () => {
    let profile = { ...this.state.profile };    
    const response = await authServices.updateUserdata(profile);
    if (response.data.status === 1 ) {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "success",
      });    
      setTimeout(
        () =>
        this.setState({
          sources: response.data.data.profile_image
        }),
        100
      );
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "error",
      });
    }
  };    
  removecomma (Items ,key){
    if(key === 'title'){
      return Items.replace(',','');
    } else {
      return Items;
    }  
  }
  convertArrayOfObjectsToCSV = array => {
    let result
    const columnDelimiter = ','
    const lineDelimiter = '\n'
    const keys = Object.keys(array[0])
    result = ''
    result += keys.join(columnDelimiter)
    result += lineDelimiter
    array.forEach(item => {
      let ctr = 0
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter
        result += this.removecomma(item[key],key);
        ctr++
      })
      result += lineDelimiter
      
    })
    return result
  }
  downloadCSV = array => {
    const link = document.createElement('a')
    let csv = this.convertArrayOfObjectsToCSV(array)
    if (csv == null) return
    const filename = 'Order.csv'    
    if (!csv.match(/^data:text\/csv/i)) {
      csv = `data:text/csv;charset=utf-8,${csv}`
    }    
    link.setAttribute('href', encodeURI(csv))
    link.setAttribute('download', filename)
    link.click()
  }
  // Trash
  saveAndtoogle = id => {
    this.setState({ index: id })
    this.toogle()
  }
  toogle = () => {
    let status = !this.state.modal
    this.setState({ modal: status })
  }
  cancelOrders = async () => {
    let id = this.state.index
    const response = await OrdersApi.cancelOrders(id)
    
    if (response) {
      if (response.data.status === 1) {
        this.setState({
          message: response.data.message,
          message_type: "success",
        });
        this.toogle()
        this.getAllOrders()
      }
    }
  }   
  searchSpace = event => {
    let keyword = event.target.value    
    this.setState({ search: keyword })
  }
  componentDidMount = () => {
    this.getAllOrders();
    this.getUserDetails();
  }
  getUserDetails = async () => {
    const response =  authServices.getCurrentUser();    
    if(response){
      if(response.email){
          authServices.getUserDetails(response.email)
          .then( async response1 => {
            if(response1){                                
              if(response1.data.data.hasOwnProperty('email')){
                this.setState({profile:response1.data.data});
                this.setState({profileImg:response1.data.data});
              }
            }
          });
      }
    }
  }
  getAllOrders = async () => {
    try {
      this.setState({ spinner: true })
      const response = await OrdersApi.getUserOrders()
      if (response.data.status === 1) {
        const data_table_row = []
        const data_table_row_trash = []
        this.setState({ spinner: false })
        response.data.data.map((item, index) => {
          if (item.page_status === 0) {
            let quantity = 0
            let product_name = ''    
            const SetdataTrash = {}
            SetdataTrash.id = item._id    
            SetdataTrash.status = item.status    
            let date = new Date(item.created_on)
            let year = date.getFullYear()
            let month = date.getMonth() + 1
            let dt = date.getDate()    
            if (dt < 10) {
              dt = '0' + dt
            }
            if (month < 10) {
              month = '0' + month
            }    
            SetdataTrash.date = dt + '-' + month + '-' + year    
            item.order_details.product.map((item1, index) => {
              
              if (item1.product_name) {
                product_name += item1.product_name + ' ,'

                if (item1.quantity) {
                  quantity =
                    parseInt(quantity, 10) + parseInt(item1.quantity, 10)
                }
              }
            })    
            SetdataTrash.title = product_name.replace(/.$/,"")
            SetdataTrash.qunatity = quantity
            SetdataTrash.email = item.shipping_details.email
            SetdataTrash.price = item.order_details.total.total
            SetdataTrash.page_status = item.page_status
            SetdataTrash.order_no =  item.order_no ? item.order_no : '' 
            data_table_row_trash.push(SetdataTrash)
          } else {
            let quantity =  0
            let refundvalue = 0
            let product_name = ''    
            const Setdata = {}
            Setdata.id = item._id    
            Setdata.status = item.status    
            let date = new Date(item.created_on)
            let year = date.getFullYear()
            let month = date.getMonth() + 1
            let dt = date.getDate()    
            if (dt < 10) {
              dt = '0' + dt
            }
            if (month < 10) {
              month = '0' + month
            }    
            Setdata.date = dt + '-' + month + '-' + year    
            item.order_details.product.map((item1, index) => {
              
              if (item1.product_name) {
                product_name += item1.product_name + ', '    
                if (item1.quantity) {
                  quantity =
                    parseInt(quantity, 10) + parseInt(item1.quantity, 10)
                }
              }
            })
            refundvalue = 
            ( item.order_details.refund.length > 0 ?
              ( item.order_details.refund.reduce((refundvalue, Qty) => refundvalue * 1 + (Qty.other.amount * 1) / 100, 0)
              ) : 0
            )
            Setdata.title = product_name.replace(/, $/,"")
            Setdata.qunatity = quantity
            Setdata.email = item.shipping_details.email
            Setdata.price =  (item.order_details.total.total - refundvalue).toFixed(2)              
            Setdata.page_status = item.page_status
            Setdata.order_no =  item.order_no ? item.order_no : '' 
            data_table_row.push(Setdata)
          }
        })
        this.setState({ dataTrash: data_table_row_trash })
        this.setState({ data: data_table_row })
        this.setState({ spinner: false })
      } else {
        this.setState({ spinner: false })
      }
    } catch (err) {    
      this.setState({ spinner: false })    
    }
  }
  actionHandlerOrder = async (Orderids, contextActions, status) => {
    try {
      const response = await OrdersApi.actionHandlerOrder(
        Orderids,
        contextActions,
        status
      )
      if (response.data.status === 1) {
        this.setState({
          message: response.data.message,
          message_type: 'success'
        })    
        this.getAllOrders()
      } else {
        this.setState({
          message: response.data.message,
          message_type: 'error'
        })
        this.getAllOrders()
      }
    } catch (err) {
      this.setState({
        message: 'Something went wrong',
        message_type: 'error'
      })
    }
  }    
  actionTrashHandlerOrder = async (Orderids, contextActions) => {
    try {
      const response = await OrdersApi.actionTrashHandlerOrder(
        Orderids,
        contextActions
      )
      if (response.data.status === 1) {
        this.setState({
          message: response.data.message,
          message_type: 'success'
        })

        this.getAllOrders()
      } else {
        this.setState({
          message: response.data.message,
          message_type: 'error'
        })
        this.getAllOrders()
      }
    } catch (err) {
      this.setState({
        message: 'Something went wrong',
        message_type: 'error'
      })
    }
  }
  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows })    
  }
  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value })
  }    
  handlecontextActionsTrash = async e => {
    this.setState({ contextActionsTrashselect: e.target.value })
  }    
  render () {
    const images = require.context("../assets/images", true);
    const contextActions = memoize(actionHandler => (
      <div className='common_action_section'>
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value='0'>Trash</option>
        </select>
        <button className='danger' onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ))

    const contextActionsTrash = memoize(actionHandler => (
      <div className='common_action_section'>
        <select
          value={this.state.contextActionsTrashselect}
          onChange={this.handlecontextActionsTrash}
        >
          <option value='untrash'>Restore</option>
          <option value='delete'>Delete Permanently</option>
        </select>
        <button className='danger' onClick={this.actionTRashHandler}>
          Apply
        </button>
      </div>
    ))

    let rowData = this.state.data
    let rowDataTrash = this.state.dataTrash

    let search = this.state.search

    if (search.length > 0) {
      search = search.trim().toLowerCase()
      rowData = rowData.filter(l => {
        return (
          l.title.toLowerCase().match(search) ||
          l.status.toLowerCase().match(search) ||
          l.email.toLowerCase().match(search)
        )
      })
      rowDataTrash = rowDataTrash.filter(l => {
        return (
          l.title.toLowerCase().match(search) ||
          l.status.toLowerCase().match(search) ||
          l.email.toLowerCase().match(search)
        )
      })
    }

    return (
      <React.Fragment>
      <Helmet>
      <title>{ TITLE }</title>
      </Helmet>
        <Header />
        <div id="contact-us" className='container-fluid'>
        <div className="contact-banner">
          <div className="cnt_banner_bg">
            <div className="abg"></div>
            <div className="container">
              <div className="cnthedng">
                <h1>DASHBOARD</h1>
              </div>
            </div>
          </div>
        </div>
        <div className='row dashboard-row'>
            <div className='col-md-3 col-sm-12 dashboard-sidebar'>
                <div className='profile-top-section'>
                    <div className='profile-image'>

                    <img alt="profile picture" className="" src={this.state.profile.profile_image ? 
                    this.props.authUser.profile_image : images(`./frontend/profile-dummy.png`)}></img>

                    
                    </div>
                    <div className='profile-desc'>
                    <h3>Welcome, {this.props.authUser.first_name}</h3>
                    </div> 
                    <div className="change-pro-img">
                    <Link to="#" onClick={() => this.setState({profileModalImgShow:true})} >Change Profile Photo</Link>
                    </div>


                </div>
                <div className='profile-bottom-section'>
                    <h2>PROFILE INFORMATION</h2>
                    <div className='edit-icon'>
                    <div className=''>
                      <p>{this.props.profile.first_name} {this.state.profile.last_name}</p>
                      <p>{this.props.profile.phone_number}</p>
                      <p>{this.props.profile.company_name}</p>
                      <p>{this.props.profile.country}</p>
                    </div>
                    <Link to="#" onClick={() => {
                      this.props.displayEditProfile(true);
                    }} ><img alt="profile edit" src={images(`./frontend/Pencil_Icon.png`)} className=""></img></Link>
                    </div>
                    <h4 className="recovery-h4">Add Recovery Email:</h4> 
                    <div className='edit-icon recovery-email'>
                    
                    <div className=''>
                      {this.state.recoveryEmailShow === false && <p>{this.state.profile.recovery_email}</p> }
                      {this.state.recoveryEmailShow === true && 
                      <RecoveryEmail
                      show={this.state.recoveryEmailShow}
                      onHide={() => this.setState({recoveryEmailShow:false})}
                      profile = { this.state.profile }
                      getUserDetails = { this.getUserDetails }
                      ></RecoveryEmail>

                      }
                      
                    </div>
                    {this.state.recoveryEmailShow === false && 
                    <Link to="#" onClick={() => this.setState({recoveryEmailShow:true})} >
                      <img alt="recovery email edit" src={images(`./frontend/Pencil_Icon.png`)} className=""></img>
                    </Link>
                    }
                    {this.state.recoveryEmailShow === true && 
                    <Link to="#" onClick={() => this.setState({recoveryEmailShow:false})} >
                      <img alt="recovery email close" src={images(`./frontend/newclose.png`)} className=""></img>
                    </Link>
                    }
                    </div>
                </div>
            </div>
            <div className='col-md-9 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  
                </div>
                <div className='col-md-12 contents pt-4 pb-4 pr-4'>
                  <div className='main_admin_page_common dashboard-right-sec'>
                    <div className=''>
                      <div className='admin_breadcum'>
                        <div className='row'>
                          <div className='col-md-3'>
                            <p className='page-title'>ORDER DETAILS</p>
                          </div>
                          
                            <div className='col-md-4 btn-export-data'>  
                            {this.state.data ? this.state.data.length > 0 ?
                            <CSVLink
                            filename={"Order.csv"}
                            className="Export_data_btn"
                            data={rowData}> Export Data
                          </CSVLink>
                            : "":""
                            }
                            </div>

                          <div className='col-md-5'>
                            <div className='searchbox'>
                              <div className='commonserachform'>
                                <span />
                                <input
                                  type='text'
                                  placeholder='Search'
                                  onChange={e => this.searchSpace(e)}
                                  name='search'
                                  className='search form-control'
                                />
                                <input type='submit' className='submit_form' />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className=''>
                      

                        <div
                          style={{
                            display:
                              this.state.spinner === true
                                ? 'flex justify-content-center '
                                : 'none'
                          }}
                          className='overlay text-center'
                        >
                          <ReactSpinner
                            type='border'
                            color='primary'
                            size='10'
                          />
                        </div>
                        <div className='col-xl-12 col-md-12 col-sm-12 p-0'>
                          <div className='tab-content'>
                            {this.state.message !== '' ? (
                              <p className='tableinformation'>
                                <div className={this.state.message_type}>
                                  <p>{this.state.message}</p>
                                </div>
                              </p>
                            ) : null}
                            <div className='tab-pane active' id='tab_a'>
                              <DataTable
                                columns={this.state.columns}
                                data={rowData}
                                selectableRows 
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                clearSelectedRows={this.state.toggleCleared}
                                onSelectedRowsChange={this.handlerowChange}
                                contextActions={contextActions(this.deleteAll)}
                                paginationPerPage = {30}
                                paginationRowsPerPageOptions = {[30, 50, 75, 100]}
                                noDataComponent = {<p>There are currently no records.</p>}
                              />
                            </div>
                            {rowData.length === 0 && (
                              <div className="display-msg">You have currently not placed an order.</div>
                            )}

                            <div className='tab-pane' id='tab_b'>
                              <DataTable
                                columns={this.state.columns}
                                  data={rowDataTrash}
                                selectableRows
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                clearSelectedRows={this.state.toggleCleared}
                                onSelectedRowsChange={this.handlerowChange}
                                contextActions={contextActionsTrash(
                                  this.deleteAll
                                )}
                                paginationPerPage = {30}
                                paginationRowsPerPageOptions = {[30, 50, 75, 100]}
                                noDataComponent = {<p>There are currently no records.</p>}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          cancelOrders={this.cancelOrders}
          message='Are you sure want to Cancel Order?'
          name='Cancel Order'
        />

        <EditProfileImage
          show={this.state.profileModalImgShow}
          onHide={() => this.setState({profileModalImgShow:false})}
          profileImg = { this.state.profileImg }
          getUserDetails = { this.getUserDetails}
          onuplaodProfile={this.onuplaodProfile}
          value={this.state.profileImg.profile_image}
          error={this.state.errors}
          updateUser={this.updateUser}
          message_type={this.state.message_type}
          message={this.state.message}
          submit_status={this.state.submit_status} 
        />
                      
          <Footer />
      </React.Fragment>
    )
  }
}
const mapStateToProps = (state) => ({
  token: state.auth.token,
  authUser: state.auth.authUser,
  profile: state.auth.profile
});
export default connect(
  mapStateToProps,
  {  displayEditProfile }
) (Dashboard);

