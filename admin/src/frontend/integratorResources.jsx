import React, { Component } from 'react';
import { Container, Row, Col} from 'react-bootstrap';
import Header from "./common/header";
import Footer from "./common/footer";
import Productbanner from './common/productBanner';
import * as resourcesService from "../ApiServices/resources";
import * as ProductsService from "../ApiServices/products";
import { connect } from "react-redux";
import Moment from 'react-moment';
import { Helmet } from 'react-helmet';
const TITLE = 'Integrator Resources | Automation Company - Industrial Automation';

class integratorResources extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
        searchFile: "",
        blocking: false,
        softwareData:[],
        imageData:[],
        documentData:[],
        productBdata: {
            title: "INTEGRATOR RESOURCES",
            backgroundImage: "resources.png",
          },
          message: "",
          status: "",
          errors: {},
          productslist:[],
          categorylist:[],         
        };
        this.myRef = React.createRef();
    }

componentDidMount = () => {
  this.getResources();
  this.getAllProductsIntegrator(); 
  this.getAllCategoryIntegrator(); 
};
processResources = (response) => {
  let softwareInfo = [];
  let imageInfo = [];
  let documentInfo =[];
  response.data.resourcesDetails.map((data, index) => {

    if(data.resource_category === 'software' && data.resource_type === 'integrator') {
      softwareInfo.push (
        <tr  key={index}>
          <td className="resources-left"><a download href={data.file_url}>{data.filename}</a></td>
          <td className="resources-center">{data.version}</td>
          <td className="resources-right date-box">
          <Moment format="MMMM D, YYYY">{data.createdon}</Moment>         
          </td>
        </tr>
      );
    } 
    if(data.resource_category === 'image' && data.resource_type === 'integrator') {
      imageInfo.push (
        <tr  key={index}>
          <td className="resources-left"><a download href={data.file_url}>{data.filename}</a></td>
          <td className="resources-right date-box">
            <Moment format="MMMM D, YYYY">{data.createdon}</Moment>         
            </td>
        </tr>
      );
    }
    if(data.resource_category === 'document' && data.resource_type === 'integrator') {
      documentInfo.push (
        <tr  key={index}>
          <td className="resources-left"><a download href={data.file_url}>{data.filename}</a></td>
          <td className="resources-right date-box">
            <Moment format="MMMM D, YYYY">{data.createdon}</Moment>         
          </td>
        </tr>
      );
    }

  })
  this.setState({ softwareData: softwareInfo });
  this.setState({ imageData: imageInfo });
  this.setState({ documentData: documentInfo });
}

/* Get all Integrator Resources */
getResources = async () => {
  this.setState({ blocking: true });
  try {
    const response = await resourcesService.getResources(
      this.state.productId, this.state.categoryId,this.state.searchFile, this.props.token);
    if (response.status === 200) {
      if (response.data.status === 1) {

        this.processResources(response);
      } else {
        this.resetResources();
      }
    } else {
      this.resetResources();
    }
  } catch (err) {
    this.resetResources();
  }
};
resetResources = () => {
  this.setState({ blocking: false });

  this.setState({ softwareData: [] });
  this.setState({ imageData: [] });
  this.setState({ documentData: [] });
  this.setState({message: "Sorry! No records found."} );
}

/* Get all Products for integrator Resources */
getAllProductsIntegrator = async () => {
  this.setState({ blocking: true});
  try {
    const response =  await ProductsService.getAllProductsIntegrator();
    let productsDetails = response.data.data;
      if(productsDetails){
        let productslist = productsDetails
        this.setState({productslist})  
      }
  } catch (err) {
      this.setState({ blocking: false});
  }
}

/* Get all Categories for integrator Resources  */
getAllCategoryIntegrator = async () => {
  this.setState({ blocking: true});
  try {
    const response =  await ProductsService.getAllCategoryIntegrator();
    let categoryDetails = response.data.data;
      if(categoryDetails){
        let categorylist = categoryDetails
      this.setState({categorylist})  
    }
  } catch (err) {
      this.setState({ blocking: false});
  }
}

/* handle chnage serach */
handleFileChange  = async (e) => {
  const filename = e.target.value;
  this.setState({ searchFile: filename}, () => {
    this.getResources()
  });  
}

/* handle category select change */
handleCatChange  = async (e) => {
  const catId = e.target.value;
  this.setState({ categoryId: catId}, () => {
    this.getResources()
  });
}

/* handle product select change */
handleProdChange  = async (e) => {
  const pid = e.target.value;
  this.setState({ productId: pid}, () => {
    this.getResources()
  });
}
        
    render() { 
        const { productBdata, productslist, categorylist,  
        softwareData, imageData, documentData } = this.state;
        const images = require.context("../assets/images", true);
        return ( 
            <React.Fragment>
            <Helmet>
            <title>{ TITLE }</title>
            </Helmet>
             <Header />
             <div id="product-subcategory" className="resources integrator-resources-page">
                <div className="center" >
                  <Productbanner  productbannerInfo={productBdata} />
                </div>
                <Container>
                  <div className="search">
                    <h2>Search</h2>
                    <div className="resources-searchby">
                      <input type="text" placeholder="Search by keywords" value={this.state.searchFile} onChange={this.handleFileChange}></input>
                       <button></button>
                    </div>
                    <p>or</p>
                    <span>
                      <select className="" value={this.state.productId} onChange={this.handleProdChange}>
                        <option value="">Products</option>
                        {productslist.map((data,index) =>{
                        return (
                        <option value={data._id}>{data.product_name}</option>
                        );
                        })} 
                        </select><span className="slash"> / </span>
                        <select className="" value={this.state.categoryId} onChange={this.handleCatChange}>
                        <option value="">Category</option>
                        {categorylist.map((data,index) =>{
                        return (
                        <option value={data._id}>{data.category_name}</option>
                        );
                        })}
                      </select>
                    </span>
                    <div className="common-resources">
                       <h3>SOFTWARE</h3>
                       <table width= "100%">
                          <thead>
                            <tr>
                              <th className="resources-left">File Name</th>
                              <th className="resources-center">Version</th>
                              <th className="resources-right">Date</th>
                            </tr>
                          </thead>
                          <tbody>
                            {softwareData.length === 0 && 
                            <tr>
                            <td className="resources-left" colSpan="3">{this.state.message}</td>
                            </tr>
                            }
                            {softwareData}
                          </tbody>
                        </table> 
                      </div>
                      <div className="common-resources">
                        <h3>IMAGE FILE</h3>
                        <table width= "100%">
                          <thead>
                            <tr>
                              <th className="resources-left">File Name</th>
                              <th className="resources-right">Date</th>
                            </tr>
                          </thead>
                          <tbody>
                            {imageData.length === 0 && 
                            <tr>
                            <td className="resources-left" colSpan="2">{this.state.message}</td>
                            </tr>
                            }
                            {imageData}
                          </tbody>
                        </table> 
                      </div> 
                      <div className="common-resources">
                       <h3>DOCUMENTS</h3>
                       <table width= "100%">
                          <thead>
                            <tr>
                            <th className="resources-left">File Name</th>
                            <th className="resources-right">Date</th>
                            </tr>
                          </thead>
                          <tbody>
                            {documentData.length === 0 && 
                            <tr>
                            <td className="resources-left" colSpan="2">{this.state.message}</td>
                            </tr>
                            }
                            {documentData}
                          </tbody>
                        </table> 
                      </div>  
                    </div>
                  </Container>
                <div className="tech-support">
                  <Container>
                    <Row>
                      <Col md={6}>
                        <h2>Cannot Find The File You Are Looking For?</h2>
                        <p>Please contact us. Our technical support team is always happy to help answering your question and provide you with an solution.</p>
                      </Col>
                      <Col md={6}>
                          <div className="support-box">
                              <img  alt="distributor resource" src={images(`./frontend/tech-support.png`)}></img>
                              <a href="#">Tech Support</a>
                          </div>
                      </Col>
                    </Row>
                  </Container>
                </div>
             </div>
             <Footer />
            </React.Fragment>
         );
    }
}
const mapStateToProps = (state) => ({
  authUser: state.auth.authUser,    
  token: state.auth.token
});
export default connect(mapStateToProps, { 

})(integratorResources);
