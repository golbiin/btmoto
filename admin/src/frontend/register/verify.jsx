import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import Footer from './../common/footer';
import Header from './../common/header';
import * as loginService from '../../ApiServices/login';
import {Container, Row, Col, Alert} from 'react-bootstrap';
import { showHideLogin } from './../../services/actions/userActions';

const Verify = (props) => {
  const { authUser } = useSelector(state => ({
    authUser: state.auth.authUser,
  }));
  const dispatch = useDispatch();
  const [hasError, setErrors] = useState(false);
  const [status, setStatus] = useState(0);
  const [hasSuccess, setSuccess] = useState(false);
  const [token, setToken] = useState(props.match.params.token);

  async function verifyToken() {
    loginService.verifyToken(token).then( response => {
        setStatus(response.data.status);
        if(response.data.status === 1) {            
            setSuccess(response.data.message);
        } else {
            setErrors(response.data.message);
        }
    }).catch(err => setErrors(err));
  }

  useEffect(() => {
    verifyToken();
  } , [token]) ;

  if(authUser.email) {
    return(
      <Redirect to="/"></Redirect>
    )
  }

  return (  
    <div>
    <Header/>
    <Container >
        <Row>
            <Col sm= {12}>
                <div className="verification-container">
                { (hasError) && (
                <Alert  variant={'danger'}>
                  {hasError} { (status === 2) && (
                    <Link to="#" onClick={() => dispatch(showHideLogin(true, 'login'))}>login</Link>
                  ) }  
                </Alert>)}
                { (hasSuccess) && (
                <Alert  variant={'success'}>
                    {hasSuccess} <Link to="#" onClick={() => dispatch(showHideLogin(true, 'login'))}>login</Link>
                </Alert>)}                      
                </div>
            </Col>
        </Row>
    </Container>
    <Footer/>
    </div> 
  );
};
export default Verify;