import React from "react";
import { Formiz, useForm, FormizStep } from "@formiz/core";
import { isEmail } from "@formiz/validations";
import alertify from "alertifyjs";
import { Container, Row, Col, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FieldInput } from "./fields/FieldInput";
import { FieldSelect } from "./fields/FieldSelect";
import { Countries } from "./../../countries";
import { bussinessType, industry, categories } from "../../config.json";
import { FieldCheckbox } from "./fields/FieldCheckbox";
import * as loginService from "../../ApiServices/login";
import Terms from './terms';
import PrivacyPolicy from './privacyPolicy';

export const RegisterForm = ({ closeModal }) => {
  const regForm = useForm();
  const [isLoading, setIsLoading] = React.useState(false);
  const [showModalTerms, setShowModalTerms] = React.useState(false);
  const [showModalPrivacy, setShowModalPrivacy] = React.useState(false);
  const [error, setError] = React.useState("");
  let [passwordError, setPasswordError] = React.useState("");
  const [interestedProducts, setInterestedProducts] = React.useState("");

  const submitForm = (values) => {
    setIsLoading(true);
    setError("");
    values.interested_products = interestedProducts;
    loginService.register(values).then((response) => {
      setIsLoading(false);
      regForm.isStepSubmitted = false;
      if (response.status === 200 && response.data.status === 1) {
        closeModal(false, "signup");
        alertify.alert(
          "A CONFIRMATION EMAIL HAS BEEN SENT",
          "<p>A confirmation email has been sent.</p><p>Please go to your inbox and click the confirmation link to activate your account.</p>"
        );
      } else if (response.data.status === 2) {
        regForm.invalidateFields({
          email: response.data.message,
        });
        const step = regForm.getFieldStepName("email");
        regForm.goToStep(step);
      } else {
        setError(response.data.message);
      }
    })
    .catch((err) => {});
  };

  const agreeTermsLabel = () => {
    return(
      <p>I agree to the <Link to="#" onClick={ () => setShowModalTerms(true)} >Terms of Service</Link> and 
      <Link to="#" onClick={ () => setShowModalPrivacy(true)}> Privacy Policy</Link></p>
    )
  }

  return (
    <Container className="register-form-block">
      <Formiz onValidSubmit={submitForm} connect={regForm}>
        <form
          noValidate
          onSubmit={regForm.submitStep}
          className="register-form"
        >
          <div className="register-form__content">
            <FormizStep name="step1">
              <Row>
                <Col sm={6}>
                  <FieldInput
                    name="first_name"
                    label="First Name*"
                    required="Please enter your First Name"
                  />
                </Col>
                <Col sm={6}>
                  <FieldInput
                    name="last_name"
                    label="Last Name*"
                    required="Please enter your Last Name"
                  />
                </Col>
              </Row>
              <Row>
                <Col sm={12}>
                  <FieldInput
                    name="email"
                    label="Email*"
                    required="Your email is required"
                    validations={[
                      {
                        rule: isEmail(),
                        message: "Not a valid email",
                      },
                    ]}
                  />
                </Col>
              </Row>
              <Row>
                <Col sm={12}>
                  <FieldInput
                    name="password"
                    label="Password*"
                    type="password"
                    required="A password is required."
                    validations={[
                      {
                        rule: (value) => {
                          if (!value) {
                            setPasswordError("A password is required.");
                            return false;
                          }
                          if (value && value.length <= 7) {
                            setPasswordError("The “Password” length must be at least 8 characters long.");
                            return false;
                          }

                          if (!value.match(/[a-zA-Z]/)) {
                            setPasswordError(
                              "Password should contain a letter"
                            );
                            return false;
                          }
                          if (!value.match(/[0-9]/)) {
                            setPasswordError(
                              "Password should contain a number"
                            );
                            return false;
                          }
                          if (!value.match(/[!@#$%^&*()?/]/)) {
                            setPasswordError(
                              "Password should contain a Special character"
                            );
                            return false;
                          }

                          return true;
                        },
                        deps: [regForm.values.password],
                        message: passwordError,
                      },
                    ]}
                  />
                </Col>
              </Row>
              <Row>
                <Col sm={12}>
                  <FieldInput
                    name="confirm_password"
                    label="Confirm Password*"
                    type="password"
                    required="Confirm whether the password matches above."
                    validations={[
                      {
                        rule: (value) => regForm.values.password === value,
                        deps: [regForm.values.password],
                        message: "Passwords do not match",
                      },
                    ]}
                  />
                </Col>
              </Row>
              <Row>
                <Col sm={12}>
                  <FieldCheckbox
                  type="checkbox"
                    name="agree_terms"
                    label=""
                    required="Please agree to terms of service and privacy policy."
                    options={[
                      {
                        label:agreeTermsLabel(),
                        value: 1,
                      },
                    ]}
                  />
                </Col>
              </Row>
              <Row>
                <Col sm={12}>
                  <FieldCheckbox
                    name="agree_newsletter"
                    label=""
                    options={[
                      {
                        label:
                          "Sign me up for the newsletter to get current news, events and product updates",
                        value: 1,
                      },
                    ]}
                  />
                </Col>
              </Row>
            </FormizStep>
            <FormizStep name="step2">
              <Row>
                <Col sm={12}>
                  <FieldInput
                    name="company_name"
                    label="Company Name*"
                    required="Your company name is required."
                  />
                </Col>
                <Col sm={12}>
                  <FieldSelect
                    // eslint-disable-next-line react/no-array-index-key
                    name="country"
                    label="Country*"
                    required="Please select your country."
                    placeholder="Select Country"
                    defaultValue=""
                    options={Countries}
                  />
                </Col>
                <Col sm={6}>
                  <FieldInput
                    name="city"
                    label="City*"
                    required="Please enter your city."
                  />
                </Col>
                <Col sm={6}></Col>
                <Col sm={6}>
                  <FieldInput
                   name="zipcode"
                   label="Zip Code*" 
                   required="Please enter your ZIP code."
                   />
                </Col>
                <Col sm={6}></Col>
                <Col sm={6}>
                  <FieldInput
                    type="number"
                    name="phone_number"
                    label="Phone Number*"
                    required="Please enter your phone number."
                  />
                </Col>
                <Col sm={6}></Col>
                <Col sm={12}>
                  <div className={`register-form-group`}>
                    <label>
                      {"Which CIMON products are you interested in? *"}
                    </label>
                    {(categories || []).map((item) => (
                      <Form.Check
                        name={"selected_products"}
                        type={"checkbox"}
                        value={item.value ?? ""}
                        label={item.label}
                        className="register-checkbox"
                        onChange={(e) => {
                          const selectedProducts = [];
                          categories.forEach((category) => {
                            if (category.value === e.target.value) {
                              category.checked = e.target.checked;
                            }
                            if (category.checked) {
                              selectedProducts.push(category.value);
                            }
                          });
                          const p = selectedProducts.join(",");
                          setInterestedProducts(p);
                        }}
                      />
                    ))}
                  </div>
                </Col>
                <Col sm={12}>
                  <FieldSelect
                    name="bussiness_type"
                    label="Business Type"
                    defaultValue=""
                    options={bussinessType}
                  />
                </Col>
                <Col sm={12}>
                  <FieldSelect
                    name="industry"
                    label="Industry"
                    defaultValue=""
                    options={industry}
                  />
                </Col>
                <Col sm={12}>
                  <FieldInput name="recovery_email" label="Recovery Email "
                   validations={[
                    {
                      rule: (value) =>
                        regForm.values.email !== value,
                      deps: [regForm.values.email],
                      message: "Registration Email should not be the same as the Recovery Email",
                    },
                  ]} />
                </Col>
                <Col sm={12}>
                  <FieldInput
                    name="confirm_recovery_email"
                    label="Confirm Recovery Email "
                    validations={[
                      {
                        rule: (value) =>
                          regForm.values.recovery_email === value,
                        deps: [regForm.values.recovery_email],
                        message: "Recovery Email do not match",
                      },
                    ]}
                  />
                </Col>
              </Row>
            </FormizStep>
          </div>

          <div className="register-form__footer">
            <Row>
              <Col className="is_error red">
                <p>{error}</p>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                {!regForm.isFirstStep && (
                  <button
                    className="register-button register-prev-btn"
                    type="button"
                    onClick={regForm.prevStep}
                  >
                    Previous
                  </button>
                )}
              </Col>
              <Col sm={6}>
                {regForm.isLastStep ? (
                  <button
                    className="register-button register-next-button"
                    type="submit"
                    disabled={
                      isLoading || (!regForm.isValid && regForm.isStepSubmitted)
                    }
                  >
                    {isLoading ? "Loading..." : "Submit"}
                  </button>
                ) : (
                  <button
                    className="register-button register-next-button"
                    type="submit"
                    disabled={!regForm.isStepValid && regForm.isStepSubmitted}
                  >
                    Next
                  </button>
                )}
              </Col>
            </Row>
          </div>
        </form>
      </Formiz>
      <Terms
      show={showModalTerms}
      onHide={() => setShowModalTerms(false)}
      />
      <PrivacyPolicy
      show={showModalPrivacy}
      onHide={() => setShowModalPrivacy(false)}
      />
    </Container>
  );
};
