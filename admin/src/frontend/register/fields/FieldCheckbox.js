import React from 'react'
import { useField } from '@formiz/core'
import {Form} from 'react-bootstrap';

export const FieldCheckbox = (props) => {
  const {   
    errorMessage,
    id,
    isValid,
    isSubmitted,
    setValue,
    value,
  } = useField(props)
  const { label, type, required , options, name} = props
  const [isTouched, setIsTouched] = React.useState(false)
  const [isChecked, setIsChecked] = React.useState(false)
  const showError = !isValid && (isTouched || isSubmitted)

  return (
    <div className={`register-form-group ${(showError) ? 'is-error' : ''}`}>
        <label>{label}</label>
        {(options || []).map((item) => (

        <Form.Check 
        name={name}
        id={name}
        type={ 'checkbox'}
        value={item.value ?? ''}
        label={item.label} 
        className="register-checkbox"
        checked ={isChecked}
        onChange={e => {          
          setIsChecked(!isChecked);
          if( !isChecked ) {
            setValue(e.target.value)
          } else {
            setValue('');
          }          
        }}
        onBlur={() => setIsTouched(true)}
        aria-invalid={showError}
        aria-required={!!required}
        aria-describedby={showError ? `${id}-error` : null}        
        />
    ))}
      {showError && (
        <div id={`${id}-error`} className="register-form-feedback">
          { errorMessage }
        </div>
      )}
    </div>
  )
}