import React from 'react'
import { useField } from '@formiz/core'

export const FieldInput = (props) => {
  const {
    errorMessage,
    id,
    isValid,
    isSubmitted,
    setValue,
    value,
  } = useField(props)

  
  const { label, type, required, name, defaultValue } = props
  const [isTouched, setIsTouched] = React.useState(false)
  const showError = !isValid && (isTouched || isSubmitted)

  return (
    <div className={`register-form-group ${(showError) ? 'is-error' : ''}`}>
      <input
       name = {name}
        placeholder={label}
        id={id}
        type={type || 'text'}
        defaultValue = {defaultValue ?? ''}
        value={value ?? ''}
        className="register-input"
        onChange={e => {
            setValue(e.target.value)
        }}
        onBlur={() => setIsTouched(true)}
        aria-invalid={showError}
        aria-required={!!required}
        aria-describedby={showError ? `${id}-error` : null}
      />
      {showError && (
        <div id={`${id}-error`} className="register-form-feedback">
          { errorMessage }
        </div>
      )}
    </div>
  )
}