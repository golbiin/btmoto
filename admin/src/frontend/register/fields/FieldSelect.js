import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useField, fieldPropTypes, fieldDefaultProps } from '@formiz/core';

const propTypes = {
  label: PropTypes.node,
  placeholder: PropTypes.string,
  helper: PropTypes.node,
  options: PropTypes.array,
  ...fieldPropTypes,
};
const defaultProps = {
  label: '',
  placeholder: '',
  helper: '',
  options: [],
  ...fieldDefaultProps,
};

export const FieldSelect = (props) => {
  const {
    errorMessage,
    id,
    isValid,
    isSubmitted,
    resetKey,
    setValue,
    value,
  } = useField(props);
  const {
    children, label, options, placeholder, 
  } = props;
  const [isTouched, setIsTouched] = useState(false);
  const showError = !isValid && (isTouched || isSubmitted);

  useEffect(() => {
    setIsTouched(false);
  }, [resetKey]);

  return (
    <div className={`register-form-group ${(showError) ? 'is-error' : ''}`}>
      <select
        id={id}
        key={resetKey}
        value={value || ''}
        onBlur={() => setIsTouched(true)}
        aria-invalid={showError}
        aria-describedby={!isValid ? `${id}-error` : null}
        placeholder={placeholder}
        onChange={(e) => setValue(e.target.value)}
        className="register-select"
      >
        <option key={""} value={""}>
            Select {label}
          </option>
        {(options || []).map((item) => (
          <option key={item.value} value={item.value}>
            {item.label || item.value}
          </option>
        ))}
      </select>
      {showError && (
        <div id={`${id}-error`} className="register-form-feedback">
          { errorMessage }
        </div>
      )}
      {children}
      </div>
  );
};

FieldSelect.propTypes = propTypes;
FieldSelect.defaultProps = defaultProps;
