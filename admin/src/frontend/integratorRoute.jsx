import React from "react";
import { Route, Redirect } from "react-router-dom";
import * as authServices from "../ApiServices/login";
import { USER_TYPES } from './../config.json';
const IntegratorRoutea = ({ component: Component, render, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {

        const authUser = authServices.getCurrentUser();
        if (authUser && authUser.usertype == USER_TYPES.INTEGRATOR)
            return Component ? <Component {...props} /> : render(props);
        return <Redirect to="/integrator-portal" />;
        
      }}
    />
  );
};

export default IntegratorRoutea;
