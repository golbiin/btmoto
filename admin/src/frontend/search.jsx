import React, { useState, useEffect } from 'react';
import BlockUi from 'react-block-ui';
import Header from "./common/header";
import Footer from "./common/footer";
import SubFooter from "./common/subFooter";
import { Link } from "react-router-dom";
import { Container, Row, Col } from 'react-bootstrap';
import * as newsService from './../ApiServices/news';

const Search = (props) => {
  const [term, setTerm] = useState(props.match.params.s);
  const [blocking, setBlocking] = useState(false);
  const [ productSearch, setProductSearch] = useState([]);
  const [ newsSearch, setNewsSearch] = useState([]);
  const [searchError, setSearchError] = useState('');
  const getSearchResults = async () => {
    setBlocking(true);
    const response = await newsService.getSearchResults(term);
    if(response.status == 200) {
      if(response.data.status == 1) {
        const results = response.data.search;
        if(results.products.length == 0 && results.news.length == 0) {
          setSearchError(' Sorry! no results found');
        }
        setProductSearch(results.products);
        setNewsSearch(results.news);
      } else {
        setSearchError(' Sorry! no results found');
      }
    } else {
      setSearchError(' Sorry! no results found');
    }
    setBlocking(false);
  }
  useEffect(() => {
    getSearchResults();
  }, []);
  return (
    <React.Fragment >
      <Header/>
      <div className="search-page">
      <div className="bannerWrapper">
        <div className="sip_banner_bg">
          <div className="sbg"></div>
          <div className="container">
            <div className="hedng">
              <h1>SEARCH</h1>
            </div>
            {/* <Link
              to="#"
              className="scroll-down-link"
            ></Link> */}
          </div>
        </div>
      </div>
      <Container className="search-results">
        <Row>
        <BlockUi tag="div" blocking={blocking} >
          <Col xs={12}><h3 className="search-title">Search results for {term}</h3></Col>
          { searchError !== '' && <Col><h4>{searchError}</h4></Col>}
          {productSearch.length > 0 &&
          <div>
            {productSearch.map((product, index) => { 
              let url , productUrl = '/';
              if(product.url.indexOf("scada") !== -1){
                  productUrl =  '/introduction/scada'
              } else{
                productUrl = product.url+product.slug
              }
                return(
                  <Col xs={12} md={12} className="search-item">
                    <h4>
                      <Link to={productUrl}>{product.product_name}</Link>
                    </h4>
                    <div dangerouslySetInnerHTML={{__html: (product.short_description != null
                                    ? product.short_description
                                    : "") }}>
                      </div>
                  </Col>
                )
            })}

          </div>
          }
          {newsSearch.length > 0 &&
          <div>      
            {newsSearch.map((news, index) => { 
                return(
                  <Col xs={12} md={12} className="search-item">
                    <h4>
                      <Link to={"/latest-news/" + news.slug}>{news.title}</Link>
                    </h4>
                    <div dangerouslySetInnerHTML={{__html: (news.description != null
                                    ? news.description
                                    : "") }}>
                      </div>
                  </Col>
                )
            })}
          </div>
          }
        </BlockUi>
        </Row>
      </Container>
      </div>   
      <SubFooter/>
      <Footer/>
    </React.Fragment>
    
  );
}

export default Search;
