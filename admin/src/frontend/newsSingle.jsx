import React, { Component } from "react";
import {Container,Row,Col} from "react-bootstrap";
import * as newsService from "../ApiServices/news";
import Header from "./common/header";
import Footer from "./common/footer";
import SubFooter from "./common/subFooter";
import { Helmet } from 'react-helmet';


class NewsSingle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newsData: [],
    };
  }

  componentDidMount = () => {
    this.getNewsBySlug();
    window.scrollTo(0, 0);
  };

  /*  Get Single News Details  */
  getNewsBySlug = async () => {
    const news_slug = this.props.match.params.slug;
    const response = await newsService.getNewsBySlug(news_slug);
    if (response.data.status === 1) {
      let newUserarray = {
        title: response.data.data.title,
        image: response.data.data.image,
        date: response.data.data.date,
        content: response.data.data.content,
      };
      this.setState({ newsData: newUserarray });
      this.setState({ spinner: false });
    }
  };

  render() {
    const { newsData } = this.state;
    const news_slug = this.props.match.params.slug;
    
    
    const TITLE = news_slug +' | Automation Company - Industrial Automation';
    return (
      <React.Fragment>
      <Helmet>
      <title>{ TITLE }</title>
      </Helmet>
        <Header />
        <div className="latest-news-single-wrapper">
          <Container className="single-news-container">
            <Row>
              <Col>
                <h2>{newsData.title}</h2>
                <p dangerouslySetInnerHTML={{ __html: newsData.content }} />
              </Col>
            </Row>
            <Row className="single-news-row">
              <Col>
                <p className="author-data">Posted On: March 20, 2020</p>
              </Col>
            </Row>
            <Row className="share-row">
              <Col>
                <label>Share: </label>
                <ul class="social-network social-circle">
                  <li>
                    <a
                      onClick={() =>
                        window.open("https://twitter.com/CIMONsns", "_blank")
                      }
                      class="icoTwitter"
                      title="Twitter"
                    >
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.facebook.com/cimoninc/"
                      target="_blank"
                      class="icoFacebook"
                      title="Facebook"
                    >
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.linkedin.com/company/cimonautomation"
                      target="_blank"
                      class="icoLinkedin"
                      title="Linkedin"
                    >
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.instagram.com/cimon_official/ "
                      target="_blank"
                      class="icoInstagram"
                      title="Instagram"
                    >
                      <i class="fa fa-instagram"></i>
                    </a>
                  </li>
                </ul>
              </Col>
            </Row>
          </Container>
        </div>
        <SubFooter />
        <Footer />
      </React.Fragment>
    );
  }
}

export default NewsSingle;
