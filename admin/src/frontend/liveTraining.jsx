import React, { Component } from "react";
import Header from "./common/header";
import Footer from "./common/footer";
import { Container, Row, Col } from "react-bootstrap";
import Banner from "./common/banner";
import SubFooter from "./common/subFooter";
import { Helmet } from 'react-helmet';
const TITLE = 'Live Training | Automation Company - Industrial Automation'

class LiveTraining extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      getJobsDet: [],
      status: "",
      categoryData: [
        {
          title: "LIVE TRAINING",
          sub_title: "Start Your Automation Training",
          description:
            "Lorem ipsum dolor sit , consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat .",
          image: "livetraining.png",
        },
      ],
      content: {},
     
    };
  }
 
  componentDidMount = async () => {  
  
  };

  render() {
    const { categoryData } = this.state;
    return (
      <React.Fragment>
         <Helmet>
          <title>{ TITLE }</title>
        </Helmet>
        <Header />
        <div className="container-fluid">
          <div id="cimonlivetraning">
            <Banner categoryInfo={categoryData} />
            <Container className="careers">
              <div className="career-title-section">
                <Row className="justify-content-center">
                  <Col
                    md={12}
                    className="text-center"
                    dangerouslySetInnerHTML={{
                      __html: this.state.content.section1
                        ? this.state.content.section1
                        : "",
                    }}
                  ></Col>
                </Row>
              </div>
            </Container>
            <Container className="title-section text-center">
                    <Row>
                        <Col xs={12}>
                           <h2>BOOK YOUR FREE CIMON AUTOMATION HARDWARE & SOFTWARE TRAINING</h2> 
                          <p>Click a date below to book your FREE CIMON Training</p>
                        </Col>
                    </Row>
                </Container>
                <Container className="body-section text-center">
                    <Row>
                        <Col xs={12}>
                            <h3>JULY 2020</h3>
                            
                        </Col>
                        <hr />
                    </Row>
                    <Row className="common_row">
                    <div className="col-md-4 date_container">
                    <p className="date">July 27, <span>Monday</span> </p>
                    <p className="time">8:00-10:00</p>
                    </div>
                    <div className="col-md-4 para_conatiner">CIMON PLC + CICON PLC software training</div>
                    <div className="col-md-4 more_details "><a href="#">More Details</a>
                    <ul class="social-network social-circle">
                      <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="https://www.facebook.com/cimoninc/"  rel="noopener noreferrer" target="_blank" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="https://www.linkedin.com/company/cimonautomation"  rel="noopener noreferrer" target="_blank" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                      <li><a href="mailto:info@cimon.com"  rel="noopener noreferrer" target="_blank" class="icoEmail" title="icoEmail"><i class="fa 
fa-envelope"></i></a></li></ul>
                    </div>
                    </Row>
                    <Row className="common_row">
                    <div className="col-md-4 date_container">
                    <p className="date">July 27, <span>Tuesday</span> </p>
                    <p className="time">8:00-10:00</p>
                    </div>
                    <div className="col-md-4 para_conatiner">CIMON Xpanel HMI + XpanelDesigner software training</div>
                    <div className="col-md-4 more_details "><a>More Details</a>
                    <ul class="social-network social-circle">
                      <li><a class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="https://www.facebook.com/cimoninc/"  rel="noopener noreferrer" target="_blank" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="https://www.linkedin.com/company/cimonautomation"  rel="noopener noreferrer" target="_blank" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                      <li><a href="mailto:info@cimon.com"  rel="noopener noreferrer" target="_blank" class="icoEmail" title="icoEmail"><i class="fa 
fa-envelope"></i></a></li></ul>
                    </div>
                    </Row>
                    <Row className="common_row">
                    <div className="col-md-4 date_container">
                    <p className="date">July 27, <span>Wednesday</span> </p>
                    <p className="time">8:00-10:00</p>
                    </div>
                    <div className="col-md-4 para_conatiner">CIMON IPC and UltimateAccess SCADA training</div>
                    <div className="col-md-4 more_details "><a>More Details</a>
                    <ul class="social-network social-circle">
                      <li><a class="icoTwitter" title="Twitter"  rel="noopener noreferrer"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="https://www.facebook.com/cimoninc/"  rel="noopener noreferrer" target="_blank" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="https://www.linkedin.com/company/cimonautomation"  rel="noopener noreferrer" target="_blank" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                      <li><a href="mailto:info@cimon.com"  rel="noopener noreferrer" target="_blank" class="icoEmail" title="icoEmail"><i class="fa 
fa-envelope"></i></a></li></ul>
                    </div> 
                    </Row>
                </Container>
            <SubFooter />
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

export default LiveTraining;
