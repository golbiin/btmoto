import React, { Component } from "react";
import Header from "./common/header";
import TitleBanner from "./common/titleBanner";
import JobSearch from "./common/jobSearch";
import Footer from "./common/footer";
import { Container, Row, Col } from "react-bootstrap";
import SubFooter from "./common/subFooter";
import BlockUi from "react-block-ui";
import * as careerService from "../ApiServices/careers";
import * as pageService from "../ApiServices/page";
import { Helmet } from 'react-helmet';
const TITLE = 'Career-Center | Automation Company - Industrial Automation';

class CareerCenterSingle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      titleData: [
        {
          title: "CAREER CENTER",
          image: "career-center-back.jpg",
        },
      ],
      jobSearchData: {
        title: "NEW JOB SEARCH",
      },
      getsingleJobsDet: [],
    };
  }

  componentDidMount = () => {
    this.getsinglejobs();
    this.getContent('career-center');
  };
  
  /* Get Page Contents */
  getContent = async (slug) => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        /* Banner-Slider Contents */
        if (response.data.data.content.bannerSlider) {
          const titleData = [...this.state.titleData];
          titleData[0]['title'] = response.data.data.content.bannerSlider[0].heading ? response.data.data.content.bannerSlider[0].heading : '';
          titleData[0]['image'] = response.data.data.content.bannerSlider[0].image ? response.data.data.content.bannerSlider[0].image : 'career-center-back.jpg';
          this.setState({
            titleData: titleData,
          });
        }
        const jobSearchData = { ...this.state.jobSearchData };
        jobSearchData.title = response.data.data.content.title1 ? response.data.data.content.title1 : 'NEW JOB SEARCH';
        this.setState({
          content: response.data.data.content,
          jobSearchData: jobSearchData
        });
      } 
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* Get Job Description */
  getsinglejobs = async () => {
    this.setState({ blocking: true });
    try {
      const career_slug = this.props.match.params.slug;
      const response = await careerService.getsinglejobs(career_slug);
      if (response.status === 200) {
        this.setState({ getsingleJobsDet: response.data.singleDetails });
        this.setState({ blocking: false });
      } else {
        this.props.history.push({
           pathname: "/career-center",
        });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  render() {
    const {titleData,jobSearchData} = this.state;
    return (
      <React.Fragment>
        <Helmet>
        <title>{ TITLE }</title>
        </Helmet>
        <Header />
        <div className="career-center-single-wrapper">
          <TitleBanner bannerInfo={titleData} />
          <JobSearch
            jobSearch={jobSearchData}
            handleChange={this.handleChange}
            propsnew={this.props}
          />
          <div className="main-content">
            <Container>
              <BlockUi tag="div" blocking={this.state.blocking}>
                <div className="title-section">
                  <h2>
                    {this.state.getsingleJobsDet.job_name != null
                      ? this.state.getsingleJobsDet.job_name
                      : ""}
                  </h2>
                </div>
                <Row className="content-section">
                  <Col lg={8}>
                    <p
                      dangerouslySetInnerHTML={{
                        __html:
                          this.state.getsingleJobsDet.content != null
                            ? this.state.getsingleJobsDet.content
                            : "",
                      }}
                    />
                  </Col>
                </Row>
              </BlockUi>
            </Container>
          </div>
          <SubFooter />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default CareerCenterSingle;
