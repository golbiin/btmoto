import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
//import poster from '../../assets/images/frontend/sc-video-bg.png';
import ReactPlayer from "react-player/lazy";
import "../../services/aos";

class VideoV1 extends Component {
  state = {
    show: false
  };
  vaildUrl(url) {
    var expression = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    if (url.match(regex)) {
      return true;
    } else {
      return false;
    }
  }
  playVideo = () => {
    this.setState({ show: true });
  };
  render() {
    const {
      videoInfo,
      section1,
      section1_sub,
      videourl,
      videotype
    } = this.props;

    let poster = "";
    let video = "";
    if (this.vaildUrl(videourl)) {
      video = videourl;
    } else {
      video = require("../../assets/videos/" + videoInfo.video);
    }
    let { show } = this.state;
    console.log("videotype", videotype, videourl);

    // if (videotype == "embed_url") {
    //   console.log("enter");
    //   var video_id = videourl.split("v=")[1];
    //   var ampersandPosition = video_id.indexOf("&");
    //   if (ampersandPosition != -1) {
    //     video_id = video_id.substring(0, ampersandPosition);
    //   }

    //   poster =
    //     "https://img.youtube.com/vi/" + video_id.trim() + "/hqdefault.jpg";
    // } else {
    //   poster =
    //     "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604682739327_clipboard-image%20%281%29.png";
    // }

    return (
      <React.Fragment>
        <Container
          className="video-section text-center"
          data-aos="fade-up"
          data-aos-duration="2000"
        >
          <Row>
            <Col xs={12}>
              <div
                dangerouslySetInnerHTML={{ __html: section1 ? section1 : "" }}
              ></div>
              <div className="scada-video-box">
                {/* <a
                      onClick={this.playVideo}
                      style={{ display: !show ? "block" : "none" }}
                    >
                      <img
                        src={poster}
                        alt="video-poster"
                        className="img-fluid"
                        style={{ display: !show ? "block" : "none" }}
                      />
                    </a> */}
                {/* <div style={{ display: show ? "block" : "none" }}> */}
                <div>
                  <ReactPlayer
                    className="video-category-player"
                    url={video}
                    width="100%"
                    height="458px"
                    playing={show}
                    controls={true}
                  />
                </div>
              </div>
              <div
                dangerouslySetInnerHTML={{
                  __html: section1_sub ? section1_sub : ""
                }}
              ></div>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}

export default VideoV1;
