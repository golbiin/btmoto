import React from "react";
import { Modal, Container, Col, Row } from "react-bootstrap";
import BlockUi from 'react-block-ui';

const ForgotPassword = props => {
  let modal = props.modal;
  const toggle = () => {
    modal = false;
    props.toogle();
  };

  return (
    <div>
      <Modal 
        show={modal} onHide={toggle}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        dialogClassName ="forgot-popup"
      >
        <Modal.Header closeButton>
          <span className="close_popup" onClick={toggle}></span>
        </Modal.Header>   
        <Modal.Body >
        <BlockUi tag="div" blocking={props.forgot_submit_status} >
          <Container>                
          <Row>
            <Col sm={12}>
            <p className={props.forgot_message_type}>{props.forgot_message}</p>
            <h3>Please enter your login email address to reset your password</h3>
            </Col>
          </Row>
          <Row className="forgot-form">
              <Col sm= {12}>
              <input
                type="text"
                name="forgot_email"
                placeholder="Enter your email"
                className="email-enter"
                value={props.value.forgot_email}
                onChange={props.handleForgotpwd}
              />
              {props.error.forgot_email ? (
                <div className="danger" style={{ paddingTop: "10px" }}>
                  {props.error.forgot_email}
                </div>
              ) : (
                ""
              )}
              </Col>
              <Col sm={12} md={6} xs={12}></Col>
              <Col sm={12} md={6} xs={12}>
              <button
                className="btn blue-btn forgot-button"
                onClick={props.forgotPasswordSubmit}
              >
                RESET PASSWORD
              </button>
              </Col>
          </Row>       
          </Container>
          </BlockUi>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default ForgotPassword;