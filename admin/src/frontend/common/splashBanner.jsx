import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";

class Splashbanner extends Component {
  constructor(props) {
    super(props);
    this.state = {  
    };
  }
  
  vaildUrl(url) {
    var expression =
      /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    if (url.match(regex)) {
      return true
    } else {
      return false
    }
  }

  render() {
    const { bannerInfo , slug} = this.props;
    let bg = '';
   // console.log('bannerInfo',bannerInfo);
    if (this.vaildUrl(bannerInfo.backgroundImage)) {
      bg = bannerInfo.backgroundImage
    } else {
      bg =  'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603902139033_aerial-view-tanker-oil-storage-tank-petrochemical-oil-shipping-terminal.jpg';
  //    require("../../assets/images/frontend/" +bannerInfo.backgroundImage);
    }

    return (
      <React.Fragment>
        <div
          className="product_banner product_banner_splash"
          style={{
            backgroundImage: "linear-gradient(      rgba(82, 75, 90, 0.9),    rgba(82, 75, 90, 0.9)    ), url(" + bg + ")",
            minHeight: "200px",
            width: "100%",
          }}>
          <Container className={ 'pdtfull_container ' + slug }>
            <Row className="banner_det">
              <Col xs={12} className="pdtbannercontent">
              <h2>{bannerInfo.title}</h2>
                <h4>{bannerInfo.subtitle}</h4>
              </Col>
            </Row>
          </Container>
        </div>
      </React.Fragment>
    );
  }
}

export default Splashbanner;
