import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class SilckSilder extends Component {
  state = {};
  vaildUrl(url) {
    var expression =
      /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    if (url.match(regex)) {
      return true
    } else {
      return false
    }
  }
  render() 
  {
    let settings = {
      dots: false,
      infinite: true,
      speed: 500,
      centerMode: true,
      centerPadding: "0px",
      slidesToShow: 3,
      arrows: true,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1950,
          settings: {
            centerMode: true,
            centerPadding: "450px",
            slidesToShow: 1,
            arrows: true,
          },
        },
        {
          breakpoint: 1500,
          settings: {
            arrows: true,
            centerMode: true,
            centerPadding: "250px",
            slidesToShow: 1,
          },
        },
        {
          breakpoint: 1200,
          settings: {
            arrows: true,
            centerMode: true,
            centerPadding: "250px",
            slidesToShow: 1,
          },
        },
        {
          breakpoint: 991,
          settings: {
            arrows: true,
            centerMode: true,
            centerPadding: "60px",
            slidesToShow: 1,
          },
        },
      ],
    };

    const images = require.context("../../assets/images", true);
    const { banners } = this.props;
   
  
    return (
      <React.Fragment>
        <Slider {...settings}>
          {
            banners ?
              banners.map((data, index) => {
                let bg = '';
                if (this.vaildUrl(data.image)) {
                  bg = data.image
                } else {
                  bg = images(`./frontend/oursucessstory.png`);
                }

                return (
                  <React.Fragment key={index}>
                    <div className="image_banner">
                      <img
                        src={bg}
                        className="img-fluid "
                        alt="customer"
                      ></img>
                    </div>
                  </React.Fragment>
                );
              }) : ''
          }
        </Slider>
      </React.Fragment>
    );

  }
}

export default SilckSilder;
