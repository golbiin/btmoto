import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import { siteUrl } from "../../config.json";
class Homeslider extends Component {
  vaildUrl(url) {
    var expression = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    if (url.match(regex)) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    const { categoryInfo } = this.props;
    console.log(">>>>>>>>>>>>>>>>>>", categoryInfo);
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false

      //autoplay: true,
      //autoplaySpeed: 6000
    };

    return (
      <React.Fragment>
        <div className="category-banner-container">
          <Slider {...settings}>
            {categoryInfo.map((data, index) => {
              let bg = "";

              if (this.vaildUrl(data.image)) {
                bg = data.image;
              } else {
                bg = require("../../assets/images/frontend/" + data.image);
              }
              let extension = bg
                .split(/[#?]/)[0]
                .split(".")
                .pop()
                .trim();
              console.log("????????????????????", data.image, index);
              return extension == "mp4" ? (
                <React.Fragment key={index}>
                  <div className="container-fluid category-banner-section scada-banner video_section">
                    <div className="home-video-section">
                      <div className="videoWrapper">
                        <video
                          width="100%"
                          height="auto"
                          autoplay="autoplay"
                          playsinline="playsinline"
                          muted="muted"
                          loop="loop"
                        >
                          <source src={data.image} type="video/mp4" />
                        </video>
                      </div>

                      {/* <div class="arrow_box">
                        <Link
                          onClick={this.scrollToMyRef}
                          to="#"
                          className="scroll-down-link"
                        ></Link>
                      </div> */}
                      <div className="caption_fullwidth">
                        <div className="container caption_container">
                          <div
                            className="slideshow_caption"
                            style={{ position: "absolute" }}
                          >
                            <h2
                              dangerouslySetInnerHTML={{
                                __html: data.title ? data.title : "sfdsfsdf"
                              }}
                            ></h2>
                            <div
                              dangerouslySetInnerHTML={{
                                __html: data.description
                                  ? data.description
                                  : "sdfsf"
                              }}
                            ></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                <React.Fragment key={index}>
                  <div
                    className="container-fluid category-banner-section scada-banner"
                    style={{ backgroundImage: "url(" + bg + ")" }}
                    id={index}
                  >
                    <div className="caption_fullwidth home-slider">
                      <div className="container caption_container">
                        <div className="slideshow_caption">
                          <h2
                            dangerouslySetInnerHTML={{
                              __html: data.title ? data.title : "sfdsfsdf"
                            }}
                          ></h2>
                          <div
                            dangerouslySetInnerHTML={{
                              __html: data.description
                                ? data.description
                                : "sdfsf"
                            }}
                          ></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              );
            })}
          </Slider>
        </div>
      </React.Fragment>
    );
  }
}

export default Homeslider;
