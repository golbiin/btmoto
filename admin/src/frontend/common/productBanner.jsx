import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";

class Productbanner extends Component {
  constructor(props) {
    super(props);
    this.state = {  
    };
  }
  
  vaildUrl(url) {
    var expression =
      /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    if (url.match(regex)) {
      return true
    } else {
      return false
    }
  }

  render() {
    const { productbannerInfo} = this.props;
    let bg = '';
    if (this.vaildUrl(productbannerInfo.backgroundImage)) {
      bg = productbannerInfo.backgroundImage
    } else {
      bg = require("../../assets/images/frontend/" +
      productbannerInfo.backgroundImage);
    }

    return (
      <React.Fragment>
        <div
          className="product_banner"
          style={{
            backgroundImage: "url(" + bg + ")",
            height: "310px",
            width: "100%",
          }}>
          <Container className="pdtfull_container">
            <Row className="banner_det">
              <Col xs={12} className="pdtbannercontent">
              <h2>{productbannerInfo.title}</h2>
                <h4>{productbannerInfo.subtitle}</h4>
              </Col>
            </Row>
          </Container>
        </div>
      </React.Fragment>
    );
  }
}

export default Productbanner;
