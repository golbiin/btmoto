import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";

class CommonBannerv1 extends Component {
  render() {
    const { bannerInfo } = this.props;

    return (
      <React.Fragment>
        <div className="about">
          <div className="category-banner-container">
            {bannerInfo.map((data, index) => {
            let  bkg = '';
            if(data.image){
                bkg = data.image
            } else {
                bkg = require("../../assets/images/frontend/" + data.image);
            }             
            
              return (
                  <div key={index}
                    className="container-fluid category-banner-section scada-banner about-banner"
                    style={{ backgroundImage: "url(" + bkg + ")" }}
                  >
                    <Container className="banner-container">
                      <Row>
                        <Col></Col>
                        <Col xs={7} className="abt-banner-content career-banner">
                          <h2>{data.title}</h2>
                          <div
                            dangerouslySetInnerHTML={{
                              __html: data.description,
                            }}
                          />
                        </Col>
                      </Row>
                    </Container>
                  </div>
              );
            })}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CommonBannerv1;
