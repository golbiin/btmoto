import React from "react";
import { Modal, Container, Col, Row } from "react-bootstrap";
import BlockUi from 'react-block-ui';

const ForgotEmail = props => {
  let modal1 = props.modal1;
  const recoveryemail = () => {
    modal1 = false;
    props.recoverymail();
  };

  return (
    <div>
      <Modal 
        show={modal1} onHide={recoveryemail}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        dialogClassName ="forgot-popup">   
        <Modal.Header closeButton>
          <span className="close_popup" onClick={recoveryemail}></span>
        </Modal.Header>   
        <Modal.Body >
        <BlockUi tag="div" blocking={props.recovery_submit_status} >
          <Container>           
          <Row>
            <Col sm={12}>
            <p className={props.recovery_message_type}>{props.recovery_message}</p>
            <h3>Enter your recovery email address to recover your account information.</h3>
            </Col>
          </Row>
          <Row className="forgot-form">
              <Col sm= {12}>
              <input
                type="text"
                name="forgot_email"
                placeholder="Enter your email"
                className="email-enter"
                value={props.value.forgot_email}
                onChange={props.handleForgotrecov}
              />
              {props.error.forgot_email ? (
                <div className="danger" style={{ paddingTop: "10px" }}>
                  {props.error.forgot_email}
                </div>
              ) : (
                ""
              )}
              </Col>
              <Col sm={12} md={6} xs={12}></Col>
              <Col sm={12} md={6} xs={12}>
              <button
                className="btn blue-btn forgot-button"
                onClick={props.forgotEmailSubmit}
              >
                SUBMIT
              </button>
              </Col>
          </Row>         
          </Container>
          </BlockUi>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default ForgotEmail;