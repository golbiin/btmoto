import React from "react";
import { Button, Modal} from "react-bootstrap";

class DistributorModel extends React.Component {
  render() {
    const { content } = this.props;
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"  
        centered
        dialogClassName="login-signup-popup" 
      >
        <Modal.Body className="disMod">
          <p>
            {content}
          </p>
        </Modal.Body>
        <Modal.Footer className="footer_adj">
          <Button variant="primary" onClick={this.props.onHide}>Accept</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default DistributorModel;
