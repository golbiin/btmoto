import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import "../../services/aos";

class RecommendedProducts extends Component {
  render() {
    const { products } = this.props;
    const shuffle_products = products.sort(() => 0.5 - Math.random());
    // Get sub-array of first n elements after shuffled
    let randomProducts = shuffle_products.slice(0, 3);

    const images = require.context("../../assets/images", true);
    if (products.length === 0) {
      return (
        <React.Fragment>
          <p className="paddingBottom"></p>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <div
            className="recommended-products"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <Container>
              <Row className="text-center">
                <h2>Recommended Products</h2>
              </Row>
              <Row className="text-center">
                {randomProducts.map((product, index) => {
                  return (
                    <Col md={4} sm key={index}>
                      {(() => {
                        if (product.url.indexOf("scada") !== -1) {
                          return (
                            <div>
                              <img
                                className="img-fluid"
                                alt=""
                                src={
                                  product.images.featured_image.image_url !=
                                  null
                                    ? product.images.featured_image.image_url
                                    : images(`./frontend/news-placeholder1.png`)
                                }
                              ></img>
                              <h5>
                                {product.product_name != null
                                  ? product.product_name
                                  : ""}
                              </h5>
                            </div>
                          );
                        } else {
                          return (
                            <div>
                              <a href={product.url + product.slug}>
                                {" "}
                                <img
                                  className="img-fluid"
                                  alt=""
                                  src={
                                    product.images.featured_image.image_url !=
                                    null
                                      ? product.images.featured_image.image_url
                                      : images(
                                          `./frontend/news-placeholder1.png`
                                        )
                                  }
                                ></img>
                              </a>
                              <h5>
                                <a href={product.url + product.slug}>
                                  {product.product_name != null
                                    ? product.product_name
                                    : ""}
                                </a>
                              </h5>
                            </div>
                          );
                        }
                      })()}
                    </Col>
                  );
                })}
              </Row>
            </Container>
          </div>
        </React.Fragment>
      );
    }
  }
}
export default RecommendedProducts;
