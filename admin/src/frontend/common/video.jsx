import React, { Component } from "react";
import { Container, Row, Col} from 'react-bootstrap';
import poster from '../../assets/images/frontend/sc-video-bg.png';


class Video extends Component { 
    constructor(props) {
        super(props);
    }
    render() {
        const { videoInfo } = this.props;
        var video = require('../../assets/videos/'+videoInfo.video);
        return (
            <React.Fragment>
                <Container className="video-section text-center">
                    <Row>
                        <Col xs={12}>
                            <h2>{videoInfo.title}</h2>
                            <h4>{videoInfo.sub_title}</h4>
                            <p dangerouslySetInnerHTML={ {__html: videoInfo.description} } />
                            <div className="scada-video-box">
                                <video controls poster={poster}>
                                    <source src={video} type="video/mp4"/>
                                </video>  
                            </div>  
                            <div className = {videoInfo.hideButton ? 'scada-button-box d-none' : 'scada-button-box'}>
                                <div className={videoInfo.showButton ? '': 'd-none'}><button className="download-access">{videoInfo.buttonText}</button><p className="btn-notes">{videoInfo.buttonNotes}</p></div>
                                <div><button className="download-access download-manual">Download Manual</button></div>
                                <div><button className="download-catalog">Download Catalog</button></div>
                            </div>                          
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

export default Video;
