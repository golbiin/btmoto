import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { CountryDropdown } from "react-country-region-selector";
import Joi from "joi-browser";

class JobSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      jobSearchData: {
        job_key: "",
        country: "",
      },
      getJobsDet: [],
      message: "",
      status: "",
      errors: {},
    };
  }

  /* Job validation schema */
  schema = {
    job_key: Joi.string().allow(""),
    country: Joi.optional().label("country"),
  };

  /* Select Handle Change */
  selectCountry = async (val) => {
    let jobSearchData = { ...this.state.jobSearchData };
    let errors = { ...this.state.errors };
    jobSearchData["country"] = val;
    this.setState({ jobSearchData, errors });
    delete errors.country;
    this.handleChange(val, "country");
  };

  /* Input Handle change */
  handleInput = async (e) => {
    const value = e.target.value;
    this.setState({ job_key: value });
    this.handleChange(value, "job_key");
  };

  /* Common Input Handle change */
  handleChange = async (value, item) => {
    const jobSearchData = { ...this.state.jobSearchData };
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(item, value);
    if (errorMessage) errors[item] = errorMessage;
    else delete errors[item];
    jobSearchData[item] = value;
    this.setState({ jobSearchData, errors });
  };

  /* Joi Validation Call */
  validateProperty = (item, value) => {
    const obj = { [item]: value };
    const schema = { [item]: this.schema[item] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Search Job Function */
  searchJobDetails = async (e) => {
    const jobSearchData = { ...this.state.jobSearchData };
    const errors = { ...this.state.errors };
    if (jobSearchData.country === "") {
      errors.country = " Please select your country.";
      this.setState({ errors });
    }
    let result = Joi.validate(jobSearchData, this.schema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
    }
    if (Object.keys(errors).length === 0) {
      const jobSearchData = {
        job_key: this.state.jobSearchData.job_key,
        country: this.state.jobSearchData.country,
      };
      if (this.props.propsnew.location.pathname === "/career-center") {
        this.props.searchJobs(jobSearchData);
      }
      this.props.propsnew.history.push({
        pathname: "/career-center",
        jobSearchData: jobSearchData,
      });
    }
  };

  render() {
    const { jobSearch } = this.props;
    const { country } = this.state.jobSearchData;
    return (
      <React.Fragment>
        <div className="job-search-wrapper">
          <Container>
            <div className="full_section">
              <div className="title-section">
                <h2>{jobSearch.title}</h2>
              </div>
              <Form className="job-search-form">
                <Row>
                  <Col md={6} className="left-section">
                    <Form.Group controlId="keyword">
                      <Form.Control
                        name="job_key"
                        type="text"
                        placeholder="KEYWORDS"
                        value={this.state.jobSearchData.job_key}
                        onChange={this.handleInput}
                      />
                    </Form.Group>
                  </Col>
                  <Col md={6} className="right-section">
                    <Row>
                      <Col lg={6}>
                        <Form.Group controlId="location">
                          <CountryDropdown
                            value={country}
                            onChange={(val) => this.selectCountry(val)}
                            className="form-control"
                          />
                          {this.state.errors.country ? (
                            <div className="danger">
                              {this.state.errors.country}
                            </div>
                          ) : (
                            ""
                          )}
                        </Form.Group>
                      </Col>

                      <Col lg={6}>
                        <Button
                          className="order-button"
                          variant="primary"
                          onClick={this.searchJobDetails}
                        >
                          SEARCH
                        </Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Form>
            </div>
            <div className="result_section"></div>
          </Container>
        </div>
      </React.Fragment>
    );
  }
}

export default JobSearch;
