import React from 'react'
import { Button, Modal, Tabs, Tab, Form } from 'react-bootstrap';
import Joi from "joi-browser";
import BlockUi from 'react-block-ui';
import {RegisterForm} from '../register/registerForm';

class LoginSignup extends React.Component {
  state = {
    data: { username: "", password: "" },
    errors: {},
  };

  /* Joi validation schema */
  schema = {
    username: Joi.string()
      .required()
      .email()
      .error(() => {
        return {
          message: "Please enter your email",
        };
      }),
    password: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your password",
        };
      }),
  };

  /* Input Handle Change */
  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;   
    this.setState({ data, errors });    
  };

  /*Joi Validation Call*/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

   /*Login Submit */
  loginSubmit = async (event) => {
    event.preventDefault();
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    let result = Joi.validate(data, this.schema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
    } else {
      this.props.login(data, this.state.remember);
    }
  };

  changeActiveTab = (tab) => {
    this.props.changeTab(tab);
  }
  
	render() {
    const {activeTab, showTab, closeModal, loginProcessing, loginFailure, toogle, recoverymail } = this.props;
		return (
			<Modal
        show={showTab} onHide={closeModal}
				size="lg"
				aria-labelledby="contained-modal-title-vcenter"
        centered
        dialogClassName ="login-signup-popup"
			>
				<Modal.Header closeButton>
				</Modal.Header>
				<Modal.Body>
          <h3 className={activeTab+'-h3'}><label>Create your CIMON Account</label></h3>
          <Tabs value={activeTab} defaultActiveKey={activeTab} transition={false} onSelect={this.changeActiveTab} id="signup-login-tab">
            <Tab eventKey="signup" title="SIGN UP">
              <RegisterForm closeModal={closeModal}/>
            </Tab>
            <Tab eventKey="login" title="LOG IN" >
              {loginFailure.message !== "" ? (
                  <div className={loginFailure.message_type}>
                    <p>{loginFailure.message}</p>
                  </div>
              ) : null}
              <BlockUi tag="div" blocking={loginProcessing} >
              <Form method="post" className="login-form" onSubmit= {this.loginSubmit}>
                <Form.Group controlId="formBasicEmail" className="login-form-group">                  
                  <Form.Control className="login-inputs" type="email" placeholder="Enter email" 
                  name="username"
                  onChange={this.handleChange}
                  value={this.state.data.username}/>
                  {this.state.errors.username ? (
                    <div className="danger">{this.state.errors.username}</div>
                  ) : (
                    ""
                  )}
                </Form.Group>
                <Form.Group controlId="formBasicPassword" className="login-form-group">                  
                  <Form.Control className="login-inputs" type="password" placeholder="Password" 
                  name="password"
                  onChange={this.handleChange}
                  value={this.state.data.password} />
                  {this.state.errors.password ? (
                    <div className="danger">{this.state.errors.password}</div>
                  ) : (
                    ""
                  )}
                </Form.Group>
                <Form.Group className="login-button-group">  
                <div className="forget-group forgot-pass-email">
                  <a  id="forgot_password"  onClick={toogle} >Forgot Password</a>
                  <a  id="forgot_email"  onClick={recoverymail} >Forgot Email</a>
                </div>  
                <div className="button-group">
                  <Button className="login-button" variant="primary" type="submit"
                  >
                    LOGIN
                  </Button>
                </div>                
                </Form.Group>
                <Form.Group controlId="formBasicCheckbox" className="remember-group">
                  <Form.Check type="checkbox" label="Remember Me"
                  name="forgot"
                  id="forgot"
                  onChange={this.rememberMe} />
                </Form.Group> 
              </Form>
              </BlockUi>
            </Tab>
          </Tabs>
        </Modal.Body>
			</Modal>
		);
	}
}

export default LoginSignup;
