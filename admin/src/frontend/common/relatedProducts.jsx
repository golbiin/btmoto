import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";

class RelatedProducts extends Component {
  render() {
    const { relProducts } = this.props;
    var newproductName;
    if(relProducts.length === 0) {
      return (
      <React.Fragment>
        <p className="paddingBottom"></p>
      </React.Fragment>
      )
    } else {
      return (
        <React.Fragment>
          <div className="relatedProductSec">
            <Container>
              <Row className="text-center">
                <h2>RELATED PRODUCTS</h2>
              </Row>
              <Row className="text-center rl_pdts">
                {relProducts.map((product, index) => {
                  newproductName = product.product_name.replace(/cimon/gi, "");
                  return (  
                    <Col
                      className="rel_col"
                      md={{ span: 3, offset: 1 }}
                      key={index}
                    >
                      <a href={product.url + product.slug}><h5>{newproductName}</h5></a>
                      <a href={product.url + product.slug}> <img
                        className="img-fluid"
                        alt=""
                        src={product.images.featured_image.image_url}
                      ></img> </a>
                    </Col>
                  );
                })}
              </Row>
            </Container>
          </div>
        </React.Fragment>
      );
    }
  }
}

export default RelatedProducts;
