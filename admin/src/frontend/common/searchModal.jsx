import React, { useState } from "react";
import { Modal, Form } from 'react-bootstrap';
import { useForm } from "react-hook-form";


const SearchModal = (props) => {
    const [ s , setS] = useState('');
    const { register, handleSubmit } = useForm({ 
    });

    /* Input handle change*/
  const handleChange = evt => {
      const newValue = evt.target.value;      
      setS(newValue);            
    }
    
  const onSubmit = async (data, e) => {
      setS(data.s);
      window.location.href = "/search/"+s;
      
    };

      return (
        <Modal
              {...props}
              size="lg"
              aria-labelledby="contained-modal-title-vcenter"
              centered
              className="search-modal"
            >
              <Modal.Body>
                <h4>CIMON Search</h4>
                <Form id="searchform"  className="searchform"  method="get" onSubmit={handleSubmit(onSubmit)}> 
                  <div className="input-div">
                  <input
                      name = "s"
                      placeholder="Search"
                      id="s"
                      type="text"                                                   
                      onChange={handleChange} 
                      ref= {register}
                  />
                  </div>               
                  <div className="button-div">
                  <button class="btn btn-default" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                  </div>                
                </Form>
              </Modal.Body>
            </Modal>   
      );
    
}

export default SearchModal;