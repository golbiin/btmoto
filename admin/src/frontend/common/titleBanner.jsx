import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";

class TitleBnner extends Component {
  vaildUrl(url) {
    var expression =
      /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    if (url.match(regex)) {
      return true
    } else {
      return false
    }
  }

  render() {
    const { bannerInfo } = this.props;
    const images = require.context("../../assets/images/frontend/", true);
    return (
      <React.Fragment>
        {bannerInfo.map((data, index) => {
          let  bg = '';
        if (this.vaildUrl(data.image)) {
          bg = data.image
        } else {
          bg =  require("../../assets/images/frontend/" + data.image);
        }          
          return (
            <div
              key={index}
              className="container-fluid title-banner-section"
              style={{ backgroundImage: "url(" + bg + ")" }}
            >
              <Container className="banner-container">
                <Row>
                  <Col xs={12} className="banner-content">
                    <h1>{data.title}</h1>
                  </Col>
                </Row>
              </Container>
            </div>
          );
        })}
      </React.Fragment>
    );
  }
}

export default TitleBnner;
