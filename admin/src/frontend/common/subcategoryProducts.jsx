import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";

class SubcategoryProducts extends Component {
  render() {
    const {categoryData1 } = this.props;
    const images = require.context("../../assets/images/frontend", true);
    let sub_category = this.props.propsnew.match.params.slug;
    console.log('elveee------------------->', categoryData1);
    return (
      <React.Fragment>
        <div className="recommended-products subcategory-products">
          <Container>
            <Row className="text-center">
              <h2>All Modules Types</h2>
            </Row>
            <Row className="text-center">
              {categoryData1.map((catdat, index) => {
                return (
                  <Col xs={6} md={3} key={index}>
                    <h5>
                      {catdat.category_name != null ? catdat.category_name : ""}
                    </h5>
                    <Link
                      to={
                        "/products/plc/" +
                        sub_category +
                        "/" +
                        (catdat.slug != null ? catdat.slug : "")
                      }
                    >
                      {catdat.image !== "" ? (
                        <img
                          className="img-fluid additional-background"
                          alt=""
                          src={catdat.image}
                        />
                      ) : (
                        <img
                          className="img-fluid no-backg"
                          alt=""
                          src={images(`./noImage.png`)}
                        />
                      )}
                    </Link>
                  </Col>
                );
              })}
            </Row>
          </Container>
        </div>
      </React.Fragment>
    );
  }
}

export default SubcategoryProducts;
