import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {loadCart,removeProduct,updateCart,changeQuantity,hideCart,setProcessing,applyCoupon,removeCoupon} from "../../services/actions/cartActions";
import LoginSignup from "./loginSignup";
import ForgotPassword from "./forgotPassword";
import ForgotEmail from "./forgotEmail";
import SearchModal from "./searchModal";
import * as MenuService from "../../ApiServices/menus";
import Cart from "../cart/cart";
//import gtrans from "../../services/googletranslate";
import { getCurrentUser, signOut, login, showHideLogin, changeTab, displayEditProfile, hideEditProfile } from './../../services/actions/userActions';
import EditProfile from "./../profile/editProfile";
import Joi from "joi-browser";
import * as authServices from "./../../ApiServices/authenticate";

class Header extends Component {
  static propTypes = {
    loadCart: PropTypes.func.isRequired,
    updateCart: PropTypes.func.isRequired,
    cartProducts: PropTypes.array.isRequired,
    hideCart: PropTypes.func.isRequired,
    setProcessing: PropTypes.func.isRequired,
    getCurrentUser: PropTypes.func.isRequired,
    signOut: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
    showHideLogin: PropTypes.func.isRequired,
    applyCoupon: PropTypes.func.isRequired,
    removeCoupon: PropTypes.func.isRequired,
  
  };
  constructor(props) {
    super(props);
    this.state = {      
      profile: {},
      showSearch: false,
      modalShow: false,
      cartShow: false,
      mainMenu: [],
      pagevalue: "",
      translated: "",
      langcode: "",
      context: props.context,
      currentUser: {},
      modal: false,
      modal1: false,
      forgotpassword: {
        forgot_email: ""
      },
      forgotpassword_copy: {
        forgot_email: ""
      },
      forgotemail: {
        forgot_email: ""
      },
      forgotemail_copy: {
        forgot_email: ""
      },
      errors: {},
      forgot_submit_status: false,
      forgot_message: "",
      forgot_message_type: "",
      recovery_submit_status: false,
      recovery_message: "",
      recovery_message_type: "",
      lang:[],
    };
    this.props.setProcessing(false);
  }

   /* forgot email validation schema */
  forgotSchema = {
    forgot_email: Joi.string()
      .required()
      .email()
      .error(() => {
        return {
          message:
            "The email that you’ve entered is invalid. Please enter a properly formatted email."
        };
      })
  };

  componentDidMount = async () => {
   
    // var addScript = document.createElement('script');
    // addScript.setAttribute('src', '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit');        
    // document.body.appendChild(addScript);  
    // window.googleTranslateElementInit = this.googleTranslateElementInit;
    // gtrans.GtranslateElement();
    this.getHeader();
    this.getAlllanguage();
    this.props.showHideLogin(false,'login');
    this.props.getCurrentUser();
    this.props.loadCart(false);  
    this.props.displayEditProfile(false)  
  };

  /* Language Translation */

  // googleTranslateElementInit = async () => {
  //   new window.google.translate.TranslateElement(
  //     {
  //       pageLanguage: "en",
  //       includedLanguages: "en,es,ko",

  //       layout: window.google.translate.TranslateElement.InlineLayout.SIMPLE,
  //     },
  //     "google_translate_element"
  //   );
  // };

  /* List Header Elements */
  getHeader = async () => {
    this.setState({ blocking: true });
    try {
      const response = await MenuService.getHeaderMenus();
      if (response.data.status == 1) {
        if (response.data.headermenu.menu_items.length > 0) {
          this.setState({
            mainMenu: response.data.headermenu.menu_items,
          });
        }
      } else {
        this.setState({ blocking: false });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };



 /* Get all Language */
 getAlllanguage = async () => {
    this.setState({ blocking: true });
    try {
      const response = await authServices.getAlllanguage();
      if(response.data.status == 1){
        this.setState({lang:response.data.language})
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };
  showSignUpLoginPopup = (type) => {
    this.setState({ modalShow: true });
    this.setState({ activeTab: type }); 
  }

  Signout = () => {
    this.props.signOut();
  };

  /* Forgot Password */
  toogle = () => {
    this.props.showHideLogin(false,'login')
    let status = !this.state.modal;
    let forgotPassword = { ...this.state.forgotpassword };
    let forgotPasswordcopy = { ...this.state.forgotpassword_copy };
    forgotPassword = forgotPasswordcopy;
    const errors = { ...this.state.errors };
    delete errors["forgot_email"];
    this.setState({
      modal: status,
      forgotPassword,
      forgot_message: "",
      forgot_submit_status: false,
      errors
    });
  };
 
  /* Forgot Recovery Email */
  recoverymail = () => {
    this.props.showHideLogin(false,'login')
    let status = !this.state.modal1;
    let forgotEmail = { ...this.state.forgotemail };
    let forgotEmailcopy = { ...this.state.forgotemail_copy };
    forgotEmail = forgotEmailcopy;
    const errors = { ...this.state.errors };
    delete errors["forgot_email"];
    this.setState({
      modal1: status,
      forgotEmail,
      recovery_message: "",
      recovery_submit_status: false,
      errors
    });
  };

  /* Forgot Form Joi Validation Call */
  validateForgotForm = ({ name, value }) => {
    const obj = { [name]: value };
    const forgotSchema = { [name]: this.forgotSchema[name] };
    const { error } = Joi.validate(obj, forgotSchema);
    return error ? error.details[0].message : null;
  };

 /* Forgot Passowrd Input Handle change */
  handleForgotpwd = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const forgotpassword = { ...this.state.forgotpassword };
    const errorMessage = this.validateForgotForm(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    forgotpassword[input.name] = input.value;
    this.setState({ forgotpassword, errors }); 
  };

/* Forgot Email Input Handle change */
  handleForgotrecov = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const forgotemail = { ...this.state.forgotemail };
    const errorMessage = this.validateForgotForm(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    forgotemail[input.name] = input.value;
    this.setState({forgotemail, errors });   
  };

/* Forgot Password Change */
  forgotPasswordSubmit = async () => {
    let forgotPassword = { ...this.state.forgotpassword };
    const errors = { ...this.state.errors };
    this.setState({ forgot_submit_status: true });
    let result = Joi.validate(forgotPassword, this.forgotSchema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
      this.setState({ forgot_submit_status: false });
    } else {
      try {
        const response = await authServices.forgotPassword(forgotPassword);
        if (response) {
          const forgotPasswordcopy = { ...this.state.forgotpassword_copy };
          if (response.data.status === 0) {          
            this.setState({
              forgot_message: response.data.message,
              forgot_message_type: "error",
              forgot_submit_status: false
            });
          } else {
            forgotPassword = forgotPasswordcopy;
            delete errors["forgot_email"];
            this.setState({
              forgotPassword,
              forgot_message: response.data.message,
              forgot_message_type: "sucess",
              forgot_submit_status: false,
              errors
            });
          }
        }
      } catch (err) {}
    }
  };

  /* Forgot Email Recovery */
  forgotEmailSubmit = async () => {
    let forgotEmail = { ...this.state.forgotemail };
    const errors = { ...this.state.errors };
    this.setState({ recovery_submit_status: true });
    let result = Joi.validate(forgotEmail, this.forgotSchema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
      this.setState({ recovery_submit_status: false });
    } else {
      try {
        const response = await authServices.forgotRecoveryEmail(forgotEmail);
        if (response) {
          const forgotEmailcopy = { ...this.state.forgotemail_copy };
          if (response.data.status === 0) {   
            this.setState({
              recovery_message: response.data.message,
              recovery_message_type: "error",
              recovery_submit_status: false
            });
          } else {
            forgotEmail = forgotEmailcopy;
            delete errors["forgot_email"];
            this.setState({
              forgotEmail,
              recovery_message: response.data.message,
              recovery_message_type: "sucess",
              recovery_submit_status: false,
              errors
            });
          }
        }
      } catch (err) {}
    }
  };

  render() {

    const images = require.context("../../assets/images", true);
    const { mainMenu } = this.state;
    const {cartTotal,cartProducts,removeProduct,changeQuantity,loadCart,hideCart,showCartFlag,isCartProcessing,authUser,loginProcessing,loginFailure,login,displayLoginPopup,activeTab,showHideLogin,changeTab,applyCoupon,couponDetails, removeCoupon} = this.props;

    return (
      <React.Fragment>
        <div className="container-fluid">
          <div id="header">
            <div className="secondary-header">
              <div className="container">
                <nav className="sub_menu ddd">
                  <ul className="menu">
                    <li
                      id="menu-item-search"
                      className="noMobile menu-item menu-item-search"
                    >
                      <Link to="#" onClick={() => {
                        this.setState({showSearch:true})
                      }}> </Link>
                    </li>

                    <li id="lang-menu" className="dropdown  menu-item">
                      {/* <li>Select</li> */}
                      {/* <div
                        id="google_translate_element"
                        onClick={() => gtrans.GtranslateElement()}
                      ></div> */}
                      <div>
                      
                        <li className="menu-item menu-item-type-custom dropdown">
                        <a className="arrowLog dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Select Language</a>
                        <ul className="sub-menu dropdown-menu" aria-labelledby="dropdownMenuLogin">
                        {this.state.lang.map((data, index) => (   
                        <li  className="menu-item menu-item-type-custom  dropdown-item">
                          <a href={data.url} target="_blank">{data.language}</a>
                          </li>
                        )
                        )}
                      </ul>
                      </li>
                      </div>

                      
                    </li>
                    {(() => {
                      if(authUser.email == '') {
                        return (
                          <li
                            id="loginmenu"
                            className="menu-item menu-item-type-custom dropdown"
                          >
                            <a
                              className="arrowLog dropdown-toggle"
                              onClick={() => showHideLogin(true,'login')}
                            >Login</a>
                          </li>
                        
                        )
                      }
                    })()}
                    {(() => {
                    if(authUser.email !== '') { 
                      return(
                        <li id="signupmenu" className="menu-item menu-item-type-custom dropdown">
                        <a className="arrowLog dropdown-toggle" id="dropdownMenuLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {authUser.profile_image? <img className="profile-img" src={authUser.profile_image}></img>:
                       <img className="profile-img" src={images(`./frontend/profile-head.png`)}></img> }
                        {"Welcome, "+" "+authUser.first_name}
                        </a>
                        <ul className="sub-menu dropdown-menu" aria-labelledby="dropdownMenuLogin">
                          <li id="submenu-log" className="menu-item menu-item-type-custom  dropdown-item">
                              <Link to="/dashboard" >Dashboard</Link>
                          </li>
                          {/* <li id="submenu-log" className="menu-item menu-item-type-custom  dropdown-item">
                              <Link to="/ticket-dashboard" >Ticket Dashboard</Link>
                          </li> */} 
                          <li id="submenu-log" className="menu-item menu-item-type-custom  dropdown-item">
                              <Link to="#" onClick={() => {                    
                                this.props.displayEditProfile(true);
                              } } >Edit Profile</Link>
                          </li>
                          <li id="submenu-log" className="menu-item menu-item-type-custom  dropdown-item">
                              <Link to="/" onClick={this.Signout} >Sign Out</Link>
                          </li>                          
                        </ul>
                      </li>                     
                     )
                    } else{
                      return (
                        <li
                          id="signupmenu"
                          className="menu-item menu-item-type-custom dropdown"
                        >
                          <a
                            className="arrowLog"
                            role="button"
                            id="dropdownMenuLink"
                            onClick={() => showHideLogin(true, 'signup')}
                          >Sign Up</a>
                        </li>
                      )
                    }
                    })()}
                    <li className="cart_li menu-item">
                      <Link
                        to="#"
                        className="popup-with-form"
                        onClick={() => loadCart(true)}
                      >
                        {cartTotal.productQuantity ? cartTotal.productQuantity: 0} Item
                      </Link>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
            <div className="header-main">
              <div className="container">
                <nav className="navbar navbar-expand-md navbar-dark">
                  <Link className="navbar-brand" to="/">
                    <img
                      className="logoimg"
                      alt=""
                      src={images(`./frontend/logo.png`)}
                    ></img>
                  </Link>
                  <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarsExampleDefault"
                    aria-controls="navbarsExampleDefault"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                  >
                    <span className="navbar-toggler-icon"></span>
                  </button>

                  <div
                    className="collapse navbar-collapse mainHeader"
                    id="navbarsExampleDefault"
                  >
                    <ul className="navbar-nav ml-auto">
                    <span className="login-signup-mob">
                    {(() => { 
                      if(authUser.email == '') {
                        return (  
                          <li
                            id="loginmenumob"
                            className="menu-item menu-item-type-custom dropdown"
                          >
                            <a
                              className="arrowLog dropdown-toggle"
                              onClick={() => showHideLogin(true,'login')}
                            >Login &</a>
                          </li>
                        )
                      }
                    })()}
                    {(() => {
                    if(authUser.email !== '') { 
                      return(
                        <li id="signupmenumob" className="menu-item menu-item-type-custom dropdown">
                        <a className="arrowLog dropdown-toggle" id="dropdownMenuLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       {authUser.profile_image? <img className="profile-img" src={authUser.profile_image}></img>:
                       <img className="profile-img" src={images(`./frontend/profile-head.png`)}></img> }
                        {"Welcome, "+" "+authUser.first_name}
                        </a>
                        <ul className="sub-menu-mob dropdown-menu" aria-labelledby="dropdownMenuLogin">
                          <li id="submenu-log" className="menu-item menu-item-type-custom  dropdown-item">
                              <Link to="/dashboard" >Dashboard</Link>
                          </li>
                          <li id="submenu-log" className="menu-item menu-item-type-custom  dropdown-item">
                              <Link to="#" onClick={() => {                    
                                this.props.displayEditProfile(true);
                              } } >Edit Profile</Link>
                          </li>
                          <li id="submenu-log" className="menu-item menu-item-type-custom  dropdown-item">
                              <Link to="/" onClick={this.Signout} >Signout</Link>
                          </li>                          
                        </ul>
                      </li>                     
                     )
                    } else{
                      return (
                        <li
                          id="signupmenumob"
                          className="menu-item menu-item-type-custom dropdown"
                        >
                          <a
                            className="arrowLog"
                            role="button"
                            id="dropdownMenuLink"
                            onClick={() => showHideLogin(true, 'signup')}
                          >Sign Up</a>
                        </li>
                      )
                    }
                    })()}
                  </span>
                      {mainMenu.map((menu, index) => {
                        return (
                          <li
                            key={index}
                            className={
                              menu.child_values.length > 0
                                ? "nav-item dropdown"
                                : "nav-item"
                            }
                          >
                            <Link
                              className={
                                menu.child_values.length > 0
                                  ? "nav-link dropdown-toggle"
                                  : "nav-link"
                              }
                              to={menu.url ? menu.url : ""}
                              data-toggle={
                                menu.child_values.length > 0 ? "dropdown" : ""
                              }
                            >
                              {menu.name ? menu.name : ""}
                            </Link>
                            
                              {menu.child_values.length > 0
                                ? <div className="dropdown-menu">
                                  {menu.child_values.map((innerMenu, index) => {
                                    return (
                                      <Link
                                        className="dropdown-item"
                                        to={innerMenu.url ? innerMenu.url : ""}
                                        key={index}
                                      >
                                        {innerMenu.name}
                                      </Link>
                                    );
                                  })}
                                  </div>
                                : " "}
                            
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
        {
          (authUser.email == '') && 
          <LoginSignup 
          showTab={displayLoginPopup} 
          closeModal={showHideLogin} 
          activeTab={activeTab} 
          loginProcessing= {loginProcessing}
          login= {login}
          loginFailure= {loginFailure}
          changeTab = {changeTab}
          toogle = { this.toogle }
          recoverymail = { this.recoverymail }
          />
        }
        {
          (authUser.email == '') && 
          <ForgotPassword
          modal={this.state.modal}
          toogle={this.toogle}
          handleForgotpwd={this.handleForgotpwd}
          value={this.state.forgotpassword}
          error={this.state.errors}
          forgotPasswordSubmit={this.forgotPasswordSubmit}
          forgot_message_type={this.state.forgot_message_type}
          forgot_message={this.state.forgot_message}
          forgot_submit_status={this.state.forgot_submit_status}
          />
        }

          {
          (authUser.email == '') && 
          <ForgotEmail
          modal1={this.state.modal1}
          recoverymail ={this.recoverymail}
          handleForgotrecov={this.handleForgotrecov}
          value={this.state.forgotemail}
          error={this.state.errors}
          forgotEmailSubmit={this.forgotEmailSubmit}
          recovery_message_type={this.state. recovery_message_type}
          recovery_message={this.state.recovery_message}
          recovery_submit_status={this.state. recovery_submit_status}
          />
        }  
        <Cart
          applyCoupon = {applyCoupon}
          show={showCartFlag}
          hideCart={hideCart}
          cartProducts={cartProducts}
          cartTotal={cartTotal}
          removeProduct={removeProduct}
          changeQuantity={changeQuantity}
          isCartProcessing = {isCartProcessing}
          couponDetails={couponDetails}
          removeCoupon = {removeCoupon}
        />
        <SearchModal
          show={this.state.showSearch}
          onHide={() => this.setState({showSearch:false})}
        />
        {(authUser.email !== '') && 
        <EditProfile
          profileProcessing = {this.props.profileProcessing}
          show={this.props.displayEditProfileStatus}
          onHide={() => this.props.hideEditProfile(false)}
          profile = { this.props.profile }
        />
        }
       
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  authUser: state.auth.authUser,
  cartId: state.cart.cartId,
  cartProducts: state.cart.products,
  cartTotal: state.cart.data,
  showCartFlag: state.cart.showCartFlag,
  isCartProcessing: state.cart.isCartProcessing,
  loginProcessing: state.auth.loginProcessing,
  loginFailure: state.auth.loginFailure,
  displayLoginPopup: state.auth.displayLoginPopup,
  activeTab: state.auth.activeTab,
  couponDetails: state.cart.couponDetails,
  displayEditProfileStatus: state.auth.displayEditProfileStatus,
  profile: state.auth.profile,
  profileProcessing: state.auth.profileProcessing
});
export default connect(mapStateToProps, {
  getCurrentUser,
  signOut,
  loadCart,
  updateCart,
  removeProduct,
  changeQuantity,
  hideCart,
  setProcessing,
  login,
  showHideLogin,
  changeTab,
  applyCoupon,
  removeCoupon,
  displayEditProfile,
  hideEditProfile
})(Header);
