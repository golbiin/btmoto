import React, { Component } from "react";
import Slider from "react-slick";
import { Container } from "react-bootstrap";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import  "../../services/aos";

class CareerSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  vaildUrl(url) {
    var expression = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    if (url.match(regex)) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    const { careerSliderInfo, title } = this.props;
    var settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      initialSlide: 0,
      arrows: true,
      focusOnSelect: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
    return (
      <React.Fragment>
        <div className="simple-careeer-slider" data-aos="fade-up"  data-aos-duration="2000">
          <Container className="careers">
            <div
              className="title-sec text-center"
              dangerouslySetInnerHTML={{ __html: title ? title : "" }}
            ></div>
            <div className="slider-secc">
              <Slider className="careerslider" {...settings}>
                {careerSliderInfo.map((careerimages, index) => {
                  let bg = "";
                  if (this.vaildUrl(careerimages.image)) {
                    bg = careerimages.image;
                  } else {
                    bg = require("../../assets/images/frontend/" +
                      careerimages.image);
                  }
                  return (
                    <div className="icon-content" key={index}>
                      <div className="content-box">
                        <img className="img-fluid" alt="" src={bg}></img>
                      </div>
                    </div>
                  );
                })}
              </Slider>
            </div>
          </Container>
        </div>
      </React.Fragment>
    );
  }
}

export default CareerSlider;
