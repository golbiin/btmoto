import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class AsNavFor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null,
    };
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2,
    });
  }

  render() {
    const { sliderproducts } = this.props;
    const settings = {
      arrows: false,
      infinite: false,
    };
    return (
      <React.Fragment>
        <div className="singleproductslider">
          <Slider
            className="first_slider"
            {...settings}
            asNavFor={this.state.nav2}
            ref={(slider) => (this.slider1 = slider)}
          >
            {sliderproducts.map((sproduct, index) => {
              return (
                <div className="sliderMidImage" key={index}>
                  <img
                    className="img-fluid"
                    alt=""
                    src={sproduct.image_url}
                  ></img>
                </div>
              );
            })}
          </Slider>
          <Slider
            className="second_slider"
            asNavFor={this.state.nav1}
            ref={(slider) => (this.slider2 = slider)}
            slidesToShow={5}
            focusOnSelect={true}
            infinite={false}
          >
            {sliderproducts.map((sproduct, index) => {
              return (
                <div className="sliderMidImage" key={index}>
                  <img
                    className="img-fluid"
                    alt=""
                    src={sproduct.image_url}
                  ></img>
                </div>
              );
            })}
          </Slider>
        </div>
      </React.Fragment>
    );
  }
}

export default AsNavFor;
