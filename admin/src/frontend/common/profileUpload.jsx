import React, { Component } from "react";
class Profileupload extends Component {
  state = {
    uploading: false,
    profile_image: "",
  };

  uplaodHandle = async (event) => {
    event.preventDefault();
    let files = event.target.files[0]; 
    if (files) {
      if (this.checkMimeType(event)) {
        let profile_image = URL.createObjectURL(event.target.files[0]);
        this.setState({ profile_image: profile_image });
        this.props.onuplaodProfile(event, "profile_image");
     };
    }
  };

  checkMimeType = (event) => {
    let files = event.target.files[0];
    const types = ["image/png", "image/jpeg", "image/gif", "image/jpg"];
    let error = "";
    if (!types.includes(files.type)) {
      error = files.type + " is not a supported format\n";
      this.props.onuplaodProfile(error, "errors");
    } else {
      return true;
    }
  };

  render() {
    const images = require.context("../../assets/images", true);
    return (
      <React.Fragment>
        <div className="profile_up">
          <label htmlFor="image">
            {this.state.profile_image ? (
              <React.Fragment>
                <input
                  type="file"
                  name="image"
                  id="file"
                  accept= "image/png, image/jpeg, image/gif, image/jpg "
                  onChange={this.uplaodHandle}
                  className="profile_img"
                />
                 <span>Only .jpg and .png file are accepted.</span>
                 <div>
                <img alt="" src={this.state.profile_image} className="img-fluid upload_img" />
                </div>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <input
                  type="file"
                  name="image"
                  id="file"
                  accept= "image/png, image/jpeg, image/gif, image/jpg "
                  onChange={this.uplaodHandle}
                  className="profile_img"
                />
                 <span>Only .jpg and .png file are accepted.</span>
                <img
                  src={
                    this.props.value
                      ? this.props.value
                      : images(`./frontend/profile-dummy.png`)
                  }
                  className="img-fluid pro_img" alt=""
                />
              </React.Fragment>
            )}
             <span className="upload-img-label">
            <i className="fa fa-camera" aria-hidden="true"></i>
          </span>
          </label>
        </div>
      </React.Fragment>
    );
  }
}

export default Profileupload;
