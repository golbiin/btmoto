import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Slider from "react-slick";

class Banner extends Component {
  vaildUrl(url) {
    var expression =
      /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    if (url.match(regex)) {
      return true
    } else {
      return false
    }
  }


  render() {
    const { categoryInfo } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
    };

    return (
      <React.Fragment>
        <div className="category-banner-container">
          <Slider {...settings}>
            {categoryInfo.map((data, index) => {
              let bg = '';
              if (this.vaildUrl(data.image)) {
                bg = data.image
              } else {
                bg = require("../../assets/images/frontend/" + data.image);
              }

              return (
                <React.Fragment key={index}>
                  <div
                    className="container-fluid category-banner-section scada-banner"
                    style={{ backgroundImage: "url(" + bg + ")" }}
                  >
                    <Container className="banner-container">
                      <Row>
                        <Col></Col>
                        <Col xs={7} className="banner-content">
                          {data.title ? <h2 dangerouslySetInnerHTML={{
                            __html: data.title,
                          }}></h2> : ''}
                          {data.sub_title ? <h4>{data.sub_title}</h4> : ''}
                          <p
                            dangerouslySetInnerHTML={{
                              __html: data.description,
                            }}
                          />
                        </Col>
                      </Row>
                    </Container>
                  </div>
                </React.Fragment>
              );
            })}
          </Slider>
        </div>
      </React.Fragment>
    );
  }
}

export default Banner;
