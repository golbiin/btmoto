import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as FooterService from "../../ApiServices/footerColumns";
import $ from "jquery";
import * as authServices from "../../ApiServices/login";
class Footer extends Component {
  state = {
    footerclms :[],
    Footerlist : {
      socket_content : {
        socket1_content : '',
        socket2_content: '',
        socket3_content: ''
      }
    },
  };

  componentDidMount = () => {
    this.getFooter() 
  }; 

  /* List Footer Elements */
  
  getFooter = async () => {
    this.setState({ blocking: true});
    try {
      const response =  await FooterService.getFooter();
      if(response.data.status === 1) {
        let FooterDetails = response.data.data;
        this.setState({Footerlist:FooterDetails}) 
        if (response.data.FooterDetails.length > 0) {
          this.setState({
            footerclms : response.data.FooterDetails,
          });
        }
      }
      this.setState({ blocking: false});
    } catch (err) {
        this.setState({ blocking: false});
    }
  }

  render() {
    const { footerclms,Footerlist} = this.state;
    if(!this.state.userloggin){
        $('.loginsignuppopup').on('click',function(e){
          e.preventDefault();
          document.getElementById("dropdownMenuLink").click();
      });


     
      
    }

    $('.feedbackidpopup').on('click',function(e){
      console.log('feedbackidpopup ----->');
      e.preventDefault();
      document.getElementById("feedbackid").click();
    });
   
    return (
      <React.Fragment>
        <div className="container_wrap footer_color" id="footer">
          <div className="top-footer">
            <div className="container">
              <div className="footer_container">
                <div className="row">
                  {footerclms.map((menu, index) => {
                    return(                      
                      <div className="col-md-2 col-sm-12 col-first" key={index}>                        
                        <h3 className="widgettitle"> {menu.name}</h3>
                        <div className="menu-products">                        
                          <ul className="menu-products-menu">
                            {menu.menu_items ? menu.menu_items.map((innerMenu, menu_index) =>{
                              return (
                                <li className="menu-item" key={menu_index}>
 
                                    {(() => {
                                    if (innerMenu.name.toLowerCase() == 'forum' || innerMenu.url.includes('.zendesk')) {
                                      return (
                                      <span> 
                                        <a target="_blank" href={innerMenu.url}>{innerMenu.name}</a>
                                      </span> 
                                      );
                                    } else {
                                      return (
                                      <span>
                                        <a href={innerMenu.url}>{innerMenu.name}</a>
                                      </span>
                                      );
                                    }
                                    })()}
                                 
                                </li>
                               
                              );
                            }) : " "}                    
                          </ul>
                        </div>
                      </div>                  
                   );
                  })}
                </div>
              </div>
            </div>
          </div>
          <div className="bottom-footer">
            <div className="row">            
              <div className="col-lg-2 col-md-12 footer-logo" >
                <div className="innerdiv1"
                      dangerouslySetInnerHTML={{
                        __html:
                        Footerlist.socket_content.socket1_content != null
                            ? Footerlist.socket_content.socket1_content
                            : "",
                      }}
                   />                
              </div>

              <div className="col-lg-7 col-md-12  mr-auto footer-info">
                <div className="innerdiv2"
                  dangerouslySetInnerHTML={{
                    __html:
                    Footerlist.socket_content.socket2_content != null
                        ? Footerlist.socket_content.socket2_content
                        : "",
                  }}
                />
              </div>
              <div className="col-lg-2 col-md-12  colauto" >
                <div className="innerdiv3" 
                      dangerouslySetInnerHTML={{
                        __html:
                        Footerlist.socket_content.socket3_content != null
                            ? Footerlist.socket_content.socket3_content
                            : "",
                      }}
                   />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Footer;
