import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";

class CommonBanner extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { bannerInfo } = this.props;
    const images = require.context("../../assets/images/frontend/", true);

    return (
      <React.Fragment>
        <div className="about">
          <div className="category-banner-container">
            {bannerInfo.map((data, index) => {
              const bkg = require("../../assets/images/frontend/" + data.image);
              return (             
                  <div key={index}
                    className="container-fluid category-banner-section scada-banner about-banner"
                    style={{ backgroundImage: "url(" + bkg + ")" }}
                  >
                    <Container className="banner-container">
                      <Row>
                        <Col></Col>
                        <Col xs={7} className="abt-banner-content career-banner">
                          <h2>{data.title}</h2>
                          <p
                            dangerouslySetInnerHTML={{
                              __html: data.description,
                            }}
                          />
                          <div
                            className={
                              data.showButton ? "x-panel-desinger" : "d-none"
                            }
                          >
                            <button className="xpannel-access">
                              Career Opportunity
                            </button>
                          </div>
                        </Col>
                      </Row>
                    </Container>
                  </div>           
              );
            })}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CommonBanner;
