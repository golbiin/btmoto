import React, { Component } from "react";
import Header from "../common/header";
import Footer from "../common/footer";
import Joi from "joi-browser";
import {  Redirect } from 'react-router-dom';
import { Container, Row } from 'react-bootstrap';
import * as authServices from "../../ApiServices/authenticate";
import { Link } from "react-router-dom";
import BlockUi from 'react-block-ui';
import { connect } from "react-redux";
import { showHideLogin } from './../../services/actions/userActions';

class ResetPassword extends Component {
  state = {
    userid: "",
    data: { password: "", retype_password: "" },
    dataCopy: { password: "", retype_password: "" },
    submit_status: false,
    message: "",
    response_type: ""
  };

  /*  validation schema  */
  schema = {
    password: Joi.string()
      .required()
      .error(() => {
        return {
          message: " Please enter a properly formatted Password."
        };
      }),
    retype_password: Joi.string()
      .required()
      .error(() => {
        return {
          message: " Please enter a properly formatted Password."
        };
      })
  };

  componentDidMount = async () => {
    let id = this.props.match.params.token;
    this.setState({ userid: id });
  };

   /* Input Handle Change */
  handleChange = async ({ currentTarget: input }) => {
    this.setState({ message: "" });
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    if (input.name === "retype_password") {
      if (input.value !== data.password) {
        errors[input.name] = "The passwords you’ve entered does not match.";
      } else {
        delete errors[input.name];
      }
    } else {
      const error = this.passwordComplexity(input.value);
      if (error) errors[input.name] = error;
      else delete errors[input.name];
    }
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  /* Password Validation*/
  passwordComplexity = value => {
    let error = "";
    if (value.length > 7) {
    } else {
      error = "Minimum 8 characters required";
      return error;
    }
    if (value.match(/[a-zA-Z]/)) {
    } else {
      error = "Password should contain a letter";
      return error;
    }
    if (value.match(/[0-9]/)) {
    } else {
      error = "Password should contain a number";
      return error;
    }
    if (value.match(/[!@#$%^&*()?/]/)) {
    } else {
      error = "Password should contain a Special character";
      return error;
    }
    return error;
  };

/* Reset Password */
  resetpwd = async () => {
    let data = { ...this.state.data };
    const errors = { ...this.state.errors };
    this.setState({
      submit_status: true
    });
    let result = Joi.validate(data, this.schema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
      this.setState({
        submit_status: false
      });
    } else {
      try {
        const response = await authServices.resetPassword(
          data,
          this.state.userid
        );
        if (response) {
          if (response.data.status === 1) {
            data["password"] = "";
            data["retype_password"] = "";
            this.setState({ data });
            this.setState({
              submit_status: false,
              message: response.data.message,
              response_type: "sucess"
            });
          } else {
            this.setState({
              submit_status: false,
              message: response.data.message,
              response_type: "error"
            });
          }
        }
      } catch (err) {}
    }
  };

  render() {

    const {authUser, showHideLogin} = this.props;
    if(authUser.email) {
        return <Redirect to="/" />;
    } else {
      return (
        <React.Fragment>
          <div className="main">
            <Header />
            <Container>
              <BlockUi tag="div" blocking={this.state.submit_status} >
              <Row>
              <div id="reset-password-form">
                <div className="reset-password">
                  <h1>RESET YOUR PASSWORD</h1>
                  <p className={this.state.response_type}>
                    {this.state.message}
                    {this.state.response_type == 'sucess' &&
                    <Link to="#" onClick={()=> showHideLogin(true, 'login')}>login</Link>
                    }
                  </p>
                  <div className="form-group">
                    <label>New password</label>
                    <input
                      type="password"
                      name="password"
                      className="form-control"
                      value={this.state.data.password}
                      onChange={this.handleChange}
                    />
                    {this.state.errors && this.state.errors.password ? (
                      <div className="danger">
                        <p
                          style={{
                            fontWeight: "600",
                            marginBottom: "4px",
                            fontFamily: "Comfortaa - bold"
                          }}
                        >
                          <strong>
                            The password you’ve entered is not strong enough; it
                            must have:
                          </strong>
                        </p>
                        <p style={{ marginBottom: "0px" }}>
                          Minimum 8 characters required{" "}
                        </p>
                        <p style={{ marginBottom: "0px" }}>
                          Password should contain a special character
                        </p>
                        <p style={{ marginBottom: "0px" }}>
                          Password should contain a number
                        </p>
                        <p style={{ marginBottom: "0px" }}>
                          Password should contain a letter
                        </p>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="form-group">
                    <label>Re-type password</label>
                    <input
                      type="password"
                      name="retype_password"
                      className="form-control"
                      value={this.state.data.retype_password}
                      onChange={this.handleChange}
                    />
                    {this.state.errors && this.state.errors.retype_password ? (
                      <div className="danger" style={{ paddingTop: "10px" }}>
                        {this.state.errors.retype_password}
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <button className="blue-btn" onClick={this.resetpwd}>
                    SUBMIT
                  </button>
                </div>
              </div>
              </Row>
              </BlockUi>
            </Container>
            <Footer />
          </div>
        </React.Fragment>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  authUser: state.auth.authUser,    
});
export default connect(mapStateToProps, { 
  showHideLogin
}) (ResetPassword)

