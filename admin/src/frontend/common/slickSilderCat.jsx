import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Slider from "react-slick";

class SlickSilderCat extends Component {
  vaildUrl(url) {
    var expression =
      /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
    var regex = new RegExp(expression);
    if (url.match(regex)) {
      return true
    } else {
      return false
    }
  }


  render() {
    const { categoryInfo } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      autoplay : true,
    };

    return (
      <React.Fragment>
        <div className="ipc-catogery-silder">
          <Slider {...settings}>
            {categoryInfo.map((data, index) => {
              let bg = '';
              if (this.vaildUrl(data.image)) {
                bg = data.image
              } else {
                bg = require("../../assets/images/frontend/" + data.image);
              }

              return (
                <React.Fragment key={index}>
                  <div
                    className=""
            
                  >
                    <Container className="banner-container max_width_1080">
                      <Row>
                        
                        <Col className="banner-content col-md-4  col-xs-12">
                          {data.title ? <h4 dangerouslySetInnerHTML={{
                            __html: data.title,
                          }}></h4> : ''}
                          <p
                            dangerouslySetInnerHTML={{
                              __html: data.description,
                            }}
                          />
                        </Col>
                        <Col   className=" col-md-8 col-xs-12"><img className="img-fluid" src={bg}></img></Col>
                      </Row>
                    </Container>
                  </div>
                </React.Fragment>
              );
            })}
          </Slider>
        </div>
      </React.Fragment>
    );
  }
}

export default SlickSilderCat;
