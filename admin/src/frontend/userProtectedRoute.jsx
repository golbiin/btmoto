import React from "react";
import { Route, Redirect } from "react-router-dom";
import * as authServices from "../ApiServices/login";
const UserProtectedRoutea = ({ component: Component, render, ...rest }) => {

  return (
    <Route
      {...rest}
      render={(props) => {
        if (!authServices.getCurrentUser())
          return <Redirect to="/" />;
        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default UserProtectedRoutea;
