import React, { Component } from 'react';
import { Link } from "react-router-dom";
import BlockUi from 'react-block-ui';
import * as checkoutService from '../../ApiServices/checkout';
import {  Row, Col } from 'react-bootstrap';

class CheckoutThankYou extends Component {
    constructor(props) {
        super(props);
        this.state = {
            blocking : false,
            status: false,
            order: {
                order_details: {
                    products:[],
                    total: {
                        product_total: 0,
                        discount: 0,
                        shipping_total: 0,
                        tax_total: 0,
                        total: 0
                    }
                },
                coupon_details :{
                    couponcode:0,
                }
            },
             data: [],
             products_order:[],
             dataTrash: [],
            message: "",
            toggleCleared: false,
            spinner: true,

        }
    }

    componentDidMount = () => {
        this.getOrder();   
    };
    /* Get order */
    getOrder = async () => {
        let orderId   = this.props.match.params.order_id;
        this.setState({ blocking: true});
        try {
            const response =  await checkoutService.getOrder(orderId);
            if(response.status === 200 ) {
                if (response.data.order ) {
                    this.setState({ products_order: response.data.order.order_details.product});
                    this.setState({ order: response.data.order});
                    this.setState({ status: true});
                    this.setState({message: "Thank you for your order #"+response.data.order.order_no});
                } else {
                    this.setState({message: "Sorry! We could not find any orders"});
                }
                
            }
            this.setState({ blocking: false});
        } catch (err) {
            this.setState({ blocking: false});
        }
    }

    render() { 
        const images = require.context("../../assets/images", true);
        const {order,products_order} = this.state;
        return ( 
            <React.Fragment>  
             <div className="checkout-page-wrapper">
                <div className="page-box">
                    <div className="logo-box">
                        <Link className="logo" to="/">
                            <img
                            className="logoimg"
                            alt=""
                            src={images(`./frontend/cimon-logo-checkout.png`)}
                            ></img>
                        </Link>
                    </div>                     
                    <div className="content-wrapper">
                    <BlockUi tag="div" blocking={this.state.blocking} >
                    <h4 className="empty-cart">{this.state.message}</h4>
                    </BlockUi>
                    </div>
                    {this.state.status &&
                    <div className="col-md-6 order-details">
                       <h4>Order Details</h4>
                           <div className="products-details">
                           <ul class="cart-list">
                               <li class="item">
                               <Row>
                                    <Col lg={4} md={8} sm={4} className="product-item">
                                    <div className="title-box">
                                        <h5>Product Name</h5>
                                        {products_order.map((data, index) => {
                                            return(
                                                <span key={index}>{data.product_name}<br></br></span>
                                            );
                                            })}   
                                    </div>
                                    </Col>
                                    <Col lg={4} md={8} sm={4} className="product-item">
                                    <div className="title-quality">
                                    <h5>Quantity</h5>
                                        {products_order.map((data, index) => {
                                            return(
                                                <span>{data.quantity}<br></br> </span>
                                            );
                                            })}   
                                    </div>
                                    </Col>
                                    <Col lg={4} md={8} sm={4} className="product-item">
                                    <div className="title-price">
                                    <h5>Product Price</h5>
                                        {products_order.map((data, index) => {
                                            return(
                                                <span>${data.line_item_total}<br></br> </span>
                                            );
                                            })}   
                                    </div>
                                    </Col>
                                </Row>
                                </li>
                            </ul>
                           </div>
                        <div class="total-box">
                            <ul>
                                <li><h6>Subtotal</h6><span>${order.order_details.total.product_total}</span></li>
                                {
                                    (order.order_details.total.discount > 0) &&
                                    <li >
                                        <h6>Discount <br/>Coupon Apllied : {order.coupon_details.couponcode}</h6>
                                        <span className="red">-${order.order_details.total.discount}</span>
                                    </li>
                                }
                                <li><h6>Shipping</h6><span>${order.order_details.total.shipping_total}</span></li>
                                <li><h6>Tax</h6><span>${order.order_details.total.tax_total}</span></li>
                                <li class="total"><h6>TOTAL</h6><strong>${order.order_details.total.total}</strong></li>
                            </ul>
                        </div>
                    </div>   
                    }
                </div>
             </div>
            </React.Fragment>
        );
    }
}
export default CheckoutThankYou