import React, {  useMemo, useState, useEffect, useReducer,  } from 'react';
import { Link } from "react-router-dom";
import {CardNumberElement,CardCvcElement,CardExpiryElement,useStripe,useElements,} from '@stripe/react-stripe-js';
import { Button, Container, Row, Col, Form } from 'react-bootstrap';
import alertify from "alertifyjs";
import * as checkoutService from '../../ApiServices/checkout';
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers";
import * as yup from "yup";
import BlockUi from 'react-block-ui';
import PropTypes from 'prop-types';
import util from '../../services/util';
import { CURRENCY_SYMBOL, CURRENCY_CODE } from '../../config.json';
import { stateData } from "../../state";
import { Helmet } from 'react-helmet';
const TITLE = 'Checkout | Automation Company - Industrial Automation';

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const CheckoutsSchema = yup.object().shape({
    first_name: yup.string("")
                    .required("Please enter your first name.")
                    .matches("^[a-zA-Z ]*$", "Please enter alphabets only")
                    .min(3, "First name must contains atleast 3 characters")
                    .max(50, "Max length of first name is 50 charcters"),
    last_name: yup.string()
                    .required("Please enter your last name.")
                    .matches("^[a-zA-Z ]*$", "Please enter alphabets only")
                    .min(3, "Last name must contains atleast 3 characters")
                    .max(50, "Max length of last name is 50 charcters"),
    email: yup.string()
                    .required("Please enter your email address.")
                    .email("Please enter valid email"),
    address: yup.string()
                    .required("Please enter your address.")
                    .min(3, "Address must contains atleast 3 characters")
                    .max(500, "Max length of address is 500 charcters"),
    appartment : yup.string(),
    city: yup.string()
                    .required("Please enter your City.")
                    .min(3, "City must contains atleast 3 characters")
                    .max(200, "Max length of city is 200 charcters"),
    country: yup.string()
                .required("Please select your Country."),
    region: yup.string()
                .required("Please select your State."),
    zipcode: yup.string()
                .required("Please enter your ZIP Code."),
    phone: yup.string()
                .required("Phone number is required")
                .max(15, "Phone Number cannot be more than 15 digits long")
                .matches(phoneRegExp, 'Phone number is not valid'),
    shipping_methods:yup.string()
                .required("Please select from the available few shipping options."),

});

const useOptions = () => {
  const fontSize = 15;
  const options = useMemo(
    () => ({
      style: {
        base: {
          fontSize,
          color: "#606881",
          letterSpacing: "0.025em",
          fontFamily: "Arial Regular",
          "::placeholder": {
            color: "#606881"
          },
          border: "1px solid"
        },
        invalid: {
          color: "#9e2146"
        }
      }
    }),
    [fontSize]
  );

  return options;
};

const CheckoutForm = ({ removeCoupon, token, authUser, cartId, cartProducts, cartTotal, loadCart, 
    showCartFlag, clearCart, callbackRedirect, couponDetails }) => {
    const { register, handleSubmit, errors } = useForm({
        resolver: yupResolver(CheckoutsSchema)
    });
    
    const [isProcessing, setProcessingTo] = useState(false); // form submit
    const [isShippingProcessing, setShippingProcessingTo] = useState(false); // shipping method 
    const [isErrors, setIsErrors] = useState([]); // server side validation
    const [checkoutError, setCheckoutError] = useState(); // cart details error
    const [shippingError, setShippingError] = useState(); // ups validation errors
    const [shippingMethods, setShippingMethods] = useState([]);
    const [shippingChargeSelected, setShippingChargeSelected ] =  useState(0);
    const [shippingServiceSelected, setShippingServiceSelected ] =  useState(0);
    const [taxCharge, setTaxCharge ] =  useState(0);
    const [shipping, setShipping] = useReducer(
                                        (state, newState) => ({...state, ...newState}),
                                        {
                                            first_name: authUser.first_name,
                                            last_name: authUser.last_name,
                                            email: authUser.email,
                                            company: authUser.company_name,                                    
                                            address: '',
                                            appartment: '',
                                            city: authUser.city,
                                            country: authUser.country,
                                            region : '',
                                            zipcode: authUser.zipcode,
                                            phone:authUser.phone_number,
                                        }
                                    );
    const stripe = useStripe();
    const elements = useElements();
    const options = useOptions();
    
    const images = require.context("../../assets/images", true);
    const handleShippingChange = evt => {
        
        const name = evt.target.name;
        const newValue = evt.target.value;      
        setShipping({[name]: newValue});  
    }

    const handleShippingBlur = e => {
        upsValidateAddress();
    }

    const handleShippingMethodChange = (evt, charge) => {
        const value = evt.target.value;   
        setShippingChargeSelected(charge);    
        setShippingServiceSelected(value);       
    }

    useEffect(() => {
        upsValidateAddress();
    }, [shipping.country]);
    useEffect(() => {
        upsValidateAddress();
    }, [shipping.region]);
    useEffect(() => {
        upsValidateAddress();
    }, [shipping.zipcode]);

    const upsValidateAddress = async ( ) => {
        var isValidZip = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(shipping.zipcode);
        if(shipping.country && shipping.region && shipping.city &&  shipping.zipcode && shipping.address && isValidZip) {
            setShippingProcessingTo(true);
            setShippingMethods([]);
            setShippingChargeSelected(0);
            setShippingServiceSelected(''); 
            const response =  await checkoutService.upsValidateAddress(shipping, cartProducts, cartTotal);
            if(response.data.status == 1) {
                setShippingMethods(response.data.rating);
                setTaxCharge(response.data.tax)
                setShippingError(''); 
            } else {                
                setShippingError(response.data.message);
            }
            setShippingProcessingTo(false);
        }
    }

    const handleCardDetailsChange = ev => {
        ev.error ? setCheckoutError(ev.error.message) : setCheckoutError();
    };

    let total =   0;
    const onSubmit = async (data, e) => {
        if (!stripe || !elements) {
            return;
        }
        const cardElementNumber = elements.getElement(CardNumberElement);
        const shippingDetails = data;
        setProcessingTo(true);
        const billingDetails = {
            name: data.first_name+" "+data.last_name,
            email: data.email,
            address: {
                city: data.city,
                line1: data.address,
                state: data.region,
                postal_code: data.zipcode
            }
        };
        try {

            const {error, paymentMethod} = await stripe.createPaymentMethod({
                type: "card",
                card: cardElementNumber,
                billing_details: billingDetails
            });

            if (error) {            
                setCheckoutError(error);
                setProcessingTo(false);
                return;
            } 
            if(!error) {

                const { id } = paymentMethod;      
                try {
                    const response = await checkoutService.checkout(id, shippingDetails, cartProducts, cartTotal, cartId, couponDetails,token, authUser);  
                    const data = response.data
                    if(response.status == 200 && response.data.status == 1) {
                        
                        e.target.reset();
                        clearCart();
                        
                        callbackRedirect(true, response.data.order.order._id);
                
                    } else if (response.data.status == 2) {
                        setIsErrors(response.data.message);
                    } else {
                        alertify.error(data.message)
                    } 
                    setProcessingTo(false);           
                } catch (error) {
                    setProcessingTo(false);
                }

            }
        } catch (err) {
            setCheckoutError(err);
            setProcessingTo(false);
        }
    };


    let taxPrice        =   0;
    let discount = couponDetails.discount;
    let cartTotalPrice  =   parseFloat(util.formatPrice(cartTotal.totalPrice, CURRENCY_CODE));  
    let cartDiscountTotal = (parseFloat(cartTotalPrice) - parseFloat(discount)) > 0 ? (parseFloat(cartTotalPrice) - parseFloat(discount)) : 0;   
    let totalPrice      =   parseFloat(cartDiscountTotal)+ parseFloat(shippingChargeSelected);
    

    if(Object.keys(taxCharge).length > 0 ) {
        if(taxCharge.shipping) {
            taxPrice =  ( ( totalPrice* parseFloat(taxCharge.rate_percent) ) / 100);
        } else {
            taxPrice    =  (( cartTotalPrice* parseFloat(taxCharge.rate_percent)) / 100);
        }
        taxPrice = taxPrice > 0 ? taxPrice.toFixed(2) : taxPrice;
    } 
    taxPrice = util.formatPrice(parseFloat(taxPrice));
    let grandTotal = util.formatPrice(parseFloat(totalPrice)+parseFloat(taxPrice));   
 
    return (

        <div>    
        <BlockUi tag="div" blocking={isProcessing || isShippingProcessing} >
        <Helmet>
        <title>{ TITLE }</title>
        </Helmet>
        <Form name="checkoutForm" id="checkoutForm" className="shipping-form" onSubmit={handleSubmit(onSubmit)}>
            <Container>
            <Row>
                    <Col md={6} className="left-section">
                        <h2>Checkout</h2>
                        <h4>shipping details</h4>
                        
                        <Row>
                            <Col md={6}>
                                <Form.Group controlId="fname" className="shipping-form-group">                  
                                    <Form.Control name="first_name" className="shipping-inputs" type="text" placeholder="First Name *" 
                                    ref={register}
                                    onChange={handleShippingChange}
                                    
                                    defaultValue={authUser.first_name}
                                    />
                                    {errors.first_name && <p className="danger">{errors.first_name.message}</p>}
                                    {isErrors.first_name && <p className="danger">{isErrors.first_name}</p>}

                                </Form.Group>
                            </Col>
                            <Col md={6}>
                                <Form.Group controlId="lname" className="shipping-form-group">                  
                                    <Form.Control name="last_name" className="shipping-inputs" type="text" placeholder="Last Name *" 
                                    ref={register}
                                    onChange={handleShippingChange} 
                                    defaultValue={authUser.last_name}
                                    />
                                    {errors.last_name && <p className="danger">{errors.last_name.message}</p>}
                                    {isErrors.last_name && <p className="danger">{isErrors.last_name}</p>}
                                </Form.Group>
                            </Col>
                            <Col xs={12}>
                                <Form.Group controlId="email" className="shipping-form-group">                  
                                    <Form.Control 
                                    onChange={handleShippingChange} 
                                    defaultValue={authUser.email}
                                    name="email" className="shipping-inputs" type="email" placeholder="Email *" ref={register} />
                                    {errors.email && <p className="danger">{errors.email.message}</p>}
                                    {isErrors.last_name && <p className="danger">{isErrors.last_name}</p>}
                                </Form.Group>
                            </Col>
                            <Col xs={12}>
                                <Form.Group controlId="company" className="shipping-form-group">                  
                                    <Form.Control 
                                    onChange={handleShippingChange} 
                                    defaultValue={authUser.company_name}
                                    name="company" className="shipping-inputs" type="text" placeholder="Company (optional)" ref={register}  />                                
                                </Form.Group>
                            </Col>
                            <Col xs={12}>
                                <Form.Group controlId="address" className="shipping-form-group">                  
                                    <Form.Control 
                                    onBlur={ handleShippingBlur }
                                    onChange={handleShippingChange} 
                                    name="address" className="shipping-inputs" type="text" placeholder="Address *" ref={register} />
                                    {errors.address && <p className="danger">{errors.address.message}</p>}
                                    {isErrors.address && <p className="danger">{isErrors.address}</p>}
                                </Form.Group>
                            </Col>
                            <Col xs={12}>
                                <Form.Group controlId="appartment" className="shipping-form-group">                  
                                    <Form.Control 
                                    onChange={handleShippingChange} 
                                    name="appartment" className="shipping-inputs" type="text" placeholder="Apartment (optional) " ref={register} />
                                </Form.Group>
                            </Col>
                            <Col xs={12}>
                                <Form.Group controlId="city" className="shipping-form-group">                  
                                    <Form.Control 
                                    defaultValue={authUser.city}
                                    onBlur={ handleShippingBlur }
                                    onChange={handleShippingChange}  
                                    name="city" className="shipping-inputs" type="text" placeholder="City *" ref={register}/>
                                    {errors.city && <p className="danger">{errors.city.message}</p>}
                                    {isErrors.city && <p className="danger">{isErrors.city}</p>}
                                </Form.Group>
                            </Col>
                            <Col lg={4}>
                                <Form.Group controlId="city" className="shipping-form-group" >                  
                                    <label>Country/Region *</label>
                                    <select className="form-control" name="country" ref={register} value={shipping.country  }
                                    onChange={handleShippingChange}>
                                        <option value="">Select Country</option>
                                        <option value="US" >United States</option>
                                    </select>
                                    {errors.country && <p className="danger">{errors.country.message}</p>}
                                    {isErrors.country && <p className="danger">{isErrors.country}</p>}
                                </Form.Group>
                            </Col>
                            <Col lg={4} className="no-left-lg">
                                <Form.Group controlId="region" className="shipping-form-group">                  
                                    <label>State *</label>
                                    <select  className="form-control" name="region" ref={register}  value={shipping.region}                           
                                    onChange={handleShippingChange}
                                    >
                                        <option value="">Select Region</option>
                                        {stateData.map((data, key) => {
                                        return (
                                            <option key={key} value= {data.abbreviation} >
                                            {data.name}
                                            </option>
                                        );
                                        })}
                                    </select>
                                    {errors.region && <p className="danger">{errors.region.message}</p>}
                                    {isErrors.region && <p className="danger">{isErrors.region}</p>}
                                </Form.Group>
                            </Col>
                            <Col lg={4} className="no-left-lg">
                                <Form.Group controlId="zip" className="shipping-form-group">                  
                                    <Form.Control 
                                    defaultValue={authUser.zipcode}
                                    onChange={handleShippingChange}
                                    name="zipcode" className="shipping-inputs" type="text" placeholder="ZIP Code *" ref={register} />
                                    {errors.zipcode && <p className="danger">{errors.zipcode.message}</p>}
                                    {isErrors.zipcode && <p className="danger">{isErrors.zipcode}</p>}
                                </Form.Group>
                            </Col>
                            <Col xs={12}>
                                <Form.Group controlId="phone" className="shipping-form-group">                  
                                    <Form.Control 
                                    defaultValue={authUser.phone_number}
                                    onChange={handleShippingChange} 
                                    name="phone" className="shipping-inputs" type="text" placeholder="Phone *" ref={register} />
                                    {errors.phone && <p className="danger">{errors.phone.message}</p>}
                                    {isErrors.phone && <p className="danger">{isErrors.phone}</p>}
                                </Form.Group>
                            </Col>
                        </Row>
                        <br/>
                        <div className="payment-box">
                            {shippingMethods.length > 0 && <h4>Shipping Methods *</h4>}
                            <Row>
                            {shippingMethods.map((rating, index) => (
                                <Col xs={12} key={`inline-${rating.code}`}>                                        
                                    <Form.Check 
                                    ref={register}
                                    onChange={e => handleShippingMethodChange (e, rating.charge) } 
                                    name="shipping_methods" value={rating.code} inline label={rating.service + ' ('+ rating.currency +' '+ rating.charge + ')'} type="radio" id={`inline-${rating.code}`} />                                
                                </Col>
                            ))}
                            {errors.shipping_methods && <Col xs={12}><p className="danger">{errors.shipping_methods.message}</p></Col>}
                            {isErrors.shipping_methods && <Col xs={12}><p className="danger">{isErrors.shipping_methods}</p></Col>}
                            </Row>
                        </div>              
                    </Col>
                    <Col md={6} className="right-section">
                        <div className="top-box">
                            <h4>
                                <span>your order</span> 
                                <Link to="#" onClick={() => loadCart(true)}>edit your cart</Link>
                            </h4>
                        </div>
                        <ul className="cart-list">
                            {cartProducts.map( (product, index) => {
                                let formattedPrice =    0, productPrice = 0;
                                if(product.price) {
                                    let price = util.getPrice(product.price);
                                    formattedPrice = util.formatPrice(price);
                                }
                                return(
                                    <>
                                    <li className="item" key={index}>
                                        <Row className="align-items-center">
                                            <Col lg={6} md={8} sm={6} className="product-item">
                                                <div className="img-box">
                                                    <img src={
                                                        product.images.featured_image.image_url != null
                                                            ? product.images.featured_image.image_url
                                                            : images(`./frontend/news-placeholder1.png`)
                                                    }>
                                                    </img>
                                                </div>
                                                <div className="title-box">
                                                    <h4>
                                                        <Link to={product.url+product.slug}>
                                                        {product.product_name != null ? product.product_name : ""}
                                                        </Link>
                                                    </h4>
                                                </div>
                                            </Col>
                                            <Col lg={6} md={4} sm={6} xs={12}>
                                                <Row className="align-items-center">
                                                    <Col lg={6} md={12} sm={6} xs={6} className="quantity">
                                                        <div className="qty-box">                                                        
                                                            <div className="quantity">{product.quantity}</div>                                                        
                                                        </div>
                                                    </Col>
                                                    <Col lg={6} md={12} sm={6} xs={6} className="price">
                                                        <p> {formattedPrice !== "" ? CURRENCY_SYMBOL+''+formattedPrice : "0.00" }</p>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </li>
                                    </>
                            )
                            })}
                        </ul>
                        <div className="total-box">
                            <ul>
                                <li>
                                    <span>Subtotal</span>
                                    <span>{CURRENCY_SYMBOL+cartTotalPrice}</span>
                                </li>
                                { couponDetails.couponcode != '' && (
                                        <li>
                                            <span>Coupon Applied<br/>{couponDetails.couponcode}</span>
                                            <span>- { CURRENCY_SYMBOL }{couponDetails.discount} <button onClick={removeCoupon} className="btn-remove-coupon">[Remove]</button></span>
                                        </li>
                                )}
                                <li>
                                    <span>Shipping</span>
                        <span>{CURRENCY_SYMBOL}{shippingChargeSelected}</span>
                                </li>
                                <li>
                                    <span>Tax</span>
                                    <span>{CURRENCY_SYMBOL}{taxPrice}</span>
                                </li>
                                
                                <li className="total">
                                    <span>TOTAL</span>
                                    <span>{CURRENCY_SYMBOL}{grandTotal}</span>
                                </li>
                            </ul>
                        
                        </div>
                        <div className="payment-box">
                            <h4>payment details</h4>
                            <Row>
                                <Col xs={12}>
                                    <Form.Group controlId="card_number" className="shipping-form-group">                  
                                        <label>Card Number *</label>
                                        <CardNumberElement
                                            options={options}
                                            onReady={() => {
                                            
                                            }}
                                            onChange={handleCardDetailsChange}
                                            onBlur={() => {
                                            
                                            }}
                                            onFocus={() => {
                                            
                                            }}
                                            placeholder="2568 -XXXX - XXXX - XXXX"
                                        />
                                 </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group controlId="valid" className="shipping-form-group">                  
                                        <label>Valid Through *</label>
                                        <CardExpiryElement
                                            options={options}
                                            onReady={() => {
                                            
                                            }}
                                            onChange={handleCardDetailsChange}
                                            onBlur={() => {
                                            
                                            }}
                                            onFocus={() => {
                                            
                                            }}
                                        />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group controlId="cvv" className="shipping-form-group">                  
                                        <label>CVC Code *</label>
                                        <CardCvcElement
                                            options={options}
                                            onReady={() => {
                                            
                                            }}
                                            onChange={handleCardDetailsChange}
                                            onBlur={() => {
                                            
                                            }}
                                            onFocus={() => {
                                            
                                            }}
                                        />
                                    </Form.Group>
                                </Col>
                                <Col xs={12}>                
                                {shippingError && <p className="danger">{shippingError}</p>}                 
                                {checkoutError && <p className="danger">{checkoutError.message}</p>}        
                                <Button className="order-button" variant="primary" type="submit" disabled={isProcessing || !stripe || isShippingProcessing }>
                                {isProcessing ? "Processing..." : `Place Order`}
                                </Button>
                                </Col>
                            </Row>
                        </div>                    
                    </Col>
                </Row>
            </Container>
        </Form>           
        </BlockUi>
        </div>        
    );
};

CheckoutForm.propTypes = {
    cartProducts: PropTypes.array.isRequired,
    cartTotal: PropTypes.object.isRequired,    
    clearCart: PropTypes.func.isRequired,    
    couponDetails : PropTypes.object.isRequired
};
export default CheckoutForm;
