import React, { Component } from "react";
import Header from "./common/header";
import Footer from "./common/footer";
import SubFooter from "./common/subFooter";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
const TITLE = "Success Stories | Automation Company - Industrial Automation";

class SuccessStories extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  scrollToMyRef = () => window.scrollTo(0, this.myRef.current.offsetTop);
  state = {};
  render() {
    const images = require.context("../assets/images", true);
    return (
      <React.Fragment>
        <Helmet>
          <title>{TITLE}</title>
        </Helmet>
        <Header />
        <div className="container-fluid">
          <div id="successStories">
            <div className="bannerWrapper">
              <div className="sip_banner_bg">
                <div className="sbg"></div>
                <div className="container">
                  <div className="hedng">
                    <h1>HELPING BUILD THE FACTORIES OF THE FUTURE</h1>
                  </div>
                  <Link
                    onClick={this.scrollToMyRef}
                    to="#"
                    className="scroll-down-link"
                  ></Link>
                </div>
              </div>
            </div>
            <div className="container" ref={this.myRef}>
              <div className="row blogSection">
                <div className="col-md-3 col-sm-6 col-xs-12 blogListing">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/farm.jpg`)}
                  ></img>
                  <h3>
                    {" "}
                    <Link to="#" className="">
                      Smart Farm System Build
                    </Link>
                  </h3>
                  <p>
                    CIMON’s Smart Farm System technology can build the optimal
                    environment for crops and livestock. CIMON’s Smart Farm
                    System technology can provide the optimal environment for
                    crops and livestock.“Smart Farm” is a combination of
                    farming…
                  </p>
                </div>
                <div className="col-md-3 col-sm-6 col-xs-12 blogListing">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/Fotolia.jpg`)}
                  ></img>
                  <h3>
                    {" "}
                    <Link to="#" className="">
                      Smart Factory Build
                    </Link>
                  </h3>
                  <p>
                    Smart Factory Build Smart factories make mass customization
                    possible. Various production lines connect and exchange data
                    using IoT devices and RFID which allow for shifts in the
                    production line dependent on customer needs. Through…
                  </p>
                </div>
                <div className="col-md-3 col-sm-6 col-xs-12 blogListing">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/smart_bg.jpg`)}
                  ></img>
                  <h3>
                    {" "}
                    <Link to="#" className="">
                      Heating, Ventilating, and Air Conditioning System
                    </Link>
                  </h3>
                  <p>
                    Heating, Ventilating, and Air Conditioning System Building
                    heating, ventilation and cooling systems is an essential
                    function which circulates air for fresh air flow and
                    humidity control. Heating and cooling systems are also
                    important due…
                  </p>
                </div>
                <div className="col-md-3 col-sm-6 col-xs-12 blogListing">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/iStock.jpg`)}
                  ></img>
                  <h3>
                    {" "}
                    <Link to="#" className="">
                      Hospital Automatic Control System
                    </Link>
                  </h3>
                  <p>
                    Hospital Automatic Control System Small issues can cause a
                    huge disruption if an unexpected problem occurs in a
                    hospital environment. Treatment for the patients is
                    dependent on electricity, therefore, a stable and efficient
                    total solution…
                  </p>
                </div>
                <div className="col-md-3 col-sm-6 col-xs-12 blogListing">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/main_bg5.jpg`)}
                  ></img>
                  <h3>
                    {" "}
                    <Link to="#" className="">
                      Power Monitoring and Control System
                    </Link>
                  </h3>
                  <p>
                    Power Monitoring and Control System For most companies,
                    electricity costs account for a large percentage of
                    operating costs and electricity consumption increases due to
                    an unpredictable climate. CIMON’s power monitoring control
                    system…
                  </p>
                </div>
                <div className="col-md-3 col-sm-6 col-xs-12 blogListing">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/iStocklast.jpg`)}
                  ></img>
                  <h3>
                    {" "}
                    <Link to="#" className="">
                      Water Supply Facility Management System
                    </Link>
                  </h3>
                  <p>
                    Water Supply Facility Management System Water is
                    irreplaceable and is one of the valuable resources we have.
                    Rapid urbanization and concentration of population create an
                    unequal distribution of water resources. CIMON’s water
                    resources…
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <SubFooter />
        <Footer />
      </React.Fragment>
    );
  }
}

export default SuccessStories;
