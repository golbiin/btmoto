import React, { Component } from "react";
import PropTypes from 'prop-types';

/* Quantity dropdown for all product pages */
class Quantity extends Component {

    handleQuantityChange = (event, product, type) => {
        this.props.changeProductQuantity(event.target.value, product, type)
    }
    render() {
        const { product , type } = this.props; 
        let options = [];
        for (var i = (type === 'plp-multiple' ? 0 : 1); i <= 10; i++) {
            options.push(<option  key={i} value={i}>{i}</option>);
        }
        return (
        <React.Fragment>
            <select name="quantity[]" onChange={(e) => this.handleQuantityChange(e, product, type) } >
            {options}
            </select>
        </React.Fragment>
        );
    }
} 
Quantity.propTypes = {
    changeProductQuantity: PropTypes.func.isRequired,
};
export default Quantity;
