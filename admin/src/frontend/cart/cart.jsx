import React from 'react'
import { Button, Modal, Row, Col, Form } from 'react-bootstrap';
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import alertify from "alertifyjs";
import BlockUi from 'react-block-ui';
import util from '../../services/util';
import Joi from "joi-browser";
import * as checkoutService from "../../ApiServices/checkout";
import { ups } from './../../config.json';
import { CURRENCY_SYMBOL, CURRENCY_CODE } from '../../config.json';

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errors: {},
            data: { 
                coupon_code: ''
            },
            avaialableShippingMethods:[]
        }
    }

    /*Coupon validation schema */
    couponSchema = {
        coupon_code: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Coupon code is required",
            };
        }),
    };

    componentDidMount = async() => {
        this.getShippingMethodsAvailable();
    }

    /*Get available shipping methods */
    getShippingMethodsAvailable = async () => {
        const response = await checkoutService.getAvailableShippingMethod();
        if (response.data.status === 1) {
            this.setState( {avaialableShippingMethods: response.data.methods});
        }
    }

    /*on change input save data*/
    handleChange = ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const data = { ...this.state.data };
        const errorMessage = this.validateProperty(input);
        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        data[input.name] = input.value;   
        this.setState({ data, errors });    
    };

    /*JOI VALIDATION CALL*/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const couponSchema = { [name]: this.couponSchema[name] };
        const { error } = Joi.validate(obj, couponSchema);
        return error ? error.details[0].message : null;
    };

    /* Check coupon valid */
    handleCouponSubmit = async (event) => {
        event.preventDefault();
        const data = { ...this.state.data };
        const errors = { ...this.state.errors };
        let result = Joi.validate(data, this.couponSchema);
        if (result.error) {
        let path = result.error.details[0].path[0];
        let errormessage = result.error.details[0].message;
        errors[path] = errormessage;
        this.setState({ errors });
        } else {
            this.props.applyCoupon(data);
        }
    }

    /*Remove product from cart */
    handleRemoveProduct(product) {
        const { removeProduct } = this.props;  
        alertify.confirm('Remove Item', 'Are you sure you want to remove this item?', function(){
            removeProduct(product);
        } , function(){ }).showModal ('remove-cart-product-modal');
    }

	render() {

        const images = require.context("../../assets/images", true);
        const { cartProducts, cartTotal,changeQuantity, hideCart, isCartProcessing, couponDetails, removeCoupon } = this.props;  
        const cartProductsItems =   [];
        let cartTotalPrice   =  util.formatPrice(cartTotal.totalPrice ? cartTotal.totalPrice: 0 , CURRENCY_CODE); 
        for (const [index, product] of cartProducts.entries()) {       
            let formattedPrice =    0;
            let price = util.getPrice(product.price);
            formattedPrice = util.formatPrice(price);
             
            let productUrl = '/';
            if(product.url.indexOf("scada") !== -1){
                productUrl =  '/introduction/scada'
              } else{
                productUrl = product.url+product.slug
            }
            cartProductsItems.push(
            <Row className="product-details" key={index}>
                <Col xs={12} md={3}>
                    <img alt="product image" className="pro-img" 
                    src={
                        product.images.featured_image.image_url != null
                            ? product.images.featured_image.image_url
                            : images(`./frontend/news-placeholder1.png`)
                    }>
                    </img>
                </Col>                                            
                <Col xs={12} md={3}>
                    <h3> <Link to={productUrl}>{product.product_name != null ? product.product_name : ""} </Link> </h3>
                </Col>
                <Col xs={12} md={2} sm={6}>
                <div className="qty-box">
                        <Button className="minus" onClick={() => changeQuantity(product, 'decrease')}>-</Button>
                        <div className="quantity">{product.quantity}</div>
                        <Button className="minus" onClick={() => changeQuantity(product, 'increase')}>+</Button>
                    </div>
                </Col>
                <Col xs={12} md={2} sm={6}>
                    <h4 className="price">
                    {formattedPrice !== "" ? CURRENCY_SYMBOL+''+formattedPrice : "0.00" }
                    </h4>  
                </Col>
                <Col xs={12} md={2} sm={6}>
                <Link to="#" className="remove" href="#" onClick={() => this.handleRemoveProduct(product)}>Remove</Link>  
                </Col>
            </Row>            
            );
        }
        const availableShippings = [];
        for (const [index, methodId] of this.state.avaialableShippingMethods.entries()) {
            const method = ups.services.find((m) => m.id === methodId);
            availableShippings.push(
                <Col xs={12} sm={6} md={4} key={index} className="content">
                    <div className="img-box">
                        <img alt="product image" src={images(`./frontend/cart-shipping-icon.png`)}></img>
                    </div>
                    <div className="text-box">
                        <h4>{method.name}</h4>
                    </div>
                </Col>        
            )
        }
		return (
			<Modal
                show={this.props.show} onHide={() => hideCart(!this.props.show)}
				size="xl"
				aria-labelledby="contained-modal-title-vcenter"
                dialogClassName ="cart-popup">
				<Modal.Header closeButton>
                    <h3>Your cart {!cartProductsItems.length && (" is empty")}</h3>
				</Modal.Header>
                {(()=>{
                    if(cartProductsItems.length) {
                        return (
                        <Modal.Body> 
                            <BlockUi tag="div" blocking={isCartProcessing} >                         
                            <Row>
                                <Col xs={12} md={9} className="left-section">
                                    <Row className="item">                              
                                        <Col xs={12} md={12} sm={6} className="item-details">
                                            
                                            {cartProductsItems}                                          
                                        </Col>
                                        <Col xs={12} md={12} sm={6} className="coupon-box">
                                            <Form name="couponForm" id="couponForm" onSubmit={this.handleCouponSubmit}>
                                            <Row>                                            
                                            <div className="coupon-field">
                                                <Form.Control value={this.state.data.coupon_code} name="coupon_code" className="coupon-inputs" type="text" placeholder="Coupon Code" 
                                                onChange={this.handleChange}
                                                />  
                                                {this.state.errors.coupon_code ? (
                                                    <div className="danger">{this.state.errors.coupon_code}</div>
                                                ) : ( "")}
                                            </div>
                                            <div className="coupon-apply-btn">
                                            <Button className="apply_coupon" type="submit">Apply Coupon</Button>
                                            </div>                                            
                                            </Row>
                                            </Form>                                             
                                        </Col>                                
                                    </Row> 
                                    <Row className="delivery-methods">
                                        <Col xs={12} className="title">
                                            <h3>Available delivery methods. Exclusions apply.</h3>
                                        </Col>
                                        {availableShippings}                         
                                    </Row>
                                </Col>
                                <Col xs={12} md={3} className="right-section">                                    
                                    <div className="tax-box box">
                                        <span className="text">Shipping Charge</span>
                                        <span className="value">Applicable charge will be added upon confirmation of shipping address</span>
                                    </div>
                                    <div className="tax-box box">
                                        <span className="text">Sales Tax</span>
                                        <span className="value">Applicable taxes will be added upon confirmation of shipping address</span>
                                    </div>                                    
                                    <div className="total-box">
                                        <span className="text">Total</span>
                                        <span className="value">{CURRENCY_SYMBOL}{cartTotalPrice}</span>
                                    </div>
                                    { couponDetails.couponcode !== '' && (
                                        <div>
                                        <div className="tax-box box">
                                            <span className="text">Coupon Applied<br/>{couponDetails.couponcode}</span>
                                            <span className="value red">- { CURRENCY_SYMBOL }{couponDetails.discount} <button onClick={removeCoupon} className="btn-remove-coupon">[Remove]</button></span>
                                        </div>
                                        <div className="total-box">
                                            <span className="text">Grand Total</span>
                                            <span className="value">{CURRENCY_SYMBOL}{cartTotalPrice - couponDetails.discount > 0 ? (cartTotalPrice - couponDetails.discount) : 0.00 }</span>
                                        </div>
                                        </div>
                                    )}                                    
                                    <div className="schedule-box">
                                        <a href="/checkout">Proceed to Checkout</a>
                                    </div>
                                    <div className="pay-box">                                        
                                        <a href="#" className="paypal">
                                            Stripe                                   
                                        </a>
                                    </div>
                                </Col>
                            </Row>  
                            </BlockUi>          
                        </Modal.Body>                    
                        );
                    }

                })()}

            </Modal>
		);
    }
}
Cart.propTypes = {
    cartProducts: PropTypes.array.isRequired,
    cartTotal: PropTypes.object.isRequired,
    removeProduct: PropTypes.func.isRequired,
    hideCart: PropTypes.func.isRequired,
    removeCoupon: PropTypes.func.isRequired,

};
export default Cart;
