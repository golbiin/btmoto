import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import Header from "./common/header";
import Footer from "./common/footer";
import Productbanner from "./common/productBanner";
import DistributorModel from "./common/distributorModel";
import * as StoreServices from "../ApiServices/storeLocators";
import BlockUi from "react-block-ui";
import Autocomplete from "react-google-autocomplete";
import * as pageService from "../ApiServices/page";
import { Helmet } from "react-helmet";

class distributorLocator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalShow: false,
      setModalShow: false,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: { name: "in" },
      productBdata: {
        title: "DISTRIBUTOR & S.I LOCATOR",
        backgroundImage: "distributor-locator-banner.jpg",
      },
      selectedOption: "Distributor",
      storelocationDetails: [],
      searchData: {
        selectedplace: "",
        distance: "",
        latitude: "",
        longitude: "",
      },
      content: {},
      page_title:"",
    };
  }
  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true,
    });
  onMapClicked = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null,
      });
    }
  };

  /* Get Page Contents */
  getContent = async (slug) => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
       /* Banner-Slider Contents */
        if (response.data.data.content.bannerSlider) {
          const Setdata = {};
          response.data.data.content.bannerSlider.map((banner, index) => {
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.backgroundImage = banner.image
              ? banner.image
              : "distributor-locator-banner.jpg";
          });
          this.setState({
            productBdata: Setdata,
          });
        }
        this.setState({ content: response.data.data.content });
        if(response.data.data.page_title){
          this.setState({ 
            page_title: response.data.data.page_title
            });

        }
      } 
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  componentDidMount = () => {
    this.setState({ modalShow: true });
    this.getStoreDetailsBySlug(this.state.selectedOption);
    const slug = this.props.match.path.substring(
      this.props.match.path.lastIndexOf("/") + 1
    );
    this.getContent(slug);
  };

  onValueChange = (e) => {
    this.setState({ selectedOption: e.target.value });
    this.getStoreDetailsBySlug(e.target.value);
  };

  onValueselected = (place) => {
    const location = place.geometry.location;
    var latitude = location.lat();
    var longitude = location.lng();
    this.setState((prevState) => ({
      searchData: {
        ...prevState.searchData,
        selectedplace: place.formatted_address,
        latitude: latitude,
        longitude: longitude,
      },
    }));
  };

  handleChange = async (e) => {
    const value = e.target.value;
    this.setState((prevState) => ({
      searchData: {
        ...prevState.searchData,
        distance: value,
      },
    }));
  };

/*Get Store details */
  getStoreDetailsBySlug = async (data = " ") => {
    this.setState({ blocking: true });
    try {
      let store_category;
      if (data) {
        store_category = data;
      }
      const response = await StoreServices.getStoreDetailsBySlug(
        store_category
      );
      if (response.data.status === 1) {
        this.setState({ blocking: false });
        if (response.data.locatorDetails.length > 0) {
          this.setState({ storelocationDetails: response.data.locatorDetails });
        }
      } else {
        this.setState({ blocking: false });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  /* Search Location */

  searchStoreLocation = async (e) => {
    this.setState({ blocking: true });
    const searchData = { ...this.state.searchData };
    try {
      const response = await StoreServices.searchStoreLocation(searchData);
      
      if (response.data.status === 1) {
        this.setState({ blocking: false });
        if (response.data.storelocation.length > 0) {
          this.setState({ storelocation: response.data.locatorDetails });
        }
      } else {
        this.setState({ blocking: false });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  render() {
    const { productBdata, storelocationDetails,page_title } = this.state;
    const images = require.context("../assets/images", true);
    return (
      <React.Fragment>
        <Helmet>
        <title>{ page_title ? page_title:" " }</title>
        </Helmet>
        <Header />
        <div className="distributor-locator-page">
          <div className="center">
            <Productbanner productbannerInfo={productBdata} />
          </div>
          <DistributorModel
            show={this.state.modalShow}
            content={this.state.content.title4 ? this.state.content.title4 : ""}
            onHide={() => this.setState({ modalShow: false })}
          />
          <div className="search-wrapper">
            <Container>
              <form className="js-form">
                <div className="selection-box">
                  <span>
                    {this.state.content.title1 ? this.state.content.title1 : ""}
                 
                  </span>
                  <Form.Check
                    inline
                    label="Distributor"
                    name="locate"
                    checked={this.state.selectedOption === "Distributor"}
                    type="radio"
                    id="distributor"
                    value="Distributor"
                    
                    onChange={(e) => this.onValueChange(e)}
                  />
                  <Form.Check
                    inline
                    label="System Integrator"
                    name="locate"
                    type="radio"
                    checked={this.state.selectedOption === "SystemIntegrator"}
                    id="integrator"
                    value="SystemIntegrator"
                    onChange={this.onValueChange}
                  />
                </div>
                <div className="search-box">
                  <Form.Group controlId="address" className="search-form-group">
   
                    <Autocomplete
                      style={{ width: "100%" }}
                      onPlaceSelected={this.onValueselected}
                      types={["(regions)"]}
                      className="form-control search-input"
                      placeholder="Location, City, Zip Code"
                      id="searchTextField"
                      type="text"
                      name="address"
                      componentRestrictions={{ country: "us" }}
                      onChange={this.handleInput}
                    />
                  </Form.Group>

                  <Form.Group controlId="distance" className="distance-group">
                    <Form.Control
                      as="select"
                      className="distance"
                      onChange={this.handleChange}
                    >
                      <option value="0">Distance</option>
                      <option value="10">10 mi</option>
                      <option value="25">25 mi</option>
                      <option value="50">50 mi</option>
                      <option value="100">100 mi</option>
                      <option value="200">200 mi</option>
                      <option value="500">500 mi</option>
                      <option value="1000">1000 mi</option>
                      <option value="5000">5000 mi</option>
                      <option value="10000">10000 mi</option>
                    </Form.Control>
                  </Form.Group>

                  <Button
                    className="search-button"
                    onClick={this.searchStoreLocation}
                  >
                    {/* SEARCH */}
                    {this.state.content.title2 ? this.state.content.title2 : ""}
                  </Button>
                </div>
              </form>
            </Container>
          </div>
          <div className="result-wrapper">
            <Container>
              <h2>
                {this.state.content.title3 ? this.state.content.title3 : ""}
                {/* RESULTS */}
              </h2>
              <Row>
                <Col md="4" className="left-section">
                  <div className="js-sidebar">
                    <a href="#" class="js-geolocation"></a>
                    <BlockUi tag="div" blocking={this.state.blocking}>
                      {storelocationDetails.map((store, index) => {
                        return (
                          <div className="item">
                            <div className="spl_image">
                              <img
                                className="img-fluid"
                                alt=""
                                src={
                                  store.logo_img != null
                                    ? store.logo_img
                                    : images(`./frontend/news-placeholder1.png`)
                                }
                              ></img>
                            </div>
                            <h4>{store.title != null ? store.title : ""}</h4>
                            <p>{store.address != null ? store.address : ""}</p>
                            <p>{store.address != null ? store.address2 : ""}</p>
                            <p>
                              {store.city != null ? store.city : ""}
                              {store.state != null ? store.state : ""}
                              {store.zip != null ? store.zip : ""}
                            </p>
                            <p>
                              {" "}
                              {store.country != null ? store.country : ""}{" "}
                            </p>

                            <div className="phone-distance service-areas">
                              <p>
                                {store.service_areas.length > 0 ? (
                                  <React.Fragment>
                                    <span> Service areas:</span>
                                    {store.service_areas.join(",")}
                                  </React.Fragment>
                                ) : (
                                  " "
                                )}
                              </p>
                            </div>

                            <div className="phone-distance">
                              <h4>
                                Tel: {store.phone != null ? store.phone : ""}
                              </h4>
                              <span>10 miles</span>
                            </div>
                            <div className="siteurl">
                              <p>
                                {store.site_url != null ? store.site_url : ""}{" "}
                              </p>
                            </div>
                          </div>
                        );
                      })}
                    </BlockUi>
                  </div>
                </Col>
                <Col md="8" className="right-section">
                  <div className="js-map1">
                    <Map
                      google={this.props.google}                 
                      className={"map"}
                      initialCenter={{
                        lat: 36.0051281,
                        lng: -115.093628,
                      }}
                      zoom={14}
                      onClick={this.onMapClicked}
                    >
                      <Marker
                        onClick={this.onMarkerClick}
                        title={"The marker`s title will appear as a tooltip."}
                        name={"Cimon Inc"}
                        position={{ lat: 36.0051281, lng: -115.093628 }}
                      />

                      <InfoWindow
                        marker={this.state.activeMarker}
                        visible={this.state.showingInfoWindow}
                      >
                        <div className="map-locate">
                          <h5>{this.state.selectedPlace.name}</h5>
                        </div>
                      </InfoWindow>
                    </Map>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyDeYCp70l3DM3af-VzfqQBLSS1ud3jsC8c",
})(distributorLocator);
