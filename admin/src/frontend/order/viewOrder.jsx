import React, { useEffect, useState } from "react";
import { Container, Col, Row } from "reactstrap";
import Header from './../common/header';
import Footer from './../common/footer';
import OrderItems from './orderItems';
import * as checkoutService from './../../ApiServices/checkout';

/* View Order */
const ViewOrder = props => {
    const [blocking, setBlocking] = useState(false);
    const [order, setOrder] = useState({
        order_details:{
            total: {
                product_total: 0,
                discount: 0,
                tax_total: 0,
                shipping_total: 0,
                total: 0
            },
            product: [],
            refund: []
        }
    });
    const [orderStatus, setOrderStatus] = useState(false);
    const getOrder = async () => {
        let orderId   = props.match.params.order_id;
        setBlocking(true);
        try {
            const response =  await checkoutService.getOrder(orderId);
            if(response.status == 200 ) {
                setBlocking(false);
                if (response.data.order ) {
                    setOrder(response.data.order);
                    setOrderStatus(true);
                } else {
                    setOrderStatus(false);
                }
            }
        } catch (err) {
            setBlocking(false);
        }
    }

    useEffect ( () => {
        getOrder();
    }, [])

    if(orderStatus) {
        return (
            <div>
                <Header></Header>
                <OrderItems
                order = {order}
                />
                <Footer></Footer>
            </div>
        );
        
    } else {
        return ( 
            <div>
                <Header></Header>
                <Container>
                    <Row className="view-order-details">
                        <Col>
                            <h3>{blocking == false ? "Sorry! We could not find any orders" : "Fetching order details. Please wait.." }</h3>
                        </Col>
                    </Row>
                </Container>
                <Footer></Footer>
            </div>
        );
    }
};

export default ViewOrder;
