import React, { useState, useEffect } from 'react';
import { CURRENCY_SYMBOL } from '../../config.json';
import moment from 'moment';
import { Container, Col, Row, Table } from "reactstrap";

/* Order Items */
const OrderItems = props => {
    const [removedProductArray, setRemovedProductArray] = useState([]);
    const [ refundArray, setRefundArray] = useState([]);
    const [amountRefunded, setamountRefunded] = useState(0);
    const [refundedTaxAmt, setRefundedTaxAmt] = useState(0);
    const [lineSubtotal, setLineSubtotal] = useState(0);
    const getcurrentQunaitybyId = (productid) => {
      let rowData = removedProductArray.filter(l => {
        return (
          l._id.match(productid)
        )
      });
      if (rowData.length > 0) {
        return (rowData[0].count ? rowData[0].count : 0)
      } else {
        return 0;
      }
    }
    const intialRemovedItemsfunction = (refundarray) => {
      let Removeditems = [];
      if (refundarray.length) {
        refundarray.map(
          (singlerefund, idx) =>
            Removeditems.push(singlerefund)
        )
      }
      getRemovedItems(Removeditems);
    }
    const getRemovedItems = async (Removeditems) => {  
      if (Removeditems.length > 0) {
        var holder = {};
        Removeditems.forEach(function (valuesare) {
          if (valuesare) {
            if (valuesare.removedstock) {
              valuesare.removedstock.forEach(function (removedstocksingle) {
                if ((removedstocksingle.count * 1) > 0) {
                  if (removedstocksingle._id !== '') {
                    if (holder.hasOwnProperty(removedstocksingle._id)) {
                      holder[removedstocksingle._id] = holder[removedstocksingle._id] + removedstocksingle.count * 1;
                    } else {
                      holder[removedstocksingle._id] = removedstocksingle.count * 1;
                    }
                  }
                }  
              })
            }  
          }
        })
      }
      var obj2 = [];
      if (holder) {
        for (var prop in holder) {
          obj2.push({ _id: prop, count: holder[prop] });
        }
      }
      setRemovedProductArray(obj2);
    }
    const getAmountRefunded = async (refund_array = [], total_amount = 0) => {
      let refundvalue = 0
      let refunded_shipping_amt = 0
      let refunded_tax_amt = 0
      let removed_product_qty = 0

      if (refund_array.length > 0) {
        refund_array.map(
          (singlerefund, idx) =>
            (
              (refundvalue = refundvalue + (singlerefund.other.amount * 1) / 100)
              (refunded_shipping_amt = refunded_shipping_amt + (singlerefund.shipping * 1)),
              (refunded_tax_amt = refunded_tax_amt + (singlerefund.tax * 1)),
              (removed_product_qty = removed_product_qty + (singlerefund.removedstock[0].count * 1))
            )
        )
  
      }
      setamountRefunded((refundvalue).toFixed(2));
      setRefundedTaxAmt((refunded_tax_amt).toFixed(2));
    }
    const getLineSubtotal = () => {
      let lineSubtotal = 0;
      props.order.order_details.product.map((products, idx) => (
        lineSubtotal += ((products.price * 1) * (products.quantity * 1))
      ))
      setLineSubtotal(lineSubtotal);
    }

    useEffect ( () => {
      intialRemovedItemsfunction( props.order.order_details.refund);
      getAmountRefunded( props.order.order_details.refund, props.order.order_details.total );
      setRefundArray( props.order.order_details.refund ? props.order.order_details.refund : 0);
      getLineSubtotal();
    }, [])
  
    return (  
      <React.Fragment>
        <Container>
          <Row className="view-order-details">
            <Col><h2>Order details for order #{props.order.order_no}</h2></Col>
            <Col sm={12}>
            <Table responsive>
              <thead>
                <tr>
                  <th>Item</th>
                  <th>Cost</th>
                  <th>Qty</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {props.order.order_details.product.map((products, idx) => (
                  <tr key={idx} >
                    <th>                      
                    <p>{products.product_name}</p>
                    </th>
                    <th>
                      <div>
                        {CURRENCY_SYMBOL + products.price}
                      </div>
                      <p style={{ 'marginBottom': '0px', 'color': 'red' }}></p>
                    </th>
                    <th>
                      {' '} x {products.quantity}
                      { amountRefunded > 0 ?
                        <p style={{ 'marginBottom': '0px', 'color': 'red' }}>- x {
                          getcurrentQunaitybyId(products.product_id)
                        }</p> : ''
                      }
                    </th>
                    <th>
                      {CURRENCY_SYMBOL + (products.price * products.quantity)}
                      {amountRefunded > 0  ?
                        <p style={{ 'marginBottom': '0px', 'color': 'red' }}>{
                          CURRENCY_SYMBOL + (getcurrentQunaitybyId(products.product_id) * products.price)}</p> : ''
                      }
                    </th>
                  </tr>
                ))}
              </tbody>
            </Table>
            </Col>
            <Col sm={12}>
            { refundArray.length > 0 ? (
            <div className='refund_loop_conatiner'>
              {refundArray.map((singlerefund, idx) => (
                <div className='refund_loop row shipping  '>
                  <div className='col-md-10'>
                    <p>
                      Refund #{singlerefund.other.id} -{' '}
                      {moment
                        .unix(singlerefund.other.created)
                        .format('DD-MM-YYYY HH:mm:ss')}{' '}
                      by admin{' '}
                    </p>
                  </div>
                  <div className='col-md-2 red'>
                    <p style={{ 'margin-bottom': '0px', 'color': 'red' }}>
                      -
                      {singlerefund.other.amount > 0
                        ? CURRENCY_SYMBOL + singlerefund.other.amount / 100
                        : 0}
                    </p>
                  </div>
                </div>
              ))}{' '}
            </div>
            ) : (
              ''
            )}
            </Col>
            <Col sm={12}>
            {props.order.order_details.total ? (
            <div className='single_order row shipping'>
              <div className="rownew">
                <div className='col-md-10'>
                  <p>Items Subtotal:</p>
                </div>
                <div className='col-md-2'>
                  <p>
                    {CURRENCY_SYMBOL + lineSubtotal}</p>
                </div>
              </div>
              <div className="rownew">
                <div className='col-md-10'>
                  <p>Shipping : <b><i>{ props.order.shipping_method.service ? props.order.shipping_method.service : ''}</i></b></p>                  
                </div>
                <div className='col-md-2'>
                  <p>{CURRENCY_SYMBOL + props.order.order_details.total.shipping_total}
                  </p>
                </div>
              </div>
              <div className="rownew">
                <div className='col-md-10'>
                  <p>Tax : 
                  </p>
                </div>
                <div className='col-md-2'>
                  <p>{CURRENCY_SYMBOL}{props.order.order_details.total.tax_total ?  props.order.order_details.total.tax_total : ''}

                    {refundedTaxAmt > 0 ? (
                      <span style={{ 'color': 'red' }}>
                        - {refundedTaxAmt ? CURRENCY_SYMBOL + refundedTaxAmt : 0}</span>
                    ) : '0.00'
                    }
                  </p>
                </div>              
              </div>
              {(props.order.order_details.total.discount !== '' && props.order.order_details.total.discount > 0 ) ?  
                ( <div className="rownew">
                    <div className='col-md-10'>
                    <p>Coupon : 
                      <b><i>{ props.order.coupon_details.couponcode ? props.order.coupon_details.couponcode : ''}</i></b>
                    </p>
                    </div>
                    <div className='col-md-2'>
                      <p style={{ color: 'red' }}>{CURRENCY_SYMBOL + props.order.order_details.total.discount}  </p>
                    </div>
                  </div> ) : ''   
              }
              <div className="rownew">
                <div className='col-md-10'>
                  <p>Refund :</p>
                </div>
                <div className='col-md-2'>
                  <p style={{ color: 'red' }}>{CURRENCY_SYMBOL + amountRefunded}  </p>
                </div>
              </div>           
              <div className="rownew">
                <div className='col-md-10'>
                  <p>
                    <b>Order Total</b>
                  </p>
                </div>
                <div className='col-md-2'>
                  <p>{CURRENCY_SYMBOL + ((props.order.order_details.total.total * 1).toFixed(2) - (amountRefunded * 1).toFixed(2)).toFixed(2)}</p>
                </div>
              </div>
            </div>
          ) : (
              ''
            )}
            </Col>
          </Row>
        </Container>      
      </React.Fragment>
   )
}
export default OrderItems
