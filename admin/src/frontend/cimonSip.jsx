import React, { Component } from "react";
import Header from "./common/header";
import Footer from "./common/footer";
import { Link } from "react-router-dom";
import { Helmet } from 'react-helmet';
const TITLE = 'SIP | Automation Company - Industrial Automation';

class SiProgram extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  scrollToMyRef = () => window.scrollTo(0, this.myRef.current.offsetTop);
  state = {
    list: [
      {
        iconId: "imgIcon1",
        value: "Heavily reduced pricing on all CIMON products",
      },
      {
        iconId: "imgIcon2",
        value: "License to resell CIMON products",
      },
      {
        iconId: "imgIcon3",
        value: "Free comprehensive training on all CIMON Hardware/Software",
      },
      {
        iconId: "imgIcon4",
        value:
          "Technical Support from CIMON Global Support via phone, e-mail, and web",
      },
      {
        iconId: "imgIcon5",
        value: "2 Training seats offered each year",
      },
      {
        iconId: "imgIcon6",
        value: "Access to the CIMON Online Knowledge Database and resources",
      },
      {
        iconId: "imgIcon7",
        value: "Newsletters and technical bulletins",
      },
      {
        iconId: "imgIcon8",
        value: "Developer briefing about upcoming updates and upgrades",
      },
      {
        iconId: "imgIcon9",
        value: "Promotional eligibility in CIMON Quarterly Newsletter",
      },
      {
        iconId: "imgIcon0",
        value: "Listing on our Integrator partner web page and map",
      },
      {
        iconId: "imgIcon11",
        value:
          "Lead generation and lead referral as based on project location, specialty and scope",
      },
      {
        iconId: "imgIcon12",
        value: "Exclusive product release insights",
      },
      {
        iconId: "imgIcon13",
        value: "Special discount on CIMON Starter Kits",
      },
    ],
  };

  render() {
    const images = require.context("../assets/images", true);
    return (
      <React.Fragment>
        <Helmet>
        <title>{ TITLE }</title>
        </Helmet>
        <Header />
        <div className="container-fluid">
          <div id="sip">
            <div className="bannerWrapper">
              <div className="sip_banner_bg">
                <div className="sbg"></div>
                <div className="container">
                  <div className="hedng">
                    <h1>System Integrator Program</h1>
                  </div>
                  <Link
                    onClick={this.scrollToMyRef}
                    to="#"
                    className="scroll-down-link"
                  ></Link>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="top_header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                <h1>CIMON Certified System Integrator Program</h1>
                <h5>
                  Take advantage now for tremendous savings on CIMON brand
                  hardware and software as well as support and service
                  benefits..
                </h5>
              </div>
              <hr />
            </div>
            <div className="icon-list-container" ref={this.myRef}>
              <div className="container">
                <div className="icon_section row">
                  <div className="col-md-12">
                    <ul className="icon-list">
                      {this.state.list.map((item, index) => (
                        <li className="iconlist_li" key={index}>
                          <div
                            className={"iconlist_icon" + " " + item.iconId}
                          ></div>
                          <div className="iconlist_content_wrap">
                            <h4 className="iconlist_title">{item.value}</h4>
                            <div className="iconlist_content">
                              <p>*Only 1 starter kit per company</p>
                            </div>
                          </div>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="sipTextblock">
              <div className="container">
                <div className="row">
                  <div className="sip_text1 col-md-12">
                    <h5>
                      CIMON is pleased to announce this exciting opportunity
                      specifically designed with the system integrator in mind.
                    </h5>
                    <p>
                      Our CIMON Certified System Integrator program allows you
                      to take advantage of tremendous savings on CIMON brand
                      hardware and software as well as receive unlimited free
                      support and service benefits. <br />
                      Those who enroll and complete the integrator certification
                      requirements enjoy massive benefits with minimal time and
                      expense. This is the program designed to jumpstart
                      integration projects and planning in a <br />
                      whole new way.
                    </p>
                  </div>

                  <div className="sip_text2 col-md-12">
                    <h5>Requirements:</h5>
                    <p>
                      To qualify, integrators must complete at least two free
                      CIMON software training sessions and purchase a CIMON
                      starter kit within 60 days of enrollment..
                      <br />
                      <strong>That’s it! </strong>There are no further costs or
                      fees associated with this program!
                    </p>
                  </div>
                  <hr />
                  <div className="sip_text3 col-md-12">
                    <p>
                      If you have questions or concerns, send us an email at
                      &nbsp;
                      <Link to="mailto:sales@cimoninc.com">
                        sales@cimoninc.com
                      </Link>
                      , or get in touch with our sales team, who are always
                      happy to address your needs. We encourage you seize this
                      opportunity to receive tons of perks, discounts, and other
                      benefits you can find only at CIMON.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="grey_bgsection">
              <div className="container grey_cont">
                <div className="row award_section">
                  <div className="col-md-12">
                    <h3>Awards</h3>

                    <img
                      className="img-fluid"
                      alt=""
                      src={images(`./frontend/AWARD.png`)}
                    ></img>
                  </div>
                </div>
                <div className="row certify_section">
                  <div className="col-md-12">
                    <h3>Certifications</h3>

                    <img
                      className="img-fluid"
                      alt=""
                      src={images(`./frontend/certifications_01.png`)}
                    ></img>
                  </div>
                </div>
                <div className="row customers_section">
                  <div className="col-md-12">
                    <h3>Major Customers</h3>

                    <img
                      className="img-fluid"
                      alt=""
                      src={images(`./frontend/customer_03.png`)}
                    ></img>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default SiProgram;
