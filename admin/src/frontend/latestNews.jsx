import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col, Media } from 'react-bootstrap';
import Moment from 'react-moment';
import TextTruncate from 'react-text-truncate'; 
import BlockUi from 'react-block-ui';
import Header from "./common/header";
import Footer from "./common/footer";
import SubFooter from "./common/subFooter";
import Productbanner from "./common/productBanner";
import PaginationPage from "./common/paginationPage";
import * as newsService from "../ApiServices/news";
import { Helmet } from 'react-helmet';
const TITLE = 'Latest Articles | Automation Company - Industrial Automation';

class LatestNews extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            blocking: false,
            totalPages: 0,
            currentPage: 1,
            bannerData: {
                title: "Articles",
                subtitle: "",
                backgroundImage: "latest-news-banner.jpg",
                mainContent:""
            },
            newsData: [],
        }
    }

    componentDidMount() {
        this.getNewsCount(1);
    }

/* Get News Count */
    getNewsCount = async () => {
        this.setState({ blocking: true});
        try {
            const response =  await newsService.getLatestNews();            
            if(response.status === 200 ) {
                this.setState({
                    totalPages: response.data.totalPages
                  }, /* call is for first page records only */
                    () => this.getLatestNews(this.state.currentPage))
            }
        } catch (err) {
            this.setState({ blocking: false});
        }
    }

/* Get Latest News  */
    getLatestNews = async ( pageNumber ) => {
        this.setState({ blocking: true});
        try {
            const response =  await newsService.getLatestNews(pageNumber);
            if(response.status === 200 ) {
                if (response.data.latestNews.length > 0 ) {
                    this.setState({ newsData: response.data.latestNews});
                }
            }            
            this.setState({ blocking: false});
        } catch (err) {
            this.setState({ blocking: false});
        }
    }

/*Pagination section */
    nextpage = (pageNumber) => {
        this.setState({
          currentPage: pageNumber,
        })
        this.getLatestNews(pageNumber);
    }

    tenChange = (pageNumber, isposOrneg) => {
        var finalPage;
        if (isposOrneg > 0) 
          finalPage = pageNumber + 10;
        else 
          finalPage = pageNumber - 10;
        this.setState({
          currentPage: finalPage,
          newsData: [],
        })
        this.getLatestNews(finalPage);
    }
    
    hundreadChange = (pageNumber, isposOrneg) => {
        var finalPage;
        if (isposOrneg > 0) //+100 clicked
          finalPage = pageNumber + 100
        else  //-100 Clicked
          finalPage = pageNumber - 100
        this.setState({
          currentPage: finalPage,
          newsData: [],
        })
        this.getLatestNews(finalPage);
    
    }

    render() { 
        const { bannerData, newsData } = this.state;
        const images = require.context("../assets/images", true);
        var newsImg;
        return ( 
            <React.Fragment>
                <Helmet>
                <title>{ TITLE }</title>
                </Helmet>
                <Header />
                <div className="latest-news-wrapper">
                <Productbanner productbannerInfo={bannerData} />
                <Container className="latest-news-container">
                    <BlockUi tag="div" blocking={this.state.blocking}>
                    {newsData.map( (data,index)=>{ 
                        if(data.image) {
                            newsImg = data.image;
                        }  else {
                            newsImg =   images(`./frontend/news-placeholder.png`);
                        }                          
                        return (
                            <>
                            <Row key={index}>
                                <Col>
                                    <Media className="latest-news-list">
                                        <div className="img-div">
                                        <Link to={"/latest-news/" + data.slug} className="read-link">
                                        <img
                                            width={285}
                                            height={192}
                                            className="mr-3"
                                            src={newsImg}
                                            alt="Generic placeholder"
                                        />
                                         </Link>        
                                        </div>
                                        <Media.Body>
                                            <div className="latest-news-head">
                                                <h5>
                                                <Link to={"/latest-news/" + data.slug} className="read-link">
                                                {data.title}
                                                </Link>                                               
                                                </h5>                                           
                                                <label className="date-box">
                                                    <Moment format="MMMM D, YYYY">
                                                        {data.date}
                                                    </Moment>
                                                    </label>
                                            </div>
                                            <TextTruncate
                                                    line={4}
                                                    element="p"
                                                    truncateText="…"
                                                    text={data.description}       
                                            />
                                        </Media.Body>
                                    </Media>
                                </Col>
                            </Row>
                            </>
                        );                            
                    })}
                    </BlockUi>
                    <Row>
                        <Col>
                        {
                            this.state.totalPages > 0 &&
                            <PaginationPage
                                pages={this.state.totalPages}
                                nextPage={this.nextpage}
                                currentPage={this.state.currentPage}
                            >
                            </PaginationPage>
                        }
                        </Col>
                    </Row>
                </Container>
                </div> 
                <SubFooter />
                <Footer />
            </React.Fragment>

         );
    }
}
 
export default LatestNews;