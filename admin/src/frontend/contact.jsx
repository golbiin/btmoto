import React, { Component } from 'react';
import Header from "./common/header";
import Footer from "./common/footer";
import Joi from "joi-browser";
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import { CountryDropdown } from 'react-country-region-selector';
import * as contactService from "../ApiServices/contact";
import * as pageService from "../ApiServices/page";
import { Helmet } from 'react-helmet';

class Contact extends Component {

    constructor (props) {
      super(props);
      this.state =  {        
        options: this.options,
        value: null,
        blocking: false,
        showingInfoWindow: false,
        activeMarker: {},
        selectedPlace: {name: "in"},
        bannercontent : {
          heading : '',
          subheading : '',
          image : ''
        },
        content : {},
        data: {
            fullname: "",
            country: "",
            phone: "",
            email: "",
            comments: this.props.match.params.product_name,
          },
          errors: {},
          contactData: [],
          page_title:"",
      };
    }
    /* Joi validation schema */
    schema = {
      fullname: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your Full Name.",
        };
      }),
      country: Joi.optional().label("country").error(() => {
        return {
          message: "Please select your Country.",
        };
      }),
      phone: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your Phone Number.",
        };
      }),
      email: Joi.string()
      .required()
      .email()
      .error(() => {
        return {
          message:
            "Please enter your Email Address.",
        };
      }),
      comments: Joi.string().allow(""),
    };  
    componentDidMount = () => {
      const slug = 'contact';
      this.getcontact();
      this.getContent(slug);
    };

  /* Banner Section */
  getContent = async (slug) => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        if (response.data.data.content.bannerSlider) {          
          const Setdata = {};
          response.data.data.content.bannerSlider.map((banner, index) => {
            Setdata.title = banner.heading ? banner.heading : '';
            Setdata.description = banner.subheading ? banner.subheading : '';
            Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";  
          });
          this.setState({
            bannercontent: Setdata,
          });
        }
        this.setState({ content: response.data.data.content });
        if(response.data.data.page_title){
          this.setState({ 
            page_title: response.data.data.page_title
            });

        }
      } 
    } catch (err) {
      this.setState({ spinner: false });
    }
  };
  /* Get all Contacts */
  getcontact = async () => {
    this.setState({ blocking: true});
    try {
        const response =  await contactService.getcontact();
        if(response.status === 200 ) {
            if (response.data.contact.length > 0 ) {
              this.setState({ contactData: response.data.contact});
            }
        }            
        this.setState({ blocking: false});
    } catch (err) {
      this.setState({ blocking: false});
    }
  }

  saveContact = async (e) => {
    e.preventDefault()
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    if(data.country === ""){
        errors.country = " Please select your Country.";
        this.setState({ errors });
    } else{
        delete errors.country;
        this.setState({ errors });
    }
    let options = { abortEarly: false }; 
    let result = Joi.validate(data, this.schema, options);
    if (result.error) {
        result.error.details.map( error => {
          let path = error.path[0];
          let errormessage = error.message;
          errors[path] = errormessage;
        })
        this.setState({ errors });
    }
    else {
      const category_data = {
        fullname: this.state.data.fullname,
        country: this.state.data.country,
        phone: this.state.data.phone,
        email : this.state.data.email,
        comments : this.state.data.comments,
        id : this.state.data.id,
      }
      try {
        this.setState({ 
          submit_status: true,
          message: "Subscribed Succesfully",
          message_type: "success" 
        });
        const response = await contactService.createContact(category_data);
        if (response) {
          this.setState({ submit_status: false });
          if(response.data.status === 1){
            this.setState({
    
              fullname: "",
              country: "",
              phone: "",
              email: "",
              comments: "",
              submit_status: true,
              message: "Subscribed Succesfully",
              message_type: "success" 
            });
          } else {
            this.setState({
              message: response.data.message,
              message_type: "error",
            });
          }
        } else {
          this.setState({ submit_status: false });
          this.setState({
            message: "Something went wrong! Please try after some time",
            message_type: "error",
          });
        }
        setTimeout(() => { 
          this.setState(() => ({message: ''}))
        }, 5000);
      }
      catch (err) {  }
    }  
  }
 /*Input handle Change */
  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors }); 
  };

  /*Joi Validation Call*/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Google Map */
  onMarkerClick = (props, marker, e) =>
    this.setState({
    selectedPlace: props,
    activeMarker: marker,
    showingInfoWindow: true
    });
    onMapClicked = (props) => {
      if (this.state.showingInfoWindow) {
        this.setState({
          showingInfoWindow: false,
          activeMarker: null
        })
      }
    };

    selectCountry (val) {
      let data = { ...this.state.data };
      let errors = { ...this.state.errors };
      data['country']= val;
      if(data.country === ""){
        errors.country = " Country is required";
      } else{
          delete errors.country;
      }
      this.setState({ data, errors });
    }
    
    render() { 
      const {country} = this.state.data;
      const contactData = this.state.contactData;
      const {page_title} = this.state;
      let Background =  this.state.bannercontent.image ? this.state.bannercontent.image : 'https://cimon-rt.s3.amazonaws.com/video/2020/7/1598635117402-contact-bg.ef0e5df9.jpg'
      return (
        <React.Fragment>
        <Helmet>
        <title>{ page_title ? page_title:" " }</title>
        </Helmet>
        <Header />
        <div id="contact-us" className="container-fluid">
          <div className="contact-banner">
              <div className="cnt_banner_bg" style={{backgroundImage: "url(" + Background + ")"}}>
                  <div className="abg"></div>
                  <div className="container">
                  <div className="cnthedng">
                  <h1>
                  {this.state.bannercontent.title ? this.state.bannercontent.title :'CONTACT'}
                  </h1>
                </div>
              </div>
            </div>
          </div>
          <div id="contact-section">
            <div className="container">
              <div className="row form-section">
                <div className="col-md-6 contact-form-section">
                  <div dangerouslySetInnerHTML={{ __html: this.state.content.section1 ? this.state.content.section1 : '' }}></div>
                    <div className="contact-form">
                      <div className="form-group">
                        <label>Full Name </label>
                          <input type="text" name="fullname"  
                          value={this.state.data.fullname} className="form-control"
                          onChange={this.handleChange} ></input>
                          {this.state.errors.fullname ? (
                          <div className="danger">
                          {this.state.errors.fullname}
                          </div>
                            ) : ( "" )}
                      </div>
                      <div className="form-group">
                        <label>Country </label>
                          <CountryDropdown className="form-control"                                          
                          value={country}
                          onChange={(val) => this.selectCountry(val)} /> 
                          {this.state.errors.country ? (
                          <div className="danger">
                          {this.state.errors.country}
                          </div>
                          ) : ( "" )}
                      </div>
                        <div className="form-group">
                          <label>Phone </label>
                            <input type="text" name="phone"
                            value={this.state.data.phone}
                            onChange={this.handleChange} className="form-control"></input>
                            {this.state.errors.phone ? (
                            <div className="danger">
                            {this.state.errors.phone}
                            </div>
                            ) : ( "" )}
                        </div>
                        <div className="form-group">
                            <label>Email </label>
                              <input type="email" name="email"
                              value={this.state.data.email} 
                              onChange={this.handleChange} className="form-control"></input>
                              {this.state.errors.email ? (
                              <div className="danger">
                              {this.state.errors.email}
                              </div>
                              ) : ( "" )}
                        </div>
                        <div className="form-group comment-field">
                          <label>Comments </label>
                            <textarea className="textarea form-control comments" name="comments"
                            value={this.state.data.comments}
                            onChange={this.handleChange} 
                            >
                            </textarea>
                        </div>
                        <div className="btn-submit">
                          <button type="submit" onClick={this.saveContact} className="btn blue-btn"> Submit </button>
                        </div>
                        <div className="msg"> 
                          {this.state.submit_status === true ? 'Your Subscribtion Succesfully Completed' : ''}
                        </div>
                    </div>
                </div>
                <div className="col-md-6 contact-address"> 
                  {contactData.map( (data,index)=>{ 
                    return(
                      <>
                        <div className="address-heading">
                        <h4>{data.name}</h4>
                        <p>{data.address1},</p> 
                        <p>{data.address2}</p>
                        { data.phone !== '' ?
                        <p>Tel: <a href={"tel:"+ data.phone }>{data.phone}</a></p> : <p></p> }
                        { data.email !== '' ?
                        <p>Email: <a href={"mailto:" + data.email }>{data.email}</a>
                        </p> : <p></p> }
                        </div>
                      </>
                      )                                   
                    })} 
                    <div className="googlemap">
                      <Map google={this.props.google} 
                      className={"map"}
                      initialCenter={{
                      lat: this.state.content.latitude ? this.state.content.latitude : 36.0051281 ,
                      lng: this.state.content.longitude ? this.state.content.longitude : -115.093628
                      }}
                      zoom={14}
                      onClick={this.onMapClicked}>
                      <Marker onClick={this.onMarkerClick}
                      title={this.state.content.tooltip}
                      name={this.state.content.companyname ? this.state.content.companyname : 'Cimon Inc'}
                      position={{ lat: this.state.content.latitude ? this.state.content.latitude : 36.0051281,
                      lng:this.state.content.longitude ? this.state.content.longitude : -115.093628 }}
                      />
                      <InfoWindow
                      marker={this.state.activeMarker}
                      visible={this.state.showingInfoWindow}>
                      <div className="map-locate">
                      <h5>{this.state.selectedPlace.name}</h5>
                      </div>
                      </InfoWindow>
                      </Map>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>    
        <Footer />
        </React.Fragment>
      );
    }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyDeYCp70l3DM3af-VzfqQBLSS1ud3jsC8c"
})(Contact);
  
