import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import SubFooter from "./../../../frontend/common/subFooter";
import Banner from "./../../../frontend/common/banner";
import VideoV1 from "./../../../frontend/common/video-v1";
import RecommendedProducts from "././../../../frontend/common/recommendProducts";
import * as ProductsService from "./../../../ApiServices/products";
import { multipleAddProduct } from "./../../../services/actions/cartActions";
import * as CategoriesService from "./../../../ApiServices/categories";
import ProductLayout3 from "./../../../frontend/products/productLayout3";
import * as pageService from "./../../../ApiServices/page";
import { Helmet } from "react-helmet";
import "./../../../services/aos";
const TITLE = "SCADA | Automation Company - Industrial Automation";

class Scada extends Component {
  static propTypes = {
    multipleAddProduct: PropTypes.func.isRequired
  };
  constructor(...args) {
    super(...args);
    this.state = {
      countryCode: "",
      products: [],
      content: {},
      categoryData: [],

      videoData: {
        title: "TITLE GOES HERE",
        sub_title:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam",
        description:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure",
        video: "cimon.mp4",
        showButton: true,
        buttonText: "Download UltimateAccess",
        buttonNotes: "version 1.1 Updated 11-11-2019"
      },
      runtimeBundle: [],
      runtimeViewer: [],
      page_title: "",
      meta_tag: "",
      meta_description: ""
    };
  }

  componentDidMount = async () => {
    const slug = this.props.match.path.substring(
      this.props.match.path.lastIndexOf("/") + 1
    );
    this.getScadadata();
    this.getGeoInfo();
    this.getAllCategoryProducts();
    this.getContent(slug);
  };

  /* Get page contents */
  getContent = async slug => {
    try {
      console.log("response.data.datan", slug);
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        /*banner slider*/
        console.log("response.data.data", response.data.data);
        if (response.data.data.content.bannerSlider) {
          const banner_content = [];
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {};
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.image = banner.image ? banner.image : "sc_banner.jpg";
            banner_content.push(Setdata);
          });
          this.setState({
            categoryData: banner_content
          });
        }
        this.setState({ content: response.data.data.content });
        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* Get user geolocation */
  getGeoInfo = () => {
    axios
      .get("https://ipapi.co/json/")
      .then(response => {
        let data = response.data;
        this.setState({
          countryCode: data.country_code
        });
      })
      .catch(error => {});
  };

  /* Get scada product details */
  getScadadata = async () => {
    this.setState({ blocking: true });
    try {
      let category_slug = "scada";
      const response = await ProductsService.getAllScadaproducts(category_slug);
      if (response.data.status === 1) {
        if (response.data.category1Data.length > 0) {
          this.setState({ runtimeBundle: response.data.category1Data });
        }
        if (response.data.category2Data.length > 0) {
          this.setState({ runtimeViewer: response.data.category2Data });
        }
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  /* Get recommended products */
  getAllCategoryProducts = async () => {
    this.setState({ blocking: true });
    try {
      let category_slug = "plc";
      const response = await CategoriesService.getAllCategoryProducts(
        category_slug
      );
      let productsDetails = response.data.categoryData;
      if (productsDetails) {
        let products = productsDetails;
        this.setState({ products });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  render() {
    const {
      categoryData,
      products,
      videoData,
      runtimeViewer,
      runtimeBundle,
      page_title,
      meta_tag,
      meta_description
    } = this.state;
    const { isCartProcessing, multipleAddProduct } = this.props;
    console.log("categoryData", categoryData);
    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <Header />
        <div className="scada-container">
          <Banner categoryInfo={categoryData} />
          <VideoV1
            section1={
              this.state.content.section1 ? this.state.content.section1 : ""
            }
            section1_sub={
              this.state.content.section1sub1
                ? this.state.content.section1sub1
                : ""
            }
            videourl={
              this.state.content.videourl
                ? this.state.content.videourl
                : "cimon.mp4"
            }
            videoInfo={videoData}
            videotype={this.state.content.video_type}
          />
          <Container
            className="scada-section-3"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <Row>
              <Col
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section2
                    ? this.state.content.section2
                    : ""
                }}
              ></Col>
            </Row>
          </Container>
          <Container
            className="scada-section-4"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <Row className="top-section-4">
              <Col
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section3
                    ? this.state.content.section3
                    : ""
                }}
              ></Col>
            </Row>
            <Row
              className="bottom-section-4"
              dangerouslySetInnerHTML={{
                __html: this.state.content.section4
                  ? this.state.content.section4
                  : ""
              }}
            ></Row>
            <Row
              className="bottom-section-4 text-left"
              dangerouslySetInnerHTML={{
                __html: this.state.content.section5
                  ? this.state.content.section5
                  : ""
              }}
            ></Row>
            <Row
              className="bottom-section-4 text-left"
              dangerouslySetInnerHTML={{
                __html: this.state.content.section6
                  ? this.state.content.section6
                  : ""
              }}
            ></Row>
          </Container>
          <Container
            className="scada-section-5"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <Row>
              <Col>
                <div className="development-wrapper">
                  <h3>STAND ALONE: DEVELOPMENT + RUNTIME BUNDLE</h3>
                  <ProductLayout3
                    products={runtimeBundle}
                    isCartProcessing={isCartProcessing}
                    multipleAddProduct={multipleAddProduct}
                    countryCode={this.state.countryCode}
                  ></ProductLayout3>
                </div>
                <div className="runtime-wrapper">
                  <h3>STAND ALONE: RUNTIME VIEWER</h3>
                  <ProductLayout3
                    products={runtimeViewer}
                    isCartProcessing={isCartProcessing}
                    multipleAddProduct={multipleAddProduct}
                    countryCode={this.state.countryCode}
                  ></ProductLayout3>
                </div>
              </Col>
            </Row>
          </Container>
          <div
            className="scada-common-icon-wrapper"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <Container>
              <Row>
                <Col
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section7
                      ? this.state.content.section7
                      : ""
                  }}
                ></Col>
              </Row>
              <Row
                className="wizard-row"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section7sub1
                    ? this.state.content.section7sub1
                    : ""
                }}
              ></Row>
            </Container>
          </div>
          <RecommendedProducts products={products} />
        </div>
        <SubFooter />
        <Footer />
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  isCartProcessing: state.cart.isCartProcessing
});
export default connect(mapStateToProps, {
  multipleAddProduct
})(Scada);
