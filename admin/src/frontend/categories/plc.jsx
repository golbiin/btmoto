import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import Header from "../common/header";
import Footer from "../common/footer";
import SubFooter from "../common/subFooter";
import Banner from "../common/banner";
import RecommendedProducts from "./../common/recommendProducts";
import * as CategoriesService from "../../ApiServices/categories";
import * as pageService from "../../ApiServices/page";
import VideoV1 from "../common/video-v1";
import  "../../services/aos";
import { Helmet } from 'react-helmet';

class Plc extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      plccategories: [],
      products: [],
      categoryData: [
        {
          title: "CIMON PLC",
          sub_title: "Programmable Logic Controller",
          description:
            "A programmable logic controller (PLC) is essential for automating systems and processes. PLCs allow different machines to communicate with each other and take control of operations. This communication is critical when it comes to the monitoring and execution of various industrial tasks. Multiple types of PLC modules can be joined and linked together for a higher level of efficiency, performance, security and optimal functioning.",
          image: "plc-banner.jpg",
        },
        {
          title: "CIMON PLC",
          sub_title: "Programmable Logic Controller",
          description:
            "A programmable logic controller (PLC) is essential for automating systems and processes. PLCs allow different machines to communicate with each other and take control of operations. This communication is critical when it comes to the monitoring and execution of various industrial tasks. Multiple types of PLC modules can be joined and linked together for a higher level of efficiency, performance, security and optimal functioning.",
          image: "plc-banner.jpg",
        },
        {
          title: "CIMON IPC",
          sub_title: "Programmable Logic Controller",
          description:
            "A programmable logic controller (PLC) is essential for automating systems and processes. PLCs allow different machines to communicate with each other and take control of operations. This communication is critical when it comes to the monitoring and execution of various industrial tasks. Multiple types of PLC modules can be joined and linked together for a higher level of efficiency, performance, security and optimal functioning.",
          image: "plc-banner.jpg",
        },
      ],
      content: {},
      videoData: {
        title: "Efficient Control and Power",
        sub_title: "",
        description:
          "CIMON PLCs offer superior quality and streamlined efficiency. Easily add modules for extra versatility and complex function control. Take charge of your most complex industrial projects with our impressive CICON software while avoiding costly downtime by implementing a full-measure redundancy system.",
        video: "cimon.mp4",
        showButton: true,
        buttonText: "Download CICON",
        buttonNotes: "version 1.1 Updated 11-11-2019",
      },
      page_title:"",
      iconData: [
        {
          id: 1,
          name: "CICON<br/>Software",
          image: "plc-icons1.png",
        },
        {
          id: 2,
          name: "Base<br/>Expansion",
          image: "plc-icons2.png",
        },
        {
          id: 3,
          name: "Variety of Network<br/>Solutions Supported",
          image: "plc-icons3.png",
        },
        {
          id: 4,
          name: "High-Speed<br/>MPU",
          image: "plc-icons4.png",
        },
        {
          id: 5,
          name: "Embedded<br/>Flash Memory",
          image: "plc-icons5.png",
        },
        {
          id: 6,
          name: "PLC Series<br/>Compatibility",
          image: "plc-icons6.png",
        },
       
      ],
    };
  }

  componentDidMount = () => {
    const slug = this.props.match.path.substring(this.props.match.path.lastIndexOf('/') + 1);
    this.getCategorydata();
    this.getAllCategoryProducts();
    this.getContent(slug);
  };

  /* Get page content */
  getContent = async (slug) => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        // banner slider
        if (response.data.data.content.bannerSlider) {
          const banner_content = [];
          
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {};
            Setdata.title = banner.heading ? banner.heading : '';
            Setdata.description = banner.subheading ? banner.subheading : '';
            Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
            banner_content.push(Setdata);
          });
          this.setState({
            categoryData: banner_content,
          });
        }
        this.setState({ content: response.data.data.content });
        if(response.data.data.page_title){
          this.setState({ 
            page_title: response.data.data.page_title
            });

        }
      } 
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* Get banner content */
  getCategorydata = async () => {
    this.setState({ blocking: true });
    try {
      let category_slug = "plc";
      const response = await CategoriesService.getAllCategorybySlug(
        category_slug
      );
      let plcDetails = response.data.categoryData;
      if (plcDetails) {
        let plccategories = plcDetails;
        this.setState({ plccategories });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  /* Get recommended products */
  getAllCategoryProducts = async () => {
    this.setState({ blocking: true });
    try {
      let category_slug = "plc";
      const response = await CategoriesService.getAllCategoryProducts(category_slug);
      let productsDetails = response.data.categoryData;
      if (productsDetails) {
        let products = productsDetails
        this.setState({ products })
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  }

  render() {
    const {
      categoryData,
      products,
      videoData,
      page_title,
      plccategories,
    } = this.state;
    const images = require.context("../../assets/images", true);
    return (
      <React.Fragment>
        <Header />
        <Helmet>
        <title>{ page_title ? page_title:" " }</title>
        </Helmet>
        <div id="category-plc" className="product-ipc category-plc">
          <div className="scada-container">
            <Banner categoryInfo={categoryData} />
            <VideoV1
              section1={this.state.content.section1 ? this.state.content.section1 : ''}
              section1_sub={this.state.content.section1sub1 ? this.state.content.section1sub1 : ''}
              videourl={this.state.content.videourl ? this.state.content.videourl : 'cimon.mp4'}
              videoInfo={videoData} />
          </div>
          <div className="ipc-panel-pc plc-products" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                {plccategories.map((data, index) => {
                  return (
                    <div className="col-md-4" key={index}>
                      <div className="head-section">
                        <h3>
                          {data.category_name != null ? data.category_name : ""}
                        </h3>
                      </div>
                      <div className="img-section text-center">
                        <img
                          src={data.image}
                          alt=""
                          className="img-fluid"
                        ></img>
                      </div>
                      <div className="content-plc-product"
                        dangerouslySetInnerHTML={{
                          __html:
                            data.descrption != null ? data.descrption : "",
                        }}
                      />
                      <div className="learn-btn btns-learn-more">
                        <Link
                          to={
                            data.page_type === "cms"
                              ? "/introduction/plc/" + data.slug
                              : ""
                          }
                          className="learn-more"
                        >
                          LEARN MORE
                        </Link>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <Container className="plc-graph-section text-center" data-aos="fade-up"  data-aos-duration="2000">
            <Row>
              <Col dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }}>                
              </Col>
            </Row>
          </Container>
          <div className="tittle-goes" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12 tittle-head" dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}>
                </div>
              </div>
            </div>
          </div>
          {this.state.content.commonSilder ? 
          <div className="slider-section" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div
                    id="carouselSingleSlider"
                    className="carousel slide"
                    data-ride="carousel"
                  >
                    <div className="carousel-inner">
                    {this.state.content.commonSilder.map((sildes, index) => {
                      return (<div className={index === 0 ? 'carousel-item active' : 'carousel-item'}>
                      <img
                        claclassName="d-block w-100"
                        src={sildes.image ? sildes.image  :  images(`./frontend/plc-slider.png`)}
                        className="img-fluid"
                        alt="First slide"
                      ></img>
                    </div>)
                    })  
                    }
                    </div>
                    <a
                      className="carousel-control-prev"
                      href="#carouselSingleSlider"
                      role="button"
                      data-slide="prev"
                    >
                      <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Previous</span>
                    </a>
                    <a
                      className="carousel-control-next"
                      href="#carouselSingleSlider"
                      role="button"
                      data-slide="next"
                    >
                      <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Next</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>: ""}
          <div className="tittile-goes-content" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section4 ? this.state.content.section4 : '' }}>
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}></div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section6 ? this.state.content.section6 : '' }}>
              </div>
            </div>
          </div>
          <div className="plc-common-section quard-core" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container" dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}>             
            </div>
          </div>
          <div className="cicon-container" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12 tittle-head text-center" dangerouslySetInnerHTML={{ __html: this.state.content.section8 ? this.state.content.section8 : '' }}>                  
                </div>
              </div>
            </div>
          </div>

          <div className="tittile-goes-content tittle-goes-2" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section9 ? this.state.content.section9 : '' }}>                
              </div>              
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section10 ? this.state.content.section10 : '' }}>               
              </div>
              <div className="row tittle-goes-wrapper certificates-section" dangerouslySetInnerHTML={{ __html: this.state.content.section11 ? this.state.content.section11 : '' }}>              
              </div>
            </div>
          </div>

          <div className="plc-icons-container" data-aos="fade-up"  data-aos-duration="2000">
            <Container className="text-center" dangerouslySetInnerHTML={{ __html: this.state.content.section12 ? this.state.content.section12 : '' }}>             
            </Container>
          </div>
          <RecommendedProducts products={products} />
          <SubFooter />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default Plc;
