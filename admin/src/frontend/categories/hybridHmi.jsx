import React, { Component } from 'react';
import Header from "../common/header";
import Footer from "../common/footer";
import SubFooter from "../common/subFooter";
import Banner from '../common/banner';
import RecommendedProducts from './../common/recommendProducts';
import * as CategoriesService from "../../ApiServices/categories";
import * as pageService from "../../ApiServices/page";
import VideoV1 from "../common/video-v1";
import { Helmet } from 'react-helmet';
import  "../../services/aos";


class HybridHmi extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            categoryData1: [],
            products: [],
            content: {},
            page_title:"",
            categoryData: [{

                "title": "XPANEL HYBRID",
                "sub_title": "CIMON HMI + PLC-S",
                "description": "The Xpanel Hybrid combines a 7'' HMI and a PLC-S CPU module, giving you the power and flexibility you need without haying to buy two separate systems. The Xpanel Hybrid gives you the ability to add up to three extension modules onto the HMI. giving you the extra expansion features and capabilities you need to maximize functionality and operation. ",
                "image": "hybrid_hmi_bg.png"
            },
            {
                "title": "XPANEL HYBRID",
                "sub_title": "CIMON HMI + PLC-S",
                "description": "The Xpanel Hybrid combines a 7'' HMI and a PLC-S CPU module, giving you the power and flexibility you need without haying to buy two separate systems. The Xpanel Hybrid gives you the ability to add up to three extension modules onto the HMI. giving you the extra expansion features and capabilities you need to maximize functionality and operation.",
                "image": "hybrid_hmi_bg.png"
            },
            {
                "title": "XPANEL HYBRID",
                "sub_title": "CIMON HMI + PLC-S",
                "description": "The Xpanel Hybrid combines a 7'' HMI and a PLC-S CPU module, giving you the power and flexibility you need without haying to buy two separate systems. The Xpanel Hybrid gives you the ability to add up to three extension modules onto the HMI. giving you the extra expansion features and capabilities you need to maximize functionality and operation.",
                "image": "hybrid_hmi_bg.png"
            }
            ],
            videoData: {
                "title": "CIMON PLC and HMI",
                "description": "By combining the power of both PLC and HMI devices into a single unit, CIMON offers a compact and convenient method of automation control. CIMON HMIs and PLCs can work together to bring optimal manufacturing and process efficiency into your workspace. Meet the Xpanel Hybrid.",
                "video": "cimon.mp4",
                "hideButton": true,
            }
        }
    }

    componentDidMount = async () => {
        const slug = this.props.match.path.substring(this.props.match.path.lastIndexOf('/') + 1);
        this.getCategorydata();
        this.getAllCategoryProducts();
        this.getContent(slug);
    };

    /* Get page content */
    getContent = async (slug) => {
        try {
            const response = await pageService.getPageContent(slug);
            if (response.data.status == 1) {
                /* banner slider */
                if (response.data.data.content.bannerSlider) {
                    const banner_content = [];
                    const common_silder = [];
                    response.data.data.content.bannerSlider.map((banner, index) => {
                        const Setdata = {};
                        Setdata.title = banner.heading ? banner.heading : '';
                        Setdata.description = banner.subheading ? banner.subheading : '';
                        Setdata.image = banner.image ? banner.image : "hybrid_hmi_bg.png";
                        banner_content.push(Setdata);
                    });
                    this.setState({
                        categoryData: banner_content,
                    });
                }
                this.setState({ content: response.data.data.content });

                if(response.data.data.page_title){
                    this.setState({ 
                      page_title: response.data.data.page_title
                      });
          
                  }
            } 
        } catch (err) {
            this.setState({ spinner: false });
        }
    };

    /* Get banner content */
    getCategorydata = async () => {
        this.setState({ blocking: true });
        try {
            let category_slug = "hmi_hybrid";
            const response = await CategoriesService.getAllCategorybySlug(
                category_slug
            );
            if (response.status == 200) {
                if (response.data.categoryData.length > 0) {
                    this.setState({ categoryData1: response.data.categoryData });
                    this.setState({
                        message: response.data.message,
                        status: response.data.status,
                    });
                }
            } else {
                this.setState({ blocking: false });
                this.setState({
                    message: response.data.message,
                    status: response.data.status,
                });
            }
        } catch (err) {
            this.setState({ blocking: false });
        }
    };

    /* Get recommended products */
    getAllCategoryProducts = async () => {
        this.setState({ blocking: true });
        try {
            let category_slug = "hybrid-hmi";
            const response = await CategoriesService.getAllCategoryProducts(category_slug);
            let productsDetails = response.data.categoryData;
            if (productsDetails) {
                let products = productsDetails
                this.setState({ products })
            }
        } catch (err) {
            this.setState({ blocking: false });
        }
    }

    render() {
        const { categoryData, products, videoData, categoryData1,page_title } = this.state;
        const images = require.context("../../assets/images", true);
        return (
            <React.Fragment>
                <Helmet>
                <title>{ page_title ? page_title:" " }</title>
                </Helmet>
                <Header />
                <div id="product-hybrid" className="category-hybrid">
                    <div className="scada-container">
                        <Banner categoryInfo={categoryData} />
                        <VideoV1
                            section1={this.state.content.section1 ? this.state.content.section1 : ''}
                            videourl={this.state.content.videourl ? this.state.content.videourl : 'cimon.mp4'}
                            videoInfo={videoData} />
                        <div dangerouslySetInnerHTML={{ __html: this.state.content.section1sub1 ? this.state.content.section1sub1 : '' }}>
                        </div>                        
                    </div>
                    <div className="product-ipc" >
                        <div className="tittle-goes" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container" dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }}>                                
                            </div>
                        </div>

                        {this.state.content.commonSilder ?
                            <div className="slider-section" data-aos="fade-up"  data-aos-duration="2000">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div id="carouselSingleSlider" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    {
                                                        this.state.content.commonSilder.map((sildes, index) => {
                                                            return (<div className={index == 0 ? 'carousel-item active' : 'carousel-item'}>
                                                                <img
                                                                    className="d-block w-100"
                                                                    src={sildes.image ? sildes.image : images(`./frontend/new-hybrid-bgtrans.png`)}
                                                                    className="img-fluid"
                                                                    alt="First slide"
                                                                ></img>
                                                            </div>)
                                                        })
                                                    }                                                        
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselSingleSlider" role="button" data-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselSingleSlider" role="button" data-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            : ''
                        }
                        <div className="tittile-goes-content" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container">
                                <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}>
                                </div>
                                <div
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section4 ? this.state.content.section4 : '' }}
                                    className="row tittle-goes-wrapper mob-interchanges">
                                </div>                            
                                <div className="row tittle-goes-wrapper"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}
                                >
                                </div>
                                <div className="row tittle-goes-wrapper mob-interchanges"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section6 ? this.state.content.section6 : '' }}
                                ></div>
                                <div className="row tittle-goes-wrapper"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}
                                ></div>
                                <div className="row tittle-goes-wrapper mob-interchanges"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section8 ? this.state.content.section8 : '' }}
                                ></div>
                            </div>
                        </div>

                        <div className="plc-common-section quard-core" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container"
                                dangerouslySetInnerHTML={{ __html: this.state.content.section9 ? this.state.content.section9 : '' }}

                            ></div>
                        </div>
                        <div className="tittle-goes" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-12 tittle-head"
                                        dangerouslySetInnerHTML={{ __html: this.state.content.section10 ? this.state.content.section10 : '' }}
                                    ></div>
                                </div>
                            </div>
                        </div>
                        <div className="tittile-goes-content hmi-new-section" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container">
                                <div className="row tittle-goes-wrapper first-section"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section11 ? this.state.content.section11 : '' }}

                                ></div>
                                <div className="row tittle-goes-wrapper second-section"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section12 ? this.state.content.section12 : '' }}

                                ></div>
                                <div className="row tittle-goes-wrapper third-section"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section13 ? this.state.content.section13 : '' }}
                                ></div>
                                <RecommendedProducts products={products} />
                            </div>
                        </div>
                    </div>
                    <SubFooter />
                </div>
                <Footer />
            </React.Fragment>
        );
    }
}
export default HybridHmi;
