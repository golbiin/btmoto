import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Header from "../common/header";
import Footer from "../common/footer";
import Productbanner from "../common/productBanner";
import SubFooter from "../common/subFooter";
import SubcategoryProducts from "./../common/subcategoryProducts";
import * as CategoriesService from "../../ApiServices/categories";
import { Helmet } from 'react-helmet';
const TITLE = 'PLC | Automation Company - Industrial Automation';

class Cm1 extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      productBdata: {
        title: "",
        subtitle: "",
        backgroundImage: "subcategoryBannerNew.jpg",
        mainContent:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat.",
      },
      products: [],
      categoryData1: [],
    };
  }
  componentDidMount = async () => {
    this.getBannerDetails();
    this.getCmCategories();
  };

  /*Get plc-cm1 subcategories details */
  getCmCategories = async () => {
    this.setState({ blocking: true });
    let sub_category = this.props.match.params.slug;
    try {
      const response = await CategoriesService.getAllCmCategories(sub_category);
      if (response.status === 200) {
        if (response.data.categoryData.length > 0) {
          this.setState({ categoryData1: response.data.categoryData });
          this.setState({
            message: response.data.message,
            status: response.data.status,
          });
        }
      } else {
        this.setState({ blocking: false });
        this.setState({
          message: response.data.message,
          status: response.data.status,
        });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  /* Get banner content */
  getBannerDetails = async () => {
    this.setState({ blocking: true });
    try {
      let category_slug = "plc";
      let subCategory = this.props.match.params.slug;
      const response = await CategoriesService.getBannerDetails(category_slug);  
      if (response.status === 200) {
        if (response.data.singleDetails) {
          let bannerdesc = response.data.singleDetails.banner_sub_description;
          
          if (subCategory) {
            let cimon = "PLC Model Type: ";
            this.setState((prevState) => ({
              productBdata: {
                ...prevState.productBdata,
                title: cimon + subCategory,
                subtitle: bannerdesc,
              },
            }));
          }
        }
      } else {
        this.setState({ blocking: false });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  render() {
    const { productBdata, categoryData1 } = this.state;
    const images = require.context("../../assets/images", true);
    return (
      <React.Fragment>
        <Helmet>
          <title>{ TITLE }</title>
        </Helmet>
        <Header />
        <div id="product-subcategory" className="plc-subcategory">
          <div className="">
            <Productbanner productbannerInfo={productBdata} />
          </div>
          <div className="subcategory-topsection">
            <div className="container">
              <div className="row tittle-goes-wrapper">
                <div className="col-md-6 content-wrapper">
                  <img
                    alt=" category image"
                    className="d-block w-100 img-fluid"
                    src={images(`./frontend/cm1.png`)}
     
                  ></img>
                </div>
                <div className="col-md-6 content-wrapper">
                  <div className="content-section">
                    <h3> {productBdata.title}</h3>
                    <p>
                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                      sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                      magna aliquam erat volutpat. Ut wisi enim ad minim veniam,
                      quis nostrud exerci tation ullamcorper suscipit lobortis
                      nisl ut aliquip ex ea commodo consequat. Duis autem vel
                      eum iriure dolor in hendrerit in vulputate velit esse
                      molestie consequat, vel illum dolore eu feugiat nulla
                      facilisis at vero eros et accumsan et iusto odio dignissim
                      qui blandit.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Container>
            <Row className="feature-point">
              <Col xs={12} md={6}>
                <ul>
                  <li>
                    <h5>Feature Point</h5>
                    <p>
                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                      sed diam nonummy nibh euismod tincidunt umagna aliquam
                    </p>
                  </li>
                </ul>
              </Col>
              <Col xs={12} md={6}>
                <ul>
                  <li>
                    <h5>Feature Point</h5>
                    <p>
                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                      sed diam nonummy nibh euismod tincidunt umagna aliquam
                    </p>
                  </li>
                </ul>
              </Col>
              <Col xs={12} md={6}>
                <ul>
                  <li>
                    <h5>Feature Point</h5>
                    <p>
                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                      sed diam nonummy nibh euismod tincidunt umagna aliquam
                    </p>
                  </li>
                </ul>
              </Col>
              <Col xs={12} md={6}>
                <ul>
                  <li>
                    <h5>Feature Point</h5>
                    <p>
                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                      sed diam nonummy nibh euismod tincidunt umagna aliquam
                    </p>
                  </li>
                </ul>
              </Col>
            </Row>
          </Container>
          { categoryData1.length == 0 && <div className="paddingBottom"></div>}
          { categoryData1.length > 0 &&
          <SubcategoryProducts
            categoryData1={categoryData1}
            propsnew={this.props}
          />
          }
        </div>
        <SubFooter />
        <Footer />
      </React.Fragment>
    );
  }
}

export default Cm1;
