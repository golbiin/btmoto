import React, { Component } from "react";
import { Container, Row, Col, Form, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { COUNTRY_CODE, CURRENCY_SYMBOL } from "../../config.json";
import util from '../../services/util';
import Quantity from './../cart/quantity';

class ProductLayout2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countryCode: '',
            isproductAvailable: false
        }
    }

    componentDidMount = () => {
        this.getGeoInfo();
    };

    /* Get user geolocation */
   getGeoInfo = () => {
       axios.get('https://ipapi.co/json/').then((response) => {
           let data = response.data;
           this.setState({
             countryCode: data.country_code
           });
       }).catch((error) => {
           
       });
     };

  render() {
    const { products, categories, categoryDetails, subCategories, multipleAddProduct, isCartProcessing } = this.props;
    const images = require.context("../../assets/images", true);
    let isproductAvailable = false;
    let productUrl      =   categories.join("/");  
    let categoryArray   =    [...categories];
    let childCategory   =   categoryArray.splice((categoryArray.length-1), 1);
    let categoryUrl = categoryArray.join("/");
    return (
    <React.Fragment>
        <div id="product-subcategory" className="plc-subcategory">
            <div className="subcategory-topsection">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3 subcategorysingle-left">
                        <img
                            className="d-block w-100 img-fluid"
                            alt=""
                            src={
                                categoryDetails.image !== ""
                                ? categoryDetails.image
                                : images(`./frontend/news-placeholder1.png`)
                            }
                        ></img>                            
                        <h5>Module Types</h5>
                        {subCategories.map((data,index) =>{
                        return (

                            <p key={data._id}>
                                <a href={"/products/"+categoryUrl+"/"+data.slug} className={                                    
                                (data.slug === childCategory ? 'current-page' : '')}>{data.category_name}
                                </a>
                            </p>
                   
                        )})}
                        
                        </div>
                        <div className="col-md-9 subcategorysingle-wrapper">
                            <div className="content-section">
                                <h3>{categoryDetails.category_name}</h3>
                                <p dangerouslySetInnerHTML={{__html: categoryDetails.short_description}}></p>
                                <Form name="multipleAdToCart" id="multipleAdToCart"  onSubmit={multipleAddProduct}>
                                <Container className="scada-section-5">
                                    <Row>
                                        <Col>
                                            <div className="subcategory-wrapper">
                                                <div className="table-section table-responsive">	
                                                    {(() => {
                                                        if(products.length > 0 ) {                                                            
                                                            return(
                                                                <table className="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Model</th>
                                                                            <th>Type</th>
                                                                            <th>Description</th>
                                                                            <th>Price</th>
                                                                            <th></th>
                                                                            {(() => {
                                                                            if ( this.state.countryCode === COUNTRY_CODE) { 
                                                                                return (<th className="quantity"></th>);
                                                                            }
                                                                            })()}
                                                                        </tr>
                                                                </thead>				
                                                                <tbody>	
                                                                {products.map((product,index) => {
                                                                    
                                                                    product.quantity    =   product.quantity  ? product.quantity : 0;            
                                                                    let formattedPrice =    0;
                                                                    let price = util.getPrice(product.price);
                                                                    formattedPrice = util.formatPrice(price);
                                                                    return (       
                                                                        
                                                                    <tr key={product._id}>
                                                                        <td>
                                                                        {product.product_name != null
                                                                        ? product.product_name
                                                                        : ""}                                                                                 
                                                                            </td>
                                                                        <td>{categoryDetails.category_name}</td>
            
            
                                                                        <td dangerouslySetInnerHTML={{
                                                                            __html:
                                                                                product.short_description != null
                                                                                ? product.short_description
                                                                                : "",
                                                                            }}> 
                                                                            </td>
                                                                        <td className="price_col">
                                                                            {formattedPrice !== "" ? (
                                                                        <span>{CURRENCY_SYMBOL+formattedPrice}</span>
                                                                            ) : (<span>{CURRENCY_SYMBOL}0.00</span>)}
                                                                            </td> 
                                                                        <td className="details">
                                                                        <Link  key={index}
                                                                        className="detail-button"
                                                                        to={"/product/" + productUrl + "/"+ product.slug}>
                                                                        Details
                                                                        </Link>
                                                                        </td> 
                                                                        {(() => {
                                                                        if ( this.state.countryCode === COUNTRY_CODE) { 
                                                                            return (<td>
                                                                                {(() => {
                                                                                    if (util.productAvailable(product)) {
                                                                                        isproductAvailable = true;
                                                                                        return (
                                                                                            <Quantity changeProductQuantity={this.props.changeProductQuantity} product={product} type="plp-multiple"/>
                                                                                        )
                                                                                        } else {
                                                                                        return (
                                                                                            <label className="out-of-stock-label" >Out of stock</label> 
                                                                                        )
                                                                                        }
                                                                                })()}
                                                                            </td>);
                                                                        }
                                                                        })()}
                                                                    </tr>				
                                                                
                                                                    );
                                                                })}			
                                                                </tbody>
                                                            </table>                                                                                                    
                                                            );
                                                        } else {
                                                            return (
                                                                <h4 className="no-products">Sorry no products found!</h4>
                                                            )
                                                        }
                                                    })()}		

                                                </div>
                                                <div className="button-box">
                                                {(() => {
                                                if ( this.state.countryCode === COUNTRY_CODE) {    
                                                    if (isproductAvailable) {                                           
                                                        return (
                                                        <div>                                      
                                                        <Button disabled= {isCartProcessing === true ? true : false }  className="cart-button" onClick={() => this.props.multipleAddProduct(products)} >Add To Cart</Button> 
                                                        </div>
                                                        )
                                                    }
     
                                                } else {
                                                    return (
                                                        <Link to="/contact" className="cart-button">Contact Us</Link>
                                                    )
                                                }
                                                })()} 
                                                
                                                </div>
                                            </div>                           
                                        </Col>
                                    </Row>
                                </Container>
                                </Form>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>
    );
  }
}


ProductLayout2.propTypes = {
    products: PropTypes.array.isRequired,
    multipleAddProduct: PropTypes.func.isRequired,
    changeProductQuantity: PropTypes.func.isRequired,
    isCartProcessing: PropTypes.bool.isRequired,
    categories: PropTypes.array.isRequired,
    subCategories: PropTypes.array.isRequired,
};
export default ProductLayout2;
