import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { CURRENCY_SYMBOL } from '../../config.json';
import util from '../../services/util';
import Quantity from './../cart/quantity';
import axios from 'axios';
import { COUNTRY_CODE } from "../../config.json";

class ProductLayout1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countryCode: '',
        }
    }
    componentDidMount = () => {
         this.getGeoInfo();
     };

       /* Get user geolocation */
    getGeoInfo = () => {
        axios.get('https://ipapi.co/json/').then((response) => {
            let data = response.data;
            this.setState({
              countryCode: data.country_code
            });
        }).catch((error) => {
            
        });
      };
      
    render() {
        const { products, categories, isCartProcessing } = this.props;    
        const images = require.context("../../assets/images", true);
        let productUrl = categories.join("/");
        if(products.length === 0) {
          return (
            <React.Fragment>
              <h4 className="no-product">Sorry no products found.</h4> 
            </React.Fragment>  
          )     
        } else {
          return (
          <React.Fragment>          
            {products.map((product,index) =>{            
              product.quantity    =   product.quantity  ? product.quantity : 1;            
              let formattedPrice =    0;
              let price = util.getPrice(product.price);
              formattedPrice = util.formatPrice(price);

              return (
                  <Row className="list-mid-sec" key={index}>
                  <Col sm={12} md={6} className="left-col">

                  <img
                      className="img-fluid"
                      alt=""
                      src={
                      product.images.featured_image.image_url != null
                          ? product.images.featured_image.image_url
                          : images(`./frontend/news-placeholder1.png`)
                      }
                  ></img>

                      <h3>
                      {product.images.featured_image.image_name != null
                              ? product.images.featured_image.image_name
                              : ""}
                      
                      </h3>
                  </Col>
                  <Col
                      sm={6}
                      className="right-col"
                      md={{ span: 6, offset: 2 }}
                      sm={{ span: 12, offset: 0 }}
                  >
                      <div className="rightsideContents">
                      <Row>
                          <Col md={6} sm={6} xs={6} className="zero-right-padding">
                          
                          <h3 key={index}>
                          {product.product_name != null
                              ? product.product_name
                              : ""}                
                              
                          </h3>
                          
                          </Col>
                          
                          <Col md={6} sm={6} xs={6}>
                          <h4>
                          
                              <span className="pdt_pr">Price: </span>
                              {formattedPrice !== "" ? (
                              <span>{CURRENCY_SYMBOL+formattedPrice}</span>
                      
                          ) : (<span>{CURRENCY_SYMBOL}0.00</span>)}
                          </h4>
                          </Col>
                      </Row>
                      <Row>
                          <Col>
                          <h5>
                          {product.product_sub_text != null
                              ? product.product_sub_text
                              : ""}  
                                
                          </h5>
              
                          <p dangerouslySetInnerHTML={{__html: (product.short_description != null
                                  ? product.short_description
                                  : "") }}>
                                  </p>
                          </Col>
                      </Row>
                      <div className="pdt_lastrow">
                          <Row>
                          <Col md={3} sm={2} xs={3}>
                              {(() => {
                                if( this.state.countryCode === COUNTRY_CODE ) {
                                  return (
                                  <Quantity changeProductQuantity={this.props.changeProductQuantity} product={product} type="plp"/>
                                  );
                                } else {
                                  return (
                                    <p></p>
                                  )
                                }
                              })()}  
                          </Col>
                          <Col md={4} sm={3} xs={4}>
                              <div className="button-box">
                                {(() => {
                                if( this.state.countryCode === COUNTRY_CODE ) {
                                  if (util.productAvailable(product)) {
                                    return (
                                      <div>                                      
                                      <Button disabled= {isCartProcessing === true ? true : false }  className="cart-button" onClick={() => this.props.addProduct(product)} >Add To Cart</Button> 
                                      </div>
                                    )
                                  } else {
                                    return (
                                      <Link to="#" className="cart-button" >Out of stock</Link> 
                                    )
                                  }
                                } else {
                                  return (
                                    <Link to="/contact" className="cart-button">Contact Us</Link>
                                  )
                                }
                                })()}                       
                              </div>
                          </Col>
                          
                          <Col md={5} sm={3} xs={5}>
                              <div className="button-row">
                              <Link  key={index}
                              className="detail-button"
                              // to={productUrl+ "/" + product.slug}
                              to={"/product/" + productUrl + "/"+ product.slug}>
                              More Detail
                              </Link>
                              </div>
                          </Col>
                          </Row>
                      </div>
                      </div>
                  </Col>
                  </Row>
              );
            })} 
          </React.Fragment>
          );
        } 
    }
} 

ProductLayout1.propTypes = {
    products: PropTypes.array.isRequired,
    addProduct: PropTypes.func.isRequired,
    changeProductQuantity: PropTypes.func.isRequired,
};
export default ProductLayout1;

