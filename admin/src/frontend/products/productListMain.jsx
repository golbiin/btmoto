import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import BlockUi from "react-block-ui";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {fetchProducts, changeProductQuantity} from "../../services/actions/productActions";
import { addProduct, multipleAddProduct } from "../../services/actions/cartActions";
import Header from "../common/header";
import Footer from "../common/footer";
import Productbanner from "../common/productBanner";
import SubFooter from "../common/subFooter";
import ProductLayout1 from "./productLayout1";
import ProductLayout2 from "./productLayout2";
import * as CategoryService from "../../ApiServices/categories";
import { Helmet } from 'react-helmet';


class ProductListMain extends Component {
  static propTypes = {
    fetchProducts: PropTypes.func.isRequired,
    addProduct: PropTypes.func.isRequired,
    products: PropTypes.array.isRequired,
    multipleAddProduct: PropTypes.func.isRequired,
    categories: PropTypes.array,
    categoryDetails: PropTypes.object.isRequired,
    subCategories: PropTypes.array,
  };
  state = {
    blocking: false,
    productsBanner: "",
    productBdata: {
      title: "",
      subtitle: "",
      backgroundImage: "j19_bg.png",
      mainContent:
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat.",
    },
    categoryData1: [],
  };
  constructor(props) {
    super(props);
    window.scrollTo({ top: 100, left: 0, behavior: "smooth" });
  }

  componentDidMount = () => {
    let mainCateogery = this.props.match.params.main_cat;
    let subCategory = this.props.match.params.sub_cat;
    let childCategory = this.props.match.params.child_cat;
    let categories = [mainCateogery];
    if (subCategory) {
      categories.push(subCategory);
    }
    if (childCategory) {
      categories.push(childCategory);
    }

    this.handleFetchProducts(categories);
    this.getBannerDetails();
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.match.params.child_cat !== this.props.match.params.child_cat
    ) {
      let mainCateogery = this.props.match.params.main_cat;
      let subCategory = this.props.match.params.sub_cat;
      let childCategory = this.props.match.params.child_cat;
      let categories = [mainCateogery];
      if (subCategory) {
        categories.push(subCategory);
      }
      if (childCategory) {
        categories.push(childCategory);
      }
      this.handleFetchProducts(categories);
    }
  }

  /*Get Banner Details */
  getBannerDetails = async () => {
    this.setState({ blocking: true });
    try {
      let mainCateogery = this.props.match.params.main_cat;
      let subCategory = this.props.match.params.sub_cat;
      const response = await CategoryService.getBannerDetails(mainCateogery);
      if (response.status === 200) {
        if (response.data.singleDetails) {        
          let bannerdesc = response.data.singleDetails.banner_sub_description; 
          if (mainCateogery) {
            let cimon = "CIMON ";
            if (mainCateogery === "plc") {
              cimon = "PLC Model Type: ";
              this.setState((prevState) => ({
                productBdata: {
                  ...prevState.productBdata,
                  title: cimon + subCategory,
                  subtitle: bannerdesc,
                },
              }));
            }
            else
            {
              this.setState((prevState) => ({
                productBdata: {
                  ...prevState.productBdata,
                  title: cimon + mainCateogery,
                  subtitle: bannerdesc,
                },
              }));
            } 
          }
        }
      } else {
        this.setState({ blocking: false });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

/* Fetch products */
  handleFetchProducts = (categories = this.props.categories) => {
    this.setState({ blocking: true });
    this.props.fetchProducts(categories, () => {
      this.setState({ blocking: false });
    });
  };

  noProduct = () => {
    return (
      this.state.blocking === true ? <h4 className="no-product">Loading...</h4>:<h4 className="no-product">Sorry no products found!</h4>
    );
  }

  render() {
    let mainCateogery = this.props.match.params.main_cat;
    let subCategory = this.props.match.params.sub_cat;
    let childCategory = this.props.match.params.child_cat;
    let categories = [mainCateogery];
    if (subCategory) {
      categories.push(subCategory);
    }
    if (childCategory) {
      categories.push(childCategory);
    }
    const { productBdata } = this.state;
    const {
      products,
      categoryDetails,
      subCategories,
      isCartProcessing,
    } = this.props;
    
    const TITLE = subCategory + '| Automation Company - Industrial Automation';

    return (
      <React.Fragment>
      <Helmet>
      <title>{ TITLE }</title>
      </Helmet>
        <Header />
        <div className="container-fluid">
          <div id="pdtlist_container">
            <Productbanner productbannerInfo={productBdata} />
            <Container className="pdtcategory">
              <BlockUi
                tag="div"
                blocking={this.state.blocking || isCartProcessing}
              >
                {(() => {

                  
                  if (categoryDetails) {
                    if (
                      categoryDetails.page_type === "product" &&
                      categoryDetails.page_layout === "layout_1"
                    ) {
                      return (
                        <div>
                          <Row>
                            <Col className="categorycontent">
                              <h2>{categoryDetails.category_name}</h2>
                              <p
                                dangerouslySetInnerHTML={{
                                  __html: categoryDetails.short_description,
                                }}
                              ></p>

                              {products.product_name != null
                                ? products.product_name
                                : ""}
                            </Col>
                          </Row>
                          
                          <ProductLayout1
                            products={products}
                            categories={categories}
                            addProduct={this.props.addProduct}
                            changeProductQuantity={
                              this.props.changeProductQuantity
                            }
                            isCartProcessing={isCartProcessing}
                          />
                        </div>
                      );
                    } else if (
                      categoryDetails.page_type === "product" &&
                      categoryDetails.page_layout === "layout_2"
                    ) {
                      return (
                        <ProductLayout2
                          products={products}
                          categories={categories}
                          categoryDetails={categoryDetails}
                          subCategories={subCategories}
                          multipleAddProduct={this.props.multipleAddProduct}
                          changeProductQuantity={
                            this.props.changeProductQuantity
                          }
                          isCartProcessing = {isCartProcessing}
                        />
                      );
                    } else {
                      this.noProduct();                      
                    }
                  } else {                    
                    this.noProduct(); 
                  }
                })()}
              </BlockUi>
            </Container>
          </div>
          <SubFooter />
        </div>

        <Footer />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  products: state.productsState.products,
  categoryDetails: state.productsState.categoryDetails,
  subCategories: state.productsState.subCategories,
  isCartProcessing: state.cart.isCartProcessing,
});

export default connect(mapStateToProps, {
  fetchProducts,
  changeProductQuantity,
  addProduct,
  multipleAddProduct
})(ProductListMain);
