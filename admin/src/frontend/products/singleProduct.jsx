import React, { Component } from "react";
import Header from "../common/header";
import Footer from "../common/footer";
import SubFooter from "../common/subFooter";
import Productbanner from "../common/productBanner";
import { Container, Row, Col, Button } from "react-bootstrap";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import AsNavFor from "../common/singleProductslider";
import RelatedProducts from "./../common/relatedProducts";
import BlockUi from "react-block-ui";
import {CURRENCY_SYMBOL,ADMIN_EMAIL,COUNTRY_CODE,} from "../../config.json";
import { Link } from "react-router-dom";
import axios from "axios";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {fetchProductSingle,changeProductQuantity,} from "../../services/actions/productActions";
import { addProduct } from "../../services/actions/cartActions";
import util from "../../services/util";
import Quantity from "./../cart/quantity";
import * as CategoryService from "../../ApiServices/categories";
import { Helmet } from 'react-helmet';
import * as ProductService from "../../ApiServices/products";


class SingleProduct extends Component {
  static propTypes = {
    fetchProductSingle: PropTypes.func.isRequired,
    addProduct: PropTypes.func.isRequired,
    product: PropTypes.object.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      countryCode: "",
      productsBanner: "",
      productBdata: {
        title: "",
        subtitle: "",
        backgroundImage: "j19_bg.png",
        mainContent:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat.",
      },
    };
  }

  componentDidMount = () => {
    this.handleFetchProduct();
    this.getGeoInfo();
    this.getBannerDetails();
  };
  
/* Get user geolocation */ 
  getGeoInfo = () => {
    axios
      .get("https://ipapi.co/json/")
      .then((response) => {
        let data = response.data;
        this.setState({
          countryCode: data.country_code,
        });
      })
      .catch((error) => {
        
      });
  };

/* Fetch Product  */
  handleFetchProduct = async () => {
    this.setState({ blocking: true });
    try {
      const product_slug = this.props.match.params.product;
      this.props.fetchProductSingle(product_slug, () => {
        this.setState({ blocking: false });
      });
    } catch (err) {
      this.setState({ blocking: false });
    }
  };




 /* Get Banner Contents */
  getBannerDetails = async () => {
    this.setState({ blocking: true });
    try {
      let mainCateogery = this.props.match.params.main_cat;
      const response = await CategoryService.getBannerDetails(mainCateogery);
      if (response.status === 200) {
        if (response.data.singleDetails) {
          let bannerdesc = response.data.singleDetails.banner_sub_description;
          if (mainCateogery) {
            const cimon = "CIMON ";
            this.setState((prevState) => ({
              productBdata: {
                ...prevState.productBdata,
                title: cimon + mainCateogery,
                subtitle: bannerdesc,
              },
            }));
          }
        }
      } else {
        this.setState({ blocking: false });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

/*Print product details */
  printTab = () => {
    window.print();
  };

  render() {
    const images = require.context("../../assets/images", true);
    const { productBdata } = this.state;
    let Main_cateogery = this.props.match.params.main_cat;
    let product_slug = this.props.match.params.product;
    var productMail = ADMIN_EMAIL;
    const { product, isCartProcessing } = this.props;
    let formattedPrice = 0;
    if (product.price) {
      let price = util.getPrice(product.price);
      formattedPrice = util.formatPrice(price);
    }
    const TITLE = product_slug + '| Automation Company - Industrial Automation';
    const salesUrl = "/contact" + (product.product_name != null ? "/"+product.product_name.trim(): "")
    const additional_tabs = (product.tabdata ? product.tabdata : [])
    return (
      <React.Fragment>
      <Helmet>
      <title>{ TITLE }</title>
      </Helmet>
        <Header />
        <div className="container-fluid">
          <div id="singleProduct">
            <Productbanner productbannerInfo={productBdata} />

            <div className="row">
            <Container className="single_section1">
              <BlockUi tag="div" blocking={this.state.blocking}>
                {(() => {
                  if (product && product.status === 0) {
                    return (
                      <Row>
                        <p>Sorry no product found</p>
                      </Row>
                    );
                  } else {
                    return (
                      <BlockUi
                        tag="div"
                        blocking={this.state.blocking || isCartProcessing}
                      >
                        <Row>
                          <Col
                            md={4}
                            className="sp_leftsec1Wd  text-center text-md-left"
                          >
                            <div className="sp_image">
                              <img
                                className="img-fluid"
                                alt=""
                                src={
                                  product.image != null
                                    ? product.image
                                    : images(`./frontend/news-placeholder1.png`)
                                }
                              ></img>
                            </div>
                            <div className="price_sec">
                              <h4>
                                <span className="pdt_pr">Price: </span>
                                {CURRENCY_SYMBOL}
                                {formattedPrice != null
                                  ? formattedPrice
                                  : "0.00"}
                              </h4>
                            </div>
                            <div className="quantity_sec">
                              <Quantity
                                changeProductQuantity={
                                  this.props.changeProductQuantity
                                }
                                product={product}
                                type="pdp"
                              />
                            </div>

                            <div className="button-box">
                              {(() => {
                                
                                if (this.state.countryCode === COUNTRY_CODE) {    
                                  if (util.productAvailable(product)) {
                                    return (
                                      <Button
                                        disabled={
                                          isCartProcessing === true
                                            ? true
                                            : false
                                        }
                                        className="cart-button"
                                        onClick={() =>
                                          this.props.addProduct(product)
                                        }
                                      >
                                        Add To Cart
                                      </Button>
                                    );
                                  } else {
                                    return (
                                      <Link to="#" className="cart-button">
                                        Out of stock
                                      </Link>
                                    );
                                  }
                                } 
                              })()}
                            </div>
                            
                            <div className="contact_sales">
                              <Link to={salesUrl} className="cart-button">Contact Sales</Link>
                            </div>
                            
                            { Main_cateogery !== null &&
                            (Main_cateogery === "ipc" || Main_cateogery === "plc") ? (" ") : (
                              <div className="contact_sales xpaneldesign">
                                <a
                                  className="cart-button"
                                  href={
                                    product.xpaneldesigner.image_url
                                      ? product.xpaneldesigner.image_url
                                      : ""
                                  }
                                >
                                  Download XpanelDesigner
                                </a>
                              </div>
                            )}
                          </Col>
                          <Col md={{ span: 8 }} className="sp_rightsec1Wd">
                            <div className="sp_tabs">
                              <Tabs defaultActiveKey="model" id="">
                                <Tab eventKey="model" title="Model">
                                  <div className="tabContentSec first-tab">
                                    <div className="func_buttons flex-row-reverse">
                                      <a
                                        className="share_btn"
                                        target="_top"
                                        title="share"
                                        href={`mailto:${productMail}?subject=${
                                          product.product_name != null
                                            ? product.product_name
                                            : ""
                                        }&body=&body=Welcome%20to%20cimon!`}
                                      >
                                        share
                                      </a>

                                      <Link
                                        className="print_btn"
                                        tp="#"
                                        onClick={() => window.print()}
                                        title="print"
                                      >
                                        print
                                      </Link>
                                    </div>

                                    <h2>
                                      {product.product_name != null
                                        ? product.product_name
                                        : ""}

                                      <span className="special_amp">”</span>
                                    </h2>
                                    {product.description !== "" && (
                                      <h5>Key Features</h5>
                                    )}
                                    <div
                                      dangerouslySetInnerHTML={{
                                        __html:
                                          product.description != null
                                            ? product.description
                                            : "",
                                      }}
                                    />

                                    {(() => {
                                      if (product.gallery.length > 0) {
                                        return (
                                          <div className="thumbNail">
                                            <AsNavFor
                                              sliderproducts={product.gallery}
                                            />
                                          </div>
                                        );
                                      }
                                    })()}
                                  </div>
                                </Tab>

                                <Tab
                                  eventKey="specifications"
                                  title="Specifications"
                                >
                                  <div className="tabContentSec second-tab">
                                    <div className="func_buttons flex-row-reverse">
                                      <a
                                        className="share_btn"
                                        target="_top"
                                        title="share"
                                        href={`mailto:${productMail}?subject=${
                                          product.product_name != null
                                            ? product.product_name
                                            : ""
                                        }&body=&body=Welcome%20to%20cimon!`}
                                      >
                                        share
                                      </a>

                                      <a
                                        className="print_btn"
                                        href="#"
                                        title="print"
                                        onClick={this.printTab}
                                      >
                                        print
                                      </a>
                                    </div>
                                    <h2>
                                      {product.product_name != null
                                        ? product.product_name
                                        : ""}
                                       
                                      <span className="special_amp">”</span>
                                    </h2>
                                    <h6>
                                      CPU 128 Step / 32 bit / 8192 / Expandable
                                      / RTC
                                    </h6>
                                    <div className="products-table">
                                      <div
                                        className="table-responsive"
                                        dangerouslySetInnerHTML={{
                                          __html:
                                            product.specifications != null
                                              ? product.specifications
                                              : "",
                                        }}
                                      />
                                    </div>
                                  </div>
                                </Tab>

                                <Tab eventKey="dimensions" title="Dimensions">
                                  <div className="tabContentSec third-tab">
                                    <div className="func_buttons flex-row-reverse">
                                      <a
                                        className="share_btn"
                                        target="_top"
                                        title="share"
                                        href={`mailto:${productMail}?subject=${
                                          product.product_name != null
                                            ? product.product_name
                                            : ""
                                        }&body=&body=Welcome%20to%20cimon!`}
                                      >
                                        share
                                      </a>

                                      <a
                                        className="print_btn"
                                        href="#"
                                        title="print"
                                        onClick={() => window.print()}
                                      >
                                        print
                                      </a>
                                    </div>
                                    <h2>
                                      {product.product_name != null
                                        ? product.product_name
                                        : ""}
                                      <span className="special_amp">”</span>
                                    </h2>
                                    <div className="dim_button_group">
                                      <div className="common_dimension dimen_button_left">
                                        <a
                                          href={
                                            product.cadfile_2D.image_url
                                              ? product.cadfile_2D.image_url
                                              : "#"
                                          }
                                        >
                                          Download 2D CAD file
                                        </a>
                                      </div>
                                      <div className="common_dimension dimen_button_right">
                                        <a
                                          href={
                                            product.cadfile_3D.image_url
                                              ? product.cadfile_3D.image_url
                                              : "#"
                                          }
                                        >
                                          Download 3D CAD file
                                        </a>
                                      </div>
                                    </div>

                                   
                                    {product.dimensionImages.map(
                                      (image, index) => {
                                        return (
                                          <div
                                            className="sp_dimensionImage"
                                            key={index}
                                          >
                                            <img
                                              className="img-fluid"
                                              alt=""
                                              src={image.image_url}
                                            ></img>
                                          </div>
                                        );
                                      }
                                    )}
                                  
                                  </div>
                                </Tab>
                                <Tab
                                  eventKey="certifications"
                                  title="Certifications"
                                ></Tab>
                                         
                                      {additional_tabs.map(
                                        (attr, idx) => (
                                          <Tab
                                      eventKey={idx}
                                      title={attr.name}
                                    >

                                  <div
                                      dangerouslySetInnerHTML={{
                                        __html:
                                        attr.description != null
                                            ? attr.description
                                            : "",
                                      }}
                                    />
                                    </Tab>
                                    )
                                )}
                            
                              </Tabs>
                            
                            </div>
                            <Col md={12}>
                              <div className="related-products">
                                <RelatedProducts
                                  relProducts={product.relatedProducts}
                                />
                              </div>
                            </Col>
                          </Col>
                        </Row>
                      </BlockUi>
                    );
                  }
                })()}
              </BlockUi>
            </Container>
            </div>
          </div>
          <SubFooter />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  product: state.productsState.product,
  isCartProcessing: state.cart.isCartProcessing,
});

export default connect(mapStateToProps, {
  fetchProductSingle,
  changeProductQuantity,
  addProduct,
})(SingleProduct);
