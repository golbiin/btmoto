import React, { Component } from "react";
import {  Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import BlockUi from "react-block-ui";
import { COUNTRY_CODE, CURRENCY_SYMBOL } from "../../config.json";
import util from '../../services/util';

class ProductLayout3 extends Component {
    /* Product quantity change for add to cart */
    handleQuantityChange = (event, product) => {
        product.quantity = event.target.value;
    }

    render() {
        const { products, multipleAddProduct, isCartProcessing, countryCode } = this.props;
        let isproductAvailable = false;
        let options = [];

        for (var i = 0; i <= 10; i++) {
            options.push(<option  key={i} value={i}>{i}</option>);
        }
        if(products.length === 0 ) {
            return(
                <React.Fragment>
                    <h4 className="no-product">Sorry no products found!</h4>
                </React.Fragment>
            );
        } else {
            return (
            <React.Fragment>
                <BlockUi
                        tag="div"
                        blocking={isCartProcessing}
                >
                    <div className="table-section table-responsive">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>Model</th>
                                <th>Type</th>
                                <th className="description">Description</th>
                                <th>Price</th>
                                {(() => {
                                if ( countryCode === COUNTRY_CODE) { 
                                    return(
                                    <th className="quantity">Quantity</th>
                                    );
                                }
                                })()} 
                            </tr>
                            </thead>
                            <tbody>
                            {products.map((product, index) => {

                                let price = util.getPrice(product.price);
                                let formattedPrice = util.formatPrice(price);
                                product.quantity = product.quantity ? product.quantity : 0;  
                                return (                            
                                <tr key={index.key}>
                                    <td>                              
                                    <span>
                                        {product.product_name != null
                                        ? product.product_name
                                        : ""}
                                    </span>
                                    </td>
                                    {product.attributes != null
                                    ? product.attributes.map((att, index1) => {
                                        return (
                                            <td key={index1}>
                                            {att.Attribute_values + " Tags"}
                                            </td>
                                        );
                                        })
                                    : <td></td>}
                                    {index === 0 ? (
                                    <td className="merged-row" rowSpan={index}>
                                        <p
                                        dangerouslySetInnerHTML={{
                                            __html:
                                            product.short_description != null
                                                ? product.short_description
                                                : "",
                                        }}
                                        ></p>
                                    </td>
                                    ) : (
                                    ""
                                    )}

                                    <td className="price_col">
                                    {formattedPrice != null
                                        ? (CURRENCY_SYMBOL+formattedPrice)
                                        : (CURRENCY_SYMBOL+"0.00")}
                                    </td>
                                    {(() => {
                                    if ( countryCode === COUNTRY_CODE) {
                                        return( 
                                        <td>                                
                                        {(() => {
                                        if (util.productAvailable(product)) {
                                            isproductAvailable  = true;
                                            return (
                                            <select name="quantity[]" onChange={(e) => this.handleQuantityChange(e, product) } >
                                            {options}
                                            </select>
                                            )
                                        } else {
                                            return (
                                            <label style={{color:"red"}}>Out of stock</label>
                                            )
                                        }
                                        })()}
                                        </td>
                                        );
                                    }
                                    })()} 
                                </tr>
                                );
                            })}
                            </tbody>
                        </table>
                        </div>
                    <div className="button-box">
                    {(() => {
                    if ( countryCode == COUNTRY_CODE) {    
                        if (isproductAvailable) {                                           
                            return (
                            <div>                                      
                            <Button disabled= {isCartProcessing === true ? true : false }  className="cart-button" onClick={() => multipleAddProduct(products)} >Add To Cart</Button> 
                            </div>
                            )
                        }
                    } else {
                        return (
                            <Link to="/contact" className="cart-button">Contact Us</Link>
                        )
                    }
                    })()} 
                    </div>
                </BlockUi>
            </React.Fragment>
            );
        }
    }
}
ProductLayout3.propTypes = {
    products: PropTypes.array.isRequired,
    multipleAddProduct: PropTypes.func.isRequired,
    isCartProcessing: PropTypes.bool.isRequired,
};
export default ProductLayout3;
