import React, { Component } from 'react';
import BlockUi from "react-block-ui";
import { Helmet } from 'react-helmet';
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import Header from "./../common/header";
import Footer from "./../common/footer";
import SubFooter from "./../common/subFooter";
import * as pageService from "./../../ApiServices/page";
import HomeTemplate from './page/homeTemplate'
import AboutTemplate from './page/aboutTemplate'
import ScadaTemplate from './page/scadaTemplate'
import IpcTemplate from './page/ipcTemplate'
import PlcTemplate from './page/plcTemplate'
import HmiTemplate from './page/hmiTemplate'
import HybridHmiTemplate from './page/hybridHmiTemplate'
import CareersTemplate from './page/careersTemplate'

const TITLE = 'Home | Automation Company - Industrial Automation';
class Page extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            notFound: false,
            page_title: '',
            blocking:true,
            template_name: '',
            slug: '',
            page: {},
            id:''
        };
    }
    getPageContents = async () => {
        console.log(this.props);
        const slug = this.props.match.params.page;
        this.setState({
            slug: slug,
            blocking:true
        });
        try {
            const response =  await pageService.getPageContent(slug);
            if(response.data.status === 1){
                this.setState({
                    template_name: response.data.data.template_name,  
                    page: response.data.data, 
                    page_title: response.data.data.page_title,
                    id: response.data.data._id,
                    blocking:false                 
                })
            } else {
                this.setState({
                    blocking:false ,
                    notFound:true                
                })
            }
        } catch (err) {
            this.setState({
                blocking:false ,
                notFound:true 
            })
        }
    }

    componentDidMount = async () => {
        this.getPageContents();
    }

    renderEditPage() {

        const {template_name, page, slug, id} = this.state;
        console.log(template_name);
        switch(template_name) {
            case "about-us" : 
                return(
                    <AboutTemplate
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case "home" : 
                return(
                    <HomeTemplate
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
             case 'scada':
                return(
                    <ScadaTemplate
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
           case "ipc" : 
                return(
                    <IpcTemplate
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case 'plc':
                return(
                    <PlcTemplate
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case "hmi" : 
                return(
                    <HmiTemplate
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case 'hybrid-hmi':
                return(
                    <HybridHmiTemplate
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case "careers" : 
                return(
                    <CareersTemplate
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
                
            default:
                return (
                    ''                      
                );
        }

    }
    render() {

        const {blocking, template_name, page_title, notFound} = this.state;
        return (
        <React.Fragment>
            <Helmet>
            <title>{ page_title ? page_title:" " }</title>
            </Helmet>
            <Header />
            <BlockUi tag="div" blocking={blocking} >
            { ((blocking === false && template_name === '') || notFound=== true) ?
            <div className="search-page">
                <div className="bannerWrapper">
                <div className="sip_banner_bg">
                    <div className="sbg"></div>
                    <div className="container">
                    <div className="hedng">
                        <h1>PAGE NOT FOUND</h1>
                    </div>
                    <Link to="#" className="scroll-down-link"></Link>
                    </div>
                </div>
                </div>
                <Container className="search-results">
                <Row>
                    <Col>
                    <h4>{"Sorry! Page not found."}</h4>
                    </Col>
                </Row>
                </Container>
            </div>  
            :
            this.renderEditPage()
            }
            </BlockUi>  
            <SubFooter />
            <Footer />       
        </React.Fragment>
        );
    }
}

export default Page;