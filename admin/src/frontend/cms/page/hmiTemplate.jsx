import React, { Component } from "react";
import { Container, Row, Col, Tabs, Tab } from "react-bootstrap";
import Banner from "./../../common/banner";
import VideoV1 from "./../../common/video-v1";
import  "./../../../services/aos";


class HmiTemplate extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      categoryData: [],
      content : {},
      videoData: {
        title: "CIMON Xpanel: Slim. Smart. Strong.",
        sub_title: "",
        description:
          "<p>Our company has extensive knowledge and experience in the automation industry, and with that knowledge and experience, we’ve developed state-of-the-art HMI systems. </p><p>What elements should you consider before choosing an HMI? You’d want to see if there is a sleek, heavy-duty outer shell design. You’d want to check if the user interface is easy to navigate. You’d want to experience for yourself everything of which the HMI is capable. </p><p>CIMON Xpanel is the result of years of industry immersion. Carefully designed and catered to even the most detail-oriented OEM, the CIMON Xpanel has all your control and automation needs in mind.</p>",
        video: "cimon.mp4",
        showButton: true,
        buttonText: "Download XpanelDesigner",
        buttonNotes: "version 1.1 Updated 11-11-2019",
      },
    };
  }

  componentDidMount = () => {
    this.getContent();
  };

  /* Get page content */
  getContent = async () => {

    /* banner slider*/
    if (this.props.page.content.bannerSlider) {
      const banner_content = [];
      const common_silder = [];
      this.props.page.content.bannerSlider.map((banner, index) => {
        const Setdata = {};
        Setdata.title = banner.heading ? banner.heading : '';
        Setdata.description = banner.subheading ? banner.subheading : '';
        Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
        banner_content.push(Setdata);
      });
      this.setState({
        categoryData: banner_content,
      });
    }
    this.setState({ content: this.props.page.content });
    if(this.props.page.page_title){
      this.setState({ 
        page_title: this.props.page.page_title
        });

    }

  };

  render() {
    const {
      categoryData,
      videoData,
    } = this.state;
    const images = require.context("./../../../assets/images", true);
    return (
      <React.Fragment>

        <div
          id="category-plc"
          className="product-ipc category-plc category-hmi"
        >
          <Banner categoryInfo={categoryData} />
          <VideoV1 
              section1={this.state.content.section1 ? this.state.content.section1 : ''}
              section1_sub={this.state.content.section1sub1 ? this.state.content.section1sub1 : ''}
              videourl={this.state.content.videourl ? this.state.content.videourl : 'cimon.mp4'}
              videoInfo={videoData} />
          <div className="tittle-goes" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12 tittle-head" dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }}>
                </div>
              </div>
            </div>
          </div>
          {this.state.content.productSilder ?     
          <div className="slider-section" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div
                    id="carouselSingleSlider"
                    className="carousel slide"
                    data-ride="carousel"
                  >
                    <div className="carousel-inner">                        
                    {this.state.content.productSilder.map((sildes, index) => {
                      return (<div className={index == 0 ? 'carousel-item active' : 'carousel-item'}>
                      <img
                        className="d-block w-100"
                        src={sildes.image ? sildes.image  :  images(`./frontend/slider-1.jpg`)}
                        className="img-fluid"
                        alt="First slide"
                      ></img>
                    </div>)
                    })  
                    }
                    </div>
                    <a
                      className="carousel-control-prev"
                      href="#carouselSingleSlider"
                      role="button"
                      data-slide="prev"
                    >
                      <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Previous</span>
                    </a>
                    <a
                      className="carousel-control-next"
                      href="#carouselSingleSlider"
                      role="button"
                      data-slide="next"
                    >
                      <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Next</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div> : '' }
          <div className="tittile-goes-content" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}>
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section4 ? this.state.content.section4 : '' }}>
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}>
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section6 ? this.state.content.section6 : '' }}>
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}>
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section8 ? this.state.content.section8 : '' }}>
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section9 ? this.state.content.section9: '' }}></div>
            </div>
          </div>

          <div className="plc-common-section quard-core" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container" dangerouslySetInnerHTML={{ __html: this.state.content.section10 ? this.state.content.section10 : '' }}>
            </div>
          </div>

          <div className="cicon-container" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12 tittle-head text-center" dangerouslySetInnerHTML={{ __html: this.state.content.section11 ? this.state.content.section11 : '' }}>
                </div>
              </div>
            </div>
          </div>

          <div className="tabs-section-wrapper text-center" data-aos="fade-up"  data-aos-duration="2000">
            <Container>
              <Row>
                <div className="col-md-12" 
                dangerouslySetInnerHTML={{ __html: this.state.content.section12 ? this.state.content.section12 : '' }}
                ></div>
                <Col className="col-md-12" 
                >
                  <Tabs
                    defaultActiveKey="serial_network"
                    id="performance-tab"
                    className="performance-tab"
                  >
                    <Tab
                      eventKey="serial_network"
                      title="SERIAL NETWORK"
                      className="performance-tab-content"
                    >
                      <div className="tab-content-box">
                        <div className="tab-content-item">
                          <h4>Serial Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img1.png`)}
                            ></img>
                          </div>
                        </div>
                        <div className="tab-content-item">
                          <h4>Serial Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img2.png`)}
                            ></img>
                          </div>
                        </div>
                        <div className="tab-content-item full-width">
                          <h4>CIMON Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img3.png`)}
                            ></img>
                          </div>
                        </div>
                      </div>
                      <p>
                        Use serial, ethernet, modbus, or other communication
                        protocols to connect with the Xpanel HMI. With many
                        options available, you can design your connectivity
                        network structure in a way that will better facilitate
                        communication and the fulfillment of tasks.
                      </p>
                    </Tab>
                    <Tab
                      eventKey="ethernet_network"
                      title="ETHERNET NETWORK"
                      className="performance-tab-content"
                    >
                      <div className="tab-content-box">
                        <div className="tab-content-item">
                          <h4>Serial Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img1.png`)}
                            ></img>
                          </div>
                        </div>
                        <div className="tab-content-item">
                          <h4>Serial Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img2.png`)}
                            ></img>
                          </div>
                        </div>
                        <div className="tab-content-item full-width">
                          <h4>CIMON Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img3.png`)}
                            ></img>
                          </div>
                        </div>
                      </div>
                      <p>
                        Use serial, ethernet, modbus, or other communication
                        protocols to connect with the Xpanel HMI. With many
                        options available, you can design your connectivity
                        network structure in a way that will better facilitate
                        communication and the fulfillment of tasks.
                      </p>
                    </Tab>
                    <Tab
                      eventKey="cimon_network"
                      title="CIMON NETWORK"
                      className="performance-tab-content"
                    >
                      <div className="tab-content-box">
                        <div className="tab-content-item">
                          <h4>Serial Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img1.png`)}
                            ></img>
                          </div>
                        </div>
                        <div className="tab-content-item">
                          <h4>Serial Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img2.png`)}
                            ></img>
                          </div>
                        </div>
                        <div className="tab-content-item full-width">
                          <h4>CIMON Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img3.png`)}
                            ></img>
                          </div>
                        </div>
                      </div>
                      <p>
                        Use serial, ethernet, modbus, or other communication
                        protocols to connect with the Xpanel HMI. With many
                        options available, you can design your connectivity
                        network structure in a way that will better facilitate
                        communication and the fulfillment of tasks.
                      </p>
                    </Tab>
                  </Tabs>                 
                 </Col>
              </Row>
            </Container>
          </div>
          <div className="tittile-goes-content tittle-goes-2" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row tittle-goes-wrapper certificates-section"    dangerouslySetInnerHTML={{ __html: this.state.content.section14 ? this.state.content.section14: '' }}
                ></div>
            </div>
          </div>
          <div className="scada-common-icon-wrapper" data-aos="fade-up"  data-aos-duration="2000"
            dangerouslySetInnerHTML={{ __html: this.state.content.section15 ? this.state.content.section15: '' }}>
          </div>

        </div>

      </React.Fragment>
    );
  }
}
export default HmiTemplate;
