
import React, { Component } from "react";
import Banner from "./../../common/banner";
import VideoV1 from "./../../common/video-v1";
import  "./../../../services/aos";

class IpcTemplate extends Component {
  constructor(...args) {
    super(...args);
    this.state = {

      categoryData: [
        {
          title: "CIMON IPC",
          sub_title: "Industrial PC",
          description:
            "<p>An industrial PC is an important and crucial part of a business in the controls industry. Simply put, an industrial PC allows you to operate various types of industry software (such as UltimateAccess SCADA) while still maintaining the tough endurance and durability you’d expect in harsh environments.</p><br/><p>CIMON is one of the leading industrial PC manufacturers, and with good reason. With over 20 years of experience in the automation industry, CIMON puts those years of industry knowledge into the development of its high-quality products.</p>",
          image: "ipc_banner.png",
        },
        {
          title: "CIMON IPC",
          sub_title: "Industrial PC",
          description:
            "<p>An industrial PC is an important and crucial part of a business in the controls industry. Simply put, an industrial PC allows you to operate various types of industry software (such as UltimateAccess SCADA) while still maintaining the tough endurance and durability you’d expect in harsh environments.</p><br/><p>CIMON is one of the leading industrial PC manufacturers, and with good reason. With over 20 years of experience in the automation industry, CIMON puts those years of industry knowledge into the development of its high-quality products.</p>",
          image: "ipc_banner.png",
        },
        {
          title: "CIMON IPC",
          sub_title: "Industrial PC",
          description:
            "<p>An industrial PC is an important and crucial part of a business in the controls industry. Simply put, an industrial PC allows you to operate various types of industry software (such as UltimateAccess SCADA) while still maintaining the tough endurance and durability you’d expect in harsh environments.</p><br/><p>CIMON is one of the leading industrial PC manufacturers, and with good reason. With over 20 years of experience in the automation industry, CIMON puts those years of industry knowledge into the development of its high-quality products.</p>",
          image: "ipc_banner.png",
        },
      ],
      content: {},
      videoData: {
        title: "CIMON IPC: A Reliable Computer",
        description:
          "Able to meet the needs of industry projects large and small, CIMON IPCs are a welcome addition to your business strategy. Choose from a diverse lineup of IPC products with a broad range of capabilities, all suited for the harsh conditions of wear and tear.",
        video: "cimon.mp4",
        showButton: false,
      },

    };
  }
  componentDidMount = async () => {
    this.getContent();
  };

  /* Get page content */
  getContent = async () => {
    /*banner slider */
    if (this.props.page.content.bannerSlider) {
      const banner_content = [];
      this.props.page.content.bannerSlider.map((banner, index) => {
        const Setdata = {};
        Setdata.title = banner.heading ? banner.heading : '';
        Setdata.description = banner.subheading ? banner.subheading : '';
        Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
        banner_content.push(Setdata);
      });
      this.setState({
        categoryData: banner_content,
      });
    }
    this.setState({ content: this.props.page.content });
    if(this.props.page.page_title){
      this.setState({ 
        page_title: this.props.page.page_title
      });
    }
  };

  render() {
    const images = require.context("./../../../assets/images", true);
    const { categoryData, videoData } = this.state;
    return (
      <React.Fragment>
        
        <div className="product-ipc">
          <div className="scada-container">
            <Banner categoryInfo={categoryData} />
            <VideoV1
              section1={this.state.content.section1 ? this.state.content.section1 : ''}
              section1_sub={this.state.content.section1sub1 ? this.state.content.section1sub1 : ''}
              videourl={this.state.content.videourl ? this.state.content.videourl : 'cimon.mp4'}
              videoInfo={videoData} />
          </div>

          <div className="cimon-pc" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div class="row" dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }}>
              </div>
            </div>
          </div>

          <div className="tittle-goes" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-md-7 tittle-head align-self-center" dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}>
                </div>
              </div>
            </div>
          </div>

          {this.state.content.commonSilder ?                     
            <div className="slider-section" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div
                    id="carouselSingleSlider"
                    className="carousel slide"
                    data-ride="carousel"
                  >
                    <div className="carousel-inner">                   
                    {this.state.content.commonSilder.map((sildes, index) => {
                      return (<div className={index == 0 ? 'carousel-item active' : 'carousel-item'}>
                      <img
                        className="d-block w-100"
                        src={sildes.image ? sildes.image  :  images(`./frontend/slider-1.jpg`)}
                        className="img-fluid"
                        alt="First slide"
                      ></img>
                    </div>)
                    })}                      
                    </div>
                    <a
                      className="carousel-control-prev"
                      href="#carouselSingleSlider"
                      role="button"
                      data-slide="prev"
                    >
                      <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Previous</span>
                    </a>
                    <a
                      className="carousel-control-next"
                      href="#carouselSingleSlider"
                      role="button"
                      data-slide="next"
                    >
                      <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Next</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div> : ''
          }
          <div className="tittile-goes-content" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section4 ? this.state.content.section4 : '' }}>                               
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}>                
              </div>              
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section6 ? this.state.content.section6 : '' }}>
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}>
              </div>              
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section8 ? this.state.content.section8 : '' }}>
              </div>              
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section9 ? this.state.content.section9 : '' }}>
              </div>
            </div>
          </div>
          <div className="plc-common-section quard-core" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container" dangerouslySetInnerHTML={{ __html: this.state.content.section10 ? this.state.content.section10 : '' }}>              
            </div>
          </div>
          <div className="tittle-goes" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-md-7 tittle-head align-self-center" dangerouslySetInnerHTML={{ __html: this.state.content.section11 ? this.state.content.section11 : '' }}>                  
                </div>
              </div>
            </div>
          </div>
          <div className="tittile-goes-content tittle-goes-2 dynamichtml" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section12 ? this.state.content.section12 : '' }}  >                
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section13 ? this.state.content.section13 : '' }}  >
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section14 ? this.state.content.section14 : '' }}  >
              </div>
            </div>
          </div>
          <div className="more-with-cimon" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row" dangerouslySetInnerHTML={{ __html: this.state.content.section15 ? this.state.content.section15 : '' }} >                
              </div>
            </div>
          </div>

        </div>
      </React.Fragment>
    );
  }
}
export default IpcTemplate;
