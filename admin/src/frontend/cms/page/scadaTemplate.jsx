import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Banner from "./../../common/banner";
import VideoV1 from "./../../common/video-v1";
import  "./../../../services/aos";
const TITLE = 'SCADA | Automation Company - Industrial Automation'
class ScadaTemplate extends Component {

  constructor(...args) {
    super(...args);
    this.state = {
      content : {},
      categoryData: [],
      videoData: {
        title: "TITLE GOES HERE",
        sub_title:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam",
        description:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure",
        video: "cimon.mp4",
        showButton: true,
        buttonText: "Download UltimateAccess",
        buttonNotes: "version 1.1 Updated 11-11-2019",
      },
      page_title:"",
    };
  }

  componentDidMount = async () => {
    this.getContent();
  };

  /* Get page contents */
  getContent = async () => {

    /*banner slider*/
    if (this.props.page.content.bannerSlider) {
      const banner_content = [];      
      this.props.page.content.bannerSlider.map((banner, index) => {
        const Setdata = {};
        Setdata.title = banner.heading ? banner.heading : '';
        Setdata.description = banner.subheading ? banner.subheading : '';
        Setdata.image = banner.image ? banner.image : "sc_banner.jpg";
        banner_content.push(Setdata);
      });
      this.setState({
        categoryData: banner_content,
      });
    }
    this.setState({ content: this.props.page.content });
    if(this.props.page.page_title){
      this.setState({ 
        page_title: this.props.page.page_title
      });

    }
  };


  render() {
    const {
      categoryData,
      videoData,
    } = this.state;
    return (
      <React.Fragment>
        <div className="scada-container scada-template-container">
          <Banner categoryInfo={categoryData} />
          <VideoV1
           section1={this.state.content.section1 ? this.state.content.section1 : ''}
           section1_sub={this.state.content.section1sub1 ? this.state.content.section1sub1 : ''} 
           videourl = { this.state.content.videourl ? this.state.content.videourl : 'cimon.mp4'}
           videoInfo={videoData} />
          <Container className="scada-section-3" data-aos="fade-up"  data-aos-duration="2000">
            <Row>
              <Col dangerouslySetInnerHTML={{__html: this.state.content.section2 ? this.state.content.section2 : '' }} >
              </Col>
            </Row>
          </Container>
          <Container className="scada-section-4" data-aos="fade-up"  data-aos-duration="2000">
            <Row className="top-section-4">
              <Col dangerouslySetInnerHTML={{__html: this.state.content.section3 ? this.state.content.section3 : '' }}  >
              </Col>
            </Row>
            <Row className="bottom-section-4" dangerouslySetInnerHTML={{__html: this.state.content.section4 ? this.state.content.section4 : '' }}>
            </Row>
            <Row className="bottom-section-4 text-left" dangerouslySetInnerHTML={{__html: this.state.content.section5 ? this.state.content.section5 : '' }}>
            </Row>
            <Row className="bottom-section-4 text-left" dangerouslySetInnerHTML={{__html: this.state.content.section6 ? this.state.content.section6 : '' }}>              
            </Row>
          </Container>

          <div className="scada-common-icon-wrapper" data-aos="fade-up"  data-aos-duration="2000">
            <Container>
              <Row>
                <Col dangerouslySetInnerHTML={{__html: this.state.content.section7 ? this.state.content.section7 : '' }}>
                </Col>
              </Row>             
              <Row className="wizard-row" dangerouslySetInnerHTML={{__html: this.state.content.section7sub1 ? this.state.content.section7sub1 : '' }}>
              </Row>
            </Container>
          </div>
        </div>
      </React.Fragment>
    );
  }

}
export default ScadaTemplate;
