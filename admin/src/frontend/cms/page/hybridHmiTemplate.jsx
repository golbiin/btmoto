import React, { Component } from 'react';
import Banner from './../../common/banner';
import VideoV1 from "./../../common/video-v1";
import  "./../../../services/aos";


class HybridHmiTemplate extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            content: {},
            categoryData: [{

                "title": "XPANEL HYBRID",
                "sub_title": "CIMON HMI + PLC-S",
                "description": "The Xpanel Hybrid combines a 7'' HMI and a PLC-S CPU module, giving you the power and flexibility you need without haying to buy two separate systems. The Xpanel Hybrid gives you the ability to add up to three extension modules onto the HMI. giving you the extra expansion features and capabilities you need to maximize functionality and operation. ",
                "image": "hybrid_hmi_bg.png"
            },
            {
                "title": "XPANEL HYBRID",
                "sub_title": "CIMON HMI + PLC-S",
                "description": "The Xpanel Hybrid combines a 7'' HMI and a PLC-S CPU module, giving you the power and flexibility you need without haying to buy two separate systems. The Xpanel Hybrid gives you the ability to add up to three extension modules onto the HMI. giving you the extra expansion features and capabilities you need to maximize functionality and operation.",
                "image": "hybrid_hmi_bg.png"
            },
            {
                "title": "XPANEL HYBRID",
                "sub_title": "CIMON HMI + PLC-S",
                "description": "The Xpanel Hybrid combines a 7'' HMI and a PLC-S CPU module, giving you the power and flexibility you need without haying to buy two separate systems. The Xpanel Hybrid gives you the ability to add up to three extension modules onto the HMI. giving you the extra expansion features and capabilities you need to maximize functionality and operation.",
                "image": "hybrid_hmi_bg.png"
            }
            ],
            videoData: {
                "title": "CIMON PLC and HMI",
                "description": "By combining the power of both PLC and HMI devices into a single unit, CIMON offers a compact and convenient method of automation control. CIMON HMIs and PLCs can work together to bring optimal manufacturing and process efficiency into your workspace. Meet the Xpanel Hybrid.",
                "video": "cimon.mp4",
                "hideButton": true,
            }
        }
    }

    componentDidMount = async () => {
        this.getContent();
    };

    /* Get page content */
    getContent = async (slug) => {

        /* banner slider */
        if (this.props.page.content.bannerSlider) {
            const banner_content = [];
            const common_silder = [];
            this.props.page.content.bannerSlider.map((banner, index) => {
                const Setdata = {};
                Setdata.title = banner.heading ? banner.heading : '';
                Setdata.description = banner.subheading ? banner.subheading : '';
                Setdata.image = banner.image ? banner.image : "hybrid_hmi_bg.png";
                banner_content.push(Setdata);
            });
            this.setState({
                categoryData: banner_content,
            });
        }
        this.setState({ content: this.props.page.content });

        if(this.props.page.page_title){
            this.setState({ 
                page_title: this.props.page.page_title
            });    
        }

    };


    render() {
        const { categoryData, videoData } = this.state;
        const images = require.context("./../../../assets/images", true);
        return (
            <React.Fragment>
                <div id="product-hybrid" className="category-hybrid hybrid-template-container">
                    <div className="scada-container">
                        <Banner categoryInfo={categoryData} />
                        <VideoV1
                            section1={this.state.content.section1 ? this.state.content.section1 : ''}
                            videourl={this.state.content.videourl ? this.state.content.videourl : 'cimon.mp4'}
                            videoInfo={videoData} />
                        <div dangerouslySetInnerHTML={{ __html: this.state.content.section1sub1 ? this.state.content.section1sub1 : '' }}>
                        </div>                        
                    </div>
                    <div className="product-ipc" >
                        <div className="tittle-goes" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container" dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }}>                                
                            </div>
                        </div>

                        {this.state.content.commonSilder ?
                            <div className="slider-section" data-aos="fade-up"  data-aos-duration="2000">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div id="carouselSingleSlider" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    {
                                                        this.state.content.commonSilder.map((sildes, index) => {
                                                            return (<div className={index == 0 ? 'carousel-item active' : 'carousel-item'}>
                                                                <img
                                                                    className="d-block w-100"
                                                                    src={sildes.image ? sildes.image : images(`./frontend/new-hybrid-bgtrans.png`)}
                                                                    className="img-fluid"
                                                                    alt="First slide"
                                                                ></img>
                                                            </div>)
                                                        })
                                                    }                                                        
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselSingleSlider" role="button" data-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselSingleSlider" role="button" data-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            : ''
                        }
                        <div className="tittile-goes-content" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container">
                                <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}>
                                </div>
                                <div
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section4 ? this.state.content.section4 : '' }}
                                    className="row tittle-goes-wrapper mob-interchanges">
                                </div>                            
                                <div className="row tittle-goes-wrapper"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}
                                >
                                </div>
                                <div className="row tittle-goes-wrapper mob-interchanges"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section6 ? this.state.content.section6 : '' }}
                                ></div>
                                <div className="row tittle-goes-wrapper"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}
                                ></div>
                                <div className="row tittle-goes-wrapper mob-interchanges"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section8 ? this.state.content.section8 : '' }}
                                ></div>
                            </div>
                        </div>

                        <div className="plc-common-section quard-core" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container"
                                dangerouslySetInnerHTML={{ __html: this.state.content.section9 ? this.state.content.section9 : '' }}

                            ></div>
                        </div>
                        <div className="tittle-goes" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-12 tittle-head"
                                        dangerouslySetInnerHTML={{ __html: this.state.content.section10 ? this.state.content.section10 : '' }}
                                    ></div>
                                </div>
                            </div>
                        </div>
                        <div className="tittile-goes-content hmi-new-section" data-aos="fade-up"  data-aos-duration="2000">
                            <div className="container">
                                <div className="row tittle-goes-wrapper first-section"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section11 ? this.state.content.section11 : '' }}

                                ></div>
                                <div className="row tittle-goes-wrapper second-section"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section12 ? this.state.content.section12 : '' }}

                                ></div>
                                <div className="row tittle-goes-wrapper third-section"
                                    dangerouslySetInnerHTML={{ __html: this.state.content.section13 ? this.state.content.section13 : '' }}
                                ></div>
                               
                            </div>
                        </div>
                    </div>                 
                </div>
            </React.Fragment>
        );
    }
}
export default HybridHmiTemplate;
