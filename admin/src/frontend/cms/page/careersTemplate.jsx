import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Banner from "./../../common/banner";
import JobSearch from "./../../common/jobSearch";
import CareerSlider from "./../../common/careerSlider";
import  "./../../../services/aos";

class CareersTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      jobSearchData: {
        title: "START YOUR JOB SEARCH NOW!",
      },
      getJobsDet: [],
      status: "",
      categoryData: [
        {
          title: "CAREER",
          description:
            "Lorem ipsum dolor sit , consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat .",
          image: "careerbanner.png",
        },
      ],
      content: {},
      careerSliderImages: [
        {
          id: 1,
          image: "careersllider1.png",
        },
        {
          id: 2,
          image: "careersllider2.png",
        },
        {
          id: 3,
          image: "careersllider3.png",
        },
        {
          id: 4,
          image: "careersllider4.png",
        },
        {
          id: 5,
          image: "careersllider1.png",
        },
        {
          id: 6,
          image: "careersllider2.png",
        },
        {
          id: 7,
          image: "careersllider3.png",
        },
      ],
      page_title:"",
    };
  }

  /* Get Page Contents */
  getContent = async () => {

    if (this.props.page.content.bannerSlider) {
      const banner_content = [];
      const common_silder = [];
      this.props.page.content.bannerSlider.map((banner, index) => {
        const Setdata = {};
        Setdata.title = banner.heading ? banner.heading : "";
        Setdata.description = banner.subheading ? banner.subheading : "";
        Setdata.image = banner.image ? banner.image : "careerbanner.png";
        banner_content.push(Setdata);
      });
      this.props.page.content.commonSilder.map((banner, index) => {
        const Setdata = {};
        Setdata.id = index + 1;
        Setdata.image = banner.image ? banner.image : "careersllider1.png";
        common_silder.push(Setdata);
      });
      this.setState({
        categoryData: banner_content,
        careerSliderImages: common_silder,
      });
    }
    this.setState({ content: this.props.page.content });
    if(this.props.page.page_title){
      this.setState({ 
        page_title: this.props.page.page_title
        });

    }
  };

  componentDidMount = async () => {
    this.getContent();
  };

  render() {
    const { categoryData, careerSliderImages, jobSearchData } = this.state;
    
    return (
      <React.Fragment>
        <div className="container-fluid">
          <div id="cimonCareers">
            <Banner categoryInfo={categoryData} />
            <Container className="careers" data-aos="fade-up"  data-aos-duration="2000">
              <div className="career-title-section">
                <Row className="justify-content-center">
                  <Col
                    md={12}
                    className="text-center"
                    dangerouslySetInnerHTML={{
                      __html: this.state.content.section1
                        ? this.state.content.section1
                        : "",
                    }}
                  ></Col>
                </Row>
              </div>
            </Container>
            <div className="search-section1" data-aos="fade-up"  data-aos-duration="2000">
              <JobSearch
                jobSearch={jobSearchData}
                handleChange={this.handleChange}
                propsnew={this.props}
              />
            </div>
            <div className="title-image-wrapper" data-aos="fade-up"  data-aos-duration="2000">
              <Container className="careers">
                <Row
                  className="justify-content-md-center"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section2
                      ? this.state.content.section2
                      : "",
                  }}
                ></Row>
              </Container>
            </div>
            <div className="blog-wrapper" data-aos="fade-up"  data-aos-duration="2000">
              <Container className="careers">
                <Row
                  className="justify-content-md-between justify-content-sm-around "
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section3
                      ? this.state.content.section3
                      : "",
                  }}
                ></Row>
              </Container>
            </div>
            <div className="image-title-wrapper" data-aos="fade-up"  data-aos-duration="2000">
              <Container className="careers">
                <Row
                  className="justify-content-md-between justify-content-sm-center"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section4
                      ? this.state.content.section4
                      : "",
                  }}
                ></Row>
              </Container>
            </div>
            <div className="grey-icon-wrapper" data-aos="fade-up"  data-aos-duration="2000" >
              <Container className="careers" >
                <div
                  className="title-sec text-center"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section5
                      ? this.state.content.section5
                      : "",
                  }}
                ></div>
                <Row
                  className="justify-content-between"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section6
                      ? this.state.content.section6
                      : "",
                  }}
                ></Row>
              </Container>
            </div>
            <div className="simple-slider">
              <CareerSlider
                title={
                  this.state.content.section7 ? this.state.content.section7 : ""
                }
                careerSliderInfo={careerSliderImages}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default CareersTemplate;
