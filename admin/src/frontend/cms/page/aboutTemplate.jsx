import React, { Component } from 'react';
import CommonBannerv1 from "./../../common/commonBanner-v1";
import  "./../../../services/aos";
const TITLE = 'About Us | Automation Company - Industrial Automation';
class AboutTemplate extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      bannerData:[{
            "title": "ABOUT US",
            "description": "CIMON is a leading industdal automaton solution provider with offices in the USA, South Korea, and other countries around the world. Tackling Industry 4.0 requirements head-on, CIMON strives towards an efficient, more productive future, allowing for the next generation of advanced techndogy and civilization.",
            "image": "about.jpg",
            "showButton":true,
        } 
      ],
      content: {},
      page_title:"",
    }
  }
  componentDidMount = () => {
    this.getPageContents();
  }
  /* Get Page Contents */
  getPageContents = async () => {

    if(this.props.page.content.bannerSlider){
      const banner_content = [];
      this.props.page.content.bannerSlider.map((banner,index) => {
        const Setdata = {};
        Setdata.title = banner.heading ? banner.heading : '' ;
        Setdata.description =  banner.subheading ? banner.subheading : '';
        Setdata.image =  banner.image ? banner.image : "/static/media/about.034a1af3.jpg";
        banner_content.push(Setdata);
      });
      this.setState({ 
        bannerData: banner_content
      });
    }
    this.setState({ content: this.props.page.content});

    if(this.props.page.page_title){
      this.setState({ 
        page_title: this.props.page.page_title
      });
    }
  }

  render() { 
    const {bannerData,page_title} = this.state;
    return ( 
      <React.Fragment>
        <div id="about-us" className="container-fluid about">
          <CommonBannerv1 bannerInfo={ bannerData } />
          <div className="product-ipc">
            <div className="tittle-goes abt-first-section" data-aos="fade-up"  data-aos-duration="2000">
              <div className="container">
                <div className="row">
                  <div className="col-md-12 tittle-head" 
                      dangerouslySetInnerHTML={{__html: this.state.content.section1 ?
                      this.state.content.section1 : '' }}> 
                    </div> 
                  </div>
                </div>
              </div>
              <div className="tittile-goes-content"  data-aos="fade-up"  data-aos-duration="2000">
                <div className="container">
                  <div className="row tittle-goes-wrapper mob-interchanges" 
                    dangerouslySetInnerHTML={{__html: this.state.content.section2 ?
                    this.state.content.section2 : '' }}>
                  </div>
                  <div className="row tittle-goes-wrapper respected-prowess"
                    dangerouslySetInnerHTML={{__html: this.state.content.section3 ?
                      this.state.content.section3 : '' }}>
                  </div>
                </div>
              </div>
            </div>            
          <div className="counter-section"  data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row counter-center">
                <div className="col-md-3 sb1 col-xs-1 common-clms first-clms"
                  dangerouslySetInnerHTML={{__html: this.state.content.section4_1 ?
                    this.state.content.section4_1 : '' }}>
                </div>
                <div dangerouslySetInnerHTML={{__html: this.state.content.section4_2 ?
                  this.state.content.section4_2 : '' }} className="col-md-3 sb1 col-xs-1 
                    common-clms second-clms">
                </div>
                <div dangerouslySetInnerHTML={{__html: this.state.content.section4_3 ?
                  this.state.content.section4_3 : '' }}
                  className="col-md-3 sb1 col-xs-1 common-clms three-clms">   
                </div>
              </div>
            </div>
          </div>
          <div className="product-ipc" data-aos="fade-up"  data-aos-duration="2000">
            <div className="tittle-goes abt-first-section">
              <div className="container">
                <div className="row">
                  <div className="col-md-12 tittle-head" dangerouslySetInnerHTML={{__html: this.state.content.section5 ?
                      this.state.content.section5 : '' }}>   
                    </div>
                  </div>
              </div>
            </div>
            <div className="tittle-goes abt-fifth-section compassion">
              <div className="container">
                <div className="row" dangerouslySetInnerHTML={{__html: this.state.content.section6 ? 
                  this.state.content.section6 : '' }}>    
                </div>
              </div>
            </div>
          </div>
          <div className="testimonial-section">
            <div className="container">
              <div className="row content">
                <div className="col-md-12 post-entry">
                  <div dangerouslySetInnerHTML={{__html: this.state.content.section7 ?
                      this.state.content.section7 : '' }} className="testimonial-wrapper"> 
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div id="home-section-bussiness-trust" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">
                  <h2>TRUSTED PARTNERSHIPS</h2>
                </div>
                <div className="container my-4">
                  <div
                  id="logo_slider"
                  className="carousel slide carousel-multi-item"
                  data-ride="carousel"
                  >
                    <div className="carousel-inner" role="listbox">
                      <div className="carousel-item active">
                        <div className="row" dangerouslySetInnerHTML={{__html: this.state.content.section8 ?
                            this.state.content.section8 : '' }} >
                        </div>
                      </div>
                      <div className="carousel-item">
                        <div className="row"  dangerouslySetInnerHTML={{__html: this.state.content.section8 ?
                            this.state.content.section8 : '' }}>
                        </div>
                      </div>
                    </div>
                    <div className="controls-top">
                      <a
                        className="carousel-control-prev"
                        role="button"
                        href="#logo_slider"
                        data-slide="prev"
                        >
                        <i className="fa fa-chevron-left"></i>
                      </a>
                      <a
                        className="carousel-control-next"
                        role="button"
                        href="#logo_slider"
                        data-slide="next"
                      >
                        <i className="fa fa-chevron-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="home-section-bussiness-trust" className="recognition" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div  dangerouslySetInnerHTML={{__html: this.state.content.section9 ?
                    this.state.content.section9 : '' }} className="col-lg-12 col-md-12 col-sm-12">
                </div>
              </div>
            </div>
          </div>
          <div className="about-section-eleven">
            <div className="container">
              <div className="row" dangerouslySetInnerHTML={{__html: this.state.content.section10 ?
                  this.state.content.section10 : '' }}> 
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default AboutTemplate ;