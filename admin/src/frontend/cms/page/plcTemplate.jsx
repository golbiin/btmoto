import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Banner from "./../../common/banner";
import VideoV1 from "./../../common/video-v1";
import  "./../../../services/aos";

class PlcTemplate extends Component {
  constructor(...args) {
    super(...args);
    this.state = {

      categoryData: [
        {
          title: "CIMON PLC",
          sub_title: "Programmable Logic Controller",
          description:
            "A programmable logic controller (PLC) is essential for automating systems and processes. PLCs allow different machines to communicate with each other and take control of operations. This communication is critical when it comes to the monitoring and execution of various industrial tasks. Multiple types of PLC modules can be joined and linked together for a higher level of efficiency, performance, security and optimal functioning.",
          image: "plc-banner.jpg",
        },
        {
          title: "CIMON PLC",
          sub_title: "Programmable Logic Controller",
          description:
            "A programmable logic controller (PLC) is essential for automating systems and processes. PLCs allow different machines to communicate with each other and take control of operations. This communication is critical when it comes to the monitoring and execution of various industrial tasks. Multiple types of PLC modules can be joined and linked together for a higher level of efficiency, performance, security and optimal functioning.",
          image: "plc-banner.jpg",
        },
        {
          title: "CIMON IPC",
          sub_title: "Programmable Logic Controller",
          description:
            "A programmable logic controller (PLC) is essential for automating systems and processes. PLCs allow different machines to communicate with each other and take control of operations. This communication is critical when it comes to the monitoring and execution of various industrial tasks. Multiple types of PLC modules can be joined and linked together for a higher level of efficiency, performance, security and optimal functioning.",
          image: "plc-banner.jpg",
        },
      ],
      content: {},
      videoData: {
        title: "Efficient Control and Power",
        sub_title: "",
        description:
          "CIMON PLCs offer superior quality and streamlined efficiency. Easily add modules for extra versatility and complex function control. Take charge of your most complex industrial projects with our impressive CICON software while avoiding costly downtime by implementing a full-measure redundancy system.",
        video: "cimon.mp4",
        showButton: true,
        buttonText: "Download CICON",
        buttonNotes: "version 1.1 Updated 11-11-2019",
      },
     
    };
  }

  componentDidMount = () => {
    this.getContent();
  };

  /* Get page content */
  getContent = async () => {
    // banner slider
    if (this.props.page.content.bannerSlider) {
      const banner_content = [];
      
      this.props.page.content.bannerSlider.map((banner, index) => {
        const Setdata = {};
        Setdata.title = banner.heading ? banner.heading : '';
        Setdata.description = banner.subheading ? banner.subheading : '';
        Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
        banner_content.push(Setdata);
      });
      this.setState({
        categoryData: banner_content,
      });
    }
    this.setState({ content: this.props.page.content });
    if(this.props.page.page_title){
      this.setState({ 
        page_title: this.props.page.page_title
      });
    }      
  };

  render() {
    const {
      categoryData,
      videoData,
    } = this.state;
    const images = require.context("./../../../assets/images", true);
    return (
      <React.Fragment>

        <div id="category-plc" className="product-ipc category-plc plc-template-container">
          <div className="scada-container">
            <Banner categoryInfo={categoryData} />
            <VideoV1
              section1={this.state.content.section1 ? this.state.content.section1 : ''}
              section1_sub={this.state.content.section1sub1 ? this.state.content.section1sub1 : ''}
              videourl={this.state.content.videourl ? this.state.content.videourl : 'cimon.mp4'}
              videoInfo={videoData} />
          </div>

          <Container className="plc-graph-section text-center" data-aos="fade-up"  data-aos-duration="2000">
            <Row>
              <Col dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }}>                
              </Col>
            </Row>
          </Container>
          <div className="tittle-goes" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12 tittle-head" dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}>
                </div>
              </div>
            </div>
          </div>
          {this.state.content.commonSilder ? 
          <div className="slider-section" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div
                    id="carouselSingleSlider"
                    className="carousel slide"
                    data-ride="carousel"
                  >
                    <div className="carousel-inner">
                    {this.state.content.commonSilder.map((sildes, index) => {
                      return (<div className={index === 0 ? 'carousel-item active' : 'carousel-item'}>
                      <img
                        claclassName="d-block w-100"
                        src={sildes.image ? sildes.image  :  images(`./frontend/plc-slider.png`)}
                        className="img-fluid"
                        alt="First slide"
                      ></img>
                    </div>)
                    })  
                    }
                    </div>
                    <a
                      className="carousel-control-prev"
                      href="#carouselSingleSlider"
                      role="button"
                      data-slide="prev"
                    >
                      <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Previous</span>
                    </a>
                    <a
                      className="carousel-control-next"
                      href="#carouselSingleSlider"
                      role="button"
                      data-slide="next"
                    >
                      <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Next</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>: ""}
          <div className="tittile-goes-content" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section4 ? this.state.content.section4 : '' }}>
              </div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}></div>
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section6 ? this.state.content.section6 : '' }}>
              </div>
            </div>
          </div>
          <div className="plc-common-section quard-core" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container" dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}>             
            </div>
          </div>
          <div className="cicon-container" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-md-12 tittle-head text-center" dangerouslySetInnerHTML={{ __html: this.state.content.section8 ? this.state.content.section8 : '' }}>                  
                </div>
              </div>
            </div>
          </div>

          <div className="tittile-goes-content tittle-goes-2" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section9 ? this.state.content.section9 : '' }}>                
              </div>              
              <div className="row tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section10 ? this.state.content.section10 : '' }}>               
              </div>
              <div className="row tittle-goes-wrapper certificates-section" dangerouslySetInnerHTML={{ __html: this.state.content.section11 ? this.state.content.section11 : '' }}>              
              </div>
            </div>
          </div>

          <div className="plc-icons-container" data-aos="fade-up"  data-aos-duration="2000">
            <Container className="text-center" dangerouslySetInnerHTML={{ __html: this.state.content.section12 ? this.state.content.section12 : '' }}>             
            </Container>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default PlcTemplate;
