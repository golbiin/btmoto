import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";

import Uploadprofile from "../common/uploadProfileimage";
import UploadThumb from "../common/uploadThumbimage";


import * as settingsService from "../../ApiServices/admin/settings";
import * as manageUser from "../../ApiServices/admin/manageUser";
import * as manageSource from "../../ApiServices/admin/manageSource";
import * as Industry from "../../ApiServices/admin/industry";
import CKEditor from "ckeditor4-react";
import { apiUrl, siteUrl } from "../../../src/config.json";
import Moment from 'moment';
import moment from 'moment';
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import BlockUi from 'react-block-ui';
class EditArticles extends Component {
  constructor(props) {
    super(props);
    this.handleCheck = this.handleCheck.bind(this);

    this.handleEditorChange = this.handleEditorChange.bind(this);
    this.onEditorChange = this.onEditorChange.bind(this);
  }
  state = {
    spinner: false,
    checkval: "",
    profile_image: "",
    profile_image_thumb: "",
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    upload_data: "",
    upload_data_thumb: "",
    cimonNews: [],
    related_cat: '',
    releasedate: '',
    topic: '',
    industry: '',
    source: '',
    validated: false,
    newsStatus: true,
    focused: false,
    IndustryValues: [],
    SourceValues: [],
    CatItems: []
  };

  onEditorChange(evt) {
    const cimonNews = { ...this.state.cimonNews };
    cimonNews["content"] = evt.editor.getData();
    this.setState({
      cimonNews: cimonNews
    });
  }

  handleEditorChange(changeEvent) {
    const cimonNews = { ...this.state.cimonNews };
    cimonNews["content"] = changeEvent.target.value;
    this.setState({
      cimonNews: cimonNews
    });
  }

  onclassicEdit = async (value, item) => {
    if (item === "editorContent") {
      const cimonNews = { ...this.state.cimonNews };
      cimonNews["content"] = value.getData();
      this.setState({
        cimonNews: cimonNews
      });
    }
  };

  /* Checkbox on Change */
  handleCheck(e) {
    this.setState({ checkval: e.target.checked });
  }

  /*Joi validation schema*/

  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    slug: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    // pagetitle: Joi.string()
    // .required()
    // .error(() => {
    //   return {
    //     message: "This field is Required"
    //   };
    // }),

    metatag: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    metadescription: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),

    description: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    _id: Joi.optional().label("id"),
    profile_image: Joi.allow(null),
    profile_image_thumb: Joi.allow(null),
    content: Joi.allow(null),
    article_order: Joi.number()
      .required()
      .error(() => {
        return {
          message: "This field is a required field and must be a number."
        };
      })
  };

  /* Input Handle Change */
  handleChange = (event, type = null) => {
    let cimonNews = { ...this.state.cimonNews };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateNewsdata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    cimonNews[name] = value;
    this.setState({ cimonNews, errors });
  };
  validateNewsdata = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Get single news */
  componentDidMount = async () => {
    this.setState({ spinner: true });
    const id = this.props.match.params.id;
    const response = await settingsService.getSingleArticles(id);
    if (response.data.status == 1) {
      let newNewsarray = {
        _id: response.data.data.news_data._id,
        title: response.data.data.news_data.title,
        slug: response.data.data.news_data.slug,
        profile_image: response.data.data.news_data.image,
        description: response.data.data.news_data.description,
        content: response.data.data.news_data.content,
        //    pagetitle:   response.data.data.news_data.page_title ,
        metatag: response.data.data.news_data.meta_tag,
        metadescription: response.data.data.news_data.meta_desc,

        profile_image_thumb: response.data.data.news_data.image_thumb ? response.data.data.news_data.image_thumb : '',
        article_order: response.data.data.news_data.article_order
      };
      this.setState({
        //related_cat : response.data.data.news_data.related_cat,
        topic: response.data.data.news_data.topic,
        industry: response.data.data.news_data.industry,
        source: response.data.data.news_data.source,
        releasedate: moment(response.data.data.news_data.releasedate).toDate(),

      })
      setTimeout(() => {
        this.setState({ cimonNews: newNewsarray });
        let checkvalue =
          response.data.data.news_data.is_top_articles == "1" ? true : false;
        this.setState({ checkval: checkvalue });
        this.setState({ spinner: false });
      }, 2000);
    } else {
      this.setState({
        newsStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
      this.setState({ spinner: false });
    }

    this.getIndustry()
    this.getSource();
    this.getAllCatsIdsandNames()
  };



  getSource = async () => {

    const response = await manageSource.getAllSources();
    if (response.data.status == 1) {
      console.log('getIndustry', response.data.data)
      this.setState({
        SourceValues: response.data.data
      });
    } else {

    }
  };
  getIndustry = async () => {

    const response = await Industry.getAllIndustry();
    if (response.data.status == 1) {
      console.log('getIndustry', response.data.data)
      this.setState({
        IndustryValues: response.data.data
      });
    } else {

    }
  };


  /*  getAllCatsIdsandNames */
  getAllCatsIdsandNames = async () => {
    try {
      const response = await manageSource.getAllCatsIdsandNames()
      if (response.status === 200) {
        console.log('response', response.data.data)
        this.setState({ CatItems: response.data.data })
      }
    } catch (err) {
      this.setState({ blocking: false })
    }
  }
  /* Form Submit */
  handleSubmit = async () => {
    const cimonNews = { ...this.state.cimonNews };
    const errors = { ...this.state.errors };
    console.log('errors', errors);
    let result = Joi.validate(cimonNews, this.schema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });

        if (this.state.upload_data && this.state.upload_data_thumb) {


          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            cimonNews["profile_image"] = filepath;
            this.setState({ cimonNews });

            const response2 = await manageUser.uploadProfile(
              this.state.upload_data_thumb
            );
            if (response2.data.status == 1) {
              let filepath = response2.data.data.file_location;
              cimonNews["profile_image_thumb"] = filepath;
              this.setState({ cimonNews });
              this.updateArticlesData();
            } else {
              this.setState({
                submitStatus: false,
                message: response2.data.message,
                responsetype: "error"
              });
              this.setState({ spinner: false });
            }
            //  this.updateArticlesData();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }

        } else if (this.state.upload_data) {
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            cimonNews["profile_image"] = filepath;
            this.setState({ cimonNews });
            this.updateArticlesData();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }
        } else if (this.state.upload_data_thumb) {
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data_thumb
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            cimonNews["profile_image_thumb"] = filepath;
            this.setState({ cimonNews });
            this.updateArticlesData();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }
        } else {
          this.updateArticlesData();
        }
      }
    }
  };

  /* Update News */
  updateArticlesData = async () => {
    let newDate = "";
    if (this.state.releasedate != "")
      newDate = Moment(new Date(this.state.releasedate)).format('YYYY-MM-DD');
    const cimonNews = { ...this.state.cimonNews };
    //console.log(cimonNews);
    //cimonNews["checked"] = this.state.checkval == true ? "1" : "0";
    // cimonNews['related_cat'] = this.state.related_cat;
    cimonNews["releasedate"] = newDate;
    cimonNews['topic'] = this.state.topic;
    cimonNews['industry'] = this.state.industry;
    cimonNews['source'] = this.state.source;
    this.setState({ cimonNews });
    const response = await settingsService.updateArticles(cimonNews);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
  };
  // // /* Upload Image */
  onuplaodProfile = async (value, item) => {
    console.log('onuplaodProfile', value);
    let errors = { ...this.state.errors };
    let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      let file = value.target.files[0];
      upload_data = file;
      this.setState({ upload_data: upload_data });
    }
  };


  /* Upload Image thumbnail */
  onUploadThumbnail = async (value, item) => {
    console.log('onUploadThumbnail', value);

    let errors = { ...this.state.errors };
    let upload_data_thumb = { ...this.state.upload_data_thumb };
    if (item === "errors") {
      errors["profile_image_thumb"] = value;
    } else {
      let file = value.target.files[0];
      console.log('UploadThumbnail file', file);
      upload_data_thumb = file;
      this.setState({ upload_data_thumb: upload_data_thumb });
    }
  };

  handleselecttopic = async e => {
    console.log(e.target.value);
    this.setState({ topic: e.target.value })
  }
  handleselectindustry = async e => {
    console.log(e.target.value);
    this.setState({ industry: e.target.value })
  }
  handleselectsource = async e => {
    console.log(e.target.value);
    this.setState({ source: e.target.value })
  }
  handledateChange = (date) => {
    this.setState({
      releasedate: date,
    });
  };



  handleRemoveProfileImageThumb = () => {
    const cimonNews = { ...this.state.cimonNews };
    cimonNews["profile_image_thumb"] = '';
    this.setState({
      cimonNews: cimonNews,
      upload_data_thumb: ''
    });
  };

  handleRemoveProfileImage = () => {
    const cimonNews = { ...this.state.cimonNews };
    cimonNews["profile_image"] = '';
    this.setState({
      cimonNews: cimonNews,
      upload_data: ''
    });
  };


  render() {
    let checkedCond = this.state.cimonNews.checked;
    const { IndustryValues, SourceValues, CatItems } = this.state;
    console.log('upload_data_thumb', this.state.upload_data_thumb, this.state.cimonNews.profile_image_thumb);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                    <BlockUi tag="div" blocking={this.state.spinner} >
                      <div className="row addpage-form-wrap">
                        <div className="col-lg-8 col-md-12">
                          <div className="form-group">
                            <label htmlFor="">Update Articles</label>
                            <input
                              name="title"
                              value={this.state.cimonNews.title}
                              onChange={e => this.handleChange(e)}
                              type="text"
                              placeholder="Add title *"
                              className="form-control"
                            />
                            {this.state.errors.title ? (
                              <div className="error text-danger">
                                {this.state.errors.title}
                              </div>
                            ) : (
                                ""
                              )}
                          </div>
                          <div className="form-group">
                            <label htmlFor="">Slug</label>
                            <input
                              name="slug"
                              value={this.state.cimonNews.slug}
                              onChange={e => this.handleChange(e)}
                              type="text"
                              placeholder="slug *"
                              className="form-control"
                            />
                            {this.state.errors.slug ? (
                              <div className="error text-danger">
                                {this.state.errors.slug}
                              </div>
                            ) : (
                                ""
                              )}
                          </div>



                          {/* <div className="form-group">
                          <label htmlFor="">Page Title</label>
                          <input
                            name="pagetitle"
                            value={this.state.cimonNews.pagetitle}
                            onChange={e => this.handleChange(e)}
                            type="text"
                            placeholder="Page Title *"
                            className="form-control"
                          />
                          {this.state.errors.pagetitle ? (
                            <div className="error text-danger">
                              {this.state.errors.pagetitle}
                            </div>
                          ) : (
                            ""
                          )}
                        </div> */}


                          <div className="form-group">
                            <label htmlFor=""> Meta tag</label>
                            <input
                              name="metatag"
                              value={this.state.cimonNews.metatag}
                              onChange={e => this.handleChange(e)}
                              type="text"
                              placeholder="Meta Tag *"
                              className="form-control"
                            />
                            {this.state.errors.metatag ? (
                              <div className="error text-danger">
                                {this.state.errors.metatag}
                              </div>
                            ) : (
                                ""
                              )}
                          </div>


                          <div className="form-group">
                            <label htmlFor="">Meta Description</label>
                            <input
                              name="metadescription"
                              value={this.state.cimonNews.metadescription}
                              onChange={e => this.handleChange(e)}
                              type="text"
                              placeholder="Meta Description *"
                              className="form-control"
                            />
                            {this.state.errors.metatag ? (
                              <div className="error text-danger">
                                {this.state.errors.metadescription}
                              </div>
                            ) : (
                                ""
                              )}
                          </div>






                          <div className="form-group">
                            <label htmlFor="">Short Description</label>
                            <textarea
                              name="description"
                              onChange={e => this.handleChange(e)}
                              value={this.state.cimonNews.description}
                              className="form-control"
                            ></textarea>
                            {this.state.errors.description ? (
                              <div className="error text-danger">
                                {this.state.errors.description}
                              </div>
                            ) : (
                                ""
                              )}
                          </div>

                          <div className='form-group'>
                            <div className='row'>
                              <div className='col-md-12'>
                                <h5>Select topic</h5>

                                <select name="sel_topic"
                                  value={this.state.topic}
                                  className='form-group-relase'
                                  onChange={this.handleselecttopic}
                                >

                                  <option value="">Category</option>
                                  {
                                    CatItems.length > 0 ? (
                                      CatItems.map((data, index) => {
                                        return (
                                          <>

                                            <option key={data._id} value={data._id}>{data.category_name}</option>
                                          </>);
                                      })) : ""
                                  }
                                </select>

                              </div>
                            </div>
                          </div>


                          <div className='form-group'>
                            <div className='row'>
                              <div className='col-md-12'>
                                <h5>Select industry</h5>

                                <select name="sel_industry"
                                  value={this.state.industry}
                                  className='form-group-relase'
                                  onChange={this.handleselectindustry}
                                >
                                  <option value="">Select industry</option>
                                  {
                                    IndustryValues.length > 0 ? (
                                      IndustryValues.map((data, index) => {
                                        return (
                                          <>

                                            <option key={data._id} value={data._id}>{data.title}</option>
                                          </>);
                                      })) : ""
                                  }


                                </select>

                              </div>
                            </div>
                          </div>


                          <div className='form-group'>
                            <div className='row'>
                              <div className='col-md-12'>
                                <h5>Select source</h5>

                                <select name="sel_source"
                                  value={this.state.source}
                                  className='form-group-relase'
                                  onChange={this.handleselectsource}
                                >
                                  <option value="">Select Source</option>
                                  {
                                    SourceValues.length > 0 ? (
                                      SourceValues.map((data, index) => {
                                        return (
                                          <>

                                            <option key={data._id} value={data._id}>{data.title}</option>
                                          </>);
                                      })) : ""
                                  }
                                </select>

                              </div>
                            </div>
                          </div>


                          <div className='form-group relDate'>
                            <div className='row'>
                              <div className='col-md-12'>
                                <h5>Release Date</h5>
                                {}
                                <DatePicker
                                  selected={this.state.releasedate}
                                  placeholderText="Release Date *"
                                  maxDate={new Date()}
                                  dateFormat="dd-MM-yyyy"
                                  onChange={this.handledateChange}
                                // className= {form-control}
                                />

                                {/* <select
                                value={this.state.releasedate}
                                className='form-group-relase'
                                onChange={this.handleDateChange}
                              >
                                <option value=''>Release Date</option>
                                {options}
                              </select> */}
                              </div>
                            </div>
                          </div>



                          <div className="form-group">
                            <div className="row">
                              <div className="col-md-12">
                                <label htmlFor="">Content</label>
                                <CKEditor
                                  id="editor"
                                  name="editor"
                                  data={this.state.cimonNews.content}
                                  
                                 
                                   onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                 
                                    CKEDITOR.plugins.addExternal('imagecrop', siteUrl + '/plugins/image-crop-master/', 'plugin.js');
                                  
                                     
                                  
                                  }
                                  }
                                  config={{


                                    //   extraPlugins: ["videosnapshot"],
                                    extraPlugins: ["simplebutton,justify,font,colorbutton,colordialog,tableresize,dialog,dialogadvtab,tableselection,showborders"

                                    ],

                                    // removePlugins: ['save,newpage,flash,about,iframe,language'],
                                    //extraPlugins: ['simplebutton'],
 
                                    cropperJsUrl : 'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js',
                                    cropperCssUrl : 'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.css',
                            
                                    allowedContent: true,
                                    filebrowserImageUploadUrl:
                                      apiUrl + "/admin/upload/imageupload-new"
                                  }}
                                  onInit={editor => { }}
                                  onChange={this.onEditorChange}

                                  
                                />
                              </div>
                            </div>
                          </div>

                          <div className="form-group">
                            <label htmlFor="">Article Order</label>
                            <input
                              name="article_order"
                              onChange={this.handleChange}
                              value={this.state.cimonNews.article_order}
                              type="text"
                              placeholder="Article Order *"
                              className="form-control"
                            />
                            {this.state.errors.article_order ? (
                              <div className="error text-danger">
                                {this.state.errors.article_order}
                              </div>
                            ) : (
                                ""
                              )}
                          </div>



                          {/* 
                          <div className="form-group">
                            <div className="row">
                              <div className="col-md-12">
                                <label htmlFor="">Content tabletools</label>
                                <CKEditor
                                  data={this.state.cimonNews.content}
                                   onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{  
                                    extraPlugins: [ "justify", "font", "colorbutton", "colordialog", "image2" ,"tableresize","table", "dialogui","dialog","tabletools", "colordialog", "tableresize","dialogadvtab"],
                                    allowedContent: true,
                                    filebrowserImageUploadUrl:
                                      apiUrl + "/admin/upload/imageupload-new"
                                  }}
                                  onInit={editor => { }}
                                  onChange={this.onEditorChange}
                                />
                              </div>
                            </div>
                          </div> */}





                        </div>

                        <div className="col-lg-4 col-md-12 ">
                          {/* <div className="form-group form-group-inline checkbox-div">
                            <label htmlFor="">Is top Articles?</label>
                            <input
                              type="checkbox"
                              onChange={this.handleCheck}
                              checked={this.state.checkval}
                            />
                          </div> */}
                          <div className="form-group thumbanail_container">



                            {
                              this.state.cimonNews.profile_image || this.state.upload_data ? (<img
                                onClick={() =>
                                  this.handleRemoveProfileImage(
                                    // index
                                  )
                                }

                                src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1617228058913-download.jpg"
                                className="img-fluid close_btn"
                                alt="customer"
                              ></img>) : ''
                            }

                            <Uploadprofile
                              onuplaodProfile={this.onuplaodProfile}
                              value={this.state.cimonNews.profile_image}
                              errors={this.state.errors}
                            />
                          </div>

                          <div className="form-group  thumbanail_container">
                            <label>Upload Thumbnail</label>

                            {
                              this.state.cimonNews.profile_image_thumb || this.state.upload_data_thumb ? (<img
                                onClick={() =>
                                  this.handleRemoveProfileImageThumb(
                                    // index
                                  )
                                }

                                src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1617228058913-download.jpg"
                                className="img-fluid close_btn"
                                alt="customer"
                              ></img>) : ''
                            }

                            <UploadThumb
                              onUploadThumbnail={this.onUploadThumbnail}
                              value={this.state.cimonNews.profile_image_thumb ? this.state.cimonNews.profile_image_thumb : ''}
                              errors={this.state.errors}
                            />
                          </div>

                          <div className="faq-sideinputs">
                            <div className="faq-btns form-btn-wrap">
                              <div className="update_btn input-group-btn float-right">
                                <button
                                  disabled={
                                    this.state.submit_status === true
                                      ? "disabled"
                                      : false
                                  }
                                  onClick={this.handleSubmit}
                                  className="btn btn-info"
                                >
                                  {this.state.submit_status ? (
                                    <ReactSpinner
                                      type="border"
                                      color="dark"
                                      size="1"
                                    />
                                  ) : (
                                      ""
                                    )}
                                Update
                              </button>
                              </div>
                            </div>
                            {this.state.message !== "" ? (
                              <div className="tableinformation">
                                <div className={this.state.message_type}>
                                  <p>{this.state.message}</p>
                                </div>
                              </div>
                            ) : null}
                          </div>



                        </div>
                      </div>
                    </BlockUi>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default EditArticles;
