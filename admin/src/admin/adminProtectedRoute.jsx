import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import * as authServices from "../ApiServices/admin/login";
const ProtectedRoutea = ({ component: Component, render, ...rest }) => {
  const user = authServices.getCurrentUser();

  return (
    <Route
      {...rest}
      render={(props) => {
        if (!authServices.getCurrentUser())
          return <Redirect to="/admin/login" />;
        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default ProtectedRoutea;
