import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import DeleteConfirm from '../common/deleteConfirm'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import * as settingsService from '../../ApiServices/admin/liveTraining'
import ReactSpinner from 'react-bootstrap-spinner'
import memoize from 'memoize-one'
import TrashConfirm from '../common/trashConfirm'
import MultiSelect from 'react-multi-select-component'
import Joi, { join } from 'joi-browser'
import { Container, Draggable } from 'react-smooth-dnd'
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
class AdminListNews extends Component {
  constructor (props) {
    super(props)
    this.setTrainingSelectedArray = this.setTrainingSelectedArray.bind(this)
  }
  state = {
    confirm: {
      name: 'Delete',
      message: 'Are you sure you want to delete this page? ',
      action: 'delete'
    },
    typeFilterValue: '',
    modal_trash: false,
    dateFilterValue: '',
    dateFilterOptions: [],
    typeFilterOptions: [],
    search: '',
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    LiveTraining: [],
    spinner: true,
    contextActions_select: '-1',
    activetab: 'tab_a',
    columns: [
      {
        name: 'Title',
        selector: 'title',
        sortable: true,
        cell: row => (
          <div>
            <Link to={'/admin/live-training/edit/' + row._id}>{row.title}</Link>
          </div>
        )
      },

      {
        name: 'Users',
        //      selector: 'title',
        sortable: true,
        width: '175px',
        cell: row => (
          <div>
            <Link
              className='anchor_view'
              to={'/admin/live-training/training/' + row._id}
            >
              View
            </Link>
          </div>
        )
      },

      {
        name: '',
        cell: row => (
          <div className='action_dropdown' data-id={row._id}>
            <i
              data-id={row._id}
              className='dropdown_dots'
              data-toggle='dropdown'
              aria-haspopup='true'
              aria-expanded='false'
            />
            <div
              className='dropdown-menu dropdown-menu-center'
              aria-labelledby='dropdownMenuButton'
            >
              <Link
                onClick={() => this.saveAndtoogle(row._id, 'duplicate')}
                className='dropdown-item'
              >
                Duplicate
              </Link>

              <Link
                to={'/admin/live-training/edit/' + row._id}
                className='dropdown-item'
              >
                Edit Page
              </Link>

              <Link
                onClick={() => this.saveAndtoogle(row._id, 'delete')}
                className='dropdown-item'
              >
                Delete
              </Link>
              {row.page_status != 0 ? (
                <Link
                  onClick={() => this.saveAndtoogletrash(row._id, 'trash')}
                  className='dropdown-item'
                >
                  Trash
                </Link>
              ) : (
                ''
              )}
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: '56px' // custom width for icon button
      }
    ],
    message_type: '',
    message: '',
    published_training: [],
    related_Training_options: [],
    related_Training_selected: [],
    cimonNews: {},
    error: {},
    attributes: [],
    btnmonth : '-1'
  }

  componentDidMount = () => {
    this.getDateFilterOptions()
    this.getTypeFilterOptions()
    this.getAllTraining()
    this.getAllTrainingPublished()
    this.getAllEventTraining()
  }

  /* Get all published Training   */
  getAllEventTraining = async () => {
    try {
      const cimonNews = { ...this.state.cimonNews }
      const response = await settingsService.getEventTranining()
      if (response.data.status === 1) {
        this.setState({
          related_Training_selected:
            response.data.data[0].value.Training_selected,
          attributes: response.data.data[0].value.attributes,
          cimonNews: cimonNews
        })
      }
    } catch (err) {
      this.setState({ spinner: false })
    }
  }

  /* Get all published Training   */
  getAllTrainingPublished = async () => {
    try {
      const response = await settingsService.getAllTrainingPublished()
      if (response.data.status === 1) {
        const Setdata = { ...this.state.trainings }
        const data_related_row = []
        const data_selected_row = []
        response.data.data.map((Training, index) => {
          const Setdata = {}
          Setdata.value = Training._id
          Setdata.label = Training.title
          // if (this.state.related_Training.indexOf(Training._id) > -1) {
          //   data_selected_row.push(Setdata);
          // }
          data_related_row.push(Setdata)
        })

        this.setState({ related_Training_options: data_related_row })
        //   this.setState({ related_Training_selected: data_selected_row })
      }
    } catch (err) {
      this.setState({ spinner: false })
    }
  }

  setTrainingSelected = event => {
    const data_selected_row = []
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value
    })

    //this.setState({ related_Training_options: data_selected_row });
    this.setState({ related_Training_selected: event })
  }

  /*  DeletePageId */
  DeletePageId = id => {
    const data_table = this.state.data_table.filter(i => i.id !== id)
    this.setState({ data_table })
  }

  /* Get all News */
  getAllTraining = async () => {
    this.setState({ spinner: true })

    try {
      const filter = {
        date: this.state.dateFilterValue,
        type: this.state.typeFilterValue
      }
      const response = await settingsService.getAllTraining(filter)
      const data_table_row = []
      const data_table_rowTrash = []
      const data_table_rowDraft = []
      if (response.data.status === 1) {
        response.data.data.map((LiveTraining, index) => {
          if (LiveTraining.page_status == 0) {
            const SetdataTrash = {}
            SetdataTrash.title = LiveTraining.title
            SetdataTrash.slug = LiveTraining.slug
            SetdataTrash._id = LiveTraining._id
            SetdataTrash.page_status = LiveTraining.page_status
            SetdataTrash.parent_id = LiveTraining.parent_id
            SetdataTrash.description = LiveTraining.description
            SetdataTrash.page_status = LiveTraining.page_status
            data_table_rowTrash.push(SetdataTrash)
          } else if (LiveTraining.page_status == 1) {
            const Setdata = {}
            Setdata.title = LiveTraining.title
            Setdata.slug = LiveTraining.slug
            Setdata._id = LiveTraining._id
            Setdata.page_status = LiveTraining.page_status
            Setdata.parent_id = LiveTraining.parent_id
            Setdata.description = LiveTraining.description
            Setdata.page_status = LiveTraining.page_status
            data_table_row.push(Setdata)
          } else if (LiveTraining.page_status == 2) {
            const SetdataDraft = {}
            SetdataDraft.title = LiveTraining.title
            SetdataDraft.slug = LiveTraining.slug
            SetdataDraft._id = LiveTraining._id
            SetdataDraft.page_status = LiveTraining.page_status
            SetdataDraft.parent_id = LiveTraining.parent_id
            SetdataDraft.description = LiveTraining.description
            SetdataDraft.page_status = LiveTraining.page_status
            data_table_rowDraft.push(SetdataDraft)
          }
        })
      }
      this.setState({
        data_table: data_table_row,
        dataTrash: data_table_rowTrash,
        dataDraft: data_table_rowDraft
      })
      this.setState({ spinner: false })
    } catch (err) {
      this.setState({ spinner: false })
    }
  }
  /* Get all files*/
  getDateFilterOptions = async () => {
    this.setState({ spinner: true })
    try {
      const response = await settingsService.getDateFilterOptions()
      if (response.data.status === 1) {
        this.setState({
          dateFilterOptions: response.data.options[0].eventStartDate
        })
      } else {
        this.setState({ dateFilterOptions: [] })
      }
      this.setState({ spinner: false })
    } catch (err) {
      this.setState({ spinner: false })
    }
  }

  /* Get all types*/
  getTypeFilterOptions = async () => {
    this.setState({ spinner: true })
    try {
      const response = await settingsService.getTypeFilterOptions()
      if (response.data.status === 1) {
        this.setState({ typeFilterOptions: response.data.options })
      } else {
        this.setState({ typeFilterOptions: [] })
      }
      this.setState({ spinner: false })
    } catch (err) {
      this.setState({ spinner: false })
    }
  }
  searchSpace = event => {
    let keyword = event.target.value
    this.setState({ search: keyword })
  }
  doAction = action => {
    console.log('doAction----->', action)

    switch (action) {
      case 'duplicate':
        this.duplicateTraining()
        break

      case 'trash':
        this.trashTraining()
        break
      case 'delete':
        this.deleteTraining()
        break
      default:
    }
  }

  trashTraining = async () => {
    let id = this.state.index
    const response = await settingsService.trashTraining(id)
    if (response) {
      if (response.data.status == 1) {
        const data_table = this.state.data_table.filter(i => i._id !== id)
        this.setState({ data_table })
        this.toogleTrash()
        this.getAllTraining()
      }
    }
  }

  duplicateTraining = async () => {
    //   console.log('duplicateTraining----->')
    let id = this.state.index
    const response = await settingsService.duplicateTraining(id)
    if (response) {
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: 'success'
        })

        this.toogle()
        this.getAllTraining()
      }
    }
  }

  // trashTraining = async () => {
  //   let id = this.state.index;
  //   const response = await settingsService.trashTraining(id);
  //   if (response) {
  //     if (response.data.status == 1) {
  //       this.toogleTrash();
  //       this.getAllTraining();
  //     }
  //   }
  //   this.toogleTrash();
  // };

  deleteTraining = async () => {
    let id = this.state.index
    const response = await settingsService.deleteTraining(id)
    if (response) {
      if (response.data.status == 1) {
        const data_table = this.state.data_table.filter(i => i._id !== id)
        const dataTrash = this.state.dataTrash.filter(i => i._id !== id)
        const dataDraft = this.state.dataDraft.filter(i => i._id !== id)
        this.setState({
          data_table: data_table,
          dataTrash: dataTrash,
          dataDraft: dataDraft
        })
        this.toogle()
      }
    }
  }
  saveAndtoogletrash = id => {
    this.setState({ index: id })
    this.toogleTrash()
  }
  toogleTrash = () => {
    //console.log(">>>>>>>>>>>>>>>", this.state.modal_trash);
    let status = !this.state.modal_trash
    this.setState({ modal_trash: status })
    // console.log(">>>>>>>>>>>>>>>", this.state.modal_trash);
  }
  saveAndtoogle = (id, action) => {
    //  console.log('saveAndtoogleaction', id, action)
    switch (action) {
      case 'trash':
        this.setState({
          confirm: {
            name: 'Trash',
            message: 'Are you sure you want to move the item to trash? ',
            action: action
          }
        })
        break
      case 'delete':
        this.setState({
          confirm: {
            name: 'Delete',
            message: "Are you sure you'd like to delete this item?",
            action: action
          }
        })
        break

      case 'duplicate':
        this.setState({
          confirm: {
            name: 'Duplicate',
            message: "Are you sure you'd like to duplicate this item?",
            action: action
          }
        })
        break

      default:
        this.setState({
          confirm: {
            name: 'Delete',
            message: "Are you sure you'd like to delete this item?",
            action: action
          }
        })
    }
    this.setState({ index: id })
    this.toogle()
  }
  toogle = () => {
    let status = !this.state.modal
    this.setState({ modal: status })
    //  console.log('status------>', status)
  }

  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows })
  }
  handlebackorderChange = async e => {
    this.setState({ btnmonth: e.target.value })
  }

  actionHandler = e => {
    this.setState({
      message: '',
      message_type: ''
    })

    // console.log('actionHandler', e)
    const { selectedRows } = this.state
    const newsids = selectedRows.map(r => r._id)
    let status = ''
    let actionvalue = this.state.contextActions_select
    if (actionvalue === '-1') {
      return
    } else if (actionvalue === '0') {
      status = 'Trash'
    } else if (actionvalue === '1') {
      status = 'Published'
    } else if (actionvalue === '2') {
      status = 'Draft'
    } else if (actionvalue === '3') {
      status = 'Delete trash'
    }

    if (window.confirm(`Are you sure you want to move ` + status)) {
      this.setState({ toggleCleared: !this.state.toggleCleared })
      this.actionHandlerTraining(newsids, actionvalue, status)
    }
  }

  actionHandlerTraining = async (newsids, contextActions, status) => {
    // console.log('actionHandlerTraining', newsids, contextActions, status)
    try {
      const response = await settingsService.actionHandlerTraining(
        newsids,
        contextActions,
        status
      )
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: 'success'
        })

        this.getAllTraining()
      } else {
        this.setState({
          message: response.data.message,
          message_type: 'error'
        })
        this.getAllTraining()
      }
    } catch (err) {
      this.setState({
        message: 'Something went wrong',
        message_type: 'error'
      })
    }
  }

  tabActive = async tab => {
    this.setState({
      message: '',
      message_type: ''
    })
    this.setState({ activetab: tab })
  }

  handleSearchByDate = event => {
    let keyword = event.target.value
    this.setState({ dateFilterValue: keyword })
    this.setState(
      {
        dateFilterValue: keyword
      },
      () => {
        this.getAllTraining()
      }
    )
  }
  handleSearchByType = event => {
    let keyword = event.target.value
    this.setState({ typeFilterValue: keyword })
    this.setState(
      {
        typeFilterValue: keyword
      },
      () => {
        this.getAllTraining()
      }
    )
  }

  /* Form Submit */
  handleSubmit = async () => {
    try {
      //    console.log('checkState---->', this.state.cimonNews)
      const checkState = {
        Training_selected: this.state.related_Training_selected,
        attributes: this.state.attributes,
      }
      //    console.log('checkState', checkState)
      const response = await settingsService.updateEventTraining(checkState)
      if (response) {
        if (response.data.status === 1) {
          this.setState({ spinner: false })
          this.setState({
            message: response.data.message,
            message_type: 'success'
          })

          window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
          this.setState({ submit_status: false })
          setTimeout(() => {
            //   window.location.reload();
          }, 2000)
        } else {
          this.setState({ spinner: false })
          this.setState({ submit_status: false })
          window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
          this.setState({
            message: response.data.message,
            message_type: 'error'
          })
        }
      }
    } catch (err) {
      // this.setState({ spinner: false });
      // this.setState({
      //   message: err,
      //   message_type: "error",
      // });
    }
  }

  /*Joi validation schema*/

  schema = {
    button_common: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    march_button: Joi.string().allow(''),
    april_button: Joi.string().allow(''),
    may_button: Joi.string().allow('')
  }

  validateNewsdata = (name, value) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)
    return error ? error.details[0].message : null
  }

  /* Input Handle Change */
  handleChange = (event, type = null) => {
    let cimonNews = { ...this.state.cimonNews }
    const errors = { ...this.state.errors }
    this.setState({ message: '' })
    delete errors.validate
    let name = event.target.name //input field  name
    let value = event.target.value //input field value
    const errorMessage = this.validateNewsdata(name, value)
    if (errorMessage) errors[name] = errorMessage
    else delete errors[name]
    cimonNews[name] = value
    this.setState({ cimonNews, errors })
  }

  handleAttributesNameChange = idx => evt => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute
      return { ...attribute, name: evt.target.value }
    })

    this.setState({ attributes: newAttributes })
  }

  handleAttributesUrlChange = idx => evt => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute
      return {
        ...attribute,
        url: evt.target.value.toLowerCase().replace(/\s+/g, '')
      }
    })

    this.setState({ attributes: newAttributes })
  }

  handleAddAttributes = () => {
    this.setState({
      attributes: this.state.attributes.concat([
        { name: '', url: '#', child_values: [] , month : '-1' }
      ])
    })
  }

  handleRemoveAttributes = idx => () => {
    const filteredvalue = this.state.attributes.filter(
      (s, sidx) => idx !== sidx
    )
    if (filteredvalue) {
      this.setState({
        attributes: filteredvalue
      })
    }
  }
  handleAttributessubNameChange = (idx, sub_idx) => evt => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx === sidx) {
        const SetsubAttribute = attribute.child_values
        SetsubAttribute[sub_idx].name = evt.target.value
        return { ...attribute, child_values: SetsubAttribute }
      } else {
        return attribute
      }
    })
    this.setState({ attributes: newAttributes })
  }

  setTrainingSelectedArray = (event, id) => {
    const data_selected_row = []
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value
    })

    console.log('event-->', event, id, data_selected_row)

    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (id !== sidx) return attribute

      const SetsubAttribute = attribute.child_values
      return { ...attribute, child_values: event }
    })

    this.setState({ attributes: newAttributes })
    //this.setState({ related_Training_options: data_selected_row });
    this.setState({ related_Training_selected: event })
  }



  handleMonthChange = (event, id) => {
   // this.setState({ btnmonth: e.target.value });
   const newAttributes = this.state.attributes.map((attribute, sidx) => {
    if (id !== sidx) return attribute
    return { ...attribute, month: event.target.value }
  })


  console.log('newAttributes',newAttributes);

   this.setState({ attributes: newAttributes })
  };

  render () {
    let rowData = this.state.data_table
    let search = this.state.search
    let rowDataTrash = this.state.dataTrash
    let rowDataDraft = this.state.dataDraft
   // console.log('attributes---------->', this.state.attributes)
    const {
      published_training,
      related_Training_options,
      related_Training_selected
    } = this.state

    //console.log('published_training', related_Training_selected)

    const contextActions = memoize(actionHandler => (
      <div className='common_action_section'>
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value='-1'>Select</option>
          {this.state.activetab !== 'tab_a' ? (
            <option value='1'>Published</option>
          ) : (
            ''
          )}
          {this.state.activetab !== 'tab_b' ? (
            <option value='0'>Trash</option>
          ) : (
            ''
          )}
          {this.state.activetab !== 'tab_c' ? (
            <option value='2'>Draft</option>
          ) : (
            ''
          )}
          {this.state.activetab !== 'tab_a' &&
          this.state.activetab !== 'tab_c' ? (
            <option value='3'>Delete Trash</option>
          ) : (
            ''
          )}
        </select>
        <button className='danger' onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ))
    if (search.length > 0) {
      search = search.trim().toLowerCase()
      rowData = rowData.filter(l => {
        return l.title.toLowerCase().match(search)
      })
    }

    let dateFilterOptionHtml = this.state.dateFilterOptions.map(dateObj => (
      <option
        key={dateObj.month + '/' + dateObj.year}
        value={dateObj.month + '/' + dateObj.year}
      >
        {dateObj.month + '/' + dateObj.year}
      </option>
    ))

    let typeFilterOptionHtml = this.state.typeFilterOptions.map(typeObj => (
      <option key={typeObj._id} value={typeObj._id}>
        {typeObj._id == 1
          ? 'Scheduled'
          : typeObj._id == 2
          ? 'Postponed'
          : 'Cancelled'}
      </option>
    ))


    console.log('btnmonth', this.state.btnmonth);

    return (
      <React.Fragment>
        <div className='container-fluid admin-body admin-live-training-list-page'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>

            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents  home-inner-content pt-4 pb-4 pr-4'>
                  <div className='main_admin_page_common'>
                    <div className=''>
                      <div className='admin_breadcum'>
                        <div className='row'>
                          {/* <div className='col-md-1'>
                            <p className='page-title'>News</p>
                          </div> */}
                          <div className='col-md-2'>
                            <Link
                              className='add_new_btn video_add_btn'
                              to='/admin/live-training/add'
                            >
                              Add New
                            </Link>
                          </div>
                          <div className='col-md-3'>
                            <div className='bulkaction'>
                              <div className='commonserachform'>
                                <select
                                  className='form-control'
                                  name='date_filter'
                                  onChange={e => this.handleSearchByType(e)}
                                >
                                  <option value=''>All Types</option>
                                  {typeFilterOptionHtml}
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-md-3'>
                            <div className='bulkaction'>
                              <div className='commonserachform'>
                                <select
                                  className='form-control'
                                  name='date_filter'
                                  onChange={e => this.handleSearchByDate(e)}
                                >
                                  <option value=''>All Dates</option>
                                  {dateFilterOptionHtml}
                                </select>
                              </div>
                            </div>
                          </div>

                          <div className='col-md-4'>
                            <div className='searchbox'>
                              <div className='commonserachform'>
                                <span />
                                <input
                                  type='text'
                                  placeholder='Search'
                                  onChange={e => this.searchSpace(e)}
                                  name='search'
                                  className='search form-control'
                                />
                                <input type='submit' className='submit_form' />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='faq-list-table-wrapper'>
                        <React.Fragment>
                          <div
                            style={{
                              display:
                                this.state.spinner === true
                                  ? 'flex justify-content-center '
                                  : 'none'
                            }}
                            className='overlay text-center'
                          >
                            <ReactSpinner
                              type='border'
                              color='primary'
                              size='10'
                            />
                          </div>
                        </React.Fragment>
                        {this.state.message !== '' ? (
                          <p className='tableinformation'>
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          </p>
                        ) : null}
                        <ul className='nav nav-pills nav-fill'>
                          <li
                            className={
                              this.state.activetab === 'tab_a' ? 'active' : ''
                            }
                          >
                            <a
                              href='#tab_a'
                              data-toggle='pill'
                              onClick={() => this.tabActive('tab_a')}
                            >
                              All ({this.state.data_table.length})
                            </a>
                          </li>
                          <li
                            className={
                              this.state.activetab === 'tab_b' ? 'active' : ''
                            }
                          >
                            <a
                              href='#tab_b'
                              data-toggle='pill'
                              onClick={() => this.tabActive('tab_b')}
                            >
                              Trash ({this.state.dataTrash.length})
                            </a>
                          </li>

                          <li
                            className={
                              this.state.activetab === 'tab_c' ? 'active' : ''
                            }
                          >
                            <a
                              href='#tab_c'
                              data-toggle='pill'
                              onClick={() => this.tabActive('tab_c')}
                            >
                              Draft ({this.state.dataDraft.length})
                            </a>
                          </li>
                        </ul>
                        <div className='tab-content'>
                          <div className='tab-pane active' id='tab_a'>
                            <DataTable
                              columns={this.state.columns}
                              data={rowData}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent={
                                <p>There are currently no records.</p>
                              }
                            />
                          </div>
                          <div className='tab-pane' id='tab_b'>
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataTrash}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent={
                                <p>There are currently no records.</p>
                              }
                            />
                          </div>
                          <div className='tab-pane' id='tab_c'>
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataDraft}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent={
                                <p>There are currently no records.</p>
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* 
                    <div className='listliveclass'>
                      <h3>Event Training</h3>
                      <MultiSelect
                        options={related_Training_options}
                        value={related_Training_selected}
                        onChange={this.setTrainingSelected}
                      />

                    
                    </div> */}

                    <div className='form-group listliveclass'>
                      {/********************************************************************************************** */}
                      <div className='attributes'>
                        <ul className='col-md-12 pb-2 pl-0 pr-0 main_loop_sub traning_addnew'>
                          <Container>
                            {this.state.attributes
                              ? this.state.attributes.map((attribute, idx) => (
                                  <div key={idx}>
                                    <li key={idx}>
                                      <input
                                        type='text'
                                        className='form-control w-100 '  
                                        placeholder={`Button #${idx + 1}`}
                                        value={attribute.name}
                                        onChange={this.handleAttributesNameChange(
                                          idx
                                        )}
                                      />
                                      {/* <input
                                      type='text'
                                      className='form-control w-100'
                                      placeholder={`Button #${idx + 1}`}
                                      value={attribute.url}
                                      onChange={this.handleAttributesUrlChange(
                                        idx
                                      )}
                                    /> */}

                                      <select
                                        className='form-control w-100 month_list'
                                        value={attribute.month}
                                        onChange={e =>
                                          this.handleMonthChange(e, idx)
                                        }
                                      >
                                        <option value='-1'>Select Month</option>
                                        <option value='1'>January</option>
                                        <option value='2'>February</option>
                                        <option value='3'>March</option>
                                        <option value='4'>April</option>
                                        <option value='5'>May</option>
                                        <option value='6'>June</option>
                                        <option value='7'>July</option>
                                        <option value='8'>August</option>
                                        <option value='9'>September</option>
                                        <option value='10'>October</option>
                                        <option value='11'>November</option>
                                        <option value='12'>December</option>
                                      </select>

                                      <MultiSelect
                                        options={related_Training_options}
                                        value={attribute.child_values}
                                        onChange={e =>
                                          this.setTrainingSelectedArray(e, idx)
                                        }
                                      />

                                      <button
                                        className='rem-btn btn btn-danger'
                                        type='button'
                                        onClick={this.handleRemoveAttributes(
                                          idx
                                        )}
                                      >
                                        <span aria-hidden='true'>&times;</span>
                                      </button>
                                    </li>
                                  </div>
                                ))
                              : ''}
                          </Container>
                        </ul>

                        {(() => {
                          return (
                            <div className='attribute_container'>
                              <button
                                type='button'
                                onClick={this.handleAddAttributes}
                                className='add-btn btn btn-success'
                              >
                                Add New Training Button
                              </button>
                            </div>
                          )
                        })()}
                      </div>

                      {/************************************************************************************************* */}

                      <button
                        disabled={
                          this.state.submit_status === true ? 'disabled' : false
                        }
                        onClick={this.handleSubmit}
                        className='btn btn-info'
                      >
                        {this.state.submit_status ? (
                          <ReactSpinner type='border' color='dark' size='1' />
                        ) : (
                          ''
                        )}
                        Update
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          name={this.state.confirm.name}
          message={this.state.confirm.message}
          action={this.state.confirm.action}
          deleteUser={this.doAction}
        />
        <TrashConfirm
          modal={this.state.modal_trash}
          toogle={this.toogleTrash}
          trash_action={this.trashTraining}
        />
      </React.Fragment>
    )
  }
}

export default AdminListNews
