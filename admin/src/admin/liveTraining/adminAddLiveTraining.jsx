import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import * as settingsService from "../../ApiServices/admin/liveTraining";
import * as manageUser from "../../ApiServices/admin/manageUser";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl} from "../../../src/config.json";
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import moment from 'moment'
import BlockUi from 'react-block-ui';
class AdminAddNews extends Component {
  
    constructor(props) {
        super(props);
        this.handleCheck = this.handleCheck.bind(this);
        this.onEditorChange = this.onEditorChange.bind( this );  
        this.getAllEventLocation();
    }
    state = { 
        eventLocations:[],
        activetab: "tab_a",
        editorContent : "",
        checked: false , 
        data: { 
          title: "" , 
          slug: "",
          description:"", 
          eventStartDate: ''               ,
          eventStartTimeHour: '1',
          eventStartTimeMinutes: '0',
          eventStartTimeMeridiem: 'AM',
          eventEndDate: ''               ,
          eventEndTimeHour: '1',
          eventEndTimeMinutes: '0',
          eventEndTimeMeridiem: 'AM',
          allDayEvent: false,
          hideEventTime: false,
          hideEventEndTime: false,
          eventLocation: '',
          eventCost: '',
          eventStatus: 1,
          eventLink: '',
          eventId: ''
        },
        eventStartDate: '',
        profile_image:"",
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        upload_data: "",
        editordata: '',
        focused: false,
        date : new Date(),
    }
    tabActive = async (tab) => {
      this.setState({ activetab: tab });
    };
    onEditorChange( evt ) {
		this.setState( {
			editordata: evt.editor.getData()
		} );
    }
    
    onclassicEdit = async (value, item) => {
        if (item === "editorContent") {
            this.setState({
                editorContent : value.getData()
            })
        }  
    };

    /* Checkbox on change */
    handleCheck(e){
        this.setState({
         checked: e.target.checked
        })
        
    }
    
    /* Joi validation schema */  
    schema = {
        title: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        slug: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        
        facebook: Joi.string().uri()
        .required()
        .error(() => {
          return {
            message: "This field is a required field valid url",
          };
        }),
        twitter: Joi.string() .uri()
        .required()
        .error(() => {
          return {
            message: "This field is a required field valid url",
          };
        }),
        linkedin: Joi.string() .uri()
        .required()
        .error(() => {
          return {
            message: "This field is a required field valid url",
          };
        }),
        email: Joi.string().email()
        .required()
        .error(() => {
          return {
            message: "This field is a required valid email",
          };
        }),
        description: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        profile_image:Joi.allow(null),
        date : Joi.date(),
        eventStartDate: Joi.date()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        eventStartTimeHour: Joi.string()
        .allow(''),
        eventStartTimeMinutes: Joi.string()
        .allow(''),
        eventStartTimeMeridiem: Joi.string()
        .allow(''),
        
        eventEndDate: Joi.date()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        eventEndTimeHour: Joi.string()
        .allow(''),
        eventEndTimeMinutes: Joi.string()
        .allow(''),
        eventEndTimeMeridiem: Joi.string()
        .allow(''),
        eventLocation: Joi.string()
        .allow(''),
        eventCost: Joi.string()
        .allow(''),
        eventLink: Joi.string().uri()
        .allow('').error(() => {
          return {
            message: "Zoom link must be a valid uri",
          };
        }),
        eventId: Joi.string()
        .allow(''),
        allDayEvent: Joi.boolean()
        .allow(''),
        hideEventTime: Joi.boolean()
        .allow(''),
        hideEventEndTime: Joi.boolean()
        .allow(''),
        eventStatus: Joi.number()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
    };/* Input Handle Change */
    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const data = { ...this.state.data };
        const errorMessage = this.validateProperty(input);
        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        data[input.name] = input.value;
        this.setState({ data, errors });
    };
    handledateChange = date => {
      const event = new Date();
      this.setState({
        date: (date ? date : moment(event).toDate())
      })
    }
    handleEventDateChange = (name,date) => {
      
      const data = { ...this.state.data };
      data[name] = date;
      this.setState({ data });

      if(date) {
        const errors = { ...this.state.errors };        
        const obj = { [name]: date };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        const errorMessage = error ? error.details[0].message : null;

        if (errorMessage) errors[name] = errorMessage;
        else delete errors[name];

        this.setState({ errors });
      }

      console.log(this.state.data);

    }

    handleCheckboxChange = async ({ currentTarget: input }) => {
      const errors = { ...this.state.errors };
      const data = { ...this.state.data };
      const errorMessage = this.validateProperty(input);
      
      if (errorMessage) errors[input.name] = errorMessage;
      else delete errors[input.name];
      data[input.name] = input.checked;
      this.setState({ data, errors });
    };
    handleRadioChange = async ({ currentTarget: input }) => {
      const errors = { ...this.state.errors };
      const data = { ...this.state.data };
      const errorMessage = this.validateProperty(input);
      
      if (errorMessage) errors[input.name] = errorMessage;
      else delete errors[input.name];
      data[input.name] = input.value;
      this.setState({ data, errors });
    };

    /*Joi Validation Call*/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };
      
    /* Form Submit */
    handleSubmit = async () => {

        const data = { ...this.state.data };
        const errors = { ...this.state.errors };
        let result = Joi.validate(data, this.schema);
        console.log('entered', data, result);
        if (result.error) {
          let path = result.error.details[0].path[0];
          let errormessage = result.error.details[0].message;
          errors[path] = errormessage;
          this.setState({
            errors: errors  
          })
        } else {
          if (errors.length > 0) {
              this.setState({
                  errors: errors  
                })
          }else{
              this.setState({ submit_status: true });
              this.setState({ spinner: true });              
              if (this.state.upload_data) {
                  const response1 = await manageUser.uploadProfile(
                    this.state.upload_data
                  );
                  if (response1.data.status == 1) {
                    let filepath = response1.data.data.file_location;
                    this.setState({ profile_image : filepath  });
                    this.updateNewsData();
                  } else {
                    this.setState({
                      submitStatus: false,
                      message: response1.data.message,
                      responsetype: "error",
                    });
                  }
                } else {
                  this.updateNewsData();
                }
          }      
        }
    };
    /* Add news */
    updateNewsData = async () => {
        const news_data = {
            title: this.state.data.title,
            slug: this.state.data.slug, 
            description : this.state.data.description,
            profile_image : this.state.profile_image,
            checked : (this.state.checked == true ? '1' : '0'),
            content : this.state.editordata,
            facebook : this.state.data.facebook,
            twitter : this.state.data.twitter,
            linkedin : this.state.data.linkedin,
            email : this.state.data.email,
            date : this.state.date,
            eventStartDate: this.state.data.eventStartDate,              
            eventStartTimeHour: this.state.data.eventStartTimeHour, 
            eventStartTimeMinutes:this.state.data.eventStartTimeMinutes, 
            eventStartTimeMeridiem: this.state.data.eventStartTimeMeridiem, 
            eventEndDate: this.state.data.eventEndDate,               
            eventEndTimeHour: this.state.data.eventEndTimeHour, 
            eventEndTimeMinutes: this.state.data.eventEndTimeMinutes, 
            eventEndTimeMeridiem: this.state.data.eventEndTimeMeridiem, 
            allDayEvent: this.state.data.allDayEvent, 
            hideEventTime: this.state.data.hideEventTime, 
            hideEventEndTime: this.state.data.hideEventEndTime, 
            eventLocation: this.state.data.eventLocation, 
            eventCost: this.state.data.eventCost, 
            eventStatus: this.state.data.eventStatus,
            eventLink: this.state.data.eventLink, 
            eventId: this.state.data.eventId, 
        }
       

        const response = await settingsService.createTraining(news_data);
        if (response) {
            if(response.data.status === 1){
                this.setState({ spinner: false });
                this.setState({
                    message: response.data.message,
                    message_type: "success",
                  });
                  this.setState({ submit_status: false });
                  setTimeout(() => { 
                    window.location.reload();
                  }, 2000);
            } else {
                this.setState({ spinner: false });
                this.setState({ submit_status: false });
                this.setState({
                    message: response.data.message,
                    message_type: "error",
                  });
            }
        }
    }
/* upload image */
    onuplaodProfile = async (value, item) => {
        let errors = { ...this.state.errors };
        
        let upload_data = { ...this.state.upload_data };
        if (item === "errors") {
          errors["profile_image"] = value;
        } else {
          let file = value.target.files[0];
          upload_data = file;
          this.setState({ upload_data : upload_data });
        }
    };

    
    /* Get single Training */
    componentDidMount = async () => {
      // this.getAllEventLocation();    
    };
    /* Get all News */
    getAllEventLocation = async () => {
      this.setState({ spinner: true })
      try {
        const response = await settingsService.getAllActiveEventLocation(1);
        if (response.data.status === 1) {
          this.setState({
            eventLocations: response.data.locations,
          })
        } else {
          this.setState({
            eventLocations: [],
          })
        }
        this.setState({ spinner: false })
      } catch (err) {
        this.setState({ spinner: false });
        this.setState({
          eventLocations: [],
        })
      }
    }

    render() { 
        let checkedCond  = this.state.checked;  
        let hourOptions = []    ;
        let minuteOptions = [];
        for(var i = 1; i <= 12; i++) {
          var obj = {};
          obj['value'] = i;
          obj['label'] = i;
          hourOptions.push(obj);
        }
        for(var i = 0; i <= 11; i++) {
          var obj = {};
          obj['value'] = i*5;
          obj['label'] = i*5;
          minuteOptions.push(obj);
        }
        let locOptionHtml = this.state.eventLocations.map(locObj => (
          <option
            key={locObj._id }
            value={locObj._id }
          >
            {locObj.location_name}
          </option>
        ));
        return ( <React.Fragment>
            <div className="container-fluid admin-body admin-live-training">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div> 
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4" >  
                    <div className="addpage-form">
                        <BlockUi tag="div" blocking={this.state.spinner} >
                        <div  className="row addpage-form-wrap">
                          
                            <div className="col-lg-8 col-md-12">                               
                                                       
                                <div className="form-group">
                                    <label htmlFor="">Add New</label>
                                    <input name="title" onChange={this.handleChange}  type="text" placeholder="Add title *" 
                                    className="form-control"/>
                                    {this.state.errors.title ? (<div className="error text-danger">
                                            {this.state.errors.title}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Slug</label>
                                    <input name="slug" onChange={this.handleChange}  
                                     value={this.state.data.slug}
                                    type="text" placeholder="slug *" className="form-control"/>
                                    {this.state.errors.slug ? (<div className="error text-danger">
                                            {this.state.errors.slug}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Short Description</label>
                                    <textarea name="description" 
                                    onChange={this.handleChange} 
                                    value={this.state.description}
                                    className="form-control"></textarea>
                                    {this.state.errors.description ? (<div className="error text-danger">
                                            {this.state.errors.description}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <label htmlFor="">Content</label>
                                            <CKEditor
                                                data={this.state.editordata}
                                                onBeforeLoad={CKEDITOR => {
                                                  CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                                }
                                                }
                                                config={ {
                                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                    allowedContent: true,
                                                    filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                                    } }

                                                onInit = { 
                                                    editor => {          
                                                    } 
                                                }
                                                onChange={this.onEditorChange}
                                                style={{
                                                    float: 'left',
                                                    width: '99%'
                                                }}
                                            />
                                        </div>                                        
                                    </div>
                                </div> 
                                <div className="form-group">
                                    <label htmlFor="">Facebook</label>
                                    <input name="facebook" onChange={this.handleChange}  
                                     value={this.state.facebook}
                                    type="text" placeholder="Facebook *" className="form-control"/>
                                    {this.state.errors.facebook ? (<div className="error text-danger">
                                            {this.state.errors.facebook}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Twitter</label>
                                    <input name="twitter" onChange={this.handleChange}  
                                     value={this.state.twitter}
                                    type="text" placeholder="Twitter *" className="form-control"/>
                                    {this.state.errors.twitter ? (<div className="error text-danger">
                                            {this.state.errors.twitter}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Linkedin</label>
                                    <input name="linkedin" onChange={this.handleChange}  
                                     value={this.state.linkedin}
                                    type="text" placeholder="Linkedin *" className="form-control"/>
                                    {this.state.errors.linkedin ? (<div className="error text-danger">
                                            {this.state.errors.linkedin}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Email</label>
                                    <input name="email" onChange={this.handleChange}  
                                     value={this.state.email}
                                    type="text" placeholder="Email *" className="form-control"/>
                                    {this.state.errors.email ? (<div className="error text-danger">
                                            {this.state.errors.email}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="col-md-12">
                                  <div className="product-attribute-tab row bg-white">
                                    <div className="col-xl-12 col-md-12 col-sm-12">
                                      <p>
                                        Event Details
                                      </p>
                                    </div>
                                    <div className="col-xl-3 col-md-3 col-sm-3">
                                      <ul className="nav nav-pills nav-fill">
                                        <li
                                          className={
                                            this.state.activetab === "tab_a"
                                              ? "active"
                                              : ""
                                          }
                                        >
                                          <a
                                            href="#tab_a"
                                            data-toggle="pill"
                                            onClick={() => this.tabActive("tab_a")}
                                          >
                                            Date And Time
                                          </a>
                                        </li>
                                        <li
                                          className={
                                            this.state.activetab === "tab_b"
                                              ? "active"
                                              : ""
                                          }
                                        >
                                          <a
                                            href="#tab_b"
                                            data-toggle="pill"
                                            onClick={() => this.tabActive("tab_b")}
                                          >
                                            Location/Venue
                                          </a>
                                        </li>
                                        <li
                                          className={
                                            this.state.activetab === "tab_c"
                                              ? "active"
                                              : ""
                                          }
                                        >
                                          <a
                                            href="#tab_c"
                                            data-toggle="pill"
                                            onClick={() => this.tabActive("tab_c")}
                                          >
                                            Cost
                                          </a>
                                        </li>
                                        <li
                                          className={
                                            this.state.activetab === "tab_e"
                                              ? "active"
                                              : ""
                                          }
                                        >
                                          <a
                                            href="#tab_e"
                                            data-toggle="pill"
                                            onClick={() => this.tabActive("tab_e")}
                                          >
                                            Links
                                          </a>
                                        </li>
                                        <li
                                          className={
                                            this.state.activetab === "tab_d"
                                              ? "active"
                                              : ""
                                          }
                                        >
                                          <a
                                            href="#tab_d"
                                            data-toggle="pill"
                                            onClick={() => this.tabActive("tab_d")}
                                          >
                                            Event Status
                                          </a>
                                        </li>
                                      </ul>
                                    </div>

                                    <div className="col-xl-9 col-md-9 col-sm-9 ">
                                      <div className="tab-content">
                                        <div className="tab-pane active" id="tab_a">
                                          <div className="tab-icon"></div>
                                          <div className="edit-details-head pb-4">
                                            <div className="form-group">
                                              <label>Date And Time</label>
                                              <div className="form-group-product">   
                                                <p>Start Date & Time</p>    
                                                <div className="form-group-datetime">
                                                  <div className="form-group-date">
                                                    <DatePicker
                                                    timeInputLabel='Time:'
                                                    dateFormat='MMMM d, yyyy h:mm aa'
                                                    showTimeInput
                                                    name="eventStartDate"
                                                    className="live-training-dates"
                                                    selected={this.state.data.eventStartDate}
                                                    placeholderText="Start Date *"
                                                    minDate={new Date()}                                  
                                                                                
                                                    onChange={(e) => this.handleEventDateChange("eventStartDate", e)}
                                                  />
                                                  </div>

                                                  <div sty className= {`form-group-time hideTimebox`}>
                                                    <select
                                                    value={this.state.data.eventStartTimeHour}      
                                                    name="eventStartTimeHour"
                                                    
                                                    onChange={this.handleChange}
                                                    >
                                                    {hourOptions.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                                                    </select> <span>:</span>
                                                    <select
                                                    value={this.state.data.eventStartTimeMinutes}
                                                    name="eventStartTimeMinutes"
                                                    
                                                    onChange={this.handleChange}
                                                    >
                                                    {minuteOptions.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                                                    </select>
                                                    <select 
                                                      value={this.state.data.eventStartTimeMeridiem}
                                                      name="eventStartTimeMeridiem"
                                                      onChange={this.handleChange} 
                                                      >
                                                      <option value="AM">AM</option>
                                                      <option value="PM">PM</option>
                                                    </select>
                                                    
                                                  </div>
                                                </div>
                                                {this.state.errors.eventStartDate ? (
                                                  <div className="danger">
                                                    {this.state.errors.eventStartDate}
                                                  </div>
                                                ) : (
                                                  ""
                                                )}
                            
                                              </div>
                                              
                                              <div className="form-group-product">   
                                                <p>End Date & Time</p>    
                                                <div className="form-group-datetime">
                                                  <div className="form-group-date">
                                                    <DatePicker
                                                    timeInputLabel='Time:'
                                                    dateFormat='MMMM d, yyyy h:mm aa'
                                                    showTimeInput
                                                    name="eventEndDate"
                                                    className="live-training-dates"
                                                    selected={this.state.data.eventEndDate}
                                                    placeholderText="End Date *"
                                                    minDate={this.state.data.eventStartDate? this.state.data.eventStartDate:new Date()}                                  
                                                    // dateFormat="dd-MM-yyyy"                                  
                                                    onChange={(e) => this.handleEventDateChange("eventEndDate", e)}
                                                    />
                                                  </div>
                                                  <div className= {`form-group-time hideTimebox`}>
                                                    <select
                                                    value={this.state.data.eventEndTimeHour}
                                                    name="eventEndTimeHour"
                                                    
                                                    onChange={this.handleChange}
                                                    >
                                                    {hourOptions.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                                                    </select> <span>:</span>
                                                    <select
                                                    value={this.state.data.eventEndTimeMinutes}
                                                    name="eventEndTimeMinutes"
                                                    
                                                    onChange={this.handleChange}
                                                    >
                                                    {minuteOptions.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                                                    </select>
                                                    <select
                                                      value={this.state.data.eventEndTimeMeridiem}
                                                      name="eventEndTimeMeridiem"
                                                      onChange={this.handleChange} 
                                         

                                                    >
                                                      <option value="AM">AM</option>
                                                      <option value="PM">PM</option>
                                                    </select>                                            
                                                  </div>
                                                </div>
                                                {this.state.errors.eventEndDate ? (
                                                  <div className="danger">
                                                    {this.state.errors.eventEndDate}
                                                  </div>
                                                ) : (
                                                  ""
                                                )}                    
                                              </div>
                                              
                                              <div className="form-group-product form-group-inline checkbox-div hideTimebox">
                                                  <input value={this.state.data.allDayEvent} name="allDayEvent" type="checkbox" 
                                                  className="date-checkbox"  
                                                  onChange={this.handleCheckboxChange}
                                                  />                                                                                    
                                                  <label htmlFor="">All day event</label>                                          
                                              </div>
                                              <div className="form-group-product form-group-inline checkbox-div hideTimebox">   
                                                  <input value={this.state.data.hideEventTime} 
                                                  name="hideEventTime" type="checkbox" className="date-checkbox"  
                                                  onChange={this.handleCheckboxChange}/>                                                                                 
                                                  <label htmlFor="">Hide Event Time</label>                                          
                                              </div>
                                              <div className="form-group-product form-group-inline checkbox-div hideTimebox">   
                                                  <input value={this.state.data.hideEventEndTime} name="hideEventEndTime" 
                                                  type="checkbox" className="date-checkbox"  
                                                  onChange={this.handleCheckboxChange}/>                                                                                  
                                                  <label htmlFor="">Hide Event End Time</label>                                          
                                              </div>
                                            </div>
                                          
                                          </div>
                                        </div>
                                        <div className="tab-pane" id="tab_b">
                                          <div className="form-group">
                                            <div className="form-group-product"> 
                                              <p>Event Location</p>
                                              <select 
                                                value={this.state.data.eventLocation}
                                                name="eventLocation"
                                                
                                                onChange={this.handleChange} >
                                                <option>Select Location</option>
                                                {locOptionHtml}
                                              </select>
                                            </div>
                                          </div>
                                          
                                        </div>
                                        <div className="tab-pane" id="tab_c">
                                          <div className="tab-icon"></div>
                                          <div className="gallery-dimension-wrap">
                                            <div className="form-group">
                                              <div className="form-group-product"> 
                                                <p>Cost</p>
                                                <input value={this.state.data.eventCost} name="eventCost" 
                                                onChange={this.handleChange}  type="text" placeholder="Add cost" 
                                                className="form-control"/>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        
                                        <div className="tab-pane" id="tab_e">
                                          <div className="tab-icon"></div>
                                          <div className="gallery-dimension-wrap">
                                            <div className="form-group">
                                              <div className="form-group-product"> 
                                                <p>Zoom Link</p>
                                                <input value={this.state.data.eventLink} name="eventLink" 
                                                onChange={this.handleChange}  type="text" placeholder="Add Zoom Link" 
                                                className="form-control"/>
                                                {this.state.errors.eventLink ? (
                                                  <div className="danger">
                                                    {this.state.errors.eventLink}
                                                  </div>
                                                ) : (
                                                  ""
                                                )} 
                                              </div>
                                              <div className="form-group-product"> 
                                                <p>Webinar Id</p>
                                                <input value={this.state.data.eventId} name="eventId" 
                                                onChange={this.handleChange}  type="text" placeholder="Add Webinar Id" 
                                                className="form-control"/>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        
                                        <div className="tab-pane" id="tab_d">
                                          <div className="tab-icon"></div>
                                          <div className="form-group">
                                            <div className="event_status">
                                              <div className="form-group-product form-group-inline checkbox-div">
                    
                                                  <input value="1" 
                                                    checked = {this.state.data.eventStatus == 1 ? true : false }
                                                    name="eventStatus" type="radio" className="date-checkbox"
                                                    onChange={this.handleRadioChange}  />                                                                                    
                                                  <label htmlFor="">Scheduled</label>                                          
                                              </div>
                                              <div className="form-group-product form-group-inline checkbox-div">   
                                                  <input value="2" name="eventStatus" type="radio" className="date-checkbox"  
                                                  checked = {this.state.data.eventStatus == 2}
                                                  onChange={this.handleRadioChange}/>                                                                                 
                                                  <label htmlFor="">Postponed</label>                                          
                                              </div>
                                              <div className="form-group-product form-group-inline checkbox-div">   
                                                  <input value="3" name="eventStatus" type="radio" className="date-checkbox"  
                                                  checked = {this.state.data.eventStatus == 3 }
                                                  onChange={this.handleRadioChange}/>                                                                                  
                                                  <label htmlFor="">Cancelled</label>                                          
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                  
                                      </div>
                                    </div>
                                  
                                  </div>
                                </div>
                        
                             
                            
                            </div> 
                            <div className="col-lg-4 col-md-12 ">                                
                                {/* <div className="form-group form-group-inline checkbox-div">
                                    <label htmlFor="">Is top news?</label>
                                    <input type="checkbox"  onChange={this.handleCheck}  checked={checkedCond}/>
                                </div>  */}

                        <div className='form-group thumbanail_container'>
                          <label>Published on</label>
                          <DatePicker
                            selected={this.state.date ? this.state.date : new Date()}
                            timeInputLabel='Time:'
                            dateFormat='MMMM d, yyyy h:mm aa'
                            showTimeInput
                            onChange={this.handledateChange}
                          />
                        </div>

                                    <div className="form-group thumbanail_container">
                                        <Uploadprofile
                                        onuplaodProfile={this.onuplaodProfile}
                                        value={
                                        this.state.profile_image
                                        }
                                        errors={this.state.errors}
                                        />
                                    </div>
                                <div className="faq-sideinputs">   
                                    <div className="faq-btns form-btn-wrap">   
                                     <div className="float-left">
                                     </div>
                                     <div className="update_btn input-group-btn float-right"> 
                                     <button
                                     disabled={this.state.submit_status === true ? "disabled" : false}
                                     onClick={this.handleSubmit} className="btn btn-info">
                                     {this.state.submit_status ? (
                                            <ReactSpinner type="border" color="dark" size="1" />
                                        ) : (
                                            ""
                                        )}
                                     Publish
                                     </button></div>
                                </div>
                                {this.state.message !== "" ? (
                                    <p className="tableinformation">
                                    <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                    </div>
                                    </p>
                                    ) : null
                                    }           
                                </div>
                            </div>
                          
                        </div>
                        </BlockUi>
                     </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment> );
    }
}
 
export default AdminAddNews;