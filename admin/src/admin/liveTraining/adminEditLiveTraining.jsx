import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import * as settingsService from "../../ApiServices/admin/liveTraining";
import * as manageUser from "../../ApiServices/admin/manageUser";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl} from "../../../src/config.json";
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import moment from 'moment';
import BlockUi from 'react-block-ui';


class AdminEditNews extends Component {
    constructor(props) {
        super(props);
        this.handleCheck = this.handleCheck.bind(this);

        this.handleEditorChange = this.handleEditorChange.bind( this );
        this.onEditorChange = this.onEditorChange.bind( this );
    }
    state = { 
        eventLocations:[],
        checkval: "",
        profile_image:"",
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        upload_data: "",
        cimonNews: [],
        validated: false,
        newsStatus: true,
        focused: false
    }

    onEditorChange( evt ) {
      const cimonNews = { ...this.state.cimonNews };
      cimonNews["content"] = evt.editor.getData()
      this.setState({
        cimonNews : cimonNews
      })
    }
  
    handleEditorChange( changeEvent ) {
      const cimonNews = { ...this.state.cimonNews };
      cimonNews["content"] = changeEvent.target.value
      this.setState({
        cimonNews : cimonNews
      }) 
    }

    onclassicEdit = async (value, item) => {
      if (item === "editorContent") {
          const cimonNews = { ...this.state.cimonNews };
          cimonNews["content"] = value.getData();
          this.setState({
            cimonNews : cimonNews
          })
      } 
    };

    /* Checkbox on Change */
    handleCheck(e){

        this.setState({ checkval : e.target.checked });
    }
    
    /*Joi validation schema*/  
    schema = {
        title: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        slug: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        description: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        facebook: Joi.string().uri()
        .required()
        .error(() => {
          return {
            message: "This field is a required field valid url",
          };
        }),
        twitter: Joi.string() .uri()
        .required()
        .error(() => {
          return {
            message: "This field is a required field valid url",
          };
        }),
        linkedin: Joi.string() .uri()
        .required()
        .error(() => {
          return {
            message: "This field is a required field valid url",
          };
        }),
        email: Joi.string().email()
        .required()
        .error(() => {
          return {
            message: "This field is a required valid email",
          };
        }),
        date : Joi.date(),
        _id: Joi.optional().label("id"),
        profile_image:Joi.allow(null),
        content:Joi.allow(null),
        eventStartDate: Joi.date()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        eventStartTimeHour: Joi.string()
        .allow(''),
        eventStartTimeMinutes: Joi.string()
        .allow(''),
        eventStartTimeMeridiem: Joi.string()
        .allow(''),
        
        eventEndDate: Joi.date()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        eventEndTimeHour: Joi.string()
        .allow(''),
        eventEndTimeMinutes: Joi.string()
        .allow(''),
        eventEndTimeMeridiem: Joi.string()
        .allow(''),
        eventLocation: Joi.string()
        .allow(''),
        eventCost: Joi.string()
        .allow(''),
        eventLink: Joi.string().uri()
        .allow('').error(() => {
          return {
            message: "Zoom link must be a valid uri",
          };
        }),
        eventId: Joi.string()
        .allow(''),
        allDayEvent: Joi.boolean()
        .allow(''),
        hideEventTime: Joi.boolean()
        .allow(''),
        hideEventEndTime: Joi.boolean()
        .allow(''),
        eventStatus: Joi.number()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        })
    
    };
    validateProperty = ({ name, value }) => {
      const obj = { [name]: value };
      const schema = { [name]: this.schema[name] };
      const { error } = Joi.validate(obj, schema);
      return error ? error.details[0].message : null;
    };
    /* Input Handle Change */
    handleChange = (event, type = null) => {
        let cimonNews = { ...this.state.cimonNews };
        const errors = { ...this.state.errors };
        this.setState({ message: "" });
        delete errors.validate;
        let name = event.target.name; //input field  name
        let value = event.target.value; //input field value
        const errorMessage = this.validateNewsdata(name, value);
        if (errorMessage) errors[name] = errorMessage;
        else delete errors[name];
        cimonNews[name] = value;
        this.setState({ cimonNews, errors });
    };
    handleEventDateChange = (name,date) => {
      
      const cimonNews = { ...this.state.cimonNews };
      cimonNews[name] = date;
      this.setState({ cimonNews: cimonNews });

      if(date) {
        const errors = { ...this.state.errors };        
        const obj = { [name]: date };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        const errorMessage = error ? error.details[0].message : null;

        if (errorMessage) errors[name] = errorMessage;
        else delete errors[name];

        this.setState({ errors });
      }

      console.log(this.state.data);

    }
    handleCheckboxChange = async ({ currentTarget: input }) => {
      const errors = { ...this.state.errors };
      const cimonNews = { ...this.state.cimonNews };
      const errorMessage = this.validateProperty(input);
      
      if (errorMessage) errors[input.name] = errorMessage;
      else delete errors[input.name];
      cimonNews[input.name] = input.checked;
      this.setState({ cimonNews, errors });
    };
    handleRadioChange = async ({ currentTarget: input }) => {
      const errors = { ...this.state.errors };
      const cimonNews = { ...this.state.cimonNews };
      const errorMessage = this.validateProperty(input);
      
      if (errorMessage) errors[input.name] = errorMessage;
      else delete errors[input.name];
      cimonNews[input.name] = input.value;
      this.setState({ cimonNews, errors });
    };
    validateNewsdata = (name, value) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };
  
    /* Get single Training */
    componentDidMount = async () => {

        this.setState({ spinner: true });
        this.getAllEventLocation();    
        const id = this.props.match.params.id;
        const response = await settingsService.getSingleTraining(id);
        if(response.data.status == 1){
          let newNewsarray = {
            _id: response.data.data.data._id,
            title: response.data.data.data.title,
            slug: response.data.data.data.slug,
            profile_image: response.data.data.data.image,
            description: response.data.data.data.description,
            content: response.data.data.data.content,
            facebook :  response.data.data.data.facebook,
            twitter :  response.data.data.data.twitter,
            linkedin :  response.data.data.data.linkedin,
            email : response.data.data.data.email,
            date :  response.data.data.data.date,
            eventStartDate: moment(response.data.data.data.eventStartDate).toDate(),             
            eventStartTimeHour: response.data.data.data.eventStartTimeHour,  
            eventStartTimeMinutes: response.data.data.data.eventStartTimeMinutes,  
            eventStartTimeMeridiem: response.data.data.data.eventStartTimeMeridiem,  
            eventEndDate:  moment(response.data.data.data.eventEndDate).toDate(),  
            eventEndTimeHour: response.data.data.data.eventEndTimeHour,  
            eventEndTimeMinutes: response.data.data.data.eventEndTimeMinutes,  
            eventEndTimeMeridiem: response.data.data.data.eventEndTimeMeridiem,  
            allDayEvent: response.data.data.data.allDayEvent,  
            hideEventTime: response.data.data.data.hideEventTime,  
            hideEventEndTime: response.data.data.data.hideEventEndTime,  
            eventLocation: response.data.data.data.eventLocation,  
            eventCost: response.data.data.data.eventCost,  
            eventStatus: response.data.data.data.eventStatus,  
            eventLink: response.data.data.data.eventLink, 
            eventId: response.data.data.data.eventId, 
          };
          this.setState({
            date: moment(response.data.data.data.date).toDate()
          })
          setTimeout(() => {  
            this.setState({ cimonNews : newNewsarray });
            let checkvalue =  ((response.data.data.data.is_top_news == '1') ? true : false);
            this.setState({ checkval : checkvalue });
            this.setState({spinner: false});
          }, 2000);
        }else{
          this.setState({
            newsStatus: false,
            message: response.data.message,
            responsetype: "error",
          });
          this.setState({spinner: false});
        }
    };
    
    /* Get all News */
    getAllEventLocation = async () => {
      this.setState({ spinner: true })
      try {
        const response = await settingsService.getAllActiveEventLocation(1);
        if (response.data.status === 1) {
          this.setState({
            eventLocations: response.data.locations,
          })
        } else {
          this.setState({
            eventLocations: [],
          })
        }
        this.setState({ spinner: false })
      } catch (err) {
        this.setState({ spinner: false });
        this.setState({
          eventLocations: [],
        })
      }
    }

    /* Form Submit */
    handleSubmit = async () => {
     
        const cimonNews = { ...this.state.cimonNews };
        const errors = { ...this.state.errors };
        console.log('elveee' ,cimonNews ,errors);
        let result = Joi.validate(cimonNews, this.schema);
        if (result.error) {
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
                this.setState({
                    errors: errors  
                  })
            } else {
            if (errors.length > 0) {
                this.setState({
                    errors: errors  
                  })
            }else{
                this.setState({ submit_status: true });
                this.setState({ spinner: true });
                
                if (this.state.upload_data) {
                    const response1 = await manageUser.uploadProfile(
                      this.state.upload_data
                    );
                    if (response1.data.status == 1) {
                      let filepath = response1.data.data.file_location;
                      cimonNews["profile_image"] = filepath;
                      this.setState({ cimonNews });
                      this.updateNewsData();
                    } else {
                      this.setState({
                        submitStatus: false,
                        message: response1.data.message,
                        responsetype: "error",
                      });
                    }
                  } else {
                    this.updateNewsData();
                  }
            }
        }
    };

    /* Update News */
    updateNewsData = async () => {
        const cimonNews = { ...this.state.cimonNews };
        cimonNews["checked"] = (this.state.checkval == true ? '1' : '0');
        cimonNews["date"] = (this.state.date);
        this.setState({ cimonNews });

      
        const response = await settingsService.updateTraining(cimonNews);
        if (response) {
            if(response.data.status === 1){
                this.setState({ spinner: false });
                this.setState({
                    message: response.data.message,
                    message_type: "success",
                  });
                  this.setState({ submit_status: false });
                  setTimeout(() => { 
                    window.location.reload();
                  }, 2000);
            } else {
                this.setState({ spinner: false });
                this.setState({ submit_status: false });
                this.setState({
                    message: response.data.message,
                    message_type: "error",
                  });
            }
        }
    }
/* Upload Image */
    onuplaodProfile = async (value, item) => {
        let errors = { ...this.state.errors };
        let upload_data = { ...this.state.upload_data };
        if (item === "errors") {
          errors["profile_image"] = value;
        } else {
          let file = value.target.files[0];
          upload_data = file;
          this.setState({ upload_data : upload_data });
        }
    };

    handledateChange = date => {
      const event = new Date();
      this.setState({
        date: (date ? date : moment(event).toDate())
      })
    }
    tabActive = async (tab) => {
      this.setState({ activetab: tab });
    };

    render() { 
        let checkedCond  = this.state.cimonNews.checked;
        let hourOptions = []    ;
        let minuteOptions = [];
        for(var i = 1; i <= 12; i++) {
          var obj = {};
          obj['value'] = i;
          obj['label'] = i;
          hourOptions.push(obj);
        }
        for(var i = 0; i <= 11; i++) {
          var obj = {};
          obj['value'] = i*5;
          obj['label'] = i*5;
          minuteOptions.push(obj);
        }
        let locOptionHtml = this.state.eventLocations.map(locObj => (
          <option
            key={locObj._id }
            value={locObj._id }
          >
            {locObj.location_name}
          </option>
        ));
        return ( <React.Fragment>
            <div className="container-fluid admin-body admin-live-training">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div> 
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4" >  
                    <div className="addpage-form">
                        <BlockUi tag="div" blocking={this.state.spinner} >
                          <div  className="row addpage-form-wrap">
                              <div className="col-lg-8 col-md-12">                                
                                                                
                                  <div className="form-group">
                                      <label htmlFor="">Update</label>
                                      <input name="title" 
                                      value={this.state.cimonNews.title} 
                                      onChange={(e) => this.handleChange(e)}
                                      type="text"
                                      placeholder="Add title *" 
                                      className="form-control"/>
                                      {this.state.errors.title ? (<div className="error text-danger">
                                              {this.state.errors.title}
                                              </div>
                                          ) : (
                                              ""
                                          )}
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="">Slug</label>
                                      <input name="slug" 
                                      value={this.state.cimonNews.slug} 
                                      onChange={(e) => this.handleChange(e)}
                                      type="text" placeholder="slug *" className="form-control"/>
                                      {this.state.errors.slug ? (<div className="error text-danger">
                                              {this.state.errors.slug}
                                              </div>
                                          ) : (
                                              ""
                                          )}
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="">Short Description</label>
                                      <textarea name="description"  
                                      onChange={(e) => this.handleChange(e)}
                                      value={this.state.cimonNews.description}
                                      className="form-control">
                                      </textarea>
                                      {this.state.errors.description ? (<div className="error text-danger">
                                              {this.state.errors.description}
                                              </div>
                                          ) : (
                                              ""
                                          )}
                                  </div>
                                  <div className="form-group">
                                    <div className="row">
                                      <div className="col-md-12">
                                        <label htmlFor="">Content</label>
                                        <CKEditor
                                            data={this.state.cimonNews.content}
                                            onBeforeLoad={CKEDITOR => {
                                              CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                            }
                                            }
                                            config={ {
                                              extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                allowedContent: true,
                                                filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                                } }

                                            onInit = { 
                                                editor => {          
                                                } 
                                            }
                                            onChange={this.onEditorChange}
                                        />
                                      </div> 
                                  </div>
                                  </div>
                        
                        
                                  <div className="form-group">
                                      <label htmlFor="">Facebook</label>
                                      <input name="facebook" 
                                      value={this.state.cimonNews.facebook} 
                                      onChange={(e) => this.handleChange(e)}
                                      type="text" placeholder="Facebook *" className="form-control"/>
                                      {this.state.errors.facebook ? (<div className="error text-danger">
                                              {this.state.errors.facebook}
                                              </div>
                                          ) : (
                                              ""
                                          )}
                                  </div>


                                  <div className="form-group">
                                      <label htmlFor="">Twitter</label>
                                      <input name="twitter" 
                                      value={this.state.cimonNews.twitter} 
                                      onChange={(e) => this.handleChange(e)}
                                      type="text" placeholder="Twitter *" className="form-control"/>
                                      {this.state.errors.twitter ? (<div className="error text-danger">
                                              {this.state.errors.twitter}
                                              </div>
                                          ) : (
                                              ""
                                          )}
                                  </div>

                                  <div className="form-group">
                                      <label htmlFor="">Linkedin</label>
                                      <input name="linkedin" 
                                      value={this.state.cimonNews.linkedin} 
                                      onChange={(e) => this.handleChange(e)}
                                      type="text" placeholder="Linkedin *" className="form-control"/>
                                      {this.state.errors.linkedin ? (<div className="error text-danger">
                                              {this.state.errors.linkedin}
                                              </div>
                                          ) : (
                                              ""
                                          )}
                                  </div>

                                  <div className="form-group">
                                      <label htmlFor="">Email</label>
                                      <input name="email" 
                                      value={this.state.cimonNews.email} 
                                      onChange={(e) => this.handleChange(e)}
                                      type="text" placeholder="Email *" className="form-control"/>
                                      {this.state.errors.email ? (<div className="error text-danger">
                                              {this.state.errors.email}
                                              </div>
                                          ) : (
                                              ""
                                          )}
                                  </div>

                                  <div className="col-md-12">
                                    <div className="product-attribute-tab row bg-white">
                                      <div className="col-xl-12 col-md-12 col-sm-12">
                                        <p>
                                          Event Details
                                        </p>
                                      </div>
                                      <div className="col-xl-3 col-md-3 col-sm-3">
                                        <ul className="nav nav-pills nav-fill">
                                          <li
                                            className={
                                              this.state.activetab === "tab_a"
                                                ? "active"
                                                : ""
                                            }
                                          >
                                            <a
                                              href="#tab_a"
                                              data-toggle="pill"
                                              onClick={() => this.tabActive("tab_a")}
                                            >
                                              Date And Time
                                            </a>
                                          </li>
                                          <li
                                            className={
                                              this.state.activetab === "tab_b"
                                                ? "active"
                                                : ""
                                            }
                                          >
                                            <a
                                              href="#tab_b"
                                              data-toggle="pill"
                                              onClick={() => this.tabActive("tab_b")}
                                            >
                                              Location/Venue
                                            </a>
                                          </li>
                                          <li
                                            className={
                                              this.state.activetab === "tab_c"
                                                ? "active"
                                                : ""
                                            }
                                          >
                                            <a
                                              href="#tab_c"
                                              data-toggle="pill"
                                              onClick={() => this.tabActive("tab_c")}
                                            >
                                              Cost
                                            </a>
                                          </li>
                                          <li
                                            className={
                                              this.state.activetab === "tab_e"
                                                ? "active"
                                                : ""
                                            }
                                          >
                                            <a
                                              href="#tab_e"
                                              data-toggle="pill"
                                              onClick={() => this.tabActive("tab_e")}
                                            >
                                              Links
                                            </a>
                                          </li>
                                          <li
                                            className={
                                              this.state.activetab === "tab_d"
                                                ? "active"
                                                : ""
                                            }
                                          >
                                            <a
                                              href="#tab_d"
                                              data-toggle="pill"
                                              onClick={() => this.tabActive("tab_d")}
                                            >
                                              Event Status
                                            </a>
                                          </li>
                                        </ul>
                                      </div>

                                      <div className="col-xl-9 col-md-9 col-sm-9 ">
                                        <div className="tab-content">
                                          <div className="tab-pane active" id="tab_a">
                                            <div className="tab-icon"></div>
                                            <div className="edit-details-head pb-4">
                                              <div className="form-group">
                                                <label>Date And Time</label>
                                                <div className="form-group-product">   
                                                  <p>Start Time</p>    
                                                  <div className="form-group-datetime">
                                                    <div className="form-group-date">
                                                      <DatePicker
                                                      name="eventStartDate"
                                                      className="live-training-dates"
                                                      selected={this.state.cimonNews.eventStartDate}
                                                      placeholderText="Start Date *"
                                                      minDate={new Date()}                                  
                                                      timeInputLabel='Time:'
                                                      dateFormat='MMMM d, yyyy h:mm aa'
                                                      showTimeInput                               
                                                      onChange={(e) => this.handleEventDateChange("eventStartDate", e)}
                                                    />
                                                    </div>

                                                    <div className= {`form-group-time hideTimebox`}>
                                                      <select
                                                      value={this.state.cimonNews.eventStartTimeHour}      
                                                      name="eventStartTimeHour"
                                                      
                                                      onChange={this.handleChange}
                                                      >
                                                      {hourOptions.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                                                      </select> <span>:</span>
                                                      <select
                                                      value={this.state.cimonNews.eventStartTimeMinutes}
                                                      name="eventStartTimeMinutes"
                                                      
                                                      onChange={this.handleChange}
                                                      >
                                                      {minuteOptions.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                                                      </select>
                                                      <select 
                                                        value={this.state.cimonNews.eventStartTimeMeridiem}
                                                        name="eventStartTimeMeridiem"
                                                        onChange={this.handleChange} 
                                                        >
                                                        <option value="AM">AM</option>
                                                        <option value="PM">PM</option>
                                                      </select>
                                                      
                                                    </div>
                                                  </div>
                                                  {this.state.errors.eventStartDate ? (
                                                    <div className="danger">
                                                      {this.state.errors.eventStartDate}
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )}
                              
                                                </div>
                                                
                                                <div className="form-group-product">   
                                                  <p>End Date</p>    
                                                  <div className="form-group-datetime">
                                                    <div className="form-group-date">
                                                      <DatePicker
                                                      name="eventEndDate"
                                                      className="live-training-dates"
                                                      selected={this.state.cimonNews.eventEndDate}
                                                      placeholderText="End Date *"
                                                      minDate={this.state.cimonNews.eventStartDate ? this.state.cimonNews.eventStartDate : new Date()}                                  
                                                      timeInputLabel='Time:'
                                                      dateFormat='MMMM d, yyyy h:mm aa'
                                                      showTimeInput                               
                                                      onChange={(e) => this.handleEventDateChange("eventEndDate", e)}
                                                      />
                                                    </div>
                                                    <div className= {`form-group-time hideTimebox`}>
                                                      <select
                                                      value={this.state.cimonNews.eventEndTimeHour}
                                                      name="eventEndTimeHour"
                                                      
                                                      onChange={this.handleChange}
                                                      >
                                                      {hourOptions.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                                                      </select> <span>:</span>
                                                      <select
                                                      value={this.state.cimonNews.eventEndTimeMinutes}
                                                      name="eventEndTimeMinutes"
                                                      
                                                      onChange={this.handleChange}
                                                      >
                                                      {minuteOptions.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                                                      </select>
                                                      <select
                                                        value={this.state.cimonNews.eventEndTimeMeridiem}
                                                        name="eventEndTimeMeridiem"
                                                        onChange={this.handleChange} 
                                                        
                                                      >
                                                        <option value="AM">AM</option>
                                                        <option value="PM">PM</option>
                                                      </select>                                            
                                                    </div>
                                                  </div>
                                                  {this.state.errors.eventEndDate ? (
                                                    <div className="danger">
                                                      {this.state.errors.eventEndDate}
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )}                    
                                                </div>
                                                
                                                <div className="form-group-product form-group-inline checkbox-div hideTimebox">
                                                    <input value={this.state.cimonNews.allDayEvent} 
                                                    name="allDayEvent" type="checkbox" 
                                                    className="date-checkbox"  
                                                    checked = {this.state.cimonNews.allDayEvent}
                                                    onChange={this.handleCheckboxChange}
                                                    />                                                                                    
                                                    <label htmlFor="">All day event</label>                                          
                                                </div>
                                                <div className="form-group-product form-group-inline checkbox-div hideTimebox">   
                                                    <input value={this.state.cimonNews.hideEventTime} 
                                                    name="hideEventTime" type="checkbox" className="date-checkbox"  
                                                    onChange={this.handleCheckboxChange}
                                                    checked = {this.state.cimonNews.hideEventTime}
                                                    />                                                                                 
                                                    <label htmlFor="">Hide Event Time</label>                                          
                                                </div>
                                                <div className="form-group-product form-group-inline checkbox-div hideTimebox">   
                                                    <input value={this.state.cimonNews.hideEventEndTime} name="hideEventEndTime" 
                                                    type="checkbox" className="date-checkbox" 
                                                    checked = {this.state.cimonNews.hideEventEndTime} 
                                                    onChange={this.handleCheckboxChange}/>                                                                                  
                                                    <label htmlFor="">Hide Event End Time</label>                                          
                                                </div>
                                              </div>
                                            
                                            </div>
                                          </div>
                                          <div className="tab-pane" id="tab_b">
                                            <div className="form-group">
                                              <div className="form-group-product"> 
                                                <p>Event Location</p>
                                                <select 
                                                  value={this.state.cimonNews.eventLocation}
                                                  name="eventLocation"
                                                  
                                                  onChange={this.handleChange} >
                                                  <option>Select Location</option>
                                                  {locOptionHtml}
                                                </select>
                                              </div>
                                            </div>
                                            
                                          </div>

                                          <div className="tab-pane" id="tab_c">
                                            <div className="tab-icon"></div>
                                            <div className="gallery-dimension-wrap">
                                              <div className="form-group">
                                                <div className="form-group-product"> 
                                                  <p>Cost</p>
                                                  <input value={this.state.cimonNews.eventCost} name="eventCost" 
                                                  onChange={this.handleChange}  type="text" placeholder="Add cost" 
                                                  className="form-control"/>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div className="tab-pane" id="tab_e">
                                            <div className="tab-icon"></div>
                                            <div className="gallery-dimension-wrap">
                                              <div className="form-group">
                                                <div className="form-group-product"> 
                                                  <p>Zoom Link</p>
                                                  <input value={this.state.cimonNews.eventLink} name="eventLink" 
                                                  onChange={this.handleChange}  type="text" placeholder="Add Zoom Link" 
                                                  className="form-control"/>
                                                  {this.state.errors.eventLink ? (
                                                    <div className="danger">
                                                      {this.state.errors.eventLink}
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )} 
                                                </div>
                                                <div className="form-group-product"> 
                                                  <p>Webinar Id</p>
                                                  <input value={this.state.cimonNews.eventId} name="eventId" 
                                                  onChange={this.handleChange}  type="text" placeholder="Add Webinar Id" 
                                                  className="form-control"/>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        
                                          <div className="tab-pane" id="tab_d">
                                            <div className="tab-icon"></div>
                                            <div className="form-group">
                                              <div className="event_status">
                                                <div className="form-group-product form-group-inline checkbox-div">
                      
                                                    <input value="1" 
                                                      checked = {this.state.cimonNews.eventStatus == 1 ? true : false }
                                                      name="eventStatus" type="radio" className="date-checkbox"
                                                      onChange={this.handleRadioChange}  />                                                                                    
                                                    <label htmlFor="">Scheduled</label>                                          
                                                </div>
                                                <div className="form-group-product form-group-inline checkbox-div">   
                                                    <input value="2" name="eventStatus" type="radio" className="date-checkbox"  
                                                    checked = {this.state.cimonNews.eventStatus == 2}
                                                    onChange={this.handleRadioChange}/>                                                                                 
                                                    <label htmlFor="">Postponed</label>                                          
                                                </div>
                                                <div className="form-group-product form-group-inline checkbox-div">   
                                                    <input value="3" name="eventStatus" type="radio" className="date-checkbox"  
                                                    checked = {this.state.cimonNews.eventStatus == 3 }
                                                    onChange={this.handleRadioChange}/>                                                                                  
                                                    <label htmlFor="">Cancelled</label>                                          
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                    
                                        </div>
                                      </div>
                                    
                                    </div>
                                  </div>
                        
                              </div>

                              <div className="col-lg-4 col-md-12 ">                           
                                      {/* <div className="form-group form-group-inline checkbox-div">
                                          <label htmlFor="">Is top news?</label>
                                          <input type="checkbox"  onChange={this.handleCheck}  checked={this.state.checkval}/>
                                      </div> */}
                                      <div className="form-group thumbanail_container">
                                          <Uploadprofile
                                          onuplaodProfile={this.onuplaodProfile}
                                          value={
                                          this.state.cimonNews.profile_image
                                          }
                                          errors={this.state.errors}
                                          />
                                      </div>
                                      <div className="faq-sideinputs">

                                      <div className='form-group thumbanail_container'>
                            <label>Published on</label>
                            <DatePicker
                              selected={this.state.date?this.state.date: new Date()}
                              timeInputLabel='Time:'
                              dateFormat='MMMM d, yyyy h:mm aa'
                              showTimeInput
                              onChange={this.handledateChange}
                            />
                          </div>

                                      <div className="faq-btns form-btn-wrap">   
                                        <div className="update_btn input-group-btn float-right"> 
                                          <button
                                          disabled={this.state.submit_status === true ? "disabled" : false}
                                          onClick={this.handleSubmit} className="btn btn-info">
                                          {this.state.submit_status ? (
                                          <ReactSpinner type="border" color="dark" size="1" />
                                          ) : (
                                          ""
                                          )}
                                          Update
                                          </button>
                                        </div>
                                      </div>
                                      {this.state.message !== "" ? (
                                      <div className="tableinformation">
                                        <div className={this.state.message_type}>
                                        <p>{this.state.message}</p>
                                        </div>
                                      </div>
                                      ) : null
                                      }   
                                  </div>
                              </div>
                          </div>
                        </BlockUi>
                     </div>                     
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment> );
    }
}
 
export default AdminEditNews;