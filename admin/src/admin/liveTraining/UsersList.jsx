import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar' 
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import * as LiveTrainingService from '../../ApiServices/admin/liveTraining'
import ReactSpinner from 'react-bootstrap-spinner'
import moment from "moment";
class UsersListLiveTraning extends Component {
  state = {
  
    modal_trash: false,
    dateFilterValue: '',
    dateFilterOptions: [],
    typeFilterOptions: [],
    search: '',
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    LiveTraining: [],
    spinner: true,
    contextActions_select: '-1',
    activetab: 'tab_a',
    columns: [
      {
        name: 'User',
        selector: 'user_id',
        sortable: true,
        // cell: row => (
        //   <div>
        //     <Link to={'/admin/live-training/edit/' + row._id}>{row.title}</Link>
        //   </div>
        // )
      },
      {
        name: 'Event',
        selector: 'event_id',
        sortable: true,
        // cell: row => (
        //   <div>
        //     <Link to={'/admin/live-training/edit/' + row._id}>{row.title}</Link>
        //   </div>
        // )
      },
      {
        name: 'Date',
        selector: 'created_on',
        sortable: true,
        cell: row => (
      <p>
          {
                moment(row.created_on).format(
                  "DD-MM-YYYY"
                )
          }
      </p>
        )
      },


      
      
    ],
    message_type: '',
    message: '',
   
  }

  componentDidMount = () => {

    this.getUserTraining()

  }



  /* Get all News */
  getUserTraining = async () => {
    this.setState({ spinner: true })

    try {
      const TraningId = this.props.match.params.id;
      const response = await LiveTrainingService.getUsersByTraningId(TraningId)
      if (response.data.status === 1) {
        console.log('response.data.data', response.data.data);
      }
      const data_table_row = []
      
    
      if (response.data.status === 1) {
        
         
        response.data.data.map((data, index) => {
          const Setdata = {};
          console.log('--->',data.users[0].email,data.Trainings[0].title);
           Setdata.user_id = data.users[0].email;
           Setdata.event_id = data.Trainings[0].title;
           Setdata.created_on = data.created_on;
          data_table_row.push(Setdata);
        })
       
      }
       
      this.setState({ spinner: false,
        data_table : data_table_row
      })
    } catch (err) {
      this.setState({ spinner: false })
    }
  }


  searchSpace = event => {
    let keyword = event.target.value
    this.setState({ search: keyword })
  }


 

  render() {
    let rowData = this.state.data_table
    let search = this.state.search
    
 
    if (search.length > 0) {
      search = search.trim().toLowerCase()
      rowData = rowData.filter(l => {
        return (l.user_id.toLowerCase().match(search) || l.event_id.toLowerCase().match(search))
      })
    }

     

    return (
      <React.Fragment>
        <div className='container-fluid admin-body admin-live-training-list-page'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>

            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents  home-inner-content pt-4 pb-4 pr-4'>
                  <div className='main_admin_page_common'>
                    <div className=''>
                      <div className='admin_breadcum'>
                        <div className='row'>
                          {/* <div className='col-md-1'>
                            <p className='page-title'>News</p>
                          </div> */}
                          <div className='col-md-2'>
                          <Link className="anchor_view" to={'/admin/live-training/'}>Back</Link>
                          </div>
                          <div className='col-md-6'></div>
                          <div className='col-md-4'>
                            <div className='searchbox'>
                              <div className='commonserachform'>
                                <span />
                                <input
                                  type='text'
                                  placeholder='Search'
                                  onChange={e => this.searchSpace(e)}
                                  name='search'
                                  className='search form-control'
                                />
                                <input type='submit' className='submit_form' />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='faq-list-table-wrapper'>
                        <React.Fragment>
                          <div
                            style={{
                              display:
                                this.state.spinner === true
                                  ? 'flex justify-content-center '
                                  : 'none'
                            }}
                            className='overlay text-center'
                          >
                            <ReactSpinner
                              type='border'
                              color='primary'
                              size='10'
                            />
                          </div>
                        </React.Fragment>
                        {this.state.message !== '' ? (
                          <p className='tableinformation'>
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          </p>
                        ) : null}
                        <div className='tab-content'>
                          <div className='tab-pane active' id='tab_a'>
                            <DataTable
                              columns={this.state.columns}
                              data={rowData}
                        //      selectableRows
                              highlightOnHover
                              pagination
                            //  selectableRowsVisibleOnly
                          //    clearSelectedRows={this.state.toggleCleared}
                         //     onSelectedRowsChange={this.handlerowChange}
                            
                              noDataComponent={
                                <p>There are currently no records.</p>
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       
      </React.Fragment>
    )
  }
}

export default UsersListLiveTraning
