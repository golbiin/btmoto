import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";

import * as settingsService from "../../ApiServices/admin/videoTraining";
import CKEditor from "ckeditor4-react";
import { apiUrl ,siteUrl} from "../../../src/config.json";
import moment from "moment";
import BlockUi from 'react-block-ui';
class AdminEditNews extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    spinner: false,
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    upload_data: "",
    videoData: {
      _id: "",
      video_title: "",
      video_url: "",
      category_name: "",
      video_order: ""
    },
    validated: false,
    videoStatus: true,
    focused: false
  };

  /* Joi validation schema */

  schema = {
    _id: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    video_title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    video_url: Joi.string()
      .uri()
      .required()
      .error(() => {
        return {
          message: "please enter a valid url"
        };
      }),
    category_name: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    video_order: Joi.optional()
  };

  /* Input Handle Change */
  handleChange = (event, type = null) => {
    let videoData = { ...this.state.videoData };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateVideodata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    videoData[name] = value;
    this.setState({ videoData, errors });
  };
  validateVideodata = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };
  /* Get single Training */
  componentDidMount = async () => {
    this.setState({ spinner: true });
    const id = this.props.match.params.id;
    const response = await settingsService.getSingleTraining(id);
    if (response.data.status == 1) {
      let videoarray = {
        _id: response.data.data.data._id,
        video_title: response.data.data.data.video_title,
        video_url: response.data.data.data.video_url,
        category_name: response.data.data.data.category_name,
        video_order: response.data.data.data.video_order
      };
      this.setState({ videoData: videoarray });
    } else {
      this.setState({
        videoStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
    this.setState({ spinner: false });
  };

  /* Form Submit */
  handleSubmit = async () => {
    const videoData = { ...this.state.videoData };
    const errors = { ...this.state.errors };

    let result = Joi.validate(videoData, this.schema);
    console.log(result);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors: errors });
    } else {
      if (errors.length > 0) {
        this.setState({ errors: errors });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });
        this.updateVideoData();
      }
    }
  };

  /* Update Video */
  updateVideoData = async () => {
    const videoData = { ...this.state.videoData };
    this.setState({ videoData });
    const response = await settingsService.updateTraining(videoData);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
    
  };

  render() {
    console.log(this.state.videoData);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                    <div className="row addpage-form-wrap">
                      <div className="col-lg-8 col-md-12">
                        {this.state.errors._id ? (
                          <div className="error text-danger">
                            {this.state.errors._id}
                          </div>
                        ) : (
                          ""
                        )}
                        <div className="form-group">
                          <label htmlFor="">Update Video Title</label>
                          <input
                            name="video_title"
                            value={this.state.videoData.video_title}
                            onChange={e => this.handleChange(e)}
                            type="text"
                            placeholder="Enter title *"
                            className="form-control"
                          />
                          {this.state.errors.video_title ? (
                            <div className="error text-danger">
                              {this.state.errors.video_title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Video Url</label>
                          <input
                            name="video_url"
                            value={this.state.videoData.video_url}
                            onChange={e => this.handleChange(e)}
                            type="text"
                            placeholder="Video url *"
                            className="form-control"
                          />
                          {this.state.errors.video_url ? (
                            <div className="error text-danger">
                              {this.state.errors.video_url}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label>Category *</label>
                          <select
                            className="form-control"
                            name="category_name"
                            onChange={this.handleChange}
                            value={this.state.videoData.category_name}
                          >
                            <option value="">Select Category</option>
                            <option value="Webinars">Webinars</option>
                            <option value="Tutorials/Quick Tips">
                              Tutorials/Quick Tips
                            </option>
                            <option value="Product Videos">
                              Product Videos
                            </option>
                          </select>
                          {this.state.errors.category_name ? (
                            <div className="error text-danger">
                              {this.state.errors.category_name}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Video order</label>
                          <input
                            name="video_order"
                            value={this.state.videoData.video_order}
                            onChange={e => this.handleChange(e)}
                            type="number"
                            placeholder="Video Postition *"
                            className="form-control"
                          />
                        </div>
                      </div>

                      <div className="col-lg-4 col-md-12 ">
                        <div className="faq-sideinputs">
                          <div className="faq-btns form-btn-wrap">
                            <div className="update_btn input-group-btn float-right">
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="btn btn-info"
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                                Update
                              </button>
                            </div>
                          </div>
                          {this.state.message !== "" ? (
                            <div className="tableinformation">
                              <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                              </div>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminEditNews;
