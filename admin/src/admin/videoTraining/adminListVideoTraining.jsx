import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import DeleteConfirm from "../common/deleteConfirm";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import * as settingsService from "../../ApiServices/admin/videoTraining";
import ReactSpinner from "react-bootstrap-spinner";
import memoize from "memoize-one";
import TrashConfirm from "../common/trashConfirm";

class AdminListNews extends Component {
  state = {
    typeFilterValue: "",
    dateFilterValue: "",
    dateFilterOptions: [],
    typeFilterOptions: [],
    search: "",
    modal_trash: false,
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    LiveTraining: [],
    spinner: true,
    contextActions_select: "-1",
    activetab: "tab_a",
    columns: [
      {
        name: "Title",
        selector: "video_title",
        sortable: true,
        cell: row => (
          <div>
            <Link to={"/admin/video-training/edit/" + row._id}>
              {row.video_title}
            </Link>
          </div>
        )
      },
      {
        name: "Url",
        selector: "video_url",
        sortable: true,
        cell: row => <div>{row.video_url}</div>
      },
      {
        name: "",
        cell: row => (
          <div className="action_dropdown" data-id={row._id}>
            <i
              data-id={row._id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            />
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >
              <Link
                to={"/admin/video-training/edit/" + row._id}
                className="dropdown-item"
              >
                Edit Page
              </Link>
              <Link
                onClick={() => this.saveAndtoogle(row._id)}
                className="dropdown-item"
              >
                Delete
              </Link>
              {row.page_status != 0 ? (
                <Link
                  onClick={() => this.saveAndtoogletrash(row._id, "trash")}
                  className="dropdown-item"
                >
                  Trash
                </Link>
              ) : (
                ""
              )}
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px" // custom width for icon button
      }
    ]
  };
  componentDidMount = () => {
    this.getDateFilterOptions();
    this.getTypeFilterOptions();
    this.getAllTraining();
  };
  DeletePageId = id => {
    const data_table = this.state.data_table.filter(i => i.id !== id);
    this.setState({ data_table });
  };
  dragMe = () => {
    console.log("dragged");
  };
  /* Get all files*/
  getDateFilterOptions = async () => {
    this.setState({ spinner: true });
    try {
      const response = await settingsService.getDateFilterOptions();
      if (response.data.status === 1) {
        this.setState({
          dateFilterOptions: response.data.options[0].created_on
        });
      } else {
        this.setState({ dateFilterOptions: [] });
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* Get all types*/
  getTypeFilterOptions = async () => {
    this.setState({ spinner: true });
    try {
      const response = await settingsService.getTypeFilterOptions();
      if (response.data.status === 1) {
        this.setState({ typeFilterOptions: response.data.options });
      } else {
        this.setState({ typeFilterOptions: [] });
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* Get all News */
  getAllTraining = async () => {
    this.setState({ spinner: true });
    try {
      const filter = {
        date: this.state.dateFilterValue,
        type: this.state.typeFilterValue
      };
      const response = await settingsService.getAllTraining(filter);
      const data_table_row = [];
      const data_table_rowTrash = [];
      const data_table_rowDraft = [];
      if (response.data.status === 1) {
        response.data.data.map((videoTraining, index) => {
          if (videoTraining.page_status == 0) {
            const SetdataTrash = {};
            SetdataTrash.video_title = videoTraining.video_title;
            SetdataTrash.video_url = videoTraining.video_url;
            SetdataTrash._id = videoTraining._id;
            SetdataTrash.category_name = videoTraining.category_name;
            SetdataTrash.page_status = videoTraining.page_status;
            data_table_rowTrash.push(SetdataTrash);
          } else if (videoTraining.page_status == 1) {
            const Setdata = {};
            Setdata.video_title = videoTraining.video_title;
            Setdata.video_url = videoTraining.video_url;
            Setdata._id = videoTraining._id;
            Setdata.category_name = videoTraining.category_name;
            Setdata.page_status = videoTraining.page_status;
            data_table_row.push(Setdata);
          } else if (videoTraining.page_status == 2) {
            const SetdataDraft = {};
            SetdataDraft.video_title = videoTraining.video_title;
            SetdataDraft.video_url = videoTraining.video_url;
            SetdataDraft._id = videoTraining._id;
            SetdataDraft.category_name = videoTraining.category_name;
            SetdataDraft.page_status = videoTraining.page_status;
            data_table_rowDraft.push(SetdataDraft);
          }
        });
        this.setState({
          data_table: data_table_row,
          dataTrash: data_table_rowTrash,
          dataDraft: data_table_rowDraft
        });
      }
      this.setState({
        data_table: data_table_row,
        dataTrash: data_table_rowTrash,
        dataDraft: data_table_rowDraft
      });
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  searchSpace = event => {
    let keyword = event.target.value;
    this.setState({ search: keyword });
  };

  deleteTraining = async () => {
    let id = this.state.index;
    const response = await settingsService.deleteTraining(id);
    if (response) {
      if (response.data.status == 1) {
        const data_table = this.state.data_table.filter(i => i._id !== id);
        this.setState({ data_table });
        this.toogle();
        this.getAllTraining();
      }
    }
  };

  trashTraining = async () => {
    let id = this.state.index;
    const response = await settingsService.trashTraining(id);
    if (response) {
      if (response.data.status == 1) {
        const data_table = this.state.data_table.filter(i => i._id !== id);
        this.setState({ data_table });
        this.toogleTrash();
        this.getAllTraining();
      }
    }
  };
  saveAndtoogle = id => {
    this.setState({ index: id });
    this.toogle();
  };
  saveAndtoogletrash = id => {
    this.setState({ index: id });
    this.toogleTrash();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };
  toogleTrash = () => {
    let status = !this.state.modal_trash;
    this.setState({ modal_trash: status });
  };
  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows });
  };
  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value });
  };

  actionHandler = e => {
    this.setState({
      message: "",
      message_type: ""
    });
    const { selectedRows } = this.state;
    const ids = selectedRows.map(r => r._id);
    let status = "";
    let actionvalue = this.state.contextActions_select;
    if (actionvalue === "-1") {
      return;
    } else if (actionvalue === "0") {
      status = "Trash";
    } else if (actionvalue === "1") {
      status = "Published";
    } else if (actionvalue === "2") {
      status = "Draft";
    } else if (actionvalue === "3") {
      status = "Delete trash";
    }
    if (window.confirm(`Are you sure you want to move ` + status)) {
      this.setState({ toggleCleared: !this.state.toggleCleared });
      this.actionHandlerTraining(ids, actionvalue, status);
    }
  };

  actionHandlerTraining = async (ids, contextActions, status) => {
    try {
      const response = await settingsService.actionHandlerTraining(
        ids,
        contextActions,
        status
      );
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success"
        });

        this.getAllTraining();
      } else {
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
        this.getAllTraining();
      }
    } catch (err) {
      this.setState({
        message: "Something went wrong",
        message_type: "error"
      });
    }
  };

  handleSearchByDate = event => {
    let keyword = event.target.value;
    this.setState({ dateFilterValue: keyword });
    this.setState(
      {
        dateFilterValue: keyword
      },
      () => {
        this.getAllTraining();
      }
    );
  };
  handleSearchByType = event => {
    let keyword = event.target.value;
    this.setState({ typeFilterValue: keyword });
    this.setState(
      {
        typeFilterValue: keyword
      },
      () => {
        this.getAllTraining();
      }
    );
  };

  tabActive = async tab => {
    this.setState({
      message: "",
      message_type: ""
    });
    this.setState({ activetab: tab });
  };

  render() {
    let rowData = this.state.data_table;
    let search = this.state.search;
    let rowDataTrash = this.state.dataTrash;
    let rowDataDraft = this.state.dataDraft;
    const contextActions = memoize(actionHandler => (
      <div className="common_action_section">
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value="-1">Select</option>
          {this.state.activetab !== "tab_a" ? (
            <option value="1">Published</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_b" ? (
            <option value="0">Trash</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_c" ? (
            <option value="2">Draft</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_a" &&
          this.state.activetab !== "tab_c" ? (
            <option value="3">Delete Trash</option>
          ) : (
            ""
          )}
        </select>
        <button className="danger" onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ));
    if (search.length > 0) {
      search = search.trim().toLowerCase();
      rowData = rowData.filter(l => {
        return l.video_title.toLowerCase().match(search);
      });
    }
    let dateFilterOptionHtml = this.state.dateFilterOptions.map(dateObj => (
      <option
        key={dateObj.month + "/" + dateObj.year}
        value={dateObj.month + "/" + dateObj.year}
      >
        {dateObj.month + "/" + dateObj.year}
      </option>
    ));

    let typeFilterOptionHtml = this.state.typeFilterOptions.map(typeObj => (
      <option key={typeObj._id} value={typeObj._id}>
        {typeObj._id}
      </option>
    ));

    return (
      <React.Fragment>
        <div className="container-fluid admin-body admin-video-training-page">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4">
                  <div className="main_admin_page_common">
                    <div className="">
                      <div className="admin_breadcum">
                        <div className="row">
                          {/* <div className='col-md-'>
                            <p className='page-title'>Video Training</p>
                          </div> */}
                          <div className="col-md-2">
                            <Link
                              className="add_new_btn video_add_btn"
                              to="/admin/video-training/add"
                            >
                              Add New
                            </Link>
                          </div>
                          <div className="col-md-3">
                            <div className="bulkaction">
                              <div className="commonserachform">
                                <select
                                  className="form-control"
                                  name="date_filter"
                                  onChange={e => this.handleSearchByType(e)}
                                >
                                  <option value="">All Types</option>
                                  {typeFilterOptionHtml}
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="bulkaction">
                              <div className="commonserachform">
                                <select
                                  className="form-control"
                                  name="date_filter"
                                  onChange={e => this.handleSearchByDate(e)}
                                >
                                  <option value="">All Dates</option>
                                  {dateFilterOptionHtml}
                                </select>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-4">
                            <div className="searchbox">
                              <div className="commonserachform">
                                <span />
                                <input
                                  type="text"
                                  placeholder="Search"
                                  onChange={e => this.searchSpace(e)}
                                  name="search"
                                  className="search form-control"
                                />
                                <input type="submit" className="submit_form" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="faq-list-table-wrapper">
                        <React.Fragment>
                          <div
                            style={{
                              display:
                                this.state.spinner === true
                                  ? "flex justify-content-center "
                                  : "none"
                            }}
                            className="overlay text-center"
                          >
                            <ReactSpinner
                              type="border"
                              color="primary"
                              size="10"
                            />
                          </div>
                        </React.Fragment>
                        {this.state.message !== "" ? (
                          <div className="tableinformation">
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          </div>
                        ) : null}
                        <ul className="nav nav-pills nav-fill">
                          <li
                            className={
                              this.state.activetab === "tab_a" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_a"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_a")}
                            >
                              All ({this.state.data_table.length})
                            </a>
                          </li>
                          <li
                            className={
                              this.state.activetab === "tab_b" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_b"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_b")}
                            >
                              Trash ({this.state.dataTrash.length})
                            </a>
                          </li>

                          <li
                            className={
                              this.state.activetab === "tab_c" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_c"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_c")}
                            >
                              Draft ({this.state.dataDraft.length})
                            </a>
                          </li>
                        </ul>
                        <div className="tab-content">
                          <div className="tab-pane active" id="tab_a">
                            <DataTable
                              columns={this.state.columns}
                              data={rowData}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                          <div className="tab-pane" id="tab_b">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataTrash}
                              selectableRows 
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                          <div className="tab-pane" id="tab_c">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataDraft}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.deleteTraining}
        />
        <TrashConfirm
          modal={this.state.modal_trash}
          toogle={this.toogleTrash}
          trash_action={this.trashTraining}
        />
      </React.Fragment>
    );
  }
}

export default AdminListNews;
