import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import * as settingsService from "../../ApiServices/admin/videoTraining";
import { apiUrl ,siteUrl} from "../../../src/config.json";
import BlockUi from 'react-block-ui';
import moment from "moment";
class AdminAddNews extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    spinner: false,
    data: {
      video_title: "",
      video_url: "",
      category_name: "",
      video_order: ""
    },
    errors: {},
    submit_status: false,
    message: "",
    message_type: ""
  };

  /* Joi validation schema */

  schema = {
    video_title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    video_url: Joi.string()
      .uri()
      .required()
      .error(() => {
        return {
          message: "please enter a valid url"
        };
      }),
    category_name: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    video_order: Joi.number()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      })
  };
  /* Input Handle Change */
  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);

    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  /*Joi Validation Call*/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Form Submit */
  handleSubmit = async () => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    let result = Joi.validate(data, this.schema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors: errors });
    } else {
      if (errors.length > 0) {
        this.setState({ errors: errors });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });
        this.updateVideoTraining();
      }
    }
  };
  /* Add Video */
  updateVideoTraining = async () => {
    const video_data = {
      video_title: this.state.data.video_title,
      video_url: this.state.data.video_url,
      category_name: this.state.data.category_name,
      video_order: this.state.data.video_order
    };
    const response = await settingsService.createTraining(video_data);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                  <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className="row addpage-form-wrap">
                      <div className="col-lg-8 col-md-12">
                        <div className="form-group">
                          <label htmlFor="">Add New</label>
                          <input
                            name="video_title"
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add title *"
                            className="form-control"
                            value={this.state.data.video_title}
                          />
                          {this.state.errors.video_title ? (
                            <div className="error text-danger">
                              {this.state.errors.video_title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Video Url</label>
                          <input
                            name="video_url"
                            onChange={this.handleChange}
                            value={this.state.data.video_url}
                            type="text"
                            placeholder="Video Url *"
                            className="form-control"
                          />
                          {this.state.errors.video_url ? (
                            <div className="error text-danger">
                              {this.state.errors.video_url}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label>Category *</label>
                          <select
                            className="form-control"
                            name="category_name"
                            onChange={this.handleChange}
                          >
                            <option value="">Select Category</option>
                            <option value="Webinars">Webinars</option>
                            <option value="Tutorials/Quick Tips">
                              Tutorials/Quick Tips
                            </option>
                            <option value="Product Videos">
                              Product Videos
                            </option>
                          </select>
                          {this.state.errors.category_name ? (
                            <div className="error text-danger">
                              {this.state.errors.category_name}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Video Order</label>
                          <input
                            name="video_order"
                            onChange={this.handleChange}
                            value={this.state.data.video_order}
                            type="text"
                            placeholder="Video Order *"
                            className="form-control"
                          />
                          {this.state.errors.video_order ? (
                            <div className="error text-danger">
                              {this.state.errors.video_order}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-12 ">
                        <div className="faq-sideinputs">
                          <div className="faq-btns form-btn-wrap">
                            <div className="update_btn input-group-btn float-right">
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="btn btn-info"
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                                Publish
                              </button>
                            </div>
                          </div>
                          {this.state.message !== "" ? (
                            <div className="tableinformation">
                              <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                              </div>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </BlockUi>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </React.Fragment>
    );
  }
}

export default AdminAddNews;
