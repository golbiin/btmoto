import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import DataTable from "react-data-table-component";
class Language extends Component {
    constructor(props) {
        super(props);
    }
    state = { 
        editorContent : "",
        checked: false , 
        id: "",
  
          language: "" , 
          code: "",
          url:"", 
          languagestatus:"",             
       
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        focused: false,
        addresses: [],
        data_table: [],
        table_message: "",
        table_message_type: "",
        delete_status: false,
        edit_status: false,
        spinner: true,
        columns: [
          {
            name: "Language",
            selector: "language",
            sortable: true,
          },
          
          {
            name: "Code",
            selector: "code",
            sortable: true,
          }, 
          {
            name: "Url",
            selector: "url",
            sortable: true,
          }, 
          {
            name: "Status",
            selector: "languagestatus",
            cell: (row) => <p>{row.languagestatus == 1 ? "Enabled" : "Disabled"}</p>,
            sortable: true,
          },
          {
            name: "Action", 
            cell: row => (
              <div className="action_dropdown" data-id={row._id}>
                <i
                  data-id={row._id}
                  className="dropdown_dots"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                ></i>
                <div
                  className="dropdown-menu dropdown-menu-center"
                  aria-labelledby="dropdownMenuButton"
                >
                  <a
                    onClick={() => this.getLanguageById(row._id)}
                    className="dropdown-item"
                  >
                    Edit
                  </a>
                  <a
                    onClick={() => this.handleRemoveLanguage(row._id)}
                    className="dropdown-item"
                  >
                    Delete
                  </a>
                </div>
              </div>
            ),
            allowOverflow: false,
            button: true,
            width: "56px", // custom width for icon button
          },
        ],
    }
    
    /* Joi validation schema */  
    schema = {
        language: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        code: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        
        url: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
       
        languagestatus: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
    
    };

    componentDidMount = () => {
      this.getAllLanguages();
    };

/*Get all Languages */
    getAllLanguages = async () => {
      this.setState({ spinner: true });
      try {
        const response = await settingsService.getAllLanguages();
        console.log('get all languages', response);
        if (response.data.status == 1) {
          this.setState({ addresses: response.data.data });
          const Setdata = { ...this.state.data_table };
          const data_table_row = [];
          response.data.data.map((add, index) => {
            const Setdata = {};
            Setdata.language = add.language;
            Setdata.code = add.code;
            Setdata.url = add.url;
            Setdata.languagestatus = add.languagestatus;
            Setdata._id = add._id;
            data_table_row.push(Setdata); 
          });     
          this.setState({
            data_table: data_table_row,
          });
        }
        this.setState({ spinner: false });
      } catch (err) {
        this.setState({ spinner: false });
      }
    };

/* get language by id */
    getLanguageById = async (id) => {
      const response = await settingsService.getLanguageById({ id: id });
      if (response.data.status == 1) {
        this.setState({
          edit_status: true,
          submit_status: false,
          message: "",
          message_type: "",
          delete_status: false,
          language: response.data.data.language,
          code: response.data.data.code,
          url: response.data.data.url,
         languagestatus: response.data.data.languagestatus,
          id: response.data.data._id,
        });
      }
    };

    addLanguage = () => {
      this.setState({
        language: "",
        code: "",
        url: "",
        languagestatus: "1",
        attributes: [],
        errors: {},
        submit_status: false,
        message: "",
        message_type: "",
        delete_status: false,
        edit_status: false,
        descrption: "",
      });
    };

    /*delete languages */
    handleRemoveLanguage = async (id) => {
      this.setState({
        delete_status: true,
      });
      const response = await settingsService.removeLanguage({ _id: id });
      console.log('removeLanguage',response);
      if (response.data.status === 1) {
        let newLang = [...this.state.data_table];
        newLang = newLang.filter(function (obj) {
          return obj._id !== id;
        });
        this.setState({
          table_message: response.data.message,
          table_message_type: "sucess",
        });
  
        this.setState({
          data_table: newLang,
          delete_status: false,
          edit_status: false,
        });
        setTimeout(() => {
          this.setState(() => ({ table_message: "" }));
        }, 5000);
      } else {
        this.setState({
          table_message: response.data.message,
          table_message_type: "sucess",
        });
        setTimeout(() => {
          this.setState(() => ({ table_message: "" }));
        }, 5000);
      }
    };
  
    
    /* Input Handle Change */
       handleChange = (input) => (event) => {
      const errors = { ...this.state.errors };
      delete errors.validate;
      const errorMessage = this.validateProperty(input, event.target.value);
      if (errorMessage) errors[input] = errorMessage;
      else delete errors[input];
      let value = event.target.value;
      this.setState({ [input]: value, errors });
    };
 
    /*Joi Validation Call*/
    validateProperty = (name, value) => {
      const obj = { [name]: value };
      const schema = { [name]: this.schema[name] };
      const { error } = Joi.validate(obj, schema);
      return error ? error.details[0].message : null;
    };
      
     /* Form Submit */
     handleSubmit = async (e) => {
      e.preventDefault();
      const checkState = {
        language: this.state.language,
        code: this.state.code,
        url: this.state.url,
        languagestatus: this.state.languagestatus,
      };
        const errors = { ...this.state.errors };
        this.setState({ errors: {} });  
        let result = Joi.validate(checkState, this.schema); 
        if (result.error) {      
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({ errors: errors })
            }
            else{
              const language_data = {
                language: this.state.language,
                code: this.state.code, 
                url : this.state.url,
                languagestatus : this.state.languagestatus,
                id: this.state.id,  
              };
              try {
                this.setState({
                  submit_status: true,
                  message: "",
                  message_type: "",
                }); 
                if (this.state.edit_status === true) {         
                  const response = await settingsService.updateLanguage(language_data);
                  console.log('updateLanguage',response);
                  if (response) {
                    this.setState({ submit_status: false });
                    if (response.data.status === 1) {
                      this.setState({
                        message: response.data.message,
                        message_type: "success",
                        language: "",
                        code: "",
                        url : "",
                        languagestatus: "1",
                         id: "",
                        edit_status: false,
                      });
                      this.getAllLanguages();
                    } else {
                      this.setState({
                        message: response.data.message,
                        message_type: "error",
                      });
                    }
                  } else {
                    this.setState({ submit_status: false });
                    this.setState({
                      message: "Something went wrong! Please try after some time",
                      message_type: "error",
                    });
                  }
                  setTimeout(() => {
                    this.setState(() => ({ message: "" }));
                  }, 5000);
                } else {
                  const response = await settingsService.createLanguages(language_data);
                  if (response) {
                    this.setState({ submit_status: false });
                    if (response.data.status === 1) {
                      this.setState({
                        message: response.data.message,
                        message_type: "success",
                        language: "",
                        code: "",
                        url : "",
                        languagestatus: "1",
                      });
                      this.getAllLanguages();
                    } else {
                      this.setState({
                        message: response.data.message,
                        message_type: "error",
                      });
                    }
                  } else {
                    this.setState({ submit_status: false });
                    this.setState({
                      message: "Something went wrong! Please try after some time",
                      message_type: "error",
                    });
                  }
                  setTimeout(() => {
                    this.setState(() => ({ message: "" }));
                  }, 5000);
                }
                
              } catch (err) {}
            }
          };  

  /* Select languagestatus */
  handleLanguageStatus = async (e) => {
    this.setState({ languagestatus: e.target.value });
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty('languagestatus', e.target.value);
    if (errorMessage) errors['languagestatus'] = errorMessage;
    else delete errors['languagestatus'];
    this.setState({ 'errors': errors });
  };


    render() { 
      let datarow = this.state.data_table;   
        return ( <React.Fragment>
            <div className="container-fluid admin-body">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div> 
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents main_admin_page_common  useradd-new pt-2 pb-4 pr-4" >  
                    <div className="categories-wrap p-4">
                        <div  className="row">
                        <div className="col-md-4">
                          <div className="card">
                            <div className="card-header">
                              <strong>
                                {this.state.edit_status === true
                                  ? "Edit"
                                  : "Add"}{" "}
                               Languages
                              </strong>
                              {this.state.edit_status === true ? (
                                <button
                                  onClick={this.addLanguage}
                                  className="ml-4 add-cat-btn btn btn-success"
                                >
                                  + Add New
                                </button>
                              ) : (
                                ""
                              )}
                            </div>

                            <div className="card-body categories-add shipping-add">
                              
                              <div className="form-group">
                                <input
                                  className="form-control"
                                  type="text"
                                  value={this.state.language}
                                  onChange={this.handleChange('language')}
                                  placeholder="Language *"
                                />
                                {this.state.errors.language ? (
                                  <div className="danger">
                                    {this.state.errors.language}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group">
                                <input
                                  className="form-control"
                                  type="text"
                                  value={this.state.code}
                                  onChange={this.handleChange('code')}
                                  placeholder="Code *"
                                />
                                {this.state.errors.code ? (
                                  <div className="danger">
                                    {this.state.errors.code}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group">
                                <input
                                  className="form-control"
                                  type="text"
                                  value={this.state.url}
                                  onChange={this.handleChange('url')}
                                 placeholder="Url *"
                                />
                                 {this.state.errors.url ? (
                                  <div className="danger">
                                    {this.state.errors.url}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group">
                              <select
                                  className="form-control"
                                  value={this.state.languagestatus}
                                  onChange={this.handleLanguageStatus}
                                >
                                  <option value="1">Enabled</option>
                                  <option value="0">Disabled</option>
                                </select>
                                {this.state.errors.languagestatus ? (
                                  <div className="danger">
                                    {this.state.errors.languagestatus}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="save-btn btn btn-dark"
                              >
                                {this.state.submit_status === true ? (
                                   <React.Fragment>
                                  <ReactSpinner 
                                  type="border"
                                    color="dark"
                                    size="1" />
                                      <span className="submitting">
                                      {this.state.edit_status === true
                                        ? "UPDATING LANGUAGE..."
                                        : "CREATING LANGUAGE..."}
                                    </span>
                                      </React.Fragment>
                                ): this.state.edit_status === true ? (
                                  "UPDATE LANGUAGE"
                                ) : (
                                  "CREATE LANGUAGE"
                                )}
                              </button>
                              <div className="tableinformation">
                                {this.state.message !== "" ? (
                                  <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                  </div>
                                ) : null}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-8">
                          <div className="card">
                            <div className="card-header">
                              <strong>Languages</strong>
                            </div>
                            <div className="card-body categories-list">
                              <React.Fragment>
                                <div
                                  style={{
                                    display:
                                      this.state.spinner === true
                                        ? "flex"
                                        : "none",
                                  }}
                                  className="overlay"
                                >
                                  <ReactSpinner
                                    type="border"
                                    color="primary"
                                    size="10"
                                  />
                                </div>
                              </React.Fragment>
                              <p className="tableinformation">
                                {this.state.table_message !== "" ? (
                                  <div
                                    className={this.state.table_message_type}
                                  >
                                    <p>{this.state.table_message}</p>
                                  </div>
                                ) : null}
                              </p>
                              <DataTable
                                columns={this.state.columns}
                                 data={datarow}
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                noDataComponent = {<p>There are currently no records.</p>}
                              />
                            </div>
                          </div>
                        </div>
                        </div>
                     </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment> );
    }
}
 
export default Language;