import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as CouponService from "../../ApiServices/admin/manageCoupon";
import DataTable from "react-data-table-component";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import MultiSelect from "react-multi-select-component";
import moment from "moment";
import * as manageProducts from "../../ApiServices/admin/products";
class AdminCoupons extends Component {
  state = {
    id: "",
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    table_message: "",
    table_message_type: "",
    addresses: [],
    delete_status: false,
    edit_status: false,
    data_table: [],
    spinner: true,
    couponcode: "",
    amount: "",
    discounttype: "",
    exiprydate: "", //moment(Date.now()).toDate(),
    products: [],
    products_selected: [],
    products_value: [],
    users: [],
    users_value: [],
    users_selected: [],
    guestusers: "",
    couponstatus: "1",

    columns: [
      {
        name: "Code",
        selector: "couponcode",
        sortable: true,
      },
      {
        name: "Date",
        selector: "exiprydate",
        cell: (row) => <p>{moment(row.exiprydate).format("DD-MM-YYYY")}</p>,
        sortable: true,
      },
      {
        name: "Amount",
        selector: "amount",

        sortable: true,
      },
      {
        name: "Type",
        selector: "discounttype",
        cell: (row) => (
          <p>
            {(() => {
              if (row.discounttype == 1) {
                return <p>Fixed discount</p>;
              } else {
                return <p>Percentage discount</p>;
              }
            })()}
          </p>
        ),
        sortable: true,
      },
      {
        name: "Status",
        selector: "couponstatus",
        cell: (row) => <p>{row.couponstatus == 1 ? "Enabled" : "Disabled"}</p>,
        sortable: true,
      },
      {
        name: "Action",
        cell: (row) => (
          <div className="action_dropdown" data-id={row._id}>
            <i
              data-id={row._id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            ></i>
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >
              <a
                onClick={() => this.getContactById(row._id)}
                className="dropdown-item"
              >
                Edit
              </a>
              <a
                onClick={() => this.handleRemoveContact(row._id)}
                className="dropdown-item"
              >
                Delete
              </a>
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px", // custom width for icon button
      },
    ],
  };

  componentDidMount = () => {
    this.getAllCoupons();
    this.getAllProductsIdsandNames();
    this.getAllUsersIdsandNames();
  };
  getAllCoupons = async () => {
    this.setState({ spinner: true });
    try {
      const response = await CouponService.getAllCoupons();
      if (response.data.status == 1) {
        this.setState({ addresses: response.data.data });
        const Setdata = { ...this.state.data_table };
        const data_table_row = [];
        response.data.data.map((add, index) => {
          const Setdata = {};
          Setdata.couponcode = add.couponcode;
          Setdata.couponstatus = add.couponstatus;
          Setdata.discounttype = add.discounttype;
          Setdata.exiprydate = add.exiprydate;
          Setdata.amount = add.amount;
          Setdata._id = add._id;
          data_table_row.push(Setdata);
        });      

        this.setState({
          data_table: data_table_row,
        });
        
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  schema = {
    couponcode: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field.",
        };
      }),
    discounttype: Joi.number()
      .positive()
      .integer()
      
      .error(() => {
        return {
          message: "This field is a required field.",
        };
      }),
    amount: Joi.when('discounttype', {
      is: Joi.valid(1),
      then: Joi.number().positive()
      .required()      
      .error(() => {
        return {
          message: "Please enter valid amount",
        };
      }),
      otherwise: Joi.number().positive()      
      .required()
      .max(100)
      .error(() => {
        return {
          message: "Please enter valid amount",
        };
      }),
    }),
    
    exiprydate: Joi.date()
      .required()
      .error(() => {
        return {
          message: "This field is a required field.",
        };
      }),
    couponstatus: Joi.string(),
    id: Joi.string().allow(""),
  };
  validateProperty = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };
  handleChange = (input) => (event) => {
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty(input, event.target.value);
    if (errorMessage) errors[input] = errorMessage;
    else delete errors[input];
    let val = event.target.value;
    if(input == "amount") {
      val = +parseFloat(val).toFixed(2)
    }
    this.setState({ [input]: val, errors });
  };

  getContactById = async (id) => {
    const response = await CouponService.getCouponById({ id: id });
    if (response.data.status == 1) {
      this.setState({
        edit_status: true,
        submit_status: false,
        message: "",
        message_type: "",
        delete_status: false,
        couponcode: response.data.data.couponcode,
        amount: response.data.data.amount.toString(),
        discounttype: response.data.data.discounttype,
        exiprydate: moment(response.data.data.exiprydate).toDate(),
        products: response.data.data.products,
        users: response.data.data.users,
        couponstatus: response.data.data.couponstatus,
        id: response.data.data._id,
      });
      this.getAllProductsIdsandNames();
      this.getAllUsersIdsandNames();
    }
  };

  addCoupon = () => {
    this.setState({
      couponcode: "",
      amount: "",
      discounttype: "",
      exiprydate: "",
      products: [],
      products_selected: [],
      user: "",
      couponstatus: "1",
      attributes: [],
      errors: {},
      submit_status: false,
      message: "",
      message_type: "",
      delete_status: false,
      edit_status: false,
      descrption: "",
    });
  };

  saveCoupon = async (e) => {
    e.preventDefault();
    const checkState = {
      couponcode: this.state.couponcode,
      amount: this.state.amount,
      discounttype: this.state.discounttype,
      exiprydate: this.state.exiprydate,
      couponstatus: this.state.couponstatus,
    };
    const errors = { ...this.state.errors };    
    this.setState({ errors: {} });  
    let result = Joi.validate(checkState, this.schema);    
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;      
      errors[path] = errormessage;
      this.setState({ errors });
    } else {
      const category_data = {
        couponcode: this.state.couponcode,
        amount: this.state.amount,
        discounttype: this.state.discounttype,
        exiprydate: this.state.exiprydate,
        products: this.state.products,
        users: this.state.users,
        couponstatus: this.state.couponstatus,
        id: this.state.id,
      };
      try {
        this.setState({
          submit_status: true,
          message: "",
          message_type: "",
        });
        if (this.state.edit_status === true) {         
          const response = await CouponService.updateCoupon(category_data);
          if (response) {
            this.setState({ submit_status: false });
            if (response.data.status === 1) {
              this.setState({
                message: response.data.message,
                message_type: "success",
                couponcode: "",
                amount: "",
                discounttype: "-1",
                exiprydate: "",
                products: [],
                products_selected: [],
                products_value: [],
                users: [],
                users_selected: [],
                users_value: [],
                couponstatus: "1",
                id: "",
                edit_status: false,
              });
              this.getAllCoupons();
            } else {
              this.setState({
                message: response.data.message,
                message_type: "error",
              });
            }
          } else {
            this.setState({ submit_status: false });
            this.setState({
              message: "Something went wrong! Please try after some time",
              message_type: "error",
            });
          }
          setTimeout(() => {
            this.setState(() => ({ message: "" }));
          }, 5000);
        } else {
          const response = await CouponService.createCoupon(category_data);
          if (response) {
            this.setState({ submit_status: false });
            if (response.data.status === 1) {
              this.setState({
                message: response.data.message,
                message_type: "success",
                couponcode: "",
                amount: "",
                discounttype: "-1",
                exiprydate: "",
                products: [],
                products_selected: [],
                products_value: [],
                users: [],
                users_selected: [],
                users_value: [],
                couponstatus: "1",
              });
              this.getAllCoupons();
            } else {
              this.setState({
                message: response.data.message,
                message_type: "error",
              });
            }
          } else {
            this.setState({ submit_status: false });
            this.setState({
              message: "Something went wrong! Please try after some time",
              message_type: "error",
            });
          }
          setTimeout(() => {
            this.setState(() => ({ message: "" }));
          }, 5000);
        }
      } catch (err) {}
    }
  };

  handleRemoveContact = async (id) => {
    this.setState({
      delete_status: true,
    });
    const response = await CouponService.removeCoupon({ _id: id });
    if (response.data.status === 1) {
      let newContacts = [...this.state.data_table];
      newContacts = newContacts.filter(function (obj) {
        return obj._id !== id;
      });
      this.setState({
        table_message: response.data.message,
        table_message_type: "sucess",
      });

      this.setState({
        data_table: newContacts,
        delete_status: false,
        edit_status: false,
      });
      setTimeout(() => {
        this.setState(() => ({ table_message: "" }));
      }, 5000);
    } else {
      this.setState({
        table_message: response.data.message,
        table_message_type: "sucess",
      });
      setTimeout(() => {
        this.setState(() => ({ table_message: "" }));
      }, 5000);
    }
  };

  handledateChange = (date) => {
    this.setState({
      exiprydate: date,
    });
    if(date) {
      const errors = { ...this.state.errors };
      delete errors.validate;
      const errorMessage = this.validateProperty('exiprydate', date);
      if (errorMessage) errors['exiprydate'] = errorMessage;
      else delete errors['exiprydate'];
      this.setState({ 'errors': errors });
    }
  };

  handlediscountChange = async (e) => {
    this.setState({ discounttype: e.target.value });
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty('discounttype', e.target.value);
    if (errorMessage) errors['discounttype'] = errorMessage;
    else delete errors['discounttype'];
    this.setState({ 'errors': errors });
  };

  handlecouponChange = async (e) => {
    this.setState({ couponstatus: e.target.value });
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty('couponstatus', e.target.value);
    if (errorMessage) errors['couponstatus'] = errorMessage;
    else delete errors['couponstatus'];
    this.setState({ 'errors': errors });
  };

  getAllProductsIdsandNames = async () => {
    const response = await manageProducts.getAllProductsIdsandNames();
    if (response.data.status == 1) {
      const Setdata = { ...this.state.products };
      const data_related_row = [];
      const data_selected_row = [];
      response.data.data.map((Products, index) => {
        const Setdata = {};
        Setdata.value = Products._id;
        Setdata.label = Products.product_name;
        if (this.state.products.indexOf(Products._id) > -1) {
          data_selected_row.push(Setdata);
        }
        data_related_row.push(Setdata);
      });
      this.setState({ products_value: data_related_row });
      this.setState({ products_selected: data_selected_row });     
    }
  };

  getAllUsersIdsandNames = async () => {
    const response = await CouponService.getAllUsersIdsandNames();
    if (response.data.status == 1) {
      const Setdata = { ...this.state.users };
      const data_related_row = [];
      const data_selected_row = [];
      response.data.data.map((users, index) => {
        const Setdata = {};
        Setdata.value = users._id;
        Setdata.label = users.email;
        if (this.state.users.indexOf(users._id) > -1) {
          data_selected_row.push(Setdata);
        }
        data_related_row.push(Setdata);
      });
      this.setState({ users_value: data_related_row });
      this.setState({ users_selected: data_selected_row });
    }
  };
  userSelected = (event) => {
    const data_selected_row = [];
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value;
    });   
    this.setState({ users: data_selected_row });
    this.setState({ users_selected: event });
  };

  
  setSelected = (event) => {
    const data_selected_row = [];
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value;
    });
    this.setState({ products: data_selected_row });
    this.setState({ products_selected: event });
  };

  render() {
    let datarow = this.state.data_table;
    const userPlaceholder = {
      selectSomeItems: "Users",
      allItemsAreSelected: "All items are selected.",
      selectAll: "Select All",
    };
    const productPlaceholder = {
      selectSomeItems: "Products",
      allItemsAreSelected: "All items are selected.",
      selectAll: "Select All",
      search: "Search",
    };
    console.log('users_value' ,this.state.users_value);
    console.log('users users_selected' , this.state.users , this.state.users_selected);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4">
                  <React.Fragment>
                    <div className="categories-wrap p-4">
                      <div className="row">
                        <div className="col-md-4">
                          <div className="card">
                            <div className="card-header">
                              <strong>
                                {this.state.edit_status === true
                                  ? "Edit"
                                  : "Add"}{" "}
                                Coupons
                              </strong>
                              {this.state.edit_status === true ? (
                                <button
                                  onClick={this.addCoupon}
                                  className="ml-4 add-cat-btn btn btn-success"
                                >
                                  + Add New
                                </button>
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="card-body coupon-add categories-add">
                        

                              <div className="form-group">
                                <input
                                  className="form-control"
                                  placeholder="Coupon Code*"
                                  type="text"
                                  value={this.state.couponcode}
                                  onChange={this.handleChange("couponcode")}
                                />
                                {this.state.errors.couponcode ? (
                                  <div className="danger">
                                    {this.state.errors.couponcode}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group">
                                <select
                                  name="discounttype"
                                  value={this.state.discounttype}
                                  onChange={this.handlediscountChange}
                                >
                                  <option value="-1">
                                    Select Discount Type *
                                  </option>
                                  <option value="1">Fixed discount</option>
                                  <option value="2">Percentage discount</option>
                                </select>

                                {this.state.errors.discounttype ? (
                                  <div className="danger">
                                    {this.state.errors.discounttype}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="form-group">                                
                                <input
                                  onChange={this.handleChange("amount")}
                                  placeholder="Amount *"
                                  value={this.state.amount}
                                  className="form-control"
                                  type="number"
                                ></input>
                                {this.state.errors.amount ? (
                                  <div className="danger">
                                    {this.state.errors.amount}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>                    
                              <div className="form-group">
                                {}
                                <DatePicker
                                  selected={this.state.exiprydate}
                                  placeholderText="Exipry Date *"
                                  minDate={new Date()}                                  
                                  dateFormat="dd-MM-yyyy"                                  
                                  onChange={this.handledateChange}
                                />
                                {this.state.errors.exiprydate ? (
                                  <div className="danger">
                                    {this.state.errors.exiprydate}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group">
                                <MultiSelect
                                  options={this.state.products_value}
                                  value={this.state.products_selected}
                                  onChange={this.setSelected}
                                  disableSearch
                                  overrideStrings={productPlaceholder}
                                />
                                {this.state.errors.products ? (
                                  <div className="danger">
                                    {this.state.errors.products}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group">
                                <MultiSelect
                                  options={this.state.users_value}
                                  value={this.state.users_selected}
                                  onChange={this.userSelected}
                                  overrideStrings={userPlaceholder} //  labelledBy={"Select"}
                                />
                                {this.state.errors.user ? (
                                  <div className="danger">
                                    {this.state.errors.user}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="form-group">
                                <select
                                  value={this.state.couponstatus}
                                  onChange={this.handlecouponChange}
                                >
                                  <option value="1">Enabled</option>
                                  <option value="0">Disabled</option>
                                </select>
                                {this.state.errors.couponstatus ? (
                                  <div className="danger">
                                    {this.state.errors.couponstatus}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.saveCoupon}
                                className="save-btn btn btn-dark"
                              >
                                {this.state.submit_status === true ? (
                                  <React.Fragment>
                                    <ReactSpinner
                                      type="border"
                                      color="primary"
                                      size="1"
                                    />
                                    <span className="submitting">
                                      {this.state.edit_status === true
                                        ? "UPDATING COUPON..."
                                        : "CREATING COUPON..."}
                                    </span>
                                  </React.Fragment>
                                ) : this.state.edit_status === true ? (
                                  "UPDATE COUPON"
                                ) : (
                                  "CREATE COUPON"
                                )}
                              </button>
                              {this.state.message !== "" ? (
                                <div className={this.state.message_type}>
                                  <p>{this.state.message}</p>
                                </div>
                              ) : null}
                            </div>
                          </div>
                        </div>
                        <div className="col-md-8">
                          <div className="card">
                            <div className="card-header">
                              <strong>Coupons</strong>
                            </div>
                            <div className="card-body categories-list">
                              <React.Fragment>
                                <div
                                  style={{
                                    display:
                                      this.state.spinner === true
                                        ? "flex"
                                        : "none",
                                  }}
                                  className="overlay"
                                >
                                  <ReactSpinner
                                    type="border"
                                    color="primary"
                                    size="10"
                                  />
                                </div>
                              </React.Fragment>
                              <p className="tableinformation">
                                {this.state.table_message !== "" ? (
                                  <div
                                    className={this.state.table_message_type}
                                  >
                                    <p>{this.state.table_message}</p>
                                  </div>
                                ) : null}
                              </p>
                              <DataTable
                                columns={this.state.columns}
                                data={datarow}
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                noDataComponent = {<p>There are currently no records.</p>}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminCoupons;
