import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import * as settingsService from "../../ApiServices/admin/manageFaq";
import * as manageUser from "../../ApiServices/admin/manageUser";
import CKEditor from "ckeditor4-react";
import { apiUrl ,siteUrl} from "../../../src/config.json";
import MultiSelect from "react-multi-select-component";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import BlockUi from 'react-block-ui';
class AdminEditFaq extends Component {
  constructor(props) {
    super(props);
    this.onEditorChange = this.onEditorChange.bind(this);
  }
  state = {
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    validated: false,
    focused: false,
    related_cat: [],
    related_cat_options: [],
    related_cat_selected: [],
    cimonFaq: [],
    FaqStatus: true,
    spinner:false
  };

  onEditorChange(evt) {
    const cimonFaq = { ...this.state.cimonFaq };
    cimonFaq["description"] = evt.editor.getData();
    this.setState({
      cimonFaq: cimonFaq
    });
  }

  /*Joi validation schema*/
  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    description: Joi.allow(null),
    _id: Joi.optional().label("id"),
    slug: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      })
  };

  /* Input Handle Change */
  handleChange = (event, type = null) => {
    let cimonFaq = { ...this.state.cimonFaq };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateCasestudydata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    cimonFaq[name] = value;
    this.setState({ cimonFaq, errors });
  };
  validateCasestudydata = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  setcatSelected = event => {
    const data_selected_row = [];
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value;
    });
    this.setState({ related_cat: data_selected_row });
    this.setState({ related_cat_selected: event });
  };

  getCategoryParent = async () => {
    const response = await settingsService.getCategoryParent();
    if (response.data.status == 1) {
      const Setdata = { ...this.state.related_cat };
      const data_related_row = [];
      const data_selected_row = [];
      response.data.data.map((Cats, index) => {
        const Setdata = {};
        Setdata.value = Cats._id;
        Setdata.label = Cats.category_name;

        if (this.state.related_cat) {
          if (this.state.related_cat.indexOf(Cats._id) > -1) {
            data_selected_row.push(Setdata);
          }
        }
        data_related_row.push(Setdata);
      });

      this.setState({ related_cat_options: data_related_row });
      this.setState({ related_cat_selected: data_selected_row });
      console.log(this.state.related_cat_options);
    }
  };

  /* Get single Casestudy */
  componentDidMount = async () => {
    this.setState({ spinner: true });
    const id = this.props.match.params.id;
    const response = await settingsService.getSingleFaq(id);
    if (response.data.status == 1) {
      let newCasestudyarray = {
        _id: response.data.data.faq_data._id,
        title: response.data.data.faq_data.title,
        description: response.data.data.faq_data.description,
        slug: response.data.data.faq_data.slug
      };
      this.setState({
        related_cat: response.data.data.faq_data.categories
      });

      setTimeout(() => {
        this.setState({ cimonFaq: newCasestudyarray });
        this.setState({ spinner: false });
      }, 2000);

      this.getCategoryParent();
    } else {
      this.setState({
        FaqStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
      this.setState({ spinner: false });
    }
  };

  /* Form Submit */
  handleSubmit = async () => {
    const cimonFaq = { ...this.state.cimonFaq };
    const errors = { ...this.state.errors };
    let result = Joi.validate(cimonFaq, this.schema);
    console.log(result.error);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });

        this.updateCasestudyData();
      }
    }
  };

  /* Update fAQ */
  updateCasestudyData = async () => {
    const cimonFaq = { ...this.state.cimonFaq };
    cimonFaq["categories"] = this.state.related_cat;
    this.setState({ cimonFaq });
    const response = await settingsService.updateFaq(cimonFaq);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents addpage-form-wrap Casestudy-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                  <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className="row addpage-form-wrap">
                      <div className="col-lg-8 col-md-12">
                        <div className="form-group">
                          <label htmlFor="">Update Faq</label>
                          <input
                            name="title"
                            value={this.state.cimonFaq.title}
                            onChange={e => this.handleChange(e)}
                            type="text"
                            placeholder="Add title *"
                            className="form-control"
                          />
                          {this.state.errors.title ? (
                            <div className="error text-danger">
                              {this.state.errors.title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Slug</label>
                          <input
                            name="slug"
                            onChange={this.handleChange}
                            value={this.state.cimonFaq.slug}
                            type="text"
                            placeholder="slug *"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-12">
                              <label htmlFor="">Content</label>
                              <CKEditor
                                data={this.state.cimonFaq.description}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorChange}
                              />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-4 col-md-12 ">
                        <div className="form-group form-group-inline checkbox-div">
                          <label htmlFor="">Select Category</label>
                          <MultiSelect
                            options={this.state.related_cat_options}
                            value={this.state.related_cat_selected}
                            onChange={this.setcatSelected} //  labelledBy={"Select"}
                          />
                        </div>

                        <div className="faq-sideinputs">
                          <div className="faq-btns form-btn-wrap">
                            <div className="update_btn input-group-btn float-right">
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="btn btn-info"
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                                Update
                              </button>
                            </div>
                          </div>
                          {this.state.message !== "" ? (
                            <div className="tableinformation">
                              <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                              </div>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </BlockUi>

                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminEditFaq;
