import React, { Component } from 'react';
import * as managePageService from "../../../ApiServices/admin/managePages";
import Adminheader from "../../common/adminHeader";
import Adminsidebar from "../../common/adminSidebar";
import BlockUi from "react-block-ui";
import AboutEditPage from './aboutEditPage';
import CareerCenterEditPage from './careerCenterEditPage';
import CareerEditPage from './careerEditPage';
import ContactEditPage from './contactEditPage';
import DistributorEditPage from './distributorEditPage';
import DistributorLocatorEditPage from './distributorLocatorEditPage';
import FooterEditPage from './footerEditPage';
import HmiEditPage from './hmiEditPage';
import HomeEditPage from './homeEditPage';
import HybridHmiEditPage from './hybridhmiEditPage';
import IntegratorPortalEditPage from './integratorPortalEditPage';
import IpcEditPage from './ipcEditPage';
import NewsEditPage from './newsEditPage';
import PlcEditPage from './plcEditPage';
import ScadaEditPage from './scadaEditPage';
import ScadabundleEditPage from './scadabundlesEditPage';
import CimonCiconEditPage from './cimonCiconEditPage';
import IndustryEditPage from './industryEditPage';

import CaseStudyEditPage from './caseStudyEditPage';
import LiveTrainingEditPage  from './liveTrainingEditPage';
import VideoTutorialEditPage  from './videoTutorialEditPage';
import FaqEditPage  from './faqEditPage';

import ProductSplash from './productSplash';
 import InternationalDistributorEditPage from './internationalDistributorEditPage';
 import TechSupportEditPage from './techSupportEditPage';
 import OurCompanyEditPage from './ourCompanyEditPage';
 import OurCustomersEditPage from './ourCustomersEditPage';
 import SIProgramEditPage from './siProgramEditPage';
import DistributorOverseaEdit from './distributorOverseaEdit'
import EditNewPage from './editNewPage' 
import CertificatesEdit from './certificatesEditPage';
import XpanelDesigner from './xpanelDesigner';
import CommonBanners  from './commonBanners'
import { Link } from 'react-router-dom';


class AdminEditPage extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            blocking:true,
            template_name: '',
            slug: '',
            page: {},
            id:''
        };
    }

    getPageContent = async () => {
        const slug = this.props.match.params.slug;
        this.setState({
            id: slug,
            blocking:true
        });
        try {
            this.setState({ spinner: true });
            const response = await managePageService.getSinglePageById(slug);
            if (response.data.status === 1) { 
       
                this.setState({
                    template_name: response.data.data.page_data.template_name,  
                    page: response.data.data.page_data, 
                    slug: response.data.data.page_data.slug,
                    blocking:false                 
                })
            } else {
                this.setState({
                    template_name: '',  
                    page: {}, 
                    blocking:false                 
                })
            }
        } catch (err) {
            this.setState({
                template_name: '',   
                page: {},
                blocking:false                 
            })
        }        
    }

    componentDidMount = async () => {
        this.getPageContent();
    }

    renderEditPage() {

        const {template_name, page, slug, id} = this.state;
        console.log(template_name);
        switch(template_name) {
            case "about-us" : 
                return(
                    <AboutEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case 'career-center':
                return(
                    <CareerCenterEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case "home" : 
                return(
                    <HomeEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case 'scada':
                return(
                    <ScadaEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
                case 'cimon-cicon':
                return(
                    <CimonCiconEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
                case 'scadabundle':
                    return(
                        <ScadabundleEditPage
                        page = {page}
                        template_name = {template_name}
                        slug = {slug}
                        id={id}
                        {...this.props}
                        />
                    );
                    break;
            case "ipc" : 
                return(
                    <IpcEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case 'plc':
                return(
                    <PlcEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case "hmi" : 
                return(
                    <HmiEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case 'hybrid-hmi':
                return(
                    <HybridHmiEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case "careers" : 
                return(
                    <CareerEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case 'news':
                return(
                    <NewsEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case 'industry':
                    return(
                        <IndustryEditPage
                        page = {page}
                        template_name = {template_name}
                        slug = {slug}
                        id={id}
                        {...this.props}
                        />
                    );
                    break;


            case "contact" : 
                return(
                    <ContactEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case "download" : 
                return(
                    <ScadaEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
            break;
            case 'footer':
                return(
                    <FooterEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case "distributor-portal" : 
                return(
                    <DistributorEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case 'distributor-locator':
                return(
                    <DistributorLocatorEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
            case "integrator-portal" : 
                return(
                    <IntegratorPortalEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case "techsupport" : 
                return(
                    <TechSupportEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case "our-company" : 
                return(
                    <OurCompanyEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case "our-customers" : 
                return(
                    <OurCustomersEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case "si-program" : 
                return(
                    <SIProgramEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case "case-study" : 
                return(
                    <CaseStudyEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                
                case "live-training" : 
                return(
                    <LiveTrainingEditPage 
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case "video-tutorials" : 
                return(
                    <VideoTutorialEditPage 
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case "faq" : 
                return(
                    <FaqEditPage 
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;



                case "products-splash" : 
                return(
                  
                    <ProductSplash
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case "international-distributor" : 
                return(
                    
                   <InternationalDistributorEditPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;

                case "distributor-oversea" : 
                return(
                    
                   <DistributorOverseaEdit
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
                case "dynamic" : 
                return(
                   <EditNewPage
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
                

                case "certificates" : 
                return(
                    
                   <CertificatesEdit
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;


                case "xpaneldesigner" : 
                return(
                    
                   <XpanelDesigner
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;


                case "common-banners" : 
                return(
                    
                   <CommonBanners
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
                
                  
            default:
                

        }

    }
    render() {

        const {blocking, template_name} = this.state;
        return (
        <React.Fragment>
            <BlockUi tag="div" blocking={blocking} >
            {blocking == false && template_name == '' ?
            <div className="container-fluid admin-body">
                <div className="admin-row">
                    <div className="col-md-2 col-sm-12 sidebar">
                    <Adminsidebar props={this.props} />
                    </div>
        
                    <div className="col-md-10 col-sm-12  content">
                        <div className="row content-row">
                            <div className="col-md-12  header">
                            <Adminheader props={this.props} />
                            </div>
                            <div className="col-md-12  contents backgroundoverlay home-inner-content pt-4 pb-4 pr-4">
                                <div class="row addpage-form-wrap">
                                    <div class="addpage-form">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label for="">Edit Page</label>
                                                <div>
                                                    Sorry! no page details found.
                                                    <Link to="/admin/pages"> Back to pages</Link>
                                                </div>
                                            </div>
                                        </div>                                            
                                    </div>
                                </div>
                            </div>                    
                    </div>
                    </div>
                </div>
                
            </div>
            :
            this.renderEditPage()
            }
            </BlockUi>         
        </React.Fragment>
        );
    }
}

export default AdminEditPage;