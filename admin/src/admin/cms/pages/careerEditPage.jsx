import React, { Component } from 'react';
import Adminheader from "../../common/adminHeader";
import Adminsidebar from "../../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as managePageService from "../../../ApiServices/admin/managePages";

import CKEditor from 'ckeditor4-react';
import { apiUrl , siteUrl, bootstrap } from "../../../../src/config.json";
import UploadThumb from "../../common/uploadThumbimage";
import MultipleUploadImages from "../../common/multipleuploadimages";
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import * as manageUser from "../../../ApiServices/admin/manageUser";
class CareerEditPage extends Component {

    constructor(props) {
        super(props);  
        this.onEditorChange = this.onEditorChange.bind( this );
        this.onEditorBannercontent = this.onEditorBannercontent.bind( this );
        this.onEditorSection1 = this.onEditorSection1.bind( this );
        this.onEditorSection2 = this.onEditorSection2.bind( this );
        this.onEditorSection3 = this.onEditorSection3.bind( this );
        this.onEditorSection4 = this.onEditorSection4.bind( this );
        this.onEditorSection5 = this.onEditorSection5.bind( this );
        this.onEditorSection6 = this.onEditorSection6.bind( this );
        this.onEditorSection7 = this.onEditorSection7.bind( this );
        this.onEditorSection8 = this.onEditorSection8.bind( this );

    }

    state = {
        cimonPages: [],
        uploadedImages:[],
        parentPages: [
          { name: 'One', id: 1 },
          { name: 'Two', id: 2 },
          { name: 'Three', id: 3 },
          { name: 'four', id: 4 }
        ],
        errors: {},
        validated: false,
        pageStatus: true,
        submitStatus: false,
        spinner : true,
        profile_image : '',
        profile_image2: '',
        profile_image3 : '',
        profile_image4 : '',
        message: "",
        message1:"",
        responsetype: "",
        editorState:"",
        bannerContent : '',
        section1 : '',
        section2 : '',
        section3 : '',
        section4 : '',
        section5 : '',
        section6 : '',
        section7 : '',
        section8 : '',
        bannerSlider: [
            {
                heading : '',
                subheading : '',
                image :'',
            }
        ],
        upload_image_status: [],
        commonSilder: [],
        common_image_status : [],
        is_preview:"",
        is_delete:"",
      };


    onEditorChange( evt ) {
		this.setState( {
			editorState: evt.editor.getData()
		} );
	}

    onEditorBannercontent( evt ) {
		this.setState( {
			bannerContent : evt.editor.getData()
		} );
	}
    onEditorSection1( evt ) {
		this.setState( {
			section1 : evt.editor.getData()
		} );
	}
    
    onEditorSection2( evt ) {
		this.setState( {
			section2 : evt.editor.getData()
		} );
    }
    onEditorSection3( evt ) {
		this.setState( {
			section3 : evt.editor.getData()
		} );
    }
    onEditorSection4( evt ) {
		this.setState( {
			section4 : evt.editor.getData()
		} );
    }
    onEditorSection5( evt ) {
		this.setState( {
			section5 : evt.editor.getData()
		} );
    }
    onEditorSection6( evt ) {
		this.setState( {
			section6 : evt.editor.getData()
		} );
    }
    onEditorSection7( evt ) {
		this.setState( {
			section7 : evt.editor.getData()
		} );
    }
    onEditorSection8( evt ) {
		this.setState( {
			section8 : evt.editor.getData()
		} );
	}



    /*****************Joi validation schema**************/  
    schema = {
        title: Joi.string()
        .required()
        .error(() => {
        return {
            message: "This field is Required",
        };
        }),
        page_title: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        meta_tag: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        meta_desc: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        _id: Joi.optional().label("id"),
        slug : Joi.string(),
        editor_content: Joi.string()

    };
    /*****************end Joi validation schema**************/

    /*****************INPUT HANDLE CHANGE **************/

    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const cimonPages = { ...this.state.cimonPages };
        const errorMessage = this.validateProperty(input);
        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        cimonPages[input.name] = input.value;
        this.setState({ cimonPages, errors });
        
    };

    /*****************JOI VALIDATION CALL**************/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };
    

    // /***************** FORM SUBMIT **************/

    

    componentDidMount = async () => {

        const slug = this.props.slug;
        const id = this.props.id;
    
        this.setState({
            spinner: true,
            slug: slug,
            id:id
        });
        
        
        try{


        const response = await managePageService.getSinglePageById(id);
       
        if(response.data.status === 1){
            let newPagearray = {
                slug: response.data.data.page_data.slug,
                // slug: this.state.slug,
                title: response.data.data.page_data.title,
                page_title: response.data.data.page_data.page_title,
                meta_tag: response.data.data.page_data.meta_tag,
                meta_desc: response.data.data.page_data.meta_desc,
            };
            this.setState({ cimonPages : newPagearray });
            this.setState({ is_preview: response.data.data.page_data.is_preview });
            
            this.setState({ is_delete: response.data.data.page_data.is_delete });
           
            if (response.data.data.page_data.hasOwnProperty('content') ) {


                setTimeout(() => {
                    this.setState({
                        
                        
                        bannerContent: response.data.data.page_data.content.bannerContent ? response.data.data.page_data.content.bannerContent : '',
                        commonSilder : response.data.data.page_data.content.commonSilder ? response.data.data.page_data.content.commonSilder : [],
                        bannerSlider: response.data.data.page_data.content.bannerSlider ? response.data.data.page_data.content.bannerSlider : this.state.bannerSlider,

                        section1: response.data.data.page_data.content.section1 ? response.data.data.page_data.content.section1 : '',
                        section2: response.data.data.page_data.content.section2 ? response.data.data.page_data.content.section2 : '',
                        section3: response.data.data.page_data.content.section3 ? response.data.data.page_data.content.section3 : '',
                        section4: response.data.data.page_data.content.section4 ? response.data.data.page_data.content.section4 : '',
                        section5: response.data.data.page_data.content.section5 ? response.data.data.page_data.content.section5 : '',
                        section6: response.data.data.page_data.content.section6 ? response.data.data.page_data.content.section6 : '',
                        section7: response.data.data.page_data.content.section7 ? response.data.data.page_data.content.section7 : '',
                        section8: response.data.data.page_data.content.section8 ? response.data.data.page_data.content.section8 : '',
                        profile_image :response.data.data.page_data.content.profile_image ? response.data.data.page_data.content.profile_image : '',
                        profile_image2: response.data.data.page_data.content.profile_image2 ? response.data.data.page_data.content.profile_image2 : '',
                        profile_image3 : response.data.data.page_data.content.profile_image3 ? response.data.data.page_data.content.profile_image3 : '',
                        profile_image4 : response.data.data.page_data.content.profile_image4 ? response.data.data.page_data.content.profile_image4 : '',
                       
                        spinner: false
                    });

                    let status_array = [];
                    this.state.bannerSlider.map((attribute, sidx) => {
                        
                        status_array.push(false);
                    });

                    let status_image_array = [];
                    this.state.commonSilder.map((attribute, sidx) => {
                        status_image_array.push(false);
                    });


                    this.setState({
                        upload_image_status: status_array,
                        common_image_status: status_image_array
                    });


                }, 2500);


            }
            this.setState({ spinner: false });

        }else{
            this.setState({ spinner: false });
            this.setState({
                pageStatus: false,
                message: response.data.message,
                responsetype: "error",
            });
        }


    } catch (err) {
        this.setState({
            message: 'something went wrong',
            responsetype: "error",
            spinner: false
        });
    }

        
    }


    handleSubmit = async () => {
        const cimonPages = { ...this.state.cimonPages };
        const errors = { ...this.state.errors };
      //  cimonPages["editor_content"] = this.state.editorState;

        let result = Joi.validate(cimonPages, this.schema);

        if (result.error) {
            
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({
                errors: errors  
            })
        } else {
            
            if (errors.length > 0) {
                this.setState({
                    errors: errors  
                })
            }else{
                this.setState({ spinner: true });

                const page_data = {
                    id: this.state.id,
                    title: this.state.cimonPages.title,
                    slug :this.state.cimonPages.slug,
                    page_title: this.state.cimonPages.page_title,
                    meta_tag: this.state.cimonPages.meta_tag,
                    meta_desc: this.state.cimonPages.meta_desc,
                   // attributes: SetAttributes,
                     content : {
                    bannerContent: this.state.bannerContent,
                    bannerSlider: this.state.bannerSlider.filter((s, sidx) => s.heading !== ''),
                    commonSilder : this.state.commonSilder.filter((s, sidx) => s.image !== ''),
                    section1 : this.state.section1,
                    section2 : this.state.section2, 
                    section3 : this.state.section3,
                    section4 : this.state.section4,
                    section5 : this.state.section5,
                    section6 : this.state.section6,
                    section7 : this.state.section7,
                    section8 : this.state.section8,
                    profile_image :this.state.profile_image,
                    profile_image2: this.state.profile_image2,
                    profile_image3 : this.state.profile_image3,
                    profile_image4 : this.state.profile_image4,
                   }
                   
                  }

try{
                
                this.setState({ submit_status: true });
                const response = await managePageService.updatePagedata(page_data);
                if (response.data.status === 1) {
                    this.setState({
                    submitStatus: false,
                    message: response.data.message,
                    responsetype: "success",
                    spinner: false 
                    });
                   
                    // setTimeout(
                    // () =>
                    //     this.props.history.push({
                    //     pathname: '/admin/pages',
                    //     }),
                    // 2000
                    // );
                } else {
                    this.setState({
                    submitStatus: false,
                    spinner: false,
                    message: response.data.message,
                    responsetype: "error",
                    });
                   
                }

            } catch (err) {
                this.setState({
                    message: 'something went wrong',
                    responsetype: "error",
                    spinner: false,
                    submit_status: false
                });
            }


            }
        }
    };

    handlePreview = async () => {
        const slug = this.props.slug;
        const id = this.props.id;
        const cimonPages = { ...this.state.cimonPages };
        const errors = { ...this.state.errors };
      //  cimonPages["editor_content"] = this.state.editorState;

        let result = Joi.validate(cimonPages, this.schema);

        if (result.error) {
            
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({
                errors: errors  
            })
        } else {
            
            if (errors.length > 0) {
                this.setState({
                    errors: errors  
                })
            }else{
                this.setState({ spinner: true });

                const page_data = {
                    slug: this.state.slug,
                   // attributes: SetAttributes,
                   preview : {
                    page_title: this.state.cimonPages.page_title,
                    title: this.state.cimonPages.title,
                    bannerContent: this.state.bannerContent,
                    bannerSlider: this.state.bannerSlider.filter((s, sidx) => s.heading !== ''),
                    commonSilder : this.state.commonSilder.filter((s, sidx) => s.image !== ''),
                    section1 : this.state.section1,
                    section2 : this.state.section2, 
                    section3 : this.state.section3,
                    section4 : this.state.section4,
                    section5 : this.state.section5,
                    section6 : this.state.section6,
                    section7 : this.state.section7,
                    section8 : this.state.section8,
                   }
                   
                  }

              try{
                
                this.setState({ submit_status: true });
                const response = await managePageService.previewPagedata(page_data);
                if (response.data.status === 1) {
                    this.setState({
                    submitStatus: false,
                    message1: response.data.message,
                    responsetype: "success",
                    spinner: false 
                    });
                   
                    setTimeout(() => {
                        const win = window.open("/cimon-gate/admin/preview/" + id, "_blank");
                        win.focus();
                        // this.props.history.push({
                        //   pathname: "/admin/preview/" + id,
                        // });
                      }, 2000);
                } else {
                    this.setState({
                    submitStatus: false,
                    spinner: false,
                    message1: response.data.message,
                    responsetype: "error",
                    });
                   
                }

            } catch (err) {
                this.setState({
                    message1: 'something went wrong',
                    responsetype: "error",
                    spinner: false,
                    submit_status: false
                });
            }


            }
        }
    };
   ;

    handleheadingChange = idx => evt => {

        const newbannerSlider = this.state.bannerSlider.map((attribute, sidx) => {
            
            if (idx !== sidx) return attribute;
            return { ...attribute, heading: evt.target.value };
        });

        this.setState({ bannerSlider: newbannerSlider });
    };

    handlesubheadingChange = idx => evt => {

        const newbannerSlider = this.state.bannerSlider.map((attribute, sidx) => {
            if (idx !== sidx) return attribute;
            return { ...attribute, subheading: evt.editor.getData() };
        });

        this.setState({ bannerSlider: newbannerSlider });
    };




    handleAddbannerSlider = () => {


        this.setState({
            bannerSlider: this.state.bannerSlider.concat([{ heading: "", subheading: "", image: '' }]),
            upload_image_status: this.state.upload_image_status.concat([false])
        });
    };

    handleRemovebannerSlider = idx => () => {
        this.setState({
            bannerSlider: this.state.bannerSlider.filter((s, sidx) => idx !== sidx),
            upload_image_status: this.state.upload_image_status.filter((s, sidx) => idx !== sidx)
        });
    };


    onMultipleupload = async (value, item, idx) => {
        let errors = { ...this.state.errors };
        try {

            if (item === "errors") {
                errors["videourl"] = value;
                this.setState({
                    errors: errors,
                });
                
            } else {

                const staus = [...this.state.upload_image_status];
                staus[idx] = true;
                this.setState({ upload_image_status: staus });


                let file = value.target.files[0];
                const response1 = await managePageService.uploadVideo(
                    file
                );
                if (response1.data.status === 1) {
                    const newbannerSlider = this.state.bannerSlider.map((attribute, sidx) => {
                        if (idx !== sidx) return attribute;
                        return { ...attribute, image: response1.data.data.file_location };
                    });

                    this.setState({ bannerSlider: newbannerSlider });
                } else {
                    
                    this.setState({
                        message: response1.data.message,
                        responsetype: "error",
                    });
                }
                //  this.setState({ download_1_status: false });
                staus[idx] = false;
                this.setState({ upload_image_status: staus });


            }

        } catch (err) {
            this.setState({
                message: 'something went wrong',
                responsetype: "error",
                download_1_status: false
            });
        }


    };


    onCommonUpload = async (value, item, idx) => {
        let errors = { ...this.state.errors };
       
        try {

            if (item === "errors") {
                errors["videourl"] = value;
                this.setState({
                    errors: errors,
                });
                
            } else {

                const staus = [...this.state.common_image_status];
                staus[idx] = true;
                this.setState({ common_image_status: staus });
                let file = value.target.files[0];
                const response1 = await managePageService.uploadVideo(
                    file
                );
                if (response1.data.status === 1) {
                    const newAttributes = this.state.commonSilder.map((silder, sidx) => {
                        if (idx !== sidx) return silder;
                        return { ...silder, image: response1.data.data.file_location };
                    });

                    this.setState({ commonSilder: newAttributes });
                } else {
                    
                    this.setState({
                        message: response1.data.message,
                        responsetype: "error",
                    });
                }
                //  this.setState({ download_1_status: false });
                staus[idx] = false;
                this.setState({ common_image_status: staus });

            }

        } catch (err) {
            this.setState({
                message: 'something went wrong',
                responsetype: "error",
                download_1_status: false
            });
        }


    };
    handleAddcommonSilder = () => {
        this.setState({
            commonSilder: this.state.commonSilder.concat([{ image: '' }]),
            common_image_status: this.state.common_image_status.concat([false])
        });
    };

    handleRemovecommonSilder = idx => () => {
        this.setState({
            commonSilder: this.state.commonSilder.filter((s, sidx) => idx !== sidx),
            common_image_status: this.state.common_image_status.filter((s, sidx) => idx !== sidx)
        });
    };

    uploadImage = async ( idx , file)  => () => {
        console.log('uploadImage');
    
    }

      // // /* Upload Image */
      onUploadThumbnail = async (value, item) => {

        let file = value.target.files[0];
    console.log('onUploadThumbnail',value , file);
    let errors = { ...this.state.errors };
  
            if(item == 'idx'){
                let upload_data = { ...this.state.upload_data };
                
                upload_data = file;
                this.setState({ upload_data: upload_data });
                
            } else if (item == 'idx2'){
         
            let upload_data2 = { ...this.state.upload_data2 };
             
            upload_data2 = file;
            this.setState({ upload_data2: upload_data2 });
            
            }else if (item == 'idx3'){

                let upload_data3 = { ...this.state.upload_data2 };
                
                upload_data3 = file;
                this.setState({ upload_data3: upload_data3 });
                 
            }else if (item == 'idx4'){
                
            let upload_data4 = { ...this.state.upload_data2 };
            
            upload_data4 = file;
            this.setState({ upload_data4: upload_data4 });
            
        }
   
        const response1 = await manageUser.uploadProfile(
            file
          );
          if (response1.data.status == 1) {
    
    
            let filepath = response1.data.data.file_location;
           console.log('filepath',filepath);
            if(item == 'idx'){
                this.setState({ profile_image: filepath });
            } else if (item == 'idx2'){
                this.setState({ profile_image2: filepath });
            
            } else if (item == 'idx3'){
                this.setState({ profile_image3: filepath });
            } else if (item == 'idx4'){
                this.setState({ profile_image4: filepath });
            }
           
          }
    console.log('upload_data',this.state.profile_image , this.state.profile_image2, this.state.profile_image3,
    this.state.profile_image4);
  };

    
    render() { 

        return ( <React.Fragment>
            <div className="container-fluid admin-body">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div>
               
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents backgroundoverlay  home-inner-content pt-4 pb-4 pr-4" > 
                    <React.Fragment>
                              <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                                    <ReactSpinner type="border" color="primary" size="10" />
                              </div>
                            </React.Fragment>  
                     <div  className="row addpage-form-wrap">
                         <div  className="addpage-form">
                         {this.state.is_preview === true?(<div className="col-md-12 ">
                                            <div className="form-group1 preview_grp">
                                            <span className="input-group-btn">
                                          
                                               <button onClick={this.handlePreview} className="btn btn-info  pull-right">Preview</button>
                                               {this.state.message1 !== "" ? (
                                                <div className="tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message1}</p>
                                                </div></div>
                                            ) : null} 
                                            
                                             </span>

                                               
                                            </div>
                                           
                                          
                                            
                                        </div>):""}
                         
                           <div className="col-md-12 ">
                           

                                <div className="form-group">
                                    <label>Edit Page</label>
                                    <input name="title"  value={this.state.cimonPages.title} onChange={this.handleChange} type="text" placeholder="Add title" className="form-control"/>
                                    {this.state.errors.title ? (<div className="error text-danger">
                                            {this.state.errors.title}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                    {this.state.is_delete && <p><a href={siteUrl+"/"+this.state.cimonPages.slug}>{siteUrl+"/"+this.state.cimonPages.slug}</a></p>}
                                </div>
                            </div>
                            <div className="col-md-12 ">
                        <div className="form-group">
                          <label htmlFor="">Page Title</label>
                          <input
                            name="page_title"
                            value={this.state.cimonPages.page_title}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Page title"
                            className="form-control"
                          />
                          {this.state.errors.page_title ? (
                            <div className="error text-danger">
                              {this.state.errors.page_title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Meta tag</label>
                          <input
                            name="meta_tag"
                             value={this.state.cimonPages.meta_tag}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Meta Tag"
                            className="form-control"
                          />
                          {this.state.errors.meta_tag ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_tag}
                            </div>
                          ) : (
                            ""
                          )}
                          
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Meta Description</label>
                          <input
                            name="meta_desc"
                           value={this.state.cimonPages.meta_desc}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.meta_desc ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_desc}
                            </div>
                          ) : (
                            ""
                          )}
                          
                        </div>
                        <div className="form-group"> 
                          <label htmlFor="">Page Slug</label>
                          <input
                            name="slug"
                             value={this.state.cimonPages.slug}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Page Slug"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ""
                          )}
                          
                        </div>

                        
                      </div>

                            <div className="col-md-12 ">
                                            <div className="form-group">
                                                <label htmlFor="">Banner</label>



                                                <div className="banner-section">
                                                    {
                                                        this.state.bannerSlider ?

                                                            this.state.bannerSlider.map((attribute, idx) => (
                                                                <div className="banner-single"  key={idx}>
                                                        


                                                                    <Accordion>
                                                                        <Card>
                                                                            <Card.Header>
                                                                                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                                                                    {attribute.heading ? attribute.heading : `slide #${idx + 1}`}
                                                                                </Accordion.Toggle>


                                                                                <button className="rem-btn btn btn-danger"
                                                                        onClick={this.handleRemovebannerSlider(idx)}
                                                                        type="button"><span aria-hidden="true">×</span></button>
                                                                            </Card.Header>
                                                                            <Accordion.Collapse eventKey="0">
                                                                                <Card.Body>
                                                                                    <input
                                                                                        onChange={this.handleheadingChange(idx)}
                                                                                        type="text"
                                                                                        value={attribute.heading ? attribute.heading : ""}
                                                                                        placeholder="Heading"
                                                                                        className="form-control" />


                                                                                    <CKEditor
                                                                                        data={attribute.subheading}
                                                                                        onBeforeLoad={CKEDITOR => {
                                                                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                                                                          }
                                                                                          }
                                                                                        config={
                                                                                            {
                                                                                                extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                                                                contentsCss: bootstrap,
                                                                                                height: '6em',
                                                                                                allowedContent: true,
                                                                                                filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                                                                            }
                                                                                        }
                                                                                        onInit={
                                                                                            editor => {
                                                                                            }
                                                                                        }
                                                                                        onChange={this.handlesubheadingChange(idx)}
                                                                                    />

                                                                                    <MultipleUploadImages
                                                                                        idx={idx}
                                                                                        imagestatus={this.state.upload_image_status[idx]}
                                                                                        onMultipleupload={this.onMultipleupload}
                                                                                        value={
                                                                                            (attribute.image ? attribute.image : "")
                                                                                        }
                                                                                    // errors={this.state.errors}
                                                                                    />
                                                                                </Card.Body>
                                                                            </Accordion.Collapse>
                                                                        </Card>
                                                                    </Accordion>






                                                                </div>
                                                            )) : ''
                                                    }


<button
                                                        type="button"
                                                        onClick={this.handleAddbannerSlider}
                                                        className="add-btn btn btn-success"
                                                    > + </button>

                                                </div>



                                            </div>
                                        </div>

                            <div className="col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Section 1</label>  
                                    <CKEditor
                                        data= {this.state.section1}
                                        onBeforeLoad={CKEDITOR => {
                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                          }
                                          }
                                        config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" ,"image2" ,"tableresize","forms"],
                                            contentsCss  : bootstrap,
                                            allowedContent: true,
                                         //   colorButton_enableAutomatic : false,
                                          //  colorButton_colors : 'CF5D4E,454545,FFF,DDD,CCEAEE,66AB16',
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorSection1}
                                    />
                                </div>  
                            </div>


                            <div className="col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Section 2</label>  
                                    <CKEditor
                                        data= {this.state.section2}
                                        onBeforeLoad={CKEDITOR => {
                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                          }
                                          }
                                        config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            contentsCss  : bootstrap,
                                            allowedContent: true,
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorSection2}
                                    />
                                </div>  
                            </div>


                            <div className="col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Section 3</label>  
                                    <CKEditor
                                        data= {this.state.section3}
                                        onBeforeLoad={CKEDITOR => {
                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                          }
                                          }
                                        config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            contentsCss  : bootstrap,
                                            allowedContent: true,
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorSection3}
                                    />
                                </div>  



                                <div className="row">
                            <div className="col-md-6 ">
                            <div id="images1" className="form-group thumbanail_container">
                                    <label htmlFor="">Section 3 images</label>  
                                    <UploadThumb
                                     id="idx"
                            onUploadThumbnail={(e) => this.onUploadThumbnail(e , 'idx')}
                            value={this.state.profile_image}
                            errors={this.state.errors}
                          />
                                </div>  
                            </div>
                                     
                            <div className="col-md-6 ">
                            <div id="images2" className="form-group thumbanail_container">
                                    <label htmlFor="">Section 3 images</label>  
                                    <UploadThumb
                                    id="idx2"
                            onUploadThumbnail={(e)=> this.onUploadThumbnail(e,'idx2')}
                            value={this.state.profile_image2}
                            errors={this.state.errors}
                          />
                                </div>  
                            </div>
                        
                            </div>
                                    
                       


                            </div>


                                       


                            <div className="col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Section 4</label>  
                                    <CKEditor
                                        data= {this.state.section4}
                                        onBeforeLoad={CKEDITOR => {
                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                          }
                                          }
                                        config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            contentsCss  : bootstrap,
                                            allowedContent: true,
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorSection4}
                                    />
                                </div>  
                       
                       
                                <div className="row">
                            <div className="col-md-6 ">
                            <div id="images1" className="form-group thumbanail_container">
                                    <label htmlFor="">Section 4 images</label>  
                                    <UploadThumb
                                     id="idx3"
                            onUploadThumbnail={(e) => this.onUploadThumbnail(e , 'idx3')}
                            value={this.state.profile_image3}
                            errors={this.state.errors}
                          />
                                </div>  
                            </div>
                                     
                            <div className="col-md-6 ">
                            <div id="images2" className="form-group thumbanail_container">
                                    <label htmlFor="">Section 4 images</label>  
                                    <UploadThumb
                                    id="idx4"
                            onUploadThumbnail={(e)=> this.onUploadThumbnail(e,'idx4')}
                            value={this.state.profile_image4}
                            errors={this.state.errors}
                          />
                                </div>  
                            </div>
                        
                            </div>
                                    
                       
                            </div>


                            <div className="col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Section 5</label>  
                                    <CKEditor
                                        data= {this.state.section5}
                                        onBeforeLoad={CKEDITOR => {
                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                          }
                                          }
                                        config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            contentsCss  : bootstrap,
                                            allowedContent: true,
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorSection5}
                                    />
                                </div>  
                            </div>


                            <div className="col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Section 6</label>  
                                    <CKEditor
                                        data= {this.state.section6}
                                        onBeforeLoad={CKEDITOR => {
                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                          }
                                          }
                                        config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            contentsCss  : bootstrap,
                                            allowedContent: true,
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorSection6}
                                    />
                                </div>  
                            </div>

                            <div className="col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Section 7</label>  
                                    <CKEditor
                                        data= {this.state.section7}
                                        onBeforeLoad={CKEDITOR => {
                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                          }
                                          }
                                        config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            contentsCss  : bootstrap,
                                            allowedContent: true,
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorSection7}
                                    />
                                </div>  
                            </div>




       <div className="col-md-12 ">
                                            <div className="form-group">
                                                <label htmlFor="">Silder</label>



                                                <div className="banner-section">
                                                    {
                                                        this.state.commonSilder ?

                                                            this.state.commonSilder.map((attribute, idx) => (
                                                                <div className="banner-single">
                                                                    <button className="rem-btn btn btn-danger"
                                                                        onClick={this.handleRemovecommonSilder(idx)}
                                                                        type="button"><span aria-hidden="true">×</span></button>

                    <MultipleUploadImages
                    idx={idx}
                    imagestatus={this.state.common_image_status[idx]}
                    onMultipleupload={this.onCommonUpload}
                    value={
                    (attribute.image ? attribute.image : "")
                    }
                  
                    /> 
  
                                                                </div>
                                                            )) : ''
                                                    }


                                                    <button
                                                        type="button"
                                                        onClick={this.handleAddcommonSilder}
                                                        className="add-btn btn btn-success"
                                                    > + </button>

                                                </div>



                                            </div>
                                        </div>


                            <div className="col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Section 8</label>  
                                    <CKEditor
                                        data= {this.state.section8}
                                        onBeforeLoad={CKEDITOR => {
                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                          }
                                          }
                                        config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            contentsCss  : bootstrap,
                                            allowedContent: true,
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorSection8}
                                    />
                                </div>  
                            </div>                           
                           
                           
                            <div className="col-md-12 ">
                                <div className="form-inline form float-right form-btn-wrap">
                                    
                                    <span className="input-group-btn">
                                        <button onClick={this.handleSubmit} className="btn btn-info">Update</button>
                                    </span>
                                </div>
                                {this.state.message !== "" ? (
                                                <div className="tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message}</p>
                                                </div></div>
                                            ) : null} 
                                          
                                            
                            </div>
                        </div>
                     </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment>
          );
    }
}
 
export default CareerEditPage;