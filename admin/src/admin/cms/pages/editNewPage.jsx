import React, { Component } from "react";
import Adminheader from "../../common/adminHeader";
import Adminsidebar from "../../common/adminSidebar";
import { Link } from "react-router-dom";
import Joi, { join } from "joi-browser";
import * as managePageService from "../../../ApiServices/admin/managePages";
import CKEditor from "ckeditor4-react";
import { apiUrl, siteUrl, bootstrap } from "../../../../src/config.json";
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import MultipleUploadImages from "../../common/multipleuploadimages";
import UploadVideo from "../../common/uploadVideo";
import ReactSpinner from "react-bootstrap-spinner"; 
class EditNewPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parentPages: [
        { name: "One", id: 1 },
        { name: "Two", id: 2 },
        { name: "Three", id: 3 },
        { name: "four", id: 4 }
      ],
      data: { title: "", editor_content: "" },
      errors: {},
      submit_status: false,
      message: "",
      responsetype: "",
      editordata: [],
      bannerSlider: [
        {
          heading: '',
          subheading: '',
          image: '',
        }
      ],
      upload_image_status: [],
      focused: false,
      newsectionCout: 0,
      upload_url: '',
      embed_url:"",
      videofile: "",
      videourl: '',
      is_preview:"",
    };

    this.onEditorChange = this.onEditorChange.bind(this);
  }

  onEditorChange(evt, i) {
    console.log('onEditorChange evt', this.state.editordata, i);
    const editordata = [...this.state.editordata];
    editordata[i - 1] = evt.editor.getData()
    this.setState({
      editordata: editordata
    });
  }

  /*****************Joi validation schema**************/

  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    page_title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    meta_tag: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    meta_desc: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    slug: Joi.string(),
    editor_content: Joi.any().allow(),
    videourl: Joi.string().allow(''),
    videourl: Joi.string().allow(''),
   video_type: Joi.string().allow(''),
  };s
  /*****************end Joi validation schema**************/

  /*****************INPUT HANDLE CHANGE **************/

  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);

    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  /*****************JOI VALIDATION CALL**************/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  // /***************** FORM SUBMIT **************/

  handleSubmit = async () => {
    this.setState({ submit_status: true });
    const data = { ...this.state.data };

    const errors = { ...this.state.errors };
    data["editor_content"] = this.state.editordata;


    const page_data = {
      id: this.state.id,
      title: this.state.data.title,
      slug: this.state.data.slug,
      page_title: this.state.data.page_title,
      meta_tag: this.state.data.meta_tag,
      meta_desc: this.state.data.meta_desc,
      // attributes: SetAttributes,
      content: {
        editor_content: this.state.editordata,
        bannerSlider: this.state.bannerSlider.filter((s, sidx) => s.heading !== ''),
        videourl: this.state.data.videourl,
        video_type: this.state.data.video_type
        //commonSilder : this.state.commonSilder.filter((s, sidx) => s.image !== ''),
      }

    }

    console.log('data', data, page_data);
    let result = Joi.validate(data, this.schema);

    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      this.setState({ submit_status: false });
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
      

        const response = await managePageService.updatePagedatabyId(page_data);
        if (response) {
          if (response.data.status == 1) {
            this.setState({
              message: response.data.message,
              responsetype: "success"
            });
            window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
            this.setState({ submit_status: false });
            setTimeout(() => { 
              window.location.reload();
            }, 2000);
          } else {
            this.setState({ submit_status: false });
            this.setState({
              message: response.data.message,
              responsetype: "error"
            });
            window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
          }
          return response;
        }
      }
    }
  };

  addNewSection = async () => {
    let newsectionCout = this.state.newsectionCout;
    this.setState({ newsectionCout: newsectionCout + 1 });
    console.log('addNewSection', this.state.newsectionCout);
    //  console.log('addNewSection' ,addNewSection);
  };

  componentDidMount = async () => {
    this.getPageContent();
  }


  getPageContent = async () => {
    const id = this.props.id;
    this.setState({
      id: id,
      blocking: true
    });

    console.log(this.props)
    try {
      this.setState({ spinner: true });
      const response = await managePageService.getSinglePageById(id);
      console.log('entere section', response.data.data.page_data.title);
      if (response.data.status === 1) {

        console.log('response.data.data.page_data', response.data.data.page_data.is_preview);
        let newPagearray = {
          slug: response.data.data.page_data.slug,
          //   slug: this.state.slug,
          title: response.data.data.page_data.title,
          page_title: response.data.data.page_data.page_title,
          meta_tag: response.data.data.page_data.meta_tag,
          meta_desc: response.data.data.page_data.meta_desc,
         
        }; 

        this.setState({
          data: newPagearray,
        });


        if (response.data.data.page_data.hasOwnProperty('content')) {
            console.log('entered------------------>')
          let data = { ...this.state.data };

          data['videourl'] = response.data.data.page_data.content.videourl ? response.data.data.page_data.content.videourl : ''

          data["video_type"] = response.data.data.page_data
             .content.video_type
             ? response.data.data.page_data.content.video_type
             : "upload_video";

           let uploadurl =
             data.video_type === "upload_video"
               ? response.data.data.page_data.content.videourl
               : "";
           let embedurl =
             data.video_type === "embed_url"
               ? response.data.data.page_data.content.videourl
               : "";
          
          setTimeout(() => {
              this.setState({
                data: data,
                upload_url: uploadurl,
                embed_url: embedurl ,
                is_preview : response.data.data.page_data.is_preview ? response.data.data.page_data.is_preview : false
              });

          }, 3000); 
      }

        const editordata = [...this.state.editordata];


        response.data.data.page_data.content.editor_content.map((page_data, idx) => {
          console.log('page_data', page_data);
          editordata[idx] = page_data
          //status_array.push(false);
        });

        this.setState({
          editordata: editordata,
          bannerSlider: response.data.data.page_data.content.bannerSlider.length > 0 ? response.data.data.page_data.content.bannerSlider :  [
            {
              heading: '',
              subheading: '',
              image: '',
            }
          ],
          newsectionCout: editordata.length ? editordata.length : 0
        });

      } else {

          this.props.history.push({
              pathname: "/admin/pages",
          });
        this.setState({
          template_name: '',
          page: {},
          blocking: false
        })
      }
    } catch (err) {
      this.setState({
        template_name: '',
        page: {},
        blocking: false
      })
    }
  }


  handleheadingChange = idx => evt => {

    const newbannerSlider = this.state.bannerSlider.map((attribute, sidx) => {

      if (idx !== sidx) return attribute;
      return { ...attribute, heading: evt.target.value };
    });

    this.setState({ bannerSlider: newbannerSlider });
  };

  handlesubheadingChange = idx => evt => {

    const newbannerSlider = this.state.bannerSlider.map((attribute, sidx) => {
      if (idx !== sidx) return attribute;
      return { ...attribute, subheading: evt.editor.getData() };
    });

    this.setState({ bannerSlider: newbannerSlider });
  };




  handleAddbannerSlider = () => {


    this.setState({
      bannerSlider: this.state.bannerSlider.concat([{ heading: "", subheading: "", image: '' }]),
      upload_image_status: this.state.upload_image_status.concat([false])
    });
  };

  handleRemovebannerSlider = idx => () => {
    this.setState({
      bannerSlider: this.state.bannerSlider.filter((s, sidx) => idx !== sidx),
      upload_image_status: this.state.upload_image_status.filter((s, sidx) => idx !== sidx)
    });
  };


  onMultipleupload = async (value, item, idx) => {
    let errors = { ...this.state.errors };
    try {

      if (item === "errors") {
        errors["videourl"] = value;
        this.setState({
          errors: errors,
        });

      } else {

        const staus = [...this.state.upload_image_status];
        staus[idx] = true;
        this.setState({ upload_image_status: staus });


        let file = value.target.files[0];
        const response1 = await managePageService.uploadVideo(
          file
        );
        if (response1.data.status === 1) {
          const newbannerSlider = this.state.bannerSlider.map((attribute, sidx) => {
            if (idx !== sidx) return attribute;
            return { ...attribute, image: response1.data.data.file_location };
          });

          this.setState({ bannerSlider: newbannerSlider });
        } else {

          this.setState({
            message: response1.data.message,
            responsetype: "error",
          });
        }
        //  this.setState({ download_1_status: false });
        staus[idx] = false;
        this.setState({ upload_image_status: staus });


      }

    } catch (err) {
      this.setState({
        message: 'something went wrong',
        responsetype: "error",
        download_1_status: false
      });
    }


  };



  onvideoupload = async (value, item) => {
    let errors = { ...this.state.errors };
    try {

        if (item === "errors") {
            errors["videourl"] = value;
            this.setState({
                errors: errors,
            });
            
        } else {
            this.setState({ download_1_status: true });
            let file = value.target.files[0];
            const response1 = await managePageService.uploadVideo(
                file
            );
            if (response1.data.status === 1) {
                
                let data = { ...this.state.data };
                let filepath = { "image_name": response1.data.data.file_name, "image_url": response1.data.data.file_location };
                data['videourl'] = response1.data.data.file_location
                this.setState({ 
                    videofile: filepath,
                    data: data
                });
                
            } else {
                this.setState({
                    message: response1.data.message,
                    responsetype: "error",
                });
            }
            this.setState({ download_1_status: false });
        }

    } catch (err) {
        errors["videourl"] = err.message;
        this.setState({
            message: 'something went wrong',
            responsetype: "error",
            errors: errors,
            download_1_status: false
        });
    }


};


chooseVideoType = (val) => {
  let data = { ...this.state.data };
  data.video_type = val;
  if (val === "embed_url") {
    data.videourl = this.state.embed_url;
     this.setState({ data });
    }
  else {
    data.videourl = this.state.upload_url;
     this.setState({ data });
    }
}


handlePreview = async () => {
  const slug = this.props.slug;
  const id = this.props.id;
  const data = { ...this.state.data };
  const errors = { ...this.state.errors };
  let result = Joi.validate(data, this.schema);
  console.log('handlePreview error',result);
  if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
          errors: errors
      })
  } else {
   
      if (errors.length > 0) {
          this.setState({
              errors: errors
          })
      } else {
          this.setState({ spinner: true });

           const page_data = {
            slug: this.state.data.slug,
      // attributes: SetAttributes,
      preview: {
        id: this.state.data.slug,
        title: this.state.data.title,
        slug: this.state.data.slug,
        page_title: this.state.data.page_title,
        meta_tag: this.state.data.meta_tag,
        meta_desc: this.state.data.meta_desc,
        editor_content: this.state.editordata,
        bannerSlider: this.state.bannerSlider.filter((s, sidx) => s.heading !== ''),
        videourl: this.state.data.videourl,
        video_type: this.state.data.video_type
        //commonSilder : this.state.commonSilder.filter((s, sidx) => s.image !== ''),
      }

    }
        console.log('preview' ,page_data);

          try {
          
              this.setState({ submit_status: true });
              const response = await managePageService.previewPagedata(page_data);
              if (response.data.status === 1) {
                  this.setState({
                    submit_status: false,
                      message1: response.data.message,
                      responsetype: "success",
                      spinner: false
                  }); 
                  setTimeout(() => {
                      const win = window.open("/cimon-gate/admin/preview/" + id, "_blank");
                  win.focus();
                      // this.props.history.push({
                      //   pathname: "/admin/preview/" + id,
                      // });
                    }, 2000);

              } else {
                  this.setState({
                    submit_status: false,
                      spinner: false,
                      message1: response.data.message,
                      responsetype: "error",
                  });

              }
          } catch (err) {
              this.setState({
                  message1: 'something went wrong',
                  responsetype: "error",
                  spinner: false,
                  submit_status: false
              });
          }


      }
  }
};




  render() {
    // const { editorState } = this.state.editorState;
    let optionTemplate = this.state.parentPages.map(v => (
      <option pages={v.id}>{v.name}</option>
    ));



      console.log('thissssssss' , this.state.is_preview);
    let contentlength = this.state.newsectionCout ? this.state.newsectionCout : 0;
    let singleElements = [];
    for (let i = 0; i < contentlength;) {
      // push the component to elements!
      let count = i;
      singleElements.push(
        <div className="col-md-12 ">
          <div className="form-group">
            <label htmlFor="">Content  {count + 1}</label>
            <CKEditor
              data={this.state.editordata[i]}
               onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                contentsCss: bootstrap,
                allowedContent: true,
                filebrowserImageUploadUrl:
                  apiUrl + "/admin/upload/imageupload-new"
              }}
              onInit={editor => { }}
              onChange={e => this.onEditorChange(e, i)}
            />
          </div>
        </div>
      );
      i++
    }

    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4">
                  <div className="row addpage-form-wrap">
                    <div className="addpage-form">
                      <div className="col-md-12 ">
                        {this.state.message !== "" ? (
                          <div className="form-group tableinformation">
                            <div className={this.state.responsetype}>
                              <p>{this.state.message}</p>
                            </div>
                          </div>
                        ) : null}



{this.state.is_preview === true?(<div className="col-md-12 ">
                                            <div className="form-group1 preview_grp">
                                            <span className="input-group-btn">
                                          
                                               <button onClick={this.handlePreview} className="btn btn-info  pull-right">Preview</button>
                                               {this.state.message1 !== "" ? (
                                                <div className="tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message1}</p>
                                                </div></div>
                                            ) : null} 
                                            
                                             </span>

                                               
                                            </div>
                                           
                                          
                                            
                                        </div>):""}
                              

                        <div className="form-group">
                          <label htmlFor="">Add New Page</label>
                          <input
                            name="title"
                            value={this.state.data.title}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add title"
                            className="form-control"
                          />
                          {this.state.errors.title ? (
                            <div className="error text-danger">
                              {this.state.errors.title}
                            </div>
                          ) : (
                              ""
                            )}
                        </div>
                      </div>



                      <div className="col-md-12 ">
                        <div className="form-group">
                          <label htmlFor="">Page Title</label>
                          <input
                            name="page_title"
                            value={this.state.data.page_title}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Page title"
                            className="form-control"
                          />
                          {this.state.errors.page_title ? (
                            <div className="error text-danger">
                              {this.state.errors.page_title}
                            </div>
                          ) : (
                              ""
                            )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Meta tag</label>
                          <input
                            name="meta_tag"
                            value={this.state.data.meta_tag}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Meta Tag"
                            className="form-control"
                          />
                          {this.state.errors.meta_tag ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_tag}
                            </div>
                          ) : (
                              ""
                            )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Meta Description</label>
                          <input
                            name="meta_desc"
                            value={this.state.data.meta_desc}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.meta_desc ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_desc}
                            </div>
                          ) : (
                              ""
                            )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Page Slug</label>
                          <input
                            name="slug"
                            value={this.state.data.slug}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Page Slug"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                              ""
                            )}
                        </div>
                      </div>





                      <div className="col-md-12 ">
                        <div className="form-group">
                          <label htmlFor="">Banner</label>



                          <div className="banner-section">
                            {
                              this.state.bannerSlider ?

                                this.state.bannerSlider.map((attribute, idx) => (
                                  <div className="banner-single" key={idx}>



                                    <Accordion>
                                      <Card>
                                        <Card.Header>
                                          <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                            {attribute.heading ? attribute.heading : `slide #${idx + 1}`}
                                          </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                          <Card.Body>
                                            <input
                                              onChange={this.handleheadingChange(idx)}
                                              type="text"
                                              value={attribute.heading ? attribute.heading : ""}
                                              placeholder="Heading"
                                              className="form-control" />


                                            <CKEditor
                                              data={attribute.subheading}
                                              config={
                                                {
                                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                  contentsCss: bootstrap,
                                                  height: '6em',
                                                  allowedContent: true,
                                                  filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                                }
                                              }
                                              onInit={
                                                editor => {
                                                }
                                              }
                                              onChange={this.handlesubheadingChange(idx)}
                                            />

                                            <MultipleUploadImages
                                              idx={idx}
                                              imagestatus={this.state.upload_image_status[idx]}
                                              onMultipleupload={this.onMultipleupload}
                                              value={
                                                (attribute.image ? attribute.image : "")
                                              }
                                            // errors={this.state.errors}
                                            />
                                          </Card.Body>
                                        </Accordion.Collapse>
                                      </Card>
                                    </Accordion>






                                  </div>
                                )) : ''
                            }




                          </div>



                        </div>
                      </div>



                      <div className="col-md-12 ">
                            <div className="form-group">
                              <label for="videotypeid">Video Url</label>
                              <div>
                                <input
                                  type="radio"
                                  name="url_type"
                                  id="videotypeid"
                                  checked={
                                    this.state.data.video_type ===
                                    "upload_video"
                                  }
                                  onClick={() =>
                                    this.chooseVideoType("upload_video")
                                  }
                                />
                                <label
                               for="uploadvideoid"
                                  style={{
                                    paddingRight: "15px",
                                    paddingLeft: "8px",
                                    fontSize: "15px"
                                  }}
                                >
                                  Upload video
                                </label>
                                <input
                                id = "uploadvideoid"
                                  type="radio"
                                  name="url_type"
                                  checked={
                                    this.state.data.video_type ===
                                    "embed_url"
                                  }
                                  onClick={() =>
                                    this.chooseVideoType("embed_url")
                                  }
                                />
                                <label
                                  style={{
                                    paddingRight: "15px",
                                    paddingLeft: "8px",
                                    fontSize: "15px"
                                  }}
                                >
                                  Embed video url
                                </label>
                              </div>

                              {this.state.data.video_type ===
                              "upload_video" ? (
                                <React.Fragment>





                                  <UploadVideo
                                    onuplaodVideo={this.onvideoupload}
                                    value={
                                      this.state.videofile.image_url
                                        ? this.state.videofile.image_url
                                        : ""
                                    }
                                    errors={this.state.errors}
                                  />
                                  {this.state.download_1_status === true ? (
                                    <ReactSpinner
                                      type="border"
                                      color="dark"
                                      size="1"
                                    />
                                  ) : (
                                    ""
                                  )}
                                </React.Fragment>
                              ) : (
                                ""
                              )}
                              <input
                                name="videourl"
                                value={this.state.data.videourl}
                                onChange={this.handleChange}
                                type="text"
                                placeholder="Video Url"
                                className="form-control"
                              />
                              {this.state.errors.videourl ? (
                                <div className="error text-danger">
                                  {this.state.errors.videourl}
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>




                      {/* <div className="col-md-12 ">
                        <div className="form-group">
                          <label htmlFor="">Content</label>
                          <CKEditor
                            data=""
                             onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                              extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                              allowedContent: true,
                              filebrowserImageUploadUrl:
                                apiUrl + "/admin/upload/imageupload-new"
                            }}
                            onInit={editor => { }}
                            onChange={e => this.onEditorChange(e,0)}
                          />
                        </div>
                      </div> */}




                      <div className="col-md-12">
                        {singleElements}
                      </div>
                      <div className="col-md-12">
                        <div class="attribute_container">
                          <button onClick={this.addNewSection}
                            type="button" class="add-btn btn btn-success">Add New</button>
                        </div>
                      </div>





                      {/* <div className="col-md-12 ">
                        <div className="collapse-wrapper">
                          <div className="title-collapse">
                            <a
                              className=" "
                              data-toggle="collapse"
                              href="#collapseExample"
                              role="button"
                              aria-expanded="false"
                              aria-controls="collapseExample"
                            >

                              Page Template
                            </a>
                          </div>
                          <div className="collapse" id="collapseExample">
                            <div className="form-group">
                              <label htmlFor="">Template</label>
                              <select
                                className="form-control"
                                value={this.state.pages}
                                onChange={this.handleChange}
                              >
                                {optionTemplate}
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                
                
                 */}

                      <div className="col-md-12 ">
                        <div className="form-inline form float-right form-btn-wrap">
                          {/* <Link to="#">Move to Trash</Link> */}
                          <span className="input-group-btn">
                            <button
                              disabled={
                                this.state.submit_status === true
                                  ? "disabled"
                                  : false
                              }
                              
                              onClick={this.handleSubmit}
                              className="btn btn-info"
                            >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                              Update
                            </button>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default EditNewPage;
