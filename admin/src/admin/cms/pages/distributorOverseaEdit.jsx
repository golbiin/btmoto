import React, { Component } from 'react';
import Adminheader from "../../common/adminHeader";
import Adminsidebar from "../../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as managePageService from "../../../ApiServices/admin/managePages";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl, bootstrap } from "../../../../src/config.json";
import UploadVideo from "../../common/uploadVideo";
import MultipleUploadImages from "../../common/multipleuploadimages";
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

class DistributorOverseaEdit extends Component {

    constructor(props) {
        super(props);


        this.onEditorChange = this.onEditorChange.bind(this);


        this.onEditorSection1 = this.onEditorSection1.bind(this);
        this.onEditorSection1sub1 = this.onEditorSection1sub1.bind(this);
        this.onEditorSection2 = this.onEditorSection2.bind(this);
        this.onEditorSection3 = this.onEditorSection3.bind(this);
        this.onEditorSection4 = this.onEditorSection4.bind(this);
        this.onEditorSection5 = this.onEditorSection5.bind(this);
        this.onEditorSection6 = this.onEditorSection6.bind(this);
        this.onEditorSection7 = this.onEditorSection7.bind(this);
        this.onEditorSection7sub1 = this.onEditorSection7sub1.bind(this);
        this.onEditorSection8 = this.onEditorSection8.bind(this);

    }

    state = {
        cimonPages: [],
        uploadedImages: [],
        parentPages: [
            { name: 'One', id: 1 },
            { name: 'Two', id: 2 },
            { name: 'Three', id: 3 },
            { name: 'four', id: 4 }
        ],
        errors: {},
        validated: false,
        pageStatus: true,
        submitStatus: false,
        spinner: true,
        message: "",
        message1: "",
        responsetype: "",
        editorState: "",
        bannerContent: '',
        videofile: "",
        upload_image_status: [],
        videourl: '',
        section1: '',
        section1sub1: '',
        section2: '',
        section3: '',
        section4: '',
        section5: '',
        section6: '',
        section7: '',
        section7sub1:'',
        section8: '',
        attributes: [],
        is_preview:"",
        is_delete: "",
        upload_url: '',
        embed_url:""
    };

    onEditorChange(evt) {
        this.setState({
            editorState: evt.editor.getData()
        });
    }


    onEditorSection1(evt) {
        this.setState({
            section1: evt.editor.getData()
        });
    }

    onEditorSection1sub1(evt) {
        this.setState({
            section1sub1: evt.editor.getData()
        });
    }

    onEditorSection2(evt) {
        this.setState({
            section2: evt.editor.getData()
        });
    }
    onEditorSection3(evt) {
        this.setState({
            section3: evt.editor.getData()
        });
    }
    onEditorSection4(evt) {
        this.setState({
            section4: evt.editor.getData()
        });
    }
    onEditorSection5(evt) {
        this.setState({
            section5: evt.editor.getData()
        });
    }
    onEditorSection6(evt) {
        this.setState({
            section6: evt.editor.getData()
        });
    }
    onEditorSection7(evt) {
        this.setState({
            section7: evt.editor.getData()
        });
    }
    onEditorSection7sub1(evt) {
        this.setState({
            section7sub1: evt.editor.getData()
        });
    }
    onEditorSection8(evt) {
        this.setState({
            section8: evt.editor.getData()
        });
    }



  
    onvideoupload = async (value, item) => {
        let errors = { ...this.state.errors };
        try {

            if (item === "errors") {
                errors["videourl"] = value;
                this.setState({
                    errors: errors,
                });
                
            } else {


                this.setState({ download_1_status: true });
                let file = value.target.files[0];
                const response1 = await managePageService.uploadVideo(
                    file
                );
                if (response1.data.status === 1) {
                    
                    let cimonPages = { ...this.state.cimonPages };
                    let filepath = { "image_name": response1.data.data.file_name, "image_url": response1.data.data.file_location };
                    cimonPages['videourl'] = response1.data.data.file_location
                    this.setState({
                        videofile: filepath,
                        cimonPages: cimonPages
                    });
                    
                } else {
                    this.setState({
                        message: response1.data.message,
                        responsetype: "error",
                    });
                }
                this.setState({ download_1_status: false });
            }

        } catch (err) {
            errors["videourl"] = err.message;
            this.setState({ 
                message: 'something went wrong',
                responsetype: "error",
                errors : errors,
                download_1_status: false 
            });
        }


    };


    /*****************Joi validation schema**************/
    schema = {
        title: Joi.string()
            .required()
            .error(() => {
                return {
                    message: "This field is Required",
                };
            }),
            page_title: Joi.string()
            .required()
            .error(() => {
              return {
                message: "This field is Required",
              };
            }),
            meta_tag: Joi.string()
            .required()
            .error(() => {
              return {
                message: "This field is Required",
              };
            }),
            meta_desc: Joi.string()
            .required()
            .error(() => {
              return {
                message: "This field is Required",
              };
            }),
            
        _id: Joi.optional().label("id"),
        slug: Joi.string(),
        editor_content: Joi.string(),
        videourl: Joi.string().allow(''),
         video_type: Joi.string().allow(''),


    };
    /*****************end Joi validation schema**************/

    /*****************INPUT HANDLE CHANGE **************/

    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const cimonPages = { ...this.state.cimonPages };
        const errorMessage = this.validateProperty(input);
        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        cimonPages[input.name] = input.value;
        this.setState({ cimonPages, errors });

    };

    /*****************JOI VALIDATION CALL**************/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };


    // /***************** FORM SUBMIT **************/



    componentDidMount = async () => {
        const slug = this.props.slug;
        const id = this.props.id;
    
        this.setState({
            spinner: true,
           // slug: slug
             id:id,
        });
try{
        this.setState({ spinner: true });
        const response = await managePageService.getSinglePageById(id);
        let cimonPages = { ...this.state.cimonPages };
        if (response.data.status === 1) {
            let newPagearray = {
                slug: response.data.data.page_data.slug,
             //   slug: this.state.slug,
                title: response.data.data.page_data.title,
                page_title: response.data.data.page_data.page_title,
                meta_tag: response.data.data.page_data.meta_tag,
                meta_desc: response.data.data.page_data.meta_desc,
            };
            this.setState({ cimonPages: newPagearray });
            this.setState({ is_preview: response.data.data.page_data.is_preview });
            this.setState({ is_delete: response.data.data.page_data.is_delete });

            
            if (response.data.data.page_data.hasOwnProperty('content')) {

                let cimonPages = { ...this.state.cimonPages };

                cimonPages['videourl'] = response.data.data.page_data.content.videourl ? response.data.data.page_data.content.videourl : ''
 cimonPages["video_type"] = response.data.data.page_data.content.video_type
   ? response.data.data.page_data.content.video_type
   : "upload_video";

 let uploadurl =
   cimonPages.video_type === "upload_video"
     ? response.data.data.page_data.content.videourl
     : "";
 let embedurl =
   cimonPages.video_type === "embed_url"
     ? response.data.data.page_data.content.videourl
     : "";
                setTimeout(() => {
                    this.setState({
                      cimonPages: cimonPages,
                      bannerContent: response.data.data.page_data.content
                        .bannerContent
                        ? response.data.data.page_data.content.bannerContent
                        : "",
                      attributes: response.data.data.page_data.content
                        .bannerSlider
                        ? response.data.data.page_data.content.bannerSlider
                        : [],
                      section1: response.data.data.page_data.content.section1
                        ? response.data.data.page_data.content.section1
                        : "",
                      section1sub1: response.data.data.page_data.content
                        .section1sub1
                        ? response.data.data.page_data.content.section1sub1
                        : "",
                      section2: response.data.data.page_data.content.section2
                        ? response.data.data.page_data.content.section2
                        : "",
                      section3: response.data.data.page_data.content.section3
                        ? response.data.data.page_data.content.section3
                        : "",
                      section4: response.data.data.page_data.content.section4
                        ? response.data.data.page_data.content.section4
                        : "",
                      section5: response.data.data.page_data.content.section5
                        ? response.data.data.page_data.content.section5
                        : "",
                      section6: response.data.data.page_data.content.section6
                        ? response.data.data.page_data.content.section6
                        : "",
                      section7: response.data.data.page_data.content.section7
                        ? response.data.data.page_data.content.section7
                        : "",
                      section7sub1: response.data.data.page_data.content
                        .section7sub1
                        ? response.data.data.page_data.content.section7sub1
                        : "",

                      section8: response.data.data.page_data.content.section8
                        ? response.data.data.page_data.content.section8
                        : "",
                      spinner: false,
                      upload_url: uploadurl,
                      embed_url: embedurl
                    });

                    let status_array = [];
                    this.state.attributes.map((attribute, sidx) => {
                        
                        status_array.push(false);
                    });
                    this.setState({
                        upload_image_status: status_array
                    });


                }, 2500);





            }
            this.setState({
                spinner: false,
            });
        } else {
            this.setState({ spinner: false });
            this.setState({
                pageStatus: false,
                message: response.data.message,
                responsetype: "error",
            });
        }

    } catch (err) {
        this.setState({
            message: 'something went wrong',
            responsetype: "error",
            spinner: false
        });
    }




    }


    handleSubmit = async () => {
        const cimonPages = { ...this.state.cimonPages };
        const errors = { ...this.state.errors };
        //  cimonPages["editor_content"] = this.state.editorState;

        let result = Joi.validate(cimonPages, this.schema);

        if (result.error) {
            
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({
                errors: errors
            })
        } else {
            
            if (errors.length > 0) {
                this.setState({
                    errors: errors
                })
            } else {
                this.setState({ spinner: true });

                const page_data = {
                    title: this.state.cimonPages.title,
                    // slug: this.state.slug,
                    id: this.state.id,
                    slug :this.state.cimonPages.slug,
                    page_title: this.state.cimonPages.page_title,
                    meta_tag: this.state.cimonPages.meta_tag,
                    meta_desc: this.state.cimonPages.meta_desc,
                    
                    // attributes: SetAttributes,
                    content: {
                        bannerContent: this.state.bannerContent,
                        bannerSlider: this.state.attributes.filter((s, sidx) => s.heading !== ''),
                        section1: this.state.section1,
                        section1sub1: this.state.section1sub1,
                        section2: this.state.section2,
                        section3: this.state.section3,
                        section4: this.state.section4,
                        section5: this.state.section5,
                        section6: this.state.section6,
                        section7: this.state.section7,
                        section7sub1: this.state.section7sub1,
                        section8: this.state.section8,
                        videourl: this.state.cimonPages.videourl,
                        video_type: this.state.cimonPages.video_type
                    }

                }


    try{

                
                this.setState({ submit_status: true });
                const response = await managePageService.updatePagedata(page_data);
                if (response.data.status === 1) {
                    this.setState({
                        submitStatus: false,
                        message: response.data.message,
                        responsetype: "success",
                        spinner: false
                    });


                } else {
                    this.setState({
                        submitStatus: false,
                        spinner: false,
                        message: response.data.message,
                        responsetype: "error",
                    });

                }

    } catch (err) {
        this.setState({
            message: 'something went wrong',
            responsetype: "error",
            spinner: false ,
            submit_status : false
        });
    }



            }
        }
    };

    /* preview */

    handlePreview = async () => {
        const slug = this.props.slug;
        const id = this.props.id;
        const cimonPages = { ...this.state.cimonPages };
        const errors = { ...this.state.errors };
        //  cimonPages["editor_content"] = this.state.editorState;

        let result = Joi.validate(cimonPages, this.schema);

        if (result.error) {
            
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({
                errors: errors
            })
        } else {
            
            if (errors.length > 0) {
                this.setState({
                    errors: errors
                })
            } else {
                this.setState({ spinner: true });

                const page_data = {
                    title: this.state.cimonPages.title,
                    slug: this.state.cimonPages.slug,
                    page_title: this.state.cimonPages.page_title,
                    meta_tag: this.state.cimonPages.meta_tag,
                    meta_desc: this.state.cimonPages.meta_desc,
                    // attributes: SetAttributes,
                    preview: {
                        bannerContent: this.state.bannerContent,
                        bannerSlider: this.state.attributes.filter((s, sidx) => s.heading !== ''),
                        section1: this.state.section1,
                        section1sub1: this.state.section1sub1,
                        section2: this.state.section2,
                        section3: this.state.section3,
                        section4: this.state.section4,
                        section5: this.state.section5,
                        section6: this.state.section6,
                        section7: this.state.section7,
                        section7sub1: this.state.section7sub1,
                        section8: this.state.section8,
                        videourl: this.state.cimonPages.videourl
                    }

                }


    try{

                
                this.setState({ submit_status: true });
                const response = await managePageService.previewPagedata(page_data);
                if (response.data.status === 1) {
                    this.setState({
                        submitStatus: false,
                        message1: response.data.message,
                        responsetype: "success",
                        spinner: false
                    });
                    setTimeout(() => {
                        const win = window.open("/cimon-gate/admin/preview/" + id, "_blank");
                        win.focus();
                        // this.props.history.push({
                        //   pathname: "/admin/preview/" + id,
                        // });
                      }, 2000);


                } else {
                    this.setState({
                        submitStatus: false,
                        spinner: false,
                        message1: response.data.message,
                        responsetype: "error",
                    });

                }

    } catch (err) {
        this.setState({
            message1: 'something went wrong',
            responsetype: "error",
            spinner: false ,
            submit_status : false
        });
    }



            }
        }
    };


    handleheadingChange = idx => evt => {

        const newAttributes = this.state.attributes.map((attribute, sidx) => {
            
            if (idx !== sidx) return attribute;
            return { ...attribute, heading: evt.target.value };
        });

        this.setState({ attributes: newAttributes });
    };


    //  handlesubheadingChange = idx => evt => {

    handlesubheadingChange = idx => evt => {

        // this.setState( {
        // 			bannerContent : evt.editor.getData()
        // 		} );

        const newAttributes = this.state.attributes.map((attribute, sidx) => {
            if (idx !== sidx) return attribute;
            return { ...attribute, subheading: evt.editor.getData() };
        });

        this.setState({ attributes: newAttributes });
    };




    handleAddAttributes = () => {


        this.setState({
            attributes: this.state.attributes.concat([{ heading: "", subheading: "", image: '' }]),
            upload_image_status: this.state.upload_image_status.concat([false])
        });
    };

    handleRemoveAttributes = idx => () => {
        this.setState({
            attributes: this.state.attributes.filter((s, sidx) => idx !== sidx),
            upload_image_status: this.state.upload_image_status.filter((s, sidx) => idx !== sidx)
        });
    };


    onMultipleupload = async (value, item, idx) => {
        let errors = { ...this.state.errors };
        try {

            if (item === "errors") {
                errors["videourl"] = value;
                this.setState({
                    errors: errors,
                });
                
            } else {

                const staus = [...this.state.upload_image_status];
                staus[idx] = true;
                this.setState({ upload_image_status: staus });


                let file = value.target.files[0];
                const response1 = await managePageService.uploadVideo(
                    file
                );
                if (response1.data.status === 1) {
                    const newAttributes = this.state.attributes.map((attribute, sidx) => {
                        if (idx !== sidx) return attribute;
                        return { ...attribute, image: response1.data.data.file_location };
                    });

                    this.setState({ attributes: newAttributes });
                } else {
                    this.setState({
                        message: response1.data.message,
                        responsetype: "error",
                    });
                }
                //  this.setState({ download_1_status: false });
                staus[idx] = false;
                this.setState({ upload_image_status: staus });


            }

        } catch (err) {
            this.setState({
                message: 'something went wrong',
                responsetype: "error",
                download_1_status: false
            });
        }


    };
    
    chooseVideoType = (val) => {
      let cimonPages = { ...this.state.cimonPages };
      cimonPages.video_type = val;
      if (val === "embed_url") {
        cimonPages.videourl = this.state.embed_url;
         this.setState({ cimonPages });
        }
      else {
        cimonPages.videourl = this.state.upload_url;
         this.setState({ cimonPages });
        }
    }


    render() {



        

        return (
          <React.Fragment>
            <div className="container-fluid admin-body">
              <div className="admin-row">
                <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
                </div>

                <div className="col-md-10 col-sm-12  content">
                  <div className="row content-row">
                    <div className="col-md-12  header">
                      <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents backgroundoverlay  home-inner-content pt-4 pb-4 pr-4">
                      <React.Fragment>
                        <div
                          style={{
                            display:
                              this.state.spinner === true ? "flex" : "none"
                          }}
                          className="overlay"
                        >
                          <ReactSpinner
                            type="border"
                            color="primary"
                            size="10"
                          />
                        </div>
                      </React.Fragment>
                      <div className="row addpage-form-wrap">
                        <div className="addpage-form">
                          {this.state.is_preview === true ? (
                            <div className="col-md-12 ">
                              <div className="form-group1 preview_grp">
                                <span className="input-group-btn">
                                  <button
                                    onClick={this.handlePreview}
                                    className="btn btn-info  pull-right"
                                  >
                                    Preview
                                  </button>
                                  {this.state.message1 !== "" ? (
                                    <div className="tableinformation">
                                      <div className={this.state.responsetype}>
                                        <p>{this.state.message1}</p>
                                      </div>
                                    </div>
                                  ) : null}
                                </span>
                              </div>
                            </div>
                          ) : (
                            ""
                          )}

                          <div className="col-md-12 ">
                            <div className="form-group">
                              <label htmlFor="">Edit Page</label>
                              <input
                                name="title"
                                value={this.state.cimonPages.title}
                                onChange={this.handleChange}
                                type="text"
                                placeholder="Add title"
                                className="form-control"
                              />
                              {this.state.errors.title ? (
                                <div className="error text-danger">
                                  {this.state.errors.title}
                                </div>
                              ) : (
                                ""
                              )}
                              {this.state.is_delete && (
                                <p>
                                  <a
                                    href={
                                      siteUrl + "/distributor-oversea/" + this.state.cimonPages.slug
                                    }
                                  >
                                    {siteUrl + "/distributor-oversea/" + this.state.cimonPages.slug}
                                  </a>
                                </p>
                              )}
                            </div>
                          </div>

                          <div className="col-md-12 ">
                            <div className="form-group">
                              <label htmlFor="">Page Title</label>
                              <input
                                name="page_title"
                                value={this.state.cimonPages.page_title}
                                onChange={this.handleChange}
                                type="text"
                                placeholder="Add Page title"
                                className="form-control"
                              />
                              {this.state.errors.page_title ? (
                                <div className="error text-danger">
                                  {this.state.errors.page_title}
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="form-group">
                              <label htmlFor="">Meta tag</label>
                              <input
                                name="meta_tag"
                                value={this.state.cimonPages.meta_tag}
                                onChange={this.handleChange}
                                type="text"
                                placeholder="Add Meta Tag"
                                className="form-control"
                              />
                              {this.state.errors.meta_tag ? (
                                <div className="error text-danger">
                                  {this.state.errors.meta_tag}
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="form-group">
                              <label htmlFor="">Meta Description</label>
                              <input
                                name="meta_desc"
                                value={this.state.cimonPages.meta_desc}
                                onChange={this.handleChange}
                                type="text"
                                placeholder="Add Meta Description"
                                className="form-control"
                              />
                              {this.state.errors.meta_desc ? (
                                <div className="error text-danger">
                                  {this.state.errors.meta_desc}
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="form-group">
                              <label htmlFor="">Page Slug</label>
                              <input
                                name="slug"
                                value={this.state.cimonPages.slug}
                                onChange={this.handleChange}
                                type="text"
                                placeholder="Add Page Slug"
                                className="form-control"
                              />
                              {this.state.errors.slug ? (
                                <div className="error text-danger">
                                  {this.state.errors.slug}
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>

                          <div className="col-md-12 ">
                            <div className="form-group">
                              <label htmlFor="">Banner</label>

                              <div className="banner-section">
                                {this.state.attributes
                                  ? this.state.attributes.map(
                                      (attribute, idx) => (
                                        <div className="banner-single">
                                          <button
                                            class="rem-btn btn btn-danger"
                                            onClick={this.handleRemoveAttributes(
                                              idx
                                            )}
                                            type="button"
                                          >
                                            <span aria-hidden="true">×</span>
                                          </button>

                                          <Accordion>
                                            <Card>
                                              <Card.Header>
                                                <Accordion.Toggle
                                                  as={Button}
                                                  variant="link"
                                                  eventKey="0"
                                                >
                                                  {attribute.heading
                                                    ? attribute.heading
                                                    : `slide #${idx + 1}`}
                                                </Accordion.Toggle>
                                              </Card.Header>
                                              <Accordion.Collapse eventKey="0">
                                                <Card.Body>
                                                  <input
                                                    onChange={this.handleheadingChange(
                                                      idx
                                                    )}
                                                    type="text"
                                                    value={
                                                      attribute.heading
                                                        ? attribute.heading
                                                        : ""
                                                    }
                                                    placeholder="Heading"
                                                    className="form-control"
                                                  />

                                                  <CKEditor
                                                    data={attribute.subheading}
                                                     onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                                      extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                      contentsCss: bootstrap,
                                                      height: "6em",
                                                      allowedContent: true,
                                                      filebrowserImageUploadUrl:
                                                        apiUrl +
                                                        "/admin/upload/imageupload-new"
                                                    }}
                                                    onInit={editor => {}}
                                                    onChange={this.handlesubheadingChange(
                                                      idx
                                                    )}
                                                  />

                                                  <MultipleUploadImages
                                                    idx={idx}
                                                    imagestatus={
                                                      this.state
                                                        .upload_image_status[
                                                        idx
                                                      ]
                                                    }
                                                    onMultipleupload={
                                                      this.onMultipleupload
                                                    }
                                                    value={
                                                      attribute.image
                                                        ? attribute.image
                                                        : ""
                                                    }
                                                    // errors={this.state.errors}
                                                  />
                                                </Card.Body>
                                              </Accordion.Collapse>
                                            </Card>
                                          </Accordion>
                                        </div>
                                      )
                                    )
                                  : ""}

                                <button
                                  type="button"
                                  onClick={this.handleAddAttributes}
                                  className="add-btn btn btn-success"
                                >
                                  {" "}
                                  +{" "}
                                </button>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12 ">
                            <div className="form-group">
                              <label htmlFor="">Section 1</label>
                              <CKEditor
                                data={this.state.section1}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  contentsCss: bootstrap,
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorSection1}
                              />
                            </div>
                          </div>
 
 
          <div className="col-md-12 ">
                            <div className="form-group">
                              <label htmlFor="">Section 2</label>
                              <CKEditor
                                data={this.state.section2}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  contentsCss: bootstrap,
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorSection2}
                              />
                            </div>
                          </div>


                          <div className="col-md-12 ">
                            <div className="form-group">
                              <label htmlFor="">Section 3</label>
                              <CKEditor
                                data={this.state.section3}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  contentsCss: bootstrap,
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorSection3}
                              />
                            </div>
                          </div>

                          <div className="col-md-12 ">
                            <div className="form-group">
                              <label htmlFor="">Section 4</label>
                              <CKEditor
                                data={this.state.section4}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  contentsCss: bootstrap,
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorSection4}
                              />
                            </div>
                          </div>

                    
                          <div className="col-md-12 ">
                            <div className="form-inline form float-right form-btn-wrap">
                              {/* <Link to="#">Move to Trash</Link> */}
                              <span className="input-group-btn">
                                <button
                                  onClick={this.handleSubmit}
                                  className="btn btn-info"
                                >
                                  Update
                                </button>
                              </span>
                            </div>
                            {this.state.message !== "" ? (
                              <div className="tableinformation">
                                <div className={this.state.responsetype}>
                                  <p>{this.state.message}</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        );
    }
}

export default DistributorOverseaEdit;