import React, { Component } from 'react';
import Adminheader from "../../common/adminHeader";
import Adminsidebar from "../../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as managePageService from "../../../ApiServices/admin/managePages";
import CKEditor from 'ckeditor4-react';
import { apiUrl, bootstrap , siteUrl } from "../../../../src/config.json";

class FooterEditPage extends Component {

    constructor(props) {
        super(props);
        this.onEditorSection1 = this.onEditorSection1.bind(this);
    }

    state = {
        cimonPages: [],
        uploadedImages: [],
        parentPages: [
            { name: 'One', id: 1 },
            { name: 'Two', id: 2 },
            { name: 'Three', id: 3 },
            { name: 'four', id: 4 }
        ],
        errors: {},
        validated: false,
        pageStatus: true,
        submitStatus: false,
        spinner: true,
        message: "",
        message1:"",
        responsetype: "",
        editorState: "",
        bannerContent: '',

        videofile: "",

        videourl: '',

        section1: '',
        is_preview:"",
    };


    onEditorSection1(evt) {
        this.setState({
            section1: evt.editor.getData()
        });
    }





    /*****************Joi validation schema**************/
    schema = {
        title: Joi.string()
            .required()
            .error(() => {
                return {
                    message: "This field is Required",
                };
            }),
        _id: Joi.optional().label("id"),
        slug: Joi.string(),
        editor_content: Joi.string(),

    };
    /*****************end Joi validation schema**************/

    /*****************INPUT HANDLE CHANGE **************/

    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const cimonPages = { ...this.state.cimonPages };
        const errorMessage = this.validateProperty(input);
        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        cimonPages[input.name] = input.value;
        this.setState({ cimonPages, errors });

    };

    /*****************JOI VALIDATION CALL**************/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };


    // /***************** FORM SUBMIT **************/



    componentDidMount = async () => {

        const slug = this.props.slug;
        const id = this.props.id;
    
        this.setState({
            spinner: true,
            slug: slug
        });


        try {
            const response = await managePageService.getSinglePageById(id);
            
            if (response.data.status === 1) {
                let newPagearray = {
                    //  slug: response.data.data.page_data.slug,
                    slug: this.state.slug,
                    title: response.data.data.page_data.title,
                };
                this.setState({ cimonPages: newPagearray });
                this.setState({ is_preview: response.data.data.page_data.is_preview });


                if (response.data.data.page_data.hasOwnProperty('content')) {

                    let cimonPages = { ...this.state.cimonPages };





                    setTimeout(() => {
                        this.setState({

                            cimonPages: cimonPages,
                            section1: response.data.data.page_data.content.section1 ? response.data.data.page_data.content.section1 : '',
                            spinner: false
                        });

                    }, 2000);


                }
                this.setState({ spinner: false });

            } else {
                this.setState({ spinner: false });
                this.setState({
                    pageStatus: false,
                    message: response.data.message,
                    responsetype: "error",
                });
            }

        } catch (err) {
            this.setState({
                message: 'something went wrong',
                responsetype: "error",
                spinner: false
            });
        }


    }


    handleSubmit = async () => {
        const cimonPages = { ...this.state.cimonPages };
        const errors = { ...this.state.errors };
        //  cimonPages["editor_content"] = this.state.editorState;

        let result = Joi.validate(cimonPages, this.schema);

        if (result.error) {
            
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({
                errors: errors
            })
        } else {
            
            if (errors.length > 0) {
                this.setState({
                    errors: errors
                })
            } else {
                this.setState({ spinner: true });

                const page_data = {
                    title: this.state.cimonPages.title,
                    slug: this.state.slug,

                    // attributes: SetAttributes,
                    content: {

                        section1: this.state.section1,

                    }

                }


                
                this.setState({ submit_status: true });


                try {
                    const response = await managePageService.updatePagedata(page_data);
                    if (response.data.status === 1) {
                        this.setState({
                            submitStatus: false,
                            message: response.data.message,
                            responsetype: "success",
                            spinner: false
                        });

                    } else {
                        this.setState({
                            submitStatus: false,
                            spinner: false,
                            message: response.data.message,
                            responsetype: "error",
                        });

                    }

                } catch (err) {
                    this.setState({
                        message: 'something went wrong',
                        responsetype: "error",
                        spinner: false
                    });
                }
            }
        }
    };

    handlePreview = async () => {
        const id = this.props.id;
        const cimonPages = { ...this.state.cimonPages };
        const errors = { ...this.state.errors };
        //  cimonPages["editor_content"] = this.state.editorState;
        let result = Joi.validate(cimonPages, this.schema);

        if (result.error) {
            
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({
                errors: errors
            })
        } else {
            
            if (errors.length > 0) {
                this.setState({
                    errors: errors
                })
            } else {
                this.setState({ spinner: true });

                const page_data = {
                   
                    slug: this.state.slug,

                    // attributes: SetAttributes,
                    preview: {
                        title: this.state.cimonPages.title,
                        section1: this.state.section1,

                    }

                }


                
                this.setState({ submit_status: true });


                try {
                    const response = await managePageService.previewPagedata(page_data);
                    if (response.data.status === 1) {

                        this.setState({
                            submitStatus: false,
                            message1: response.data.message,
                            responsetype: "success",
                            spinner: false
                        });
                        setTimeout(() => {
                            const win = window.open("/cimon-gate/admin/preview/" + id, "_blank");
                            win.focus();
                            // this.props.history.push({
                            //   pathname: "/admin/preview/" + id,
                            // });
                          }, 2000);

                    } else {
                        this.setState({
                            submitStatus: false,
                            spinner: false,
                            message1: response.data.message,
                            responsetype: "error",
                        });

                    }

                } catch (err) {
                    this.setState({
                        message1: 'something went wrong',
                        responsetype: "error",
                        spinner: false
                    });
                }
            }
        }
    };

    handleheadingChange = idx => evt => {

        const newAttributes = this.state.attributes.map((attribute, sidx) => {
            
            if (idx !== sidx) return attribute;
            return { ...attribute, heading: evt.target.value };
        });

        this.setState({ attributes: newAttributes });
    };

    handlesubheadingChange = idx => evt => {

        const newAttributes = this.state.attributes.map((attribute, sidx) => {
            if (idx !== sidx) return attribute;
            return { ...attribute, subheading: evt.editor.getData() };
        });

        this.setState({ attributes: newAttributes });
    };


    render() {

        return (<React.Fragment>
            <div className="container-fluid admin-body">
                <div className="admin-row">
                    <div className="col-md-2 col-sm-12 sidebar">
                        <Adminsidebar props={this.props} />
                    </div>

                    <div className="col-md-10 col-sm-12  content">
                        <div className="row content-row">
                            <div className="col-md-12  header">
                                <Adminheader props={this.props} />
                            </div>
                            <div className="col-md-12  contents backgroundoverlay  home-inner-content pt-4 pb-4 pr-4" >
                                <React.Fragment>
                                    <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                                        <ReactSpinner type="border" color="primary" size="10" />
                                    </div>
                                </React.Fragment>
                                <div className="row addpage-form-wrap">
                                    <div className="addpage-form">
                                        <div className="col-md-12 ">

                                        {this.state.is_preview === true?(<div className="col-md-12 ">
                                            <div className="form-group1 preview_grp">
                                            <span className="input-group-btn">
                                          
                                               <button onClick={this.handlePreview} className="btn btn-info  pull-right">Preview</button>
                                               {this.state.message1 !== "" ? (
                                                <div className="tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message1}</p>
                                                </div></div>
                                            ) : null} 
                                            
                                             </span>

                                               
                                            </div>
                                           
                                          
                                            
                                        </div>):""}

                                      
                                            <div className="form-group">
                                                <label htmlFor="">Edit Page</label>
                                                <input name="title" value={this.state.cimonPages.title} onChange={this.handleChange} type="text" placeholder="Add title" className="form-control" />
                                                {this.state.errors.title ? (<div className="error text-danger">
                                                    {this.state.errors.title}
                                                </div>
                                                ) : (
                                                        ""
                                                    )}
                                            </div>
                                        </div>

                                        <div className="col-md-12 ">
                                            <div className="form-group">
                                                <label htmlFor="">Section 1</label>
                                                <CKEditor
                                                    data={this.state.section1}
                                                     onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                                        extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                        contentsCss: bootstrap,
                                                        allowedContent: true,
                                                        filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                                    }}

                                                    onInit={
                                                        editor => {
                                                        }
                                                    }
                                                    onChange={this.onEditorSection1}
                                                />
                                            </div>
                                        </div>

                                        <div className="col-md-12 ">
                                            <div className="form-inline form float-right form-btn-wrap">
                                                {/* <Link to="#">Move to Trash</Link> */}
                                                <span className="input-group-btn">
                                                    <button onClick={this.handleSubmit} className="btn btn-info">Update</button>
                                                </span>

                                           
                                            </div>
                                            {this.state.message !== "" ? (
                                                <div className="form-group1 tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message}</p>
                                                </div></div>
                                            ) : null}

                                        </div>
                                       
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
        );
    }
}

export default FooterEditPage;