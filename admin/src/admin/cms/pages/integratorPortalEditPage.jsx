import React, { Component } from 'react';
import Adminheader from "../../common/adminHeader";
import Adminsidebar from "../../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as managePageService from "../../../ApiServices/admin/managePages";
import CKEditor from 'ckeditor4-react';
import { apiUrl, bootstrap } from "../../../../src/config.json";

import MultipleUploadImages from "../../common/multipleuploadimages";
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

class IntegratorPortalEditPage extends Component {

    state = {
        cimonPages: [],
        uploadedImages: [],
        parentPages: [
            { name: 'One', id: 1 },
            { name: 'Two', id: 2 },
            { name: 'Three', id: 3 },
            { name: 'four', id: 4 }
        ],
        errors: {},
        validated: false,
        pageStatus: true,
        submitStatus: false,
        spinner: true,
        message: "",
        responsetype: "",
        editorState: "",
        bannerContent: '',

        videofile: "",

        videourl: '',

      
        attributes: [],
        upload_image_status: [],
        productSilder: [],
        product_image_status: []
    };

   

    onvideoupload = async (value, item) => {
        let errors = { ...this.state.errors };

        try {

            if (item === "errors") {
                errors["videourl"] = value;
                this.setState({
                    errors: errors,
                });
                
            } else {
                this.setState({ download_1_status: true });
                let file = value.target.files[0];
                const response1 = await managePageService.uploadVideo(
                    file
                );
                if (response1.data.status === 1) {
                    
                    let cimonPages = { ...this.state.cimonPages };
                    let filepath = { "image_name": response1.data.data.file_name, "image_url": response1.data.data.file_location };
                    cimonPages['videourl'] = response1.data.data.file_location
                    this.setState({
                        videofile: filepath,
                        cimonPages: cimonPages
                    });
                    
                } else {
                    this.setState({
                        message: response1.data.message,
                        responsetype: "error",
                    });
                }
                this.setState({ download_1_status: false });
            }

        } catch (err) {
            errors["videourl"] = err.message;
            this.setState({
                message: 'something went wrong',
                responsetype: "error",
                errors: errors,
                download_1_status: false
            });
        }


    };


    /*****************Joi validation schema**************/
    schema = {
        title: Joi.string()
            .required()
            .error(() => {
                return {
                    message: "This field is Required",
                };
            }),
            page_title: Joi.string()
            .required()
            .error(() => {
              return {
                message: "This field is Required",
              };
            }),
        _id: Joi.optional().label("id"),
        slug: Joi.string(),
        editor_content: Joi.string(),
  //      videourl: Joi.string().allow(''),
        title1:Joi.string().required(),
        title2: Joi.string().required(),
        title3: Joi.string().required(),
        title4: Joi.string().required(),
    };
    /*****************end Joi validation schema**************/

    /*****************INPUT HANDLE CHANGE **************/

    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const cimonPages = { ...this.state.cimonPages };
        const errorMessage = this.validateProperty(input);
        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        cimonPages[input.name] = input.value;
        this.setState({ cimonPages, errors });

    };

    /*****************JOI VALIDATION CALL**************/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };


    // /***************** FORM SUBMIT **************/



    componentDidMount = async () => {

        const slug = this.props.slug;
        const id = this.props.id;
    
        this.setState({
            spinner: true,
            slug: slug
        });

        try {

            const response = await managePageService.getSinglePageById(id);
            let cimonPages = { ...this.state.cimonPages };
            if (response.data.status === 1) {
                let newPagearray = {
                    //  slug: response.data.data.page_data.slug,
                    slug: this.state.slug,
                    title: response.data.data.page_data.title,
                    page_title: response.data.data.page_data.page_title,
                };
                this.setState({ cimonPages: newPagearray });
                if (response.data.data.page_data.hasOwnProperty('content')) {
                    let cimonPages = { ...this.state.cimonPages };
                    cimonPages['title1'] = response.data.data.page_data.content.title1 ? response.data.data.page_data.content.title1 : ''
                    cimonPages['title2'] = response.data.data.page_data.content.title2 ? response.data.data.page_data.content.title2 : ''
                    cimonPages['title3'] = response.data.data.page_data.content.title3 ? response.data.data.page_data.content.title3 : ''
                    cimonPages['title4'] = response.data.data.page_data.content.title4 ? response.data.data.page_data.content.title4 : ''
                   
                    setTimeout(() => {

                        this.setState({
                            cimonPages: cimonPages,
                            attributes: response.data.data.page_data.content.bannerSlider ? response.data.data.page_data.content.bannerSlider : [],
                            section1: response.data.data.page_data.content.section1 ? response.data.data.page_data.content.section1 : '',
                            spinner: false
                        });


                        let status_array = [];
                        this.state.attributes.map((attribute, sidx) => {
                            
                            status_array.push(false);
                        });

                        this.setState({
                            upload_image_status: status_array,
                        });

                    }, 2000);


                }
                this.setState({ spinner: false });

               
            } else {
                this.setState({ spinner: false });
                this.setState({
                    pageStatus: false,
                    message: response.data.message,
                    responsetype: "error",
                });
            }

        } catch (err) {
            this.setState({
                message: 'something went wrong',
                responsetype: "error",
                spinner: false
            });
        }



    }


    handleSubmit = async () => {
        const cimonPages = { ...this.state.cimonPages };
        const errors = { ...this.state.errors };
        //  cimonPages["editor_content"] = this.state.editorState;

        let result = Joi.validate(cimonPages, this.schema);

        if (result.error) {
            
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({
                errors: errors
            })
        } else {
            
            if (errors.length > 0) {
                this.setState({
                    errors: errors
                })
            } else {
                this.setState({ spinner: true });

                const page_data = {
                    title: this.state.cimonPages.title,
                    slug: this.state.slug,
                    page_title: this.state.cimonPages.page_title,
                    id : this.props.id,

                    // attributes: SetAttributes,
                    content: {
                        title1: this.state.cimonPages.title1,
                        title2: this.state.cimonPages.title2,
                        title3: this.state.cimonPages.title3,
                        title4: this.state.cimonPages.title4,
                        bannerSlider: this.state.attributes.filter((s, sidx) => s.heading !== ''),
                   
                    }

                }


                try {
                    
                    this.setState({ submit_status: true });
                    const response = await managePageService.updatePagedata(page_data);
                    if (response.data.status === 1) {
                        this.setState({
                            submitStatus: false,
                            message: response.data.message,
                            responsetype: "success",
                            spinner: false
                        });

                    } else {
                        this.setState({
                            submitStatus: false,
                            spinner: false,
                            message: response.data.message,
                            responsetype: "error",
                        });

                    }

                } catch (err) {
                    this.setState({
                        message: 'something went wrong',
                        responsetype: "error",
                        spinner: false,
                        submit_status: false
                    });
                }



            }
        }
    };


    handleheadingChange = idx => evt => {

        const newAttributes = this.state.attributes.map((attribute, sidx) => {
            
            if (idx !== sidx) return attribute;
            return { ...attribute, heading: evt.target.value };
        });

        this.setState({ attributes: newAttributes });
    };


    handlesubheadingChange = idx => evt => {


        const newAttributes = this.state.attributes.map((attribute, sidx) => {
            if (idx !== sidx) return attribute;
            return { ...attribute, subheading: evt.editor.getData() };
        });

        this.setState({ attributes: newAttributes });
    };



    onMultipleupload = async (value, item, idx) => {
        let errors = { ...this.state.errors };

        try {

            if (item === "errors") {
                errors["videourl"] = value;
                this.setState({
                    errors: errors,
                });
                
            } else {

                const staus = [...this.state.upload_image_status];
                staus[idx] = true;
                this.setState({ upload_image_status: staus });


                let file = value.target.files[0];
                const response1 = await managePageService.uploadVideo(
                    file
                );
                if (response1.data.status === 1) {
                    const newAttributes = this.state.attributes.map((attribute, sidx) => {
                        if (idx !== sidx) return attribute;
                        return { ...attribute, image: response1.data.data.file_location };
                    });

                    this.setState({ attributes: newAttributes });
                } else {
                    this.setState({
                        message: response1.data.message,
                        responsetype: "error",
                    });
                }
                //  this.setState({ download_1_status: false });
                staus[idx] = false;
                this.setState({ upload_image_status: staus });


            }

        } catch (err) {
            this.setState({
                message: 'something went wrong',
                responsetype: "error",
                download_1_status: false
            });
        }


    };


    handleAddAttributes = () => {


        this.setState({
            attributes: this.state.attributes.concat([{ heading: "", subheading: "", image: '' }]),
           upload_image_status: this.state.upload_image_status.concat([false])
        });
    };

    handleRemoveAttributes = idx => () => {
        this.setState({
            attributes: this.state.attributes.filter((s, sidx) => idx !== sidx),
            upload_image_status: this.state.upload_image_status.filter((s, sidx) => idx !== sidx)
        });
    };


    render() {
            console.log('attributes', this.state.attributes);
        return (<React.Fragment>
            <div className="container-fluid admin-body">
                <div className="admin-row">
                    <div className="col-md-2 col-sm-12 sidebar">
                        <Adminsidebar props={this.props} />
                    </div>

                    <div className="col-md-10 col-sm-12  content">
                        <div className="row content-row">
                            <div className="col-md-12  header">
                                <Adminheader props={this.props} />
                            </div>
                            <div className="col-md-12  contents backgroundoverlay  home-inner-content pt-4 pb-4 pr-4" >
                                <React.Fragment>
                                    <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                                        <ReactSpinner type="border" color="primary" size="10" />
                                    </div>
                                </React.Fragment>
                                <div className="row addpage-form-wrap">
                                    <div className="addpage-form">
                                        <div className="col-md-12 ">
                                            <div className="form-group">
                                                <label htmlFor="">Edit Page</label>
                                                <input name="title" value={this.state.cimonPages.title} onChange={this.handleChange} type="text" placeholder="Add title" className="form-control" />
                                                {this.state.errors.title ? (<div className="error text-danger">
                                                    {this.state.errors.title}
                                                </div>
                                                ) : (
                                                        ""
                                                    )}
                                            </div>
                                        </div>

                                            <div className="col-md-12 ">
                                            <div className="form-group">
                                            <label htmlFor="">Page Title</label>
                                            <input
                                            name="page_title"
                                            value={this.state.cimonPages.page_title}
                                            onChange={this.handleChange}
                                            type="text"
                                            placeholder="Add Page title"
                                            className="form-control"
                                            />
                                            {this.state.errors.page_title ? (
                                            <div className="error text-danger">
                                            {this.state.errors.page_title}
                                            </div>
                                            ) : (
                                            ""
                                            )}
                                            </div>
                                            </div>


                                        <div className="col-md-12 ">
                                            <div className="form-group">
                                                <label htmlFor="">Banner</label>

                                                <div className="banner-section">
                                                    {
                                                        this.state.attributes ?

                                                            this.state.attributes.map((attribute, idx) => (
                                                                <div className="banner-single">
                         <button class="rem-btn btn btn-danger"
                                                                        onClick={this.handleRemoveAttributes(idx)}
                                                                        type="button"><span aria-hidden="true">×</span></button>


                                                                    <Accordion>
                                                                        <Card>
                                                                            <Card.Header>
                                                                                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                                                                    {attribute.heading ? attribute.heading : `slide #${idx + 1}`}
                                                                                </Accordion.Toggle>
                                                                            </Card.Header>
                                                                            <Accordion.Collapse eventKey="0">
                                                                                <Card.Body>
                                                                                    <input
                                                                                        onChange={this.handleheadingChange(idx)}
                                                                                        type="text"
                                                                                        value={attribute.heading ? attribute.heading : ""}
                                                                                        placeholder="Heading"
                                                                                        className="form-control" />


                                                                                    <CKEditor
                                                                                        data={attribute.subheading}
                                                                                        config={
                                                                                            {
                                                                                                extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                                                                contentsCss: bootstrap,
                                                                                                height: '6em',
                                                                                                allowedContent: true,
                                                                                                filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                                                                            }
                                                                                        }
                                                                                        onInit={
                                                                                            editor => {
                                                                                            }
                                                                                        }
                                                                                        onChange={this.handlesubheadingChange(idx)}
                                                                                    />

                                                                                    <MultipleUploadImages
                                                                                        idx={idx}
                                                                                        imagestatus={this.state.upload_image_status[idx]}
                                                                                        onMultipleupload={this.onMultipleupload}
                                                                                        value={
                                                                                            (attribute.image ? attribute.image : "")
                                                                                        }
                                                                                    // errors={this.state.errors}
                                                                                    />
                                                                                </Card.Body>
                                                                            </Accordion.Collapse>
                                                                        </Card>
                                                                    </Accordion>






                                                                </div>
                                                            )) : ''
                                                    }


<button
                                                        type="button"
                                                        onClick={this.handleAddAttributes}
                                                        className="add-btn btn btn-success"
                                                    > + </button>

                                                </div>



                                            </div>
                                        </div>

                         

                                        <div className="col-md-12 ">
                                            <div className="form-group">
                                                <label htmlFor="">Title 1</label>
                                                <input name="title1" value={this.state.cimonPages.title1} onChange={this.handleChange} type="text" placeholder="Title 1" className="form-control" />
                                                {this.state.errors.title1 ? (<div className="error text-danger">
                                                    {this.state.errors.title1}
                                                </div>
                                                ) : (
                                                        ""
                                                    )}
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="">Title 2</label>
                                                <input name="title2" value={this.state.cimonPages.title2} onChange={this.handleChange} type="text" placeholder="Title 2" className="form-control" />
                                                {this.state.errors.title2 ? (<div className="error text-danger">
                                                    {this.state.errors.title2}
                                                </div>
                                                ) : (
                                                        ""
                                                    )}
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="">Title 3</label>
                                                <input name="title3" value={this.state.cimonPages.title3} onChange={this.handleChange} type="text" placeholder="Title 3" className="form-control" />
                                                {this.state.errors.title3 ? (<div className="error text-danger">
                                                    {this.state.errors.title3}
                                                </div>
                                                ) : (
                                                        ""
                                                    )}
                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="">Title 4</label>
                                                <input name="title4" value={this.state.cimonPages.title4} onChange={this.handleChange} type="text" placeholder="Title 4" className="form-control" />
                                                {this.state.errors.title4 ? (<div className="error text-danger">
                                                    {this.state.errors.title4}
                                                </div>
                                                ) : (
                                                        ""
                                                    )}
                                            </div>
                                        </div>






                                        <div className="col-md-12 ">
                                            <div className="form-inline form float-right form-btn-wrap">
                                                {/* <Link to="#">Move to Trash</Link> */}
                                                <span className="input-group-btn">
                                                    <button onClick={this.handleSubmit} className="btn btn-info">Update</button>
                                                </span>
                                            </div>
                                            {this.state.message !== "" ? (
                                                <div className="tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message}</p>
                                                </div></div>
                                            ) : null} 
                                          
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
        );
    }
}

export default IntegratorPortalEditPage;