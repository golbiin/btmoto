import React, { Component } from 'react';
import Adminheader from "../../common/adminHeader";
import Adminsidebar from "../../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as managePageService from "../../../ApiServices/admin/managePages";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl, bootstrap} from "../../../../src/config.json";
import MultipleUploadImages from "../../common/multipleuploadimages";
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

class CertificatesEdit  extends Component {
    constructor(props) {
        super(props);  
       
        this.onEditorSection1 = this.onEditorSection1.bind( this );
    }
    state = {
        cimonPages: [],
        uploadedImages:[],
        tabattributes: [],
        parentPages: [
          { name: 'One', id: 1 },
          { name: 'Two', id: 2 },
          { name: 'Three', id: 3 },
          { name: 'four', id: 4 }
        ],
        errors: {},
        validated: false,
        pageStatus: true,
        submitStatus: false,
        spinner : true,
        message: "",
        message1:"",
        responsetype: "",
        editorState:"",
        bannerContent : '',
        
        videofile: "",

        videourl : '',

        section1 : '',
          section1sub1 : '',
        section2 : '',
        section3 : '',
        section4 : '',
        section5 : '',
        section6 : '',
        section7 : '',
        section8 : '',
        section9 : '',
        section10 : '',
        section11 : '',
        section12 : '',
        section13 : '',
        section14 : '',
        section15 : '',
        section16 : '',
        attributes: [],
        upload_image_status: [],
        productSilder: [],
        product_image_status : [],
        is_preview:""
      };


    onvideoupload = async (value, item) => {
        let errors = { ...this.state.errors };

        try {

        if (item === "errors") {
          errors["videourl"] = value;
          this.setState({
            errors: errors,
          });
          
        } else {
          this.setState({ download_1_status: true });
          let file = value.target.files[0];
          const response1 = await managePageService.uploadVideo(
            file
          );
          if (response1.data.status === 1) {
            
            let cimonPages = { ...this.state.cimonPages };
            let filepath = { "image_name" : response1.data.data.file_name , "image_url" : response1.data.data.file_location};
            cimonPages['videourl'] = response1.data.data.file_location
            this.setState({ videofile : filepath  ,
                cimonPages : cimonPages
            });
            
          } else {
            this.setState({
              message: response1.data.message,
              responsetype: "error",
            });
          }
          this.setState({ download_1_status: false });
        }

    } catch (err) {
        errors["videourl"] = err.message;
        this.setState({ 
            message: 'something went wrong',
            responsetype: "error",
            errors : errors,
            download_1_status: false 
        });
    }


      };
  

    /*****************Joi validation schema**************/  
    schema = {
        title: Joi.string()
        .required()
        .error(() => {
        return {
            message: "This field is Required",
        };
        }),
        page_title: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        meta_tag: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        meta_desc: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        _id: Joi.optional().label("id"),
        slug : Joi.string(),
        editor_content: Joi.string(),
        videourl : Joi.string().allow(''),
        tabattributes : Joi.any().allow('')

    };
    /*****************end Joi validation schema**************/

    /*****************INPUT HANDLE CHANGE **************/

    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const cimonPages = { ...this.state.cimonPages };
        const errorMessage = this.validateProperty(input);
        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        cimonPages[input.name] = input.value;
        this.setState({ cimonPages, errors });
        
    };

    /*****************JOI VALIDATION CALL**************/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };
    

    // /***************** FORM SUBMIT **************/

    

    componentDidMount = async () => {
        
        const slug = this.props.slug;
        const id = this.props.id;
        console.log('componentDidMount Certficate page ' ,id);
        this.setState({ spinner: true  ,
        slug : slug,
        id:id
        });

try{


        const response = await managePageService.getSinglePageById(id);
        let cimonPages = { ...this.state.cimonPages };
        if(response.data.status === 1){
            let newPagearray = {
                slug: response.data.data.page_data.slug,
                // slug: this.state.slug,
                title: response.data.data.page_data.title,
                page_title: response.data.data.page_data.page_title,
                meta_tag: response.data.data.page_data.meta_tag,
                meta_desc: response.data.data.page_data.meta_desc,
                   };
            this.setState({ cimonPages : newPagearray });
            this.setState({ section1 : response.data.data.page_data.content.section1 });
            this.setState({  tabattributes : response.data.data.page_data.content.tabattributes ?  response.data.data.page_data.content.tabattributes  : []});
           
            if (response.data.data.page_data.hasOwnProperty('content') ) {
                
                let cimonPages = { ...this.state.cimonPages };

                cimonPages['videourl'] = response.data.data.page_data.content.videourl ?response.data.data.page_data.content.videourl : '' 

                setTimeout(() => {
                    this.setState({
                        
                        cimonPages : cimonPages,
                        attributes: response.data.data.page_data.content.bannerSlider ? response.data.data.page_data.content.bannerSlider : [],
                      
                     
                        spinner: false
                    });


                    let status_array = [];
                    this.state.attributes.map((attribute, sidx) => {
                        
                        status_array.push(false);
                    });


                    let status_pdt_array = [];
                    this.state.productSilder.map((attribute, sidx) => {
                        status_pdt_array.push(false);
                    });


                    this.setState({
                        upload_image_status: status_array,
                        product_image_status: status_pdt_array
                    });

                    this.setState({ is_preview: response.data.data.page_data.is_preview });


                }, 2000);


            }
            this.setState({ spinner: false });
 
        }else{
            this.setState({ spinner: false });
            this.setState({
                pageStatus: false,
                message: response.data.message,
                responsetype: "error",
            });
        }

    } catch (err) {
        this.setState({
            message: 'something went wrong',
            responsetype: "error",
            spinner: false
        });
    }


        
    }


    handleSubmit = async () => {
        const cimonPages = { ...this.state.cimonPages };
        const errors = { ...this.state.errors };
      //  cimonPages["editor_content"] = this.state.editorState;

        let result = Joi.validate(cimonPages, this.schema);
        console.log('results are :----',result);
        if (result.error) {
            
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({
                errors: errors  
            })
        } else {
            
            if (errors.length > 0) {
                this.setState({
                    errors: errors  
                })
            }else{
                this.setState({ spinner: true });

                const page_data = {
                    id: this.state.id,
                    title: this.state.cimonPages.title,
                    slug :this.state.cimonPages.slug,
                    page_title: this.state.cimonPages.page_title,
                    meta_tag: this.state.cimonPages.meta_tag,
                    meta_desc: this.state.cimonPages.meta_desc,
                    content : {
                        section1 : this.state.section1,
                        tabattributes : this.state.tabattributes.filter((s, sidx) => s.name !== ''),
                        bannerSlider: this.state.attributes.filter((s, sidx) => s.heading !== ''),
                   }
                   
                  }

try {
                
                this.setState({ submit_status: true });
                const response = await managePageService.updatePagedata(page_data);
                if (response.data.status === 1) {
                    this.setState({
                    submitStatus: false,
                    message: response.data.message,
                    responsetype: "success",
                    spinner: false 
                    });


                    setTimeout(
                        () =>
                          this.props.history.push({
                            pathname: '/admin/pages',
                          }),
                        2000
                      );
                      this.setState({ spinner: false });
                   
                } else {
                    this.setState({
                    submitStatus: false,
                    spinner: false,
                    message: response.data.message,
                    responsetype: "error",
                    });
                   
                }

} catch (err) {
    this.setState({
        message: 'something went wrong',
        responsetype: "error",
        spinner: false ,
        submit_status : false
    });
}



            }
        }
    };
    
    handlePreview = async () => {
        const id = this.props.id;
        const cimonPages = { ...this.state.cimonPages };
        const errors = { ...this.state.errors };
      //  cimonPages["editor_content"] = this.state.editorState;

        let result = Joi.validate(cimonPages, this.schema);

        if (result.error) {
            
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
            this.setState({
                errors: errors  
            })
        } else {
            
            if (errors.length > 0) {
                this.setState({
                    errors: errors  
                })
            }
            else
            {
                this.setState({ spinner: true });

                const page_data = {
                 
                    slug: this.state.slug,
                 
                     preview : {
                        title: this.state.cimonPages.title,
                         page_title: this.state.cimonPages.page_title,               
                         section1 : this.state.section1,
                        tabattributes : this.state.tabattributes.filter((s, sidx) => s.name !== ''),
                        bannerSlider: this.state.attributes.filter((s, sidx) => s.heading !== ''),
                        meta_tag: this.state.cimonPages.meta_tag,
                        meta_desc: this.state.cimonPages.meta_desc,
                   }
                   
                  }

try {
                
                this.setState({ submit_status: true });
                const response = await managePageService.previewPagedata(page_data);
                if (response.data.status === 1) {
                    this.setState({
                    submitStatus: false,
                    message1: response.data.message,
                    responsetype: "success",
                    spinner: false 
                    });
                    setTimeout(() => {
                        const win = window.open("/cimon-gate/admin/preview/" + id, "_blank");
                        win.focus();
                        // this.props.history.push({
                        //   pathname: "/admin/preview/" + id,
                        // });
                      }, 2000);
                   
                } else {
                    this.setState({
                    submitStatus: false,
                    spinner: false,
                    message1: response.data.message,
                    responsetype: "error",
                    });
                   
                }

} catch (err) {
    this.setState({
        message1: 'something went wrong',
        responsetype: "error",
        spinner: false ,
        submit_status : false
    });
}



            }
        }
    };
    
    handleheadingChange = idx => evt => {

        const newAttributes = this.state.attributes.map((attribute, sidx) => {
            
            if (idx !== sidx) return attribute;
            return { ...attribute, heading: evt.target.value };
        });

        this.setState({ attributes: newAttributes });
    };

    handlesubheadingChange = idx => evt => {

        const newAttributes = this.state.attributes.map((attribute, sidx) => {
            if (idx !== sidx) return attribute;
            return { ...attribute, subheading: evt.editor.getData() };
        });

        this.setState({ attributes: newAttributes });
    };




    handleAddAttributes = () => {


        this.setState({
            attributes: this.state.attributes.concat([{ heading: "", subheading: "", image: '' }]),
            upload_image_status: this.state.upload_image_status.concat([false])
        });
    };

    handleRemoveAttributes = idx => () => {
        this.setState({
            attributes: this.state.attributes.filter((s, sidx) => idx !== sidx),
            upload_image_status: this.state.upload_image_status.filter((s, sidx) => idx !== sidx)
        });
    };


    onMultipleupload = async (value, item, idx) => {
        let errors = { ...this.state.errors };
        try {

            if (item === "errors") {
                errors["videourl"] = value;
                this.setState({
                    errors: errors,
                });
                
            } else {

                const staus = [...this.state.upload_image_status];
                staus[idx] = true;
                this.setState({ upload_image_status: staus });


                let file = value.target.files[0];
                const response1 = await managePageService.uploadVideo(
                    file
                );
                if (response1.data.status === 1) {
                    const newAttributes = this.state.attributes.map((attribute, sidx) => {
                        if (idx !== sidx) return attribute;
                        return { ...attribute, image: response1.data.data.file_location };
                    });

                    this.setState({ attributes: newAttributes });
                } else {
                    this.setState({
                        message: response1.data.message,
                        responsetype: "error",
                    });
                }
                //  this.setState({ download_1_status: false });
                staus[idx] = false;
                this.setState({ upload_image_status: staus });


            }

        } catch (err) {
            this.setState({
                message: 'something went wrong',
                responsetype: "error",
                download_1_status: false
            });
        }


    };
    


    
    onProductupload = async (value, item, idx) => {
        let errors = { ...this.state.errors };
       
        try {

            if (item === "errors") {
                errors["videourl"] = value;
                this.setState({
                    errors: errors,
                });
                
            } else {

                const staus = [...this.state.product_image_status];
                staus[idx] = true;
                this.setState({ product_image_status: staus });
                let file = value.target.files[0];
                const response1 = await managePageService.uploadVideo(
                    file
                );
                if (response1.data.status === 1) {
                    const newAttributes = this.state.productSilder.map((silder, sidx) => {
                        if (idx !== sidx) return silder;
                        return { ...silder, image: response1.data.data.file_location };
                    });

                    this.setState({ productSilder: newAttributes });
                } else {
                    this.setState({
                        message: response1.data.message,
                        responsetype: "error",
                    });
                }
                //  this.setState({ download_1_status: false });
                staus[idx] = false;
                this.setState({ product_image_status: staus });

            }

        } catch (err) {
            this.setState({
                message: 'something went wrong',
                responsetype: "error",
                download_1_status: false
            });
        }


    };
    handleAddProductSilder = () => {
        this.setState({
            productSilder: this.state.productSilder.concat([{ image: '' }]),
            product_image_status: this.state.product_image_status.concat([false])
        });
    };

    handleRemoveProductSilder = idx => () => {
        this.setState({
            productSilder: this.state.productSilder.filter((s, sidx) => idx !== sidx),
            product_image_status: this.state.product_image_status.filter((s, sidx) => idx !== sidx)
        });
    };


    /* additional tabs */
handleTabAttributesNameChange = (idx) => (evt) => {
    const newAttributes = this.state.tabattributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute;
      return { ...attribute, name: evt.target.value };
    });
    this.setState({ tabattributes: newAttributes });
  };
  
  handleTabAttributesDescChange= (idx) => (evt) => { 
    const newAttributes = this.state.tabattributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute;
      return { ...attribute, description: evt.editor.getData() };
    });
    this.setState({ tabattributes: newAttributes });
  }
  handleAddTabs = () => {
    this.setState({
      tabattributes: this.state.tabattributes.concat([
        { name: "", description: "" },
      ]),
    });
  };
  
  handleRemoveTabAttributes = (idx) => () => {
    this.setState({
      tabattributes: this.state.tabattributes.filter((s, sidx) => idx !== sidx),
    });
  };
  
  
  
  
    /* handling product attributes */
    handleAttributesNameChange = (idx) => (evt) => {
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx !== sidx) return attribute;
        return { ...attribute, Attribute_id: evt.target.value };
      });
  
      this.setState({ attributes: newAttributes });
    };
  
    onEditorSection1( evt ) {
		this.setState( {
			section1 : evt.editor.getData()
		} );
	}

    render() { 


        return ( <React.Fragment>
            <div className="container-fluid admin-body">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div>
               
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents backgroundoverlay  home-inner-content pt-4 pb-4 pr-4" > 
                    <React.Fragment>
                              <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                                    <ReactSpinner type="border" color="primary" size="10" />
                              </div>
                            </React.Fragment>  
                     <div  className="row addpage-form-wrap">
                         <div  className="addpage-form">
                         {this.state.is_preview === true?(<div className="col-md-12 ">
                                            <div className="form-group1 preview_grp">
                                            <span className="input-group-btn">
                                          
                                               <button onClick={this.handlePreview} className="btn btn-info  pull-right">Preview</button>
                                               {this.state.message1 !== "" ? (
                                                <div className="tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message1}</p>
                                                </div></div>
                                            ) : null} 
                                                   
                                                   </span>

                                                                                                            
                                                                </div>



                                                                </div>):""}

                            <div className="col-md-12 ">

                                <div className="form-group">
                                    <label htmlFor="">Edit Page</label>
                                    <input name="title"  value={this.state.cimonPages.title} onChange={this.handleChange} type="text" placeholder="Add title" className="form-control"/>
                                    {this.state.errors.title ? (<div className="error text-danger">
                                            {this.state.errors.title}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                            </div>

                  <div className="col-md-12 ">
                        <div className="form-group">
                          <label htmlFor="">Page Title</label>
                          <input
                            name="page_title"
                            value={this.state.cimonPages.page_title}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Page title"
                            className="form-control"
                          />
                          {this.state.errors.page_title ? (
                            <div className="error text-danger">
                              {this.state.errors.page_title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Meta tag</label>
                          <input
                            name="meta_tag"
                             value={this.state.cimonPages.meta_tag}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Meta Tag"
                            className="form-control"
                          />
                          {this.state.errors.meta_tag ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_tag}
                            </div>
                          ) : (
                            ""
                          )}
                          
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Meta Description</label>
                          <input
                            name="meta_desc"
                           value={this.state.cimonPages.meta_desc}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.meta_desc ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_desc}
                            </div>
                          ) : (
                            ""
                          )}
                          
                        </div>
                        <div className="form-group"> 
                          <label htmlFor="">Page Slug</label>
                          <input
                            name="slug"
                             value={this.state.cimonPages.slug}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Page Slug"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ""
                          )}
                          
                        </div>
                        
                      </div>

                            <div className="col-md-12 ">
                                            <div className="form-group">
                                                <label htmlFor="">Banner</label>



                                                <div className="banner-section">
                                                    {
                                                        this.state.attributes ?

                                                            this.state.attributes.map((attribute, idx) => (
                                                                <div className="banner-single">
                                                                    <button class="rem-btn btn btn-danger"
                                                                        onClick={this.handleRemoveAttributes(idx)}
                                                                        type="button"><span aria-hidden="true">×</span></button>




<Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  {attribute.heading ? attribute.heading : `slide #${idx + 1}`}
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body> 
                <input  
                                                                        onChange={this.handleheadingChange(idx)}
                                                                        type="text"
                                                                        value={attribute.heading ? attribute.heading : ""}
                                                                        placeholder="Heading"
                                                                        className="form-control" />


                                                                    <CKEditor
                                                                        data={attribute.subheading}
                                                                        onBeforeLoad={CKEDITOR => {
                                                                          CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                                                        }
                                                                        }
                                                                        config={
                                                                            {
                                                                              extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                                                contentsCss  : bootstrap,
                                                                                height: '6em',
                                                                                allowedContent: true,
                                                                                filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                                                            }
                                                                        }
                                                                        onInit={
                                                                            editor => {
                                                                            }
                                                                        }
                                                                        onChange={this.handlesubheadingChange(idx)}
                                                                    />

                                                                    <MultipleUploadImages
                                                                        idx={idx}
                                                                        imagestatus={this.state.upload_image_status[idx]}
                                                                        onMultipleupload={this.onMultipleupload}
                                                                        value={
                                                                            (attribute.image ? attribute.image : "")
                                                                        }
                                                                    // errors={this.state.errors}
                                                                    /> 
                </Card.Body>
              </Accordion.Collapse>
              </Card>
            </Accordion>


                                                                   



                                                                </div>
                                                            )) : ''
                                                    }


                                                    <button
                                                        type="button"
                                                        onClick={this.handleAddAttributes}
                                                        className="add-btn btn btn-success"
                                                    > + </button>

                                                </div>



                                            </div>
                               
                               
                                            <div className="col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Section 1</label>  
                                    <CKEditor
                                        data= {this.state.section1}
                                        onBeforeLoad={CKEDITOR => {
                                          CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                        }
                                        }
                                        config={ {
                                          extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            contentsCss  : bootstrap,
                                            allowedContent: true,
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorSection1}
                                    />
                                </div>  
                            </div>

                                            <div class="form-group">
                          <div class="row">
                            <div class="col-md-12 attribute_container ">
                            <div className="upperattributes">
                              <label for="">Additional Tabs</label>
                              
                                   <button
                                      type="button"
                                      onClick={this.handleAddTabs}
                                      className="add-btn btn btn-success"
                                    >
                                    Add New
                                    </button>
                                    </div>
                                    <div className="attributes">
                                      {this.state.tabattributes.map(
                                        (attribute, idx) => (
                                          <div key={idx} className="row pt-2">
                                            <div className="col-md-12 pb-2 pl-0 pr-0 main_loop_sub">
                                              <input
                                                type="text"
                                                className="form-control w-100"
                                                placeholder={`Enter name of tab #${
                                                  idx + 1
                                                }`}
                                                value={attribute.name}
                                                onChange={this.handleTabAttributesNameChange(
                                                  idx
                                                )}
                                              />

                                        

                                               <CKEditor
                                              data={attribute.description}
                                               onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                                extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                allowedContent: true,
                                                filebrowserImageUploadUrl:
                                                  apiUrl + "/admin/upload/imageupload-new",
                                              }}
                                              onInit={(editor) => {}}
                                              
                                              onChange={this.handleTabAttributesDescChange(
                                                idx
                                              )}
                                            />

                                              <button
                                                className="rem-btn btn btn-danger"
                                                type="button"
                                                onClick={this.handleRemoveTabAttributes(
                                                  idx
                                                )}
                                              >
                                                <span aria-hidden="true">
                                                  &times;
                                                </span>
                                              </button>
                                            </div>

                                          </div>
                                        )
                                      )}
                                    </div>

                            </div>
                          </div>
                        </div>
           
                               
                                        </div>
                           
                            <div className="col-md-12 ">
                                <div className="form-inline form float-right form-btn-wrap">
                                    {/* <Link to="#">Move to Trash</Link> */}
                                    <span className="input-group-btn">
                                        <button onClick={this.handleSubmit} className="btn btn-info">Update</button>
                                    </span>
                                </div>
                                {this.state.message !== "" ? (
                                                <div className="tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message}</p>
                                                </div></div>
                                            ) : null} 
                                          
                                            
                            </div>
                        </div>
                     </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment>
          );
    }
}
 
export default CertificatesEdit ;