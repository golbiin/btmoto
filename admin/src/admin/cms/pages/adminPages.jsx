import React, { Component } from 'react'
import Adminheader from '../../common/adminHeader'
import Adminsidebar from '../../common/adminSidebar'
import ReactSpinner from 'react-bootstrap-spinner'
import DeleteConfirm from '../../common/deleteConfirm'
//import { storiesOf } from '@storybook/react';
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import * as managePagesService from '../../../ApiServices/admin/managePages'
import alertify from 'alertifyjs'
import memoize from 'memoize-one'
class AdminPages extends Component {
  state = {
    confirm: {
      name: 'Duplicate',
      message: 'Are you sure you want to duplicate this page? ',
      action: 'duplicate'
    },
    search: '',
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    modal: false,
    spinner: true,
    columns: [
      {
        name: 'Title',
        selector: 'title',
        cell: row => (
          <a className= {row.is_delete === false ? 'fullspaceanchor blue' : 'fullspaceanchor'}  href={'edit-page/' + row._id}>
            {row.title}
          </a>
        ),
        sortable: true
      },

      {
        name: '',
        className: 'abcccc',
        cell: row => {
          return row.is_editable || row.is_duplicate || row.is_delete ? (
            <div className='action_dropdown' data-id={row.slug}>
              <i
                data-id={row.slug}
                className='dropdown_dots'
                data-toggle='dropdown'
                aria-haspopup='true'
                aria-expanded='false'
              ></i>
              <div
                className='dropdown-menu dropdown-menu-center'
                aria-labelledby='dropdownMenuButton'
              >
                {row.is_editable ? (
                  <Link className='dropdown-item' to={'edit-page/' + row._id}>
                    Edit
                  </Link>
                ) : (
                  ''
                )}
                {row.is_duplicate ? (
                  <Link
                    className='dropdown-item'
                    to='#'
                    onClick={() => this.saveAndtoogle(row._id, 'duplicate')}
                  >
                    Duplicate
                  </Link>
                ) : (
                  ''
                )}
                {row.is_delete ? (
                  <Link
                    className='dropdown-item'
                    to='#'
                    onClick={() => this.saveAndtoogle(row._id, 'delete')}
                  >
                    Delete
                  </Link>
                ) : (
                  ''
                )}
              </div>
            </div>
          ) : (
            ''
          )
        },
        allowOverflow: false,
        button: true,
        width: '56px' // custom width for icon button
      }
    ]
  }

  DeletePageId = id => {
    const data_table = this.state.data_table.filter(i => i.id !== id)
    this.setState({ data_table })
  }

  convertArrayOfObjectsToCSV = array => {
    let result

    const columnDelimiter = ','
    const lineDelimiter = '\n'
    const keys = Object.keys(array[0])

    result = ''
    result += keys.join(columnDelimiter)
    result += lineDelimiter

    array.forEach(item => {
      let ctr = 0
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter

        result += item[key]

        ctr++
      })
      result += lineDelimiter
    })

    return result
  }

  downloadCSV = array => {
    const link = document.createElement('a')
    let csv = this.convertArrayOfObjectsToCSV(array)
    if (csv == null) return

    const filename = 'Pages.csv'

    if (!csv.match(/^data:text\/csv/i)) {
      csv = `data:text/csv;charset=utf-8,${csv}`
    }

    link.setAttribute('href', encodeURI(csv))
    link.setAttribute('download', filename)
    link.click()
  }

  searchSpace = event => {
    let keyword = event.target.value

    this.setState({ search: keyword })
  }

  componentDidMount = () => {
    this.getAllpages()
  }

  getAllpages = async () => {
    this.setState({ spinner: true })
    const response = await managePagesService.getAllpages()
    console.log(11,response);
    if (response.data.status == 1) {
      this.setState({
        pages: response.data.data
      })
      const Setdata = { ...this.state.data_table }
      const data_table_row = []
      const data_table_rowTrash = []
      const data_table_rowDraft = []
      console.log('main data', response.data.data);
      this.setState({ spinner: false })
      response.data.data.map((item, index) => {
        if (item.page_status == 2) {
          const SetdataDraft = {}
          SetdataDraft.title = item.title
          SetdataDraft.slug = item.slug
          SetdataDraft._id = item._id
          SetdataDraft.is_editable = item.is_editable
          SetdataDraft.is_duplicate = item.is_duplicate
          SetdataDraft.is_delete = item.is_delete
          data_table_rowDraft.push(SetdataDraft)
        } else if(item.page_status == 0) {
          const SetdataTrash = {}
          SetdataTrash.title = item.title
          SetdataTrash.slug = item.slug
          SetdataTrash._id = item._id
          SetdataTrash.is_editable = item.is_editable
          SetdataTrash.is_duplicate = item.is_duplicate
          SetdataTrash.is_delete = item.is_delete
          data_table_rowTrash.push(SetdataTrash)
        } else {
          const Setdata = {}
          Setdata.title = item.title
          Setdata.slug = item.slug
          Setdata._id = item._id
          Setdata.is_editable = item.is_editable
          Setdata.is_duplicate = item.is_duplicate
          Setdata.is_delete = item.is_delete
          data_table_row.push(Setdata)
        }
      
      })
      this.setState({
        data_table: data_table_row,
        dataTrash: data_table_rowTrash,
        dataDraft: data_table_rowDraft
      })
    }
  }

  deletePage = async () => {
    let id = this.state.index
    const response = await managePagesService.deletePage(id)
    if (response) {
      if (response.data.status == 1) {
        this.getAllpages()
        this.toogle()
        alertify.success(response.data.message)
      } else {
        alertify.error(response.data.message)
      }
    }
  }

  duplicatePage = async () => {
    let id = this.state.index
    const response = await managePagesService.duplicatePage(id)
    if (response) {
      alertify.set('notifier', 'position', 'top-right')
      if (response.data.status == 1) {
        this.getAllpages()
        this.toogle()
        alertify.success(response.data.message)
      } else {
        alertify.error(response.data.message)
      }
    }
  }

  doAction = action => {
    switch (action) {
      case 'duplicate':
        this.duplicatePage()
        break
      case 'delete':
        this.deletePage()
        break
      default:
    }
  }

  saveAndtoogle = (id, action) => {
    switch (action) {
      case 'duplicate':
        this.setState({
          confirm: {
            name: 'Duplicate',
            message: 'Are you sure you want to duplicate this item? ',
            action: action
          }
        })
        break
      case 'delete':
        this.setState({
          confirm: {
            name: 'Delete',
            message: "Are you sure you'd like to delete this item?",
            action: action
          }
        })
        break
      default:
        this.setState({
          confirm: {
            name: 'Delete',
            message: "Are you sure you'd like to delete this item?",
            action: action
          }
        })
    }

    this.setState({ index: id })
    this.toogle()
  }

  toogle = () => {
    let status = !this.state.modal
    this.setState({ modal: status })
  }


  actionHandler = e => {
    this.setState({
      message: '',
      message_type: ''
    })
    const { selectedRows } = this.state
    const Industryids = selectedRows.map(r => r._id)
    let status = ''
    let actionvalue = this.state.contextActions_select
    if (actionvalue === '-1') {
      return
    } else if (actionvalue === '0') {
      status = 'Trash'
    } else if (actionvalue === '1') {
      status = 'Published'
    } else if (actionvalue === '2') {
      status = 'Draft'
    }
    if (window.confirm(`Are you sure you want to move ` + status)) {
      this.setState({ toggleCleared: !this.state.toggleCleared })
      this.actionHandlerPage(Industryids, actionvalue, status)
    }
  }

  actionHandlerPage = async (Industryids, contextActions, status) => {
    try {
      const response = await managePagesService.actionHandlerPage(
        Industryids,
        contextActions,
        status
      )
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: 'success'
        })

        this.getAllpages()
      } else {
        this.setState({
          message: response.data.message,
          message_type: 'error'
        })
        this.getAllpages()
      }
    } catch (err) {
      this.setState({
        message: 'Something went wrong',
        message_type: 'error'
      })
    }
  }

  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows })
  }
  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value })
  }

  tabActive = async tab => {
    this.setState({
      message: '',
      message_type: ''
    })
    this.setState({ activetab: tab })
  }



  render () {
    let rowData = this.state.data_table
    let search = this.state.search
    let rowDataTrash = this.state.dataTrash
    let rowDataDraft = this.state.dataDraft
    
    if (search.length > 0) {
      search = search.trim().toLowerCase()
      rowData = rowData.filter(l => {
        return l.title.toLowerCase().match(search)
      })
      rowDataTrash = rowDataTrash.filter(l => {
        return l.title.toLowerCase().match(search)
      })
      rowDataDraft = rowDataDraft.filter(l => {
        return l.title.toLowerCase().match(search)
      })
    }


    const contextActions = memoize(actionHandler => (
      <div className='common_action_section'>
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value='-1'>Select</option>
          {this.state.activetab !== 'tab_a' ? (
            <option value='1'>Published</option>
          ) : (
            ''
          )}
          {this.state.activetab !== 'tab_b' ? (
            <option value='0'>Trash</option>
          ) : (
            ''
          )}
          {this.state.activetab !== 'tab_c' ? (
            <option value='2'>Draft</option>
          ) : (
            ''
          )}
        </select>
        <button className='danger' onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ))

    const rowSelectCritera = row => row.is_delete === false

    return (
      <React.Fragment>
        <div className='container-fluid admin-body pagelist'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>

            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents  home-inner-content page-edits pt-4 pb-4 pr-4'>
                  <div className='main_admin_page_common'>
                    <div className=''>
                      <div className='admin_breadcum'>
                        <div className='row'>
                          <div className='col-md-1'>
                            <p className='page-title'>Pages</p>
                          </div>
                          <div className='col-md-6'>
                          <a class="add_new_btn" href="/btmoto-gate/admin/addpage">Add New</a>
                            {/* <Link className="add_new_btn" to="/admin/addpage">Add New</Link> */}
                            <Link
                              className='Export_data_btn'
                              to='#'
                              onClick={() => this.downloadCSV(rowData)}
                            >
                              Export Data
                            </Link>
                          </div>
                          <div className='col-md-5'>
                            <div className='searchbox'>
                              <div className='commonserachform'>
                                <span></span>
                                <input
                                  type='text'
                                  placeholder='Search'
                                  onChange={e => this.searchSpace(e)}
                                  name='search'
                                  className='search form-control'
                                />
                                <input type='submit' className='submit_form' />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <React.Fragment>
                        <div
                          style={{
                            display:
                              this.state.spinner === true
                                ? 'flex justify-content-center '
                                : 'none'
                          }}
                          className='overlay text-center'
                        >
                          <ReactSpinner
                            type='border'
                            color='primary'
                            size='10'
                          />
                        </div>
                      </React.Fragment>


<div className="faq-list-table-wrapper">

                      {this.state.message !== '' ? (
                          <p className='tableinformation'>
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          </p>
                        ) : null}
                        <ul className='nav nav-pills nav-fill'>
                          <li
                            className={
                              this.state.activetab === 'tab_a' ? 'active' : ''
                            }
                          >
                            <a
                              href='#tab_a'
                              data-toggle='pill'
                              onClick={() => this.tabActive('tab_a')}
                            >
                              All ({this.state.data_table.length})
                            </a>
                          </li>
                          <li
                            className={
                              this.state.activetab === 'tab_b' ? 'active' : ''
                            }
                          >
                            <a
                              href='#tab_b'
                              data-toggle='pill'
                              onClick={() => this.tabActive('tab_b')}
                            >
                              Trash ({this.state.dataTrash.length})
                            </a>
                          </li>

                          <li
                            className={
                              this.state.activetab === 'tab_c' ? 'active' : ''
                            }
                          >
                            <a
                              href='#tab_c'
                              data-toggle='pill'
                              onClick={() => this.tabActive('tab_c')}
                            >
                              Draft ({this.state.dataDraft.length})
                            </a>
                          </li>
                        </ul>
                        <div className='tab-content'>
                          <div className='tab-pane active' id='tab_a'>
                          <DataTable
                        columns={this.state.columns}
                        data={rowData}
                        selectableRows
                        highlightOnHover
                        pagination
                        selectableRowsVisibleOnly
                        selectableRowDisabled={rowSelectCritera}
                        clearSelectedRows={this.state.toggleCleared}
                        onSelectedRowsChange={this.handlerowChange}
                        contextActions={contextActions(this.deleteAll)}
                        noDataComponent = {<p>There are currently no records.</p>}
                      />
                          </div>
                          <div className='tab-pane' id='tab_b'>
                          <DataTable
                        columns={this.state.columns}
                        data={rowDataTrash}
                        selectableRows
                        highlightOnHover
                        pagination
                        selectableRowsVisibleOnly
                        selectableRowDisabled={rowSelectCritera}
                        clearSelectedRows={this.state.toggleCleared}
                        onSelectedRowsChange={this.handlerowChange}
                        contextActions={contextActions(this.deleteAll)}
                        noDataComponent = {<p>There are currently no records.</p>}
                      />
                          </div>
                          <div className='tab-pane' id='tab_c'>
                          <DataTable
                        columns={this.state.columns}
                        data={rowDataDraft}
                        selectableRows
                        highlightOnHover
                        pagination
                        selectableRowsVisibleOnly
                        selectableRowDisabled={rowSelectCritera}
                        clearSelectedRows={this.state.toggleCleared}
                        onSelectedRowsChange={this.handlerowChange}
                        contextActions={contextActions(this.deleteAll)}
                        noDataComponent = {<p>There are currently no records.</p>}
                      />
                          </div>
                        </div>
                     
                        </div>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Delete/Duplicate confirm box */}
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          name={this.state.confirm.name}
          message={this.state.confirm.message}
          action={this.state.confirm.action}
          deleteUser={this.doAction}
        />
      </React.Fragment>
    )
  }
}

export default AdminPages
