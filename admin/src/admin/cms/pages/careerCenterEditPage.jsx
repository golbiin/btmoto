import React, { Component } from "react";
import Adminheader from "../../common/adminHeader";
import Adminsidebar from "../../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as managePageService from "../../../ApiServices/admin/managePages";
import CKEditor from "ckeditor4-react";
import { apiUrl, bootstrap ,siteUrl} from "../../../../src/config.json";
import MultipleUploadImages from "../../common/multipleuploadimages";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";

class CareerCenterEditPage extends Component {
  state = {
    id:'',
    cimonPages: [],
    uploadedImages: [],
    parentPages: [
      { name: "One", id: 1 },
      { name: "Two", id: 2 },
      { name: "Three", id: 3 },
      { name: "four", id: 4 },
    ],
    errors: {},
    validated: false,
    pageStatus: true,
    submitStatus: false,
    spinner: true,
    message: "",
    message1:"",
    responsetype: "",
    editorState: "",
    bannerContent: "",

    videofile: "",

    videourl: "",

    section1: "",
    section1sub1: "",
    section2: "",
    section3: "",
    section4: "",
    section5: "",
    section6: "",
    section7: "",
    section8: "",
    section9: "",
    section10: "",
    section11: "",
    section12: "",
    section13: "",
    section14: "",
    section15: "",
    section16: "",
    is_preview:"",
    attributes: [],
    upload_image_status: [],
    productSilder: [],
    product_image_status: [],
  };

  onvideoupload = async (value, item) => {
    let errors = { ...this.state.errors };
    try {
      if (item === "errors") {
        errors["videourl"] = value;
        this.setState({
          errors: errors,
        });
      } else {
        this.setState({ download_1_status: true });
        let file = value.target.files[0];
        const response1 = await managePageService.uploadVideo(file);
        if (response1.data.status === 1) {
          let cimonPages = { ...this.state.cimonPages };
          let filepath = {
            image_name: response1.data.data.file_name,
            image_url: response1.data.data.file_location,
          };
          cimonPages["videourl"] = response1.data.data.file_location;
          this.setState({
            videofile: filepath,
            cimonPages: cimonPages,
          });
        } else {
          this.setState({
            message: response1.data.message,
            responsetype: "error",
          });
        }
        this.setState({ download_1_status: false });
      }
    } catch (err) {
      errors["videourl"] = err.message;
      this.setState({
        message: "something went wrong",
        responsetype: "error",
        errors: errors,
        download_1_status: false,
      });
    }
  };

  /*****************Joi validation schema**************/
  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
    page_title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
      meta_tag: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
      meta_desc: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
    _id: Joi.optional().label("id"),
    slug: Joi.string(),
    editor_content: Joi.string(),
    title1: Joi.string(),
    title2: Joi.string(),
    title3: Joi.string(),
    title4: Joi.string(),
    title5: Joi.string(),
    title6: Joi.string(),
  };
  /*****************end Joi validation schema**************/

  /*****************INPUT HANDLE CHANGE **************/

  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const cimonPages = { ...this.state.cimonPages };
    const errorMessage = this.validateProperty(input);

    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    cimonPages[input.name] = input.value;
    this.setState({ cimonPages, errors });
  };

  /*****************JOI VALIDATION CALL**************/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  // /***************** FORM SUBMIT **************/

  componentDidMount = async () => {

    const slug = this.props.slug;
    const id = this.props.id;

    this.setState({
      spinner: true,
      slug: slug,
      id:id
    });

    try {
      const response = await managePageService.getSinglePageById(id);
      let cimonPages = { ...this.state.cimonPages };
      if (response.data.status === 1) {
        let newPagearray = {
          //  slug: response.data.data.page_data.slug,
          slug: this.state.slug,
          title: response.data.data.page_data.title,
          page_title: response.data.data.page_data.page_title,
          meta_tag: response.data.data.page_data.meta_tag,
          meta_desc: response.data.data.page_data.meta_desc,
        };
        this.setState({ cimonPages: newPagearray });
        this.setState({ is_preview: response.data.data.page_data.is_preview });
        if (response.data.data.page_data.hasOwnProperty("content")) {
          let cimonPages = { ...this.state.cimonPages };

          cimonPages["title1"] = response.data.data.page_data.content.title1
            ? response.data.data.page_data.content.title1
            : "";
          cimonPages["title2"] = response.data.data.page_data.content.title2
            ? response.data.data.page_data.content.title2
            : "";
          cimonPages["title3"] = response.data.data.page_data.content.title3
            ? response.data.data.page_data.content.title3
            : "";
          cimonPages["title4"] = response.data.data.page_data.content.title4
            ? response.data.data.page_data.content.title4
            : "";
          cimonPages["title5"] = response.data.data.page_data.content.title5
            ? response.data.data.page_data.content.title5
            : "";
          cimonPages["title6"] = response.data.data.page_data.content.title6
            ? response.data.data.page_data.content.title6
            : "";

          setTimeout(() => {
            this.setState({
              cimonPages: cimonPages,
              attributes: response.data.data.page_data.content.bannerSlider
                ? response.data.data.page_data.content.bannerSlider
                : [],
              spinner: false,
            });

            let status_array = [];
            this.state.attributes.map((attribute, sidx) => {
              status_array.push(false);
            });

            let status_pdt_array = [];
            this.state.productSilder.map((attribute, sidx) => {
              status_pdt_array.push(false);
            });

            this.setState({
              upload_image_status: status_array,
              product_image_status: status_pdt_array,
            });
          }, 2000);
        }
        this.setState({ spinner: false });
      } else {
        this.setState({ spinner: false });
        this.setState({
          pageStatus: false,
          message: response.data.message,
          responsetype: "error",
        });
      }
    } catch (err) {
      this.setState({
        message: "something went wrong",
        responsetype: "error",
        spinner: false,
      });
    }
  };

  handleSubmit = async () => {
    const cimonPages = { ...this.state.cimonPages };
    const errors = { ...this.state.errors };
    //  cimonPages["editor_content"] = this.state.editorState;

    let result = Joi.validate(cimonPages, this.schema);

    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors,
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors,
        });
      } else {
        this.setState({ spinner: true });

        const page_data = {
          id: this.state.id,
          title: this.state.cimonPages.title,
          slug: this.state.slug,
          page_title: this.state.cimonPages.page_title,
          meta_tag: this.state.cimonPages.meta_tag,
          meta_desc: this.state.cimonPages.meta_desc,

          // attributes: SetAttributes,
          content: {
            title1: this.state.cimonPages.title1,
            title2: this.state.cimonPages.title2,
            title3: this.state.cimonPages.title3,
            title4: this.state.cimonPages.title4,
            title5: this.state.cimonPages.title5,
            title6: this.state.cimonPages.title6,
            bannerSlider: this.state.attributes.filter(
              (s, sidx) => s.heading !== ""
            ),
          },
        };

        try {
          this.setState({ submit_status: true });
          const response = await managePageService.updatePagedata(page_data);
          if (response.data.status === 1) {
            this.setState({
              submitStatus: false,
              message: response.data.message,
              responsetype: "success",
              spinner: false,
            });
          } else {
            this.setState({
              submitStatus: false,
              spinner: false,
              message: response.data.message,
              responsetype: "error",
            });
          }
        } catch (err) {
          this.setState({
            message: "something went wrong",
            responsetype: "error",
            spinner: false,
            submit_status: false,
          });
        }
      }
    }
  };

  handlePreview = async () => {
    const id = this.props.id;
    const slug = this.props.slug;
    const cimonPages = { ...this.state.cimonPages };
    const errors = { ...this.state.errors };
    //  cimonPages["editor_content"] = this.state.editorState;

    let result = Joi.validate(cimonPages, this.schema);

    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors,
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors,
        });
      } else {
        this.setState({ spinner: true });

        const page_data = {
        
          slug: this.state.slug,
          

          // attributes: SetAttributes,
          preview: {
            slug: this.state.cimonPages.slug,
            page_title: this.state.cimonPages.page_title,
            meta_tag: this.state.cimonPages.meta_tag,
            meta_desc: this.state.cimonPages.meta_desc,

            title: this.state.cimonPages.title,
            title1: this.state.cimonPages.title1,
            title2: this.state.cimonPages.title2,
            title3: this.state.cimonPages.title3,
            title4: this.state.cimonPages.title4,
            title5: this.state.cimonPages.title5,
            title6: this.state.cimonPages.title6,
            bannerSlider: this.state.attributes.filter(
              (s, sidx) => s.heading !== ""
            ),
          },
        };

        try {
          this.setState({ submit_status: true });
          const response = await managePageService.previewPagedata(page_data);
          if (response.data.status === 1) {
            this.setState({
              submitStatus: false,
              message1: response.data.message,
              responsetype: "success",
              spinner: false,
            });
            setTimeout(() => {
              const win = window.open("/cimon-gate/admin/preview/" + id, "_blank");
                        win.focus();
              // this.props.history.push({
              //   pathname: "/admin/preview/" + id,
              // });
            }, 2000);
          } else {
            this.setState({
              submitStatus: false,
              spinner: false,
              message1: response.data.message,
              responsetype: "error",
            });
          }
        } catch (err) {
          this.setState({
            message: "something went wrong",
            responsetype: "error",
            spinner: false,
            submit_status: false,
          });
        }
      }
    }
  };

  handleheadingChange = (idx) => (evt) => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute;
      return { ...attribute, heading: evt.target.value };
    });

    this.setState({ attributes: newAttributes });
  };

  handlesubheadingChange = (idx) => (evt) => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute;
      return { ...attribute, subheading: evt.editor.getData() };
    });

    this.setState({ attributes: newAttributes });
  };

  onMultipleupload = async (value, item, idx) => {
    let errors = { ...this.state.errors };
    try {
      if (item === "errors") {
        errors["videourl"] = value;
        this.setState({
          errors: errors,
        });
      } else {
        const staus = [...this.state.upload_image_status];
        staus[idx] = true;
        this.setState({ upload_image_status: staus });

        let file = value.target.files[0];
        const response1 = await managePageService.uploadVideo(file);
        if (response1.data.status === 1) {
          const newAttributes = this.state.attributes.map((attribute, sidx) => {
            if (idx !== sidx) return attribute;
            return { ...attribute, image: response1.data.data.file_location };
          });

          this.setState({ attributes: newAttributes });
        } else {
          this.setState({
            message: response1.data.message,
            responsetype: "error",
          });
        }

        staus[idx] = false;
        this.setState({ upload_image_status: staus });
      }
    } catch (err) {
      this.setState({
        message: "something went wrong",
        responsetype: "error",
        download_1_status: false,
      });
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents backgroundoverlay  home-inner-content pt-4 pb-4 pr-4">
                  <React.Fragment>
                    <div
                      style={{
                        display: this.state.spinner === true ? "flex" : "none",
                      }}
                      className="overlay"
                    >
                      <ReactSpinner type="border" color="primary" size="10" />
                    </div>
                  </React.Fragment>
                  <div className="row addpage-form-wrap">
                    <div className="addpage-form">
                    {this.state.is_preview === true?(<div className="col-md-12 ">
                                            <div className="form-group1 preview_grp">
                                            <span className="input-group-btn">
                                          
                                               <button onClick={this.handlePreview} className="btn btn-info  pull-right">Preview</button>
                                               {this.state.message1 !== "" ? (
                                                <div className="tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message1}</p>
                                                </div></div>
                                            ) : null} 
                                            
                                             </span>

                                               
                                            </div>
                                           
                                          
                                            
                                        </div>):""}

                      <div className="col-md-12 ">
                        <div className="form-group">
                          <label htmlFor="">Edit Page</label>
                          <input
                            name="title"
                            value={this.state.cimonPages.title}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add title"
                            className="form-control"
                          />
                          {this.state.errors.title ? (
                            <div className="error text-danger">
                              {this.state.errors.title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>

                      <div className="col-md-12 ">
                        <div className="form-group">
                          <label htmlFor="">Page Title</label>
                          <input
                            name="page_title"
                            value={this.state.cimonPages.page_title}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Page title"
                            className="form-control"
                          />
                          {this.state.errors.page_title ? (
                            <div className="error text-danger">
                              {this.state.errors.page_title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Meta tag</label>
                          <input
                            name="meta_tag"
                             value={this.state.cimonPages.meta_tag}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Meta Tag"
                            className="form-control"
                          />
                          {this.state.errors.meta_tag ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_tag}
                            </div>
                          ) : (
                            ""
                          )}
                          
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Meta Description</label>
                          <input
                            name="meta_desc"
                           value={this.state.cimonPages.meta_desc}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.meta_desc ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_desc}
                            </div>
                          ) : (
                            ""
                          )}
                          
                        </div>
                        <div className="form-group"> 
                          <label htmlFor="">Page Slug</label>
                          <input
                            name="slug"
                             value={this.state.cimonPages.slug}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add Page Slug"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ""
                          )}
                          
                        </div>
                      </div>

                      <div className="col-md-12 ">
                        <div className="form-group">
                          <label htmlFor="">Banner</label>

                          <div className="banner-section">
                            {this.state.attributes
                              ? this.state.attributes.map((attribute, idx) => (
                                  <div className="banner-single">
                                    <Accordion>
                                      <Card>
                                        <Card.Header>
                                          <Accordion.Toggle
                                            as={Button}
                                            variant="link"
                                            eventKey="0"
                                          >
                                            {attribute.heading
                                              ? attribute.heading
                                              : `slide #${idx + 1}`}
                                          </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                          <Card.Body>
                                            <input
                                              onChange={this.handleheadingChange(
                                                idx
                                              )}
                                              type="text"
                                              value={
                                                attribute.heading
                                                  ? attribute.heading
                                                  : ""
                                              }
                                              placeholder="Heading"
                                              className="form-control"
                                            />

                                            <CKEditor
                                              data={attribute.subheading}
                                               onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                                extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                contentsCss: bootstrap,
                                                height: "6em",
                                                allowedContent: true,
                                                filebrowserImageUploadUrl:
                                                  apiUrl +
                                                  "/admin/upload/imageupload-new",
                                              }}
                                              onInit={(editor) => {}}
                                              onChange={this.handlesubheadingChange(
                                                idx
                                              )}
                                            />

                                            <MultipleUploadImages
                                              idx={idx}
                                              imagestatus={
                                                this.state.upload_image_status[
                                                  idx
                                                ]
                                              }
                                              onMultipleupload={
                                                this.onMultipleupload
                                              }
                                              value={
                                                attribute.image
                                                  ? attribute.image
                                                  : ""
                                              }
                                            />
                                          </Card.Body>
                                        </Accordion.Collapse>
                                      </Card>
                                    </Accordion>
                                  </div>
                                ))
                              : ""}
                          </div>
                        </div>
                      </div>

                      <div className="col-md-12 ">
                        <div className="form-group">
                          <label htmlFor="">Title 1</label>
                          <input
                            name="title1"
                            value={this.state.cimonPages.title1}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Title 1"
                            className="form-control"
                          />
                          {this.state.errors.title1 ? (
                            <div className="error text-danger">
                              {this.state.errors.title1}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Title 2</label>
                          <input
                            name="title2"
                            value={this.state.cimonPages.title2}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Title 2"
                            className="form-control"
                          />
                          {this.state.errors.title2 ? (
                            <div className="error text-danger">
                              {this.state.errors.title2}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Title 3</label>
                          <input
                            name="title3"
                            value={this.state.cimonPages.title3}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Title 3"
                            className="form-control"
                          />
                          {this.state.errors.title3 ? (
                            <div className="error text-danger">
                              {this.state.errors.title3}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Title 4</label>
                          <input
                            name="title4"
                            value={this.state.cimonPages.title4}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Title 4"
                            className="form-control"
                          />
                          {this.state.errors.title4 ? (
                            <div className="error text-danger">
                              {this.state.errors.title4}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Title 5</label>
                          <input
                            name="title5"
                            value={this.state.cimonPages.title5}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Title 5"
                            className="form-control"
                          />
                          {this.state.errors.title5 ? (
                            <div className="error text-danger">
                              {this.state.errors.title5}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Title 6</label>
                          <input
                            name="title6"
                            value={this.state.cimonPages.title6}
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Title 6"
                            className="form-control"
                          />
                          {this.state.errors.title6 ? (
                            <div className="error text-danger">
                              {this.state.errors.title6}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>

                      <div className="col-md-12 ">
                        <div className="form-inline form float-right form-btn-wrap">
                          {/* <Link to="#">Move to Trash</Link> */}
                          <span className="input-group-btn">
                            <button
                              onClick={this.handleSubmit}
                              className="btn btn-info"
                            >
                              Update
                            </button>
                          </span>
                        </div>
                        {this.state.message !== "" ? (
                                                <div className="tableinformation"><div className={this.state.responsetype} >
                                                    <p>{this.state.message}</p>
                                                </div></div>
                                            ) : null} 
                                          
                                            
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CareerCenterEditPage;
