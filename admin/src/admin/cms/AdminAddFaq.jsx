import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import * as settingsService from "../../ApiServices/admin/manageFaq";
import CKEditor from "ckeditor4-react";
import { apiUrl ,siteUrl } from "../../../src/config.json";
import MultiSelect from "react-multi-select-component";
import BlockUi from 'react-block-ui';
class AdminAddFaq extends Component {
  constructor(props) {
    super(props);
    this.onEditorChange = this.onEditorChange.bind(this);
  }
  state = {
    editorContent: "",
    data: {
      title: "",
      slug: ""
    },

    related_cat: [],
    related_cat_options: [],
    related_cat_selected: [],

    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    description: "",
    focused: false,
    spinner: false
  };

  onEditorChange(evt) {
    this.setState({
      description: evt.editor.getData()
    });
  }

  componentDidMount = async () => {
    this.getCategoryParent();
  };

  setcatSelected = event => {
    const data_selected_row = [];
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value;
    });

    this.setState({ related_cat: data_selected_row });
    this.setState({ related_cat_selected: event });
  };

  getCategoryParent = async () => {
    this.setState({ spinner: true });
    const response = await settingsService.getCategoryParent();
    if (response.data.status == 1) {
      const Setdata = { ...this.state.related_cat };
      const data_related_row = [];
      const data_selected_row = [];
      response.data.data.map((Cats, index) => {
        const Setdata = {};
        Setdata.value = Cats._id;
        Setdata.label = Cats.category_name;
        data_related_row.push(Setdata);
      });
      this.setState({ related_cat_options: data_related_row });
      this.setState({ related_cat_selected: data_selected_row });
      console.log(this.state.related_cat_options);
    }
    this.setState({ spinner: false });
  };

  /* Joi validation schema */
  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    slug: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      })
  };
  /* Input Handle Change */
  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);

    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  /*Joi Validation Call*/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Form Submit */
  handleSubmit = async () => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    let result = Joi.validate(data, this.schema);

    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });
        this.updateCaseData();
      }
    }
  };
  /* Add case */
  updateCaseData = async () => {
    const faq_data = {
      title: this.state.data.title,
      description: this.state.description,
      categories: this.state.related_cat,
      slug: this.state.data.slug
    };

    const response = await settingsService.createFaq(faq_data);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                  <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className="row addpage-form-wrap">
                      <div className="col-lg-8 col-md-12">
                        <div className="form-group">
                          <label htmlFor="">Title</label>
                          <input
                            name="title"
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add title *"
                            className="form-control"
                          />
                          {this.state.errors.title ? (
                            <div className="error text-danger">
                              {this.state.errors.title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Slug</label>
                          <input
                            name="slug"
                            onChange={this.handleChange}
                            value={this.state.slug}
                            type="text"
                            placeholder="slug *"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-12">
                              <label htmlFor="">Content</label>
                              <CKEditor
                                data={this.state.description}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorChange}
                                style={{
                                  float: "left",
                                  width: "99%"
                                }}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-12 ">
                        <div className="form-group form-group-inline checkbox-div">
                          <label htmlFor="">Categories</label>
                          <MultiSelect
                            options={this.state.related_cat_options}
                            value={this.state.related_cat_selected}
                            onChange={this.setcatSelected}
                          />
                        </div>

                        <div className="faq-sideinputs">
                          <div className="faq-btns form-btn-wrap">
                            <div className="float-left"></div>
                            <div className="update_btn input-group-btn float-right">
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="btn btn-info"
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                                Publish
                              </button>
                            </div>
                          </div>
                          {this.state.message !== "" ? (
                            <p className="tableinformation">
                              <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                              </div>
                            </p>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </BlockUi>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminAddFaq;
