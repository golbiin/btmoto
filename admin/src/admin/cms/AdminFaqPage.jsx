import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import DeleteConfirm from "../common/deleteConfirm";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import * as settingsService from "../../ApiServices/admin/manageFaq";
import ReactSpinner from "react-bootstrap-spinner";
import memoize from "memoize-one";
import TrashConfirm from "../common/trashConfirm";

class AdminFaqPage extends Component {
  state = {
    search: "",

    dateFilterValue: "",
    typeFilterValue: "",
    dateFilterOptions: [],
    filterMonthYears: "",
    selectedRows: [],
    modal_trash: false,
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    spinner: true,
    contextActions_select: "-1",
    activetab: "tab_a",

    columns: [
      {
        name: "Title",
        selector: "title",
        sortable: true,
        cell: row => (
          <div>
            <Link to={"/admin/editfaq/" + row._id}>{row.title}</Link>
          </div>
        )
      },
      {
        name: "FAQ CATEGORY",
        cell: row => (
          <div className="add_catgory_new">
            <a onClick={() => this.handleShow(row._id)}>
              {row.categories ? row.categories : ""}
            </a>
          </div>
        ),
        sortable: true,
        left: true,
        hide: "sm",
        width: "200px"
      },
      {
        name: "",
        cell: row => (
          <div className="action_dropdown" data-id={row._id}>
            <i
              data-id={row._id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            />
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >
              <Link to={"/admin/editfaq/" + row._id} className="dropdown-item">
                Edit Page
              </Link>
              <Link
                onClick={() => this.saveAndtoogle(row._id)}
                className="dropdown-item"
              >
                Delete
              </Link>

              {row.page_status != 0 ? (
                <Link
                  onClick={() => this.saveAndtoogletrash(row._id, "trash")}
                  className="dropdown-item"
                >
                  Trash
                </Link>
              ) : (
                ""
              )}
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px" // custom width for icon button
      }
    ]
  };

  /* set date format */
  formatDate(string) {
    var options = { year: "numeric", month: "numeric", day: "numeric" };
    return new Date(string).toLocaleDateString("en-GB", options);
  }

  componentDidMount = () => {
    this.getAllFaq();
  };
  DeletePageId = id => {
    const data_table = this.state.data_table.filter(i => i.id !== id);
    this.setState({ data_table });
  };

  /* Get all News */
  getAllFaq = async () => {
    this.setState({ spinner: true });
    try {
      const response = await settingsService.getAllFaq();
      if (response.data.status === 1) {
        const data_table_row = [];
        const data_table_rowTrash = [];
        const data_table_rowDraft = [];

        response.data.data.map((news, index) => {
          if (news.page_status == 0) {
            const SetdataTrash = {};
            SetdataTrash.title = news.title;
            SetdataTrash._id = news._id;
            SetdataTrash.page_status = news.page_status;
            const cat_names = [];
            news.category_info.map(cat => {
              cat_names.push(cat.category_name);
            });

            let cat_string = "";
            cat_string = cat_names.toString();
            cat_string = cat_string.replace(/,/g, ", ");
            SetdataTrash.categories = cat_string; // set categories

            data_table_rowTrash.push(SetdataTrash);
          } else if (news.page_status == 1) {
            const Setdata = {};
            Setdata.title = news.title;
            Setdata._id = news._id;
            Setdata.page_status = news.page_status;
            const cat_names = [];
            news.category_info.map(cat => {
              cat_names.push(cat.category_name);
            });

            let cat_string = "";
            cat_string = cat_names.toString();
            cat_string = cat_string.replace(/,/g, ", ");
            Setdata.categories = cat_string; // set categories

            data_table_row.push(Setdata);
          } else if (news.page_status == 2) {
            const SetdataDraft = {};
            SetdataDraft.title = news.title;
            SetdataDraft._id = news._id;
            SetdataDraft.page_status = news.page_status;
            const cat_names = [];
            news.category_info.map(cat => {
              cat_names.push(cat.category_name);
            });

            let cat_string = "";
            cat_string = cat_names.toString();
            cat_string = cat_string.replace(/,/g, ", ");
            SetdataDraft.categories = cat_string; // set categories

            data_table_rowDraft.push(SetdataDraft);
          }
        });
        this.setState({
          data_table: data_table_row,
          dataTrash: data_table_rowTrash,
          dataDraft: data_table_rowDraft
        });
        console.log("all", this.state.data_table);
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  searchSpace = event => {
    let keyword = event.target.value;
    this.setState({ search: keyword });
  };

  deleteNews = async () => {
    let id = this.state.index;
    const response = await settingsService.deleteFaq(id);
    if (response) {
      if (response.data.status == 1) {
        // const data_table = this.state.data_table.filter(i => i._id !== id)
        // this.setState({ data_table })
        const data_table = this.state.data_table.filter(i => i._id !== id);
        const dataTrash = this.state.dataTrash.filter(i => i._id !== id);
        const dataDraft = this.state.dataDraft.filter(i => i._id !== id);
        this.setState({
          data_table: data_table,
          dataTrash: dataTrash,
          dataDraft: dataDraft
        });
        this.toogle();
      }
    }
  };
  trashFaq = async () => {
    let id = this.state.index;
    const response = await settingsService.trashFaq(id);
    if (response) {
      if (response.data.status == 1) {
        // const data_table = this.state.data_table.filter(i => i._id !== id)
        // this.setState({ data_table })
        const data_table = this.state.data_table.filter(i => i._id !== id);
        const dataTrash = this.state.dataTrash.filter(i => i._id !== id);
        const dataDraft = this.state.dataDraft.filter(i => i._id !== id);
        this.setState({
          data_table: data_table,
          dataTrash: dataTrash,
          dataDraft: dataDraft
        });
        this.toogleTrash();
        this.getAllFaq();
      }
    }
  };

  saveAndtoogle = id => {
    this.setState({ index: id });
    this.toogle();
  };
  saveAndtoogletrash = id => {
    this.setState({ index: id });
    this.toogleTrash();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };
  toogleTrash = () => {
    let status = !this.state.modal_trash;
    this.setState({ modal_trash: status });
  };
  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows });
  };
  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value });
  };

  actionHandler = e => {
    this.setState({
      message: "",
      message_type: ""
    });
    const { selectedRows } = this.state;
    const newsids = selectedRows.map(r => r._id);
    let status = "";
    let actionvalue = this.state.contextActions_select;
    if (actionvalue === "-1") {
      return;
    } else if (actionvalue === "0") {
      status = "Trash";
    } else if (actionvalue === "1") {
      status = "Published";
    } else if (actionvalue === "2") {
      status = "Draft";
    } else if (actionvalue === "2") {
      status = "Delete trash";
    }
    if (window.confirm(`Are you sure you want to move ` + status)) {
      this.setState({ toggleCleared: !this.state.toggleCleared });
      this.actionHandlerNews(newsids, actionvalue, status);
    }
  };

  actionHandlerNews = async (newsids, contextActions, status) => {
    try {
      const response = await settingsService.actionHandlerCasestudy(
        newsids,
        contextActions,
        status
      );
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success"
        });

        this.getAllFaq();
      } else {
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
        this.getAllFaq();
      }
    } catch (err) {
      this.setState({
        message: "Something went wrong",
        message_type: "error"
      });
    }
  };

  tabActive = async tab => {
    this.setState({
      message: "",
      message_type: ""
    });
    this.setState({ activetab: tab });
  };

  render() {
    let rowData = this.state.data_table;
    let search = this.state.search;

    let rowDataTrash = this.state.dataTrash;
    let rowDataDraft = this.state.dataDraft;
    const contextActions = memoize(actionHandler => (
      <div className="common_action_section">
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value="-1">Select</option>
          {this.state.activetab !== "tab_a" ? (
            <option value="1">Published</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_b" ? (
            <option value="0">Trash</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_c" ? (
            <option value="2">Draft</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_a" &&
          this.state.activetab !== "tab_c" ? (
            <option value="3">Delete Trash</option>
          ) : (
            ""
          )}
        </select>
        <button className="danger" onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ));
    if (search.length > 0) {
      search = search.trim().toLowerCase();
      rowData = rowData.filter(l => {
        return l.title.toLowerCase().match(search);
      });
    }
    // let dateFilterOptionHtml = this.state.dateFilterOptions.map(dateObj =>(
    //   <option key={dateObj.month + '/' + dateObj.year} value={dateObj.month + '/' + dateObj.year} >{dateObj.month + '/' + dateObj.year}</option>
    // ));
    //console.log(2020,dateFilterOptionHtml);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body ">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4">
                  <div className="main_admin_page_common">
                    <div className="">
                      <div className="admin_breadcum">
                        <div className="row">
                          <div className="col-md-1">
                            <p className="page-title">Faq</p>
                          </div>
                          <div className="col-md-6">
                            <Link className="add_new_btn" to="/admin/addfaq">
                              Add New
                            </Link>
                          </div>

                          <div className="col-md-3">
                            <div className="searchbox">
                              <div className="commonserachform">
                                <span />
                                <input
                                  type="text"
                                  placeholder="Search"
                                  onChange={e => this.searchSpace(e)}
                                  name="search"
                                  className="search form-control"
                                />
                                <input type="submit" className="submit_form" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="faq-list-table-wrapper">
                        <React.Fragment>
                          <div
                            style={{
                              display:
                                this.state.spinner === true
                                  ? "flex justify-content-center "
                                  : "none"
                            }}
                            className="overlay text-center"
                          >
                            <ReactSpinner
                              type="border"
                              color="primary"
                              size="10"
                            />
                          </div>
                        </React.Fragment>
                        {this.state.message !== "" ? (
                          <p className="tableinformation">
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          </p>
                        ) : null}
                        <ul className="nav nav-pills nav-fill">
                          <li
                            className={
                              this.state.activetab === "tab_a" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_a"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_a")}
                            >
                              All ({this.state.data_table.length})
                            </a>
                          </li>
                          <li
                            className={
                              this.state.activetab === "tab_b" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_b"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_b")}
                            >
                              Trash ({this.state.dataTrash.length})
                            </a>
                          </li>

                          <li
                            className={
                              this.state.activetab === "tab_c" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_c"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_c")}
                            >
                              Draft ({this.state.dataDraft.length})
                            </a>
                          </li>
                        </ul>
                        <div className="tab-content">
                          <div className="tab-pane active" id="tab_a">
                            <DataTable
                              columns={this.state.columns}
                              data={rowData}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                               noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                          <div className="tab-pane" id="tab_b">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataTrash}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                          <div className="tab-pane" id="tab_c">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataDraft}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.deleteNews}
        />
        <TrashConfirm
          modal={this.state.modal_trash}
          toogle={this.toogleTrash}
          trash_action={this.trashFaq}
        />
      </React.Fragment>
    );
  }
}

export default AdminFaqPage;
