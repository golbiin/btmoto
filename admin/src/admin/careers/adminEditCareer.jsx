import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import * as CategoryService from "../../ApiServices/admin/manageCareerCategory";
import {CountryDropdown} from "react-country-region-selector";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl} from '../../../src/config.json';
import BlockUi from 'react-block-ui';
import Moment from 'moment';
import moment from 'moment';
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
class AdminEditCareer extends Component {
    constructor(props) {
        super(props);
        this.onEditorChange = this.onEditorChange.bind( this );
    }
    state = { 
        spinner: false,
        JobTypes: [
            { name: 'Part-Time', id: 'Part-Time' },
            { name: 'Full-Time', id: 'Full-Time' },
        ],

        JobLocation: [
            { name: 'Ontario', id: 'Ontario' },
            { name: 'Alberta', id: 'Alberta' },
        ],
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        related_cat : '',
        cimonCareers: [],
        validated: false,
        newsStatus: true,
        category : [],
        releasedate: '',
    }

    onEditorChange( evt ) {
      const cimonCareers = { ...this.state.cimonCareers };
      cimonCareers["content"] = evt.editor.getData()
      this.setState({
        cimonCareers : cimonCareers
      })
    }
    
    onCashange  = data => { 
  
      const cimonCareers = { ...this.state.cimonCareers };
      cimonCareers["content"] = data.getData();
      this.setState({
        cimonCareers : cimonCareers
      })
    }
    onclassicEdit = async (value, item) => {
      if (item === "editorContent") {
          const cimonCareers = { ...this.state.cimonCareers };
          cimonCareers["content"] = value.getData();
          this.setState({
            cimonCareers : cimonCareers
          })
      } 
    };

    /*Joi validation schema*/  
    schema = {
      job_name: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        slug: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        meta_tag: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        meta_desc: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        related_cat: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        job_type: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        job_location: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        _id: Joi.optional().label("id"),
        content:Joi.allow(null),
    
    };

    /* Input Handle Change */
    selectCountry = async (val) => {
      const cimonCareers = { ...this.state.cimonCareers };
      cimonCareers["job_location"] = val;
      this.setState({ cimonCareers });
    };
 
    handleChange = (event, type = null) => {
        let cimonCareers = { ...this.state.cimonCareers };
        const errors = { ...this.state.errors };
        this.setState({ message: "" });
        delete errors.validate;
        let name = event.target.name; //input field  name
        let value = event.target.value; //input field value
        const errorMessage = this.validateNewsdata(name, value);
        if (errorMessage) errors[name] = errorMessage;
        else delete errors[name];
        cimonCareers[name] = value;
        this.setState({ cimonCareers, errors });
    };
    validateNewsdata = (name, value) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };
    
    componentDidMount = async () => {
        this.setState({spinner:true});
        const id = this.props.match.params.id;
        const response = await settingsService.getSingleCareer(id);
        if(response.data.status == 1){
          let newNewsarray = {
            _id: response.data.data.career_data._id,
            job_name: response.data.data.career_data.job_name,
            slug: response.data.data.career_data.slug,
            meta_tag:  response.data.data.career_data.meta_tag ,
            meta_desc:  response.data.data.career_data.meta_desc ,
            job_type: response.data.data.career_data.job_type,
            job_location: response.data.data.career_data.location,
            content: response.data.data.career_data.content,
            related_cat : response.data.data.career_data.related_cat,
          };
          setTimeout(() => { 
            this.setState({ cimonCareers : newNewsarray ,
              releasedate: moment(response.data.data.career_data.post_date).toDate(),
            });
            this.setState({spinner:false});
          }, 2000);
        }else{
          this.setState({
            newsStatus: false,
            message: response.data.message,
            responsetype: "error",
          });
          this.setState({spinner:false});
        }

        this.getCareerCategory();    
    };

    getCareerCategory = async e => {

      console.log('getCareerCategory');
      const response = await CategoryService.getAllCategorys();
      if(response.data.status == 1){
        
        this.setState({category:response.data.data});
        
      }else{
        this.setState({
          newsStatus: false,
          message: response.data.message,
          responsetype: "error",
        });
        this.setState({spinner:false});
      }
      
    };



    handleselectcat = async e => {
      const cimonCareers = { ...this.state.cimonCareers };
      cimonCareers['related_cat'] = e.target.value;
      this.setState({ cimonCareers: cimonCareers })
    }
   /* Form Submit */
    handleSubmit = async () => {
        const cimonCareers = { ...this.state.cimonCareers };
      
        const errors = { ...this.state.errors };
        let result = Joi.validate(cimonCareers, this.schema);
        if (result.error) {
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
                this.setState({
                    errors: errors  
                  })
            } else {
            if (errors.length > 0) {
                this.setState({
                    errors: errors  
                  })
            }else{
                this.setState({ submit_status: true });
                this.setState({ spinner: true });
                const cimonCareers = { ...this.state.cimonCareers };
                cimonCareers['post_date'] = this.state.releasedate;
                
                const response = await settingsService.updateCareer(cimonCareers);
                if (response) {
                    if(response.data.status === 1){
                        this.setState({ spinner: false });
                        this.setState({
                            message: response.data.message,
                            message_type: "success",
                          });
                          this.setState({ submit_status: false });
                          setTimeout(() => { 
                        //    window.location.reload();
                          }, 2000);
                    } else {
                        this.setState({ spinner: false });
                        this.setState({ submit_status: false });
                        this.setState({
                            message: response.data.message,
                            message_type: "error",
                          });
                    }
                }
            }
        }
    };

    handledateChange = (date) => {
      this.setState({
        releasedate: date,
      });
    };
    
    render() { 
        const { category } = this.state;
        let optionTemplateType = this.state.JobTypes.map(v => (
          <option key={v.id} value={v.id} selected={this.state.cimonCareers.job_type == v.id ? v.id : ""} >{v.name}</option>
        ));
        return ( <React.Fragment>
            <div className="container-fluid admin-body">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div>
               
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4" >  
                    <div className="addpage-form">
                      <BlockUi tag="div" blocking={this.state.spinner} >
                        <div  className="row addpage-form-wrap">
                            <div className="col-lg-8 col-md-12">                                
                                                           
                                <div className="form-group">
                                    <label htmlFor="">Update Career</label>
                                    <input name="job_name" 
                                    value={this.state.cimonCareers.job_name} 
                                    onChange={(e) => this.handleChange(e)}
                                    type="text"
                                    placeholder="Job title *" 
                                    className="form-control"/>
                                    {this.state.errors.job_name ? (<div className="error text-danger">
                                            {this.state.errors.job_name}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                 <div className="form-group">
                                    <label htmlFor="">Slug</label>
                                    <input name="slug" 
                                    value={this.state.cimonCareers.slug} 
                                    onChange={(e) => this.handleChange(e)}
                                    type="text" placeholder="slug *" className="form-control"/>
                                    {this.state.errors.slug ? (<div className="error text-danger">
                                            {this.state.errors.slug}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>    

                                <div className="form-group">
                          <label htmlFor=""> Meta tag</label>
                          <input
                            name="metatag"
                            value={this.state.cimonCareers.meta_tag}
                            onChange={e => this.handleChange(e)}
                            type="text"
                            placeholder="Meta Tag *"
                            className="form-control"
                          />
                          {this.state.errors.meta_tag ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_tag}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        

                        <div className="form-group">
                          <label htmlFor="">Meta Description</label>
                          <input
                            name="metadescription"
                            value={this.state.cimonCareers.meta_desc}
                            onChange={e => this.handleChange(e)}
                            type="text"
                            placeholder="Meta Description *"
                            className="form-control"
                          />
                          {this.state.errors.meta_tag ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_desc}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Select Category</h5>

                                    <select
                                     value={this.state.cimonCareers.related_cat}
                                     className='form-group-relase'
                                     onChange={this.handleselectcat}
                                    >

                                        <option value="">Category</option>
                                        {
                                          category.length > 0 ? (
                                            category.map((data, index) => {
                                              return (
                                              <>
                                                <option key={data._id} value={data._id}>{data.title}</option> 
                                              </>);
                                          }) ) : ""
                                        }
                                      {/* <option value="">Category</option>
                                      <option value="cadfile">CAD File</option>
                                      <option value="software">Software</option>
                                      <option value="manual">Manual</option>
                                      <option value="tech-note">Tech Note</option>
                                      <option value="sample-project">Sample Project</option>
                                      <option value="catalog-files">Catalog Files</option> */}
                                    </select>        
                                          
                            </div> 
                          </div> 
                        </div>


                                <div className="form-group">
                                <div className="row">
                                    <div className="col-md-12">
                                      <label htmlFor="">Content</label>
                                      <CKEditor
                                          data={this.state.cimonCareers.content}
                                          onBeforeLoad={CKEDITOR => {
                                            CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                          }
                                          }
                                          config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                              allowedContent: true,
                                              filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                              } }

                                          onInit = { 
                                              editor => {          
                                              } 
                                          }
                                          onChange={this.onEditorChange}
                                      />
                                    </div> 
                                </div>
                                </div>
                            
                            
                            
                            
                     
                            </div>
                            <div className="col-lg-4 col-md-12 ">      


                                       <div className='form-group relDate'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Release Date</h5>
                                
                                <DatePicker
                                  selected={this.state.releasedate}
                                  placeholderText="Release Date *"
                               //   maxDate={new Date()}                                  
                                  dateFormat="dd-MM-yyyy"                                  
                                  onChange={this.handledateChange}
                                  // className= {form-control}
                                />
          
                            
                            </div> 
                          </div> 
                        </div>   




                                <div className="form-group">
                                    <label htmlFor="">Job Type</label>
                                    <select  name="job_type" className="form-control" onChange={this.handleChange}>
                                        <option value="">Select Job Type</option>
                                        {optionTemplateType}
                                    </select>
                                    {this.state.errors.job_type ? (<div className="error text-danger">
                                            {this.state.errors.job_type}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>   

                                <div className="form-group">
                                    <label htmlFor="">Job Location</label>
                                    <CountryDropdown
                                        value={this.state.cimonCareers.job_location}
                                        onChange={(val) => this.selectCountry(val)}
                                        className="form-control"
                                    />
                                    
                                    {this.state.errors.job_location ? (<div className="error text-danger">
                                            {this.state.errors.job_location}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>   
                                   
                                    <div className="faq-sideinputs">

                                    <div className="faq-btns form-btn-wrap">   
                                      <div className="update_btn input-group-btn float-right"> 
                                        <button
                                        disabled={this.state.submit_status === true ? "disabled" : false}
                                        onClick={this.handleSubmit} className="btn btn-info">
                                        {this.state.submit_status ? (
                                        <ReactSpinner type="border" color="dark" size="1" />
                                        ) : (
                                        ""
                                        )}
                                        Update
                                        </button>
                                      </div>
                                    </div>
                                    {this.state.message !== "" ? (
                                    <div className="tableinformation">
                                      <div className={this.state.message_type}>
                                      <p>{this.state.message}</p>
                                      </div>
                                    </div>
                                    ) : null
                                    }       

                                </div>
                            </div>
                        </div>
                      </BlockUi>
                     </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment> );
    }
}
 
export default AdminEditCareer;