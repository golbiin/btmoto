import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import { CountryDropdown} from "react-country-region-selector";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl} from "../../../src/config.json";
import BlockUi from 'react-block-ui';
import * as CategoryService from "../../ApiServices/admin/manageCareerCategory";
class AdminAddCareer extends Component {
    constructor(props) {
        super(props);
        this.onEditorChange = this.onEditorChange.bind( this ); 
    }
    state = { 
        JobTypes: [
            { name: 'Part-Time', id: 'Part-Time' },
            { name: 'Full-Time', id: 'Full-Time' },
        ],

        JobLocation: [
            { name: 'Ontario', id: 'Ontario' },
            { name: 'Alberta', id: 'Alberta' },
        ],
        editorContent : "",
        checked: false , 
        data: { 
                job_name: "" , 
                slug: "",
                job_type:"",
                job_location:"",
                meta_tag : '',
                meta_desc :'',
        },
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        editordata: '',
        related_cat : '',
        focused: false,
        spinner: false,
        category : []
    }
    onEditorChange( evt ) {
		this.setState( {
			editordata: evt.editor.getData()
		} );
    }
    
    /*Joi validation schema*/  
    schema = {
        job_name: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        slug: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        meta_tag :  Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    
      meta_desc : Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
        job_type: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        job_location: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
    };
   
    /* Input Handle Change */
    selectCountry = async (val) => {
        const data = { ...this.state.data };
        data["job_location"] = val;
        this.setState({ data });
    };
    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const data = { ...this.state.data };
        const errorMessage = this.validateProperty(input);
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        data[input.name] = input.value;
        this.setState({ data, errors });
    };

    /* Joi Validation Call */
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };
    handleselectcat = async e => {
        this.setState({ related_cat: e.target.value })
      } 
    /* Form Submit */
    handleSubmit = async () => {
        const data = { ...this.state.data };
        const errors = { ...this.state.errors };
        let result = Joi.validate(data, this.schema); 
        if (result.error) {      
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
                this.setState({
                    errors: errors  
                  })
            } else {
                if (errors.length > 0) {
                    this.setState({
                        errors: errors  
                    })
                 }else{
                    const career_data = {
                        job_name: this.state.data.job_name,
                        slug: this.state.data.slug, 
                        meta_tag: this.state.data.meta_tag,
                        meta_desc: this.state.data.meta_desc,
                        job_type : this.state.data.job_type,
                        job_location : this.state.data.job_location,
                        related_cat : this.state.related_cat,
                        content : this.state.editordata,
                    }          
                    this.setState({ submit_status: true });
                    this.setState({ spinner: true });
                    const response = await settingsService.createCareer(career_data);
                    if (response) {
                        if(response.data.status === 1){
                            this.setState({ spinner: false });
                            this.setState({
                                message: response.data.message,
                                message_type: "success",
                            });
                            this.setState({ submit_status: false });
                            setTimeout(() => { 
                                window.location.reload();
                            }, 2000);
                        } else {
                            this.setState({ spinner: false });
                            this.setState({ submit_status: false });
                            this.setState({
                                message: response.data.message,
                                message_type: "error",
                            });
                        }
                    }

                }
             }
        };

    componentDidMount = async () => {
      this.getCareerCategory();    
    }

    /* getCareerCategory */
    getCareerCategory = async e => {

      console.log('getCareerCategory');
      const response = await CategoryService.getAllCategorys();
      if(response.data.status == 1){
        
        this.setState({category:response.data.data});
        
      }else{
        this.setState({
          newsStatus: false,
          message: response.data.message,
          responsetype: "error",
        });
        this.setState({spinner:false});
      }
      
    };

    render() { 
      const { category } = this.state;
        let optionTemplateType = this.state.JobTypes.map(v => (
            <option key={v.id} value={v.id} >{v.name}</option>
          ));

        let optionTemplateLocation = this.state.JobLocation.map(v => (
        <option key={v.id} value={v.id}>{v.name}</option>
        ));

        return ( <React.Fragment>
            <div className="container-fluid admin-body">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div>
               
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4" >  
                    <div className="addpage-form">
                        <BlockUi tag="div" blocking={this.state.spinner} >
                        <div  className="row addpage-form-wrap">
                            <div className="col-lg-8 col-md-12">
                               
                                                       
                                <div className="form-group">
                                    <label htmlFor="">Add New Career</label>
                                    <input name="job_name" onChange={this.handleChange}  type="text" placeholder="Job title *" 
                                    className="form-control"/>
                                    {this.state.errors.job_name ? (<div className="error text-danger">
                                            {this.state.errors.job_name}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>

                                 <div className="form-group">
                                    <label htmlFor="">Slug</label>
                                    <input name="slug" onChange={this.handleChange}  
                                     value={this.state.slug}
                                    type="text" placeholder="slug *" className="form-control"/>
                                    {this.state.errors.slug ? (<div className="error text-danger">
                                            {this.state.errors.slug}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>

                                <div className="form-group">
                            <label htmlFor="">  Meta tag</label>
                            <input
                              name="meta_tag"
                              onChange={this.handleChange}
                              value={this.state.meta_tag}
                              type="text"
                              placeholder="Meta tag *"
                              className="form-control"
                            />
                          
                            {this.state.errors.meta_tag ? (
                              <div className="error text-danger">
                                {this.state.errors.meta_tag}
                              </div>
                            ) : (
                              ""
                            )}
                          </div>

                          <div className="form-group">
                            <label htmlFor=""> Meta Description</label>
                            <input
                              name="meta_desc"
                              onChange={this.handleChange}
                              value={this.state.meta_desc}
                              type="text"
                              placeholder="Meta Description *"
                              className="form-control"
                            />
                            {this.state.errors.meta_desc ? (
                              <div className="error text-danger">
                                {this.state.errors.meta_desc}
                              </div>
                            ) : (
                              ""
                            )}
                          </div>
                               
                                <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Select Category</h5>

                                    <select
                                     value={this.state.related_cat}
                                     className='form-group-relase'
                                     onChange={this.handleselectcat}
                                    >
                                      {/* <option value="">Category</option>
                                      <option value="cadfile">CAD File</option>
                                      <option value="software">Software</option>
                                      <option value="manual">Manual</option>
                                      <option value="tech-note">Tech Note</option>
                                      <option value="sample-project">Sample Project</option>
                                      <option value="catalog-files">Catalog Files</option> */}

                                      <option value="">Category</option>
                                      {
                                        category.length > 0 ? (
                                          category.map((data, index) => {
                                            return (
                                            <>
                                              <option key={data._id} value={data._id}>{data.title}</option> 
                                            </>);
                                        }) ) : ""
                                      }
                                    </select>        
                                          
                            </div> 
                          </div> 
                        </div>

                                <div className="form-group">
                                    <div className="row">
                                    <div className="col-md-12">
                                    <label htmlFor="">Content</label>
                                    <CKEditor
                                        data={this.state.editordata}
                                        onBeforeLoad={CKEDITOR => {
                                          CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                        }
                                        }
                                        config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            allowedContent: true,
                                            filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                            } }

                                        onInit = { 
                                            editor => {          
                                            } 
                                        }
                                        onChange={this.onEditorChange}
                                        style={{
                                            float: 'left',
                                            width: '99%'
                                        }}
                                    />
                                    </div>
                                    </div>

                                </div>
                            </div>
                            

                            <div className="col-lg-4 col-md-12 ">
                                <div className="form-group">
                                    <label htmlFor="">Job Type</label>
                                    <select  name="job_type" className="form-control" onChange={this.handleChange}>
                                        <option value="">Select Job Type</option>
                                        {optionTemplateType}
                                    </select>
                                    {this.state.errors.job_type ? (<div className="error text-danger">
                                            {this.state.errors.job_type}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>   

                                <div className="form-group">
                                    <label htmlFor="">Job Location</label>
                                    <CountryDropdown
                                        value={this.state.data.job_location}
                                        onChange={(val) => this.selectCountry(val)}
                                        className="form-control"
                                    />
                                    {this.state.errors.job_location ? (<div className="error text-danger">
                                            {this.state.errors.job_location}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div> 
                                <div className="faq-sideinputs">   
                                    <div className="faq-btns form-btn-wrap">   
                                     <div className="float-left">
                                     </div>
                                     <div className="update_btn input-group-btn float-right"> 
                                     <button
                                     disabled={this.state.submit_status === true ? "disabled" : false}
                                     onClick={this.handleSubmit} className="btn btn-info">
                                     {this.state.submit_status ? (
                                            <ReactSpinner type="border" color="dark" size="1" />
                                        ) : (
                                            ""
                                        )}
                                     Publish
                                     </button></div>
                                </div>
                                {this.state.message !== "" ? (
                                    <p className="tableinformation">
                                    <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                    </div>
                                    </p>
                                    ) : null
                                    }           
                                </div>
                            </div>
                        </div>
                        </BlockUi>
                     </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment> );
    }
}
 
export default AdminAddCareer;