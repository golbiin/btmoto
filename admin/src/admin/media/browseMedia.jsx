import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactSpinner from 'react-bootstrap-spinner';
import * as mediaUploads1 from "../../ApiServices/admin/mediaFiles";
class browseMedia extends Component {
  
    constructor(props) {
        super(props);
        this.state = {
            dateFilterValue: '',
            typeFilterValue: '',
            dateFilterOptions: [],
            typeFilterOptions: [],    
            search: '',    
            spinner: true,   
            data_table:[],
            selectedRow : {},
            CKEditorFuncNum : ''
        }
    }

    componentDidMount = () => {
        this.getDateFilterOptions();
        this.getTypeFilterOptions();
        this.getAllmediafiles();
        this.setState({CKEditorFuncNum : new URLSearchParams(this.props.location.search).get("CKEditorFuncNum") });
        
    };

    /* Get all files*/
    getDateFilterOptions = async () => {
        this.setState({ spinner: true});
        try {
        const response =  await mediaUploads1.getDateFilterOptions();
        if (response.data.status === 1) {
            this.setState({dateFilterOptions: response.data.options[0].created_on});
        } else {
            this.setState({dateFilterOptions: []});
        }
        this.setState({ spinner: false});
        } catch (err) {
        this.setState({ spinner: false});
        }
    }

    /* Get all types*/
    getTypeFilterOptions = async () => {
        this.setState({ spinner: true});
        try {
        const response =  await mediaUploads1.getTypeFilterOptions();
        if (response.data.status === 1) {
            this.setState({typeFilterOptions: response.data.options});
        } else {
            this.setState({typeFilterOptions: []});
        }
        this.setState({ spinner: false});
        } catch (err) {
        this.setState({ spinner: false});
        }
    }

    /* Get all files*/
    getAllmediafiles = async () => {
        this.setState({ spinner: true});
        try {

            const filter = { 'date': this.state.dateFilterValue, 'type': this.state.typeFilterValue };
            console.log(filter);
            const response =  await mediaUploads1.getAllfiles(filter);
            if (response.data.status === 1) {
                const data_table_row = [];
                response.data.data.map(  (media,index) => {
                    const Setdata = {};
                    Setdata.file_name =  media.file_name;
                    Setdata.file_url =  media.file_url;
                    Setdata.file_type =  media.file_type;
                    Setdata._id =  media._id;
                    Setdata.created_on = media.created_on;
                    // let svgImage    =   '';
                    // const extension = media.file_url.split('.').pop();
                    // if(extension == 'svg') {
                    //     const svgImage = await this.getSvg(media);
                    // }
                    // Setdata.svgImage = svgImage;

                    data_table_row.push(Setdata);
                });
                this.setState({ 
                data_table: data_table_row
                });                
            } else {
                const data_table_row = [];
                this.setState({ 
                data_table: data_table_row
                });
            } 
            this.setState({ spinner: false});
        } catch (err) {
            this.setState({ spinner: false});
        }
    }

    getSvg = async (row) => {
        const res = await fetch(row.file_url);
        return ( <div dangerouslySetInnerHTML={{ __html: res}}/>)
    }
    
    getImage = (row) => {

        const images = require.context("../../assets/images", true);
        let imgUrl = images(`./admin/document.png`);
        if (row.file_type === 'image' ) {
            imgUrl = (row.file_url);
        } else if ( row.file_type == 'audio') {
            imgUrl = images(`./admin/audio.png`)
        } else if ( row.file_type == 'video') {
            imgUrl = images(`./admin/video.png`)
        } else if ( row.file_type == 'document') {
        imgUrl = images(`./admin/document.png`)
        } else if ( row.file_type == 'archive') {
            imgUrl = images(`./admin/archive.png`)
        }
        const extension = row.file_url.split('.').pop();
        if(extension == 'svg') {
            return 'svg';
        } else  {
            return (<img className='img-fluid media-cimon' src={imgUrl} />)
        }
    }

    selectImage = (row) => {
        this.setState ({selectedRow : row}, () => {
            console.log(this.state.selectedRow);
        });

    }
    handleInsert = () => {
        if(this.state.selectedRow.file_url)   {  
            window.opener.CKEDITOR.tools.callFunction( Number(this.state.CKEditorFuncNum), this.state.selectedRow.file_url );
            window.close();
        }
    }
    searchSpace = event => {
        let keyword = event.target.value
        this.setState({ search: keyword })
    }

    handleSearchByDate = event => {
        let keyword = event.target.value
        this.setState({ dateFilterValue: keyword })
        this.setState({
        dateFilterValue: keyword
        }, () => {
        this.getAllmediafiles();
        });
    }

    handleSearchByType = event => {
        let keyword = event.target.value
        this.setState({ typeFilterValue: keyword })
        this.setState({
        typeFilterValue: keyword
        }, () => {
        this.getAllmediafiles();
        });
    }

    render () {
        
        let rowData = this.state.data_table;
        let search = this.state.search
        if(search.length > 0){
            search = search.trim().toLowerCase();
            rowData = rowData.filter(l => {
                return (l.file_name.toLowerCase().match( search ) ||
                l.file_url.toLowerCase().match( search ) );
            });
        }

        let dateFilterOptionHtml = this.state.dateFilterOptions.map(dateObj =>(
            <option key={dateObj.month + '/' + dateObj.year} value={dateObj.month + '/' + dateObj.year} >{dateObj.month + '/' + dateObj.year}</option>
        ));

        let typeFilterOptionHtml = this.state.typeFilterOptions.map(typeObj =>( 
            <option key={typeObj._id} value={typeObj._id} >{typeObj._id}</option>
        ));
        return (
        <React.Fragment>
            
            <div className='container-fluid admin-body browse-media'>
                <div className='admin-row'>

                    <div className='col-md-12 col-sm-12'>
                        <div className='row content-row'>

                            <div className='col-md-12  contents products home-inner-content pt-4 pb-4 pr-4'>
                                <div className='main_admin_page_common product-summary-tab-wrapper'>
                                    <div className=''>
                                        <div className='admin_breadcum'>
                                            <div className='row'>
                                            <div className='col-md-1'>
                                                <p className='page-title'>Media</p>
                                            </div>
                                            <div className='col-md-2'>
                                                <div className='bulkaction'>
                                                <div className='commonserachform'>
                                                    <select
                                                    className="form-control"
                                                    name="date_filter"
                                                    onChange={e => this.handleSearchByType(e)}
                                                    >
                                                    <option value="">All Types</option>
                                                    {typeFilterOptionHtml}
                                                    </select>
                                                </div>
                                                </div>
                                            </div>
                                            
                                            <div className='col-md-3'>
                                                <div className='bulkaction'>
                                                <div className='commonserachform'>
                                                    <select
                                                    className="form-control"
                                                    name="date_filter"
                                                    onChange={e => this.handleSearchByDate(e)}
                                                    >
                                                    <option value="">All Dates</option>
                                                    {dateFilterOptionHtml}
                                                    </select>
                                                </div>
                                                </div>
                                            </div>
                                            <div className='col-md-3'>
                                                <div className='searchbox'>
                                                <div className='commonserachform'>
                                                    <span />
                                                    <input
                                                    type='text'
                                                    placeholder='Search'
                                                    onChange={e => this.searchSpace(e)}
                                                    name='search'
                                                    className='search form-control'
                                                    />
                                                    <input type='submit' className='submit_form' />
                                                </div>
                                                </div>
                                            </div>
                                            </div>
                                            <div className='col-md-12 insert-media'>
                                                <div className='bulkaction'>
                                                <div className='commonserachform'>
                                                    <button className="btn btn-info"
                                                    onClick={e => this.handleInsert()}
                                                    >
                                                    Insert
                                                    </button>
                                                </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div className="faq-list-table-wrapper">
                                    
                                        <React.Fragment>
                                        <div style={{ display: this.state.spinner === true || rowData.length == 0 ? 'flex justify-content-center ' : 'none' }} className="overlay text-center">
                                                <ReactSpinner type="border" color="primary" size="10" />
                                        </div>
                                        </React.Fragment>
                                        <div className='col-md-8 media-box'>
                                        {rowData.map( (row, index) => {
                                            return(

                                                <div className={"attachment-preview " + (this.state.selectedRow.file_url == row.file_url ? 'selected' : '')}    
                                                    key={index}  
                                                    onClick={() => this.selectImage(row)}
                                                    
                                                >
                                                    <div className = 'thumbnail'>
                                                        <div className="centered">
                                                            {this.getImage(row)}
                                                            <button type="button" className="check" >
                                                                <span className="media-modal-icon"></span>                                                                
                                                            </button>
                                                        </div>                                                    
                                                    </div>
                                                </div>

                                            )
                                        })}
                                        </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
    
        </React.Fragment>
        )
    }
}

export default browseMedia

