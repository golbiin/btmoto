import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import * as mediaUploads from "../../ApiServices/admin/mediaFiles";
import ReactSpinner from "react-bootstrap-spinner";
import { Link } from 'react-router-dom';

class MediaUpload extends Component {
  constructor(props) {
    super(props);
    this.hiddenFileInput = React.createRef();
    this.uploadMultipleFiles = this.uploadMultipleFiles.bind(this);
  }

  state = {
    search: "",
    spinner: true,
    message: "",
    message_type: "",
    upload_status: false,
    submit_status: false,
    fileArray: [],
   data:[],
 
  };
  handleClick = (event) => {
    this.hiddenFileInput.current.click();

  };

/******* Custom Drag and Drop***********/

dragOver = (e) => {
  e.preventDefault();
}

dragEnter = (e) => {
  e.preventDefault();
}

dragLeave = (e) => {
  e.preventDefault();
}

fileDrop = (e) => {
  e.preventDefault();
  const files = e.dataTransfer.files;
  console.log(files);
  this.uploadMultipleFiles(e, "drop");
}

/* Upload multiple  Files */

uploadMultipleFiles = async (e, uploadType="select") => {
  let files = [];
  if(uploadType === "drop"){
    files = e.dataTransfer.files ? e.dataTransfer.files : e;
  }
  else{
    files = e.target.files ? e.target.files : e;
  }

  console.log('uploadMultipleFiles' , files);
   this.setState({ upload_status: true });

    const response1 = await mediaUploads.uploadFiles(files);
    if (response1.data.status == 1) {

      
      response1.data.data.file_location.map((item, key) =>{
          this.setState({
            fileArray: this.state.fileArray.concat( item )        
        });
      }
       
      );
      this.setState({ upload_status: false });
     try { 
      const data = [...this.state.fileArray ];
      const response = await mediaUploads.saveFiles(data);
      console.log("response-front",response);
      if (response) {
        if (response.data.status === 1) {
          this.setState({ spinner: false });
          this.setState({
            message: response.data.msg,
            message_type: "success",
          });
          this.setState({ submit_status: false });
          setTimeout(() => {
            this.props.history.push({
              pathname: "/admin/uploads/",
            });
          }, 5000);
        } else {
          this.setState({ spinner: false });
          this.setState({ submit_status: false });
          this.setState({
            message: response.data.message,
            message_type: "error",
          });
        }
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
      
    } 
    
    else {
      this.setState({
        submitStatus: false,
        message: response1.data.message,
        responsetype: "error",
      });
      this.setState({ upload_status: false });
    }
  };

 
  

  getImage = (row, index) => {

    console.log(row);
    const images = require.context("../../assets/images", true);
    let imgUrl = images(`./admin/document.png`);
    if (row.filetype === 'image' ) {
      imgUrl = (row.location);
    } else if ( row.filetype == 'audio') {
      imgUrl = images(`./admin/audio.png`)
    } else if ( row.filetype == 'video') {
      imgUrl = images(`./admin/video.png`)
    } else if ( row.filetype == 'document') {
      imgUrl = images(`./admin/document.png`)
    } else if ( row.filetype == 'archive') {
      imgUrl = images(`./admin/archive.png`)
    }

    if(row.location !== "") {
      return (
        <div className = "media-row">
        <img id={index}
        alt="..."
        width="80px"
        height="70px" className='img-fluid' src={imgUrl} /> <span> {row.originalname} uploaded successfully </span></div>)
    } else {
      return(
        <div className="media-row media-error"> 
          <p><b>"{row.originalname}" has failed to upload.</b></p>
          <p>{row.message}</p>
        </div>
      )
    }
  }
 
 
    showMedias = async (e) => {
     
     
      //this.props.history.push('/admin/uploads')
      }

    
  render() {

    const images = require.context("../../assets/images/admin", true); 
    return (
      <React.Fragment>
        <div className="container-fluid admin-body media-upload-page">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                  
            
                <div className="col-md-12  fileupload  home-inner-content pt-4 pb-4 pr-4">
                  <div className="main_admin_page_common product-summary-tab-wrapper">
                    <div className="">
                      <div className="mediaupload"
                      onDragOver={this.dragOver}
                      onDragEnter={this.dragEnter}
                      onDragLeave={this.dragLeave}
                      onDrop={this.fileDrop}
                      >
                        <div
                          id="mediaupload-ui"
                          className="hide-if-no-js drag-drop"
                        >

                          <div id="drag-drop-area">

                         
                            <div className="drag-drop-inside">
                              <p className="drag-drop-info">Drop files here</p>
                              <p>or</p>
                              <div className="drag-drop-buttons">
                                <input
                                  ref={this.hiddenFileInput}
                                  id="myInput"
                                  type="file"
                                  className="form-control"
                                  onChange={this.uploadMultipleFiles}
                                  multiple
                                  style={{ display: "none" }}
                                />
                        
                                <button
                                  onClick={this.handleClick}
                                  className="btn btn-info"
                                >
                                  Select Files
                                </button>

                                {this.state.upload_status === true ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                    margin="auto"
                                    display="block"
                                  />
                                ) : (
                                  ""
                                )}
                              </div>
                        
                            </div>
                          </div>
                        </div>
                      </div>
                  {this.state.message !== '' ? (
                        <div className='tableinformation'>
                          <div className={this.state.message_type}>
                            <p>{this.state.message}</p>
                          </div>
                        </div>
                      ) : null}
                      <div className="multi-preview">
                                        {this.state.fileArray.map(
                                          (file, index) => (

                                            <div className="single-img" key={index}>

                                              {this.getImage(file, index)}
                                              {/* <img
                                                src={file.file_url}
                                                id={index}
                                                alt="..."
                                                width="80px"
                                                height="70px"
                                              /> */}                                             
                                            </div>
                                          )
                                        )}
                                      </div>


                      
                    </div>
                  </div>
                  <Link  to={"/admin/uploads"} className="media_back">
                      Back
                </Link>
                   
                
                </div>
              </div>
            </div>
          </div>
        </div>

   
      </React.Fragment>
    );
  }
}

export default MediaUpload;
