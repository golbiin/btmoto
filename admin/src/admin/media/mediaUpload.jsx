import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import * as mediaUploads1 from "../../ApiServices/admin/mediaFiles";
import DeleteConfirm from '../common/deleteConfirm'
import TitleEdit from '../common/titleEdit'
import ReactSpinner from 'react-bootstrap-spinner'
import memoize from 'memoize-one'
class MediaUpload extends Component {

  
  constructor(props) {
    super(props);
  this.state = {
    dateFilterValue: '',
    typeFilterValue: '',
    dateFilterOptions: [],
    typeFilterOptions: [],
    selectedRows: [], 
    bulkActionSelected: '',
    filterMonthYears: '',
    modalShow: false,
    name: "",
    modalInputName: "",
   // modalShow: false,
  //  settitleModal:false,
    confirm: {
      name: "Delete",
      message: "Are you sure you'd like to delete this item?",
      action: "delete"
    },
    search: '',
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    spinner: true,
    toggleCleared: false,
    message: '',
    message_type: '',
    columns : [
      {
        name: 'Image',
        selector: 'file_url',
        cell: this.getImage,
        allowOverflow: true,
        button: true,
        width: '60px', 
        hide: 'sm'
      },
      {
        name: 'Name',
        selector: 'file_name',
        left: true,
        sortable: true,
        hide: 'sm',
        width: '300px',
      },
      {
        name: 'Url',
        selector: 'file_url',
        left: true,
        hide: 'sm',
       width: '500px',
       cell: row => <div><a target="_blank" href={row.file_url}>{row.file_url}</a></div>,
      },
      {
        name: 'Date',
        selector: 'created_on',
         left: true,
         sortable: true,
        hide: 'sm',
        width: '300px',
      },
      {
        name: '',
        cell:  row =><div className="action_dropdown" data-id={row._id}><i data-id={row._id} className="dropdown_dots"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
        <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">

      <Link
        onClick={() => this.saveAndtoogle(row._id, 'delete')}
        className='dropdown-item'
      >
        Delete
      </Link>
             
      <Link
        onClick={() => this.modalOpen(row._id)}
      // onClick={() => this.showTitleEdit(row._id)}
      className="dropdown-item">
      Edit
      </Link> 
        </div>
        </div>,
        allowOverflow: false,
        button: true,
        width: '56px', // custom width for icon button
      },
    ],

    toggleCleared: false,
    contextActions_select: '-1',
    activetab: 'tab_a',
    copySuccess: false
   
  }
  this.modalOpen = this.modalOpen.bind(this)
  }


  componentDidMount = () => {
    this.getDateFilterOptions();
    this.getTypeFilterOptions();
    this.getAllmediafiles();
    
  };

  modalOpen = (e) => {
    this.setState({ modalShow: true });
    this.setState({ rowId: e });
  }

  modalClose(e) {
    this.setState({
      modalInputName: "",
      modalShow: false
    });
  }

   /* Get all files*/
  getDateFilterOptions = async () => {
    this.setState({ spinner: true});
    try {
      const response =  await mediaUploads1.getDateFilterOptions();
      if (response.data.status === 1) {
          this.setState({dateFilterOptions: response.data.options[0].created_on});
      } else {
        this.setState({dateFilterOptions: []});
      }
      this.setState({ spinner: false});
    } catch (err) {
      this.setState({ spinner: false});
    }
  }

  /* Get all types*/
  getTypeFilterOptions = async () => {
    this.setState({ spinner: true});
    try {
      const response =  await mediaUploads1.getTypeFilterOptions();
      if (response.data.status === 1) {
          this.setState({typeFilterOptions: response.data.options});
      } else {
        this.setState({typeFilterOptions: []});
      }
      this.setState({ spinner: false});
    } catch (err) {
      this.setState({ spinner: false});
    }
  }

   /* Get all files*/
  getAllmediafiles = async () => {
    this.setState({ spinner: true});
      try {

        const filter = { 'date': this.state.dateFilterValue, 'type': this.state.typeFilterValue };
        console.log(filter);
        const response =  await mediaUploads1.getAllfiles(filter);
          if (response.data.status === 1) {
            const data_table_row = [];
            response.data.data.map((media,index) => {
            const Setdata = {};
            Setdata.file_name =  media.file_name;
            Setdata.file_url =  media.file_url;
            Setdata.file_type =  media.file_type;
            Setdata._id =  media._id;
            Setdata.created_on = media.created_on;
            data_table_row.push(Setdata);
            });
            this.setState({ 
            data_table: data_table_row
            });                
          } else {
            const data_table_row = [];
            this.setState({ 
              data_table: data_table_row
            });
          } 
        this.setState({ spinner: false});
      } catch (err) {
        this.setState({ spinner: false});
      }
  }

  
  getImage = (row) => {

    console.log(row);
    const images = require.context("../../assets/images", true);
    let imgUrl = images(`./admin/document.png`);
    if (row.file_type === 'image' ) {
      imgUrl = (row.file_url);
    } else if ( row.file_type == 'audio') {
      imgUrl = images(`./admin/audio.png`)
    } else if ( row.file_type == 'video') {
      imgUrl = images(`./admin/video.png`)
    } else if ( row.file_type == 'document') {
      imgUrl = images(`./admin/document.png`)
    } else if ( row.file_type == 'archive') {
      imgUrl = images(`./admin/archive.png`)
    }

    return (<img className='img-fluid media-cimon' src={imgUrl} />)
  }
  
  doAction = (action) => {
    switch(action) {
      case 'delete':
        this.deleteEntry();
        break;
      case 'bulk_delete' :
        this.bulkDelete();
        break;
    }
  }

  saveAndtoogle = (id, action) => {
    switch(action) {

      case 'delete':
        this.setState({
          confirm:{
            name: "Delete",
            message: "Are you sure you'd like to delete this item?",
            action: action
          } 
        });
        break;
      case 'bulk_delete':
        this.setState({
          confirm:{
            name: "Bulk Delete",
            message: "Are you sure you want to delete these items? ",
            action: action
          } 
        });
        break;
    }
    if(action != '') {
      this.setState({ index: id })
      this.toogle()
    }
  }
  saveAndEdit = (id, action) => {
    switch(action) {

      case 'edit':
        this.setState({        
            action: action
        });
        break;
    }
    this.setState({ index: id })
    this.toogle()
  }
  toogle = () => {
    let status = !this.state.modal
    this.setState({ modal: status })
  }
  /* delete Entries   */
  deleteEntry = async () => {
    
    let id = this.state.index
    const response = await mediaUploads1.deleteEntry(id)
    if (response) {
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success",
        });      
      setTimeout(() => {
        this.setState(() => ({ message: '' }))
      }, 2000);
        this.getAllmediafiles();
        this.toogle()                
      }
    }
  }
  bulkDelete = async () => {
    let id = this.state.index;
    console.log(id);
    const response = await mediaUploads1.bulkDelete(id)
    if (response) {
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success",
        });    
        this.setState({ selectedRows: [] })  
        setTimeout(() => {
          this.setState(() => ({ message: '' }))
        }, 2000);
        this.getAllmediafiles();
        this.toogle()                
      }
    }
  }

  searchSpace = event => {
    let keyword = event.target.value
    this.setState({ search: keyword })
  }
  handleSearchByDate = event => {
    let keyword = event.target.value
    this.setState({ dateFilterValue: keyword })
    this.setState({
      dateFilterValue: keyword
      }, () => {
      this.getAllmediafiles();
    });
  }
  handleSearchByType = event => {
    let keyword = event.target.value
    this.setState({ typeFilterValue: keyword })
    this.setState({
      typeFilterValue: keyword
      }, () => {
      this.getAllmediafiles();
    });
  }
  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows })
  }

  handleBulkActionChange = async (event)  => {
    let action = event.target.value;  
    this.setState({bulkActionSelected : action});
  }
  handlebulkActionApply = async () => {
    const action = this.state.bulkActionSelected;
    const { selectedRows } = this.state
    const ids = selectedRows.map(r => r._id);
    console.log(ids.length);
    if(action) {
      this.saveAndtoogle(ids, action);
    } else {
      return;
    }
  }

  render () {
    
    let rowData = this.state.data_table
    let search = this.state.search
    if(search.length > 0){
      search = search.trim().toLowerCase();
      rowData = rowData.filter(l => {
          return (l.file_name.toLowerCase().match( search ) ||
           l.file_url.toLowerCase().match( search ) );
      });
    }
    const contextActions = memoize(actionHandler => (
      <div className='common_action_section'>
        <select
          value={this.state.bulkActionSelected}
          onChange={e => this.handleBulkActionChange(e)}
        >
          <option value=''>Select</option>
          <option value='bulk_delete'>Delete</option>
        </select>
        <button className='danger' onClick={this.handlebulkActionApply}>
          Apply
        </button>
      </div>
    ));
    let dateFilterOptionHtml = this.state.dateFilterOptions.map(dateObj =>(
      <option key={dateObj.month + '/' + dateObj.year} value={dateObj.month + '/' + dateObj.year} >{dateObj.month + '/' + dateObj.year}</option>
    ));

    let typeFilterOptionHtml = this.state.typeFilterOptions.map(typeObj =>( 
      <option key={typeObj._id} value={typeObj._id} >{typeObj._id}</option>
    ));
    return (
      <React.Fragment>
        
        <div className='container-fluid admin-body '>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents products home-inner-content pt-4 pb-4 pr-4'>
                  <div className='main_admin_page_common product-summary-tab-wrapper'>
                    <div className=''>
                      <div className='admin_breadcum'>
                        <div className='row'>
                          <div className='col-md-1'>
                            <p className='page-title'>Media</p>
                          </div>
                          <div className='col-md-3'>
                            <Link
                              className='add_new_btn media_add_new_btn'
                              to='/admin/uploads/addnew'
                            >
                              Add New
                            </Link>
                          </div>
                          <div className='col-md-2'>
                            <div className='bulkaction'>
                              <div className='commonserachform'>
                                <select
                                  className="form-control"
                                  name="date_filter"
                                  onChange={e => this.handleSearchByType(e)}
                                >
                                  <option value="">All Types</option>
                                  {typeFilterOptionHtml}
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-md-3'>
                            <div className='bulkaction'>
                              <div className='commonserachform'>
                                <select
                                  className="form-control"
                                  name="date_filter"
                                  onChange={e => this.handleSearchByDate(e)}
                                >
                                  <option value="">All Dates</option>
                                  {dateFilterOptionHtml}
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-md-3'>
                            <div className='searchbox'>
                              <div className='commonserachform'>
                                <span />
                                <input
                                  type='text'
                                  placeholder='Search'
                                  onChange={e => this.searchSpace(e)}
                                  name='search'
                                  className='search form-control'
                                />
                                <input type='submit' className='submit_form' />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <React.Fragment>
                      
                      </React.Fragment>
                      {this.state.message !== '' ? (
                        <div className='tableinformation' style={{ backgroundColor:'#fff'}}>
                          <div className={this.state.message_type}>
                            <p>{this.state.message}</p>
                          </div>
                        </div>
                      ) : null}

                            <div className="faq-list-table-wrapper">

                              
                   
                <React.Fragment>
                  <div style={{ display: this.state.spinner === true ? 'flex justify-content-center ' : 'none' }} className="overlay text-center">
                        <ReactSpinner type="border" color="primary" size="10" />
                  </div>
                </React.Fragment>
                <DataTable
                  columns={this.state.columns}
                  data={rowData}
                  highlightOnHover
                  pagination
                  selectableRowsVisibleOnly
                  selectableRows 
                  contextActions={contextActions(this.deleteAll)}
                  clearSelectedRows={this.state.toggleCleared}
                  onSelectedRowsChange={this.handlerowChange}
                  noDataComponent = {<p>There are currently no records.</p>}
                />
                </div>

                     </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Delete confirm box */}
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          name = {this.state.confirm.name}
          message = {this.state.confirm.message}
          action = { this.state.confirm.action}
          deleteUser = { this.doAction}
        />
   
        {this.state.modalShow==true ?
          <TitleEdit
            newrowId ={this.state.rowId}
            show={this.state.modalShow}
            // onHide={() => this.setState({ modalShow: false })}
            modalClose={() => this.setState({ modalShow: false })}
              // modalClose={this.modalClose}
              // modalClose={() => this.modalClose(e)}
              // showTitleEdit={this.showTitleEdit}
              // onHide={() => this.setState({ settitleModal: false })}
          />
          : ""
        }
		
      </React.Fragment>
    )
  }
}

export default MediaUpload

