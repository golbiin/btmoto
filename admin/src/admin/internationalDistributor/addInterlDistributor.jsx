import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import { apiUrl ,siteUrl} from "../../config.json";
import BlockUi from 'react-block-ui';
class AdminAddEventLocations extends Component {
  
    constructor(props) {
        super(props); 
    }
    state = { 
        
        data: { 
          location_name: "" , 
          location_url:"",
          latitude:"",
          longitude:"",   
        },
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        spinner: false,
    }
    
    /* Joi validation schema */  
    schema = {
      location_name: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        location_url: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
      latitude: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field",
        };
      }),
      longitude: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field",
          };
        }),   
        storenumber: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field",
          };
        }),      
    
    };
    /* Input Handle Change */
    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const data = { ...this.state.data };
        const errorMessage = this.validateProperty(input);        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        data[input.name] = input.value;
        this.setState({ data, errors });
    };

    /*Joi Validation Call*/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };
      
    /* Form Submit */
    handleSubmit = async () => {
        console.log(122,'sdsd');
        const data = { ...this.state.data };
        const errors = { ...this.state.errors };
        let result = Joi.validate(data, this.schema);
        if (result.error) {
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
                this.setState({
                    errors: errors  
                  })
        } else {
          if (errors.length > 0) {
              this.setState({
                  errors: errors  
                })
          }else{
              this.setState({ submit_status: true });
              this.setState({ spinner: true });              
              this.updateInternationalDistributor();
          }
        }
    };
    /* Add International Distributor */
    updateInternationalDistributor = async () => {
        const locationData = {
            location_name: this.state.data.location_name , 
            location_url:this.state.data.location_url,
            latitude:this.state.data.latitude,
            longitude:this.state.data.longitude,   
            storenumber:this.state.data.storenumber,        
                  
        }
        const response = await settingsService.createInternationalDistributor(locationData);
        console.log(90,response);
        if (response) {
            if(response.data.status === 1){
                this.setState({ spinner: false });
                this.setState({
                    message: response.data.message,
                    message_type: "success",
                  });
                  this.setState({ submit_status: false });
                  setTimeout(() => { 
                    window.location.reload();
                  }, 2000);
            } else {
                this.setState({ spinner: false });
                this.setState({ submit_status: false });
                this.setState({
                    message: response.data.message,
                    message_type: "error",
                  });
                  
            }
        }
    }

    render() { 
        let checkedCond  = this.state.checked;      
        return ( <React.Fragment>
            <div className="container-fluid admin-body">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div> 
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4" >  
                    <div className="addpage-form">
                        <BlockUi tag="div" blocking={this.state.spinner} >
                        <div  className="row addpage-form-wrap">
                            <div className="col-lg-8 col-md-12">                               
                                                       
                                <div className="form-group">
                                    <label htmlFor="">Add New</label>
                                    <input name="location_name" onChange={this.handleChange}  type="text" placeholder="Add Location Name *" 
                                    className="form-control"/>
                                    {this.state.errors.location_name ? (<div className="error text-danger">
                                            {this.state.errors.location_name}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Location Url</label>
                                    <input name="location_url" onChange={this.handleChange}  type="text" placeholder="Add Location Url *" 
                                    className="form-control"/>
                                    {this.state.errors.location_url ? (<div className="error text-danger">
                                            {this.state.errors.location_url}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                              

                                <div className="form-group">
                                    <label htmlFor="">Latitude</label>
                                    <input name="latitude" onChange={this.handleChange}  
                                     value={this.state.latitude}
                                    type="text" placeholder="Latitude *" className="form-control"/>
                                    {this.state.errors.latitude ? (<div className="error text-danger">
                                            {this.state.errors.latitude}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Longitude</label>
                                    <input name="longitude" onChange={this.handleChange}  
                                     value={this.state.longitude}
                                    type="text" placeholder="Longitude *" className="form-control"/>
                                    {this.state.errors.longitude ? (<div className="error text-danger">
                                            {this.state.errors.longitude}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>        


                                   <div className="form-group">
                                    <label htmlFor="">Store Number</label>
                                    <input name="storenumber" onChange={this.handleChange}  
                                     value={this.state.storenumber}
                                    type="text" placeholder="Store Number *" className="form-control"/>
                                    {this.state.errors.storenumber ? (<div className="error text-danger">
                                            {this.state.errors.storenumber}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>        



                            </div> 
                            <div className="col-lg-4 col-md-12 ">                                
                                <div className="faq-sideinputs">   
                                    <div className="faq-btns form-btn-wrap">   
                                     <div className="float-left">
                                     </div>
                                     <div className="update_btn input-group-btn float-right"> 
                                     <button
                                     disabled={this.state.submit_status === true ? "disabled" : false}
                                     onClick={this.handleSubmit} className="btn btn-info">
                                     {this.state.submit_status ? (
                                            <ReactSpinner type="border" color="dark" size="1" />
                                        ) : (
                                            ""
                                        )}
                                     Publish
                                     </button></div>
                                </div>
                                {this.state.message !== "" ? (
                                    <p className="tableinformation">
                                    <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                    </div>
                                    </p>
                                    ) : null
                                    }           
                                </div>
                            </div>
                        </div>
                        </BlockUi>
                     </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment> );
    }
}
 
export default AdminAddEventLocations;