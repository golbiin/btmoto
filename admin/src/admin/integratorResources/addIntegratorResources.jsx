import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import { Link } from "react-router-dom";
import ReactSpinner from "react-bootstrap-spinner";
import validate from "react-joi-validation";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import * as settingsService from "../../ApiServices/admin/integratorResources";
import * as manageUser from "../../ApiServices/admin/manageUser";
import ClassicEditorContent from "../common/classicEditor";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl} from "../../../src/config.json";
import * as manageCasestudy from '../../ApiServices/admin/manageCasestudy'
import MultiSelect from 'react-multi-select-component'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import * as manageProducts from "../../ApiServices/admin/products";
import * as manageIntegrator from "../../ApiServices/admin/integrator";
import BlockUi from 'react-block-ui';
import Moment from 'moment';

class AddIntegratorResources extends Component {
    constructor(props) {
        super(props);
        this.handleCheck = this.handleCheck.bind(this);

        this.onEditorChange = this.onEditorChange.bind( this );
        this.uploadSingleFiles = this.uploadSingleFiles.bind(this);
        
    }
    state = { 
   
      file_url: "",
      uploading: false,
      upload_status: false,
      multiple_upload_status :false,
      blocking: false,
      soft_upload_status: false,
      docum_upload_status: false,
      spinner: false,
      releasedate: new Date(),
        editorContent : "",
        checked: false , 
        data: { 
                title: "" , 
                slug: "",
               // description:"",
                version : '',
                data_order: ''
        },
        profile_image:"",
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        upload_data: "",
        editordata: '',
        focused: false,
        related_products: [],
        related_products_options: [],
        related_products_selected: [],
        multiple_file_array :[],
        related_cat : '',
        version : '',
        CategoryOptions :[]
    
    }

    onEditorChange( evt ) {
		this.setState( {
			editordata: evt.editor.getData()
		} );
	}


    onclassicEdit = async (value, item) => {
        if (item === "editorContent") {
            this.setState({
                editorContent : value.getData()
            })
        }  
    };

    /***************** CHECKBOX ON CHANGE **************/
    handleCheck(e){
        this.setState({
         checked: e.target.checked
        })
        
    }
    
    /*****************Joi validation schema**************/  
    schema = {
        title: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        slug: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is a required field.",
          };
        }),
        content:Joi.allow(null),
        // profile_image:Joi.allow(null),
        // version : Joi.string().allow(null),
        version: Joi.optional().label("version"),
        file_url: Joi.string(),
        data_order: Joi.number()
          .required()
          .error(() => {
            return {
              message: "This field is a required field and must be a number"
            };
        })
        
    };

    
    
    
    /*****************end Joi validation schema**************/

    /*****************INPUT HANDLE CHANGE **************/
 
    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const data = { ...this.state.data };
        const errorMessage = this.validateProperty(input);
        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        data[input.name] = input.value;
        this.setState({ data, errors });
    };

    /*****************JOI VALIDATION CALL**************/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };
      

/* upload single file upload */
uploadSingleFiles = async (e) => {
  this.setState({ upload_status: true });
  const response1 = await manageProducts.uploadProductImages(e.target.files);

  console.log(123,response1);

  if (response1.data.status == 1) {
    response1.data.data.file_location.map((item, key) =>
      this.setState({
        productfileArray: item.Location,
      })
    );

    this.setState({ upload_status: false });
  } else {
   
    this.setState({
      submitStatus: false,
      message: response1.data.message,
      responsetype: "error",
    });
    this.setState({ upload_status: false });
  }
};

 /* Multiple file upload */

 uploadMultipleFiles = async (e) => {
  this.setState({ multiple_upload_status: true });
  const response1 = await manageProducts.uploadProductImages(e.target.files);
  if (response1.data.status == 1) {
    response1.data.data.file_location.map((item, key) =>
      this.setState({
        multiple_file_array: this.state.multiple_file_array.concat({
          file_name: item.Location,
          file_url: item.Location,
        }),
      })
    );
    this.setState({ multiple_upload_status: false });
  } else {
    this.setState({
      submitStatus: false,
      message: response1.data.message,
      responsetype: "error",
    });
    this.setState({ multiple_upload_status: false });
  }
};




    // /***************** FORM SUBMIT **************/
    handleSubmit = async () => {

        const data = { ...this.state.data };
        const errors = { ...this.state.errors };
        let result = Joi.validate(data, this.schema);
         
        if (result.error) {
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
                // this.setState({ errors });
                this.setState({
                    errors: errors  
                  })
            } else {
            if (errors.length > 0) {
                this.setState({
                    errors: errors  
                  })
            }else{
                this.setState({ submit_status: true });
                this.setState({ spinner: true });
                this.updateDistributorsData();
               
            }
        }
    };

    updateDistributorsData = async () => {
      let newDate ="";
      if(this.state.releasedate !="")
        newDate = Moment(new Date(this.state.releasedate)).format('YYYY-MM-DD');
        const news_data = {
            title: this.state.data.title,
            slug: this.state.data.slug, 
            version : this.state.data.version,
            releasedate: newDate,
            // profile_image : this.state.profile_image,
            file_url: this.state.productfileArray,
            //checked : (this.state.checked == true ? '1' : '0'),
            content : this.state.editordata,
            related_products : this.state.related_products,
            related_cat : this.state.related_cat,
            data_order :  this.state.data.data_order
        }
        news_data["uploadFiles"] = {
          archiveFiles: this.state.multiple_file_array,
        };
        console.log(44,this.state.productfileArray);
       
        const response = await settingsService.createDistributors(news_data);
        console.log(90,response);
        if (response) {
            if(response.data.status === 1){
                this.setState({ spinner: false });
                this.setState({
                    message: response.data.message,
                    message_type: "success",
                  });
                  this.setState({ submit_status: false });
                  setTimeout(() => { 
                    window.location.reload();
                  }, 2000);
            } else {
                this.setState({ spinner: false });
                this.setState({ submit_status: false });
                this.setState({
                    message: response.data.message,
                    message_type: "error",
                  });
            }
        }
    }

    
    handleRemoveDistributorImg(i) {
      const filteredvalue = this.state.multiple_file_array.filter(
        (s, sidx) => i !== sidx
      );
      if (filteredvalue) {
        this.setState({
          multiple_file_array: filteredvalue,
        });
      }
    }
  
    getProductMenu = async () => {
        const response = await manageCasestudy.getProductMenu()
        if (response.data.status == 1) {
          const Setdata = { ...this.state.related_products }
          const data_related_row = []
          const data_selected_row = []
    
          response.data.data.map((Cats, index) => {
            console.log('elveee', Cats)
            const Setdata = {}
            Setdata.value = Cats.url
            Setdata.label = Cats.name
    
            if (this.state.related_products) {
              if (this.state.related_products.indexOf(Cats.url) > -1) {
                data_selected_row.push(Setdata)
              }
            }
    
            data_related_row.push(Setdata)
          })
          console.log('related_cat', data_selected_row, this.state.related_cat)
          this.setState({ related_products_options: data_related_row })
          this.setState({ related_products_selected: data_selected_row })
        }
      }
    
      setSelected = event => {
        const data_selected_row = []
        event.map((selectValues, index) => {
          data_selected_row[index] = selectValues.value
        })
    
        this.setState({ related_products: data_selected_row })
        this.setState({ related_products_selected: event })
      }
 
      handledateChange = (date) => {
        this.setState({
          releasedate: date,
        });
      };
      handleselectcat = async e => {
        this.setState({ related_cat: e.target.value })
      }
    componentDidMount = async () => {
        this.getProductMenu();
        this.getAllCategoryOptions();
    };

    getAllCategoryOptions = async () => {
      console.log('getAllCategoryOptions  ', )
      const response = await settingsService.getAllCategoryOptions()
      if (response.data.status == 1) {
        console.log('getAllCategoryOptions  ', response.data.data)
        this.setState({
          CategoryOptions: response.data.data  
        })
      }
    }

    render() { 
   
      const images = require.context("../../assets/images/admin", true); 
        let checkedCond  = this.state.checked;
        const minOffset = 0
        const maxOffset = 25
        const thisYear = new Date().getFullYear() + 0
        const options = []
        for (let i = minOffset; i <= maxOffset; i++) {
          const year = thisYear - i
          options.push(<option value={year}>{year}</option>)
        }

        const { CategoryOptions } = this.state;

        return ( <React.Fragment>
            <div className="container-fluid admin-body archive_sec">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div>
               
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4" >  
                    <div className="addpage-form">
                        <BlockUi tag="div" blocking={this.state.spinner} >
                        <div  className="row addpage-form-wrap">
                            
                            <div className="col-lg-8 col-md-12">                               
                                                           
                                <div className="form-group">
                                    <label htmlFor="">Add New </label>
                                    <input name="title" onChange={this.handleChange}  type="text" placeholder="Add title *" 
 
                                    className="form-control"/>
                                    {this.state.errors.title ? (<div className="error text-danger">
                                            {this.state.errors.title}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>

                                 <div className="form-group">
                                    <label htmlFor="">Slug</label>
                                    <input name="slug" onChange={this.handleChange}  
                                     value={this.state.slug}
                                    type="text" placeholder="slug *" className="form-control"/>
                                    {this.state.errors.slug ? (<div className="error text-danger">
                                            {this.state.errors.slug}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>


                                <div className="form-group">
                                    <label htmlFor="">Version</label>
                                    <input name="version" onChange={this.handleChange}  
                                     value={this.state.data.version}
                                    type="text" placeholder="version" className="form-control"/>
                               
                                </div>


              

                                <div className="gallery-single">
                                <div className="form-group">
                                <label htmlFor="">Single File </label>
                                  <input
                                    type="file"
                                    className="form-control"
                                    onChange={this.uploadSingleFiles}
                                  />
                                  {this.state.upload_status === true ? (
                                    <ReactSpinner
                                      type="border"
                                      color="dark"
                                      size="1"
                                    />
                                  ) : (
                                    ""
                                  )}
                                   <a href={this.state.productfileArray?this.state.productfileArray:""}>{this.state.productfileArray?this.state.productfileArray:""}</a>  
                                
                                </div>
                              </div>
                              <div className="form-group">
                                        <label>Multiple File</label>
                                        <input
                                          type="file"
                                          className="form-control"
                                          onChange={this.uploadMultipleFiles}
                                          multiple
                                        />
                                        {this.state.multiple_upload_status ===
                                        true ? (
                                          <ReactSpinner
                                            type="border"
                                            color="dark"
                                            size="1"
                                          />
                                        ) : (
                                          ""
                                        )}

                                  <div className="multi-preview">
                                        {this.state.multiple_file_array.map(
                                          (files, index) => (
                                            <div
                                              key={index}
                                              className="single-img"
                                            >
                                             
                                              <a 
                                                href={files.file_url}
                                                id={index}
                                                alt="..."
                                                width="80px"
                                                height="70px"
                                              > { files.file_url }</a>
                                                  <span
                                                value={index}
                                                onClick={() =>
                                                  this.handleRemoveDistributorImg(
                                                    index
                                                  )
                                                }
                                              >
                                                <img
                                                  src={images(`./close.jpg`)}
                                                  className="img-fluid "
                                                  alt="customer"
                                                ></img>
                                              </span>
                                      
                                            </div>
                                          )
                                        )}
                                      </div>
                                      </div>
                                    




                           
                                {/* <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <label htmlFor="">Content</label>
                                            <CKEditor
                                                data={this.state.editordata}
                                                config={ {
                                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                                    allowedContent: true,
                                                    filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                                    } }

                                                onInit = { 
                                                    editor => {          
                                                    } 
                                                }
                                                onChange={this.onEditorChange}
                                                style={{
                                                    float: 'left',
                                                    width: '99%'
                                                }}
                                            />
                                        </div>
                                    </div>
                                </div>  */}
                            
                            <div className="form-group">
                                      <label>Content</label>
                                  <div className="row">
                                    <div className="col-md-12">

                                      <CKEditor
                                          data={this.state.data.content}
                                          config={ {
                                            extraPlugins: ["justify" , "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                              allowedContent: true,
                                              filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                              } }

                                          onInit = { 
                                              editor => {          
                                              } 
                                          }
                                          onChange={this.onEditorChange}
                                      />
                                 </div>
                                 </div>
                                 </div>

                            
                                <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Select Products</h5>

                              <MultiSelect
                                options={this.state.related_products_options}
                                value={this.state.related_products_selected}
                                onChange={this.setSelected} //  labelledBy={"Select"}
                                
                              />
                            </div>{' '}
                          </div>{' '}
                        </div>


                        <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Select Category</h5>

                                    <select
                                     value={this.state.related_cat}
                                     className='form-group-relase'
                                     onChange={this.handleselectcat}
                                    >
                                     <option value="">Category</option>
                                     {
                                      CategoryOptions.length > 0 ? (
                                          CategoryOptions.map((data, index) => {
                                          return (
                                            <>
                                            <option key={data._id} value={data._id}>{data.title}</option>
                                            </>);
                                      })) : ""
                                    }
                                      {/* <option value="catalogs">Catalogs</option>
                                      <option value="certificates">Certificates</option>
                                      <option value="software">CIMON Software</option>
                                      <option value="marketing">Marketing Materials</option>
                                      <option value="manual">Product Manuals</option>
                                      <option value="videos">Videos</option> */}
                                    </select>        
                                          
                            </div> 
                          </div> 
                        </div>



                        <div className='form-group relDate'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Release Date</h5>
                                {}
                                <DatePicker
                                  selected={this.state.releasedate}
                                  placeholderText="Release Date *"
                                  maxDate={new Date()}                                  
                                  dateFormat="dd-MM-yyyy"                                  
                                  onChange={this.handledateChange}
                                  // className= {form-control}
                                />
          
                              {/* <select
                                value={this.state.releasedate}
                                className='form-group-relase'
                                onChange={this.handleDateChange}
                              >
                                <option value=''>Release Date</option>
                                {options}
                              </select> */}
                            </div> 
                          </div> 
                        </div>
                  
                 
                                <div className="form-group">
                                    <label htmlFor=""> Order</label>
                                    <input name="data_order" onChange={this.handleChange}  type="text" placeholder="Add  Order *" 
 
                                    className="form-control"/>
                                    {this.state.errors.data_order ? (<div className="error text-danger">
                                            {this.state.errors.data_order}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div> 
                            
                            
                            </div>
                            

                            <div className="col-lg-8 col-md-12 ">
                                
                                {/* <div className="form-group form-group-inline checkbox-div">
                                    <label htmlFor="">Is top news?</label>
                                    <input type="checkbox"  onChange={this.handleCheck}  checked={checkedCond}/>
                                </div>   
                                */}
                                    {/* <div className="form-group thumbanail_container">
                                        <Uploadprofile
                                        onuplaodProfile={this.onuplaodProfile}
                                        value={
                                        this.state.profile_image
                                        }
                                        errors={this.state.errors}
                                        />
                                    </div> */}

                                <div className="faq-sideinputs">   
                                    <div className="faq-btns form-btn-wrap">   
                                     <div className="float-left">
                                         {/* <Link  to="#">Move to Trash</Link> */}
                                     </div>
                                     <div className="update_btn input-group-btn float-right"> 
                                     <button
                                     disabled={this.state.submit_status === true ? "disabled" : false}
                                     onClick={this.handleSubmit} className="btn btn-info">
                                     {this.state.submit_status ? (
                                            <ReactSpinner type="border" color="dark" size="1" />
                                        ) : (
                                            ""
                                        )}
                                     Publish
                                     </button>
                                
                                     </div>
                                </div>
                                {this.state.message !== "" ? (
                                    <p className="tableinformation">
                                    <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                    </div>
                                    </p>
                                    ) : null
                                    }   
                                     
                                </div>
                            </div>
                            
                        </div>
                        </BlockUi>
                     </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment> );
    }
}
 
export default AddIntegratorResources;