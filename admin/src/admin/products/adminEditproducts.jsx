import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import validate from "react-joi-validation";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import { Link, NavLink } from "react-router-dom";
import * as manageProducts from "../../ApiServices/admin/products";
import * as settingsService from "../../ApiServices/admin/settings";
import * as attributeService from "../../ApiServices/admin/attributeService";
import CKEditor from "ckeditor4-react";
import { apiUrl, siteUrl } from "../../../src/config.json";
import ImageUploading from "react-images-uploading";
import MultiSelect from "react-multi-select-component";
import ProductDownloads from "../common/productDownloads";
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import BlockUi from 'react-block-ui';
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css"; 
class AdminProductEdit extends Component {
  constructor(props) {
    super(props);
    this.handleCheckbox = this.handleCheckbox.bind(this);
    this.uploadMultipleFiles = this.uploadMultipleFiles.bind(this);
    this.onEditorChange1 = this.onEditorChange1.bind(this);
    this.onEditorChange2 = this.onEditorChange2.bind(this);
    this.onEditorChange3 = this.onEditorChange3.bind(this);
    this.UploadprofileElement = React.createRef();
    this.onDownloadcontent1 = this.onDownloadcontent1.bind(this);
    this.onDownloadcontent2 = this.onDownloadcontent2.bind(this);
  }
  state = {
    productfileArray: [],
    dimensionfileArray: [],
    upload_status: false,
    dimen_upload_status: false,
    download_1_status: false,
    download_2_status: false,
    download_3_status: false,
    activetab: "tab_a",
    tabData: [
      { name: "Tab 1", isActive: true },
      { name: "Tab 2", isActive: false },
      { name: "Tab 3", isActive: false },
    ],

    editorContent: "",
    editorSpecifications: "",
    featured_array: "",
    attributes: [],
    tabattributes: [],
    cimonProducts: [
      {
        stock_quantity: 0,
        available_stock_quntity: 0,
        low_stock_threshold: 0,
        weights: "1",
        lengths: "",
        widths: "",
        heights: "",
        downloadbtn : 'XpanelDesigner'
      },
    ],
    checkval: "",
    related_products: [],
    related_products_options: [],
    related_products_selected: [],
    primary_category: "",
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    upload_data: "",
    categories_list: [],
    categories: [],
    productAttribute :[],
    attribute_list: [],
    cadfile_2D: "",
    cadfile_3D: "",
    xpaneldesigner: "",
    short_description_new: "",
    allow_backorders: "do_not_allow",
    url: "",
    tags: [],
    tag: '',
    cropped:"",
    submitcrop:false,
    src: null,
    crop: {
      unit: '%',
      width: 50,
      height: 50
    },
  };

  _crop() {
    // image in dataUrl
    this.cropper.getCroppedCanvas().toBlob(blob => {
      if (!blob) {
        //reject(new Error('Canvas is empty'));
        console.error('Canvas is empty');
        return;
      } 
      const reader = new FileReader();
      reader.readAsArrayBuffer(blob);
      
      const blobFile = new File([blob], 'image.jpg'); 
      this.setState({ upload_data: blobFile });  
      window.URL.revokeObjectURL(this.fileUrl);
      this.fileUrl = window.URL.createObjectURL(blob);
      console.log(this.fileUrl);
      this.UploadprofileElement.current.setImage(this.fileUrl);
      this.setState({ cropped: this.fileUrl });  
    } );
  }
  cropImage = e => { 
    this.setState({ submitcrop:  true  });
  }
  onCropperInit(cropper) {
      this.cropper = cropper;
  }

  /* upload multiple files*/
  uploadMultipleFiles = async (e) => {
    this.setState({ upload_status: true });
    const response1 = await manageProducts.uploadProductImages(e.target.files);
    if (response1.data.status == 1) {
      response1.data.data.file_location.map((item, key) =>
        this.setState({
          productfileArray: this.state.productfileArray.concat({
            image_name: item.Location,
            image_url: item.Location,
          }),
        })
      );
      this.setState({ upload_status: false });
    } else {
      this.setState({
        submitStatus: false,
        message: response1.data.message,
        responsetype: "error",
      });
      this.setState({ upload_status: false });
    }
  };

  handleRemoveProductImg(i) {
    const filteredvalue = this.state.productfileArray.filter(
      (s, sidx) => i !== sidx
    );
    if (filteredvalue) {
      this.setState({
        productfileArray: filteredvalue,
      });
    }
  }
  uploadDimensionFiles = async (e) => {
    this.setState({ dimen_upload_status: true });
    const response1 = await manageProducts.uploadProductImages(e.target.files);
    if (response1.data.status == 1) {
      response1.data.data.file_location.map((item, key) =>
        this.setState({
          dimensionfileArray: this.state.dimensionfileArray.concat({
            image_name: item.Location,
            image_url: item.Location,
          }),
        })
      );
      this.setState({ dimen_upload_status: false });
    } else {
      this.setState({
        submitStatus: false,
        message: response1.data.message,
        responsetype: "error",
      });
      this.setState({ dimen_upload_status: false });
    }
  };

  handleRemoveDimensionImg(i) {
    const filteredvalue = this.state.dimensionfileArray.filter(
      (s, sidx) => i !== sidx
    );
    if (filteredvalue) {
      this.setState({
        dimensionfileArray: filteredvalue,
      });
    }
  }


  onDownloadcontent1(evt) {
    const cimonProducts = { ...this.state.cimonProducts };
    cimonProducts["downloadcontent1"] = evt.editor.getData();
    this.setState({
      cimonProducts: cimonProducts,
    });
  }


  onDownloadcontent2(evt) {
    const cimonProducts = { ...this.state.cimonProducts };
    cimonProducts["downloadcontent2"] = evt.editor.getData();
    this.setState({
      cimonProducts: cimonProducts,
    });
  }
  onEditorChange1(evt) {
    const cimonProducts = { ...this.state.cimonProducts };
    cimonProducts["short_description"] = evt.editor.getData();
    this.setState({
      cimonProducts: cimonProducts,
    });
  }

  onEditorChange2(evt) {
    const cimonProducts = { ...this.state.cimonProducts };
    cimonProducts["description"] = evt.editor.getData();
    this.setState({
      cimonProducts: cimonProducts,
    });
  }

  onEditorChange3(evt) {
    const cimonProducts = { ...this.state.cimonProducts };
    cimonProducts["specifications"] = evt.editor.getData();
    this.setState({
      cimonProducts: cimonProducts,
    });
  }

  tabActive = async (tab) => {
    this.setState({ activetab: tab });
  };

  /* Joi validation schema */
  schema = {
    product_name: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
    slug: Joi.string().allow(""),
    description: Joi.string().allow(""),
    featured_image: Joi.allow(null),
    profile_image : Joi.allow(null),
    dimension_image: Joi.allow(null),
    short_description: Joi.string().allow(""),
    regular_price: Joi.number()
    .allow("")
      .error(() => {
        return {
          message: "Regular Price must be a number",
        };
      }),
    sales_price: Joi.number()
      .allow("")
      .error(() => {
        return {
          message: "Must be a number",
        };
      }),
    description: Joi.allow(null),
    specifications: Joi.allow(null),
    _id: Joi.optional().label("id"),
    product_sku: [Joi.string(), Joi.string().allow("")],
    stock_availability: Joi.allow(null),
    featured_name: Joi.string().allow(""),
    stock_quantity: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed",
        };
      }),
    allow_backorders: Joi.string().allow(""),
    low_stock_threshold: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed",
        };
      }),
    banner_description: Joi.string().allow(""),
    product_sub_text: Joi.string().allow(""),
    model_number: Joi.string().allow(""),
    meta_tag : Joi.string().allow(""),
    meta_description : Joi.string().allow(""),
    orderno: 
    Joi.alternatives(
      Joi.number().allow("").allow(0),
      Joi.string().allow("").allow(0)
  )
    ,
    downloadbtn : Joi.string().allow(""),
    downloadcontent1 : Joi.string().allow(""),
    weights: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed",
        };
      }),
    lengths: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed",
        };
      }),
    widths: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed",
        };
      }),
    heights: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed",
        };
      }),
  };

  /* Input Handle Change */
  handleChange = (event, type = null) => {
    let cimonProducts = { ...this.state.cimonProducts };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value

    if (name == "featured_name") {
      let filepath = {
        image_name: value,
        image_url: cimonProducts["featured_image"],
      };
      this.setState({ featured_array: filepath });
    }

    const errorMessage = this.validateProperty(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    cimonProducts[name] = value;
    this.setState({ cimonProducts, errors });
  };
  validateProperty = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };
  /* upload profile imgae*/
  onuplaodProfile = async (value, item) => {
    this.setState({ submitcrop:  false  });
    let errors = { ...this.state.errors };
    let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["featured_image"] = value;
    } else {

      if (value.target.files && value.target.files.length > 0) {
        const reader = new FileReader();
        reader.addEventListener('load', () =>
          this.setState({ src: reader.result })
        );
        reader.readAsDataURL(value.target.files[0]);
      }

      let file = value.target.files[0];
      upload_data = file;
      
      this.setState({ upload_data: upload_data });
    }
  };

  /* product downloads starts */
  onproductdownload_1 = async (value, item) => {
    let errors = { ...this.state.errors };
    let cadfile_2D = { ...this.state.cadfile_2D };
    console.log('onproductdownload_1',errors)
    if (item === "errors") {
      errors["cadfile_2D"] = value;
    } else {
      this.setState({ download_1_status: true });
      let file = value.target.files[0];
      const response1 = await manageProducts.uploadFeaturedImage(file);
      if (response1.data.status == 1) {
        let filepath = {
          image_name: response1.data.data.file_name,
          image_url: response1.data.data.file_location,
        };
        this.setState({ cadfile_2D: filepath });
      } else {
        this.setState({
          message: response1.data.message,
          responsetype: "error",
        });
      }
      this.setState({ download_1_status: false });
    }
  };

  /* cadfile3D file upload */
  onproductdownload_2 = async (value, item) => {
    let errors = { ...this.state.errors };
    let cadfile_3D = { ...this.state.cadfile_3D };
    if (item === "errors") {
      errors["cadfile_3D"] = value;
    } else {
      this.setState({ download_2_status: true });
      let file = value.target.files[0];
      const response1 = await manageProducts.uploadFeaturedImage(file);
      if (response1.data.status == 1) {
        let filepath = {
          image_name: response1.data.data.file_name,
          image_url: response1.data.data.file_location,
        };
        this.setState({ cadfile_3D: filepath });
      } else {
        this.setState({
          message: response1.data.message,
          responsetype: "error",
        });
      }
      this.setState({ download_2_status: false });
    }
  };
  /* xpaneldesigner file upload */
  onproductdownload_3 = async (value, item) => {
    let errors = { ...this.state.errors };
    let xpaneldesigner = { ...this.state.xpaneldesigner };
    if (item === "errors") {
      errors["xpaneldesigner"] = value;
    } else {
      this.setState({ download_3_status: true });
      let file = value.target.files[0];
      const response1 = await manageProducts.uploadFeaturedImage(file);
      if (response1.data.status == 1) {
        let filepath = {
          image_name: response1.data.data.file_name,
          image_url: response1.data.data.file_location,
        };
        this.setState({ xpaneldesigner: filepath });
      } else {
        this.setState({
          message: response1.data.message,
          responsetype: "error",
        });
      }
      this.setState({ download_3_status: false });
    }
  };

  handleCheckbox = (e) => {
    this.setState({ checkval: e.target.checked });
  };
  handleselectChange = async (e) => {
    const cimonProducts = { ...this.state.cimonProducts };
    cimonProducts["stock_availability"] = e.target.value;
    this.setState({ cimonProducts: cimonProducts });
  };

  handlebackorderChange = async (e) => {
    this.setState({ allow_backorders: e.target.value });
  };

  /* Get single products */
  getProductSingle = async (id) => {
    this.setState({spinner:true});
    const response = await manageProducts.getSingleProduct(id);
    if (response.data.status == 1) {
      var img_url = ""; // featured image
      var img_name = "";
      if (response.data.data.product_data !== null) {
        if (response.data.data.product_data.images) {
          if (response.data.data.product_data.images.featured_image) {
            img_url =
              response.data.data.product_data.images.featured_image.image_url;
            img_name =
              response.data.data.product_data.images.featured_image.image_name;
            this.setState({
              featured_array:
                response.data.data.product_data.images.featured_image,
            });
          }
          if (response.data.data.product_data.images.gallery_images) {
            this.setState({
              galleryFiles:
                response.data.data.product_data.images.gallery_images,
            });

            this.setState({
              productfileArray:
                response.data.data.product_data.images.gallery_images,
            });
          }
          if (response.data.data.product_data.images.dimension_images) {
            this.setState({
              dimensionFiles:
                response.data.data.product_data.images.dimension_images,
            });

            this.setState({
              dimensionfileArray:
                response.data.data.product_data.images.dimension_images,
            });
          }
        }

        let banner_description = "",
          product_sub_text = "",
          model_number = "",
          stock_quantity = "0",
          weights = "",
          lengths = "0",
          widths = "0",
          heights = "0",
          low_stock_threshold = "0",
          available_stock_quntity = "0",
          allow_backorders = "do_not_allow";

        if (
          response.data.data.product_data.inventory.hasOwnProperty(
            "manage_inventory"
          )
        ) {
          stock_quantity = response.data.data.product_data.inventory
            .manage_inventory.stock_quantity
            ? response.data.data.product_data.inventory.manage_inventory
                .stock_quantity
            : "0";
          low_stock_threshold = response.data.data.product_data.inventory
            .manage_inventory.low_stock_threshold
            ? response.data.data.product_data.inventory.manage_inventory
                .low_stock_threshold
            : "0";
          allow_backorders =
            response.data.data.product_data.inventory.manage_inventory
              .allow_backorders;
          available_stock_quntity =
            response.data.data.product_data.inventory.manage_inventory
              .available_stock_quntity;
        }
        this.getCurrentproductUrl(
          response.data.data.product_data.primary_category
        );

        if (
          response.data.data.product_data.hasOwnProperty("banner_description")
        ) {
          banner_description = response.data.data.product_data
            .banner_description
            ? response.data.data.product_data.banner_description
            : "";
        }
        if (
          response.data.data.product_data.hasOwnProperty("product_sub_text")
        ) {
          product_sub_text = response.data.data.product_data.product_sub_text
            ? response.data.data.product_data.product_sub_text
            : "";
        }
        if (
          response.data.data.product_data.hasOwnProperty("model_number")
        ) {
          model_number = response.data.data.product_data.model_number
            ? response.data.data.product_data.model_number
            : "";
        }
        if (response.data.data.product_data.hasOwnProperty("packages")) {
          weights = response.data.data.product_data.packages.weights
            ? response.data.data.product_data.packages.weights
            : 1;
          lengths = response.data.data.product_data.packages.lengths
            ? response.data.data.product_data.packages.lengths
            : "0";
          widths = response.data.data.product_data.packages.widths
            ? response.data.data.product_data.packages.widths
            : "0";
          heights = response.data.data.product_data.packages.heights
            ? response.data.data.product_data.packages.heights
            : "0";
        }
        let newProductsarray = {
          _id: response.data.data.product_data._id,
          product_name: response.data.data.product_data.product_name,
          slug: response.data.data.product_data.slug,
          short_description: response.data.data.product_data.short_description,
          description: response.data.data.product_data.description,
          specifications: response.data.data.product_data.specifications,
          sales_price: response.data.data.product_data.price.sales_price,
          regular_price: response.data.data.product_data.price.regular_price,
          product_sku: response.data.data.product_data.inventory.product_sku,
          stock_availability:
            response.data.data.product_data.inventory.stock_availability,
          featured_image: img_url,
          featured_name: img_name,
          stock_quantity: stock_quantity,
          low_stock_threshold: low_stock_threshold,
          banner_description: banner_description,
          product_sub_text: product_sub_text,
          model_number :model_number,
          weights: weights,
          lengths: lengths,
          widths: widths,
          heights: heights,
          meta_tag :  response.data.data.product_data.meta_tag ?  response.data.data.product_data.meta_tag : '', 
          meta_description :response.data.data.product_data.meta_description ? response.data.data.product_data.meta_description  : '',
          orderno :response.data.data.product_data.orderno ? response.data.data.product_data.orderno  : 0,
        
          
          downloadbtn : response.data.data.product_data.downloadbtn ?  response.data.data.product_data.downloadbtn : 'XpanelDesigner',
          downloadcontent1 : response.data.data.product_data.downloadcontent1 ?  response.data.data.product_data.downloadcontent1 : ''
       
        };
        this.setState({
          allow_backorders: allow_backorders,
          available_stock_quntity: available_stock_quntity,
        });
        this.setState({ cimonProducts: newProductsarray });
        let checkvalue =
          response.data.data.product_data.inventory.manage_stock === true
            ? true
            : false;
        this.setState({ checkval: checkvalue });
        const attributes = response.data.data.product_data.attributes;
        if (attributes) {
          this.setState({ attributes: attributes });
        }
        const tabattributes = response.data.data.product_data.tabdata;
        if (tabattributes) {
          this.setState({ tabattributes: tabattributes });
        }
        
        const categories = response.data.data.product_data.categories;
        if (categories) {
          const categoriesvalue = this.state.categories_list
            .filter((s, sidx) => categories.indexOf(s._id) >= 0)
            .map(function (s) {
              return s._id;
            });
          this.setState({ categories: categoriesvalue });
        }



        const productAttribute = response.data.data.product_data.productAttribute;
        if (productAttribute) {
          const attributevalue = this.state.attribute_list
            .filter((s, sidx) => productAttribute.indexOf(s._id) >= 0)
            .map(function (s) {
              return s._id;
            });
          this.setState({ productAttribute : attributevalue });
        }


        const primary_category =
          response.data.data.product_data.primary_category;
        if (primary_category) {
          this.setState({ primary_category: primary_category });
        }

        const related_products =
          response.data.data.product_data.related_products;
        if (related_products) {
          this.setState({ related_products: related_products });
        }

        const downloads = response.data.data.product_data.downloads;
        if (downloads) {
          let cadfile_2D = response.data.data.product_data.downloads.cadfile_2D
            ? response.data.data.product_data.downloads.cadfile_2D
            : "";
          this.setState({ cadfile_2D: cadfile_2D });
          let cadfile_3D = response.data.data.product_data.downloads.cadfile_3D
            ? response.data.data.product_data.downloads.cadfile_3D
            : "";
          this.setState({ cadfile_3D: cadfile_3D });
          let xpaneldesigner = response.data.data.product_data.downloads
            .xpaneldesigner
            ? response.data.data.product_data.downloads.xpaneldesigner
            : "";
          this.setState({ xpaneldesigner: xpaneldesigner });
        //  this.setState({ tag:  response.data.data.product_data.product_tag });
          this.setState({ tags:  response.data.data.product_data.product_tag });
        }
        this.getAllProductsIdsandNames();
        let gallery_db_files = [];
        response.data.data.product_data.images.gallery_images.map((item, key) =>
          gallery_db_files.push({ dataURL: item.image_url })
        );
        this.setState({ defaultGallery: gallery_db_files });
      } else {
        this.props.history.push({
          pathname: "/admin/products/",
        });
      }
    } else {
      this.setState({
        newsStatus: false,
        message: response.data.message,
        responsetype: "error",
      });
    }
    this.setState({spinner:false});
  };

  componentDidMount = async () => {
    this.getAllProductCategories();
    this.getAllAttributes();
    const id = this.props.match.params.id;
    setTimeout(() => {
      this.getProductSingle(id);
    }, 1800);
  };

  /* FORM SUBMIT*/
  handleSubmit = async () => {
    const cimonProducts = { ...this.state.cimonProducts };
    const errors = { ...this.state.errors };
    let result = Joi.validate(cimonProducts, this.schema);
    console.log('handleSubmit-->',result);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors,
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors,
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });

        if (this.state.upload_data) {
          // check for featured images
          const response1 = await manageProducts.uploadFeaturedImage(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = {
              image_name: this.state.cimonProducts.featured_name,
              image_url: response1.data.data.file_location,
            };
            this.setState({ featured_array: filepath });

            this.setState({ upload_data: "" });
            this.updateProductData();
          } else {
           
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error",
            });
            this.setState({spinner:false});
          }
        } else {
          this.updateProductData();
        }
      }
    }
  };
  /* Update Products */
  updateProductData = async () => {
    const cimonProducts = { ...this.state.cimonProducts };
    cimonProducts["price"] = {
      regular_price: this.state.cimonProducts.regular_price ? this.state.cimonProducts.regular_price : 0,
      sales_price: this.state.cimonProducts.sales_price ? this.state.cimonProducts.sales_price : 0,
    };
    let stock_quantity,
      low_stock_threshold,
      allow_backorders,
      available_stock_quntity;

    if (this.state.checkval) {
      // Manage stock
      stock_quantity = this.state.cimonProducts.stock_quantity
        ? this.state.cimonProducts.stock_quantity
        : "0";
      low_stock_threshold = this.state.cimonProducts.low_stock_threshold
        ? this.state.cimonProducts.low_stock_threshold
        : "0";
      available_stock_quntity =
        this.state.available_stock_quntity > 0
          ? this.state.available_stock_quntity
          : this.state.cimonProducts.stock_quantity;
      allow_backorders = this.state.allow_backorders;
    } else {
      stock_quantity = "0";
      low_stock_threshold = "0";
      available_stock_quntity = "0";
      allow_backorders = "do_not_allow";
      available_stock_quntity = "0";
    }

    cimonProducts["inventory"] = {
      product_sku: this.state.cimonProducts.product_sku,
      manage_stock: this.state.checkval,
      manage_inventory: {
        stock_quantity: stock_quantity,
        allow_backorders: allow_backorders,
        low_stock_threshold: low_stock_threshold,
        available_stock_quntity: available_stock_quntity,
      },
      stock_availability: this.state.cimonProducts.stock_availability,
    };

    cimonProducts["packages"] = {
      weights: this.state.cimonProducts.weights,
      lengths: this.state.cimonProducts.lengths,
      widths: this.state.cimonProducts.widths,
      heights: this.state.cimonProducts.heights,
    };

    cimonProducts["downloads"] = {
      cadfile_2D: this.state.cadfile_2D,
      cadfile_3D: this.state.cadfile_3D,
      xpaneldesigner: this.state.xpaneldesigner,
    };
    cimonProducts["images"] = {
      featured_image: this.state.featured_array,
      gallery_images: this.state.productfileArray,
      dimension_images: this.state.dimensionfileArray,
    };
    cimonProducts["attributes"] = this.state.attributes;
    cimonProducts["primary_category"] = this.state.primary_category;
    cimonProducts["related_products"] = this.state.related_products;
    cimonProducts["tabdata"] = this.state.tabattributes;
    cimonProducts["categories"] = this.state.categories;
    cimonProducts["productAttribute"] = this.state.productAttribute;
    cimonProducts["url"] = this.state.url;
    cimonProducts[
      "banner_description"
    ] = this.state.cimonProducts.banner_description;
    cimonProducts[
      "product_sub_text"
    ] = this.state.cimonProducts.product_sub_text;
    cimonProducts[
      "model_number"
    ] = this.state.cimonProducts.model_number;

    cimonProducts[
      "downloadbtn"
    ] = this.state.cimonProducts.downloadbtn;
    
    cimonProducts[
      "downloadcontent1"
    ] = this.state.cimonProducts.downloadcontent1; 
    cimonProducts[
      "meta_tag"
    ] = this.state.cimonProducts.meta_tag;

    cimonProducts[
      "meta_description"
    ] = this.state.cimonProducts.meta_description;

    cimonProducts[
      "orderno"
    ] = this.state.cimonProducts.orderno ? this.state.cimonProducts.orderno : 0;

    
    cimonProducts[
      "product_tag"
    ] = this.state.tags;
   
    const response = await manageProducts.updateProduct(cimonProducts);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success",
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error",
        });
      }
    }
  };


  /* get all attribute */

  

  getAllAttributes = async () => {
    const response = await attributeService.getAllAttributes();
    if (response.data.status == 1) {
      if (response.data.data) {
        console.log('all attibutes', response.data.data);
        this.setState({
         attribute_list: response.data.data,
        });
      }
    }
  };


  /* get all product category */
  getAllProductCategories = async () => {
    const response = await settingsService.getAllProductCategories();
    if (response.data.status == 1) {
      if (response.data.data) {
        this.setState({
          categories_list: response.data.data,
          //   categories : ['5ecfabcaede3a52210ebbe61' ,'5ecface6c4b76c3670148d6d','12311321']
        });
      }
    }
  };

  /* (onchange / add)    product category or primary category */

  handleCheckChieldElement = (event) => {
    let clickedValue = event.target.value;

    if (event.target.checked) {
      const list = [...this.state.categories, clickedValue];

      if (list.indexOf(this.state.primary_category) < 0) {
        // setting Primary Category default option

        this.getCurrentproductUrl(list[0]);
        this.setState({
          primary_category: list[0],
        });
      }

      this.setState({
        categories: list,
      });
    } else {
      let categories = [...this.state.categories]; // make a separate copy of the array
      let index = categories.indexOf(event.target.value);
      if (index !== -1) {
        categories.splice(index, 1);
        this.setState({ categories: categories });
      }
      if (categories.indexOf(this.state.primary_category) < 0) {
        // setting Primary Category default option
        this.getCurrentproductUrl(categories[0]);
        this.setState({
          primary_category: categories[0],
        });
      }
    }
  };


  /* set attribute items */

  handleCheckAttrbuteElement = (event) => {
    let clickedValue = event.target.value;

    if (event.target.checked) {
      const list = [...this.state.productAttribute, clickedValue];
 
      this.setState({
        productAttribute: list,
      });
    } else {
      let productAttribute = [...this.state.productAttribute]; // make a separate copy of the array
      let index = productAttribute.indexOf(event.target.value);
      if (index !== -1) {
        productAttribute.splice(index, 1);
        this.setState({ productAttribute: productAttribute });
      }
    }
  };


  /* get all products and ids for generating select option */

  getAllProductsIdsandNames = async () => {
    const response = await manageProducts.getAllProductsIdsandNames();
    if (response.data.status == 1) {
      const Setdata = { ...this.state.related_products };
      const data_related_row = [];
      const data_selected_row = [];
      response.data.data.map((Products, index) => {
        const Setdata = {};
        Setdata.value = Products._id;
        Setdata.label = Products.product_name;
        if (this.state.related_products.indexOf(Products._id) > -1) {
          data_selected_row.push(Setdata);
        }
        data_related_row.push(Setdata);
      });

      this.setState({ related_products_options: data_related_row });
      this.setState({ related_products_selected: data_selected_row });
    }
  };

  /* set selected relative products */

  setSelected = (event) => {
    const data_selected_row = [];
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value;
    });

    this.setState({ related_products: data_selected_row });
    this.setState({ related_products_selected: event });
  };

  /* common function for generating current url and set primary category  */
  SetPrimaryCat = (Cat_id) => {
    this.setState({ primary_category: Cat_id });
    this.getCurrentproductUrl(Cat_id);
  };

  /* set product url on admin side (main category and sub category)*/

  checkurl = (url) => {
    let productUrl;
    if (url.indexOf("scada") !== -1) {
      url = "/introduction/scada";
      productUrl = siteUrl + url;
    } else {
      url = "/product" + url + "/";
      productUrl = siteUrl + url + this.state.cimonProducts.slug;
    }

    this.setState({ productUrl: productUrl });
    return url;
  };

  /* get current product url */

  getCurrentproductUrl = async (Cat_id) => {
    let url = "";
    this.setState({ submit_status: true });
    const cimonProducts = { ...this.state.cimonProducts };
    const response = await manageProducts.getCurrentproductUrl(Cat_id);
    if (response.data.status == 1) {
      this.setState({ submit_status: false });

      if (response.data.data !== "/null") {
        url = this.checkurl(response.data.data);
        this.setState({
          url: url,
        });
      } else {
        url = "/product/";
        this.setState({
          url: url,
        });
      }
    }
  };

/* additional tabs */
handleTabAttributesNameChange = (idx) => (evt) => {
  const newAttributes = this.state.tabattributes.map((attribute, sidx) => {
    if (idx !== sidx) return attribute;
    return { ...attribute, name: evt.target.value };
  });
  this.setState({ tabattributes: newAttributes });
};

handleTabAttributesDescChange= (idx) => (evt) => { 
  const newAttributes = this.state.tabattributes.map((attribute, sidx) => {
    if (idx !== sidx) return attribute;
    return { ...attribute, description: evt.editor.getData() };
  });
  this.setState({ tabattributes: newAttributes });
}
handleAddTabs = () => {
  this.setState({
    tabattributes: this.state.tabattributes.concat([
      { name: "", description: "" },
    ]),
  });
};

handleRemoveTabAttributes = (idx) => () => {
  this.setState({
    tabattributes: this.state.tabattributes.filter((s, sidx) => idx !== sidx),
  });
};




  /* handling product attributes */
  handleAttributesNameChange = (idx) => (evt) => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute;
      return { ...attribute, Attribute_id: evt.target.value };
    });

    this.setState({ attributes: newAttributes });
  };

  handleAddAttributes = () => {
    this.setState({
      attributes: this.state.attributes.concat([
        { Attribute_id: "", Attribute_values: [] },
      ]),
    });
  };

  handleRemoveAttributes = (idx) => () => {
    this.setState({
      attributes: this.state.attributes.filter((s, sidx) => idx !== sidx),
    });
  };

  handleAttributessubNameChange = (idx, sub_idx) => (evt) => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx === sidx) {
        const SetsubAttribute = attribute.Attribute_values;
        SetsubAttribute[sub_idx] = evt.target.value;
        return { ...attribute, Attribute_values: SetsubAttribute };
      } else {
        return attribute;
      }
    });

    this.setState({ attributes: newAttributes });
  };

  handleAddsubAttributes = (idx) => () => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx === sidx) {
        const SetsubAttribute = attribute.Attribute_values;
        SetsubAttribute.push("");
        return { ...attribute, Attribute_values: SetsubAttribute };
      } else {
        return attribute;
      }
    });
    this.setState({ attributes: newAttributes });
  };

  handleRemovesubAttributes = (idx, sub_idx) => () => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx === sidx) {
        const SetsubAttribute = attribute.Attribute_values.filter(
          (s, count) => sub_idx !== count
        );

        return { ...attribute, Attribute_values: SetsubAttribute };
      } else {
        return attribute;
      }
    });

    this.setState({ attributes: newAttributes });
  };

  pasteSplit(data) {
    const separators = [',', ';', '\\(', '\\)', '\\*', '/', ':', '\\?', '\n', '\r'];
    return data.split(new RegExp(separators.join('|'))).map(d => d.trim());
  }
  handletagChange(tags) {
       console.log('handletagChange',tags);
    this.setState({ tags: tags });
  }
 
  handletagChangeInput(tag) {
   
    this.setState({ tag:  tag  });
  }

  handleRemoveProfileImage  = () => {

    console.log('handleRemoveProfileImage-------->');
    const cimonProducts = { ...this.state.cimonProducts };
    cimonProducts["profile_image"] = '';
    cimonProducts["featured_image"] = '';
    
    let filepath = {
      image_name: '',
      image_url: '',
    }; 
    this.setState({
      featured_array: filepath,
      cimonProducts: cimonProducts,
      upload_data : ''
    }); 
  };


  render() {
    const images = require.context("../../assets/images/admin", true);
    let checkedCond = this.state.checked;
   // console.log('checkedCond', this.state.cimonProducts.downloadbtn);
    const { crop, croppedImageUrl, src,submitcrop } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12 product-add-page  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                    <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className="row addpage-form-wrap">
                      <div className="col-lg-9 col-md-12">
                     

                        {this.state.errors.regular_price !== "" ? (
                          <div className="validate_error">
                            <p>{this.state.errors.regular_price}</p>
                          </div>
                        ) : null}

                        <div className="form-group">
                          <label htmlFor="">Product Name</label>
                          <input
                            name="product_name"
                            value={this.state.cimonProducts.product_name}
                            onChange={(e) => this.handleChange(e)}
                            type="text"
                            placeholder="Add title *"
                            className="form-control"
                          />
                          {this.state.errors.product_name ? (
                            <div className="error text-danger">
                              {this.state.errors.product_name}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Slug</label>
                          <input
                            name="slug"
                            value={this.state.cimonProducts.slug}
                            onChange={(e) => this.handleChange(e)}
                            type="text"
                            placeholder="slug *"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Url</label>
                          <input
                            name="url"
                            value={this.state.productUrl}
                            disabled
                            type="text"
                            placeholder="url *"
                            className="form-control"
                          />
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Banner Sub Text</label>
                          <input
                            name="banner_description"
                            value={this.state.cimonProducts.banner_description}
                            onChange={(e) => this.handleChange(e)}
                            type="text"
                            placeholder="Sub Text"
                            className="form-control"
                          />
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Product Sub Text</label>
                          <input
                            name="product_sub_text"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.cimonProducts.product_sub_text}
                            type="text"
                            placeholder="Sub Text "
                            className="form-control"
                          />
                          {this.state.errors.product_sub_text ? (
                            <div className="error text-danger">
                              {this.state.errors.product_sub_text}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Product Model Number</label>
                          <input
                            name="model_number"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.cimonProducts.model_number}
                            type="text"
                            placeholder="Model Number"
                            className="form-control"
                          />
                          {this.state.errors.model_number ? (
                            <div className="error text-danger">
                              {this.state.errors.model_number}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>


                        <div className="form-group">
                          <label htmlFor="">Meta tag</label>
                          <input
                            name="meta_tag"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.cimonProducts.meta_tag}
                            type="text"
                            placeholder="Meta Tag"
                            className="form-control"
                          />
                          {this.state.errors.meta_tag ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_tag}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Meta Description</label>
                          <input
                            name="meta_description"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.cimonProducts.meta_description}
                            type="text"
                            placeholder="Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.meta_description ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_description}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                     




                        <div className="form-group">
                          <label htmlFor="">Product Order</label>
                          <input
                            name="orderno"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.cimonProducts.orderno}
                            type="number"
                          //  min = "0"
                          //  placeholder="Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.orderno ? (
                            <div className="error text-danger">
                              {this.state.errors.orderno}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Short Description</label>
                          <div className="row">
                            <div className="col-md-12">
                              <CKEditor
                                data={
                                  this.state.cimonProducts.short_description !=
                                  null
                                    ? this.state.cimonProducts.short_description
                                    : ""
                                }
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new",
                                }}
                                onInit={(editor) => {}}
                                onChange={this.onEditorChange1}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-12">
                              <label htmlFor="">Content</label>
                              <CKEditor
                                data={
                                  this.state.cimonProducts.description != null
                                    ? this.state.cimonProducts.description
                                    : ""
                                }
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  allowedContent: true,
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new",
                                }}
                                onInit={(editor) => {}}
                                onChange={this.onEditorChange2}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="product-attribute-tab row bg-white">
                            <div className="col-xl-12 col-md-12 col-sm-12">
                              <p>
                                Product Data -{" "}
                                <select>
                                  <option>Simple Product</option>
                                  <option>Bundeled products</option>
                                </select>
                              </p>
                            </div>
                            <div className="col-xl-3 col-md-3 col-sm-3">
                              <ul className="nav nav-pills nav-fill">
                                <li
                                  className={
                                    this.state.activetab === "tab_a"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_a"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_a")}
                                  >
                                    Price
                                  </a>
                                </li>
                                <li
                                  className={
                                    this.state.activetab === "tab_b"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_b"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_b")}
                                  >
                                    Inventory
                                  </a>
                                </li>
                                <li
                                  className={
                                    this.state.activetab === "tab_c"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_c"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_c")}
                                  >
                                    Images
                                  </a>
                                </li>
                                <li
                                  className={
                                    this.state.activetab === "tab_d"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_d"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_d")}
                                  >
                                    Specifications
                                  </a>
                                </li>

                                <li
                                  className={
                                    this.state.activetab === "tab_e"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_e"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_e")}
                                  >
                                    Related products
                                  </a>
                                </li>

                                <li
                                  className={
                                    this.state.activetab === "tab_f"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_f"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_f")}
                                  >
                                    Downloads
                                  </a>
                                </li>

                                <li
                                  className={
                                    this.state.activetab === "tab_g"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_g"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_g")}
                                  >
                                    Attributes
                                  </a>
                                </li>

                                <li
                                  className={
                                    this.state.activetab === "tab_h"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_h"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_h")}
                                  >
                                    Package
                                  </a>
                                </li>
                              </ul>
                            </div>

                            <div className="col-xl-9 col-md-9 col-sm-9 ">
                              <div className="tab-content">
                                <div className="tab-pane active" id="tab_a">
                                  <div className="tab-icon"></div>
                                  <div className="edit-details-head pb-4">
                                    <div className="form-group-product">
                                      <label>Regular Price</label>
                                      <input
                                        type="text"
                                        name="regular_price"
                                        className="form-control "
                                        value={
                                          this.state.cimonProducts.regular_price
                                            ? this.state.cimonProducts
                                                .regular_price
                                            : 0
                                        }
                                        onChange={(e) => this.handleChange(e)}
                                      />
                                    </div>

                                    <div className="form-group-product">
                                      <label>Sales Price</label>
                                      <input
                                        type="text"
                                        name="sales_price"
                                        className="form-control "
                                        value={
                                          this.state.cimonProducts.sales_price
                                            ? this.state.cimonProducts
                                                .sales_price
                                            : 0
                                        }
                                        onChange={(e) => this.handleChange(e)}
                                      />
                                      {this.state.errors.sales_price !== "" ? (
                                        <div className="validate_error">
                                          <p>{this.state.errors.sales_price}</p>
                                        </div>
                                      ) : null}
                                    </div>
                                  </div>
                                </div>
                                <div className="tab-pane" id="tab_b">
                                  <div className="form-group-product">
                                    <label>SKU</label>
                                    <input
                                      type="text"
                                      name="product_sku"
                                      className="form-control "
                                      value={
                                        this.state.cimonProducts.product_sku
                                      }
                                      onChange={(e) => this.handleChange(e)}
                                    />
                                    {this.state.errors.product_sku !== "" ? (
                                      <div className="validate_error">
                                        <p>{this.state.errors.product_sku}</p>
                                      </div>
                                    ) : null}
                                  </div>

                                  <div className="form-group-product">
                                    <div className="row">
                                      <div className="col-md-4">
                                        <label>Manage stock?</label>
                                      </div>
                                      <div className="col-md-6 checkbox_btn">
                                        <input
                                          type="checkbox"
                                          onChange={this.handleCheckbox}
                                          checked={this.state.checkval}
                                        />
                                      </div>
                                    </div>
                                  </div>

                                  {(() => {
                                    if (this.state.checkval) {
                                      return (
                                        <div>
                                          <div className="form-group-product">
                                            <label>Stock quantity</label>
                                            <input
                                              type="number"
                                              name="stock_quantity"
                                              className="form-control "
                                              value={
                                                this.state.cimonProducts
                                                  .stock_quantity
                                              }
                                              onChange={(e) =>
                                                this.handleChange(e)
                                              }
                                            />
                                            {this.state.errors
                                              .stock_quantity !== "" ? (
                                              <div className="validate_error">
                                                <p>
                                                  {
                                                    this.state.errors
                                                      .stock_quantity
                                                  }
                                                </p>
                                              </div>
                                            ) : null}
                                          </div>

                                          <div className="form-group-product">
                                            <div className="row">
                                              <div className="col-md-12">
                                                <label>Allow backorders?</label>
                                              </div>
                                              <div className="col-md-12">
                                                <select
                                                  className="form-control"
                                                  value={
                                                    this.state.allow_backorders
                                                      ? this.state
                                                          .allow_backorders
                                                      : ""
                                                  }
                                                  onChange={
                                                    this.handlebackorderChange
                                                  }
                                                >
                                                  <option value="do_not_allow">
                                                    Do not allow
                                                  </option>
                                                  <option value="allow_but_notify">
                                                    Allow, but notify customer
                                                  </option>
                                                  <option value="allow">
                                                    Allow
                                                  </option>
                                                </select>
                                              </div>
                                            </div>
                                          </div>

                                          <div className="form-group-product">
                                            <label>Low stock threshold</label>
                                            <input
                                              type="number"
                                              name="low_stock_threshold"
                                              className="form-control "
                                              value={
                                                this.state.cimonProducts
                                                  .low_stock_threshold
                                              }
                                              name="low_stock_threshold"
                                              onChange={(e) =>
                                                this.handleChange(e)
                                              }
                                            />
                                            {this.state.errors
                                              .low_stock_threshold !== "" ? (
                                              <div className="validate_error">
                                                <p>
                                                  {
                                                    this.state.errors
                                                      .low_stock_threshold
                                                  }
                                                </p>
                                              </div>
                                            ) : null}
                                          </div>
                                        </div>
                                      );
                                    }
                                  })()}

                                  <div className="form-group-product">
                                    <div className="row">
                                      <div className="col-md-12">
                                        <label>Stock Availability</label>
                                      </div>
                                      <div className="col-md-12">
                                        <select
                                          className="form-control"
                                          value={
                                            this.state.cimonProducts
                                              .stock_availability
                                              ? this.state.cimonProducts
                                                  .stock_availability
                                              : ""
                                          }
                                          onChange={this.handleselectChange}
                                        >
                                          <option value="In Stock">
                                            In Stock
                                          </option>
                                          <option value="Out Of Stock">
                                            Out Of Stock
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div className="tab-pane" id="tab_c">
                                  <div className="tab-icon"></div>
                                  <div className="gallery-dimension-wrap">
                                    <div className="gallery-single">
                                      <div className="form-group">
                                        <label>Gallery Files</label>
                                        <input
                                          type="file"
                                          className="form-control"
                                          onChange={this.uploadMultipleFiles}
                                          multiple
                                        />
                                        {this.state.upload_status === true ? (
                                          <ReactSpinner
                                            type="border"
                                            color="dark"
                                            size="1"
                                          />
                                        ) : (
                                          ""
                                        )}
                                      </div>

                                      <div className=" multi-preview">
                                        {this.state.productfileArray.map(
                                          (image, index) => (
                                            <div
                                              key={index}
                                              className="single-img"
                                            >
                                              <img
                                                src={image.image_url}
                                                id={index}
                                                alt="..."
                                                width="80px"
                                                height="70px"
                                              />
                                              <span
                                                value={index}
                                                onClick={() =>
                                                  this.handleRemoveProductImg(
                                                    index
                                                  )
                                                }
                                              >
                                                <img
                                                  src={images(`./close.jpg`)}
                                                  className="img-fluid "
                                                  alt="customer"
                                                ></img>
                                              </span>
                                            </div>
                                          )
                                        )}
                                      </div>
                                    </div>

                                    <div className="gallery-single">
                                      <div className="form-group">
                                        <label>Dimension Files</label>
                                        <input
                                          type="file"
                                          className="form-control"
                                          onChange={this.uploadDimensionFiles}
                                          multiple
                                        />
                                        {this.state.dimen_upload_status ===
                                        true ? (
                                          <ReactSpinner
                                            type="border"
                                            color="dark"
                                            size="1"
                                          />
                                        ) : (
                                          ""
                                        )}
                                      </div>
                                      <div className="multi-preview">
                                        {this.state.dimensionfileArray.map(
                                          (image, index) => (
                                            <div
                                              key={index}
                                              className="single-img"
                                            >
                                              <img
                                                src={image.image_url}
                                                id={index}
                                                alt="..."
                                                width="80px"
                                                height="70px"
                                              />
                                              <span
                                                value={index}
                                                onClick={() =>
                                                  this.handleRemoveDimensionImg(
                                                    index
                                                  )
                                                }
                                              >
                                                <img
                                                  src={images(`./close.jpg`)}
                                                  className="img-fluid "
                                                  alt="customer"
                                                ></img>
                                              </span>
                                            </div>
                                          )
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="tab-pane" id="tab_d">
                                  <div className="tab-icon"></div>
                                  <CKEditor
                                    data={
                                      
                                      this.state.cimonProducts.specifications !=
                                      null
                                        ? this.state.cimonProducts
                                            .specifications
                                        : ""
                                    }
                                     onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                      allowedContent: true,
                                      extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                
                                      filebrowserImageUploadUrl:
                                        apiUrl +
                                        "/admin/upload/imageupload-new",
                                    }}
                                    onInit={(editor) => {}}
                                    onChange={this.onEditorChange3}
                                  />
                                </div>

                                <div className="tab-pane" id="tab_e">
                                  <h5>Select Related products</h5>             
                                                     {" "}
                                  <MultiSelect
                                    options={
                                      this.state.related_products_options
                                    }
                                    value={this.state.related_products_selected}
                                    onChange={this.setSelected} //  labelledBy={"Select"}
                                  />
                                </div>
                                <div
                                  className="tab-pane product-downloads-content"
                                  id="tab_f"
                                >
                                  <div className="tab-icon"></div>
                                  <div className="gallery-dimension-wrap">
                                    <div className="gallery-single">
                                      <label>2D CAD File</label>
                                      <ProductDownloads
                                        onuplaodProfile={
                                          this.onproductdownload_1
                                        }
                                        value={
                                          this.state.cadfile_2D.image_url
                                            ? this.state.cadfile_2D.image_url
                                            : ""
                                        }
                                        errors={this.state.errors}
                                      />
                                      {this.state.download_1_status === true ? (
                                        <ReactSpinner
                                          type="border"
                                          color="dark"
                                          size="1"
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                    <div className="gallery-single">
                                      <label>3D CAD File</label>
                                      <ProductDownloads
                                        onuplaodProfile={
                                          this.onproductdownload_2
                                        }
                                        value={
                                          this.state.cadfile_3D.image_url
                                            ? this.state.cadfile_3D.image_url
                                            : ""
                                        }
                                        errors={this.state.errors}
                                      />
                                      {this.state.download_2_status === true ? (
                                        <ReactSpinner
                                          type="border"
                                          color="dark"
                                          size="1"
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                    <div className="gallery-single">
                                    <label>Download</label>
                                        <input
                                          type="text"
                                          name="downloadbtn"
                                          className="form-control "
                                            value={
                                                this.state.cimonProducts.downloadbtn
                                            }
                                          name="downloadbtn"
                                            onChange={(e) =>
                                                this.handleChange(e)
                                            }
                                        />
                                      {/* <label>XpanelDesigner</label> */}
                                      <ProductDownloads
                                        onuplaodProfile={
                                          this.onproductdownload_3
                                        }
                                        value={
                                          this.state.xpaneldesigner.image_url
                                            ? this.state.xpaneldesigner
                                                .image_url
                                            : ""
                                        }
                                        errors={this.state.errors}
                                      />
                                      {this.state.download_3_status === true ? (
                                        <ReactSpinner
                                          type="border"
                                          color="dark"
                                          size="1"
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </div>
                              
                              
                                    <CKEditor
                                    data={
                                      
                                      this.state.cimonProducts.downloadcontent1 !=
                                      null
                                        ? this.state.cimonProducts
                                            .downloadcontent1
                                        : ""
                                    }
                                     onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                      allowedContent: true,
                                      extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                
                                      filebrowserImageUploadUrl:
                                        apiUrl +
                                        "/admin/upload/imageupload-new",
                                    }}
                                    onInit={(editor) => {}}
                                    onChange={this.onDownloadcontent1}
                                  />

{/* <CKEditor
                                    data={
                                      
                                      this.state.cimonProducts.downloadcontent2 !=
                                      null
                                        ? this.state.cimonProducts
                                            .downloadcontent2
                                        : ""
                                    }
                                     onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                      allowedContent: true,
                                      extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                
                                      filebrowserImageUploadUrl:
                                        apiUrl +
                                        "/admin/upload/imageupload-new",
                                    }}
                                    onInit={(editor) => {}}
                                    onChange={this.onDownloadcontent2}
                                  /> */}
                              
                                  </div>
                                </div>

                                <div className="tab-pane" id="tab_g">
                                  <div className="form-group attribute_container">
                                    <h5>Attributes</h5>
                                    <button
                                      type="button"
                                      onClick={this.handleAddAttributes}
                                      className="add-btn btn btn-success"
                                    >
                                      Add New
                                    </button>
                                    <div className="attributes">
                                      {this.state.attributes.map(
                                        (attribute, idx) => (
                                          <div key={idx} className="row pt-2">
                                            <div className="col-md-12 pb-2 pl-0 pr-0 main_loop_sub">
                                              <input
                                                type="text"
                                                className="form-control w-100"
                                                placeholder={`Enter name of attribute #${
                                                  idx + 1
                                                }`}
                                                value={attribute.Attribute_id}
                                                onChange={this.handleAttributesNameChange(
                                                  idx
                                                )}
                                              />
                                              <button
                                                className="rem-btn btn btn-danger"
                                                type="button"
                                                onClick={this.handleRemoveAttributes(
                                                  idx
                                                )}
                                              >
                                                <span aria-hidden="true">
                                                  &times;
                                                </span>
                                              </button>
                                            </div>

                                            <div className="attributes_sub">
                                              <div className="row sub_loop ">
                                                {attribute.Attribute_values.map(
                                                  (attribute_sub, sub_idx) => (
                                                    <div
                                                      key={sub_idx}
                                                      className="col-md-3 pb-2 sub_loop_12"
                                                    >
                                                      <input
                                                        type="number"
                                                        className="form-control w-100"
                                                        placeholder={`Value #${
                                                          sub_idx + 1
                                                        }`}
                                                        value={attribute_sub}
                                                        onChange={this.handleAttributessubNameChange(
                                                          idx,
                                                          sub_idx
                                                        )}
                                                      />

                                                      <button
                                                        className="rem-btn btn btn-danger"
                                                        type="button"
                                                        onClick={this.handleRemovesubAttributes(
                                                          idx,
                                                          sub_idx
                                                        )}
                                                      >
                                                        <span aria-hidden="true">
                                                          &times;
                                                        </span>
                                                      </button>
                                                    </div>
                                                  )
                                                )}
                                              </div>
                                            </div>

                                            <button
                                              disabled={
                                                attribute.Attribute_id
                                                  ? false
                                                  : "disabled"
                                              }
                                              type="button"
                                              onClick={this.handleAddsubAttributes(
                                                idx
                                              )}
                                              className="add-btn btn btn-success"
                                            >
                                              {" "}
                                              +{" "}
                                            </button>
                                          </div>
                                        )
                                      )}
                                    </div>
                                  </div>
                                </div>

                                <div className="tab-pane" id="tab_h">
                                  <div className="form-group-product">
                                    <label>Weight * (LBS)</label>

                                    <input
                                      name="weights"
                                      value={
                                        this.state.cimonProducts.weights
                                          ? this.state.cimonProducts.weights
                                          : 1
                                      }
                                      onChange={(e) => this.handleChange(e)}
                                      type="number"
                                      placeholder="weight *"
                                      className="form-control"
                                    />
                                    {this.state.errors.weights ? (
                                      <div className="error text-danger">
                                        {this.state.errors.weights}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>

                                  <div className="form-group-product">
                                    <label>Length (inches)</label>

                                    <input
                                      name="lengths"
                                      value={this.state.cimonProducts.lengths}
                                      onChange={(e) => this.handleChange(e)}
                                      type="number"
                                      placeholder="Length"
                                      className="form-control"
                                    />
                                    {this.state.errors.lengths ? (
                                      <div className="error text-danger">
                                        {this.state.errors.lengths}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>

                                  <div className="form-group-product">
                                    <label>Width (inches)</label>

                                    <input
                                      name="widths"
                                      value={this.state.cimonProducts.widths}
                                      onChange={(e) => this.handleChange(e)}
                                      type="number"
                                      placeholder="width "
                                      className="form-control"
                                    />
                                    {this.state.errors.widths ? (
                                      <div className="error text-danger">
                                        {this.state.errors.widths}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                  <div className="form-group-product">
                                    <label>Height (inches)</label>

                                    <input
                                      name="heights"
                                      value={this.state.cimonProducts.heights}
                                      onChange={(e) => this.handleChange(e)}
                                      type="number"
                                      placeholder="height"
                                      className="form-control"
                                    />
                                    {this.state.errors.heights ? (
                                      <div className="error text-danger">
                                        {this.state.errors.heights}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-12 attribute_container ">
                              <label for="">Additional Tabs</label>

                                   <button
                                      type="button"
                                      onClick={this.handleAddTabs}
                                      className="add-btn btn btn-success"
                                    >
                                    Add New
                                    </button>
                                    <div className="attributes">
                                      {this.state.tabattributes.map(
                                        (attribute, idx) => (
                                          <div key={idx} className="row pt-2">
                                            <div className="col-md-12 pb-2 pl-0 pr-0 main_loop_sub">
                                              <input
                                                type="text"
                                                className="form-control w-100"
                                                placeholder={`Enter name of tab #${
                                                  idx + 1
                                                }`}
                                                value={attribute.name}
                                                onChange={this.handleTabAttributesNameChange(
                                                  idx
                                                )}
                                              />

                                        

                                               <CKEditor
                                              data={attribute.description}
                                               onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }



                                  
                                  config={{
                                    extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  
                                                allowedContent: true,
                                                filebrowserImageUploadUrl:
                                                  apiUrl + "/admin/upload/imageupload-new",
                                              }}
                                              onInit={(editor) => {}}
                                              
                                              onChange={this.handleTabAttributesDescChange(
                                                idx
                                              )}
                                            />

                                              <button
                                                className="rem-btn btn btn-danger"
                                                type="button"
                                                onClick={this.handleRemoveTabAttributes(
                                                  idx
                                                )}
                                              >
                                                <span aria-hidden="true">
                                                  &times;
                                                </span>
                                              </button>
                                            </div>

                                          </div>
                                        )
                                      )}
                                    </div>

                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-3 col-md-12 ">
                        <div className="form-group thumbanail_container">
                          <label>Featured Image</label> 

                          {
                             this.state.cimonProducts.featured_image ||  this.state.cimonProducts.profile_image  || this.state.upload_data ? (<img
                                onClick={() =>
                                this.handleRemoveProfileImage(
                               // index
                                )
                                }
    
                                src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1617228058913-download.jpg"
                                className="img-fluid close_btn"
                                style={{ 'right' : '40px', 'top' :'70px'}}
                                alt="customer"
                                ></img>) : ''
                            }


                          <Uploadprofile ref={this.UploadprofileElement}
                            onuplaodProfile={this.onuplaodProfile}
                            value={this.state.cimonProducts.featured_image}
                            errors={this.state.errors}
                          />
                           {src && !submitcrop && (
                            <div class="cropscreen">
                              <div class="cropheader">
                            <label>Crop Image</label>
                            <button onClick={this.cropImage} class="btn btn-info">Crop Image</button>
                            </div>
                            <Cropper
                                src={src}
                                style={{height: 500, width: '100%'}}
                                // Cropper.js options
                                initialAspectRatio={1 / 1}
                                guides={false}
                                crop={this._crop.bind(this)}
                                onInitialized={this.onCropperInit.bind(this)}
                            /> 
                              </div>
                            )}

                          {this.state.upload_data ||
                          this.state.cimonProducts.featured_image ? (
                            <input
                              type="text"
                              value={this.state.cimonProducts.featured_name}
                              placeholder="Image Name"
                              onChange={(e) => this.handleChange(e)}
                              name="featured_name"
                              className="form-control"
                            />
                          ) : (
                            ""
                          )}
                        </div>
                        <div className=" ">
                        <div className="form-group thumbanail_container">
                        <label>Product tags</label>
                        <TagsInput
        value={this.state.tags}
        onChange={this.handletagChange.bind(this)}
       inputValue={this.state.tag}
       onChangeInput={this.handletagChangeInput.bind(this)}
      />
      <p>Separate tag with enter</p>
        </div>
        </div>

                        <div className="category_label">
                          <h3>Categories</h3>

                          {this.state.categories_list.map((categorysingle) => (
                            <div
                              key={categorysingle._id}
                              slug={categorysingle.slug}
                              className={
                                "checkboxloop parent_" +
                                categorysingle.parent_id
                              }
                            >
                              <input
                                key={categorysingle._id}
                                onChange={this.handleCheckChieldElement}
                                id= {categorysingle._id}
                                checked={
                                  this.state.categories.indexOf(
                                    categorysingle._id
                                  ) > -1
                                    ? true
                                    : false
                                }
                                value={categorysingle._id}
                                type="checkbox"
                              />{" "}
                              <label for= {categorysingle._id}>{categorysingle.category_name}</label>
                              {(() => {
                                if (
                                  this.state.categories.indexOf(
                                    categorysingle._id
                                  ) > -1 &&
                                  this.state.primary_category ===
                                    categorysingle._id
                                ) {
                                  return (
                                    <a
                                      onClick={() =>
                                        this.SetPrimaryCat(categorysingle._id)
                                      }
                                      className="Primat_cat"
                                    >
                                      Primary
                                    </a>
                                  );
                                } else if (
                                  this.state.categories[0] ===
                                    categorysingle._id &&
                                  this.state.primary_category === ""
                                ) {
                                  return (
                                    <a
                                      onClick={() =>
                                        this.SetPrimaryCat(categorysingle._id)
                                      }
                                      className="Primat_cat"
                                    >
                                      Primary
                                    </a>
                                  );
                                } else if (
                                  this.state.categories.indexOf(
                                    categorysingle._id
                                  ) > -1
                                ) {
                                  return (
                                    <a
                                      onClick={() =>
                                        this.SetPrimaryCat(categorysingle._id)
                                      }
                                      className="Primat_cat"
                                    >
                                      Make Primary
                                    </a>
                                  );
                                }
                              })()}
                            </div>
                          ))}

                          <div className="checkboxloop"></div>
                        </div>

               









                        <div className="category_label">
                          <h3>Attribute</h3>

                          {this.state.attribute_list.map((attributeSingle) => (
                            <div
                          
                              key={attributeSingle._id}
                              slug={attributeSingle.slug}
                              className={
                                "checkboxloop parent_" +
                                attributeSingle.parent_id
                              }
                            >
                              <input
                                id={attributeSingle._id}
                                key={attributeSingle._id}
                                onChange={this.handleCheckAttrbuteElement}
                                checked={
                                  this.state.productAttribute.indexOf(
                                    attributeSingle._id
                                  ) > -1
                                    ? true
                                    : false
                                }
                                value={attributeSingle._id}
                                type="checkbox"
                              />{" "}
                              
                              <label for= {attributeSingle._id}>{attributeSingle.name}</label> 
                            </div>
                          ))}

                          <div className="checkboxloop"></div>
                        </div>

               
               
               
                        <div className="faq-sideinputs">
                          <div className="faq-btns form-btn-wrap">
                            <div className="float-left"></div>

                            <a
                              style={{ color: "#0050a8" }}
                              href={
                                siteUrl +
                                this.state.url +
                                this.state.cimonProducts.slug
                              }
                              target="_blank"
                            >
                              View
                            </a>
                            <div className="update_btn input-group-btn float-right">
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="btn btn-info"
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                                Update 
                              </button>
                              {this.state.message !== "" ? (
                          <p className="tableinformation">
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          </p>
                        ) : null}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    </BlockUi>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminProductEdit;
