import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import * as manageProducts from "../../ApiServices/admin/products";
import DeleteConfirm from "../common/deleteConfirm";
import ReactSpinner from "react-bootstrap-spinner";
import { CURRENCY_SYMBOL } from "../../config.json";
import memoize from "memoize-one";
import { CSVLink, CSVDownload } from "react-csv";
import alertify from "alertifyjs";
import CategoryShortcut from "../common/categoryShortcut";
import { Container, Row, Col, Modal, Button } from "react-bootstrap";
import TrashConfirm from "../common/trashConfirm";
const images = require.context("../../assets/images/admin", true);

class AdminProducts extends Component {
  state = {
    confirm: {
      name: "Delete",
      message: "Are you sure you'd like to delete this item?",
      action: "delete"
    },
    search: "",
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    spinner: true,
    toggleCleared: false,
    contextActions_select: "-1",
    message: "",
    message_type: "",
    activetab: "tab_a",
    columns: [
      {
        name: "image",
        cell: row => (
          <img
            className="img-fluid"
            src={row.image_url ? row.image_url : images(`./no-image.jpg`)}
          />
        ),
        allowOverflow: true,
        button: true,
        width: "56px", // custom width for icon button
        hide: "sm"
      },
      {
        name: "PRODUCT",
        selector: "product_name",
        cell: row => (
          <Link to={"/admin/product/edit/" + row._id}>{row.product_name}</Link>
        ),
        sortable: true
      },

      {
        name: "Date",
        selector: "date",
        sortable: true,
        left: true,
        hide: "sm"
      },

      {
        name: "PRODUCT CATEGORY",
        //   selector: 'product_category',

        cell: row => (
          <div className="add_catgory_new">
            <a onClick={() => this.handleShow(row._id)}>
              {row.product_category ? row.product_category : "Add Category"}
            </a>
          </div>
        ),
        sortable: true,
        left: true,
        hide: "sm",
        width: "200px"
      },
      {
        name: "STATUS",
        selector: "status",
        sortable: true,
        left: true,
        hide: "sm",
        cell: row => (
          <div
            className={row.status == "In Stock" ? "bubble green" : "bubble red"}
          >
            {row.status}
          </div>
        )
      },

      {
        name: "PRICE",
        selector: "price",
        sortable: true,
        left: true,
        hide: "sm"
      },

      {
        name: "",
        cell: row => (
          <div className="action_dropdown" data-id={row.id}>
            <i
              data-id={row.id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            />
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >
              <Link
                to={"/admin/product/edit/" + row._id}
                className="dropdown-item"
              >
                Edit
              </Link>
              <Link
                onClick={() => this.saveAndtoogle(row._id, "delete")}
                className="dropdown-item"
              >
                Delete
              </Link>

              <Link
                onClick={() => this.saveAndtoogle(row._id, "duplicate")}
                className="dropdown-item"
              >
                Duplicate
              </Link>
              {row.page_status != 0 ? (
                <Link
                  onClick={() => this.saveAndtoogletrash(row._id, "trash")}
                  className="dropdown-item"
                >
                  Trash
                </Link>
              ) : (
                ""
              )}
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px" // custom width for icon button
      }
    ]
  };

  /* download csv function */
  downloadCSV = array => {
    const link = document.createElement("a");
    let csv = this.convertArrayOfObjectsToCSV(array);
    if (csv == null) return;

    const filename = "Product.csv";

    if (!csv.match(/^data:text\/csv/i)) {
      csv = `data:text/csv;charset=utf-8,${csv}`;
    }

    link.setAttribute("href", encodeURI(csv));
    link.setAttribute("download", filename);
    link.click();
  };

  /* remove comma from text  */
  removecomma(Items, key) {
    if (key == "title") {
      return Items.replace(",", "");
    } else {
      return Items;
    }
  }

  /*generating csv format */
  convertArrayOfObjectsToCSV = array => {
    let result;
    const columnDelimiter = ",";
    const lineDelimiter = "\n";
    const keys = Object.keys(array[0]);
    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;
    array.forEach(item => {
      let ctr = 0;
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter;
        result += this.removecomma(item[key], key);
        ctr++;
      });
      result += lineDelimiter;
    });
    return result;
  };

  searchSpace = event => {
    let keyword = event.target.value;
    this.setState({ search: keyword });
  };

  /* set date format */
  formatDate(string) {
    var options = { year: "numeric", month: "numeric", day: "numeric" };
    return new Date(string).toLocaleDateString("en-GB", options);
  }

  componentDidMount = () => {
    this.getAllProducts();
  };

  handleShow = value => {
    console.log("handleShow", value);
    this.setState({ show: true, show_value: value });
    // this.renderModal(value)
  };
  /* Get all products */
  getAllProducts = async () => {
    this.setState({ spinner: true });
    try {
      const response = await manageProducts.getAllProducts();

      if (response.data.status == 1) {
        console.log("response.data.data", response.data.data);
        const Setdata = { ...this.state.data_table };
        const SetdataTr = { ...this.state.data_table };
        const data_table_row = [];
        const data_table_row_trash = [];
        const data_table_row_draft = [];
        response.data.data.map((product, index) => {
          if (product.page_status == 0) {
            const SetdataTrash = {};
            SetdataTrash.product_name = product.product_name;
            SetdataTrash.slug = product.slug;
            SetdataTrash._id = product._id;
            SetdataTrash.short_description = product.short_description;
            SetdataTrash.description = product.description;
            SetdataTrash.date = this.formatDate(product.created_on);

            var img_url = "";
            if (product.images) {
              if (product.images.featured_image) {
                img_url = product.images.featured_image.image_url;
              }
            }

            SetdataTrash.image_url = img_url; // set image

            const cat_names_trash = [];
            product.category_info.map(cat => {
              cat_names_trash.push(cat.category_name);
            });

            let cat_string = "";
            cat_string = cat_names_trash.toString();
            cat_string = cat_string.replace(/,/g, ", ");
            SetdataTrash.product_category = cat_string; // set categories

            SetdataTrash.price =
              product.price && product.price.sales_price
                ? CURRENCY_SYMBOL + product.price.sales_price
                : product.price.regular_price
                ? CURRENCY_SYMBOL + product.price.regular_price
                : CURRENCY_SYMBOL + "0";

            SetdataTrash.status =
              product.inventory && product.inventory.stock_availability
                ? product.inventory.stock_availability
                : ""; // set categories

            data_table_row_trash.push(SetdataTrash);
          } else if (product.page_status == 2) {
            const SetdataDraft = {};
            SetdataDraft.product_name = product.product_name;
            SetdataDraft.slug = product.slug;
            SetdataDraft._id = product._id;
            SetdataDraft.short_description = product.short_description;
            SetdataDraft.description = product.description;
            SetdataDraft.date = this.formatDate(product.created_on);
            var img_url = "";
            if (product.images) {
              if (product.images.featured_image) {
                img_url = product.images.featured_image.image_url;
              }
            }

            SetdataDraft.image_url = img_url; // set image
            const cat_names_trash = [];
            product.category_info.map(cat => {
              cat_names_trash.push(cat.category_name);
            });
            let cat_string = "";
            cat_string = cat_names_trash.toString();
            cat_string = cat_string.replace(/,/g, ", ");
            SetdataDraft.product_category = cat_string; // set categories
            SetdataDraft.price =
              product.price && product.price.sales_price
                ? CURRENCY_SYMBOL + product.price.sales_price
                : product.price.regular_price
                ? CURRENCY_SYMBOL + product.price.regular_price
                : CURRENCY_SYMBOL + "0";
            SetdataDraft.status =
              product.inventory && product.inventory.stock_availability
                ? product.inventory.stock_availability
                : ""; // set categories

            data_table_row_draft.push(SetdataDraft);
          } else {
            const Setdata = {};
            Setdata.product_name = product.product_name;
            Setdata.slug = product.slug;
            Setdata._id = product._id;
            Setdata.short_description = product.short_description;
            Setdata.description = product.description;
            Setdata.date = this.formatDate(product.created_on);

            var img_url = "";
            if (product.images) {
              if (product.images.featured_image) {
                img_url = product.images.featured_image.image_url;
              }
            }
            Setdata.image_url = img_url; // set image
            const cat_names = [];
            product.category_info.map(cat => {
              cat_names.push(cat.category_name);
            });
            let cat_string = "";
            cat_string = cat_names.toString();
            cat_string = cat_string.replace(/,/g, ", ");
            Setdata.product_category = cat_string; // set categories

            Setdata.price =
              product.price && product.price.sales_price
                ? CURRENCY_SYMBOL + product.price.sales_price
                : product.price.regular_price
                ? CURRENCY_SYMBOL + product.price.regular_price
                : CURRENCY_SYMBOL + "0";

            Setdata.status =
              product.inventory && product.inventory.stock_availability
                ? product.inventory.stock_availability
                : ""; // set categories

            data_table_row.push(Setdata);
          }
        });
        this.setState({
          data_table: data_table_row,
          dataTrash: data_table_row_trash,
          dataDraft: data_table_row_draft
        });
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* delete products   */
  deleteProduct = async () => {
    let id = this.state.index;
    const response = await manageProducts.deleteProduct(id);
    if (response) {
      if (response.data.status == 1) {
        this.getAllProducts();
        this.toogle();
      }
    }
  };
  /* trash products   */
  trashProduct = async () => {
    let id = this.state.index;
    const response = await manageProducts.trashProduct(id);
    if (response) {
      if (response.data.status == 1) {
        this.getAllProducts();
        this.toogleTrash();
      }
    }
  };
  toogleTrash = () => {
    let status = !this.state.modal_trash;
    this.setState({ modal_trash: status });
  };
  saveAndtoogletrash = id => {
    this.setState({ index: id });
    this.toogleTrash();
  };
  /* duplicate products   */
  duplicateProduct = async () => {
    let id = this.state.index;
    const response = await manageProducts.duplicateProduct(id);
    if (response) {
      alertify.set("notifier", "position", "top-right");
      if (response.data.status == 1) {
        this.getAllProducts();
        this.toogle();
        alertify.success(response.data.message);
      } else {
        alertify.error(response.data.message);
      }
    }
  };

  doAction = action => {
    switch (action) {
      case "delete":
        this.deleteProduct();
        break;
      case "duplicate":
        this.duplicateProduct();
      default:
    }
  };

  saveAndtoogle = (id, action) => {
    switch (action) {
      case "duplicate":
        this.setState({
          confirm: {
            name: "Duplicate",
            message: "Are you sure you want to duplicate this item? ",
            action: action
          }
        });
        break;
      default:
        this.setState({
          confirm: {
            name: "Delete",
            message: "Are you sure you'd like to delete this item?",
            action: action
          }
        });
    }
    this.setState({ index: id });
    this.toogle();
  };

  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };

  /* handling  trash , draft ,publish  page settings */
  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows });
  };
  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value });
  };
  actionHandlerProduct = async (productsids, contextActions, status) => {
    try {
      const response = await manageProducts.actionHandlerProduct(
        productsids,
        contextActions,
        status
      );
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success"
        });

        this.getAllProducts();
      } else {
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
        this.getAllProducts();
      }
    } catch (err) {
      this.setState({
        message: "Something went wrong",
        message_type: "error"
      });
    }
  };

  actionHandler = e => {
    const { selectedRows } = this.state;

    const productids = selectedRows.map(r => r._id);
    let status = "";

    let actionvalue = this.state.contextActions_select;

    if (actionvalue === "-1") {
      return;
    } else if (actionvalue === "0") {
      status = "Trash";
    } else if (actionvalue === "1") {
      status = "Published";
    } else if (actionvalue === "2") {
      status = "Draft";
    } else if (actionvalue === "2") {
      status = "Delete trash";
    }

    if (
      window.confirm(
        `Are you sure you want to move ` + status + ` :\r ${productids}?`
      )
    ) {
      this.setState({ toggleCleared: !this.state.toggleCleared });
      this.actionHandlerProduct(productids, actionvalue, status);
    }
  };

  /* selecting trash , draft */
  tabActive = async tab => {
    this.setState({ activetab: tab });
  };

  /* initalize product */
  intializeProduct = async () => {
    console.log("intializeProduct");
    this.getAllProducts();
  };
  handleClose = () => {
    this.setState({ show: false });
  };
  render() {
    let rowData = this.state.data_table;
    let search = this.state.search;
    let rowDataTrash = this.state.dataTrash;
    let rowDataDraft = this.state.dataDraft;
    const contextActions = memoize(actionHandler => (
      <div className="common_action_section">
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value="-1">Select</option>
          {this.state.activetab !== "tab_a" ? (
            <option value="1">Published</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_b" ? (
            <option value="0">Trash</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_c" ? (
            <option value="2">Draft</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_a" &&
          this.state.activetab !== "tab_c" ? (
            <option value="3">Delete Trash</option>
          ) : (
            ""
          )}
        </select>
        <button className="danger" onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ));

    if (search.length > 0) {
      search = search.trim().toLowerCase();
      rowData = rowData.filter(l => {
        return (
          l.product_name.toLowerCase().match(search) ||
          l.status.toLowerCase().match(search)
        );
      });
      rowDataTrash = rowDataTrash.filter(l => {
        return (
          l.product_name.toLowerCase().match(search) ||
          l.status.toLowerCase().match(search)
        );
      });
      rowDataDraft = rowDataDraft.filter(l => {
        return (
          l.product_name.toLowerCase().match(search) ||
          l.status.toLowerCase().match(search)
        );
      });
    }

    return (
      <React.Fragment>
        <div className="container-fluid admin-body ">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents products home-inner-content pt-4 pb-4 pr-4">
                  <div className="main_admin_page_common product-summary-tab-wrapper">
                    <div className="">
                      <div className="admin_breadcum">
                        <div className="row">
                          <div className="col-md-1">
                            <p className="page-title">Products</p>
                          </div>
                          <div className="col-md-6">
                            <Link
                              className="add_new_btn"
                              to="/admin/products/addnew"
                            >
                              Add New
                            </Link>

                            <CSVLink
                              filename={"Product.csv"}
                              className="Export_data_btn"
                              data={rowData}
                            >
                              {" "}
                              Export Data
                            </CSVLink>
                          </div>
                          <div className="col-md-5">
                            <div className="searchbox">
                              <div className="commonserachform">
                                <span />
                                <input
                                  type="text"
                                  placeholder="Search"
                                  onChange={e => this.searchSpace(e)}
                                  name="search"
                                  className="search form-control"
                                />
                                <input type="submit" className="submit_form" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <React.Fragment></React.Fragment>
                      {this.state.message !== "" ? (
                        <p className="tableinformation">
                          <div className={this.state.message_type}>
                            <p>{this.state.message}</p>
                          </div>
                        </p>
                      ) : null}

                      <ul className="nav nav-pills nav-fill">
                        <li
                          className={
                            this.state.activetab === "tab_a" ? "active" : ""
                          }
                        >
                          <a
                            href="#tab_a"
                            data-toggle="pill"
                            onClick={() => this.tabActive("tab_a")}
                          >
                            All ({rowData.length})
                          </a>
                        </li>

                        <li
                          className={
                            this.state.activetab === "tab_b" ? "active" : ""
                          }
                        >
                          <a
                            href="#tab_b"
                            data-toggle="pill"
                            onClick={() => this.tabActive("tab_b")}
                          >
                            Trash ({rowDataTrash.length})
                          </a>
                        </li>

                        <li
                          className={
                            this.state.activetab === "tab_c" ? "active" : ""
                          }
                        >
                          <a
                            href="#tab_c"
                            data-toggle="pill"
                            onClick={() => this.tabActive("tab_c")}
                          >
                            Draft ({rowDataDraft.length})
                          </a>
                        </li>
                      </ul>
                      <div
                        style={{
                          display:
                            this.state.spinner === true
                              ? "flex justify-content-center "
                              : "none"
                        }}
                        className="overlay text-center"
                      >
                        <ReactSpinner type="border" color="primary" size="10" />
                      </div>
                      <div className="col-xl-12 col-md-12 col-sm-12 p-0">
                        <div className="tab-content">
                          <div className="tab-pane active" id="tab_a">
                            <DataTable
                              columns={this.state.columns}
                              data={rowData}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                          <div className="tab-pane" id="tab_b">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataTrash}
                              selectableRows
                              highlightOnHover
                              pagination  
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>

                          <div className="tab-pane" id="tab_c">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataDraft}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Delete/Duplicate confirm box */}
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          name={this.state.confirm.name}
          message={this.state.confirm.message}
          action={this.state.confirm.action}
          deleteUser={this.doAction}
        />
        <TrashConfirm
          modal={this.state.modal_trash}
          toogle={this.toogleTrash}
          trash_action={this.trashProduct}
        />
        <div>
          <Modal className="modal_living_popup" show={this.state.show}>
            <button type="button" class="close">
              <span onClick={this.handleClose} aria-hidden="true">
                ×
              </span>
              <span class="sr-only">Close</span>
            </button>
            <Modal.Body>
              <div className="category_label">
                <CategoryShortcut
                  value={this.state.show_value}
                  triggerParentUpdate={this.intializeProduct}
                />
              </div>
            </Modal.Body>
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminProducts;
