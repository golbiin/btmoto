import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import * as manageProducts from "../../ApiServices/admin/products";
import * as settingsService from "../../ApiServices/admin/settings";
import * as attributeService from "../../ApiServices/admin/attributeService";
import CKEditor from "ckeditor4-react";
import { apiUrl, siteUrl } from "../../../src/config.json";
import MultiSelect from "react-multi-select-component";
import ProductDownloads from "../common/productDownloads";
import TagsInput from "react-tagsinput";
import "react-tagsinput/react-tagsinput.css";
import BlockUi from 'react-block-ui'; 
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css"; 
class AdminProductAdd extends Component {
  constructor(props) {
    super(props);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleCheckbox = this.handleCheckbox.bind(this);
    this.uploadMultipleFiles = this.uploadMultipleFiles.bind(this);
    this.onEditorChange1 = this.onEditorChange1.bind(this);
    this.onEditorChange2 = this.onEditorChange2.bind(this);
    this.onEditorChange3 = this.onEditorChange3.bind(this);
    this.UploadprofileElement = React.createRef(); 
  }
  state = {
    spinner: false,
    productfileArray: [],
    dimensionfileArray: [],
    upload_status: false,
    dimen_upload_status: false,
    download_1_status: false,
    download_2_status: false,
    download_3_status: false,
    activetab: "tab_a",
    tabData: [
      { name: "Tab 1", isActive: true },
      { name: "Tab 2", isActive: false },
      { name: "Tab 3", isActive: false }
    ],
    editorContent: "",
    editorSpecifications: "",
    profile_image: "",
    featured_name: "",
    attributes: [],
    tabattributes: [],
    checked: false,
    data: {
      product_name: "",
      slug: "",
      description: "",
      banner_description: "",
      product_sub_text: "",
      model_number: "",
      weights: "1",
      lengths: "",
      widths: "",
      heights: "",
      meta_tag: "",
      meta_description: "",
      orderno : '',
      downloadbtn : ''
    },
    price: {
      regular_price: "0",
      sales_price: "0"
    },
    inventory: {
      product_sku: "",
      manage_stock: false,
      stock_availability: "In Stock"
    },
    related_products: [],
    related_products_options: [],
    related_products_selected: [],
    primary_category: "",
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    upload_data: "",
    categories_list: [],
    categories: [],
    cadfile_2D: "",
    cadfile_3D: "",
    xpaneldesigner: "",
    productAttribute :[],
    attribute_list: [],

    allow_backorders: "do_not_allow",
    tags: [],
    tag: "",
    cropped:"",
    submitcrop:false,
    src: null,
    cropResult: null,
    crop: {
      unit: '%',
      width: 50,
      height: 50 
    },
  };
  _crop() {
    // image in dataUrl
    this.cropper.getCroppedCanvas().toBlob(blob => {
      if (!blob) {
        //reject(new Error('Canvas is empty'));
        console.error('Canvas is empty');
        return;
      } 
      const reader = new FileReader();
      reader.readAsArrayBuffer(blob);
      
      const blobFile = new File([blob], 'image.jpg'); 
      this.setState({ upload_data: blobFile });  
      window.URL.revokeObjectURL(this.fileUrl);
      this.fileUrl = window.URL.createObjectURL(blob);
      console.log(this.fileUrl);
      this.UploadprofileElement.current.setImage(this.fileUrl);
      this.setState({ cropped: this.fileUrl });  
    } );
  }
  cropImage = e => { 
    this.setState({ submitcrop:  true  });
  }
  onCropperInit(cropper) {
      this.cropper = cropper;
  }
  onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () =>
        this.setState({ src: reader.result })
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  // If you setState the crop in here you should return false.
  onImageLoaded = image => {
    this.imageRef = image;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    // You could also use percentCrop:
    // this.setState({ crop: percentCrop });
    this.setState({ crop });
  };

  

  /* react editor on Changes */
  onEditorChange1(evt) {
    const newdata = { ...this.state.data };
    newdata["description"] = evt.editor.getData();
    this.setState({
      data: newdata
    });
  }

  onEditorChange2(evt) {
    this.setState({
      editorContent: evt.editor.getData()
    });
  }

  onEditorChange3(evt) {
    this.setState({
      editorSpecifications: evt.editor.getData()
    });
  }

  /* Upload multiple  Files */
  uploadMultipleFiles = async e => {
    this.setState({ upload_status: true });
    const response1 = await manageProducts.uploadProductImages(e.target.files);
    if (response1.data.status == 1) {
      response1.data.data.file_location.map((item, key) =>
        this.setState({
          productfileArray: this.state.productfileArray.concat({
            image_name: item.Location,
            image_url: item.Location
          })
        })
      );
      this.setState({ upload_status: false });
    } else {
      this.setState({
        submitStatus: false,
        message: response1.data.message,
        responsetype: "error"
      });
      this.setState({ upload_status: false });
    }
  };

  /* Fetched image Remove */
  handleRemoveProductImg(i) {
    const filteredvalue = this.state.productfileArray.filter(
      (s, sidx) => i !== sidx
    );
    if (filteredvalue) {
      this.setState({
        productfileArray: filteredvalue
      });
    }
  }

  uploadDimensionFiles = async e => {
    this.setState({ dimen_upload_status: true });
    const response1 = await manageProducts.uploadProductImages(e.target.files);
    if (response1.data.status == 1) {
      response1.data.data.file_location.map((item, key) =>
        this.setState({
          dimensionfileArray: this.state.dimensionfileArray.concat({
            image_name: item.Location,
            image_url: item.Location
          })
        })
      );
      this.setState({ dimen_upload_status: false });
    } else {
      this.setState({
        submitStatus: false,
        message: response1.data.message,
        responsetype: "error"
      });
      this.setState({ dimen_upload_status: false });
    }
  };

  handleRemoveDimensionImg(i) {
    const filteredvalue = this.state.dimensionfileArray.filter(
      (s, sidx) => i !== sidx
    );
    if (filteredvalue) {
      this.setState({
        dimensionfileArray: filteredvalue
      });
    }
  }

  tabActive = async tab => {
    this.setState({ activetab: tab });
  };
  /* Checkbox on Changes */
  handleCheck(e) {
    this.setState({
      checked: e.target.checked
    });
  }

  schema = {
    product_name: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    slug: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),

    description: Joi.string().allow(""),
    short_description: Joi.string().allow(""),
    banner_description: Joi.string().allow(""),
    product_sub_text: Joi.string().allow(""),
    model_number: Joi.string().allow(""),
    profile_image: Joi.string(),
    product_tag: Joi.string().allow(""),
    downloadbtn : Joi.string().allow(""),
    regular_price: Joi.number()
    .allow("")
    .allow("0")
      .error(() => {
        return {
          message: "Regular Price must be a number"
        };
      }),
    sales_price: Joi.number().allow("")
    .allow("0"),
    product_sku: [Joi.string(), Joi.string().allow("")],
    featured_name: Joi.string().allow(""),
    stock_quantity: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed"
        };
      }),
    allow_backorders: Joi.string().allow(""),
    meta_tag: Joi.string().allow(""),
    meta_description: Joi.string().allow(""),
    orderno: 
    Joi.alternatives(
      Joi.number().allow("").allow(0),
      Joi.string().allow("").allow(0)
  ),
    low_stock_threshold: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed"
        };
      }),
    weights: Joi.number()
      .positive()
      .integer()
      .error(() => {
        return {
          message: "Only Positive Integers Allowed"
        };
      }),
    lengths: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed"
        };
      }),
    widths: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed"
        };
      }),
    heights: Joi.number()
      .positive()
      .integer()
      .allow("")
      .allow("0")
      .error(() => {
        return {
          message: "Only Positive Integers Allowed"
        };
      })
  };

  /*Input  Handle Change */

  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];

    data[input.name] = input.value;
    this.setState({ data, errors });
  };
  handlePriceChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const price = { ...this.state.price };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    price[input.name] = input.value;
    this.setState({ price, errors });
  };
  handleinventoryChange = async ({ currentTarget: input }) => {
    let inventory = { ...this.state.inventory };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    inventory[input.name] = input.value;
    this.setState({ inventory, errors });
  };

  /*Joi validation Call*/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Form Submit */
  handleSubmit = async () => {
    const data = {
      product_name: this.state.data.product_name,
      slug: this.state.data.slug,
      description: this.state.data.description,
      banner_description: this.state.data.banner_description,
      product_sub_text: this.state.data.product_sub_text,
      model_number: this.state.data.model_number,
      weights: this.state.data.weights,
      lengths: this.state.data.lengths,
      widths: this.state.data.widths,
      heights: this.state.data.heights,
      heights: this.state.data.heights,
      regular_price: this.state.price.regular_price ? this.state.price.regular_price : 0
    };
    const errors = { ...this.state.errors };
    let result = Joi.validate(data, this.schema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });

        if (this.state.upload_data) {
          // check for featured images
          const response1 = await manageProducts.uploadFeaturedImage(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = {
              image_name: this.state.data.featured_name,
              image_url: response1.data.data.file_location
            };
            this.setState({ profile_image: filepath });

            this.setState({ upload_data: "" });
            this.updateProductData();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }
        } else {
          this.updateProductData();
        }
      }
    }
  };

  /* add products */
  updateProductData = async () => {
    let stock_quantity,
      low_stock_threshold,
      allow_backorders,
      available_stock_quntity;

    if (this.state.inventory.manage_stock) {
      stock_quantity = this.state.inventory.stock_quantity
        ? this.state.inventory.stock_quantity
        : "0";
      low_stock_threshold = this.state.inventory.low_stock_threshold
        ? this.state.inventory.low_stock_threshold
        : "0";
      available_stock_quntity = this.state.available_stock_quntity
        ? this.state.available_stock_quntity
        : this.state.inventory.stock_quantity;
      allow_backorders = this.state.allow_backorders;
    } else {
      stock_quantity = "0";
      low_stock_threshold = "0";
      available_stock_quntity = "0";
      allow_backorders = "do_not_allow";
      available_stock_quntity = "0";
    }
    const inventory = {
      product_sku: this.state.inventory.product_sku,
      manage_stock: this.state.inventory.manage_stock,
      manage_inventory: {
        stock_quantity: stock_quantity,
        allow_backorders: allow_backorders,
        low_stock_threshold: low_stock_threshold,
        available_stock_quntity: available_stock_quntity
      },
      stock_availability: this.state.inventory.stock_availability
    };
    const packages = {
      weights: this.state.data.weights,
      lengths: this.state.data.lengths,
      widths: this.state.data.widths,
      heights: this.state.data.heights
    };
    const product_data = {
      product_name: this.state.data.product_name,
      slug: this.state.data.slug,
      short_description: this.state.data.description,
      description: this.state.editorContent,
      specifications: this.state.editorSpecifications,
      categories: this.state.categories,
      price: this.state.price,
      inventory: inventory,
      related_products: this.state.related_products,
      primary_category: this.state.primary_category,
      attributes: this.state.attributes,
      url: this.state.url,
      tabdata: this.state.tabattributes,
      banner_description: this.state.data.banner_description,
      product_sub_text: this.state.data.product_sub_text,
      model_number: this.state.data.model_number,
      packages: packages,
      meta_tag: this.state.data.meta_tag,
      meta_description: this.state.data.meta_description,
      orderno : this.state.data.orderno ? this.state.data.orderno : 0,
      product_tag: this.state.tags,
      productAttribute: this.state.productAttribute,
      downloadbtn : this.state.data.downloadbtn 
    };

    product_data["images"] = {
      featured_image: this.state.profile_image,
      gallery_images: this.state.productfileArray,
      dimension_images: this.state.dimensionfileArray
    };

    product_data["downloads"] = {
      cadfile_2D: this.state.cadfile_2D,
      cadfile_3D: this.state.cadfile_3D,
      xpaneldesigner: this.state.xpaneldesigner
    };
    const response = await manageProducts.createProduct(product_data);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
  };
  /* upload profile image */
  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors };
    let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      
      if (value.target.files && value.target.files.length > 0) {
        const reader = new FileReader();
        reader.addEventListener('load', () =>
          this.setState({ src: reader.result })
        );
        reader.readAsDataURL(value.target.files[0]);
      }

      let file = value.target.files[0];
      upload_data = file;
      this.setState({ upload_data: upload_data });
    }
  };

  /*products download  */
  onproductdownload_1 = async (value, item) => {
    let errors = { ...this.state.errors };
    let cadfile_2D = { ...this.state.cadfile_2D };

    if (item === "errors") {
      errors["cadfile_2D"] = value;
    } else {
      this.setState({ download_1_status: true });
      let file = value.target.files[0];
      const response1 = await manageProducts.uploadFeaturedImage(file);
      if (response1.data.status == 1) {
        let filepath = {
          image_name: response1.data.data.file_name,
          image_url: response1.data.data.file_location
        };
        this.setState({ cadfile_2D: filepath });
      } else {
        this.setState({
          message: response1.data.message,
          responsetype: "error"
        });
      }
      this.setState({ download_1_status: false });
    }
  };

  onproductdownload_2 = async (value, item) => {
    let errors = { ...this.state.errors };
    let cadfile_3D = { ...this.state.cadfile_3D };
    if (item === "errors") {
      errors["cadfile_3D"] = value;
    } else {
      this.setState({ download_2_status: true });
      let file = value.target.files[0];
      const response1 = await manageProducts.uploadFeaturedImage(file);
      if (response1.data.status == 1) {
        let filepath = {
          image_name: response1.data.data.file_name,
          image_url: response1.data.data.file_location
        };
        this.setState({ cadfile_3D: filepath });
      } else {
        this.setState({
          message: response1.data.message,
          responsetype: "error"
        });
      }
      this.setState({ download_2_status: false });
    }
  };

  onproductdownload_3 = async (value, item) => {
    let errors = { ...this.state.errors };
    let xpaneldesigner = { ...this.state.xpaneldesigner };
    if (item === "errors") {
      errors["xpaneldesigner"] = value;
    } else {
      this.setState({ download_3_status: true });
      let file = value.target.files[0];
      const response1 = await manageProducts.uploadFeaturedImage(file);
      if (response1.data.status == 1) {
        let filepath = {
          image_name: response1.data.data.file_name,
          image_url: response1.data.data.file_location
        };
        this.setState({ xpaneldesigner: filepath });
      } else {
        this.setState({
          message: response1.data.message,
          responsetype: "error"
        });
      }
      this.setState({ download_3_status: false });
    }
  };

  componentDidMount = () => {
    this.getAllProductCategories();
    this.getAllProductsIdsandNames();
    this.getAllAttributes();
    this.getCurrentproductUrl(0);
  };

  
  /* get all attribute */

  getAllAttributes = async () => {
    const response = await attributeService.getAllAttributes();
    if (response.data.status == 1) {
      if (response.data.data) {
        console.log('all attibutes', response.data.data);
        this.setState({
         attribute_list: response.data.data,
        });
      }
    }
  };

  /* set attribute items */

  handleCheckAttrbuteElement = (event) => {
    let clickedValue = event.target.value;

    if (event.target.checked) {
      const list = [...this.state.productAttribute, clickedValue];
 
      this.setState({
        productAttribute: list,
      });
    } else {
      let productAttribute = [...this.state.productAttribute]; // make a separate copy of the array
      let index = productAttribute.indexOf(event.target.value);
      if (index !== -1) {
        productAttribute.splice(index, 1);
        this.setState({ productAttribute: productAttribute });
      }
    }
  };

  handleCheckbox = e => {
    const inventory = { ...this.state.inventory };
    inventory["manage_stock"] = e.target.checked;
    this.setState({ inventory: inventory });
  };
  handleselectChange = async e => {
    const inventory = { ...this.state.inventory };
    inventory["stock_availability"] = e.target.value;
    this.setState({ inventory: inventory });
  };

  handlebackorderChange = async e => {
    this.setState({ allow_backorders: e.target.value });
  };

  /* Get all products Categories */
  getAllProductCategories = async () => {
    const response = await settingsService.getAllProductCategories();
    if (response.data.status == 1) {
      this.setState({
        categories_list: response.data.data
      });
    }
  };

  /* (onchange / add)    product category or primary category */

  handleCheckChieldElement = event => {
    let clickedValue = event.target.value;

    if (event.target.checked) {
      const list = [...this.state.categories, clickedValue];
      if (list.indexOf(this.state.primary_category) < 0) {
        this.getCurrentproductUrl(list[0]);
        this.setState({
          primary_category: list[0]
        });
      }
      this.setState({
        categories: list
      });
    } else {
      let categories = [...this.state.categories]; // make a separate copy of the array
      let index = categories.indexOf(event.target.value);
      if (index !== -1) {
        categories.splice(index, 1);
        this.setState({ categories: categories });
      }
      if (categories.indexOf(this.state.primary_category) < 0) {
        // setting Primary Category default option
        this.getCurrentproductUrl(categories[0]);
        this.setState({
          primary_category: categories[0]
        });
      }
    }
  };

  /* get all products and ids for generating select option */
  getAllProductsIdsandNames = async () => {
    const response = await manageProducts.getAllProductsIdsandNames();
    if (response.data.status == 1) {
      const Setdata = { ...this.state.related_products };
      const data_related_row = [];
      const data_selected_row = [];
      response.data.data.map((Products, index) => {
        const Setdata = {};
        Setdata.value = Products._id;
        Setdata.label = Products.product_name;
        if (this.state.related_products.indexOf(Products._id) > -1) {
          data_selected_row.push(Setdata);
        }
        data_related_row.push(Setdata);
      });

      this.setState({ related_products_options: data_related_row });
      this.setState({ related_products_selected: data_selected_row });
    }
  };

  /* set selected relative products */

  setSelected = event => {
    const data_selected_row = [];
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value;
    });

    this.setState({ related_products: data_selected_row });
    this.setState({ related_products_selected: event });
  };

  /* common function for generating current url and set primary category  */

  SetPrimaryCat = Cat_id => {
    this.setState({ primary_category: Cat_id });
    this.getCurrentproductUrl(Cat_id);
  };

  /* handling product attributes */

  handleAttributesNameChange = idx => evt => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute;
      return { ...attribute, Attribute_id: evt.target.value };
    });
    this.setState({ attributes: newAttributes });
  };

  /* additional tabs */
  handleTabAttributesNameChange = idx => evt => {
    const newAttributes = this.state.tabattributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute;
      return { ...attribute, name: evt.target.value };
    });
    this.setState({ tabattributes: newAttributes });
  };

  handleTabAttributesDescChange = idx => evt => {
    const newAttributes = this.state.tabattributes.map((attribute, sidx) => {
      if (idx !== sidx) return attribute;
      return { ...attribute, description: evt.editor.getData() };
    });
    this.setState({ tabattributes: newAttributes });
  };
  handleAddTabs = () => {
    this.setState({
      tabattributes: this.state.tabattributes.concat([
        { name: "", description: "" }
      ])
    });
  };

  handleRemoveTabAttributes = idx => () => {
    this.setState({
      tabattributes: this.state.tabattributes.filter((s, sidx) => idx !== sidx)
    });
  };

  handleAddAttributes = () => {
    this.setState({
      attributes: this.state.attributes.concat([
        { Attribute_id: "", Attribute_values: [] }
      ])
    });
  };

  handleRemoveAttributes = idx => () => {
    this.setState({
      attributes: this.state.attributes.filter((s, sidx) => idx !== sidx)
    });
  };

  handleAttributessubNameChange = (idx, sub_idx) => evt => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx === sidx) {
        const SetsubAttribute = attribute.Attribute_values;
        SetsubAttribute[sub_idx] = evt.target.value;
        return { ...attribute, Attribute_values: SetsubAttribute };
      } else {
        return attribute;
      }
    });

    this.setState({ attributes: newAttributes });
  };

  handleAddsubAttributes = idx => () => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx === sidx) {
        const SetsubAttribute = attribute.Attribute_values;
        SetsubAttribute.push("");
        return { ...attribute, Attribute_values: SetsubAttribute };
      } else {
        return attribute;
      }
    });
    this.setState({ attributes: newAttributes });
  };

  handleRemovesubAttributes = (idx, sub_idx) => () => {
    const newAttributes = this.state.attributes.map((attribute, sidx) => {
      if (idx === sidx) {
        const SetsubAttribute = attribute.Attribute_values.filter(
          (s, count) => sub_idx !== count
        );

        return { ...attribute, Attribute_values: SetsubAttribute };
      } else {
        return attribute;
      }
    });

    this.setState({ attributes: newAttributes });
  };

  /* get current products Url */
  getCurrentproductUrl = async Cat_id => {
    let url = "";
    this.setState({ submit_status: true });
    const cimonProducts = { ...this.state.cimonProducts };
    const response = await manageProducts.getCurrentproductUrl(Cat_id);
    if (response.data.status == 1) {
      this.setState({ submit_status: false });

      if (response.data.data !== "/null" && response.data.data !== "/") {
        url = "/product" + response.data.data + "/";
        this.setState({
          url: url
        });
      } else {
        url = "/product/";
        this.setState({
          url: url
        });
      }
    }
  };

  pasteSplit(data) {
    const separators = [
      ",",
      ";",
      "\\(",
      "\\)",
      "\\*",
      "/",
      ":",
      "\\?",
      "\n",
      "\r"
    ];
    return data.split(new RegExp(separators.join("|"))).map(d => d.trim());
  }
  handletagChange(tags) {
    console.log("handletagChange", tags);
    this.setState({ tags: tags });
  }

  handletagChangeInput(tag) {
    console.log("handletagChangeInput", tag);
    this.setState({ tag: tag });
  }



  
  handleRemoveProfileImage  = () => {

    
    let filepath = {
      image_name: '',
      image_url: '',
    }; 
    this.setState({
      featured_array: filepath,
      profile_image: '',
      featured_image : '',
      upload_data : ''
    }); 
  };


  render() {
    const images = require.context("../../assets/images/admin", true);
    const { crop, croppedImageUrl, src,submitcrop } = this.state;
    let checkedCond = this.state.checked;
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12 product-add-page  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                  <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className="row addpage-form-wrap">
                      <div className="col-lg-9 col-md-12">
                        {this.state.errors.regular_price !== "" ? (
                          <div className="validate_error">
                            <p>{this.state.errors.regular_price}</p>
                          </div>
                        ) : null}

                        <div className="form-group">
                          <label htmlFor="">Product Name</label>
                          <input
                            name="product_name"
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add title *"
                            className="form-control"
                          />
                          {this.state.errors.product_name ? (
                            <div className="error text-danger">
                              {this.state.errors.product_name}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Slug</label>
                          <input
                            name="slug"
                            onChange={this.handleChange}
                            value={this.state.slug}
                            type="text"
                            placeholder="slug *"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Url</label>
                          <input
                            name="url"
                            value={
                              siteUrl + this.state.url + this.state.data.slug
                            }
                            disabled
                            type="text"
                            placeholder="url *"
                            className="form-control"
                          />
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Banner Sub Text</label>
                          <input
                            name="banner_description"
                            onChange={this.handleChange}
                            value={this.state.data.banner_description}
                            type="text"
                            placeholder="Sub Text "
                            className="form-control"
                          />
                          {this.state.errors.banner_description ? (
                            <div className="error text-danger">
                              {this.state.errors.banner_description}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Product Sub Text</label>
                          <input
                            name="product_sub_text"
                            onChange={this.handleChange}
                            value={this.state.data.product_sub_text}
                            type="text"
                            placeholder="Sub Text "
                            className="form-control"
                          />
                          {this.state.errors.product_sub_text ? (
                            <div className="error text-danger">
                              {this.state.errors.product_sub_text}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Product Model Number</label>
                          <input
                            name="model_number"
                            onChange={this.handleChange}
                            value={this.state.data.model_number}
                            type="text"
                            placeholder="Model Number"
                            className="form-control"
                          />
                          {this.state.errors.model_number ? (
                            <div className="error text-danger">
                              {this.state.errors.model_number}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Meta tag</label>
                          <input
                            name="meta_tag"
                            onChange={this.handleChange}
                            value={this.state.data.meta_tag}
                            type="text"
                            placeholder="Meta Tag"
                            className="form-control"
                          />
                          {this.state.errors.meta_tag ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_tag}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Meta Description</label>
                          <input
                            name="meta_description"
                            onChange={this.handleChange}
                            value={this.state.data.meta_description}
                            type="text"
                            placeholder="Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.meta_description ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_description}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        
                        
                        <div className="form-group">
                          <label htmlFor="">Product Order</label>
                          <input
                            name="orderno"
                            onChange={this.handleChange}
                            value={this.state.data.orderno}
                            type="number"
                      //      min = "0"
                          //  placeholder="Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.orderno ? (
                            <div className="error text-danger">
                              {this.state.errors.orderno}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        
                        
                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-12">
                              <label htmlFor="">Short Description</label>
                              <CKEditor
                                data=""
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorChange1}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-12">
                              <label htmlFor="">Content</label>
                              <CKEditor
                                data=""
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorChange2}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="product-attribute-tab row bg-white">
                            <div className="col-xl-12 col-md-12 col-sm-12">
                              <p>
                                Product Data -{" "}
                                <select>
                                  <option>Simple Product</option>
                                  <option>Bundeled products</option>
                                </select>
                              </p>
                            </div>
                            <div className="col-xl-3 col-md-3 col-sm-3">
                              <ul className="nav nav-pills nav-fill">
                                <li
                                  className={
                                    this.state.activetab === "tab_a"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_a"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_a")}
                                  >
                                    Price
                                  </a>
                                </li>
                                <li
                                  className={
                                    this.state.activetab === "tab_b"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_b"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_b")}
                                  >
                                    Inventory
                                  </a>
                                </li>

                                <li
                                  className={
                                    this.state.activetab === "tab_c"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_c"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_c")}
                                  >
                                    Images
                                  </a>
                                </li>
                                <li
                                  className={
                                    this.state.activetab === "tab_d"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_d"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_d")}
                                  >
                                    Specifications
                                  </a>
                                </li>

                                <li
                                  className={
                                    this.state.activetab === "tab_e"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_e"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_e")}
                                  >
                                    Related products
                                  </a>
                                </li>

                                <li
                                  className={
                                    this.state.activetab === "tab_f"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_f"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_f")}
                                  >
                                    Downloads
                                  </a>
                                </li>

                                <li
                                  className={
                                    this.state.activetab === "tab_g"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_g"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_g")}
                                  >
                                    Attributes
                                  </a>
                                </li>

                                <li
                                  className={
                                    this.state.activetab === "tab_h"
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <a
                                    href="#tab_h"
                                    data-toggle="pill"
                                    onClick={() => this.tabActive("tab_h")}
                                  >
                                    Package
                                  </a>
                                </li>
                              </ul>
                            </div>

                            <div className="col-xl-9 col-md-9 col-sm-9 ">
                              <div className="tab-content">
                                <div className="tab-pane active" id="tab_a">
                                  <div className="tab-icon"></div>
                                  <div className="edit-details-head pb-4">
                                    <div className="form-group-product">
                                      <label>Regular Price</label>
                                      <input
                                        type="text"
                                        name="regular_price"
                                        className="form-control "
                                        onChange={this.handlePriceChange}
                                      />
                                    </div>

                                    <div className="form-group-product">
                                      <label>Sales Price</label>
                                      <input
                                        type="text"
                                        name="sales_price"
                                        className="form-control "
                                        onChange={this.handlePriceChange}
                                      />
                                      {this.state.errors.sales_price !== "" ? (
                                        <div className="validate_error">
                                          <p>{this.state.errors.sales_price}</p>
                                        </div>
                                      ) : null}
                                    </div>
                                  </div>
                                </div>
                                <div className="tab-pane" id="tab_b">
                                  <div className="form-group-product">
                                    <label>SKU</label>
                                    <input
                                      type="text"
                                      name="product_sku"
                                      className="form-control "
                                      onChange={e =>
                                        this.handleinventoryChange(e)
                                      }
                                    />
                                    {this.state.errors.product_sku !== "" ? (
                                      <div className="validate_error">
                                        <p>{this.state.errors.product_sku}</p>
                                      </div>
                                    ) : null}
                                  </div>
                                  <div className="form-group-product">
                                    <div className="row">
                                      <div className="col-md-4">
                                        <label>Manage stock?</label>
                                      </div>
                                      <div className="col-md-6 checkbox_btn">
                                        <input
                                          type="checkbox"
                                          onChange={this.handleCheckbox}
                                          checked={
                                            this.state.inventory.manage_stock
                                          }
                                        />
                                      </div>
                                    </div>
                                  </div>

                                  {(() => {
                                    if (this.state.inventory.manage_stock) {
                                      return (
                                        <div>
                                          <div className="form-group-product">
                                            <label>Stock quantity</label>
                                            <input
                                              type="number"
                                              name="stock_quantity"
                                              className="form-control "
                                              value={
                                                this.state.inventory
                                                  .stock_quantity
                                              }
                                              onChange={e =>
                                                this.handleinventoryChange(e)
                                              }
                                            />
                                            {this.state.errors
                                              .stock_quantity !== "" ? (
                                              <div className="validate_error">
                                                <p>
                                                  {
                                                    this.state.errors
                                                      .stock_quantity
                                                  }
                                                </p>
                                              </div>
                                            ) : null}
                                          </div>

                                          <div className="form-group-product">
                                            <div className="row">
                                              <div className="col-md-12">
                                                <label>Allow backorders?</label>
                                              </div>
                                              <div className="col-md-12">
                                                <select
                                                  className="form-control"
                                                  value={
                                                    this.state.allow_backorders
                                                      ? this.state
                                                          .allow_backorders
                                                      : ""
                                                  }
                                                  onChange={
                                                    this.handlebackorderChange
                                                  }
                                                >
                                                  <option value="do_not_allow">
                                                    Do not allow
                                                  </option>
                                                  <option value="allow_but_notify">
                                                    Allow, but notify customer
                                                  </option>
                                                  <option value="allow">
                                                    Allow
                                                  </option>
                                                </select>
                                              </div>
                                            </div>
                                          </div>

                                          <div className="form-group-product">
                                            <label>Low stock threshold</label>
                                            <input
                                              type="number"
                                              name="low_stock_threshold"
                                              className="form-control "
                                              value={
                                                this.state.inventory
                                                  .low_stock_threshold
                                              }
                                              name="low_stock_threshold"
                                              onChange={e =>
                                                this.handleinventoryChange(e)
                                              }
                                            />
                                            {this.state.errors
                                              .low_stock_threshold !== "" ? (
                                              <div className="validate_error">
                                                <p>
                                                  {
                                                    this.state.errors
                                                      .low_stock_threshold
                                                  }
                                                </p>
                                              </div>
                                            ) : null}
                                          </div>
                                        </div>
                                      );
                                    }
                                  })()}

                                  <div className="form-group-product">
                                    <div className="row">
                                      <div className="col-md-4">
                                        <label>Stock Availability</label>
                                      </div>
                                      <div className="col-md-6">
                                        <select
                                          className="form-control"
                                          value={
                                            this.state.inventory
                                              .stock_availability
                                          }
                                          onChange={this.handleselectChange}
                                        >
                                          <option value="In Stock">
                                            In Stock
                                          </option>
                                          <option value="Out Of Stock">
                                            Out Of Stock
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div className="tab-pane" id="tab_c">
                                  <div className="tab-icon"></div>
                                  <div className="gallery-dimension-wrap">
                                    <div className="gallery-single">
                                      <div className="form-group">
                                        <label title="Add More than five images to scroll.">
                                          Gallery Files{" "}
                                        </label>
                                        <input
                                          type="file"
                                          className="form-control"
                                          onChange={this.uploadMultipleFiles}
                                          multiple
                                        />
                                        {this.state.upload_status === true ? (
                                          <ReactSpinner
                                            type="border"
                                            color="dark"
                                            size="1"
                                          />
                                        ) : (
                                          ""
                                        )}
                                      </div>
                                      <div className="multi-preview">
                                        {this.state.productfileArray.map(
                                          (image, index) => (
                                            <div className="single-img">
                                              <img
                                                src={image.image_url}
                                                id={index}
                                                alt="..."
                                                width="80px"
                                                height="70px"
                                              />
                                              <span
                                                value={index}
                                                onClick={() =>
                                                  this.handleRemoveProductImg(
                                                    index
                                                  )
                                                }
                                              >
                                                <img
                                                  src={images(`./close.jpg`)}
                                                  className="img-fluid "
                                                  alt="customer"
                                                ></img>
                                              </span>
                                            </div>
                                          )
                                        )}
                                      </div>
                                    </div>

                                    <div className="gallery-single">
                                      <div className="form-group">
                                        <label>Dimension Files</label>
                                        <input
                                          type="file"
                                          className="form-control"
                                          onChange={this.uploadDimensionFiles}
                                          multiple
                                        />
                                        {this.state.dimen_upload_status ===
                                        true ? (
                                          <ReactSpinner
                                            type="border"
                                            color="dark"
                                            size="1"
                                          />
                                        ) : (
                                          ""
                                        )}
                                      </div>
                                      <div className="multi-preview">
                                        {this.state.dimensionfileArray.map(
                                          (image, index) => (
                                            <div className="single-img">
                                              <img
                                                src={image.image_url}
                                                id={index}
                                                alt="..."
                                                width="80px"
                                                height="70px"
                                              />
                                              <span
                                                value={index}
                                                onClick={() =>
                                                  this.handleRemoveDimensionImg(
                                                    index
                                                  )
                                                }
                                              >
                                                <img
                                                  src={images(`./close.jpg`)}
                                                  className="img-fluid "
                                                  alt="customer"
                                                ></img>
                                              </span>
                                            </div>
                                          )
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="tab-pane" id="tab_d">
                                  <div className="tab-icon"></div>
                                  <CKEditor
                                    data=""
                                     onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                      extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                      allowedContent: true,
                                      filebrowserImageUploadUrl:
                                        apiUrl + "/admin/upload/imageupload-new"
                                    }}
                                    onInit={editor => {}}
                                    onChange={this.onEditorChange3}
                                  />
                                </div>
                                <div className="tab-pane" id="tab_e">
                                  <h5>Select Related products</h5>             
                                   {" "}
                                  <MultiSelect
                                    options={
                                      this.state.related_products_options
                                    }
                                    value={this.state.related_products_selected}
                                    onChange={this.setSelected} //  labelledBy={"Select"}
                                  />
                                </div>
                                <div
                                  className="tab-pane product-downloads-content"
                                  id="tab_f"
                                >
                                  <div className="tab-icon"></div>
                                  <div className="gallery-dimension-wrap">
                                    <div className="gallery-single">
                                      <label>2D CAD File</label>
                                      <ProductDownloads
                                        onuplaodProfile={
                                          this.onproductdownload_1
                                        }
                                        value={
                                          this.state.cadfile_2D.image_url
                                            ? this.state.cadfile_2D.image_url
                                            : ""
                                        }
                                        errors={this.state.errors}
                                      />
                                      {this.state.download_1_status === true ? (
                                        <ReactSpinner
                                          type="border"
                                          color="dark"
                                          size="1"
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                    <div className="gallery-single">
                                      <label>3D CAD File</label>
                                      <ProductDownloads
                                        onuplaodProfile={
                                          this.onproductdownload_2
                                        }
                                        value={
                                          this.state.cadfile_3D.image_url
                                            ? this.state.cadfile_3D.image_url
                                            : ""
                                        }
                                        errors={this.state.errors}
                                      />
                                      {this.state.download_2_status === true ? (
                                        <ReactSpinner
                                          type="border"
                                          color="dark"
                                          size="1"
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                    <div className="gallery-single">
                                    <label>Download</label>
                                    <input
                            name="downloadbtn"
                            onChange={this.handleChange}
                            value={this.state.data.downloadbtn}
                            type="text"
                            placeholder="XpanelDesigner"
                            className="form-control"
                          />
                                      {/* <label>XpanelDesigner</label> */}
                                      <ProductDownloads
                                        onuplaodProfile={
                                          this.onproductdownload_3
                                        }
                                        value={
                                          this.state.xpaneldesigner.image_url
                                            ? this.state.xpaneldesigner
                                                .image_url
                                            : ""
                                        }
                                        errors={this.state.errors}
                                      />
                                      {this.state.download_3_status === true ? (
                                        <ReactSpinner
                                          type="border"
                                          color="dark"
                                          size="1"
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                  </div>
                                </div>
                                <div className="tab-pane" id="tab_g">
                                  <div className="form-group attribute_container">
                                    <h5>Attributes</h5>
                                    <button
                                      type="button"
                                      onClick={this.handleAddAttributes}
                                      className="add-btn btn btn-success"
                                    >
                                      Add New
                                    </button>
                                    <div className="attributes">
                                      {this.state.attributes.map(
                                        (attribute, idx) => (
                                          <div key={idx} className="row pt-2">
                                            <div className="col-md-12 pb-2 pl-0 pr-0 main_loop_sub">
                                              <input
                                                type="text"
                                                className="form-control w-100"
                                                placeholder={`Enter name of attribute #${idx +
                                                  1}`}
                                                value={attribute.Attribute_id}
                                                onChange={this.handleAttributesNameChange(
                                                  idx
                                                )}
                                              />
                                              <button
                                                className="rem-btn btn btn-danger"
                                                type="button"
                                                onClick={this.handleRemoveAttributes(
                                                  idx
                                                )}
                                              >
                                                <span aria-hidden="true">
                                                  &times;
                                                </span>
                                              </button>
                                            </div>

                                            <div className="attributes_sub">
                                              <div className="row sub_loop ">
                                                {attribute.Attribute_values.map(
                                                  (attribute_sub, sub_idx) => (
                                                    <div
                                                      key={sub_idx}
                                                      className="col-md-3 pb-2 sub_loop_12"
                                                    >
                                                      <input
                                                        type="number"
                                                        className="form-control w-100"
                                                        placeholder={`Value #${sub_idx +
                                                          1}`}
                                                        value={attribute_sub}
                                                        onChange={this.handleAttributessubNameChange(
                                                          idx,
                                                          sub_idx
                                                        )}
                                                      />

                                                      <button
                                                        className="rem-btn btn btn-danger"
                                                        type="button"
                                                        onClick={this.handleRemovesubAttributes(
                                                          idx,
                                                          sub_idx
                                                        )}
                                                      >
                                                        <span aria-hidden="true">
                                                          &times;
                                                        </span>
                                                      </button>
                                                    </div>
                                                  )
                                                )}
                                              </div>
                                            </div>
                                            <button
                                              disabled={
                                                attribute.Attribute_id
                                                  ? false
                                                  : "disabled"
                                              }
                                              type="button"
                                              onClick={this.handleAddsubAttributes(
                                                idx
                                              )}
                                              className="add-btn btn btn-success"
                                            >
                                              {" "}
                                              +{" "}
                                            </button>
                                          </div>
                                        )
                                      )}
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="tab-pane product-downloads-content"
                                  id="tab_h"
                                >
                                  <div className="tab-icon"></div>
                                  <div className="form-group-product">
                                    <label>Weight * (LBS)</label>
                                    <input
                                      type="number"
                                      name="weights"
                                      className="form-control "
                                      value={
                                        this.state.data.weights
                                          ? this.state.data.weights * 1
                                          : 1
                                      }
                                      onChange={this.handleChange}
                                    />
                                    {this.state.errors.weights ? (
                                      <div className="error text-danger">
                                        {this.state.errors.weights}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                  <div className="form-group-product">
                                    <label>Length (inches)</label>
                                    <input
                                      type="number"
                                      name="lengths"
                                      className="form-control "
                                      value={
                                        this.state.data.lengths
                                          ? this.state.data.lengths * 1
                                          : ""
                                      }
                                      onChange={this.handleChange}
                                    />
                                    {this.state.errors.lengths ? (
                                      <div className="error text-danger">
                                        {this.state.errors.lengths}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                  <div className="form-group-product">
                                    <label>Width (inches)</label>
                                    <input
                                      type="number"
                                      name="widths"
                                      className="form-control "
                                      value={
                                        this.state.data.widths
                                          ? this.state.data.widths * 1
                                          : ""
                                      }
                                      onChange={this.handleChange}
                                    />
                                    {this.state.errors.widths ? (
                                      <div className="error text-danger">
                                        {this.state.errors.widths}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                  <div className="form-group-product">
                                    <label>Height (inches)</label>
                                    <input
                                      type="number"
                                      name="heights"
                                      className="form-control "
                                      value={
                                        this.state.data.heights
                                          ? this.state.data.heights * 1
                                          : ""
                                      }
                                      onChange={this.handleChange}
                                    />
                                    {this.state.errors.heights ? (
                                      <div className="error text-danger">
                                        {this.state.errors.heights}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-12 attribute_container ">
                              <label for="">Additional Tabs</label>

                              <button
                                type="button"
                                onClick={this.handleAddTabs}
                                className="add-btn btn btn-success"
                              >
                                Add New
                              </button>
                              <div className="attributes">
                                {this.state.tabattributes.map(
                                  (attribute, idx) => (
                                    <div key={idx} className="row pt-2">
                                      <div className="col-md-12 pb-2 pl-0 pr-0 main_loop_sub">
                                        <input
                                          type="text"
                                          className="form-control w-100"
                                          placeholder={`Enter name of tab #${idx +
                                            1}`}
                                          value={attribute.name}
                                          onChange={this.handleTabAttributesNameChange(
                                            idx
                                          )}
                                        />

                                        <CKEditor
                                          data=""
                                           onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                            allowedContent: true,
                                            filebrowserImageUploadUrl:
                                              apiUrl +
                                              "/admin/upload/imageupload-new"
                                          }}
                                          onInit={editor => {}}
                                          onChange={this.handleTabAttributesDescChange(
                                            idx
                                          )}
                                        />

                                        <button
                                          className="rem-btn btn btn-danger"
                                          type="button"
                                          onClick={this.handleRemoveTabAttributes(
                                            idx
                                          )}
                                        >
                                          <span aria-hidden="true">
                                            &times;
                                          </span>
                                        </button>
                                      </div>
                                    </div>
                                  )
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-3 col-md-12 ">
                        <div className="form-group thumbanail_container">
                          <label>Featured Image</label>

                          {
                             this.state.featured_image ||  this.state.profile_image  || this.state.upload_data ? (<img
                                onClick={() =>
                                this.handleRemoveProfileImage(
                               // index
                                )
                                }
    
                                src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1617228058913-download.jpg"
                                className="img-fluid close_btn"
                                style={{ 'right' : '40px', 'top' :'70px'}}
                                alt="customer"
                                ></img>) : ''
                            }

                            
                          <Uploadprofile  ref={this.UploadprofileElement}
                            onuplaodProfile={this.onuplaodProfile}
                            value={this.state.profile_image}
                            errors={this.state.errors}
                          />
                          {src && !submitcrop && (
                            <div class="cropscreen">
                              <div class="cropheader">
                            <label>Crop Image</label>
                            <button onClick={this.cropImage} class="btn btn-info">Crop Image</button>
                            </div>
                            <Cropper
                                src={src}
                                style={{height: 500, width: '100%'}}
                                // Cropper.js options
                                initialAspectRatio={1 / 1}
                                guides={false}
                                crop={this._crop.bind(this)}
                                onInitialized={this.onCropperInit.bind(this)}
                            /> 
                              </div>
                            )}
                          {this.state.upload_data ? (
                            <input
                              type="text"
                              placeholder="Image Name"
                              onChange={this.handleChange}
                              name="featured_name"
                              className="form-control"
                            />
                          ) : (
                            ""
                          )}
                        </div>

                        <div className=" ">
                          <div className="form-group product_tag thumbanail_container">
                            <label>Product tags</label>
                            <TagsInput
                              value={this.state.tags}
                              onChange={this.handletagChange.bind(this)}
                              inputValue={this.state.tag}
                              onChangeInput={this.handletagChangeInput.bind(
                                this
                              )}
                            />
                            <p>Separate tag with enter</p>
                          </div>
                        </div>

                        <div className="category_label">
                          <h3>Categories</h3>
                          {this.state.categories_list.map(categorysingle => (
                            <div
                              className={
                                "checkboxloop parent_" +
                                categorysingle.parent_id
                              }
                            >
                              <input
                                key={categorysingle._id}
                                onChange={this.handleCheckChieldElement}
                                checked={
                                  this.state.categories.indexOf(
                                    categorysingle._id
                                  ) > -1
                                    ? true
                                    : false
                                }
                                value={categorysingle._id}
                                type="checkbox"
                              />{" "}
                              {categorysingle.category_name}
                              {(() => {
                                if (
                                  this.state.categories.indexOf(
                                    categorysingle._id
                                  ) > -1 &&
                                  this.state.primary_category ===
                                    categorysingle._id
                                ) {
                                  return (
                                    <a
                                      onClick={() =>
                                        this.SetPrimaryCat(categorysingle._id)
                                      }
                                      className="Primat_cat"
                                    >
                                      Primary
                                    </a>
                                  );
                                } else if (
                                  this.state.categories[0] ===
                                    categorysingle._id &&
                                  this.state.primary_category === ""
                                ) {
                                  return (
                                    <a
                                      onClick={() =>
                                        this.SetPrimaryCat(categorysingle._id)
                                      }
                                      className="Primat_cat"
                                    >
                                      Primary
                                    </a>
                                  );
                                } else if (
                                  this.state.categories.indexOf(
                                    categorysingle._id
                                  ) > -1
                                ) {
                                  return (
                                    <a
                                      onClick={() =>
                                        this.SetPrimaryCat(categorysingle._id)
                                      }
                                      className="Primat_cat"
                                    >
                                      Make Primary
                                    </a>
                                  );
                                }
                              })()}
                            </div>
                          ))}

                          <div className="checkboxloop"></div>
                        </div>

                        <div className="category_label">
                          <h3>Attribute</h3>

                          {this.state.attribute_list.map((attributeSingle) => (
                            <div
                          
                              key={attributeSingle._id}
                              slug={attributeSingle.slug}
                              className={
                                "checkboxloop parent_" +
                                attributeSingle.parent_id
                              }
                            >
                              <input
                                id={attributeSingle._id}
                                key={attributeSingle._id}
                                onChange={this.handleCheckAttrbuteElement}
                                checked={
                                  this.state.productAttribute.indexOf(
                                    attributeSingle._id
                                  ) > -1
                                    ? true
                                    : false
                                }
                                value={attributeSingle._id}
                                type="checkbox"
                              />{" "}
                              
                              <label for= {attributeSingle._id}>{attributeSingle.name}</label> 
                            </div>
                          ))}

                          <div className="checkboxloop"></div>
                        </div>

               
                     
                     
                     
                        <div className="faq-sideinputs">
                          <div className="faq-btns form-btn-wrap">
                            <div className="float-left"></div>
                            <div className="update_btn input-group-btn float-right">
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="btn btn-info"
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                                Publish
                              </button>
                            </div>
                          </div>

                          {this.state.message !== "" ? (
                            <p className="tableinformation">
                              <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                              </div>
                            </p>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </BlockUi>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminProductAdd;
