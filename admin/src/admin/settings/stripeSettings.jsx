import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import Joi, { join } from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import ReactSpinner from "react-bootstrap-spinner";
class StripeSetting extends Component {
    constructor(props) {
        super(props);
        this.handleCheckbox = this.handleCheckbox.bind(this);
    }
    state = { 
        data: {
        live : false, 
        Testsecretkey: "" , Testpublishedkey: "" ,Livesecretkey : "" ,Livepublishedkey : ""},
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        spinner : true,
      };
    
      /*Joi validation schema */  
    schema = {
        Testsecretkey: Joi.string().allow(null),
        Testpublishedkey: Joi.string().allow(null),
        Livesecretkey:Joi.string().allow(null),
        Livepublishedkey:Joi.string().allow(null),
    };
  
    /*Input Handle Change */
    handleChange = async ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const data = { ...this.state.data };
        const errorMessage = this.validateProperty(input);
        
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        data[input.name] = input.value;
        this.setState({ data, errors });
        
    };
  
    /* Joi validation call*/
    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;

    };
    /* Form Submit */
     handleCheckbox = (e) =>{ 
         const data = { ...this.state.data };
        data['live'] = e.target.checked 
        this.setState({ data });
    }
     handleSubmit = async () => {
        const data = { ...this.state.data };
        const errors = { ...this.state.errors };
        let result = Joi.validate(data, this.schema);      
        try {
            this.setState({ spinner: true });
            this.setState({ submit_status : true });
            const data = {value : this.state.data}
            const response = await settingsService.updateStripe(data); 
            if (response.data.status === 1) {  
                this.setState({
                    message: response.data.message,
                    message_type: "success",
                });
                this.setState({ spinner: false });
                this.setState({ submit_status : false });
            } else {
                this.setState({
                    message: response.data.message,
                    message_type: "error",
                });
                this.setState({ spinner: false });
                this.setState({ submit_status : false });
            }
        } catch (err) {
            this.setState({ spinner: false });
            this.setState({ submit_status : false });
        }
     };

    componentDidMount = async () => {
        const id = this.props.match.params.id;
        this.getStripe(); 
    };
    /* Get Stripe */
    getStripe = async () => {
        const response =  await settingsService.getStripe();
        if (response.data.status == 1) {
          if(response.data.data){
              this.setState({ 
                data: response.data.data.value,
              });
              this.setState({ spinner: false });
          }      
        } 
      }
  
    render() { 
        return (
            <React.Fragment>
            <div className="container-fluid admin-body ">
                <div className="admin-row">
                    <div className="col-md-2 col-sm-12 sidebar">
                        <Adminsidebar props={this.props} />
                    </div>
                    <div className="col-md-10 col-sm-12  content">
                        <div className="row content-row">
                            <div className="col-md-12  header">
                                <Adminheader  props={this.props} />
                            </div>
                            <div className="col-md-12  contents  useradd-new pt-4 pb-4 pr-4" >   
                            <React.Fragment>
                            <div className="card">
                                <div className="card-header">Stripe Details</div>
                                <div className="card-body">
                        <React.Fragment>
                            <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                                <ReactSpinner type="border" color="primary" size="10" />
                            </div>
                        </React.Fragment>
                        {
                            this.state.message !== "" ? (
                                <p className="tableinformation">
                                    <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                    </div>
                                </p>
                            ) : null
                            } 
                    <div className='custom-control custom-switch stripesettings' >
                            <div>Live</div> 
                            <div>   
                                <input
                                type='checkbox'
                                className='custom-control-input'
                                id='stripe_id'
                                onChange={this.handleCheckbox}  
                                checked={this.state.data.live} 
                                />
                                <label className='custom-control-label' htmlFor="stripe_id"></label>
                            </div> 
                        </div>
                        <div className="form-group">
                            <label>Test Secret Key</label>
                            <input name="Testsecretkey"  onChange={this.handleChange} type="text" 
                            value={this.state.data.Testsecretkey}
                            placeholder="Test Secret Key" className="form-control"/>
                            {this.state.errors.Testsecretkey ? (<div className="error text-danger">
                            {this.state.errors.Testsecretkey}
                            </div>
                            ) : (
                            ""
                            )}
                        </div>
                        <div className="form-group">
                            <label>Test Published Key</label>
                            <input name="Testpublishedkey"  onChange={this.handleChange} type="text" placeholder="Test Published Key" value={this.state.data.Testpublishedkey} className="form-control"/>
                            {this.state.errors.Testpublishedkey ? (<div className="error text-danger">
                            {this.state.errors.Testpublishedkey}
                            </div>
                            ) : (
                            ""
                            )}
                        </div>
                        <div className="form-group">
                            <label>Live Secret Key</label>
                            <input name="Livesecretkey"  onChange={this.handleChange} type="text" 
                            value={this.state.data.Livesecretkey}
                            placeholder="Live Secret Key" className="form-control"/>
                            {this.state.errors.Livesecretkey ? (<div className="error text-danger">
                            {this.state.errors.Livesecretkey}
                            </div>
                            ) : (
                            ""
                            )}
                        </div>
                        <div className="form-group">
                            <label>Live Published Key</label>
                            <input name="Livepublishedkey"  onChange={this.handleChange} type="text" 
                                value={this.state.data.Livepublishedkey}
                            placeholder="Live Published Key" className="form-control"/>
                            {this.state.errors.Livepublishedkey ? (<div className="error text-danger">
                            {this.state.errors.Livepublishedkey}
                            </div>
                            ) : (
                            ""
                            )}
                        </div>
                    </div>
                </div>
                <div className="submit">
                    <button
                    disabled={this.state.submit_status === true ? "disabled" : false}                               
                    className="submit-btn" onClick={this.handleSubmit} >Save</button>
                </div>
            </React.Fragment>
        </div>
    </div>
</div>
</div>
</div>
</React.Fragment> );
    }
}
 
export default StripeSetting;