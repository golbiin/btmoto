import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import Joi from 'joi-browser'
import * as manageSettings from '../../ApiServices/admin/settings'
import { Link } from 'react-router-dom'
import { Container, Draggable } from 'react-smooth-dnd'
import { applyDrag } from './utils'
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import ReactSpinner from "react-bootstrap-spinner";
class ManageSlider extends Component {
  constructor (props) {
    super(props)
  }
  state = {
    data: {
      dots: false,
      time: 1
    },
    errors: {},
    attributes: [],
    message_save_or_delete: '',
    message_save_or_delete_type: '',
    message: '',
    message_type: ''
  }

  componentDidMount = () => {
       this.getSliderOptions();
  }

  chooseVideoType = val => {
    let data = { ...this.state.data }
    data.dots = val
    this.setState({ data })
  }


  /*Get all Menus */
  getSliderOptions = async () =>{
    try {
        const response =  await manageSettings.getSliderSettngs();
        if (response.data.status === 1) {
          
        this.setState({ data: response.data.data});
              
            
        } 
    } catch (err) {
        this.setState({ spinner: false});
    }
}

  /*****************Joi validation schema**************/

  schema = {
    dots: Joi.boolean(),
    time: Joi.number().positive().integer().error(() => {
      return {
        message: 'Only positive numbers',
      };
    }),
  }
  /*****************end Joi validation schema**************/

  /*****************INPUT HANDLE CHANGE **************/

  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors }
    const data = { ...this.state.data }
    const errorMessage = this.validateProperty(input)

    if (errorMessage) errors[input.name] = errorMessage
    else delete errors[input.name]
    data[input.name] = input.value
    this.setState({ data, errors })
  }

  /*****************JOI VALIDATION CALL**************/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)
    return error ? error.details[0].message : null
  }

  // /***************** FORM SUBMIT **************/
  // /***************** FORM SUBMIT **************/

  handleSubmit = async () => {
    this.setState({  
      message: '',
      message_type: ''
    });
    

    const checkState = {  
      dots: this.state.data.dots,
      time : this.state.data.time
    };

    const errors = { ...this.state.errors };
    let result = Joi.validate(this.state.data, this.schema);

    console.log('results',result);
    if (result.error) {

      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });

     
  } else {

    this.setState({ spinner: true ,
      submit_status : true
    });
    const response = await manageSettings.updateSettngs(this.state.data);
    if (response) {
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ spinner: false,
          submit_status : false
        
        });
      } else {
        this.setState({  
          message: response.data.message,
          message_type: "error"
        });
        this.setState({ spinner: false,
          submit_status : false });  
      }
      return response;
    }
  }





  
  }

  render () {
    console.log('data----->', this.state.data)
    return (
      <React.Fragment>
        <div className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12 content'>
              <div className='row content-row'>
                <div className='col-md-12 header'>
                  <Adminheader props={this.props} />
                </div>

                <div className='col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4'>
                  <React.Fragment>
                    <div className='categories-wrap p-4'>
                      <div className='row'>
                        <div className='col-md-12'>
                          <div className='card'>
                            <div className='card-header'>
                              <strong>Slider Options</strong>
                            </div>

                            <div className='row'>
                              <div className='col-md-12'>
                                <div className='card-body  manage-menus'>
                                  <div className='row'>
                                    <div className='col-md-8'>
                                      <p>Slider Dots</p>
                                    </div>
                                    <div className='col-md-4'>
                                      <div
                                        className='form-group slider_options'
                                        style={{ display: 'flex' }}
                                      >
                                        <div>
                                          <label for='videotypeid'>Yes</label>
                                          <input
                                            type='radio'
                                            name='url_type'
                                            id='videotypeid'
                                            checked={
                                              this.state.data.dots == true
                                            }
                                            onClick={() =>
                                              this.chooseVideoType(true)
                                            }
                                          />
                                        </div>
                                        <div>
                                          <label
                                            for='uploadvideoid'
                                            style={{
                                              paddingRight: '15px',
                                              paddingLeft: '8px',
                                              fontSize: '15px'
                                            }}
                                          >
                                            No
                                          </label>
                                          <input
                                            id='uploadvideoid'
                                            type='radio'
                                            name='url_type'
                                            checked={
                                              this.state.data.dots == false
                                            }
                                            onClick={() =>
                                              this.chooseVideoType(false)
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div className='row'>
                                    <div className='col-md-8'>
                                      <p>Slider Timer (Sec)</p>
                                    </div>
                                    <div className='col-md-4'>
                                      <input
                                        name='time'
                                        value={this.state.data.time}
                                        onChange={this.handleChange}
                                        type='number'
                                        placeholder='Time'
                                        min = '0'
                                        className='form-control'
                                      />
                                      {this.state.errors.time ? (
                                        <div className='error text-danger'>
                                          {this.state.errors.time}
                                        </div>
                                      ) : (
                                        ''
                                      )}
                                    </div>

                                    <div className='col-md-12 '>
                                      <div className='form-inline handleSubmit_slider form float-right form-btn-wrap'>
                                        <button
                                          onClick={this.handleSubmit}
                                          className='btn btn-info'

                                          disabled={this.state.submit_status === true ? "disabled" : false}
                                        >
                                           {this.state.submit_status ? (
                                    <ReactSpinner type="border" color="dark" size="1" />
                                ) : (
                                    ""
                                )}
                                          Update
                                        </button>
                                      </div>
                                    </div>
                              
                                    {this.state.message !== "" ? (
                                <div style={{'width': '100%',
                                'display' : 'inline-block',
                                'text-align' : 'center',
                                'font-size' : '16px'
                                }} className={this.state.message_type}>
                                  <p>{this.state.message}</p>
                                </div>
                              ) : null}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default ManageSlider
