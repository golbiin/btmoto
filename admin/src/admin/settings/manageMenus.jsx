import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import Joi from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import { Link } from "react-router-dom";
import { Container, Draggable } from 'react-smooth-dnd';
import { applyDrag } from './utils';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
class ManageMenus extends Component {
  constructor(props) {
    super(props);
    this.containerOnDrop = this.containerOnDrop.bind(this);
    this.containerOnDrop2 = this.containerOnDrop2.bind(this);
    this.containerOnDroplevel3 = this.containerOnDroplevel3.bind(this);
  }
    state = {
      selected_menu: '-1',
      Allmenus : [],
      allMenus: [
        {_id:1 , name: 'Main Menu',
          menu_items : [
          {
            name: 'Products',
            id : 0,
            url: '#',
            sublevel: [
                  {
                    name: 'IPC',
                    position : 3,
                    url: '#',
                  },
                  {
                    name: 'SCADA',
                    position : 4,
                    url: '#',
                    sublevel: [
                      {
                        name: 'elvee',
                        position : 0,
                        url: '#',
                      },
                    ]
                  },
            ]
          },
          {
            name: 'Industries',
            id : 1,
            url: 'success-stories'
          },
          {
            name: 'Support',
            id : 2,
            url: '#'
          },
          {
            name: 'About',
            id :3,
            url: '#',
          },
        ]
        },
        {_id:2 , name: 'Footer Menu'},
      ],     
      current_menu : [],
      createmenu : false,
      menuname : '',
      errors: {},
      attributes : [],
      message_save_or_delete : '',
      message_save_or_delete_type: '',
      message: '',
      message_type : ''
    };


    componentDidMount = () => {
      this.getAllMenus();
    }

/*Get all Menus */
    getAllMenus = async () =>{
          try {
              const response =  await settingsService.getAllMenus();
              if (response.data.status === 1) {
              this.setState({ Allmenus: response.data.data});
              this.setState({ selected_menu: "-1",
              message_save_or_delete: "",
              message_save_or_delete_type: "",             
            });          
              } 
          } catch (err) {
              this.setState({ spinner: false});
          }
    }
   BookmarkNode( node) {    
			if (!node.sublevel) {  
        return (
          <li className= {"class_"}   >
            <label>Name</label>
            <input type="text" className="form-control" value={node.name}/>
            <label>Url</label> <input type="text"  className="form-control" value={node.url}/>
            <input type="hidden"    value={node.position}/>
          </li>
        );  
   } else {
    return (
      <li className= {"class_"} >
         <label>Name</label>
            <input type="text" className="form-control" value={node.name}/>
            <label>Url</label> <input type="text"  className="form-control" value={node.url}/>
            <input type="hidden"    value={node.position}/>
            {node.sublevel.map((c) => (<ul key={c.position}> 
           
            {this.BookmarkNode(c)} 
             </ul>))}
      </li>
    );
   }
    };
    setmenudata = (e) => {
      const filtervalue = this.state.Allmenus.filter((s, sidx) =>
       s._id === e
    )
    if(filtervalue){
      if(filtervalue[0].menu_items){
        this.setState({
          attributes :filtervalue[0].menu_items,
          menuname : filtervalue[0].name
        });
      } else {
        this.setState({
          attributes : []
        });
      }
    } else {
      this.setState({
        attributes : []
      });
    }     
    }
    handleMenu  = async (e) => {
      this.setState({
        message: " ",
        message_type: "",
      });
      if(e.target.value !== "-1"){
        this.setState({ 
          selected_menu : e.target.value
        });
        
        this.setmenudata(e.target.value);
      } else {

        this.setState({ 
          selected_menu : e.target.value,
          attributes : [],
          createmenu :true
        }); 
      }
     
    }
/*create Menu */
    createmenu  = async (e) => {
      this.setState({ 
        createmenu : true
      });
    }
    /*Cancel Menu */
    cancelmenu  = async (e) => {
      this.setState({ 
        createmenu : false,
        message: " ",
        message_type: "",
      });
    }

    schema = {
      menuname: Joi.string().required().error(() => {
          return {
            message: 'Please be sure you’ve filled in the name. It is not allowed to be empty.',
          };
        }),
    };
    
    handleChange = (input) => event => {
      const errors = { ...this.state.errors };
      delete errors.validate;
      const errorMessage = this.validateProperty(input, event.target.value);
      if (errorMessage) errors[input] = errorMessage;
      else delete errors[input];
      this.setState({ [input]: event.target.value, errors });
    }
    validateProperty = (name, value) => {
      const obj = { [name]: value };
      const schema = { [name]: this.schema[name] };
      const { error } = Joi.validate(obj, schema);
      return error ? error.details[0].message : null;
    };

     /*Save Menu */
    savemenu = async (e) => {
      e.preventDefault();
      const checkState = {  
        menuname: this.state.menuname,
      };
      const errors = { ...this.state.errors };
      let result = Joi.validate(checkState, this.schema);
      if (result.error) {

        let path = result.error.details[0].path[0];
        let errormessage = result.error.details[0].message;
        errors[path] = errormessage;
        this.setState({ errors });
    } else {
        try {
          this.setState({ 
            submit_status: true,
            messagenew: "",
            message_type: "" 
          });             
              if(this.state.createmenu === true){
                const data = {
                  name : this.state.menuname
                }               
                const response = await settingsService.createMenu(data); 
                if (response) {
                  if(response.data.status === 1){
                    this.setState({ createmenu: false });
                    this.setState({
                      message: response.data.message,
                      message_type: "success",
                      menuname : ''
                    });
                    setTimeout(() => { 
                      this.setState({
                        message: '',
                        message_type: "",
                      });
                    }, 5000);

                    this.getAllMenus();

                  }else {
                    this.setState({
                      message: response.data.message,
                      message_type: "error",
                    });
                   }
                }
              } else {
                this.setState({ createmenu: false });
                this.setState({
                  message: "Something went wrong! Please try after some time",
                  message_type: "error",
                });
              }
              
        } catch (err) {

        }
  }
    };
/*Delete Menu */
    deleteMenu = async (e) => {
      const data = {  
        menunid: this.state.selected_menu,
      };
      try {
        this.setState({ 
          message_save_or_delete: "",
          message_save_or_delete_type: "" 
        });
        const response = await settingsService.deleteMenu(data); 
        if (response) {
            if(response.data.status === 1){
              this.setState({
                message_save_or_delete: response.data.message,
                message_save_or_delete_type: "success",
              });
              setTimeout(() => { 
                this.setState({
                  message_save_or_delete: '',
                  message_save_or_delete_type: "",
                  attributes: []
                });
              }, 5000);
              this.getAllMenus();
            } else {
              this.setState({
                message_save_or_delete: response.data.message,
                message_save_or_delete_type: "error",
              });
            }
        }
      } catch (err) {

        this.setState({
          message_save_or_delete: 'Something went wrong',
          message_save_or_delete_type: "error",
        });

      }
    };

    /*update Menu */
    updateMenu = async (e) => {
        const data = {  
          menunid: this.state.selected_menu,
          menu_items : this.state.attributes
        };
        try {
          this.setState({ 
            submit_status: true,
            message: "",
            message_type: "" 
          });
          const response = await settingsService.updateMenu(data); 
          if (response) {
            if(response.data.status === 1){
              this.setState({
                message_save_or_delete: response.data.message,
                message_save_or_delete_type: "success",
              });

              setTimeout(() => { 
                this.setState({
                  message_save_or_delete: '',
                  message_save_or_delete_type: "",
                });
              }, 5000);

            }else {
              this.setState({
                message_save_or_delete: response.data.message,
                message_save_or_delete_type: "error",
              });
              setTimeout(() => { 
                this.setState({
                  message_save_or_delete: '',
                  message_save_or_delete_type: "",
                });
              }, 5000);
             }
          }
        } catch (err) {

        }
    }
    
    handleAttributesNameChange = idx => evt => {
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx !== sidx) return attribute;
        return { ...attribute, name: evt.target.value };
      });
  
      this.setState({ attributes: newAttributes });
    };

    handleAttributesUrlChange = idx => evt => {
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx !== sidx) return attribute;
        return { ...attribute, url: evt.target.value.toLowerCase().replace(/\s+/g, '') };
      });
  
      this.setState({ attributes: newAttributes });
    };
 
    handleAddAttributes = () => {
      this.setState({
        attributes: this.state.attributes.concat([{ name: "" , url:"#", child_values : [], type :''}])
      });
    };
  
    handleRemoveAttributes = idx => () => {
      const filteredvalue =  this.state.attributes.filter((s, sidx) => idx !== sidx);
      if(filteredvalue) {
        this.setState({
          attributes: filteredvalue
        });
      }
     
    };
    handleAttributessubNameChange = (idx,sub_idx) => evt => {
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx === sidx){
          const SetsubAttribute = attribute.child_values
           SetsubAttribute[sub_idx].name = evt.target.value;
          return { ...attribute, child_values : SetsubAttribute};
        } else {
          return attribute;
        }
      });      
      this.setState({ attributes: newAttributes });
    };
    handleAttributessubUrlChange = (idx,sub_idx) => evt => {
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx === sidx){
          const SetsubAttribute = attribute.child_values
           SetsubAttribute[sub_idx].url = evt.target.value.toLowerCase().replace(/\s+/g, '');
          return { ...attribute, child_values : SetsubAttribute};
        } else {
          return attribute;
        }
      });
      this.setState({ attributes: newAttributes });
    };
    handleAddsubAttributes = idx => () => {
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx === sidx){
          const SetsubAttribute = attribute.child_values;
          SetsubAttribute.push({ name: "" , url:"#", child_values : [], type :''})
          return { ...attribute, child_values : SetsubAttribute};
        } else {
          return attribute;
        }
      });
      this.setState({ attributes: newAttributes });
    };
  
    handleRemovesubAttributes = (idx,sub_idx) => () => {
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx === sidx){
          const SetsubAttribute =  attribute.child_values.filter((s, count) => sub_idx !== count);         
          return { ...attribute, child_values : SetsubAttribute};
        } else {
          return attribute;
        }
      });
      this.setState({ attributes: newAttributes });
    };

    handleaddchilsubAttributes  = (idx,sub_idx) => () => {     
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx === sidx){          
            attribute.child_values.map((Subattribute, sidx) => {
              if (sidx === sub_idx){
                const SetsubAttribute = Subattribute.child_values;
                SetsubAttribute.push({ name: "" , url:"#", child_values : [] , type :''});                
                return { ...Subattribute, child_values : SetsubAttribute};               
              }else {
                return Subattribute;
              }
            });
          return attribute;
        } else {
          return attribute;
        }
      });

      
     this.setState({ attributes: newAttributes });
    };
    handleAttributesThirdlevelNameChange  = (idx,sub_idx,thrid_idx) => evt => {       
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx === sidx){
          attribute.child_values.map((Subattribute, sidx) => {
            if (sidx === sub_idx){
              const SetsubAttribute = Subattribute.child_values;
              SetsubAttribute[thrid_idx].name = evt.target.value;              
              return { ...Subattribute, child_values : SetsubAttribute};             
            }else {
              return Subattribute;
            }
          });
          return attribute;
        } else {
          return attribute;
        }
      }); 
      this.setState({ attributes: newAttributes });
    };

    handleAttributesThirdLevelUrlChange = (idx,sub_idx,thrid_idx) => evt => {
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx === sidx){
          attribute.child_values.map((Subattribute, sidx) => {
            if (sidx === sub_idx){
              const SetsubAttribute = Subattribute.child_values;
              SetsubAttribute[thrid_idx].url = evt.target.value.toLowerCase().replace(/\s+/g, '');              
              return { ...Subattribute, child_values : SetsubAttribute};             
            }else {
              return Subattribute;
            }
          });
          return attribute;
        } else {
          return attribute;
        }
      });
      this.setState({ attributes: newAttributes });
    };
            handleRemoveThirdLevelAttributes = (idx,sub_idx,thrid_idx) => () => {
            const newAttributes = this.state.attributes.map((attribute, sidx) => {
            if (idx === sidx){
            const newattribute =  attribute.child_values.map((Subattribute, sidx) => {
            if (sidx === sub_idx){
            const SetsubAttribute = Subattribute.child_values.filter((s, count) => thrid_idx !== count);
            return {                   // object that we want to update
            ...Subattribute,    // keep all other key-value pairs
            child_values: SetsubAttribute     // update the value of specific key
            };
            }else { 

            return Subattribute;
            }
            });
            return {                   // object that we want to update
            ...attribute,    // keep all other key-value pairs
            child_values: newattribute     // update the value of specific key
            };
            } else {
            return attribute;
            }
            });
            this.setState({ attributes: newAttributes });
            }; 

    containerOnDrop2(id, e) {
      const newItems = {...this.state.attributes};
      const redlabel  = applyDrag(newItems[id].child_values, e);
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (id === sidx){
          const SetsubAttribute = redlabel
          return { ...attribute, child_values : SetsubAttribute};
        } else {
          return attribute;
        }
      });
      this.setState({ attributes: newAttributes });
    }

    containerOnDroplevel3(idx,sub_idx, e) {
      const newItems = {...this.state.attributes};
      const redlabel  = applyDrag(newItems[idx].child_values[sub_idx].child_values, e);
      const newAttributes = this.state.attributes.map((attribute, sidx) => {
        if (idx === sidx){
          attribute.child_values.map((Subattribute, sidx) => {
            if (sidx === sub_idx){
                  const SetsubAttribute = redlabel;
                  return { ...Subattribute, child_values : SetsubAttribute};
            }else {
                  return Subattribute;
            }
          });

           return attribute;
        } else {
           return attribute;
        }
      });
 
     this.setState({ attributes: newAttributes });
    }

    containerOnDrop(e) {
      this.setState({
        attributes: applyDrag(this.state.attributes, e)
      });
    }

    render() {
        return (
          <React.Fragment>
            <div className="container-fluid admin-body">
              <div className="admin-row">
                <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
                </div>
                <div className="col-md-10 col-sm-12 content">
                  <div className="row content-row">
                    <div className="col-md-12 header">
                      <Adminheader  props={this.props} />
                    </div>

                    <div className="col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4" >  
                      <React.Fragment>     
                        <div className="categories-wrap p-4">
                          <div className="row">
                        
                            <div className="col-md-12">
                              <div className="card">
                                <div className="card-header">
                                  <strong>Cimon Menus</strong>
                                </div>       
                                {(()=> {
                                if (this.state.createmenu === false) {
                                return  <div className="form-group menu_container ">
                                  <label>Select a menu to edit:</label>
                                    <div className="create_menu_container">
                                      <select className="form-control"  value={this.state.selected_menu}  onChange={this.handleMenu}>
                                        <option value="-1">Select</option>
                                        {this.state.Allmenus.map((item, val) => (
                                        <option key={item._id} value={item._id}>{item.name}</option>
                                        ))}  
                                      </select>
                                  </div>
                                    {this.state.message !== "" ? (
                                    <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                    </div>
                                    ) : null}
                                  <label><Link onClick={this.createmenu}>Create a new menu</Link></label>
                                </div> ; 
                                  } else {
                                  return <div className="form-group menu_container">
                                  <label>Give your menu a name, then click Create Menu</label>
                                  <div className="create_menu_container">
                                  <input 
                                  className="form-control"
                                  type="text"
                                  value={this.state.menuname}
                                  onChange={this.handleChange('menuname')} 
                                  />
                                  <button  onClick={this.savemenu}>Create Menu</button>
                                  <button onClick={this.cancelmenu}>Cancel</button>
                                </div>
                                    {this.state.errors.menuname ? (<div className="danger">{this.state.errors.menuname}</div>) : ( "")}
                                    {this.state.message !== "" ? (
                                    <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                    </div>
                                    ) : null}
                                    </div>  ;
                                    }
                              })()}   
                                <div className="row">
                                <div className="col-md-12">
                                <div className="card-body  manage-menus">

                                {(()=> {
                                if (this.state.createmenu === false) {
                                return <div className="tab-pane" id="tab_g">
                                <div className="form-group attribute_container" >
                                {(()=> {
                                if(this.state.selected_menu !== "-1") {
                                return    <div className="attribute_container"><h5>Menus</h5>
                                </div>
                                }
                              })()}  
                                            
                              <div className="attributes">
                              <ul className="col-md-12 pb-2 pl-0 pr-0 main_loop_sub">
                              <Container dragHandleSelector=".column-drag-handle"  
                              onDrop={this.containerOnDrop}

                              >
                                  {
                                  this.state.attributes ?
                                  this.state.attributes.map((attribute, idx) => (
                                  <Draggable key={idx}><li  key={idx}>
                                  <span className="column-drag-handle" style={{float:'left', padding:'0 10px'}}>&#x2630;</span>
                                  <Accordion>
                                  <Card>
                                    <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    {attribute.name ? attribute.name : `Menu #${idx + 1}`}
                                    </Accordion.Toggle>
                                    </Card.Header>
                                  <Accordion.Collapse eventKey="0">
                                  <Card.Body> 
                                  <input
                                  type="text"
                                  className="form-control w-100"
                                  placeholder={`Menu #${idx + 1}`}
                                  value={attribute.name}
                                  onChange={this.handleAttributesNameChange(idx)}
                                  />
                                  <input
                                  type="text"
                                  className="form-control w-100"
                                  placeholder={`Url #${idx + 1}`}
                                  value={attribute.url}
                                  onChange={this.handleAttributesUrlChange(idx)}
                                  />
                                  </Card.Body>
                                  </Accordion.Collapse>
                                  </Card>
                                  </Accordion>
                                  <button
                                  className="rem-btn btn btn-danger"
                                  type="button"
                                  onClick={this.handleRemoveAttributes(idx)}
                                  ><span aria-hidden="true">&times;</span></button>
                                  <ul>
                                  <Container   
                                  dragHandleSelector=".column-drag-handle"
                                  onDrop={
                                  e => this.containerOnDrop2(idx, e)
                                  }
                                  >
                                  {
                                  attribute.child_values ?
                                  attribute.child_values.map((sub_menu, sub_idx) => (
                                  <Draggable    className="jinglelele" key={sub_idx}>
                                  <span className="column-drag-handle" style={{float:'left', padding:'0 10px'}}>&#x2630;</span>
                                  <li  key={idx}>
                                  <div key={sub_idx}  className="col-md-12 pb-2 sub_loop_12">
                                  <Accordion>
                                  <Card>
                                  <Card.Header>
                                  <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                  {sub_menu.name ? sub_menu.name : `Menu #${sub_idx + 1}` }
                                  </Accordion.Toggle>
                                  </Card.Header>
                                  <Accordion.Collapse eventKey="0">
                                  <Card.Body>                                   
                                    <input
                                    type="text"
                                    className="form-control w-100"
                                    placeholder={`Menu #${sub_idx + 1}`}
                                    value={sub_menu.name}
                                    onChange={this.handleAttributessubNameChange(idx ,sub_idx)}
                                    />
                                      <input
                                    type="text"
                                    className="form-control w-100"
                                    placeholder={`Url #${sub_idx + 1}`}
                                    value={sub_menu.url}
                                    onChange={this.handleAttributessubUrlChange(idx ,sub_idx)}
                                    />
                                                                                
                                      </Card.Body>
                                    </Accordion.Collapse>
                                    </Card>
                                    </Accordion>


{/* **** level 3 ******************************************************************** */}
                        {sub_menu.child_values.length ? 
                        <ul>
                          <Container  dragHandleSelector=".column-drag-handle"
                            onDrop={
                              e => this.containerOnDroplevel3(idx ,sub_idx, e)
                              }
                          >
                        {                           
                          sub_menu.child_values.map((Thrid_level, thrid_idx) => (
                            <Draggable    className="jinglelele" key={thrid_idx}>                                        
                            <span className="column-drag-handle" style={{float:'left', padding:'0 10px'}}>&#x2630;</span>
                            <li  key={thrid_idx}>
                        <Accordion>
                        <Card>
                            <Card.Header>
                              <Accordion.Toggle as={Button} variant="link" eventKey="0">
                              {Thrid_level.name ? Thrid_level.name : `Menu #${thrid_idx + 1}`}
                              </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="0">
                              <Card.Body> 
                                  <input
                                      type="text"
                                      className="form-control w-100"
                                      placeholder={`Menu #${thrid_idx + 1}`}
                                      value={Thrid_level.name}
                                      onChange={this.handleAttributesThirdlevelNameChange( idx ,sub_idx , thrid_idx)}
                                  />
                                    <input
                                      type="text"
                                      className="form-control w-100"
                                      placeholder={`Url #${thrid_idx + 1}`}
                                      value={Thrid_level.url}
                                    onChange={this.handleAttributesThirdLevelUrlChange( idx ,sub_idx , thrid_idx)}
                                  />
                          </Card.Body>
                            </Accordion.Collapse>
                          </Card>
                        </Accordion>
                            <button
                            className="rem-btn btn btn-danger"
                            type="button"
                            onClick={this.handleRemoveThirdLevelAttributes(idx , sub_idx , thrid_idx)}
                            ><span aria-hidden="true">&times;</span></button>  
                            </li>                                          
                          </Draggable>   
                          ))                                                  
                          
                        }

                        </Container>
                        </ul>

                        : "" }
{/* **  level 3 ********************************************************************* */}
                              <button
                              className="rem-btn btn btn-danger"
                              type="button"
                              onClick={this.handleRemovesubAttributes(idx , sub_idx)}
                              ><span aria-hidden="true">&times;</span></button>
                                    {/* <button
                                      type="button" style={{float:'left', margin:'10px 0 10px 75px'}}
                                      onClick={this.handleaddchilsubAttributes(idx , sub_idx)}
                                    className="add-btn btn btn-success child_croosbtn"
                                    > + </button>  */}
                          </div>
                        </li>
                        </Draggable> 
                      ))                  
                    : ""
                    }
                    </Container>
                    </ul>
                        <button   
                          type="button"
                            onClick={this.handleAddsubAttributes(idx)}
                          className="add-btn btn btn-success"
                          > + </button>                             
                      </li>
              </Draggable>        
                    ))
                    
                    : ""
                    
                    }
                    </Container>  
                    </ul>

                      {(()=> {

                      if(this.state.selected_menu !== "-1") {
                      return    <div className="attribute_container"> 
                      <button
                      type="button"
                      onClick={this.handleAddAttributes}
                      className="add-btn btn btn-success"
                      >Add New</button></div>
                      }

                      })()}  
                      </div>
                      { this.state.attributes.length ? <button className="btn_save_option_menu" onClick={this.updateMenu}>Save</button> : ""} 
                      { this.state.selected_menu !=='-1'  ? <button className="btn_save_option_menu_danger" onClick={this.deleteMenu}>Delete</button> : ""}   
                      
                    </div>

                        {this.state.message_save_or_delete !== "" ? (
                            <div style={{ margin: '0 0 0 10px'}} className={this.state.message_save_or_delete_type}>
                            <p>{this.state.message_save_or_delete}</p>
                            </div>
                        ) : null}

                        </div>;

                      }
                  })()}       
                  </div>
                  </div>
                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        </React.Fragment>     
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </React.Fragment>
        );
      }
    }
    export default ManageMenus;
    