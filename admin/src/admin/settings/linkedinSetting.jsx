import React, { Component } from 'react';
import LinkedIn from "linkedin-login-for-react";  
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import Joi, { join } from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import ReactSpinner from "react-bootstrap-spinner";
const axios = require("axios").default;
class LinkedinSetting extends React.Component {
  static propTypes = {};
  state = { 
    data: {
    live : false, 
    access_token: "" , expires_in: "" ,client_id : "" ,client_secret : ""},
    errors: {},
    submit_status: false,
    message : "",
    message_type: "",
    spinner : true,
  };
  schema = {
    client_id: Joi.string().allow(null),
    client_secret: Joi.string().allow(null)
};
  componentDidMount = async () => {
    const id = this.props.match.params.id;
    this.getLinkedInData(); 
};
getLinkedInData = async () => {
    const response =  await settingsService.getLinkedInData();
    if (response.data.status == 1) {
      if(response.data.data){
          if(response.data.data.value!=""&&response.data.data.value!=null){
          console.log(response.data.data.value);
          this.setState({ 
            data: response.data.data.value,
          });
        }
          this.setState({ spinner: false });
      }      
    } 
  }
   /*Input Handle Change */
   handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });
    
};

/* Joi validation call*/
validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;

};
  handleSubmit = async () => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    let result = Joi.validate(data, this.schema);      
    try {
        this.setState({ spinner: true });
        this.setState({ submit_status : true });
        const data = {value : this.state.data}
        const response = await settingsService.updateLinkedinCred(data); 
        if (response.data.status === 1) {  
            this.setState({
                message: response.data.message,
                message_type: "success",
            });
            this.setState({ spinner: false });
            this.setState({ submit_status : false });
        } else {
            this.setState({
                message: response.data.message,
                message_type: "error",
            });
            this.setState({ spinner: false });
            this.setState({ submit_status : false });
        }
    } catch (err) {
        this.setState({ spinner: false });
        this.setState({ submit_status : false });
    }
 };
  /*
    ** @code = authorization code from linkedin api
    ** @redirectUri = redirect uri from linkedin api
    ** @error = error message sign in failed
    */
  callbackLinkedIn = async (error, code, redirectUri) => {
    if (error) {
      // signin failed
    } else { 
        const data = {code : code,
            client_id:this.state.data.client_id,
            client_secret:this.state.data.client_secret,
            redirect_uri: window.location.origin+'/cimon-gate/admin/linkedinsettings' }
        const response = await settingsService.getLinkedIn(data); 
        const res=response.data.data;
        var token = JSON.parse(res);
        console.log(token);

        // fetch('https://www.linkedin.com/oauth/v2/accessToken', requestOptions)
        //     .then(response => response.json())
        //     .then(data => console.log(data));
            
      // Obtain authorization token from linkedin api
      // see https://developer.linkedin.com/docs/oauth2 for more info
    }
  };

  render() {
    return (
        <React.Fragment> 
        <div className="container-fluid admin-body ">
            <div className="admin-row">
                <div className="col-md-2 col-sm-12 sidebar">
                    <Adminsidebar props={this.props} />
                </div>
                <div className="col-md-10 col-sm-12  content">
                    <div className="row content-row">
                        <div className="col-md-12  header">
                            <Adminheader  props={this.props} />
                        </div>
                        <div className="col-md-12  contents  useradd-new pt-4 pb-4 pr-4" >   
                        <React.Fragment>
                        <div className="card">
                            <div className="card-header">LinkedIn Details</div>
                            <div className="card-body">
                            <div className="form-group">
                            <label>Client ID</label>
                            <input name="client_id"  onChange={this.handleChange} type="text" 
                            value={this.state.data.client_id}
                            placeholder="Client ID" className="form-control"/>
                            {this.state.errors.client_id ? (<div className="error text-danger">
                            {this.state.errors.client_id}
                            </div>
                            ) : (
                            ""
                            )}
                        </div>
                        <div className="form-group">
                            <label>Client Secret</label>
                            <input name="client_secret"  onChange={this.handleChange} type="text" placeholder="App Secret" value={this.state.data.client_secret} className="form-control"/>
                            {this.state.errors.client_secret ? (<div className="error text-danger">
                            {this.state.errors.client_secret}
                            </div>
                            ) : (
                            ""
                            )}
                        </div>
                        <div className="submit">
                    <button
                    disabled={this.state.submit_status === true ? "disabled" : false}                               
                    className="submit-btn" onClick={this.handleSubmit} >Save</button>
                </div>
                
                            {
                            this.state.data.access_token !== "" ? (
                                <p className="tableinformation">
                                    <div>
                                    <p>Linkedin connected</p>
                                    </div>
                                   
                                </p>
                                
                            ) : null
                            } 
    <div class="submit in" style={{textAlign: "center"}} >
        { this.state.data.client_secret!="" && this.state.data.client_id!="" ?
        
        <LinkedIn
        clientId={this.state.data.client_id}
        callback={this.callbackLinkedIn} 
        className="submit-btns las"
        scope={["r_emailaddress","r_organization_social"]}
        text="Connect With LinkedIn"
         /> : null
       
      }
        </div>
       </div>
      </div> 
  </React.Fragment>
</div>
</div>
</div>
</div>
</div>
</React.Fragment> );
}
}
export default LinkedinSetting;