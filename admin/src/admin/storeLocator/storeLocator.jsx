import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import * as storeLocation from "../../ApiServices/admin/storeLocation";
import DeleteConfirm from "../common/deleteConfirm";
import ReactSpinner from "react-bootstrap-spinner";
import memoize from "memoize-one";
import { CSVLink, CSVDownload } from "react-csv";
import TrashConfirm from "../common/trashConfirm";
class AdminProducts extends Component {
  state = {
    search: "",
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    spinner: true,
    toggleCleared: false,
    contextActions_select: "-1",
    message: "",
    message_type: "",
    activetab: "tab_a",
    columns: [
      {
        name: "title",
        selector: "title",
        hide: "sm",
        cell: row => (
          <div>
            <Link to={"/admin/storelocator/edit/" + row._id}>{row.title}</Link>
          </div>
        )
      },
      {
        name: "Categories",
        selector: "store_category",
        sortable: true
      },

      {
        name: "city",
        selector: "city",
        sortable: true,
        left: true,
        hide: "sm"
      },

      {
        name: "state",
        selector: "state",
        sortable: true,
        left: true,
        hide: "sm"
      },

      {
        name: "Zipcode",
        selector: "zip",
        sortable: true,
        width: "150px"
      },

      {
        name: "",
        cell: row => (
          <div className="action_dropdown" data-id={row.id}>
            <i
              data-id={row.id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            />
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >
              <Link
                to={"/admin/storelocator/edit/" + row._id}
                className="dropdown-item"
              >
                Edit Page
              </Link>
              <Link
                onClick={() => this.saveAndtoogle(row._id)}
                className="dropdown-item"
              >
                Delete
              </Link>
              {row.page_status != 0 ? (
                <Link
                  onClick={() => this.saveAndtoogletrash(row._id, "trash")}
                  className="dropdown-item"
                >
                  Trash
                </Link>
              ) : (
                ""
              )}
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px" // custom width for icon button
      }
    ]
  };

  downloadCSV = array => {
    const link = document.createElement("a");
    let csv = this.convertArrayOfObjectsToCSV(array);
    if (csv == null) return;
    const filename = "Product.csv";
    if (!csv.match(/^data:text\/csv/i)) {
      csv = `data:text/csv;charset=utf-8,${csv}`;
    }
    link.setAttribute("href", encodeURI(csv));
    link.setAttribute("download", filename);
    link.click();
  };

  removecomma(Items, key) {
    if (key == "title") {
      return Items.replace(",", "");
    } else {
      return Items;
    }
  }

  convertArrayOfObjectsToCSV = array => {
    let result;
    const columnDelimiter = ",";
    const lineDelimiter = "\n";
    const keys = Object.keys(array[0]);
    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;
    array.forEach(item => {
      let ctr = 0;
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter;

        result += this.removecomma(item[key], key);

        ctr++;
      });
      result += lineDelimiter;
    });
    return result;
  };

  searchSpace = event => {
    let keyword = event.target.value;
    this.setState({ search: keyword });
  };

  formatDate(string) {
    var options = { year: "numeric", month: "numeric", day: "numeric" };
    return new Date(string).toLocaleDateString("en-GB", options);
  }

  /*Get all Stores */
  getAllStore = async () => {
    this.setState({ spinner: true });
    try {
      const response = await storeLocation.getAllStoreLocation();
      if (response.data.status == 1) {
        const data_table_row = [];
        const data_table_rowTrash = [];
        const data_table_rowDraft = [];
        response.data.data.map((store, index) => {
          if (store.page_status == 0) {
            const SetdataTrash = {};
            SetdataTrash.title = store.title;
            SetdataTrash.store_category = store.store_category;
            SetdataTrash._id = store._id;
            SetdataTrash.page_status = store.page_status;
            SetdataTrash.city = store.city;
            SetdataTrash.state = store.state;
            SetdataTrash.zip = store.zip;
            data_table_rowTrash.push(SetdataTrash);
          } else if (store.page_status == 1) {
            const Setdata = {};
            Setdata.title = store.title;
            Setdata.store_category = store.store_category;
            Setdata._id = store._id;
            Setdata.page_status = store.page_status;
            Setdata.city = store.city;
            Setdata.state = store.state;
            Setdata.zip = store.zip;
            data_table_row.push(Setdata);
          } else if (store.page_status == 2) {
            const SetdataDraft = {};
            SetdataDraft.title = store.title;
            SetdataDraft.store_category = store.store_category;
            SetdataDraft._id = store._id;
            SetdataDraft.page_status = store.page_status;
            SetdataDraft.city = store.city;
            SetdataDraft.state = store.state;
            SetdataDraft.zip = store.zip;
            data_table_rowDraft.push(SetdataDraft);
          }
        });
        this.setState({
          spinner: false,
          data_table: data_table_row,
          dataTrash: data_table_rowTrash,
          dataDraft: data_table_rowDraft
        });
      } else {
        this.setState({ spinner: false });
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };
  componentDidMount = () => {
    this.getAllStore();
  };

  /*delete store */
  deleteStore = async () => {
    let id = this.state.index;
    const response = await storeLocation.deleteStore(id);
    if (response) {
      if (response.data.status == 1) {
        const data_table = this.state.data_table.filter(i => i._id !== id);
        this.setState({ data_table });
        this.toogle();
        this.getAllStore();
      }
    }
  };

  /*delete store */
  trashStore = async () => {
    let id = this.state.index;
    const response = await storeLocation.trashStore(id);
    if (response) {
      if (response.data.status == 1) {
        const data_table = this.state.data_table.filter(i => i._id !== id);
        this.setState({ data_table });
        this.toogleTrash();
        this.getAllStore();
      }
    }
  };

  saveAndtoogle = id => {
    this.setState({ index: id });
    this.toogle();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };
  saveAndtoogletrash = id => {
    this.setState({ index: id });
    this.toogleTrash();
  };
  toogleTrash = () => {
    let status = !this.state.modal_trash;
    this.setState({ modal_trash: status });
  };
  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows });
  };

  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value });
  };

  actionHandler = e => {
    this.setState({
      message: "",
      message_type: ""
    });
    const { selectedRows } = this.state;
    const newsids = selectedRows.map(r => r._id);
    let status = "";
    let actionvalue = this.state.contextActions_select;
    if (actionvalue === "-1") {
      return;
    } else if (actionvalue === "0") {
      status = "Trash";
    } else if (actionvalue === "1") {
      status = "Published";
    } else if (actionvalue === "2") {
      status = "Draft";
    } else if (actionvalue === "3") {
      status = "Delete trash";
    }
    if (window.confirm(`Are you sure you want to move ` + status)) {
      this.setState({ toggleCleared: !this.state.toggleCleared });
      this.actionHandlerstore(newsids, actionvalue, status);
    }
  };

  actionHandlerstore = async (newsids, contextActions, status) => {
    try {
      const response = await storeLocation.actionHandlerstore(
        newsids,
        contextActions,
        status
      );
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success"
        });

        this.getAllStore();
      } else {
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
        this.getAllStore();
      }
    } catch (err) {
      this.setState({
        message: "Something went wrong",
        message_type: "error"
      });
    }
  };

  tabActive = async tab => {
    this.setState({ activetab: tab });
  };
  // actionHandlerstorelocator = async (newsids, contextActions, status) => {
  //   try {
  //     const response = await settingsService.actionHandlerstorelocator(
  //       newsids,
  //       contextActions,
  //       status
  //     );
  //     if (response.data.status == 1) {
  //       this.setState({
  //         message: response.data.message,
  //         message_type: "success"
  //       });

  //       this.getAllCareers();
  //     } else {
  //       this.setState({
  //         message: response.data.message,
  //         message_type: "error"
  //       });
  //       this.getAllCareers();
  //     }
  //   } catch (err) {
  //     this.setState({
  //       message: "Something went wrong",
  //       message_type: "error"
  //     });
  //   }
  // };
  render() {
    let rowData = this.state.data_table;
    let search = this.state.search;
    let rowDataTrash = this.state.dataTrash;
    let rowDataDraft = this.state.dataDraft;
    const contextActions = memoize(actionHandler => (
      <div className="common_action_section">
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value="-1">Select</option>
          {this.state.activetab !== "tab_a" ? (
            <option value="1">Published</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_b" ? (
            <option value="0">Trash</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_c" ? (
            <option value="2">Draft</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_a" &&
          this.state.activetab !== "tab_c" ? (
            <option value="3">Delete Trash</option>
          ) : (
            ""
          )}
        </select>
        <button className="danger" onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ));

    if (search.length > 0) {
      search = search.trim().toLowerCase();
      rowData = rowData.filter(l => {
        return (
          l.title.toLowerCase().match(search) ||
          l.store_category.toLowerCase().match(search) ||
          l.zip.toLowerCase().match(search)
        );
      });
    }

    return (
      <React.Fragment>
        <div className="container-fluid admin-body ">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents products home-inner-content pt-4 pb-4 pr-4">
                  <div className="main_admin_page_common product-summary-tab-wrapper">
                    <div className="">
                      <div className="admin_breadcum">
                        <div className="row">
                          <div className="col-md-2">
                            <p className="page-title">Store Locator</p>
                          </div>
                          <div className="col-md-5">
                            <Link
                              className="add_new_btn"
                              to="/admin/storelocator/add"
                            >
                              Add New
                            </Link>
                          </div>
                          <div className="col-md-5">
                            <div className="searchbox">
                              <div className="commonserachform">
                                <span />
                                <input
                                  type="text"
                                  placeholder="Search"
                                  onChange={e => this.searchSpace(e)}
                                  name="search"
                                  className="search form-control"
                                />
                                <input type="submit" className="submit_form" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <React.Fragment />
                      {this.state.message !== "" ? (
                        <p className="tableinformation">
                          <div className={this.state.message_type}>
                            <p>{this.state.message}</p>
                          </div>
                        </p>
                      ) : null}
                      <div
                        style={{
                          display:
                            this.state.spinner === true
                              ? "flex justify-content-center "
                              : "none"
                        }}
                        className="overlay text-center"
                      >
                        <ReactSpinner type="border" color="primary" size="10" />
                      </div>
                      <div className="col-xl-12 col-md-12 col-sm-12 p-0">
                        <ul className="nav nav-pills nav-fill">
                          <li
                            className={
                              this.state.activetab === "tab_a" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_a"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_a")}
                            >
                              All ({this.state.data_table.length})
                            </a>
                          </li>
                          <li
                            className={
                              this.state.activetab === "tab_b" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_b"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_b")}
                            >
                              Trash ({this.state.dataTrash.length})
                            </a>
                          </li>

                          <li
                            className={
                              this.state.activetab === "tab_c" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_c"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_c")}
                            >
                              Draft ({this.state.dataDraft.length})
                            </a>
                          </li>
                        </ul>
                        <div className="tab-content">
                          <div className="tab-pane active" id="tab_a">
                            <DataTable
                              columns={this.state.columns}
                              data={rowData}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                          <div className="tab-pane" id="tab_b">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataTrash}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                          <div className="tab-pane" id="tab_c">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataDraft} 
                              selectableRows
                              highlightOnHover
                              pagination  
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.deleteStore}
        />
        <TrashConfirm
          modal={this.state.modal_trash}
          toogle={this.toogleTrash}
          trash_action={this.trashStore}
        />
      </React.Fragment>
    );
  }
}

export default AdminProducts;
