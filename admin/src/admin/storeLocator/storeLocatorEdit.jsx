import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import ReactSpinner from 'react-bootstrap-spinner'
import validate from 'react-joi-validation'
import Joi, { join } from 'joi-browser'
import Uploadprofile from '../common/uploadProfileimage'
import { Link, NavLink } from 'react-router-dom'
import CKEditor from 'ckeditor4-react'
import { apiUrl, siteUrl } from '../../../src/config.json'
import * as storeLocation from '../../ApiServices/admin/storeLocation'
import * as manageProducts from "../../ApiServices/admin/products";
import BlockUi from 'react-block-ui';
class StoreLocatorEdit extends Component {
  constructor (props) {
    super(props)
    this.onEditorChange2 = this.onEditorChange2.bind(this)
  }
  state = {
    spinner: false,
    activetab: 'tab_a',
    tabData: [
      { name: 'Tab 1', isActive: true },
      { name: 'Tab 2', isActive: false },
      { name: 'Tab 3', isActive: false }
    ],
    errors: {},
    submit_status: false,
    message: '',
    message_type: '',
    cimonProducts: [
      {
        description: ''
      }
    ],
    data: [],
    store_category: 'Distributor',
    featured_array: [{ image_name: '', image_url: '' }],
    upload_status: false,
    marker_image:"",
  }

  onEditorChange2 (evt) {
    const data = { ...this.state.data }
    data['description'] = evt.editor.getData()
    this.setState({
      data: data
    })
  }

  tabActive = async tab => {
    this.setState({ activetab: tab })
  }

/* Joi validation schema */
  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    address: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    address2: Joi.string()
      .allow(null)
      .allow(''),
    city: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    state: Joi.string()
      .allow(null)
      .allow(''),
    zip: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    latitude: Joi.string()
      .allow(null)
      .allow(''),
    longitude: Joi.string()
      .allow(null)
      .allow(''),
    country: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    featured_image: Joi.string()
      .allow(null)
      .allow(''),
    featured_name: Joi.string()
      .allow(null)
      .allow(''),
    phone: Joi.string()
      .allow(null)
      .allow(''),
    fax: Joi.string()
      .allow(null)
      .allow(''),
    email: Joi.string()
      .email()
      .allow(null)
      .allow(''),
    description: Joi.allow(null).allow(''),
    service_areas: Joi.allow(null).allow(''),
    site_url: Joi.string()
    .allow(null)
    .allow(''),
    _id: Joi.string(),
    logo_img: Joi.string()
      .allow(null)
      .allow(''),
    store_category: Joi.string(),
    marker_image: Joi.string().allow(''),
  }
/* upload single file upload */      
uploadSingleFiles  = async (e) =>{
  this.setState({ upload_status: true });
  const response1 = await manageProducts.uploadProductImages(
    e.target.files
  ); 
  console.log(222,response1);
  if (response1.data.status == 1) {
    response1.data.data.file_location.map((item, key) => {
      this.setState({ productfileArray: item.Location});
      let data = { ...this.state.data };
      data['marker_image'] =  item.Location;
      this.setState({ data: data});      
    });    
    this.setState({ upload_status: false });
     } else {
    this.setState({
      submitStatus: false,
      message: response1.data.message,
      responsetype: "error",
    });
    this.setState({ upload_status: false });
  }
}

/*Input Handle Change */
  handleChange = (event, type = null) => {
    let data = { ...this.state.data }
    const errors = { ...this.state.errors }
    this.setState({ message: '' })
    delete errors.validate
    let name = event.target.name // input field  name
    let value = event.target.value // input field value
    if (name == 'featured_name') {
      let filepath = { image_name: value, image_url: data['featured_image'] }
      this.setState({ featured_array: filepath })
      
    }
    const errorMessage = this.validateProperty(name, value)
    if (errorMessage) errors[name] = errorMessage
    else delete errors[name]
    data[name] = value
    this.setState({ data, errors })
  }

  validateProperty = (name, value) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)
    return error ? error.details[0].message : null
  }

  /*upload profile image  */
  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors }
    let upload_data = { ...this.state.upload_data }
    if (item === 'errors') {
      errors['featured_image'] = value
    } else {
      let file = value.target.files[0]
      upload_data = file
      this.setState({ upload_data: upload_data })
    }
  }
/*get store details */
  getStoreDetailsbyId = async id => {
    try {
      this.setState({ spinner: true })
      const response = await storeLocation.getStoreDetailsbyId(id)
        console.log(2424,response);
      if (response.data.status == 1) {
        this.setState({ spinner: false, data: response.data.data })
        let filepath = { image_url: response.data.data.logo_img }
        let image_url = response.data.data.logo_img
          ? response.data.data.logo_img
          : ''
          let marker_image = response.data.data.marker_image
          ? response.data.data.marker_image
          : ''
        let store_category = response.data.data.store_category
        const data = { ...this.state.data }
        data['featured_image'] = image_url
        data["marker_image"] = marker_image;
        this.setState({ data: data })       
        this.setState({ spinner: false })
      } else {
        this.setState({ spinner: false })
      }
    } catch (err) {
      this.setState({ spinner: false })
    }
  }

  componentDidMount = async () => {
    const id = this.props.match.params.id
    this.getStoreDetailsbyId(id)
  }

/* Form Submit */
  handleSubmit = async () => {
    const errors = { ...this.state.errors }
    this.setState({
      message: '',
      message_type: ''
    })
    const data = {
      address: this.state.data.address,
      city: this.state.data.city,
      country: this.state.data.country,
      email: this.state.data.email,
      fax: this.state.data.fax,
      latitude: this.state.data.latitude,
      logo_img: this.state.data.logo_img,
      longitude: this.state.data.longitude,
      phone: this.state.data.phone,
      service_areas: this.state.data.service_areas,
      site_url: this.state.data.site_url,
      state: this.state.data.state,
      store_category: this.state.store_category,
      title: this.state.data.title,
      zip: this.state.data.zip,
      marker_image :this.state.data.marker_image
    }
    let result = Joi.validate(data, this.schema)
    console.log('consolelog',result);
    if (result.error) {
      let path = result.error.details[0].path[0]
      let errormessage = result.error.details[0].message
      errors[path] = errormessage
      this.setState({
        errors: errors
      })
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        })
      } else {
        this.setState({ submit_status: true })
        this.setState({ spinner: true })
        if (this.state.upload_data) {
          // check for featured images
          const response1 = await storeLocation.uploadFeaturedImage(
            this.state.upload_data
          )
          if (response1.data.status == 1) {
            let filepath = {
              image_name: this.state.data.featured_name,
              image_url: response1.data.data.file_location
            }
            this.setState({ featured_array: filepath })
            this.setState({ upload_data: '' })
            this.updateStoreData()
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: 'error'
            })
            this.setState({ spinner: false })
          }
        } else {
          
          this.updateStoreData()
        }
      }
    }
  }

  /*update store locator data */
  updateStoreData = async () => {
    this.setState({ spinner: true })
    const datanew = { ...this.state.data }
    const data = {
      address: this.state.data.address,
      address2: this.state.data.address2,
      city: this.state.data.city,
      country: this.state.data.country,
      email: this.state.data.email,
      fax: this.state.data.fax,
      latitude: this.state.data.latitude,
      logo_img: this.state.featured_array.image_url
        ? this.state.featured_array.image_url
        : this.state.data.logo_img,
      longitude: this.state.data.longitude,
      phone: this.state.data.phone,
      service_areas: this.state.data.service_areas,
      site_url: this.state.data.site_url,
      state: this.state.data.state,
      store_category: this.state.store_category,
      title: this.state.data.title,
      zip: this.state.data.zip,
      _id: this.state.data._id,
      marker_image :this.state.data.marker_image
    }
    const response = await storeLocation.updateStoreLocator(data)
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false })
        this.setState({
          message: response.data.message,
          message_type: 'success',
          data: []
        })
        this.setState({ spinner: false })
        this.setState({ submit_status: false })
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false })
        this.setState({ submit_status: false })
        this.setState({
          message: response.data.message,
          message_type: 'error'
        })
      }
      this.setState({ data: datanew })
      this.setState({ spinner: false })
    } else {
      this.setState({ spinner: false })
    }
  }

  handlestore_category = async e => {
    this.setState({ store_category: e.target.value })
  }

  render () {
    const images = require.context('../../assets/images/admin', true)    
    let checkedCond = this.state.checked
    return (
      <React.Fragment>
        <div className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>

            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12 product-add-page  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4'>
                  <div className='addpage-form'>
                  <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className='row addpage-form-wrap'>
                      <div className='col-lg-9 col-md-12'>
                        <div className='form-group'>
                          <label htmlFor=''>Store Name</label>
                          <input
                            name='title'
                            value={this.state.data.title}
                            onChange={e => this.handleChange(e)}
                            type='text'
                            placeholder='Add title *'
                            className='form-control'
                          />
                          {this.state.errors.title ? (
                            <div className='error text-danger'>
                              {this.state.errors.title}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>

                        <div className='form-group'>
                          <label htmlFor=''>Store Categories</label>
                          <select
                            className='form-control'
                            value={this.state.store_category}
                            onChange={this.handlestore_category}
                          >
                            <option value='Distributor'>Distributor</option>
                            <option value='SystemIntegrator'>
                              System Integrator
                            </option>
                          </select>
                          {this.state.errors.store_category ? (
                            <div className='error text-danger'>
                              {this.state.errors.store_category}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>
                        <div className="form-group">
                                    <label>Marker Icon</label>
                                    <input type="file" className="form-control"  onChange={this.uploadSingleFiles}  />
                                    {this.state.upload_status === true ? (
                                      <ReactSpinner type="border" color="dark" size="1" />
                                    ) : (
                                     ""
                                    )} 
                                
                              
                                <a href={this.state.data.marker_image?this.state.data.marker_image:""}>{this.state.data.marker_image?this.state.data.marker_image:""}</a>  
                                  </div> 
                        <div className='col-md-12'>
                          <div className='product-attribute-tab row bg-white'>
                            <div className='col-xl-3 col-md-3 col-sm-3'>
                              <ul className='nav nav-pills nav-fill'>
                                <li
                                  className={
                                    this.state.activetab === 'tab_a'
                                      ? 'active'
                                      : ''
                                  }
                                >
                                  <a
                                    href='#tab_a'
                                    data-toggle='pill'
                                    onClick={() => this.tabActive('tab_a')}
                                  >
                                    Location
                                  </a>
                                </li>
                                <li
                                  className={
                                    this.state.activetab === 'tab_b'
                                      ? 'active'
                                      : ''
                                  }
                                >
                                  <a
                                    href='#tab_b'
                                    data-toggle='pill'
                                    onClick={() => this.tabActive('tab_b')}
                                  >
                                    Additional Information
                                  </a>
                                </li>
                              </ul>
                            </div>

                            <div className='col-xl-9 col-md-9 col-sm-9 '>
                              <div className='tab-content'>
                                <div className='tab-pane active' id='tab_a'>
                                  <div className='tab-icon' />
                                  <div className='edit-details-head pb-4'>
                                    <div className='form-group-product'>
                                      <label>Address: *</label>
                                      <input
                                        type='text'
                                        name='address'
                                        className='form-control '
                                        value={this.state.data.address}
                                        onChange={e => this.handleChange(e)}
                                      />
                                      {this.state.errors.address !== '' ? (
                                        <div className='validate_error'>
                                          <p>{this.state.errors.address}</p>
                                        </div>
                                      ) : null}
                                    </div>

                                    <div className='form-group-product'>
                                      <label>Address 2:</label>
                                      <input
                                        type='text'
                                        name='address2'
                                        className='form-control '
                                        value={this.state.data.address2}
                                        value={this.state.data.address2}
                                        onChange={e => this.handleChange(e)}
                                      />
                                      {this.state.errors.address2 !== '' ? (
                                        <div className='validate_error'>
                                          <p>{this.state.errors.address2}</p>
                                        </div>
                                      ) : null}
                                    </div>

                                    <div className='form-group-product'>
                                      <label>City: *</label>
                                      <input
                                        type='text'
                                        name='city'
                                        className='form-control '
                                        value={this.state.data.city}
                                        onChange={e => this.handleChange(e)}
                                      />
                                      {this.state.errors.city !== '' ? (
                                        <div className='validate_error'>
                                          <p>{this.state.errors.city}</p>
                                        </div>
                                      ) : null}
                                    </div>

                                    <div className='form-group-product'>
                                      <label>State:</label>
                                      <input
                                        type='text'
                                        name='state'
                                        className='form-control '
                                        value={this.state.data.state}
                                        onChange={e => this.handleChange(e)}
                                      />
                                      {this.state.errors.state !== '' ? (
                                        <div className='validate_error'>
                                          <p>{this.state.errors.state}</p>
                                        </div>
                                      ) : null}
                                    </div>

                                    <div className='form-group-product'>
                                      <label>Zip Code: *</label>
                                      <input
                                        type='text'
                                        name='zipcode'
                                        className='form-control '
                                        value={this.state.data.zip}
                                        onChange={e => this.handleChange(e)}
                                      />
                                      {this.state.errors.zipcode !== '' ? (
                                        <div className='validate_error'>
                                          <p>{this.state.errors.zipcode}</p>
                                        </div>
                                      ) : null}
                                    </div>

                                    <div className='form-group-product'>
                                      <label>Country: *</label>
                                      <input
                                        type='text'
                                        name='country'
                                        className='form-control '
                                        value={this.state.data.country}
                                        onChange={e => this.handleChange(e)}
                                      />
                                      {this.state.errors.country !== '' ? (
                                        <div className='validate_error'>
                                          <p>{this.state.errors.country}</p>
                                        </div>
                                      ) : null}
                                    </div>

                                    <div className='form-group-product'>
                                      <label>Latitude:</label>
                                      <input
                                        type='text'
                                        name='latitude'
                                        className='form-control '
                                        value={this.state.data.latitude}
                                        onChange={e => this.handleChange(e)}
                                      />
                                      {this.state.errors.latitude !== '' ? (
                                        <div className='validate_error'>
                                          <p>{this.state.errors.latitude}</p>
                                        </div>
                                      ) : null}
                                    </div>

                                    <div className='form-group-product'>
                                      <label>Longitude:</label>
                                      <input
                                        type='text'
                                        name='longitude'
                                        className='form-control '
                                        value={this.state.data.longitude}
                                        onChange={e => this.handleChange(e)}
                                      />
                                      {this.state.errors.longitude !== '' ? (
                                        <div className='validate_error'>
                                          <p>{this.state.errors.longitude}</p>
                                        </div>
                                      ) : null}
                                    </div>
                                  </div>
                                </div>
                                <div className='tab-pane' id='tab_b'>
                                  <div className='form-group-product'>
                                    <label>Tel:</label>
                                    <input
                                      type='text'
                                      name='phone'
                                      className='form-control '
                                      value={this.state.data.phone}
                                      onChange={e => this.handleChange(e)}
                                    />
                                    {this.state.errors.phone !== '' ? (
                                      <div className='validate_error'>
                                        <p>{this.state.errors.phone}</p>
                                      </div>
                                    ) : null}
                                  </div>

                                  <div className='form-group-product'>
                                    <label>Fax:</label>
                                    <input
                                      type='text'
                                      name='fax'
                                      className='form-control '
                                      value={this.state.data.fax}
                                      onChange={e => this.handleChange(e)}
                                    />
                                    {this.state.errors.fax !== '' ? (
                                      <div className='validate_error'>
                                        <p>{this.state.errors.fax}</p>
                                      </div>
                                    ) : null}
                                  </div>

                                  <div className='form-group-product'>
                                    <label>Email:</label>
                                    <input
                                      type='text'
                                      name='email'
                                      className='form-control '
                                      value={this.state.data.email}
                                      onChange={e => this.handleChange(e)}
                                    />
                                    {this.state.errors.email !== '' ? (
                                      <div className='validate_error'>
                                        <p>{this.state.errors.email}</p>
                                      </div>
                                    ) : null}
                                  </div>

                                  <div className='form-group-product'>
                                    <label>Url: </label>
                                    <input
                                      type='text'
                                      name='site_url'
                                      className='form-control '
                                      value={this.state.data.site_url}
                                      onChange={e => this.handleChange(e)}
                                    />
                                   
                                  </div>

                                  <div className='form-group-product'>
                                    <label>Service Areas: </label>
                                    <input
                                      type='text'
                                      name='service_areas'
                                      className='form-control '
                                      value={this.state.data.service_areas}
                                      onChange={e => this.handleChange(e)}
                                    />
                                    {this.state.errors.service_areas !== '' ? (
                                      <div className='validate_error'>
                                        <p>{this.state.errors.service_areas}</p>
                                      </div>
                                    ) : null}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                      <div className='col-lg-3 col-md-12 '>
                        <div className='form-group thumbanail_container'>
                          <label>Featured Image</label>
                          <Uploadprofile
                            onuplaodProfile={this.onuplaodProfile}
                            value={this.state.data.featured_image}
                            errors={this.state.errors}
                          />
                        </div>

                        <div className='faq-sideinputs'>
                          <div className='faq-btns form-btn-wrap'>
                            <div className='float-left' />
                            <div className='update_btn input-group-btn float-right'>
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? 'disabled'
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className='btn btn-info'
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type='border'
                                    color='dark'
                                    size='1'
                                  />
                                ) : (
                                  ''
                                )}
                                Update
                              </button>
                            </div>
                          </div>
                          {this.state.message !== '' ? (
                          <p className='tableinformation'>
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          </p>
                        ) : null}

                        </div>
                      </div>
                    </div>
                  </BlockUi>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default StoreLocatorEdit
