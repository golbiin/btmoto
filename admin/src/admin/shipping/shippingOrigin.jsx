import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as shippingService from "../../ApiServices/admin/shippingSettings";
import ShippingMethod from "./shippingMethod";


import {
  CountryDropdown,
  RegionDropdown,
  CountryRegionData,
} from "react-country-region-selector";

class ShippingOrigin extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    data: {
      country: "",
      region: "",
      zipcode: "",
      city: "",
      street_address: "",
      street_address1: "",
      slug: "",
    },
    errors: {},
    shippingDetails: {},
    submit_status: false,
    message: "",
    message_type: "",
    table_message: "",
    table_message_type: "",
    edit_status: false,
    data_table: [],
    targetCountry: ["US"],
    columns: [
      {
        name: "Country",
        // selector: 'category_name',
        sortable: false,
        cell: (row) => <p>{row.country} </p>,
      },
      {
        name: "Region",
        // selector: 'category_name',
        sortable: false,
        cell: (row) => <p>{row.region} </p>,
      },

      {
        name: "City",
        // selector: 'category_name',
        sortable: false,
        cell: (row) => <p>{row.city} </p>,
      },

      {
        name: "ZipCode",
        // selector: 'category_name',
        sortable: false,
        cell: (row) => <p>{row.zipcode} </p>,
      },
      {
        name: "StreetAddress",
        // selector: 'category_name',
        sortable: false,
        cell: (row) => <p>{row.street_address} </p>,
      },
    ],
  };

  componentDidMount = () => {
    this.getShippingData();
  };
  getShippingData = async () => {
    this.setState({ spinner: true });

    try {
      const response = await shippingService.getShippingData();
      if (response.data.status == 1) {
        this.setState({ data: response.data.shippingDetails });

        const data_table_row = [];

        let Setdata = {};

        Setdata.country = response.data.shippingDetails.country;
        Setdata.region = response.data.shippingDetails.region;
        Setdata.city = response.data.shippingDetails.city;
        Setdata.zipcode = response.data.shippingDetails.zipcode;
        Setdata.street_address = response.data.shippingDetails.street_address;
        Setdata.street_address1 = response.data.shippingDetails.street_address1;
        data_table_row.push(Setdata);

        this.setState({
          data_table: data_table_row,
        });
      }

      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /*****************Joi validation schema**************/

  // country: Joi.optional().label("country"),
  schema = {
    country: Joi.optional().label("country"),
    region: Joi.optional().label("region"),
    slug: Joi.optional().label("slug"),
    _id: Joi.optional().label("id"),

    zipcode: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
    city: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),

    street_address: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
    street_address1: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
  };

  /*****************INPUT HANDLE CHANGE **************/

  selectCountry = async (val) => {
    let data = { ...this.state.data };
    let errors = { ...this.state.errors };
    data["country"] = val;
    this.setState({ data, errors });
    delete errors.country;
  };

  selectRegion = async (val) => {
    let data = { ...this.state.data };
    let errors = { ...this.state.errors };
    data["region"] = val;
    this.setState({ data, errors });
    delete errors.region;
  };

  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  /*****************JOI VALIDATION CALL**************/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /*****************end Joi validation schema**************/

  // /***************** FORM SUBMIT **************/
  UpdateShipping = async () => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    if (data.country === "") {
      errors.country = "Please select a country";
      this.setState({
        errors: errors,
      });
    } else {
      delete errors.country;
      this.setState({ errors });
    }
    if (data.region === "") {
      errors.region = "Please select a region";
      this.setState({
        errors: errors,
      });
    } else {
      delete errors.region;
      this.setState({ errors });
    }

    let result = Joi.validate(data, this.schema);

    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors,
      });
    } else {
      if (Object.keys(errors).length > 0) {
        // if (errors.length > 0) {
        this.setState({
          errors: errors,
        });
      } else {
        const shipping_data = {
          country: this.state.data.country,
          region: this.state.data.region,
          zipcode: this.state.data.zipcode,
          city: this.state.data.city,
          slug: this.state.data.slug,
          street_address: this.state.data.street_address,
          street_address1: this.state.data.street_address1,
        };

        this.setState({ submit_status: true });
        this.setState({ spinner: true });
        const response = await shippingService.updateShippingData(
          shipping_data
        );

        if (response) {
          if (response.data.status === 1) {
            this.setState({ spinner: false });
            this.setState({
              message: response.data.message,
              message_type: "success",
            });
            this.setState({ submit_status: false });
            this.getShippingData();
            setTimeout(() => {
              this.props.history.push({
                // pathname: "/admin/shipping/",
              });
            }, 5000);
          } else {
            this.setState({ spinner: false });
            this.setState({ submit_status: false });
            this.setState({
              message: response.data.message,
              message_type: "error",
            });
          }
        }
      }
    }
  };

  render() {
    const { country, region } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader props={this.props} />
                </div>

                <div className="col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4">
                  {/* <React.Fragment>
                              <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                                    <ReactSpinner type="border" color="primary" size="10" />
                              </div>
                            </React.Fragment> */}
                  <React.Fragment>
                    <div className="categories-wrap p-4">
                      <div className="row">
                        <div className="col-md-6">
                          <div className="card">
                            <div className="card-header">
                              <strong>Edit Shipping Origin</strong>
                            </div>
                            <div className="card-body categories-add shipping-add">
                             

                              <div className="form-group">
                                <label>Country*</label>
                                <CountryDropdown
                                  className="form-control"
                                  as="select"
                                  value={this.state.data.country}
                                  onChange={(val) => this.selectCountry(val)}
                                  valueType="short"
                                  whitelist={this.state.targetCountry}
                                />
                                {this.state.errors.country ? (
                                  <div className="error text-danger">
                                    {this.state.errors.country}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group">
                                <label>Region/State*</label>
                                <RegionDropdown
                                  className="form-control"
                                  blankOptionLabel="Select Region"
                                  defaultOptionLabel="Select Region"
                                  valueType="short"
                                  countryValueType="short"
                                  country={this.state.data.country}
                                  value={this.state.data.region}
                                  onChange={(val) => this.selectRegion(val)}
                                />
                                {this.state.errors.region ? (
                                  <div className="error text-danger">
                                    {this.state.errors.region}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group">
                                <label>Zip*</label>
                                <input
                                  name="zipcode"
                                  className="form-control"
                                  type="text"
                                  value={this.state.data.zipcode}
                                  onChange={this.handleChange}
                                  placeholder="Zip"
                                />
                                {this.state.errors.zipcode ? (
                                  <div className="danger">
                                    {this.state.errors.zipcode}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group">
                                <label>City</label>
                                <input
                                  name="city"
                                  className="form-control"
                                  type="text"
                                  value={this.state.data.city}
                                  onChange={this.handleChange}
                                  placeholder="City"
                                />

                                {this.state.errors.city ? (
                                  <div className="danger">
                                    {this.state.errors.city}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div className="form-group d-none">
                                <label>Slug</label>
                                <input
                                  name="slug"
                                  className="form-control"
                                  type="text"
                                  value={this.state.data.slug}
                                  // onChange={this.handleChange}
                                  disabled
                                  placeholder="Slug"
                                />
                              </div>

                              <div className="form-group">
                                <label>Street Address*</label>

                                <input
                                  name="street_address"
                                  className="form-control"
                                  type="text"
                                  value={this.state.data.street_address}
                                  className="form-control address"
                                  placeholder="Street Address "
                                  onChange={this.handleChange}
                                />

                                {this.state.errors.street_address ? (
                                  <div className="danger">
                                    {this.state.errors.street_address}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="form-group">
                                <label>Street Address Line 2*</label>

                                <input
                                  name="street_address1"
                                  className="form-control"
                                  type="text"
                                  value={this.state.data.street_address1}
                                  className="form-control address"
                                  placeholder="Street Address line 2"
                                  onChange={this.handleChange}
                                />

                                {this.state.errors.street_address ? (
                                  <div className="danger">
                                    {this.state.errors.street_address1}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.UpdateShipping}
                                className="save-btn btn btn-dark"
                              >
                                {this.state.submit_status === true ? (
                                  <React.Fragment>
                                    <ReactSpinner
                                      type="border"
                                      color="primary"
                                      size="1"
                                    />
                                  </React.Fragment>
                                ) : (
                                  ""
                                )}
                                Save
                              </button>
                              <div className="tableinformation">
                                {this.state.message !== "" ? (
                                  <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                  </div>
                                ) : null}
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-6">
                          <ShippingMethod />
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ShippingOrigin;
