import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as CouponService from "../../ApiServices/admin/manageCoupon";
import { Link } from "react-router-dom";
import DataTable from 'react-data-table-component';
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import MultiSelect from "react-multi-select-component";
import moment from 'moment' 
import * as manageProducts from "../../ApiServices/admin/products";
import * as ManageTax from '../../ApiServices/admin/manageTax'
class GeneralTax extends Component {
 
    state = {
      id:"",
      errors: {},
      submit_status: false,
      message: "",
      message_type: "",
      table_message: "",
      table_message_type: "",
      addresses: [],
      delete_status: false,
      edit_status: false,
      data_table : [],
      spinner : true,
      country : "",
      state_code : '',
      zipcode : '',
      rate_percent :    '', //moment(Date.now()).toDate(),
      shipping : 'false',
      tax_name : '',
      priority : '',
      columns : [
        {
          name: 'tax',
          selector: 'tax_name',
         cell:  row => <p>{ row.tax_name  }</p>,
          sortable: true,
        },
        {
          name: 'country',
          selector: 'country',
        },
        {
          name: 'state',
          selector: 'state_code',
        },
        {
          name: 'shipping',
          selector: 'shipping',
          cell:  row => <p>{ row.shipping == true ?  'Enabled' : 'Disabled'}</p>,
          sortable: true,
        },
        {
          name: 'rate (%)',
          selector: 'rate_percent',
          sortable: true,
          width:'75px'
        },        
        {
          name: 'Priority',
          selector: 'priority',
          sortable: true,
          width:'75px'
        },
      {
        name: 'Action',
        cell:  row =><div className="action_dropdown" data-id={row._id}><i data-id={row._id} className="dropdown_dots"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
        <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
          <a  onClick={ () =>this.getTaxById(row._id) } className="dropdown-item">
                Edit
          </a>
          <a  onClick={ () =>this.handleRemoveTax(row._id) } className="dropdown-item">
                Delete
          </a>
        </div>
        </div>,
        allowOverflow: false,
        button: true,
        width: '56px', // custom width for icon button
    },
    ]
    };

    componentDidMount = () => {
      this.getAllTaxs()
    };

/*Get all Tax */
    getAllTaxs = async () => {
      this.setState({ spinner: true});
      try {
        this.setState({ spinner: true })
        const response = await ManageTax.getAllTax()
        if (response.data.status == 1) {       
          const Setdata = { ...this.state.data_table };
          const data_table_row = [];
          response.data.data.map((add,index) => {
            const Setdata = {};
            Setdata.country =  add.country;
            Setdata.state_code =  add.state;
            Setdata.zipcode =  add.zipcode;
            Setdata.rate_percent =  add.rate_percent;
            Setdata.priority =  add.priority;
            Setdata._id =  add._id;
            Setdata.shipping =  add.shipping;
            Setdata.tax_name = add.tax_name;
            Setdata.shipping = add.shipping;
            data_table_row.push(Setdata);
          });
          this.setState({ 
          data_table: data_table_row
          });
        } 
        this.setState({ spinner: false});
      } catch (err) {
        this.setState({ spinner: false })
      }
    } 

/* Joi validation schema */
    schema = {
      tax_name: Joi.string().required().error(() => {
        return {
          message: 'The Tax Name field is not allowed to be empty.',
        };
      }),
      country: Joi.string().required().error(() => {
        return {
          message: 'This field is a required field.',
        };
      }),
      state_code: Joi.string().required().error(() => {
        return {
          message: 'This field is a required field.',
        };
      }),
      zipcode: Joi.string().allow(""),
    
      rate_percent: Joi.number().positive().integer().allow("0").error(() => {
        return {
          message: 'This field is a required field.',
        };
      }),
      priority : Joi.number().positive().integer().allow("0").error(() => {
        return {
          message: 'This field is a required field.',
        };
      }),
   
      shipping: Joi.any(),
     
      id: Joi.string().allow(""),
    
    };

    validateProperty = (name, value) => {
      const obj = { [name]: value };
      const schema = { [name]: this.schema[name] };
      const { error } = Joi.validate(obj, schema);
      return error ? error.details[0].message : null;
    };

    handleChange = (input) => event => {
      const errors = { ...this.state.errors };
      delete errors.validate;
      const errorMessage = this.validateProperty(input, event.target.value);
      if (errorMessage) errors[input] = errorMessage;
      else delete errors[input];
      this.setState({ [input]: event.target.value, errors });
    }
  
    getTaxById = async (id) => {
      const response =  await ManageTax.getTaxById({id: id});
      if (response.data.status == 1) {
          this.setState({ 
              edit_status: true,
              submit_status: false,
              message: "",
              message_type: "",
              delete_status: false,
              country: response.data.data[0].country,
              state_code: response.data.data[0].state,
              zipcode: response.data.data[0].zipcode,
              rate_percent :response.data.data[0].rate_percent,
              shipping : response.data.data[0].shipping,
              tax_name : response.data.data[0].tax_name,
              priority : response.data.data[0].priority,
              id : response.data.data[0]._id,
          });
         
      } 
    }
    
    addCoupon = () => {
      this.setState({ 
        country : "",
        state_code : '',
        zipcode : '',
        rate_percent : '',
        priority : '',
        tax_name : '',
        shipping : '',
        attributes: [],
        errors: {},
        submit_status: false,
        message: "",
        message_type: "",
        delete_status: false,
        edit_status: false,
        descrption: ""
      })
    }

    saveTax = async (e) => { 
      e.preventDefault()
      const checkState = {  
        country: this.state.country,
        state_code: this.state.state_code,
        zipcode: this.state.zipcode,
        rate_percent : this.state.rate_percent,
        shipping : this.state.shipping,
        priority : this.state.priority,
        tax_name :  this.state.tax_name,
       };
      const errors = { ...this.state.errors };      
      this.setState({ errors  : {}});     
      let result = Joi.validate(checkState, this.schema);
      if (result.error) {            
          let path = result.error.details[0].path[0];
          let errormessage = result.error.details[0].message;
          errors[path] = errormessage;
          this.setState({ errors });
      } else {
        const category_data = {
          country: this.state.country,
          state_code: this.state.state_code,
          zipcode: this.state.zipcode,
          rate_percent : this.state.rate_percent,
          shipping : this.state.shipping,
          tax_name : this.state.tax_name,
          priority : this.state.priority,
          id : this.state.id
        }
        try {
          this.setState({ 
            submit_status: true,
            message: "",
            message_type: "" 
          });         
        if(this.state.edit_status === true){         
         const response = await ManageTax.updateTax(category_data);
          if (response) {
            this.setState({ submit_status: false });
             if(response.data.status === 1){
              this.setState({
                message: response.data.message,
                message_type: "success",
                country: "",
                state_code : "",
                zipcode: "",
                rate_percent: "",
                shipping: "false",
                priority: '',
                tax_name: '',
                id: "",
                edit_status: false
              });
              this.getAllTaxs();
             } else {
              this.setState({
                message: response.data.message,
                message_type: "error",
              });
             }     
            } else {
            this.setState({ submit_status: false });
            this.setState({
              message: "Something went wrong! Please try after some time",
              message_type: "error",
            });
          }
        }else{
              const response = await ManageTax.createTax(category_data);
              if (response) {
                this.setState({ submit_status: false });
                if(response.data.status === 1){
                  this.setState({
                    message: response.data.message,
                    message_type: "success",
                    country: "",
                    state_code : "",
                    zipcode: "",
                    rate_percent: "",
                    priority:'',
                    shipping: "false",
                    tax_name: ''
                  });
                  this.getAllTaxs()
                } else {
                  this.setState({
                    message: response.data.message,
                    message_type: "error",
                  });
                }
              } else {
                this.setState({ submit_status: false });
                this.setState({
                  message: "Something went wrong! Please try after some time",
                  message_type: "error",
                });
              }
              setTimeout(() => { 
                this.setState(() => ({message: ''}))
              }, 5000);
        }
        } catch (err) {
          
        }
     }
    }

    handleRemoveTax = async (id) => {
      this.setState({ 
        delete_status: true,
      });
      const response =  await ManageTax.removeTax({ _id: id });
      if (response.data.status === 1) {
          let newTaxs = [...this.state.data_table];
          newTaxs = newTaxs.filter(function( obj ) {
               return obj._id !== id;
          });
          this.setState({
            table_message: response.data.message,
            table_message_type: "sucess",
          });

          this.setState({ data_table :newTaxs, delete_status: false, edit_status: false });
          setTimeout(() => { 
            this.setState(() => ({table_message: ''}))
          }, 5000);
      } else {
        this.setState({
          table_message: response.data.message,
          table_message_type: "sucess",
        });
        setTimeout(() => { 
          this.setState(() => ({table_message: ''}))
        }, 5000);
      }
    }; 

    handlecouponChange  = async (e) =>{
      this.setState({ shipping : e.target.value});
    }

    render() {
        let datarow =  this.state.data_table;
        return (
          <React.Fragment>
            <div className="container-fluid admin-body">
              <div className="admin-row">
                <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
                </div>
                <div className="col-md-10 col-sm-12 content">
                  <div className="row content-row">
                    <div className="col-md-12 header">
                      <Adminheader  props={this.props} />
                    </div>
                    <div className="col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4" >  
                      <React.Fragment>     
                        <div className="categories-wrap p-4">
                          <div className="row">
                            <div className="col-md-4">
                              <div className="card">
                                <div className="card-header">
                                  <strong>{this.state.edit_status === true ? 'Edit' : 'Add'} Tax</strong>
                                  {this.state.edit_status === true ?  
                                  <button  onClick={this.addCoupon}
                                   className="ml-4 add-cat-btn btn btn-success">+ Add New</button> : ''}
                                </div>
                                <div className="card-body coupon-add categories-add">
                              
                              

                                    <div className="form-group">
                                 <input  onChange={this.handleChange('tax_name')}  
                                 placeholder="Tax Name *"
                                 disabled={this.state.edit_status === true ? "disabled" : false}
                                 value={this.state.tax_name}    className="form-control"></input>
                                 {this.state.errors.tax_name ? (<div className="danger">{this.state.errors.tax_name}</div>) : ( "")}
                                </div>

                                  <div className="form-group">
                                    <input
                                    className="form-control"
                                    placeholder="Country Code*"
                                    type="text"
                                    value={this.state.country}
                                    onChange={this.handleChange('country')}
                                    />
                                    {this.state.errors.country ? (<div className="danger">{this.state.errors.country}</div>) : ( "")}
                                  </div>
                                  <div className="form-group">
                                    <input  onChange={this.handleChange('state_code')}  
                                    placeholder="State Code *"
                                    value={this.state.state_code}    className="form-control"></input>
                                    {this.state.errors.state_code ? (<div className="danger">{this.state.errors.state_code}</div>) : ( "")}
                                  </div>
                                  <div className="form-group">
                                 <input  onChange={this.handleChange('zipcode')}  
                                 placeholder="Zipcode"
                                 value={this.state.zipcode}    className="form-control"></input>
                                 {this.state.errors.zipcode ? (<div className="danger">{this.state.errors.zipcode}</div>) : ( "")}
                               </div>
                            <div className="form-group">
                                 <input  onChange={this.handleChange('rate_percent')}  
                                 placeholder="Rate *"
                                 value={this.state.rate_percent}    className="form-control"></input>
                                 {this.state.errors.rate_percent ? (<div className="danger">{this.state.errors.rate_percent}</div>) : ( "")}
                               </div>                            
                                <div className="form-group">
                                 <input  onChange={this.handleChange('priority')}  
                                 placeholder="Priority *"
                                 value={this.state.priority}    className="form-control"></input>
                                 {this.state.errors.priority ? (<div className="danger">{this.state.errors.priority}</div>) : ( "")}
                                 </div>
                                  <div className="form-group">                                
                                  <select 
                                      value={ 
                                      this.state.shipping
                                      }
                                      onChange={this.handlecouponChange}>
                                      <option value="true">Shipping Enabled</option>
                                      <option value="false">Shipping Disabled</option>
                                  </select>
                                    {this.state.errors.couponstatus ? (<div className="danger">{this.state.errors.couponstatus}</div>) : ( "")}
                                  </div>
                                  <button 
                                    disabled={this.state.submit_status === true ? "disabled" : false}
                                    onClick={this.saveTax}
                                    className="save-btn btn btn-dark">
                                    {
                                      this.state.submit_status === true ? (
                                        <React.Fragment>
                                        <ReactSpinner type="border" color="primary" size="1" />
                                        <span className="submitting">
                                        {this.state.edit_status === true ? 'UPDATING TAX...' : 'CREATING TAX...'}
                                         </span>
                                        </React.Fragment>
                                    ) : (
                                      this.state.edit_status === true ? 'UPDATE TAX' : 'CREATE TAX'
                                    )
                                    }
                                  </button>
                                  {this.state.message !== "" ? (
                                <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                                </div> 
                                ) : null}

                                </div>
                                
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="card">
                              <div className="card-header">
                                <strong>Tax</strong>
                              </div>
                              <div className="card-body categories-list">
                              <React.Fragment>
                              <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                                    <ReactSpinner type="border" color="primary" size="10" />
                              </div>
                            </React.Fragment>
                              <p className="tableinformation">
                              {this.state.table_message !== "" ? (
                                <div className={this.state.table_message_type}>
                                <p>{this.state.table_message}</p>
                                </div>
                                ) : null
                              }                                
                              </p>
                              <DataTable 
                                  columns={this.state.columns}
                                  data={datarow}  
                                  highlightOnHover
                                  pagination
                                  selectableRowsVisibleOnly
                                  noDataComponent = {<p>There are currently no records.</p>}
                              />                             
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        </React.Fragment>     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        );
      }
    }
    
    export default GeneralTax;
    