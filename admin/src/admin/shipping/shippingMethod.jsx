import React, { Component } from "react";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import { Card, Accordion, Form, Button } from "react-bootstrap";
import * as shippingService from "../../ApiServices/admin/shippingSettings";
import { Multiselect } from "multiselect-react-dropdown";
import { ups } from "../../config.json";

class ShippingMethod extends Component {
  constructor(props) {
    super(props);
    this.multiselectRef = React.createRef();
  }
  state = {
    
    c_services: [],
    submit_status: false,
    isChecked: true,
    message: "",
    message_type: "",
    data: {
      sandbox: "",
      acount_no: "",
      username: "",
      password: "",
      access_key: "",
      ups_customer_classification: "",
      ups_pickup_type: "",
      packaging_type_code: "",
      packing_type: "",
      weight_type: "",
      carrier_services: "",
      tracing: "",
      diamension_type: "",
      slug: "",
      sandbox_username: "",
      sandbox_account_no:"",
      sandbox_access_key:"",
      sandbox_password:"",
    },
    errors: {},
    shippingMethodDetails: {},
    table_message: "",
    table_message_type: "",
    edit_status: false,
    data_table: [],
    crOptions: [],
    crOptionsSelected: [],
    multiSelect: ups.services
  };
  handleCheck = this.handleCheck.bind(this);
  
  /* Joi validation schema */
  schema = {
  
    acount_no: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
      sandbox_account_no: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),

    access_key: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
      sandbox_access_key: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),

    username: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your username",
        };
      }),
      sandbox_username: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your username",
        };
      }),
    password: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your password",
        };
      }),
      sandbox_password: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your password",
        };
      }),
    ups_customer_classification: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),

    ups_pickup_type: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
    packaging_type_code: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),

    packing_type: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),

    weight_type: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),


    diamension_type: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }), 
    sandbox: Joi.optional().label(" sandbox"),
    slug: Joi.optional().label("slug"),
    _id: Joi.string().allow(""),
    status: Joi.string().allow(""),
    insure_package: Joi.string().allow(""),
    additional_handling_charge: Joi.string().allow(""),
    carrier_services: Joi.optional().label("carrier_services"),
  };

 /*Input Handle Change */
  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

/* Joi validation call */
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };


  handleSelect = (selected) => {
    this.changeMarket(selected);
  };

  /*checkbox checked*/
  handleCheck()  {
    this.setState({ isChecked: !this.state.isChecked });
    
  };

  onSelectchange = (selectedList) => {
    const Setdata = { ...this.state.crOptions };
    const selList = [];
    selectedList.map((slist, index) => {
      selList[index] = slist.id;
    });
    
    this.setState({ crOptions: selList });
    this.setState({ crOptionsSelected: selectedList });
  };

  onRemove = (selectedList, removedItem) => {
    const selList = [];
    selectedList.map((slist, index) => {
      selList[index] = slist.id;
      
    });
    this.setState({ crOptions: selList });
    this.setState({ crOptionsSelected: selectedList });

    
  };


  componentDidMount = () => {
    this.getShippingMethod();
  };

   /*get shipping method data*/
  getShippingMethod = async () => {
    this.setState({ spinner: true });
    try {
      const response = await shippingService.getShippingMethod();
      if (response.data.status == 1) {
        this.setState({ data: response.data.shippingMethodDetails });
        const crOptions = response.data.shippingMethodDetails.carrier_services;
        if (crOptions) {
          this.setState({ crOptions: crOptions });
          const Setdata = { ...this.state.crOptions };
          const dataRow = [];
          const fulldata = [];
          this.state.multiSelect.map((items, index) => {
            const Setdata = {};
            Setdata.id = items.id;
            Setdata.name = items.name;
            if (this.state.crOptions.indexOf(items.id) > -1) {
              dataRow.push(Setdata);
            }
            fulldata.push(Setdata);
          });
          this.setState({ multiSelect: fulldata });
          this.setState({ crOptionsSelected: dataRow }); 
          
        }
      }

      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

 /* Form Submit */
  saveShippingmethod = async () => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    let formIsValid = false;
    if (this.state.crOptions.length == 0) {
      errors["crOptions"] = "Please select at least one role";
      this.setState({errors});   
    } 
    else {
      delete errors.crOptions;
      this.setState({ errors });
    }
    let result = Joi.validate(data, this.schema);
    let shipping_method;
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
    } else {
      if (Object.keys(errors).length > 0) {
        this.setState({
          errors: errors,
        });
      } else {
        shipping_method = {
          sandbox: this.state.isChecked,
          acount_no: this.state.data.acount_no,
          access_key: this.state.data.access_key,
          username: this.state.data.username,
          password: this.state.data.password,
          sandbox_account_no: this.state.data.sandbox_account_no,
          sandbox_access_key: this.state.data.sandbox_access_key,
          sandbox_username: this.state.data.sandbox_username,
          sandbox_password: this.state.data.sandbox_password,
          slug: this.state.data.slug,
          ups_customer_classification: this.state.data.ups_customer_classification,
          ups_pickup_type: this.state.data.ups_pickup_type,
          packaging_type_code: this.state.data.packaging_type_code,
          packing_type: this.state.data.packing_type,
          weight_type: this.state.data.weight_type,
          diamension_type: this.state.data.diamension_type,
          tracing: this.state.isChecked,
          crOptions: this.state.crOptions,
        };
        this.setState({ submit_status: true });
        this.setState({ spinner: true });
        const response = await shippingService.updateShippingMethod(
          shipping_method
        );
        if (response) {
          if (response.data.status === 1) {
            this.setState({ spinner: false });
           
            this.setState({
              message: response.data.message,
              message_type: "success",
            });
            this.setState({ submit_status: false });
          } else {
            this.setState({ spinner: false });
            this.setState({ submit_status: false });
           
            this.setState({
              message: response.data.message,
              message_type: "error",
            });
          }
        }
      }
    }
  };

  render() {
    const hidden = !this.state.isChecked ? '' : 'hidden';
    const show = !this.state.isChecked ? 'hidden' : '';
    let shipping_checked;
    const { c_services, multiSelect, crOptions } = this.state;
    let Packaingtypecode = Object.keys(ups.packaging_type_code).map((key) => (
      <option value={key}>{ups.packaging_type_code[key]} </option>
    ));
    let pickuptype = Object.keys(ups.pickup_type).map((key) => (
      <option value={key}>{ups.pickup_type[key]} </option>
    ));
    let customerClassification = Object.keys(
      ups.customer_classification
    ).map((key) => (
      <option value={key}>{ups.customer_classification[key]} </option>
    ));
    let pakingType = Object.keys(ups.packing_type).map((key) => (
      <option value={key}>{ups.packing_type[key]} </option>
    ));

    return (
      <React.Fragment>
        <div className="container">
          <div className=" row Shipping-methode">
            <Accordion defaultActiveKey="0" className="col-md-12">
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="0">
                  <h4>United Parcel Service</h4>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                    
                    <Form
                      name="checkoutForm"
                      id="shippingForm"
                      className="shipping-form"
                    >
                      <Form.Group controlId="ControlTextarea1">
                        <Form.Check
                          label="Use Sandbox"
                          type="checkbox"
                          checked={this.state.isChecked}
                          onChange={this.handleCheck}
                        />
                      </Form.Group>

       {/* /------------------------------------------------------------------------ */}
                        <div className={show}>
                      <Form.Group controlId="Controlacno">
                        <Form.Label>Sandbox Account Number</Form.Label>
                        <Form.Control
                          type="text"
                          name="sandbox_account_no"
                          value={
                            this.state.data.sandbox_account_no
                          }
                          onChange={this.handleChange}
                        />
                        {this.state.errors.acount_no ? (
                          <div className="danger">
                            {this.state.errors.acount_no}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="Controlaccesskey">
                        <Form.Label>Sandbox UPS Access Key</Form.Label>
                        <Form.Control
                          type="text"
                          name="sandbox_access_key"
                          value={
                            this.state.data.sandbox_access_key
                          }
                          onChange={this.handleChange}
                        />
                        {this.state.errors.access_key ? (
                          <div className="danger">
                            {this.state.errors.access_key}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="formBasicUserid">
                        <Form.Label>Sandbox Username</Form.Label>
                        <Form.Control
                          type="text"
                          name="sandbox_username"
                          value={
                            this.state.data.sandbox_username
                          }
                          onChange={this.handleChange}
                        />
                        {this.state.errors.username ? (
                          <div className="danger">
                            {this.state.errors.username}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="formBasicPassword">
                        <Form.Label>Sandbox Password</Form.Label>
                        <Form.Control
                          type="password"
                          name="sandbox_password"
                          value={
                            this.state.data.sandbox_password
                          }
                          onChange={this.handleChange}
                        />
                        {this.state.errors.password ? (
                          <div className="danger">
                            {this.state.errors.password}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      </div>


                      <div className={hidden}>
                      <Form.Group controlId="Controlacno">
                        <Form.Label>Account Number</Form.Label>
                        <Form.Control
                          type="text"
                          name="acount_no"
                          value={
                            this.state.data.acount_no
                          }
                          onChange={this.handleChange}
                        />
                        {this.state.errors.acount_no ? (
                          <div className="danger">
                            {this.state.errors.acount_no}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="Controlaccesskey">
                        <Form.Label>UPS Access Key</Form.Label>
                        <Form.Control
                          type="text"
                          name="access_key"
                          value={
                            this.state.data.access_key
                          }
                          onChange={this.handleChange}
                        />
                        {this.state.errors.access_key ? (
                          <div className="danger">
                            {this.state.errors.access_key}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="formBasicUserid">
                        <Form.Label>Username</Form.Label>
                        <Form.Control
                          type="text"
                          name="username"
                          value={
                            this.state.data.username
                          }
                          onChange={this.handleChange}
                        />
                        {this.state.errors.username ? (
                          <div className="danger">
                            {this.state.errors.username}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          type="password"
                          name="password"
                          value={
                            this.state.data.password
                          }
                          onChange={this.handleChange}
                        />
                        {this.state.errors.password ? (
                          <div className="danger">
                            {this.state.errors.password}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      </div>

{/* //---------------------------------------------------------------------------------- */}

                      <Form.Group controlId="mode">
                        <label>Carrier Services</label>
                      

                        <Multiselect
                          options={this.state.multiSelect} // Options to display in the dropdown
                          selectedValues={this.state.crOptionsSelected} // Preselected value to persist in dropdown
                          onSelect={this.onSelectchange} // Function will trigger on select event
                          onRemove={this.onRemove} // Function will trigger on remove event
                          displayValue="name" // Property name to display in the dropdown options
                          showCheckbox={true}
                          name="carrier_services"
                          ref={this.multiselectRef}
                        />

                      <span className="danger">{this.state.errors["crOptions"]}</span>                       
                      </Form.Group>
                      <Form.Group controlId="mode">
                        <label>UPS Customer Classification</label>
                        <select
                          className="form-control"
                          name="ups_customer_classification"
                          value={this.state.data.ups_customer_classification}
                          onChange={this.handleChange}
                        >
                          <option value="">
                            Select UPS Customer Classification
                          </option>
                          {customerClassification}
                        </select>

                        {this.state.errors.ups_customer_classification ? (
                          <div className="danger">
                            {this.state.errors.ups_customer_classification}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="mode">
                        <label>UPS Pickup Type</label>
                        <select
                          className="form-control"
                          name="ups_pickup_type"
                          value={this.state.data.ups_pickup_type}
                          onChange={this.handleChange}
                        >
                          <option value="">SelectUPS Pickup Type</option>
                          {pickuptype}
                        </select>
                        {this.state.errors.ups_pickup_type ? (
                          <div className="danger">
                            {this.state.errors.ups_pickup_type}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="mode">
                        <label>Packaging Type Codes</label>
                        <select
                          className="form-control"
                          name="packaging_type_code"
                          value={this.state.data.packaging_type_code}
                          onChange={this.handleChange}
                        >
                          <option value="">Select Packaging Type Codes</option>
                          {Packaingtypecode}
                        </select>
                        {this.state.errors.packaging_type_code ? (
                          <div className="danger">
                            {this.state.errors.packaging_type_code}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="mode">
                        <label>Packing Type</label>
                        <select
                          className="form-control"
                          name="packing_type"
                          value={this.state.data.packing_type}
                          onChange={this.handleChange}
                        >
                          <option value="">Select UPS Packing Type</option>
                          {pakingType}
                        </select>

                        {this.state.errors.packing_type ? (
                          <div className="danger">
                            {this.state.errors.packing_type}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="formBasicUserid">
                        <Form.Label>Slug</Form.Label>
                        <Form.Control
                          type="text"
                          name="slug"
                          value={this.state.data.slug}
                          onChange={this.handleChange}
                          disabled
                        />
                      </Form.Group>                     
                      <Form.Group controlId="WeightType">
                        <label>Weight Type</label>
                        <select
                          className="form-control"
                          name="weight_type"
                          value={this.state.data.weight_type}
                          onChange={this.handleChange}
                        >
                          <option value="">Select UPS Weight Type</option>
                          <option value="LBS">Pound</option>
                        </select>

                        {this.state.errors.weight_type ? (
                          <div className="danger">
                            {this.state.errors.weight_type}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <Form.Group controlId="DiamentionalType">
                        <label>Dimensions Type</label>
                        <select
                          className="form-control"
                          name="diamension_type"
                          value={this.state.data.diamension_type}
                          onChange={this.handleChange}
                        >
                          <option value="">Select Dimensions Type</option>
                          <option value="IN">Inches</option>
                        </select>
                        {this.state.errors.diamension_type ? (
                          <div className="danger">
                            {this.state.errors.diamension_type}
                          </div>
                        ) : (
                          ""
                        )}
                      </Form.Group>

                      <div className="categories-wrap">
                        <div className="categories-add">
                          <Button
                            disabled={
                              this.state.submit_status === true
                                ? "disabled"
                                : false
                            }
                            onClick={this.saveShippingmethod}
                            className="save-btn btn btn-dark"
                          >
                            {this.state.submit_status === true ? (
                              <React.Fragment>
                                <ReactSpinner
                                  type="border"
                                  color="primary"
                                  size="1"
                                />
                              </React.Fragment>
                            ) : (
                              ""
                            )}
                            Save
                          </Button>
                          {this.state.message !== "" ? (
                      <div className={this.state.message_type}>
                        <p>{this.state.message}</p>
                      </div>
                    ) : null}
                        </div>
                      </div>
                    </Form>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ShippingMethod;
