import React, { Component } from "react";
import Adminheader from "./common/adminHeader";
import Adminsidebar from "./common/adminSidebar";
import * as manageUser from "../ApiServices/admin/manageUser";
import ReactSpinner from "react-bootstrap-spinner";
import Pagination from "react-js-pagination";
import Moment from "react-moment";
import DeleteConfirm from "./common/deleteConfirm";
import { Link } from "react-router-dom";

class ManageLab extends Component {
  state = {
    data: { type: "lab", activePage: 1, number_of_pages: 10 },
    lab: [],
    checkStatus: false,
    loader: true,
    searchSpec: "",
    specializes: [
      { id: 1, value: "Ceramo-Metal & PFMs", isChecked: false },
      { id: 2, value: "Full-Cast", isChecked: false },
      { id: 3, value: "BruxZir® Solid Zirconia", isChecked: false },
      { id: 4, value: "Monolithic Glass Ceramic", isChecked: false },
      { id: 5, value: "Zirconia", isChecked: false },
      { id: 6, value: "Vivaneer™ Veneers", isChecked: false },
    ],
    modal: false,
  };
  handlePageChange = (pageNumber) => {
    let data = { ...this.state.data };
    let lab = [...this.state.lab];
    data.activePage = pageNumber;
    this.setState({ data, lab, loader: true });
    setTimeout(() => this.getallusers(), 1);
  };

  componentDidMount = async () => {
    const response = await manageUser.getCount(this.state.data.type);
    if (response) {
      if (response.status != 0) {
        let totalUsers = response.data.data.total;
        this.setState({ totalUsers: totalUsers });
      }
    
    }
    this.getallusers();
  };
  getallusers = async () => {
    const response = await manageUser.getAllusers(this.state.data);
    if (response.data.status == 1) {
      this.setState({ lab: response.data.data, loader: false });

    }
  };
  changeStatus = async (id, status, index) => {
    const response = await manageUser.changeStatus(id, status);
    if (response.data.status == 1) {
      let lab = [...this.state.lab];
      lab[index]["admin_approve"] = status;
      this.setState(lab);

    }
  };
  deleteUser = async () => {

    let id = this.state.user;
    let lab_index = this.state.index;
    const response = await manageUser.deleteUser(id);
    if (response) {
      if (response.data.status == 1) {
        let newlab = [...this.state.lab];
        newlab.splice(lab_index, 1);
        this.setState({ lab: newlab });
        const response = await manageUser.getCount(this.state.data.type);
        if (response) {
          let totalUsers = response.data.total;
          this.setState({ totalUsers: totalUsers });
          this.getallusers();
          this.toogle();
        }
      }
    }
  };
  saveAndtoogle = (id, lab_index) => {
    this.setState({ user: id, index: lab_index });
    this.toogle();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };

  render() {
    const images = require.context("../assets/images/admin", true);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs users">Users</span>
                    <span className="navs nav-sub">-</span>
                    <span className="navs nav-sub">Lab</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="tz-admin-manage-block">
                    <div className="title-bar">
                      <div className="title">Lab</div>
                      <div className="export">
                        <button className="export-btn">Export Data</button>
                      </div>
                    </div>
                    <div className="listusers table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col" className="name">
                              LAB
                            </th>
                            <th scope="col" className="date">
                              DATE JOINED
                            </th>

                            <th scope="col" className="email">
                              {" "}
                              EMAIL
                            </th>
                            <th scope="col" className="approve">
                              APPROVAL
                            </th>
                            <th scope="col" className="actions"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.lab.length != 0
                            ? this.state.lab.map((lab, index) => (
                                <tr className="odd" key={index}>
                                  <td scope="row" className="name">
                                    <img
                                      src={images("./no-user.png")}
                                      alt="profile"
                                      className="img-fluid"
                                    />
                                    {lab.profile_data
                                      ? lab.profile_data.labName
                                      : "NA"}
                                  </td>
                                  <td className="date">
                                    <Moment format="DD/MM/YYYY">
                                      {lab.created_on}
                                    </Moment>
                                  </td>

                                  <td className="email">{lab.email}</td>
                                  <td className="approve" scope="col">
                                    <label className="switch ">
                                      <input
                                        type="checkbox"
                                        className="default"
                                        defaultChecked={lab.admin_approve}
                                        onChange={() =>
                                          this.changeStatus(
                                            lab._id,
                                            !lab.admin_approve,
                                            index
                                          )
                                        }
                                      />
                                      <span className="slider round"></span>
                                      <span className="approve-label">
                                        {lab.admin_approve
                                          ? "Dispprove"
                                          : "Approve"}
                                      </span>
                                    </label>
                                  </td>
                                  <td scope="col" className="actions">
                                    <div className="lg-screen-actions">
                                      <div class="dropdown">
                                        <button
                                          class="btn  dropdown-toggle"
                                          type="button"
                                          data-toggle="dropdown"
                                          aria-haspopup="true"
                                          aria-expanded="false"
                                        ></button>
                                        <div class="dropdown-menu">
                                          <Link
                                            class="dropdown-item"
                                            onClick={() =>
                                              this.saveAndtoogle(lab._id, index)
                                            }
                                          >
                                            Delete
                                          </Link>
                                          <Link
                                            class="dropdown-item"
                                            to={
                                              "/admin/users/edit-lab/" + lab._id
                                            }
                                          >
                                            Edit
                                          </Link>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="sm-screen-actions">
                                      <Link
                                        class="btn-edit btn"
                                        to={"/admin/users/edit-lab/" + lab._id}
                                      >
                                        Edit
                                      </Link>
                                      <Link
                                        className="btn btn-dlt"
                                        onClick={() =>
                                          this.saveAndtoogle(lab._id, index)
                                        }
                                      >
                                        Delete
                                      </Link>
                                    </div>
                                  </td>
                                </tr>
                              ))
                            : ""}
                          {this.state.loader ? (
                            <tr>
                              <td
                                colSpan="6"
                                style={{
                                  textAlign: "center",
                                  background: "#fff",
                                }}
                              >
                                <ReactSpinner
                                  type="border"
                                  color="primary"
                                  size="4"
                                />
                              </td>
                            </tr>
                          ) : (
                            ""
                          )}
                        </tbody>
                      </table>
                      {this.state.lab.length != 0 ? (
                        <div className="paginate">
                          <Pagination
                            hideFirstLastPages
                            prevPageText={<i className="left" />}
                            nextPageText={<i className="right" />}
                            activePage={this.state.data.activePage}
                            itemsCountPerPage={10}
                            totalItemsCount={this.state.totalUsers}
                            pageRangeDisplayed={4}
                            onChange={this.handlePageChange}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.deleteUser}
        />
      </React.Fragment>
    );
  }
}

export default ManageLab;
