import React, { Component } from "react";
import AboutUs from "./pages/aboutUs";
import Careers from "./pages/career";
import News from "./pages/news"; 
import Home from "./pages/home"; 
import Footer from "./pages/subFooter";
import Contact from "./pages/contact"; 
import CareerCenter from "./pages/careerCenter"; 
import DistributorOversea from "./pages/distributorOversea";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import Hmi from "./pages/hmi";
import Plc from "./pages/plc";
import HybridHmi from "./pages/hybridHmi";
import SiProgram from "./pages/cimonSip";
import OurCompany from "./pages/ourCompany";
import TechSupport from "./pages/techSupport";
import Download from "./pages/download";
import Certificates from "./pages/certificates";
import Ipc from "./pages/ipc";
import Scada from "./pages/scada";
import BlockUi from "react-block-ui";
import { Link } from 'react-router-dom';
import * as managePageService from "../../ApiServices/admin/managePages";
import ProductsSplash from "./pages/productsSplash";
import DynamicTemplate from "./pages/dynamicTemplate";

class Preview extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
        blocking: false,   
        template_name: '',
        slug: '',
        page: {},
        id:'' 
      
    };
  }
 
  // noPreview = () => {
  //   return (
  //     this.state.blocking === true ? <h4 className="no-product">Loading...</h4>:<h4 className="no-product">Sorry no Preview found!</h4>
  //   );
  // }
  getPageContent = async () => {
    const slug = this.props.match.params.slug;
    this.setState({
        id: slug,
        blocking:true
    });
    try {
        this.setState({ spinner: true });
        const response = await managePageService.getSinglePageById(slug);
        if (response.data.status === 1) { 
            console.log(1233,response);
            this.setState({
                template_name: response.data.data.page_data.template_name,  
                page: response.data.data.page_data, 
                slug: response.data.data.page_data.slug,
                blocking:false                 
            })
        } else {
            this.setState({
                template_name: '',  
                page: {}, 
                blocking:false                 
            })
        }
    } catch (err) {
        this.setState({
            template_name: '',   
            page: {},
            blocking:false                 
        })
    }        
}

componentDidMount = async () => {
    this.getPageContent();
}

renderEditPage() {

  const {template_name, page, slug, id} = this.state;
  switch(template_name) {
        case "about-us" : 
            return(

                <AboutUs
                page = {page}
                template_name = {template_name}
                slug = {slug}
                id={id}
                {...this.props}
                />
            );
            break;
        case 'our-company':
                return(
                    <OurCompany
                    page = {page}
                    template_name = {template_name}
                    slug = {slug}
                    id={id}
                    {...this.props}
                    />
                );
                break;
        case 'download':
            return(
                <Download
                page = {page}
                template_name = {template_name}
                slug = {slug}
                id={id}
                {...this.props}
                />
            );
            break;
        case 'products-splash':
            return(
                <ProductsSplash
                page = {page}
                template_name = {template_name}
                slug = {slug}
                id={id}
                {...this.props}
                />
            );
            break;
        case 'techsupport':
            return(
                <TechSupport
                page = {page}
                template_name = {template_name}
                slug = {slug}
                id={id}
                {...this.props}
                />
            );
            break;
        case 'careers':
            return(
                <Careers
                page = {page}
                template_name = {template_name}
                slug = {slug}
                id={id}
                {...this.props}
                />
            );
            break;
        case "career-center" : 
            return(
                <CareerCenter
                page = {page}
                template_name = {template_name}
                slug = {slug}
                id={id}
                {...this.props}
                />
            );
            break;
        case "si-program" : 
            return(
                <SiProgram
                page = {page}
                template_name = {template_name}
                slug = {slug}
                id={id}
                {...this.props}
                />
            );
            break;
        case "news" : 
        return(
            <News
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;
        case "footer" : 
        return(
            <Footer
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;
        case "contact" : 
        return(
            <Contact
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;
        case "hmi" : 
        return(
            <Hmi
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;

        case "plc" : 
        return(
            <Plc
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;

        case "hybrid-hmi" : 
        return(
            <HybridHmi
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;
        case "ipc" : 
        return(
            <Ipc
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;

        case "scada" : 
        return(
            <Scada
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;

        case "home" : 
        return(
            <Home
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;
        
        case "distributor-oversea" : 
        return(
            <DistributorOversea
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;

        case "dynamic" : 
        return(
            <DynamicTemplate
          //  page_title = {page_title}
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;



        case "dynamic" : 
        return(
            <DynamicTemplate
          //  page_title = {page_title}
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;

        
        case "certificates" : 
        return(
            
           <Certificates
            page = {page}
            template_name = {template_name}
            slug = {slug}
            id={id}
            {...this.props}
            />
        );
        break;
        


        default:


  }

}
render() {

  const {blocking, template_name} = this.state;
  return (
  <React.Fragment>
      <BlockUi tag="div" blocking={blocking} >
      {blocking == false && template_name == '' ?
      <div className="container-fluid admin-body">
          <div className="admin-row">
              <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
              </div>
  
              <div className="col-md-10 col-sm-12  content">
                  <div className="row content-row">
                      <div className="col-md-12  header">
                      <Adminheader props={this.props} />
                      </div>
                      <div className="col-md-12  contents backgroundoverlay  home-inner-content pt-4 pb-4 pr-4">
                          <div className="row addpage-form-wrap">
                              <div className="addpage-form">
                                  <div className="col-md-12 ">
                                      <div className="form-group">
                                          <label>Edit Page</label>
                                          <div>
                                              Sorry! no page details found.
                                              <Link to="/admin/pages"> Back to pages</Link>
                                          </div>
                                      </div>
                                  </div>                                            
                              </div>
                          </div>
                      </div>                    
              </div>
              </div>
          </div>
          
      </div>
      :
      this.renderEditPage()
      }
      </BlockUi>         
  </React.Fragment>
  );
}

}
export default Preview;
