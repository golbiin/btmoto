import React, { Component } from "react";
import { Container, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import Productbanner from "./../../../frontend/common/productBanner";
import Joi from "joi-browser";
import BlockUi from "react-block-ui";
import { connect } from "react-redux";
import { USER_TYPES } from "./../../../config.json";
import { login } from "./../../../services/actions/userActions";
import * as pageService from "./../../../ApiServices/page";
import { Helmet } from "react-helmet";

class DistributorPortal extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      data: { username: "", usertype: USER_TYPES.DISTRIBUTOR },
      errors: {},
      productBdata: {
        title: "DISTRIBUTOR PORTAL",
        backgroundImage: "portal.png",
      },
      content: {},
      page_title:"",
    };
  }
  /* Joi validation schema */
  schema = {
    username: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your integrator id",
        };
      }),
    usertype: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter usertype",
        };
      }),
  };

  /* On change input save data */
  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  /* Joi Validation Call */
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  loginSubmit = async (event) => {
    event.preventDefault();
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    let result = Joi.validate(data, this.schema);
    if (result.error) {
      
        let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
    } else {
      this.props.login(data, false);
    }
  };

/* Get Page Contents */  
  getContent = async (slug) => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
      /* Banner-Slider Contents */  
        if (response.data.data.content.bannerSlider) {
          const Setdata = {};
          response.data.data.content.bannerSlider.map((banner, index) => {
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.backgroundImage = banner.image
              ? banner.image
              : "portal.png";
          });
          this.setState({
            productBdata: Setdata,
          });
        }
        this.setState({ content: response.data.data.content });
        if(response.data.data.page_title){
          this.setState({ 
            page_title: response.data.data.page_title
            });

        }
      } 
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  resourceUrl = () => {
    const {authUser} = this.props;
    if(authUser.usertype  === USER_TYPES.DISTRIBUTOR) {
        return(
            <Link to={"/distributor-resources"}>Visit Distributor Resources</Link>
        )
    } else {
        return(
            <Link to={"/"}>Go to home</Link>
        );
    }
  }
  componentDidMount = async () => {
    const slug = this.props.slug;
    this.getContent(slug);
  };
  render() {
    const { productBdata,page_title } = this.state;
    const { loginProcessing, loginFailure, authUser } = this.props;
    return (
      <React.Fragment>
        <Helmet>
        <title>{ page_title ? page_title:" " }</title>
        </Helmet>
        <Header />
        <div id="product-subcategory" className="portal">
          <div className="center">
            <Productbanner productbannerInfo={productBdata} />
          </div>
          <Container>
            <BlockUi tag="div" blocking={loginProcessing}>
              <div className="welcome">
                <h2>
                  {" "}
                  {this.state.content.title1 ? this.state.content.title1 : ""}                  
                </h2>
                
                { (authUser.email) ?  
                  <div>
                    <h3>{ authUser.first_name + ' ' + authUser.last_name}</h3>
                    {this.resourceUrl()}
                  </div>
                  :     
                        
                  <Form method="post" onSubmit={this.loginSubmit}>
                    <h5>
                      {this.state.content.title2 ? this.state.content.title2 : ""}
                    </h5>
                    <input
                      type="text"
                      name="username"
                      onChange={this.handleChange}
                      value={this.state.data.username}
                    ></input>
                    {this.state.errors.username ? (
                      <div className="danger">{this.state.errors.username}</div>
                    ) : (
                      ""
                    )}
                    {loginFailure.message !== "" ? (
                      <div className={loginFailure.message_type}>
                        <p className={loginFailure.message_type}>
                          {loginFailure.message}
                        </p>
                      </div>
                    ) : null}
                    <input
                      type="hidden"
                      name="usertype"
                      onChange={this.handleChange}
                      value={this.state.data.usertype}
                    ></input>
                    {this.state.errors.usertype ? (
                      <div className="danger">{this.state.errors.usertype}</div>
                    ) : (
                      ""
                    )}
                    <p>
                      {this.state.content.title3
                        ? this.state.content.title3
                        : ""}
                    </p>
                    <button type="submit">
                      {this.state.content.title4
                        ? this.state.content.title4
                        : ""}            
                    </button>
                  </Form>                  
                }       
              
              </div>
            </BlockUi>
          </Container>
        </div>
        <Footer />
      </React.Fragment>
    );

  }
}
const mapStateToProps = (state) => ({
  authUser: state.auth.authUser,
  loginProcessing: state.auth.loginProcessing,
  loginFailure: state.auth.loginFailure,
});
export default connect(mapStateToProps, {
  login,
})(DistributorPortal);
