import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import { Helmet } from 'react-helmet';
import SubFooter from './../../../frontend/common/subFooter'
import * as authServices from "../../../ApiServices/login";
import * as pageService from "../../../ApiServices/page";

const TITLE = 'Download | Automation Company - Industrial Automation';

class Download extends Component {
  state = {
    userloggin : false,
    content: {},
    downloadData : {}
  };

  componentDidMount = () => {
    this.getUserDetails();
    const slug = this.props.slug;
    this.getContent(slug);
  };

  getUserDetails = async () => {
    console.log('getUserDetails');    
    const response =  authServices.getCurrentUser();    
    if(response){
      if(response.email){
          authServices.getUserDetails(response.email)
          .then( async response1 => {
            if(response1){                           
              if(response1.data.data.hasOwnProperty('email')){
                this.setState({userloggin:true});
              }
            }
          });
      }
    } else {
      this.setState({userloggin:false});
      
    }
  }

  openLoginpopup = () => {
    document.getElementById("dropdownMenuLink").click();
  };


  
  getContent = async (slug) => {
    console.log('getContent',slug);
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status == 1) {
        /* Banner-Slider Contents */
        if (response.data.data.content.bannerSlider) {
          const Setdata = {};
          response.data.data.content.bannerSlider.map((banner, index) => {
            Setdata.title = banner.heading ? banner.heading : '';
            Setdata.description = banner.subheading ? banner.subheading : '';
            Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
          });
          console.log('this.downloadData.imageSetdata', Setdata);
          this.setState({
            downloadData: Setdata,
          });
        }
        this.setState({ content: response.data.data.content });

        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });

        }
      } else {

      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  render() {
    const images = require.context("../../../assets/images", true);
    const {downloadData} = this.state;

    let bg = '';
    if (downloadData.image) {
    bg = downloadData.image;
    } else {
    bg = 'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1602090889709_download-banner.jpg';
    }


    return (
      <React.Fragment>
      <Helmet>
      <title>{ TITLE }</title>
      </Helmet>
        <Header />
        <div id="download" className="container-fluid">
          <div className="dwnld-banner">
            <div className="dwnld_banner_bg"
            
            style={{ backgroundImage: "url(" + bg + ")" }}
            >
              <div className="dbg"></div>
              <div className="container">
                <div className="dwnhedng">
                  <h1
                     dangerouslySetInnerHTML={{
                      __html: downloadData.title
                        ? downloadData.title
                        : "",
                    }}
                  >
                    {/* EXPLORE WHAT CIMON <br /> SOFTWARE IS CAPABLE OF */}
                  </h1>

                  <div  dangerouslySetInnerHTML={{
                      __html: downloadData.description
                        ? downloadData.description
                        : "",
                    }}>

                  </div>
                  {/* <h6>Download CIMON Software</h6>
                  <div className="row">
                    <div className="col-md-1 "></div>
                    <div className="col-md-3 verion-btn">
                      <div class="btns">


                        {this.state.userloggin === true ? (  <a   className="first">
                          <span>
                            {" "}
                            CICON
                            <br />
                            <small>Version V7.02 (02-20-2019)</small>
                          </span>
                        </a>) : (  <a  onClick={() =>
                        this.openLoginpopup()
                      } id="dropdownMenuLink" className="first">
                          <span>
                            {" "}
                            CICON
                            <br />
                            <small>Version V7.02 (02-20-2019)</small>
                          </span>
                        </a>)

                        }
                      
                      </div>
                    </div>
                    <div className="col-md-3 verion-btn">
                      <div class="btns">
                      {this.state.userloggin === true ? ( <a className="second">
                          <span>
                            XPANEL DESIGNER
                            <br />
                            <small>Version V2.52 (04-23-2020)</small>
                          </span>
                        </a>) : ( <a
                        id="dropdownMenuLink"
                        onClick={() =>
                          this.openLoginpopup()
                        } 
                        className="second">
                          <span>
                            XPANEL DESIGNER
                            <br />
                            <small>Version V2.52 (04-23-2020)</small>
                          </span>
                        </a>)
                      }
                       
                      </div>
                    </div>
                    <div className="col-md-3 verion-btn">
                      <div className="col-md-1 "></div>
                      <div class="btns">
                      {this.state.userloggin === true ? (<a className="third">
                          <span>
                            ULTIMATE ACCESS
                            <br />
                            <small>Version V3.90_R190522 (05-19-2020)</small>
                          </span>
                        </a> ) : (<a 
                         id="dropdownMenuLink"
                         onClick={() =>
                           this.openLoginpopup()
                         } 
                         className="third">
                          <span>
                            ULTIMATE ACCESS
                            <br />
                            <small>Version V3.90_R190522 (05-19-2020)</small>
                          </span>
                        </a>)
  }
                      </div>
                    </div>
                  </div> */}
                
                
                
                </div>
              </div>
            </div>
          </div>

          <div id="home-section-join-industry" data-aos="fade-up"  data-aos-duration="500">
            <div className="container" >
              <div className="row">
                <div
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section1
                      ? this.state.content.section1
                      : "",
                  }}
                
                className="col-lg-12 col-md-12 col-sm-12">
                  {/* <h2>The Future of Automation</h2>
                  <p>
                    We are a company focused on the future of automation. One
                    that helps engineers, integrators, and OEMs become
                    successful through collaboration, partnership and ongoing
                    education. That is why all of our software is available at
                    no cost with Free and Unlimited technical support, Free
                    training, and Free software upgrades. Always.
                  </p> */}
                </div>
              </div>
            </div>
          </div>

          <div id="dwn-some" >
            <div
            
            dangerouslySetInnerHTML={{
              __html: this.state.content.section2
                ? this.state.content.section2
                : "",
            }}
            className="container">
             
              {/* <div className="row">
           
           
               <div className="col-lg-12 col-md-12 col-sm-12 head">
                  <h2>Some of Our Customers</h2>
                </div>
                <div class="col col-first">
                  {" "}
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/samsung-1.png`)}
                  ></img>
                </div>
                <div class="col  col-sec">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/lg-1.png`)}
                  ></img>
                </div>
                <div class="col col-third">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/hyundai-1.png`)}
                  ></img>
                </div>
                <div class="col col-four">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/chevrolet-1.png`)}
                  ></img>
                </div>
                <div class="col col-fifth">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/kia-1.png`)}
                  ></img>
                </div>
          
          
              </div>

              <div className="row">
                <div class="col brand-imgs">
                  {" "}
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/sk-1.png`)}
                  ></img>
                </div>
                <div class="col brand-imgs">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/kt-1.png`)}
                  ></img>
                </div>
                <div class="col brand-imgs">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/korail-1.png`)}
                  ></img>
                </div>
                <div class="col brand-imgs">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/posco-1.png`)}
                  ></img>
                </div>
                <div class="col brand-imgs">
                  <img
                    className="img-fluid"
                    alt=""
                    src={images(`./frontend/icps-1.png`)}
                  ></img>
                </div>
              </div>
         */}
            </div>
          </div>

          <div id="cimon-cicon" className="dwnload-common-section" data-aos="fade-up"  data-aos-duration="500">
            <div className="container">
              <div
              
              dangerouslySetInnerHTML={{
                __html: this.state.content.section3
                  ? this.state.content.section3
                  : "",
              }}
              className="row">
         
{/*          
                <div className="col-lg-6 l-section">
                  <div className="cimon-comp-img">
                    <img
                      className="img-fluid"
                      alt=""
                      src={images(`./frontend/cimon-comp.png`)}
                    ></img>
                  </div>
                </div>
                <div className="col-lg-6 r-section">
                  <h2>CIMON CICON FOR PLC</h2>
                  <div className="cicon-content">
                    <p>
                      CICON is a PLC program editor/compiler that loads user-
                      created programs directly to the PLC. The software comes
                      with a rich set of features and provides an easy,
                      intuitive interface to save time on development and
                      maximize system performance
                    </p>
                  </div>
                  <div className="btns">
                  {this.state.userloggin === true ? (<a   className="swr-dwnld" role="button">
                      DOWNLOAD SOFTWARE 
                      <br />
                      <small>Version V7.02 (02-20-2019)</small>
                    </a>) : (<a  
                     id="dropdownMenuLink"
                     onClick={() =>
                       this.openLoginpopup()
                     } 
                    className="swr-dwnld" role="button">
                      DOWNLOAD SOFTWARE 
                      <br />
                      <small>Version V7.02 (02-20-2019)</small>
                    </a>)

                    }
                    <br />

                    <a className="manual-dwnld" role="button">
                      {" "}
                      DOWNLOAD MANUAL
                    </a>
                  </div>
                  <p>
                    Looking for an older version?{" "}
                    <a href="" className="check-older">
                      Check for older version here.
                    </a>
                  </p>
                </div>
              */}
             
             
              </div>
            </div>
          </div>

          <div id="plc-section" className="plc-common-section" data-aos="fade-up"  data-aos-duration="500">
            <div
              
              dangerouslySetInnerHTML={{
                __html: this.state.content.section4
                  ? this.state.content.section4
                  : "",
              }}
            className="container">
          
{/*           
              <div className="row first-section">
                <div class="col-md-5 l-section">
                  <div className="image-section plc-conec">
                    <h4>Variety of PLC Connection</h4>
                    <p>
                      Supports multiple connection
                      <br />
                      interfaces such as RS232/422/485,
                      <br />
                      USB cable, and Ethernet
                    </p>
                  </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5 r-section">
                  <div className="image-section pid-control">
                    <h4>Easy PID Control</h4>
                    <p>
                      convenient functions such as <br />
                      managing historical data, trends,
                      <br />
                      screen shots,etc
                    </p>
                  </div>
                </div>
              </div>

              <div className="row">
                <div class="col-md-5 left">
                  <div className="image-section plc-permi">
                    <h4>PLC Permission Mode</h4>
                    <p>
                      Provides security function to protect
                      <br />
                      programs from unauthorized users
                      <br />
                      (supported in CICON software
                      <br />
                      v.7.00 or above)
                    </p>
                  </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5 right">
                  <div className="image-section plc-simul">
                    <h4>PLC Simulator</h4>
                    <p>
                      Virtually run scan programs and special card settings
                      without having
                      <br />
                      to connect the PLC to the software
                    </p>
                  </div>
                </div>
              </div>
            */}
           
           
           
            </div>
        
        
        
          </div>

          <div className="xpanel-designer" data-aos="fade-up"  data-aos-duration="500">
            <div className="container">
              <div className=""
                
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section5
                    ? this.state.content.section5
                    : "",
                }}
              >
             
{/*              
                <div className="col-lg-5 r-section">
                  <h2>
                    <b>
                      CIMON
                      <br /> XPANEL
                    </b>
                    DESIGNER
                  </h2>
                  <div className="xpanel-content">
                    <p>
                      Xpanel Designer can make your facility smarter and more
                      powerful
                    </p>
                  </div>
                  <div className="xpanel-btns">

                  {this.state.userloggin === true ? (
                    <a className="soft-dwnld" role="button">
                      DOWNLOAD SOFTWARE
                      <br />
                      <small>Version V2.52 (04-23-2020)</small>
                    </a> ) : (

<a className="soft-dwnld"
 id="dropdownMenuLink"
 onClick={() =>
   this.openLoginpopup()
 } 
role="button">
DOWNLOAD SOFTWARE
<br />
<small>Version V2.52 (04-23-2020)</small>
</a>
                    )
  }
                    <br />

                    <a className="manual-dwnld" role="button">
                      {" "}
                      DOWNLOAD MANUAL
                    </a>
                  </div>
                  <p>
                    Looking for an older version?{" "}
                    <a className="check-older">
                      Check for older version here.
                    </a>
                  </p>
                </div>
                <div className="col-lg-6 second-section">
                  <div className="cimon-comp-img">
                    <img
                      className="img-fluid"
                      alt=""
                      src={images(`./frontend/desktop_downloads.png`)}
                    ></img>
                  </div>
                </div>
            
             */}
            
              </div>
            </div>
          </div>

          <div className="plc-common-section drag-and-drop" data-aos="fade-up"  data-aos-duration="500">
            <div
              
              dangerouslySetInnerHTML={{
                __html: this.state.content.section6
                  ? this.state.content.section6
                  : "",
              }}
            className="container">
       
{/*        
              <div className="row first-section">
                <div class="col-md-5 left">
                  <div className="image-section drag-drop">
                    <h4>Drag-and-Drop Function</h4>
                    <p>
                      Just drag-and-drop images to make
                      <br />
                      your own project for your company
                    </p>
                  </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5 right">
                  <div className="image-section xpanel">
                    <h4>Xpanel-R</h4>
                    <p>
                      It is possible to use Xpanel
                      <br />
                      Designer in CIMON PPC (Windows
                      <br />
                      7/10 OS)
                    </p>
                  </div>
                </div>
              </div>

              <div className="row">
                <div class="col-md-5 left">
                  <div className="image-section smart-hmt">
                    <h4>Smart HMI and Software</h4>
                    <p>
                      Xpanel Designer provides various
                      <br />
                      graphical objects to allow rapid
                      <br />
                      project development for your team
                    </p>
                  </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5 right">
                  <div className="image-section conve">
                    <h4>Convenient and Reliable HMI</h4>
                    <p>
                      All the PLC address data can be
                      <br />
                      indentified and modified in the
                      <br />
                      database
                    </p>
                  </div>
                </div>
              </div>
      */}
     
     
     
            </div>
          </div>

          <div className="dwnload-common-section ultimate-access" data-aos="fade-up"  data-aos-duration="500">
            <div className="container">
              <div className="row" 
                   dangerouslySetInnerHTML={{
                    __html: this.state.content.section7
                      ? this.state.content.section7
                      : "",
                  }}
              >
                {/* <div className="col-lg-7 l-section">
                  <div className="cimon-comp-img">
                    <img
                      className="img-fluid"
                      alt=""
                      src={images(`./frontend/side_desktop_downloads.png`)}
                    ></img>
                  </div>
                </div>
                <div className="col-lg-5 r-section">
                  <h2>CIMON ULTIMATEACCESS</h2>
                  <div className="cicon-content">
                    <p>
                      The Ultimate Access has many features for an easy start.
                      CIMON D (Engineering tool) and CIMON X (Runtime viewer)
                      are included in Ultimate Access. With easy interface and
                      many helpful wizards, you can save time for development.
                    </p>
                  </div>
                  <div className="btns">

                  {this.state.userloggin === true ? (<a className="swr-dwnld" role="button">
                      DOWNLOAD SOFTWARE
                      <br />
                      <small>Version V7.02 (02-20-2019)</small>
                    </a>) : (<a className="swr-dwnld" 
                     id="dropdownMenuLink"
                     onClick={() =>
                       this.openLoginpopup()
                     } 
                    role="button">
                      DOWNLOAD SOFTWARE
                      <br />
                      <small>Version V7.02 (02-20-2019)</small>
                    </a>)
  }
                    
                    <br />

                    <a className="manual-dwnld" role="button">
                      {" "}
                      DOWNLOAD MANUAL
                    </a>
                  </div>
                  <p>
                    Looking for an older version?{" "}
                    <a className="check-older">
                      Check for older version here.
                    </a>
                  </p>
                </div>
          */}
         
              </div>
            </div>
          </div>

          <div className="plc-common-section library" data-aos="fade-up"  data-aos-duration="500">
            <div 
            
            dangerouslySetInnerHTML={{
              __html: this.state.content.section7sub1
                ? this.state.content.section7sub1
                : "",
            }}
            className="container">
              {/* <div className="row first-section">
                <div class="col-md-5 left">
                  <div className="image-section lib">
                    <h4>Library</h4>
                    <p>
                      Intuitive graphic design with <br />
                      advanced images and animation <br />
                      library
                    </p>
                  </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5 right">
                  <div className="image-section netw">
                    <h4>Network</h4>
                    <p>
                      Easy network setup with more than
                      <br />
                      500 communication drivers
                      <br />
                    </p>
                  </div>
                </div>
            
            
              </div>

              <div className="row">
                <div class="col-md-5 left">
                  <div className="image-section odbc">
                    <h4>ODBC</h4>
                    <p>
                      Supports ODBC to connect to <br />
                      universal database system
                      <br />
                    </p>
                  </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5 right">
                  <div className="image-section redun">
                    <h4>Redundancy and Server-Client</h4>
                    <p>
                      Reliable system with redundancy <br />
                      and server-client architecture
                      <br />
                    </p>
                  </div>
                </div>
              </div>
          */}
         
            </div>
          </div>

          <div className="dwn-question" data-aos="fade-up"  data-aos-duration="500">
            <div className="container"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section8
                    ? this.state.content.section8
                    : "",
                }}
            
            >
{/*              
              <div className="row">
                <div class="col-md-5">
                  <div className="content-section">
                    <h3>Have A Question?</h3>
                    <p>
                      You can always contact our sales team or technical support
                      team to answer all your questions
                    </p>
                    <div className="qustn-btns">
                      <a href="/contact" className="contact-sale">Contact Sales</a>
                      <br />
                      <a href="/tech-support" className="tech-sup">Tech Support</a>
                      <br />
                      <a href="/faq" className="faq">FAQ</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div className="img-section">
                    <img
                      className="img-fluid"
                      alt=""
                      src={images(`./frontend/chat_image.png`)}
                    ></img>
                  </div>
                </div>
           
           
              </div>
           */}
          
            </div>
          </div>
        </div>
        <SubFooter />
        <Footer />
      </React.Fragment>
    );
  }
}

export default Download;
