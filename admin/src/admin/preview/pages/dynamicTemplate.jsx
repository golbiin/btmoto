import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import { Container, Row, Col } from "react-bootstrap";
import SubFooter from "./../../../frontend/common/subFooter";
import * as pageService from "../../../ApiServices/page";
import { Helmet } from "react-helmet";
import "./../../../services/aos";
import SplashBanner from "./../../../frontend/common/splashBanner";
import { bounce } from "react-animations";
import Radium, { StyleRoot } from "radium";
import { Link } from "react-router-dom";
import VideoV1 from "./../../../frontend/common/video-v1";
const styles = {
  bounce: {
    animation: "x 1s",
    animationName: Radium.keyframes(bounce, "bounce")
  }
};
class DynamicTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      jobSearchData: {
        title: "START YOUR JOB SEARCH NOW!"
      },
      getJobsDet: [],
      status: "",
      categoryData: [
        {
          title: "CAREER",
          description:
            "Lorem ipsum dolor sit , consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat .",
          image: "careerbanner.png"
        }
      ],

      productBdata: {
        title: "",
        subtitle: "",
        backgroundImage:
          "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605735774238_5123.jpg",
        mainContent:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat."
      },

      preview: {
        editor_content: []

      },
    
      page_title: "",
      meta_description: "",
      meta_tag: "",
      animationStyle: {
        red: {},
        purple: {}
      }
    };
  }

  /* Get Page Contents */
  getContent = async slug => {
    // 
    const banner_content = [];
    const common_silder = [];
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        /* Banner-Slider Contents */
      
        if (response.data.data.content.bannerSlider.length > 0) {
       
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {};
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.subtitle = banner.subheading ? banner.subheading : "";
            Setdata.backgroundImage = banner.image ? banner.image : "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605735774238_5123.jpg";
            banner_content.push(Setdata);
          });

          this.setState({
            productBdata: banner_content[0],
          });
         
        } else {
          const Setdata = {};
          Setdata.title = response.data.data.page_title ? response.data.data.page_title : "";
          Setdata.subtitle =   "";
          Setdata.backgroundImage = "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605735774238_5123.jpg";
          banner_content.push(Setdata);
          this.setState({
            productBdata: banner_content[0],
          });
        }



        this.setState({ preview: response.data.data.preview });
        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  componentDidMount = async () => {
    const slug = this.props.slug;
    this.getContent(slug);
  };

  addAnimation = color => {
    let animationStyle = { ...this.state.animationStyle };
    if (color === "red") animationStyle.red = styles.bounce;
    else if (color === "purple") animationStyle.purple = styles.bounce;

    this.setState({ animationStyle });
  };
  removeAnimation = () => {
    this.setState({ animationStyle: {} });
  };
  render() {
    const {
      categoryData,
      careerSliderImages,
      jobSearchData,
      page_title,
      meta_description,
      meta_tag,
      productBdata
    } = this.state;
    const { editor_content } = this.state.preview;
    const slug = this.props.match.params.page;
    const images = require.context("./../../../assets/images", true);
    console.log('editor_content', this.state.preview.videourl);
    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <Header />
        <div className="container-fluid">
          <div id="dynamicpage">
            <SplashBanner slug={slug} bannerInfo={productBdata} />

            {this.state.preview.videourl ?
                        <VideoV1
                        section1=""
                        section1_sub=""
                        videourl={
                          this.state.preview.videourl
                            ? this.state.preview.videourl
                            : "cimon.mp4"
                        }
                       
                        videotype={this.state.preview.video_type}
                      /> : ''
              }


            <div className="dynamicpagecontent">
            {

              editor_content.length ?
                editor_content.map((content, index) => {
                  return (

                    <Container
                      className={slug}
                      data-aos="fade-up"
                      data-aos-duration="2000"

                      dangerouslySetInnerHTML={{
                        __html: content
                          ? content
                          : ""
                      }}
                    >
                    </Container>
                  );
                }) : ''



            }

            </div>


            <SubFooter />
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default DynamicTemplate;
