import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import TitleBanner from "./../../../frontend/common/titleBanner";
import JobSearch from "./../../../frontend/common/jobSearch";
import Footer from "./../../../frontend/common/footer";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import SubFooter from "./../../../frontend/common/subFooter";
import Moment from "moment";
import BlockUi from "react-block-ui";
import * as careerService from "./../../../ApiServices/careers";
import * as pageService from "./../../../ApiServices/page";
import { Helmet } from "react-helmet";

class CareerCenter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      titleData: [
        {
          title: "CAREER CENTER",
          image: "career-center-back.jpg"
        }
      ],
      jobSearchData: {
        title: "NEW JOB SEARCH",
        job_key: "",
        country: ""
      },
      content: {},
      getJobsDet: [],
      message: "",
      status: "",
      errors: {},
      page_title: ""
    };
    this.myRef = React.createRef();
  }

  /* Get Page Contents */
  getContent = async slug => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        /* Banner-Slider Contents */
        if (response.data.data.content.bannerSlider) {
          const titleData = [...this.state.titleData];
          titleData[0]["title"] = response.data.data.content.bannerSlider[0]
            .heading
            ? response.data.data.content.bannerSlider[0].heading
            : "";
          titleData[0]["image"] = response.data.data.content.bannerSlider[0]
            .image
            ? response.data.data.content.bannerSlider[0].image
            : "career-center-back.jpg";
          this.setState({
            titleData: titleData
          });
        }
        const jobSearchData = { ...this.state.jobSearchData };
        jobSearchData.title = response.data.data.content.title1
          ? response.data.data.content.title1
          : "NEW JOB SEARCH";
        this.setState({
          content: response.data.data.content,
          jobSearchData: jobSearchData
        });
        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  componentDidMount = async () => {
    const slug = this.props.slug;
    this.getContent(slug);
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    let jobSearchDataNew =
      this.props.location.jobSearchData !== undefined
        ? this.props.location.jobSearchData
        : "";
    if (jobSearchDataNew) {
      const jobSearchData = { ...this.state.jobSearchData };
      jobSearchData["job_key"] = jobSearchDataNew.job_key;
      jobSearchData["country"] = jobSearchDataNew.country;
      this.setState({ jobSearchData });
      setTimeout(() => this.searchJobs(jobSearchData), 100);
    } else {
      this.getJobs();
    }
  };

  /* List All Jobs */
  getJobs = async () => {
    this.setState({ blocking: true });
    try {
      const response = await careerService.getJobs();
      if (response.status === 200) {
        if (response.data.jobDetails.length > 0) {
          this.setState({ getJobsDet: response.data.jobDetails });
          this.setState({
            message: response.data.message,
            status: response.data.status
          });
          this.setState({ blocking: false });
        }
      } else {
        this.setState({ blocking: false });
        this.setState({
          message: response.data.message,
          status: response.data.status
        });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  /* Search Jobs */
  searchJobs = async (data = "") => {
    this.setState({ blocking: true });
    let jobSearchData = { ...this.state.jobSearchData };
    if (data) jobSearchData = data;
    try {
      const response = await careerService.searchJobs(jobSearchData);
      if (response.data.status === 1) {
        window.scrollTo({ top: 500, left: 0, behavior: "smooth" });
        this.setState({ getJobsDet: response.data.singleDetails });
        this.setState({ blocking: false });
        this.setState({
          message: response.data.message,
          status: response.data.status
        });
      } else {
        this.setState({ blocking: false });
        this.setState({
          message: response.data.message,
          status: response.data.status
        });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  render() {
    const {
      titleData,
      jobSearchData,
      getJobsDet,
      errors,
      page_title
    } = this.state;
    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
        </Helmet>
        <Header />
        <div className="career-center-page-wrapper">
          <TitleBanner bannerInfo={titleData} />
          <JobSearch
            jobSearch={jobSearchData}
            handleChange={this.handleChange}
            searchJobs={this.searchJobs}
            propsnew={this.props}
            errors={errors}
          />
          <div className="main-content">
            <Container ref={this.myRef}>
              <div className="title-section">
                <h2>
                  {this.state.content.title2
                    ? this.state.content.title2
                    : "Result"}
                </h2>
              </div>
              <Row>
                <Col>
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <table>
                      <thead>
                        <tr>
                          <th scope="col">
                            {" "}
                            {this.state.content.title3
                              ? this.state.content.title3
                              : "Job Name"}
                          </th>
                          <th scope="col">
                            {this.state.content.title4
                              ? this.state.content.title4
                              : "Job Type"}
                          </th>
                          <th scope="col">
                            {this.state.content.title5
                              ? this.state.content.title5
                              : "Location"}
                          </th>
                          <th scope="col">
                            {this.state.content.title6
                              ? this.state.content.title6
                              : "Post Date"}
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.status === 0 ? (
                          <tr>
                            <td colSpan="4">
                              <div className="tableinformation">
                                <div className="error">
                                  <p>{this.state.message}</p>
                                </div>
                              </div>
                            </td>
                          </tr>
                        ) : (
                          getJobsDet.map((jobs, index) => {
                            return (
                              <tr key={index}>
                                <td data-label="Job Name">
                                  <Link to={"/career-center/" + jobs.slug}>
                                    {jobs.job_name}
                                  </Link>
                                </td>
                                <td data-label="Job Type"> {jobs.job_type}</td>
                                <td data-label="Location"> {jobs.location}</td>
                                <td data-label="Post Date">
                                  {Moment(jobs.post_date).format("MM-DD-YYYY")}
                                </td>
                              </tr>
                            );
                          })
                        )}
                      </tbody>
                    </table>
                  </BlockUi>
                </Col>
              </Row>
            </Container>
          </div>
          <SubFooter />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

export default CareerCenter;
