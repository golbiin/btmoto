import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import { Container, Row, Col } from "react-bootstrap";
import Banner from "./../../../frontend/common/banner";
import SubFooter from "./../../../frontend/common/subFooter";
import JobSearch from "./../../../frontend/common/jobSearch";
import CareerSlider from "./../../../frontend/common/careerSlider";
import * as pageService from "./../../../ApiServices/page";
import { Helmet } from "react-helmet";
import "./../../../services/aos";
import SplashBanner from "./../../../frontend/common/splashBanner";
import { bounce } from "react-animations";
import Radium, { StyleRoot } from "radium";
import { Link } from "react-router-dom";
const styles = {
  bounce: {
    animation: "x 1s",
    animationName: Radium.keyframes(bounce, "bounce")
  }
};
class Careers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      jobSearchData: {
        title: "START YOUR JOB SEARCH NOW!"
      },
      getJobsDet: [],
      status: "",
      categoryData: [
        {
          title: "CAREER",
          description:
            "Lorem ipsum dolor sit , consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat .",
          image: "careerbanner.png"
        }
      ],

      productBdata: {
        title: "CAREER",
        subtitle: "",
        backgroundImage:
          "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605735774238_5123.jpg",
        mainContent:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat."
      },

      content: {},
      careerSliderImages: [
        {
          id: 1,
          image: "careersllider1.png"
        },
        {
          id: 2,
          image: "careersllider2.png"
        },
        {
          id: 3,
          image: "careersllider3.png"
        },
        {
          id: 4,
          image: "careersllider4.png"
        },
        {
          id: 5,
          image: "careersllider1.png"
        },
        {
          id: 6,
          image: "careersllider2.png"
        },
        {
          id: 7,
          image: "careersllider3.png"
        }
      ],
      page_title: "",
      meta_description: "",
      meta_tag: "",
      animationStyle: {
        red: {},
        purple: {}
      }
    };
  }

  /* Get Page Contents */
  getContent = async slug => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        /* Banner-Slider Contents */
        if (response.data.data.content.bannerSlider) {
          const banner_content = [];
          const common_silder = [];
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {};
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.image = banner.image ? banner.image : "careerbanner.png";
            banner_content.push(Setdata);
          });
          response.data.data.content.commonSilder.map((banner, index) => {
            const Setdata = {};
            Setdata.id = index + 1;
            Setdata.image = banner.image ? banner.image : "careersllider1.png";
            common_silder.push(Setdata);
          });
          this.setState({
            categoryData: banner_content,
            careerSliderImages: common_silder
          });
        }
        console.log("response.data.data.content", response.data.data.content);
        this.setState({ content: response.data.data.content });
        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  componentDidMount = async () => {
    const slug = this.props.slug;
    this.getContent(slug);
  };

  addAnimation = color => {
    let animationStyle = { ...this.state.animationStyle };
    if (color === "red") animationStyle.red = styles.bounce;
    else if (color === "purple") animationStyle.purple = styles.bounce;

    this.setState({ animationStyle });
  };
  removeAnimation = () => {
    this.setState({ animationStyle: {} });
  };
  render() {
    const {
      categoryData,
      careerSliderImages,
      jobSearchData,
      page_title,
      meta_description,
      meta_tag,
      productBdata
    } = this.state;
    const images = require.context("./../../../assets/images", true);
    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <Header />
        <div className="container-fluid">
          <div id="cimonCareers">
            <SplashBanner slug="" bannerInfo={productBdata} />
            {/* <Banner categoryInfo={categoryData} /> */}
            <Container
              className="careers"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <div className="career-title-section">
                <StyleRoot>
                  <div
                    className="pin-red"
                    style={this.state.animationStyle.red}
                    onMouseOver={() => this.addAnimation("red")}
                    onMouseLeave={this.removeAnimation}
                  >
                    <img
                      className="img-fluid"
                      src={images(`./frontend/pin-red.png`)}
                    />
                  </div>
                  <div
                    className="pin-purple"
                    style={this.state.animationStyle.purple}
                    onMouseOver={() => this.addAnimation("purple")}
                    onMouseLeave={this.removeAnimation}
                  >
                    <img
                      className="img-fluid"
                      src={images(`./frontend/pin-purple.png`)}
                    />
                  </div>
                </StyleRoot>
                <Row className="justify-content-center">
                  <Col
                    md={12}
                    className="text-center"
                    dangerouslySetInnerHTML={{
                      __html: this.state.content.section1
                        ? this.state.content.section1
                        : ""
                    }}
                  ></Col>
                </Row>
              </div>
            </Container>
            {/* <div className="search-section1" data-aos="fade-up"  data-aos-duration="2000">
              <JobSearch
                jobSearch={jobSearchData}
                handleChange={this.handleChange}
                propsnew={this.props}
              />
            </div> */}

            <div
              className="career-section2"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <Container className="">
                <div className="container max_width_1080">
                  <Row
                    className="justify-content-md-center"
                    dangerouslySetInnerHTML={{
                      __html: this.state.content.section2
                        ? this.state.content.section2
                        : ""
                    }}
                  >
                    {/* <div className="col-md-6 first">
                      <h2>INNOVATION,<br></br>
                    CONTROL,<br></br>
                    EFFICIENCY</h2>
                    </div>
                    <div className="col-md-6"><p>The workers of the future will only be competitive if they
                    work smart. Automation does not mean that humans are
                    becoming obsolete. Repetitive, dangerous tasks are now
                    outsourced to artificial intelligence. The workers focus on
advanced jobs that program and control the AI.</p></div> */}
                  </Row>
                </div>
              </Container>
            </div>

            <div
              className="career-section3"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <Container className="">
                <div className="container max_width_1080">
                  <Row className="justify-content-md-center">
                    <div className="col-md-8">
                      <div className="justify-content-left">
                        <h4>Growth Mindset is part of our DNA</h4>
                        <p>
                          For over 19 years, CIMON has been working to supply
                          industrial automation to a variety of fields. CIMON is
                          focused on leading the Internet of Things (IoT)
                          industrial revolution.
                        </p>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605805651520_21252.jpg"
                        className="img-fluid"
                        alt="2020_10_1605800992806_21252"
                      />
                    </div>
                  </Row>

                  <Row className="orange_border_section">
                    <div className="col-md-4">
                      <div className="orange_box">
                        <h2>Rise To The Challenge</h2>
                      </div>
                    </div>
                    <div className="col-md-8">
                      <div className="orange-box-right">
                        <p>
                          Explore your career opportunities with CIMON today!
                        </p>
                        <Link className="blue-btn" to="/career-center/">
                          VIEW OPENINGS
                        </Link>
                        <ul>
                          <li>
                            <a
                              target="_blank"
                              href="https://www.linkedin.com/company/cimoninc"
                            >
                              <img src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605804599165_Group%201494.png" />
                            </a>
                          </li>
                          <li>
                            <a
                              target="_blank"
                              href="https://www.glassdoor.com/Overview/Working-at-CIMON-EI_IE1253201.11,16.htm"
                            >
                              <img src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605804658754_Group%201498.png" />
                            </a>
                          </li>
                          <li>
                            <a
                              target="_blank"
                              href="https://www.indeed.com/cmp/Cimon-Inc.?attributionid=adwebapp"
                            >
                              <img src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605804714435_Group%201500.png" />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </Row>

                  <Row className="justify-content-md-center">
                    <div className="col-md-4">
                      <img
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605805502352_successful-business-partners-with-young-man-and-woman-posing-back-to-back.jpg"
                        className="img-fluid"
                        alt="2020_10_1605800992806_21252"
                      />
                    </div>
                    <div className="col-md-8">
                      <div className="justify-content-right">
                        <h4>Leading The Industry 4.0 Revolution</h4>
                        <p>
                          The workers of the future will only be competitive if
                          they work smart. Automation does not mean that humans
                          are becoming obsolete. Repetitive, dangerous tasks are
                          now outsourced to artificial intelligence.{" "}
                        </p>
                      </div>
                    </div>
                  </Row>
                </div>
              </Container>
            </div>

            {/* <div className="title-image-wrapper" data-aos="fade-up" data-aos-duration="2000">
              <Container className="careers">
                <Row
                  className="justify-content-md-center"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section2
                      ? this.state.content.section2
                      : "",
                  }}
                ></Row>
              </Container>
            </div>
            <div className="blog-wrapper" data-aos="fade-up" data-aos-duration="2000">
              <Container className="careers">
                <Row
                  className="justify-content-md-between justify-content-sm-around "
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section3
                      ? this.state.content.section3
                      : "",
                  }}
                ></Row>
              </Container>
            </div> 
            <div className="image-title-wrapper" data-aos="fade-up" data-aos-duration="2000">
              <Container className="careers">
                <Row
                  className="justify-content-md-between justify-content-sm-center"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section4
                      ? this.state.content.section4
                      : "",
                  }}
                ></Row>
              </Container>
            </div>*/}

            <div
              className="career_join_us"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <Container className="max_width_1080">
                <div
                  className="title-sec text-center"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section5
                      ? this.state.content.section5
                      : ""
                  }}
                ></div>
                <Row
                  className="justify-content-between"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section6
                      ? this.state.content.section6
                      : ""
                  }}
                ></Row>
              </Container>
            </div>

            <div
              className="career-lifeatcimon"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <div className="container-fluid">
                <div className="justify-content-md-center">
                  <div className="">
                    <h2>LIFE AT CIMON</h2>
                    <div className="gallery-full">
                      <div
                        dangerouslySetInnerHTML={{
                          __html: this.state.content.section3
                            ? this.state.content.section3
                            : ""
                        }}
                        className="section1"
                      >
                        {/* <div className="half">
                          <img className="img-fluid" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605807556000_tall-african-manager-with-big-smile-holding-thumbs-up-looking-away-charming-girls-working-as-managers-posing-office-table-with-laptops-it-laughing.jpg" alt="gallery 1" />
                          
                          </div>
                        <div className="half">   <div className="colum1">
                          <div className="content values">
                            <p>“Get busy living or get busy dying.”</p>
                            <p className="author_content">-Stephen King <span>Sales Manager</span></p>
                          </div>
                        </div>
                          <div className="colum2">
                           
                          
                          </div></div>
                   
                    */}
                      </div>
                      <div
                        dangerouslySetInnerHTML={{
                          __html: this.state.content.section4
                            ? this.state.content.section4
                            : ""
                        }}
                        className="section2"
                      >
                        {/* <div className="onebythree">  
                      <div className="colum2">
                          
                          </div>
                       <div className="colum1">
                          <div className="content values">
                            <p>“The first step toward success is taken when you refuse to be a captive of the environment in which you first find yourself.”</p>
                            <p className="author_content">-Mark caine <span>Sales Manager</span></p>
                          </div>
                        </div>
                         
                        </div>


                        <div className="onebythree-half">  
                     
                       <div className="colum1">
                          <div className="content values">
                            <p>“All the world’s a stage, and all the men and women merely players: they have their exits and their entrances; and one man in his time plays many parts, his acts being seven ages.”</p>
                            <p className="author_content">-William Shakespeare <span>sales manager</span></p>
                          </div>
                        </div>
                         
                        </div>
                  
                        <div className="onebythree-last">  

                        </div> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="simple-slider">
              <CareerSlider
                title={
                  this.state.content.section7 ? this.state.content.section7 : ""
                }
                careerSliderInfo={careerSliderImages}
              />
            </div>
            <SubFooter />
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default Careers;
