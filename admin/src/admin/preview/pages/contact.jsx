import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import SubFooter from "./../../../frontend/common/subFooter";
import Joi from "joi-browser";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import { CountryDropdown } from "react-country-region-selector";
import * as contactService from "./../../../ApiServices/contact";
import * as pageService from "./../../../ApiServices/page";
import { Helmet } from "react-helmet";
import SplashBanner from "./../../../frontend/common/splashBanner";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: this.options,
      value: null,
      blocking: false,
      showingInfoWindow: false,
      activeMarker: {},
      phonenumber: "",
      selectedPlace: { name: "in" },
      bannercontent: {
        heading: "",
        subheading: "",
        image: ""
      },
      productBdata: {
        title: "Contact Us",
        subtitle: "",
        backgroundImage: "",
        mainContent: ""
      },
      content: {},
      data: {
        fullname: "",
        country: "",
        phone: "",
        email: "",
        comments: this.props.match.params.product_name
      },
      errors: {},
      contactData: [],
      page_title: "",
      meta_description: "",
      meta_tag: ""
    };
  }
  /* Joi validation schema */
  schema = {
    fullname: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your Full Name."
        };
      }),
    country: Joi.optional()
      .label("country")
      .error(() => {
        return {
          message: "Please select your Country."
        };
      }),
    phone: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your Phone Number."
        };
      }),
    email: Joi.string()
      .required()
      .email()
      .error(() => {
        return {
          message: "Please enter your Email Address."
        };
      }),
    comments: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your Comments."
        };
      })
  };
  componentDidMount = () => {
    const slug = this.props.slug;
    this.getcontact();
    this.getContent(slug);
  };

  /* Banner Section */
  getContent = async slug => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        if (response.data.data.content.bannerSlider) {
          const Setdata = {};

          response.data.data.content.bannerSlider.map((banner, index) => {
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
          });

          const productBdata = { ...this.state.productBdata };
          productBdata["backgroundImage"] = response.data.data.content
            .bannerSlider[0].image
            ? response.data.data.content.bannerSlider[0].image
            : "https://cimon-rt.s3.amazonaws.com/upload/2020/10/1604424922648-business-team-contact-us-helpdesk-internet-concept.png";
          console.log("productBdata", productBdata);
          this.setState({
            productBdata: productBdata,
            bannercontent: Setdata
          });
        }
        this.setState({ content: response.data.data.content });
        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };
  /* Get all Contacts */
  getcontact = async () => {
    this.setState({ blocking: true });
    try {
      const response = await contactService.getcontact();
      if (response.status === 200) {
        if (response.data.contact.length > 0) {
          this.setState({ contactData: response.data.contact });
        }
      }
      this.setState({ blocking: false });
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  saveContact = async e => {
    return;
    e.preventDefault();
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    // data['phone'] = this.state.phonenumber;
    // this.setState({ data : data });
    if (data.country === "") {
      errors.country = " Please select your Country.";
      this.setState({ errors });
    } else {
      delete errors.country;
      this.setState({ errors });
    }
    console.log("data is ", this.state.data);
    let options = { abortEarly: false };
    let result = Joi.validate(data, this.schema, options);
    if (result.error) {
      result.error.details.map(error => {
        let path = error.path[0];
        let errormessage = error.message;
        errors[path] = errormessage;
      });
      this.setState({ errors });
    } else {
      const category_data = {
        fullname: this.state.data.fullname,
        country: this.state.data.country,
        phone: this.state.data.phone,
        email: this.state.data.email,
        comments: this.state.data.comments,
        id: this.state.data.id
      };
      try {
        this.setState({
          submit_status: true,
          message: "",
          message_type: ""
        });
        const response = await contactService.createContact(category_data);
        if (response) {
          this.setState({ submit_status: false });
          if (response.data.status == 1) {
            setTimeout(() => {
              this.setState(prevState => ({
                data: {
                  ...prevState.data,
                  fullname: " ",
                  country: " ",
                  phone: " ",
                  email: " ",
                  comments: " "
                }
              }));
            }, 2000);
            this.setState({
              submit_status: true,
              message: "Your Subscribtion Succesfully Completed",
              message_type: "success"
            });
          } else {
            this.setState({
              message: response.data.message,
              message_type: "error"
            });
          }
        } else {
          this.setState({ submit_status: false });
          this.setState({
            message: "Something went wrong! Please try after some time",
            message_type: "error"
          });
        }

        setTimeout(() => {
          this.setState(() => ({ message: "" }));
        }, 2000);
      } catch (err) {}
    }
  };
  /*Input handle Change */
  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  /*Joi Validation Call*/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Google Map */
  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });
  onMapClicked = props => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };

  selectCountry(val) {
    let data = { ...this.state.data };
    let errors = { ...this.state.errors };
    data["country"] = val;
    if (data.country === "") {
      errors.country = " Country is required";
    } else {
      delete errors.country;
    }
    this.setState({ data, errors });
  }

  validateNumber(value, data, event, formattedValue) {
    console.log(
      "value, data, event, formattedValue",
      value,
      data,
      event,
      formattedValue
    );
    let dataCopy = { ...this.state.data };
    dataCopy["phone"] = value;
    this.setState({ data: dataCopy });
  }

  render() {
    const { country } = this.state.data;
    const contactData = this.state.contactData;
    const { page_title, meta_description, meta_tag, productBdata } = this.state;
    console.log("handleChangePhone", this.state.data);
    //   let Background =  this.state.bannercontent.image ? this.state.bannercontent.image : 'https://cimon-rt.s3.amazonaws.com/video/2020/7/1598635117402-contact-bg.ef0e5df9.jpg'
    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <Header />
        <div id="contact-us" className="container-fluid">
          <SplashBanner bannerInfo={productBdata} />
          {/* <div className="contact-banner">
              <div className="cnt_banner_bg" style={{backgroundImage: "url(" + Background + ")"}}>
                  <div className="abg"></div>
                  <div className="container">
                  <div className="cnthedng">
                  <h1>
                  {this.state.bannercontent.title ? this.state.bannercontent.title :'CONTACT'}
                  </h1>
                </div>
              </div>
            </div>
          </div>
        */}
          <div id="contact-section">
            <div className="container max_width_1080">
              <div className="row form-section">
                <div className="col-md-7 contact-form-section">
                  <div
                    dangerouslySetInnerHTML={{
                      __html: this.state.content.section1
                        ? this.state.content.section1
                        : ""
                    }}
                  ></div>
                  <div className="contact-form">
                    <div className="form-group">
                      <label>Full Name </label>
                      <input
                        type="text"
                        name="fullname"
                        value={this.state.data.fullname}
                        className="form-control"
                        onChange={this.handleChange}
                      ></input>
                      {this.state.errors.fullname ? (
                        <div className="danger">
                          {this.state.errors.fullname}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="form-group">
                      <label>Country </label>
                      <CountryDropdown
                        className="form-control"
                        value={country}
                        onChange={val => this.selectCountry(val)}
                      />
                      {this.state.errors.country ? (
                        <div className="danger">
                          {this.state.errors.country}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="form-group">
                      <label>Phone </label>

                      <PhoneInput
                        country={"us"}
                        // value={this.state.data.phone}
                        //  onlyCountries ={['us' , 'ca']}
                        defaultCountry="us"
                        value={this.state.data.phone}
                        defaultMask="........"
                        //   disableCountryCode ={'true'}
                        placeholder={""}
                        //     onChange={phonenumber =>  this.setState({ phonenumber })}
                        onChange={e => this.validateNumber(e)}
                      />
                      {/* <input type="text" name="phone"
                            value={this.state.data.phone}
                           // onChange={this.handleChange} 
                            className="form-control"></input> */}
                      {this.state.data.phone == "" ? (
                        <div className="danger">{this.state.errors.phone}</div>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="form-group">
                      <label>Email </label>
                      <input
                        type="email"
                        name="email"
                        value={this.state.data.email}
                        onChange={this.handleChange}
                        className="form-control"
                      ></input>
                      {this.state.errors.email ? (
                        <div className="danger">{this.state.errors.email}</div>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="form-group ">
                      <label>Comments </label>
                      <textarea
                        className="textarea form-control comments"
                        name="comments"
                        value={this.state.data.comments}
                        onChange={this.handleChange}
                      ></textarea>

                      {this.state.errors.comments ? (
                        <div className="danger">
                          {this.state.errors.comments}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="btn-submit">
                      <button
                        type="submit"
                        onClick={this.saveContact}
                        className="btn blue-btn"
                      >
                        {" "}
                        Submit{" "}
                      </button>
                    </div>
                    <div className="msg">
                      {this.state.submit_status === true
                        ? this.state.message
                        : ""}
                    </div>
                  </div>
                </div>
                <div className="col-md-5 contact-address">
                  <h2>Address</h2>
                  {contactData.map((data, index) => {
                    return (
                      <>
                        <div className={"address-heading p_" + index}>
                          <h4>{data.name}</h4>
                          <p>{data.address1},</p>
                          <p>{data.address2}</p>
                          {data.phone !== "" ? (
                            <p
                              className="tele_contact
                      "
                            >
                              <b>Tel:</b>{" "}
                              <a href={"tel:" + data.phone}>{data.phone}</a>
                            </p>
                          ) : (
                            <p></p>
                          )}
                          {/* { data.email !== '' ?
                        <p>Email: <a href={"mailto:" + data.email }>{data.email}</a>
                        </p> : <p></p> } */}
                        </div>
                      </>
                    );
                  })}
                  <div className="googlemap">
                    <Map
                      google={this.props.google}
                      className={"map"}
                      initialCenter={{
                        lat: this.state.content.latitude
                          ? this.state.content.latitude
                          : 36.0051281,
                        lng: this.state.content.longitude
                          ? this.state.content.longitude
                          : -115.093628
                      }}
                      zoom={14}
                      onClick={this.onMapClicked}
                    >
                      <Marker
                        onClick={this.onMarkerClick}
                        title={this.state.content.tooltip}
                        name={
                          this.state.content.companyname
                            ? this.state.content.companyname
                            : "Cimon Inc"
                        }
                        position={{
                          lat: this.state.content.latitude
                            ? this.state.content.latitude
                            : 36.0051281,
                          lng: this.state.content.longitude
                            ? this.state.content.longitude
                            : -115.093628
                        }}
                      />
                      <InfoWindow
                        marker={this.state.activeMarker}
                        visible={this.state.showingInfoWindow}
                      >
                        <div className="map-locate">
                          <h5>{this.state.selectedPlace.name}</h5>
                        </div>
                      </InfoWindow>
                    </Map>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <SubFooter />
        <Footer />
      </React.Fragment>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyDeYCp70l3DM3af-VzfqQBLSS1ud3jsC8c"
})(Contact);
