import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col, Media } from 'react-bootstrap';
import Moment from 'react-moment';

import BlockUi from 'react-block-ui';
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import SubFooter from "./../../../frontend/common/subFooter";
import SplashBanner from './../../../frontend/common/splashBanner'
import { Helmet } from 'react-helmet';
import * as pageService from "./../../../ApiServices/page";
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
const TITLE = 'Latest Articles | Automation Company - Industrial Automation';
class Certificates extends Component {
	constructor(...args) {
		super(...args);
		this.state = {

			bannerData: {
				title: "PRODUCT CERTIFICATION",
				subtitle: "",
				backgroundImage: "https://cimon-rt.s3.amazonaws.com/profile_cimon/1612561852525-handshake-businessmen%402x.png",
				mainContent: ""
			},
			activeId : '',
			content : {},
			preview : {},
			tabattributes : [],
			page_title: 'Product Certification | Automation Company - Industrial Automation',
			meta_tag: 'Product Certification',
			meta_description: 'Product Certification'

		}
	}

	componentDidMount() {
		this.getPageContents();
	}
	 
/* Get Page Contents */
getPageContents = async () => {
	const slug = this.props.slug;
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        if (response.data.data.preview.bannerSlider) {
          const banner_content = [];
          response.data.data.preview.bannerSlider.map((banner, index) => {
            const Setdata = {};
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.backgroundImage = banner.image
              ? banner.image
              : "/static/media/about.034a1af3.jpg";
            banner_content.push(Setdata);
          });

    

          this.setState({
            bannerData: banner_content[0]
          });
        }
        this.setState({ preview: response.data.data.preview });
		this.setState({ tabattributes: response.data.data.preview.tabattributes });
        if (response.data.data.preview.page_title) {
          this.setState({
            page_title: response.data.data.preview.page_title
          });
        }
        if (response.data.data.preview.meta_tag) {
          this.setState({
            meta_tag: response.data.data.preview.meta_tag
          });
        }
        if (response.data.data.preview.meta_desc) {
          this.setState({
            meta_description: response.data.data.preview.meta_desc
          });
        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };



  toggleActive = async (id) => {
 
	
	const height = document.getElementById('activeid' + id).clientHeight;
	if(!height){
		this.setState({ activeId: id });
	} else {
		this.setState({ activeId: '' });
	}
  
  }
	render() {
		const { bannerData, newsData } = this.state;
		const { page_title, meta_description, meta_tag ,tabattributes, content , preview ,activeId } = this.state;
 
		var newsImg;

		console.log('currentPage', preview.section1)
		return (
			<React.Fragment>
				<Helmet>
					<title>{page_title ? page_title : " "}</title>
					<meta
						name="description"
						content={meta_description ? meta_description : " "}
					></meta>
					<meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
				</Helmet>
				<Header />
				<div className="certificate-wrapper">
					<SplashBanner bannerInfo={bannerData} />
					<Container
						data-aos="fade-up"
						data-aos-duration="2000"
						className="certificate-container max_width_1080">
						<div class="row">
							<div class="col-md-12">
								<div class="text-center certificatehead"
									dangerouslySetInnerHTML={{
										__html:
										preview.section1 != null
										? preview.section1
										: ""
										}}
								>
								
								</div>

								<div class="certification_accordian">
									<Accordion>


									{tabattributes.map((attr, idx) => (
                                       
											<Card>
												<Card.Header
													className={ activeId === idx ? 'active' : ''}
												>
													<Accordion.Toggle as={Button} 

onClick={this.toggleActive.bind(this, idx)}
													variant="link" eventKey={idx}>
													{attr.name}
													</Accordion.Toggle>
												</Card.Header>
											<Accordion.Collapse
											id={ 'activeid' +idx } 
											eventKey={idx}>
													<Card.Body
													dangerouslySetInnerHTML={{
													__html:
													attr.description != null
													? attr.description
													: ""
													}}
													>
													</Card.Body>
											</Accordion.Collapse>
											</Card>


                                      ))}
										
										{/* <Card>
											<Card.Header>
												<Accordion.Toggle as={Button} variant="link" eventKey="1">
													IPC Certifications
      </Accordion.Toggle>
											</Card.Header>
											<Accordion.Collapse eventKey="1">
												<Card.Body>
													<a className="certificatefile">Download the IPC Certificate File</a>
													<table> <tbody>
														 <tr> <td colspan="2" width="259">■ IPC Certificates</td> <td width="67"></td> <td width="61"></td> <td width="75"></td> <td width="75"></td> <td width="75"></td> <td width="75"></td> </tr> <tr> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>Category</td> <td>CIMON Part Number</td> <td>FCC</td> <td>UL</td> <td>CE</td> <td>RoHSⅠ</td> <td>RoHSⅡ</td> <td>KC</td> </tr> <tr> <td rowspan="25">PC</td> <td>CM-iNP/iNT210-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT210-D</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT211-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT211-D</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr>
														  <tr> <td>CM-NP/NT12-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr>
														   <tr> <td>CM-iNP/iNT12-A</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr>
														    <tr> <td>CM-iNP/iNT12-D</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr>
															<tr> <td>CM-iNP/iNT212-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT212-D</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT312-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT312-D</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-NP/NT15-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT15-A</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT15-D</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT215-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT215-D</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-NP/NT15I-A</td> <td>O</td> <td>–</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iNP/iNT15I-A</td> <td>O</td> <td>–</td> <td>O</td> <td>O</td> <td></td> <td>O</td> </tr> <tr> <td>CM-NP/NT19-A</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-NP/NT19-D</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-NP/NT219-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-NP/NT219-D</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-NU1R/1D-A</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-NU1RP-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-NU2RP</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>12</td> <td>CM-12EH</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="15">P12</td> <td>CM-P12CS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12CS/16</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12CS/32</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12DH</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12DS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12EH</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-P12ES</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12ES/32</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12FH-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12FS</td> <td>O</td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-P12FS-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12FS-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12FS-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P12FS-D24</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-P12FS-K</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="16">T12</td> <td>CM-T12CH</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12CS/16</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12DS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12EH</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-T12ES</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12ES/32</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12FS</td> <td>O</td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-T12FS-150</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12FS-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12FS-A-150</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12FS-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12FS-B-150</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12FS-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12FS-D24</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-T12FS-K</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T12GS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="28">P15</td> <td>CM-P15CP</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15CP(B)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15CP/32</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15CP/B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15CS/16</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15CS/32</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15CW</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15CW(B)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15CX</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15DH</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-P15DH/2G+320H</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15DP</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15DP/D110</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15DS</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15DS/32</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FH-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FP</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-P15FP-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FP-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FP-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FS</td> <td>O</td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-P15FS-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FS-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FS-BN</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FS-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FS-D24</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-P15FS-K</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P15FS-N</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="28">T15</td> <td>CM-T15BH</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15BH/P</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15CH</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15CP</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15CP(B)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15CS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15CS(B)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15CX</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15DH</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-T15DP</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15DS</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15DS/32</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FH-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FP</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-T15FP-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FP-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FP-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FS</td> <td>O</td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-T15FS-150</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FS-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FS-A-150</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FS-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FS-B-150</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FS-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15FS-D24</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-T15FS-K</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15GP-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T15GS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="6">P19</td> <td>CM-P19DP</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-P19FP</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-P19FP-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P19FP-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P19FP-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-P19FP-K</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="7">T19</td> <td>CM-T19DP</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-T19FP</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-T19FP-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T19FP-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T19FP-B-150</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T19FP-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-T19GP-B</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> </tbody> </table></Card.Body>
											</Accordion.Collapse>
										</Card>

										<Card>
											<Card.Header>
												<Accordion.Toggle as={Button} variant="link" eventKey="2">
													Xpanel HMI Certifications
      </Accordion.Toggle>
											</Card.Header>
											<Accordion.Collapse eventKey="2">
												<Card.Body>
													<a className="certificatefile">Download the Xpanel HMI Certificate File</a>
													<table  > <tbody> <tr> <td colspan="2" width="305">■ Xpanel Certificates</td> <td width="75"></td> <td width="75"></td> <td width="75"></td> <td width="75"></td> <td width="75"></td> <td width="75"></td> </tr> <tr> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>Category</td> <td>CIMON Part Number</td> <td>FCC</td> <td>UL</td> <td>CE</td> <td>RoHSⅠ</td> <td>RoHSⅡ</td> <td>KC</td> </tr> <tr> <td rowspan="3">XT04</td> <td>CM-XT04CD-D</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT04CD-DE</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-XT04CD-DN</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td></td> </tr> <tr> <td rowspan="8">XT07</td> <td>CM-XT07CD-AN</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-XT07CD-AE</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-XT07CD-DN (SMPS 0.1)</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-XT07CD-DN (SMPS 0.2)</td> <td>O</td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT07CD-DE (SMPS 0.1)</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-XT07CD-DE (SMPS 0.2)</td> <td>O</td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT07CD-DN-KN</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT07CD-DE-EN</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="2">XT08</td> <td>CM-XT08CD-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT08CD-D</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td rowspan="5">XT10</td> <td>CM-XT10CD-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT10CD-D</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iXT10CD-A</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-iXT10CD-D</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-XT10CD-D-M</td> <td>O</td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td rowspan="2">XT12</td> <td>CM-XT12CD-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iXT12CD-A</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td rowspan="2">XT15</td> <td>CM-XT15CD-A</td> <td>O</td> <td>O</td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-iXT15CD-A</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td rowspan="12">HP</td> <td>CM-HP07CD-DNR</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP07CD-ANR</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP07CD-DNS</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP07CD-ANS</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP07CD-DER</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP07CD-AER</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP07CD-DES</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP07CD-AES</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP-DM</td> <td></td> <td></td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP-EAA</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> <td>O</td> </tr> <tr> <td>CM-HP-EDR</td> <td>O</td> <td>O</td> <td></td> <td>O</td> <td>O</td> <td></td> </tr> <tr> <td>CM-HP-SURGE</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>old version</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>XT04</td> <td>CM-XT04CB-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td rowspan="2">XT05CB</td> <td>CM-XT05CB-A</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT05CB-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td rowspan="10">XT05MA</td> <td>CM-XT05MA-A</td> <td></td> <td>O</td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05MA-D</td> <td></td> <td>O</td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05MA-D (WIN)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05MA-A (WIN)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05MA-A (KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05MA-A(B)(KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05MA-D (KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05MA-D(B)(KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05MA-DU(KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05MA-AU(KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="2">XT05MB</td> <td>CM-XT05MB-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT05MB-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td rowspan="15">XT05SA</td> <td>CM-XT05SA-A</td> <td></td> <td>O</td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-A (NANYA)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-A (WIN)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-A (KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-A(B)(KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-AU(KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-D</td> <td></td> <td>O</td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-D (WIN)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-D (NANYA)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-D 전면부(뉴로스)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-D (KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-D(KYO)NEUROS TYPE</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-D(KYO) NEUROS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-D(B)(KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT05SA-DU(KYO)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="3">XT05SB</td> <td>CM-XT05SB-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT05SB-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT05SB-D (NEU)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="9">XT06</td> <td>CM-XT06CA-A</td> <td></td> <td>O</td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT06CA-D</td> <td></td> <td>O</td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT06CB-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT06CB-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT06CA-D (LG)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT06CA-D (Mit)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT06CA-D(B)(Mit)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT06CA-DU(Mit)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT06CB-D (BCH)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="5">XT07</td> <td>CM-XT07CB-A</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT07CB-AN</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT07CB-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT07CB-D (IPSU)</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT07CB-DN</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="6">XT10</td> <td>CM-XT10CA-A</td> <td></td> <td>O</td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT10CA-D</td> <td></td> <td>O</td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-XT10CB-A</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT10CB-D</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT10CC-A</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>CM-XT10CD-A-S</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>XT12</td> <td>CM-XT12CB-A</td> <td></td> <td></td> <td>O</td> <td></td> <td></td> <td>O</td> </tr> <tr> <td>XT15</td> <td>CM-XT15CA-A</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td rowspan="24">HP</td> <td>CM-HP07CD-DX-AER</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DX-DER</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DA-AER</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DA-DER</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-D-AER</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-D-DER</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DX-ANR</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DA-ANR</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DA-DNR</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-D-ANR</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-D-DNR</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DX-DNR</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DX-AES</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DX-DES</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DX-ANS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DX-DNS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DA-AES</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DA-DES</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DA-ANS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-DA-DNS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-D-AES</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-D-DES</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-D-ANS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> <tr> <td>CM-HP07CD-D-DNS</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr> </tbody> </table>

												</Card.Body>
											</Accordion.Collapse>
										</Card>
								 */}
								
									</Accordion>
								</div>
							</div>
						</div>
					</Container>
				</div>
				<SubFooter />
				<Footer />
			</React.Fragment >

		);
	}
}

export default Certificates;