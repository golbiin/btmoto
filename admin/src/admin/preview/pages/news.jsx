import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from "react-router-dom";
import TextTruncate from 'react-text-truncate';
import Moment from 'react-moment';
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import Banner from './../../../frontend/common/banner';
import SubFooter from './../../../frontend/common/subFooter';
import * as newsService from "./../../../ApiServices/news";
import * as pageService from "./../../../ApiServices/page";
import { Helmet } from 'react-helmet';
import SplashBanner from './../../../frontend/common/splashBanner'

class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: {},
            categoryData: [{
                "title": "CIMON NEWS",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                "image": "news-banner.jpg"
            },
            {
                "title": "CIMON NEWS",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                "image": "news-banner.jpg"
            },
            {
                "title": "CIMON NEWS",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                "image": "news-banner.jpg"
            }
            ],
            blocking: false,
            visibleRecent: 4,
            visibleArticle: 4,
            error: false,
            recentNews: [],
            latestNews: [],
            topNews: [],
            spinner: false,
            page_title: "",
            meta_tag: "",
            meta_description: "",
            pageBannerdata: {
                title: 'CIMON NEWS',
                subtitle: '',
                backgroundImage:
                    'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606924331220_Phone_reading.jpg',
                mainContent: ''
            },
        }

        https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606924331220_Phone_reading.jpg
        this.loadmoreRecent = this.loadmoreRecent.bind(this);
        this.loadmoreArticle = this.loadmoreArticle.bind(this);
        this.myRef = React.createRef();
    }
    scrollToMyRef = () => window.scrollTo(0, this.myRef.current.offsetTop);

    loadmoreRecent() {
        this.setState((prev) => {
            return { visibleRecent: prev.visibleRecent + 4 };
        });
    }

    loadmoreArticle() {
        this.setState((prev) => {
            return { visibleArticle: prev.visibleArticle + 9 };
        });
    }

    /* Get Page Contents */
    getContent = async (slug) => {
        try {
            const response = await pageService.getPageContent('news');
            if (response.data.status === 1) {
                /* Banner-Slider Contents */
                if (response.data.data.content.bannerSlider) {
                    const banner_content = [];

                    response.data.data.content.bannerSlider.map((banner, index) => {
                        const Setdata = {};
                        Setdata.title = banner.heading ? banner.heading : '';
                        Setdata.description = banner.subheading ? banner.subheading : '';
                        Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
                        banner_content.push(Setdata);
                    });
                    this.setState({
                        categoryData: banner_content,
                    });
                }
                this.setState({ content: response.data.data.content });

                if (response.data.data.page_title) {
                    this.setState({
                        page_title: response.data.data.page_title
                    });

                }
                if (response.data.data.meta_tag) {
                    this.setState({
                        meta_tag: response.data.data.meta_tag
                    });
                }
                if (response.data.data.meta_desc) {
                    this.setState({
                        meta_description: response.data.data.meta_desc
                    });
                }
            }
        } catch (err) {
            this.setState({ spinner: false });
        }
    };

    componentDidMount = () => {
        const slug = this.props.slug;
        this.getNews();
        this.getContent(slug);
    };

    /*  Get All types of News  */
    getNews = async () => {
        this.setState({ spinner: true });
        try {
            const response = await newsService.getNews();
            if (response.status === 200) {
                if (response.data.latestNews.length > 0) {
                    this.setState({ latestNews: response.data.latestNews });
                }
                if (response.data.recentNews.length > 0) {
                    this.setState({ recentNews: response.data.recentNews });
                }
                if (response.data.topNews.length > 0) {
                    this.setState({ topNews: response.data.topNews });
                }
            }
            this.setState({ spinner: false });
        } catch (err) {
            this.setState({ spinner: false });
        }
    }

    render() {
        const { categoryData, latestNews, pageBannerdata, page_title, meta_description, meta_tag } = this.state;
        const images = require.context("./../../../assets/images", true);
        let newsImg;
       // console.log('recentNews', latestNews);
        return (
            <React.Fragment>
                <Helmet>
                    <title>{page_title ? page_title : " "}</title>
                    <meta
                        name="description"
                        content={meta_description ? meta_description : " "}
                    ></meta>
                    <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
                </Helmet>
               
                <Header />
                <div className="news-page-wrapper">
                    {/* <Banner categoryInfo={ categoryData } /> */}
                    <SplashBanner bannerInfo={pageBannerdata} />

                    <div className="latest-news-wrapper">
                        <Container className="news" data-aos="fade-up" data-aos-duration="2000">
                            <h2>Latest News</h2>
                            {/*   {latestNews.map((news, index) => {
                                if (news.image) {
                                    newsImg = news.image;
                                } else {
                                    newsImg = images(`./frontend/news-placeholder1.png`);
                                }
                                return (
                                    <>
                                        <Row className="no-gutters" key={index}>
                                            <Col lg={8} className="left-section">
                                                <div className="img-box">
                                                    <img alt="News poster " src={newsImg}></img>
                                                    <div className="over-text">
                                                        <TextTruncate
                                                            line={1}
                                                            element="p"
                                                            truncateText="…"
                                                            text={news.title}
                                                        />
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col lg={4} className="right-section">
                                                <TextTruncate
                                                    line={1}
                                                    element="h4"
                                                    truncateText="…"
                                                    text={news.title}
                                                />
                                                <TextTruncate
                                                    line={8}
                                                    element="p"
                                                    truncateText="…"
                                                    text={news.description}
                                                />
                                                <Link to={"/latest-news/" + news.slug} className="read-link">
                                                    READ MORE
                                        </Link>
                                            </Col>
                                        </Row>
                                    </>
                                );
                            })} */}

                            <div className="row latestnews">

                                {(() => {
                                    let index = 0;

                                    return (
                                        <>
                                            <div className="col-md-6 img-cnt ">
                                                {this.state.topNews[0] ? (
                                                    <div className="left-icn">
                                                          <Link to={"/cimon-news/" + this.state.topNews[0].slug}>
                                                        <img className="img-fluid" src={
                                                            this.state.topNews[0].image ? this.state.topNews[0].image :
                                                                'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606925297836_Robotic%20Arm%20assembly%20line.png'} />

                                                        <div className="description">
                                                            <h4>{this.state.topNews[0].title}</h4>
                                                            <p>
                                                                <Moment format="MMMM DD, YYYY">
                                                                    {this.state.topNews[0].date}
                                                                </Moment>
                                                            </p>
                                                        </div>
                                                        </Link>
                                                    </div>) : ''}
                                            </div>
                                            <div className="col-md-6">


                                                <div className=" right-icn">
                                                    <div className="row">
                                                        {this.state.topNews[1] ? (
                                                            <div className="col-md-12 img-cnt">
                                                <Link to={"/cimon-news/" + this.state.topNews[1].slug}>
                                                                <div className="class_relative">
                                                                    <img className="img-fluid" src={
                                                                        this.state.topNews[1].image ? this.state.topNews[1].image : 'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606925398618_Factory%20image.png'}>

                                                                    </img>
                                                                    <div className="description">
                                                                        <h4>{this.state.topNews[1].title}</h4>
                                                                        <p> <Moment format="MMMM DD, YYYY">
                                                                            {this.state.topNews[1].date}
                                                                        </Moment></p>
                                                                    </div>
                                                                </div>
                                                                </Link>
                                                            </div>) : ''}

                                                        {this.state.topNews[2] ? (
                                                            
                                                            <div className="col-md-6 img-cnt">
                                                                <Link to={"/cimon-news/" + this.state.topNews[2].slug}>
                                                                <div className="class_relative">
                                                                    <img className="img-fluid" src={
                                                                        this.state.topNews[2].image ? this.state.topNews[2].image : 'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606925481043_Robotic%20Arm%20image.png'} />
                                                                    <div className="description">
                                                                        <h4>{this.state.topNews[2].title}</h4>
                                                                        <p><Moment format="MMMM DD, YYYY">
                                                                            {this.state.topNews[2].date}
                                                                        </Moment></p>
                                                                    </div>

                                                                </div>
                                                                </Link>
                                                            </div>
                                                        ) : ''}
                                                        {this.state.topNews[3] ? (
                                                            <div className="col-md-6 img-cnt">
                                                                 <Link to={"/cimon-news/" + this.state.topNews[3].slug}>
                                                                <div className="class_relative">
                                                                    <img className="img-fluid" src={
                                                                        this.state.topNews[3].image ? this.state.topNews[3].image : 'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606925531044_ship%20yard%20image.png'} />
                                                                    <div className="description">
                                                                        <h4>{this.state.topNews[3].title}</h4>
                                                                        <p><Moment format="MMMM DD, YYYY">
                                                                            {this.state.topNews[3].date}
                                                                        </Moment></p>
                                                                    </div></div>
                                                                    </Link>
                                                            </div>
                                                        ) : ''}

                                                    </div>

                                                </div>


                                            </div>


                                        </>
                                    );
                                })()}
                            </div>


                        </Container>
                    </div>
                    <div className="resently-published-wrapper">
                        <Container className="news" data-aos="fade-up" data-aos-duration="2000">
                            <h2>DON’T MISS OUT</h2>
                            <div className="row">
                                <div className="col-md-6">









                                    {latestNews.map((news, index) => {
                                        if (news.image) {
                                            newsImg = news.image;
                                        } else {
                                            newsImg = 'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606940052359_Factoy%20worker%20controlling%20control%20panel.png';
                                        }
                                        return (
                                            <>
                                              <Link to={"/cimon-news/" + news.slug}> 
                                                <div className="single_top_news">
                                                    <img class="img-fluid" src={newsImg} />
                                                    <h3>{news.title}</h3>
                                                    <p  >{news.description}</p>
                                                </div>
                                               </Link>
                                            </>
                                        );
                                    })}













                                </div>
                                <div className="col-md-6">

                                    <ul>
                                        {this.state.recentNews.slice(0, this.state.visibleRecent).map((news, index) => {

                                            let image = news.image ? news.image : 'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606925297836_Robotic%20Arm%20assembly%20line.png';
                                            return (
                                                <li key={index}>
                                                    <Link to={"/cimon-news/" + news.slug}>
                                                        <div className="image-cont">
                                                            <img className="img-fluid" src={image} />
                                                        </div>
                                                        <div className="description-cont">
                                                            <p className="title">
                                                                <Link to={"/cimon-news/" + news.slug}>
                                                                    {news.title}
                                                                </Link>
                                                            </p>
                                                            <p>
                                                                <Moment format="MMM. DD, YYYY">
                                                                    {news.date}
                                                                </Moment>
                                                            </p>

                                                        </div>
                                                    </Link>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                    <div className="load-more-box">
                                        {this.state.visibleRecent < this.state.recentNews.length &&
                                            <Link to="#" onClick={this.loadmoreRecent} >
                                                LOAD MORE
                                 </Link>
                                        }
                                    </div>

                                </div>


                            </div>

                        </Container>
                    </div>
                    {/* <div className="top-articles-wrapper">
                        <Container className="news" data-aos="fade-up" data-aos-duration="2000">
                            <h2>Top Articles</h2>
                            <Row>
                                {this.state.topNews.slice(0, this.state.visibleArticle).map((news, index) => {
                                    if (news.image) {
                                        newsImg = news.image;
                                    } else {
                                        newsImg = images(`./frontend/news-placeholder.png`);
                                    }
                                    return (
                                        <Col md={4} sm={6} className="box" key={index}>
                                            <div className="item">
                                                <Link to="#" className="img-link">
                                                    <img alt="News poster" src={newsImg}></img>
                                                </Link>
                                                <div className="content-box">
                                                    <h3>{news.title} </h3>
                                                    <TextTruncate
                                                        line={4}
                                                        element="p"
                                                        truncateText="…"
                                                        text={news.description}
                                                    />
                                                    <div className="link-box">
                                                        <Link to={"/latest-news/" + news.slug} className="read-link">
                                                            READ MORE
                                                    </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </Col>
                                    )
                                })}
                            </Row>
                            <div className="load-more-box">
                                {this.state.visibleArticle < this.state.topNews.length &&
                                    <Link to="#" onClick={this.loadmoreArticle}>LOAD MORE</Link>
                                }
                            </div>
                        </Container>
                    </div>
                   */}
                    <Container className="news ">


                        <Row className="share-row">
                            <Col>
                                <label className="followuson">Follow Us On </label>
                                <ul class="social-network social-circle">
                                    <li>
                                        <a
                                            href="https://twitter.com/CIMONsns"
                                            target="_blank"
                                            class="icoTwitter"
                                            title="Twitter"
                                        >
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a
                                            href="https://www.facebook.com/cimoninc"
                                            target="_blank"
                                            class="icoFacebook"
                                            title="Facebook"
                                        >
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>

                                    <li>
                                        <a
                                            href="https://www.instagram.com/cimon_automation/"
                                            target="_blank"
                                            class="icoInstagram"
                                            title="Instagram"
                                        >
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>

                                    <li>
                                        <a
                                            href="https://www.linkedin.com/company/cimoninc"
                                            target="_blank"
                                            class="icoLinkedin"
                                            title="Linkedin"
                                        >
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                </ul>
                            </Col>
                        </Row>

                    </Container>

                    <SubFooter />
                </div>
                <Footer />
            </React.Fragment>
        );
    }
}

export default News;