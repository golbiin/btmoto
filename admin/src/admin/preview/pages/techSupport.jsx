import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import { Container, Row, Col } from "react-bootstrap";
import SubFooter from "./../../../frontend/common/subFooter";
import poster from "./../../../assets/images/frontend/videotutplay.png";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";

import SplashBanner from "./../../../frontend/common/splashBanner";
import * as pageService from "./../../../ApiServices/page";
const TITLE = "Tech Support | Automation Company - Industrial Automation";

class TechSupport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocking: false,
      getJobsDet: [],
      status: "",
      productsBanner: "",
      productBdata: {
        title: "tech support",
        subtitle: "",
        backgroundImage:
          "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604348996929_checking-current-laptop-circuit-board.png",
        mainContent:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat."
      },
      content: {},
      page_title: "",
      meta_description: "",
      meta_tag: ""
    };
  }

  getContent = async slug => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status == 1) {
        /* Banner-Slider Contents */
        if (response.data.data.content.bannerSlider) {
          const Setdata = {};
          response.data.data.content.bannerSlider.map((banner, index) => {
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.subtitle = banner.subheading ? banner.subheading : "";
            Setdata.backgroundImage = banner.image ? banner.image : "ipc_banner.jpg";
          });
          console.log("this.homedata.imageSetdata", Setdata);
          this.setState({
            productBdata: Setdata
          });
        }
        this.setState({ content: response.data.data.content });

        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      } else {
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  componentDidMount = async () => {
    const slug = this.props.slug;
    this.getContent(slug);
  };

  render() {
    const { productBdata, page_title, meta_description, meta_tag } = this.state;
    let video = "";
    let poster =
      "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603997163334_checking-current-laptop-circuit-board.jpg";
    video = require("./../../../assets/videos/IPC_TableTopVideo_0ct4.mp4");

    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <Header />

        <div className="container-fluid">
          <SplashBanner slug="tech-support" bannerInfo={productBdata} />

          <div id="contactsupport" className="">
            <div className="container max_width_1080">
              <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section1
                    ? this.state.content.section1
                    : ""
                }}
              >
                {/* <Col className="leftcontent" md={8} sm={8}>
                                             <img className="img-fluid rightimage" alt="scada image" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603914073053_AdobeStock_155005940.jpg" />         
                                            
                                                
                                             </Col>

                                             <Col md={4} sm={4}>
                                             <div className="contact_support_para">
                                                       <h3>CONTACT SUPPORT</h3>
                                                       <p>If you need technical support, we have application engineers standing by to help you with product selection, application assistance, and product trouble shooting. </p>
                                             </div>
                                             <div class="infosection">
                                            <p> Phone: <a href="tel:702-820-1068">702-820-1068</a></p>
                                            <p>  Email: <a href="mailto:support@cimoninc.com">support@cimoninc.com</a></p>
                                            
                                             </div>
                                            </Col> */}
              </Row>
            </div>
          </div>
          <div id="faqsection" className="">
            <div className="container max_width_1080">
              <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section2
                    ? this.state.content.section2
                    : ""
                }}
              >
                {/* <Col className=" p-0" md={12} sm={12}>
                                        <h2>Frequently Asked Questions</h2>
                                        </Col>
                                             <Col className="greybackground first-col" md={5} sm={5}>
                                             <p>What is a PLC?</p>
                                             <p>How to control PID ON and OFF by digital ouput</p>
                                             <p>Difference betwwen AGND and FG for SP04ETO</p>
                                             <p>CIMON plc Link protocols via Ethernet</p>
                                             <p>XpanelDesigner Software installation Error</p>
                                             <p>How to connect Siemens S7-200 Via PPI Direct (RS485)</p>
                                             <p>Animation Editor in XpanelDesigner</p>
                                             <a href="/faq">View all Questions</a>
                                             </Col>
                                             
                                             <Col className="greybackground" md={5} sm={5}>
                                            <p> Failed to initialize COM libary module in CICON</p>
                                            <p>How does.NET Framework 3.5 work for CIMON SCADA</p>
                                            <p>How to set up Xpanel user login screen</p>
                                            <p>How and when to use Xpanel repair mode?</p>
                                            
                                            <p>How to configure Cimon view server and Client in SCADA?</p>
                                            <p>How does the Scada version compatibility work?</p>
                                            <p>
How to downgrade SCADA version for v3.03 to v2.14   </p>

                                             <a href="/faq">View all Questions</a>
                                             </Col> */}
              </Row>
            </div>
          </div>

          <div id="archivesection" className="">
            <div className="container-fluid">
              <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section3
                    ? this.state.content.section3
                    : ""
                }}
              >
                {/* <Col className="" md={6} sm={6}>
                                   <div className="archivesection_left">
                                        <h2>CIMON ARCHIVE</h2>
                                        <p>Looking for more detailed documents or user manuels? Visit our Archive page for more information.</p>
                                        <Link to="/archives">Visit Archive</Link>
                                   </div>
                                   </Col>
                                   <Col className="" md={6} sm={6}>
                                   <div className="archivesection_right">
                                        
                                        </div>
                                   </Col> */}
              </Row>
            </div>
          </div>

          <div id="videoTutorials" className="videoTutorialsnewtech">
            <Container className="title-section text-center">
              <Row className="justify-content-center">
                <Col sm={12} md={12}>
                  <h2>Video Training</h2>
                  <Link to="/video-tutorials">View all videos</Link>
                </Col>
              </Row>
            </Container>
            <div
              className="video-wrapper"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <Container className="tutorial">
                <Row className="justify-content-md-between justify-content-sm-around ">
                  <Col md={4} className="pr-md-0">
                    <div className="scada-video-box">
                      <video
                        controls
                        poster="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604359040636_image1.jpg"
                      >
                        <source src={video} type="video/mp4" />
                      </video>
                    </div>
                    <h3>Using XpanelDesigner for HMI</h3>
                  </Col>
                  <Col md={4} className="pr-md-0">
                    <div className="scada-video-box">
                      {
                        <video
                          controls
                          poster="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604359090087_image2.jpg"
                        >
                          <source src={video} type="video/mp4" />
                        </video>
                      }
                    </div>
                    <h3>Using CICON for PLC</h3>
                  </Col>
                  <Col md={4} className="pr-md-0">
                    <div className="scada-video-box">
                      {
                        <video
                          controls
                          poster="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604359207624_image3.jpg"
                        >
                          <source src={video} type="video/mp4" />
                        </video>
                      }
                    </div>
                    <h3>Using CICON for PLC</h3>
                  </Col>
                  <Col md={4} className="pr-md-0">
                    <div className="scada-video-box">
                      {
                        <video
                          controls
                          poster="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604359239753_image4.jpg"
                        >
                          <source src={video} type="video/mp4" />
                        </video>
                      }
                    </div>
                    <h3>Using UltimateAccess SCADA</h3>
                  </Col>

                  <Col md={4} className="pr-md-0">
                    <div className="scada-video-box">
                      <video
                        controls
                        poster="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604359273329_image5.jpg"
                      >
                        <source src={video} type="video/mp4" />
                      </video>
                    </div>
                    <h3>Handling Communications</h3>
                  </Col>
                  <Col md={4} className="pr-md-0">
                    <div className="scada-video-box">
                      {
                        <video
                          controls
                          poster="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604359300932_image6.jpg"
                        >
                          <source src={video} type="video/mp4" />
                        </video>
                      }
                    </div>
                    <h3>Handling Communications</h3>
                  </Col>
                  {/* <Col md={4} className='pr-md-0'>

                                                  <div className='scada-video-box'>
                                                       {
                                                            <video controls poster={poster}>
                                                                 <source src={video} type='video/mp4' />
                                                            </video>
                                                       }
                                                  </div>
                                                  <h3>COMING SOON</h3>
                                             </Col>
                                             <Col md={4} className='pr-md-0'>

                                                  <div className='scada-video-box'>
                                                       {
                                                            <video controls poster={poster}>
                                                                 <source src={video} type='video/mp4' />
                                                            </video>
                                                       }
                                                  </div>
                                                  <h3>COMING SOON</h3>
                                             </Col>

                                             <Col md={4} className='pr-md-0'>

                                                  <div className='scada-video-box'>
                                                       {
                                                            <video controls poster={poster}>
                                                                 <source src={video} type='video/mp4' />
                                                            </video>
                                                       }
                                                  </div>
                                                  <h3>COMING SOON</h3>
                                             </Col> */}
                </Row>
              </Container>
            </div>
          </div>

          <div id="livewebtraining" className="tect-support-livewebtraining">
            <div className="container max_width_1080">
              <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section4
                    ? this.state.content.section4
                    : ""
                }}
              >
                {/* <Col className="" md={6} sm={6}>
                                   <div className="">
                                        <h2>Live Web Training</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie</p>
                                        <Link to="/live-training">view available schedules</Link>
                                   </div>
                                   </Col>
                                   <Col className="image-box" md={6} sm={6}>
                                        <img className="img-fluid" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603994719362_AdobeStock_336595936%402x.jpg" />
                                        </Col> */}
              </Row>
            </div>
          </div>

          <SubFooter />
        </div>

        <Footer />
      </React.Fragment>
    );
  }
}

export default TechSupport;
