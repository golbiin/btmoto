import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import SubFooter from "./../../../frontend/common/subFooter";
import CommonBannerv1 from "./../../../frontend/common/commonBanner-v1";
import * as pageService from "./../../../ApiServices/page";
import "./../../../services/aos";
import { Helmet } from "react-helmet";
import SplashBanner from "./../../../frontend/common/splashBanner";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import * as newsService from "./../../../ApiServices/news";
const TITLE = "About Us | Automation Company - Industrial Automation";
class AboutV1 extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      bannerData: [
        {
          title: "ABOUT US",
          description:
            "CIMON is a leading industdal automaton solution provider with offices in the USA, South Korea, and other countries around the world. Tackling Industry 4.0 requirements head-on, CIMON strives towards an efficient, more productive future, allowing for the next generation of advanced techndogy and civilization.",
          image: "about.jpg",
          showButton: true
        }
      ],
      pageBannerdata: {
        title: "ABOUT CIMON",
        subtitle: "",
        backgroundImage: "",
        mainContent: ""
      },
      content: {},
      page_title: "",
      meta_description: "",
      meta_tag: "",
      Articledata: [],
      topNews: []
    };
  }
  componentDidMount = () => {
    this.getPageContents();
    this.getNews();
  };
  /* Get Page Contents */
  getPageContents = async () => {
    const slug = this.props.slug;
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {
        if (response.data.data.content.bannerSlider) {
          const banner_content = [];
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {};
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.subtitle = banner.subheading ? banner.subheading : "";
            Setdata.backgroundImage = banner.image
              ? banner.image
              : "https://cimon-rt.s3.amazonaws.com/upload/2020/10/1604424922648-business-team-contact-us-helpdesk-internet-concept.png";
            banner_content.push(Setdata);
          });

          // const pageBannerdata = { ...this.state.pageBannerdata };
          // pageBannerdata["backgroundImage"] = response.data.data.content
          //   .bannerSlider[0].image
          //   ? response.data.data.content.bannerSlider[0].image
          //   : "https://cimon-rt.s3.amazonaws.com/upload/2020/10/1604424922648-business-team-contact-us-helpdesk-internet-concept.png";

          this.setState({
            pageBannerdata: banner_content[0],
            bannerData: banner_content
          });
        }
        this.setState({ content: response.data.data.content });

        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  getNews = async () => {
    this.setState({ spinner: true });
    try {
      const response = await newsService.getNewsAbout();
      if (response.status === 200) {

          console.log(response);
          const article_row = [];
          if (response.data.news.length > 0) {
            
            response.data.news.map((item, index) => {
              const SetNews = {};
              SetNews.title = item.title;
              SetNews.description = item.description;
              SetNews.slug = item.slug;
              SetNews.image = item.image;
              article_row.push(SetNews);
            });            
          }
          this.setState({ topNews: article_row });
          const article_row1 = [];
          if (response.data.articles.length > 0) {
            
            response.data.articles.map((item, index) => {
              const SetArticle = {};
              if (index < 3) {
                SetArticle.title = item.title;
                SetArticle.description = item.description;
                SetArticle.slug = item.slug;
                article_row1.push(SetArticle);
              }
            });

          }
          this.setState({ Articledata: article_row1 });
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  render() {
    const images = require.context("./../../../assets/images", true);
    const {
      bannerData,
      page_title,
      meta_description,
      meta_tag,
      pageBannerdata,
      topNews,
      Articledata
    } = this.state;
    let newsImg = images(`./frontend/news-placeholder.png`);
    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <Header />
        <div id="about-us" className="container-fluid about">
          <SplashBanner bannerInfo={pageBannerdata} />

          <div id="whoweare" className="">
            <div className="container max_width_1080">
              
              <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section1
                    ? this.state.content.section1
                    : ""
                }}
              >
                {/* <Col className="" md={6} sm={6}>
                  <div className="content">
                    <h2>Who we are</h2>
                    <p>
                      Observe the historical and real-time data that is
                      monitored on the Xpanel device, with XpanelDesigner’s
                      Trend Graph object. Choose from the various trend types
                      provided for the visualization of data.
                    </p>
                    <a href="/ourcompany">More About CIMON</a>
                  </div>
                </Col>
                <Col className="" md={6} sm={6}>
                  <img
                    alt="aboutus"
                    className="img-fluid"
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604436909273_CIMON_office_front_view.png"
                  />
                </Col> */}
              </Row>
            </div>
          </div>

          <div
            id="home-section-bussiness-trust"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">
                  <h2>Businesses Trust CIMON</h2>
                </div>
              </div>

              <div
                className="row"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section2
                    ? this.state.content.section2
                    : ""
                }}
              >
              
              </div>

              {/* <div class="row">
                <div class="col-md-2  col-sm-1 clearfix d-none d-md-block">
                  <img
                    alt=""
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603809011620_GM_Logo.jpg"
                  />
                  &nbsp;
                </div>

                <div class="col-md-2 col-sm-1 clearfix d-none d-md-block">
                  <img
                    class="img-fluid"
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558541108_download%20%2814%29.png"
                  />
                </div>

                <div class="col-md-2  col-sm-1 clearfix d-none d-md-block">
                  <img
                    class="img-fluid"
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558469097_download%20%2811%29.png"
                  />
                </div>

                <div class="col-md-2  col-sm-1 clearfix d-none d-md-block">
                  <img
                    class="img-fluid"
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558564714_download%20%2815%29.png"
                  />
                </div>

                <div class="col-md-2  col-sm-1 clearfix d-none d-md-block">
                  <img
                    class="img-fluid"
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603809190016_KIA_Company_Logo.jpg"
                  />
                </div>

                <div class="col-md-2  col-sm-1 clearfix d-none d-md-block">
                  <img
                    class="img-fluid"
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558518387_download%20%2813%29.png"
                  />
                </div>

                <div class="col-md-2  col-sm-1 clearfix d-none d-md-block">
                  <img
                    alt=""
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603808999357_Korean_Air_Company_Logo.jpg"
                  />
                </div>

                <div class="col-md-2  col-sm-1 clearfix d-none d-md-block">
                  <img
                    alt=""
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603808986859_Korail_Company_logo.jpg"
                  />
                </div>

                <div class="col-md-2  col-sm-1 clearfix d-none d-md-block">
                  <img
                    alt=""
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603808973963_Posco_Company_logo.jpg"
                  />
                </div>

                <div class="col-md-2  col-sm-1 clearfix d-none d-md-block">
                  <img
                    alt=""
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603808960657_Korea_Telecom_Company_Logo.jpg"
                  />
                </div>
              </div> 

              <p class="brand_para">
                Many clients trust in CIMON for their industrial automation
                needs. Regardless of the industry, we have an integration
                solution waiting.
              </p>

              <p>
                <a href="/our-customers" class="viewmore">
                  View More
                </a>
              </p>
              */}
            </div>
          </div>

          <div
            id="home-section-news"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div className="row">
                <div className="near-heading_section col-md-12">
                  {" "}
                  <h5>NEWS</h5> <Link to="/news">View all news</Link>
                </div>
                {topNews.map((news, idx) => (
                  <div className={"col-md-4 p" + idx}>
                    <Link to={"cimon-news/" + news.slug}>
                      <div className="single-news">
                        <img
                          className="img-fluid"
                          src={news.image ? news.image : newsImg}
                        />
                        <h3>{news.title}</h3>

                        <p>{news.description}</p>
                        <Link to={"cimon-news/" + news.slug}>
                          View Details
                        </Link>
                      </div>
                    </Link>
                  </div>
                ))}
              </div>
            </div>
          </div>

          <div
            id="home-section-article"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div className="row">
                <div className="col-md-12 col-lg-12">
                  <div className="near-heading_section">
                    {" "}
                    <h5>Articles</h5>{" "}
                    <Link to="/latest-articles">View all articles</Link>
                  </div>
                  {Articledata.map((news, idx) => (
                    <div className="single_article">
                      <Link to={"latest-articles/" + news.slug}>
                        {" "}
                        <h3>{news.title}</h3>
                      </Link>
                      <p>{news.description}</p>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>

          <div id="aboutus-customer-login">
            <div className="container max_width_1080">

            <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section3
                    ? this.state.content.section3
                    : ""
                }}
              >

            </Row>
              {/* <div className="row">
                <div className="col-md-6">
                  <a href="/cimon-system-integrator-program">
                    <img
                      className="img-fluid"
                      src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604442868813_Engieering_with_linking_industrial_site.png"
                    />
                    <h3>System Integrator Program</h3>
                  </a>
                </div>
                <div className="col-md-6">
                  <a href="/distributor-locator">
                    <img
                      className="img-fluid"
                      src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604442919081_map-pin-gps-navigation-technology-wireless-technology-city.png"
                    />
                    <h3>Distributor and S.I Locator</h3>
                  </a>
                </div>
              </div> */}
            </div>
          </div>

          <div
            id="career"
            className=""
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">

              <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section5
                    ? this.state.content.section5
                    : ""
                }}
              >
                {/* <Col className="" md={6} sm={6}>
                  <div className="content">
                    <h2>Career</h2>
                    <p>
                      We’re hiring! We’re changing the face of the industrial
                      automation industry, but we can’t do it without you. Make
                      your mark and apply today.
                    </p>
                    <Link to="/careers">view career</Link>
                  </div>
                </Col>
                <Col className="" md={6} sm={6}>
                  <img
                    alt="career"
                    className="img-fluid"
                    src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604437728206_cheerful-business-people-job-is-done-group-of-office-workers-happy-to-hit-their-own-records-and-being-successful%402x.png"
                  />
                </Col> */}
              </Row>
            </div>
          </div>

          {/* <CommonBannerv1 bannerInfo={ bannerData } /> */}
          {/* <div className="product-ipc">
            <div className="tittle-goes abt-first-section" data-aos="fade-up"  data-aos-duration="2000">
              <div className="container">
                <div className="row">
                  <div className="col-md-12 tittle-head" 
                      dangerouslySetInnerHTML={{__html: this.state.content.section1 ?
                      this.state.content.section1 : '' }}> 
                    </div> 
                  </div>
                </div>
              </div>
              <div className="tittile-goes-content"  data-aos="fade-up"  data-aos-duration="2000">
                <div className="container">
                  <div className="row tittle-goes-wrapper mob-interchanges" 
                    dangerouslySetInnerHTML={{__html: this.state.content.section2 ?
                    this.state.content.section2 : '' }}>
                  </div>
                  <div className="row tittle-goes-wrapper respected-prowess"
                    dangerouslySetInnerHTML={{__html: this.state.content.section3 ?
                      this.state.content.section3 : '' }}>
                  </div>
                </div>
              </div>
            </div>            
          <div className="counter-section"  data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row counter-center">
                <div className="col-md-3 sb1 col-xs-1 common-clms first-clms"
                  dangerouslySetInnerHTML={{__html: this.state.content.section4_1 ?
                    this.state.content.section4_1 : '' }}>
                </div>
                <div dangerouslySetInnerHTML={{__html: this.state.content.section4_2 ?
                  this.state.content.section4_2 : '' }} className="col-md-3 sb1 col-xs-1 
                    common-clms second-clms">
                </div>
                <div dangerouslySetInnerHTML={{__html: this.state.content.section4_3 ?
                  this.state.content.section4_3 : '' }}
                  className="col-md-3 sb1 col-xs-1 common-clms three-clms">   
                </div>
              </div>
            </div>
          </div>
          <div className="product-ipc" data-aos="fade-up"  data-aos-duration="2000">
            <div className="tittle-goes abt-first-section">
              <div className="container">
                <div className="row">
                  <div className="col-md-12 tittle-head" dangerouslySetInnerHTML={{__html: this.state.content.section5 ?
                      this.state.content.section5 : '' }}>   
                    </div>
                  </div>
              </div>
            </div>
            <div className="tittle-goes abt-fifth-section compassion">
              <div className="container">
                <div className="row" dangerouslySetInnerHTML={{__html: this.state.content.section6 ? 
                  this.state.content.section6 : '' }}>    
                </div>
              </div>
            </div>
          </div>
          <div className="testimonial-section">
            <div className="container">
              <div className="row content">
                <div className="col-md-12 post-entry">
                  <div dangerouslySetInnerHTML={{__html: this.state.content.section7 ?
                      this.state.content.section7 : '' }} className="testimonial-wrapper"> 
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div id="home-section-bussiness-trust" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">
                  <h2>TRUSTED PARTNERSHIPS</h2>
                </div>
                <div className="container my-4">
                  <div
                  id="logo_slider"
                  className="carousel slide carousel-multi-item"
                  data-ride="carousel"
                  >
                    <div className="carousel-inner" role="listbox">
                      <div className="carousel-item active">
                        <div className="row" dangerouslySetInnerHTML={{__html: this.state.content.section8 ?
                            this.state.content.section8 : '' }} >
                        </div>
                      </div>
                      <div className="carousel-item">
                        <div className="row"  dangerouslySetInnerHTML={{__html: this.state.content.section8 ?
                            this.state.content.section8 : '' }}>
                        </div>
                      </div>
                    </div>
                    <div className="controls-top">
                      <a
                        className="carousel-control-prev"
                        role="button"
                        href="#logo_slider"
                        data-slide="prev"
                        >
                        <i className="fa fa-chevron-left"></i>
                      </a>
                      <a
                        className="carousel-control-next"
                        role="button"
                        href="#logo_slider"
                        data-slide="next"
                      >
                        <i className="fa fa-chevron-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="home-section-bussiness-trust" className="recognition" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row">
                <div  dangerouslySetInnerHTML={{__html: this.state.content.section9 ?
                    this.state.content.section9 : '' }} className="col-lg-12 col-md-12 col-sm-12">
                </div>
              </div>
            </div>
          </div>
          <div className="about-section-eleven">
            <div className="container">
              <div className="row" dangerouslySetInnerHTML={{__html: this.state.content.section10 ?
                  this.state.content.section10 : '' }}> 
              </div>
            </div>
          </div>
          <SubFooter /> */}
          <SubFooter />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default AboutV1;
