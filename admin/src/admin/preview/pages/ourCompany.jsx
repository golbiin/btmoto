import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import SubFooter from "./../../../frontend/common/subFooter";
import CommonBannerv1 from "./../../../frontend/common/commonBanner-v1";
import * as pageService from "./../../../ApiServices/page";
import "./../../../services/aos";
import { Helmet } from "react-helmet";
import SplashBanner from "./../../../frontend/common/splashBanner";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import * as newsService from "./../../../ApiServices/news";
import CountUp, { startAnimation } from "react-countup";

import VisibilitySensor from "react-visibility-sensor";
import { fadeIn } from "react-animations";
import Radium, { StyleRoot } from "radium";

const styles = {
  bounce: {
    animation: "x 1s",
    animationName: Radium.keyframes(fadeIn, "fadeIn")
  }
};

const TITLE = "Our Company | Automation Company - Industrial Automation";
class OurCompany extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      bannerData: [
        {
          title: "ABOUT US",
          description:
            "CIMON is a leading industdal automaton solution provider with offices in the USA, South Korea, and other countries around the world. Tackling Industry 4.0 requirements head-on, CIMON strives towards an efficient, more productive future, allowing for the next generation of advanced techndogy and civilization.",
          image: "about.jpg",
          showButton: true
        }
      ],
      pageBannerdata: {
        title: "OUR COMPANY",
        subtitle: "",
        backgroundImage:
          "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606841982322_15652.jpg",
        mainContent: ""
      },
      content: {},
      Articledata: [],
      topNews: [],
      scrollStatus: true,
      sliderContent: [],
      counterContent: [],
      /* sliderContent: [
        {
          image:
            "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606844100038_5672.jpg",
          title: "Market Leader",
          content:
            "CIMON is well-regarded among peers and is known as being an honest, dependable automation solutions provider. First, our automation products are soundly designed by knowledgeable engineers and developers. Following the design phase, our automation products are manufactured and undergo scrupulous testing for product flaws and functionality. After passing inspection, CIMON automation products are ready for client purchasing. Through this precise and rigorous process, we ensure that your project needs are optimally met every step of the way, from concept to execution."
        },
        {
          image:
            "https://cimon-rt.s3.amazonaws.com/profile_cimon/2021_0_1611053604854_amicable-rapport.png",
          title: "Amicable Rapport",
          content:
            "The foundation of any business relationship begins with client-provider interactions. Establishing goodwill is essential for both parties to succeed in a mutually beneficial manner. This is why our customer support is always free and friendly. While other automation technology providers may charge monthly or annual fees for the privilege of getting in touch with them, we believe that reaching us should be as easy and helpful as possible. You can always call us, email us, or fill out a form to get in touch with us—at no cost to you"
        },
        {
          image:
            "https://cimon-rt.s3.amazonaws.com/profile_cimon/2021_0_1611053693162_respected-prowess.png",
          title: "Respected Prowess",
          content:
            "CIMON is well-regarded among peers and is known as being an honest, dependable automation solutions provider. First, our automation products are soundly designed by knowledgeable engineers and developers. Following the design phase, our automation products are manufactured and undergo scrupulous testing for product flaws and functionality. After passing inspection, CIMON automation products are ready for client purchasing. Through this precise and rigorous process, we ensure that your project needs are optimally met every step of the way, from concept to execution."
        }
      ], */
      animationStyle: {},
      page_title: "",
      meta_description: "",
      meta_tag: ""
    };
  }
  addAnimation = () => {
    let animationStyle = { ...this.state.animationStyle };
    animationStyle = styles.bounce;

    this.setState({ animationStyle });
  };
  removeAnimation = () => {
    this.setState({ animationStyle: {} });
  };
  moveSlider = () => {
    const sliderContent = [...this.state.sliderContent];
    var firstElement = sliderContent[0];
    sliderContent.shift();
    sliderContent.push(firstElement);
    this.setState({ sliderContent });
    this.addAnimation();

    setTimeout(() => {
      this.removeAnimation();
    }, 500);
  };
  componentDidMount = () => {
    //this.getPageContents()
    const slug = this.props.slug;
    this.getContent(slug);
    this.getNews();
  };

  getContent = async slug => {
    const sliderContent = [...this.state.sliderContent];
    const counterContent = [...this.state.counterContent];
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status == 1) {
        /* Banner-Slider Contents */
        if (response.data.data.content.bannerSlider) {
          
          response.data.data.content.bannerSlider.map((banner, index) => {
            let eachBanner = {
              image:"",
              title: "",
              content:""
            };
            eachBanner.title = banner.heading ? banner.heading : "";
            eachBanner.content = banner.subheading ? banner.subheading : "";
            eachBanner.image = banner.image ? banner.image : "ipc_banner.jpg";
            sliderContent.push(eachBanner);
          });
          console.log("this.homedata.imageSetdata", sliderContent);
          
          this.setState({ sliderContent });
        }

        if (response.data.data.content.counter_values) {
          
          response.data.data.content.counter_values.map((counter, index) => {
            let eachCounter = {
              count:"",
              title: ""
            };
            eachCounter.count = counter.count ? counter.count : "";
            eachCounter.title = counter.title ? counter.title : "";
            counterContent.push(eachCounter);
          });
          console.log("this.homedata.imageSetdata", counterContent);
          
          this.setState({ counterContent });
        }

        this.setState({ content: response.data.data.content });

        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      } else {
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* Get Page Contents */
  // getPageContents = async () => {
  //   const slug = this.props.match.path.substring(
  //     this.props.match.path.lastIndexOf('/') + 1
  //   )
  //   try {
  //     const response = await pageService.getPageContent(slug)
  //     console.log(11,response);
  //     if (response.data.status === 1) {
  //       if (response.data.data.content.bannerSlider) {
  //         const banner_content = []
  //         response.data.data.content.bannerSlider.map((banner, index) => {
  //           const Setdata = {}
  //           Setdata.title = banner.heading ? banner.heading : ''
  //           Setdata.description = banner.subheading ? banner.subheading : ''
  //           Setdata.image = banner.image
  //             ? banner.image
  //             : '/static/media/about.034a1af3.jpg'
  //           banner_content.push(Setdata)
  //         })

  //         const pageBannerdata = { ...this.state.pageBannerdata }
  //         pageBannerdata['backgroundImage'] = response.data.data.content
  //           .bannerSlider[0].image
  //           ? response.data.data.content.bannerSlider[0].image
  //           : 'https://cimon-rt.s3.amazonaws.com/upload/2020/10/1604424922648-business-team-contact-us-helpdesk-internet-concept.png'

  //         this.setState({
  //           pageBannerdata: pageBannerdata,
  //           bannerData: banner_content
  //         })
  //       }
  //       this.setState({ content: response.data.data.content })

  //       if (response.data.data.page_title) {
  //         this.setState({
  //           page_title: response.data.data.page_title
  //         })
  //       }
  //     }
  //   } catch (err) {
  //     this.setState({ spinner: false })
  //   }
  // }

  getNews = async () => {
    this.setState({ spinner: true });
    try {
      const response = await newsService.getNews();
      if (response.status === 200) {
        if (response.data.latestNews.length > 0) {
          this.setState({ latestNews: response.data.latestNews });
          if (response.data.topNews.length > 0) {
            const article_row = [];
            console.log("item", response.data.topNews);
            response.data.topNews.map((item, index) => {
              const SetNews = {};
              if (
                index <
                (response.data.topNews.length > 6
                  ? 6
                  : response.data.topNews.length)
              ) {
                SetNews.title = item.title;
                SetNews.description = item.description;
                SetNews.slug = item.slug;
                SetNews.image = item.image;

                article_row.push(SetNews);
              }
            });
            this.setState({ topNews: article_row });
          }
          if (response.data.recentNews.length > 0) {
            const article_row = [];
            response.data.recentNews.map((item, index) => {
              const SetArticle = {};
              if (index < 3) {
                SetArticle.title = item.title;
                SetArticle.description = item.description;
                SetArticle.slug = item.slug;
                article_row.push(SetArticle);
              }
              console.log("item", article_row, item);
            });
            // console.log('article_row',article_row ,response.data.recentNews);
            this.setState({ Articledata: article_row });
          }
        }
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  onVisibilityChange = isVisible => {
    console.log("isVisible", isVisible);
    if (isVisible) {
      if (this.state.scrollStatus) {
        console.log("this.myCountUp", this.myCountUp);
        startAnimation(this.myCountUp.props.end);
        // this.setState({ scrollStatus: false });
      }
    }
  };

  render() {
    const images = require.context("./../../../assets/images", true);
    const {
      bannerData,
      page_title,
      meta_description,
      meta_tag,
      pageBannerdata,
      topNews,
      Articledata
    } = this.state;
    let newsImg = images(`./frontend/news-placeholder.png`);
    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <Header />
        <div id="ourcompany" className="container-fluid company about">
          <SplashBanner bannerInfo={pageBannerdata} />

          <div
            id="cpy_section1"
            className=""
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section1
                    ? this.state.content.section1
                    : ""
                }}
              >
                {/* <div className='col-md-12'>
                  <h2>A Team of Industry Leaders</h2>
                  <img
                    className="img-fluid"
                    alt='A Team of Industry Leaders'
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606843707689_76425.jpg'
                  />
                  <p>
                    At CIMON, experienced professionals work together to bring
                    you the advanced automation technology you desire. Hardware
                    and software are developed simultaneously, thereby promoting
                    a faster market response time, and innovative product
                    improvements are always on the agenda. From the start of
                    project design to the completion of solution implementation,
                    we always have your business needs in mind.{' '}
                  </p>
                </div> */}
              </Row>
            </div>
          </div>

          <div
            id="cpy_section2"
            className=""
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <Row onClick={this.moveSlider}>
                <div className="col-md-6">
                  <StyleRoot className="animation">
                    {this.state.sliderContent.map((eachItem, index) => {
                      let imgClass =
                        index !== 0
                          ? index === 1
                            ? "img-fluid img-second"
                            : "img-fluid img-third"
                          : "img-fluid img-active";
                      let animationStyle =
                        index !== 0 ? {} : this.state.animationStyle;
                      return (
                        <img
                          className={imgClass}
                          alt="market Leaders"
                          src={eachItem.image}
                          style={animationStyle}
                        />
                      );
                    })}
                  </StyleRoot>
                </div>
                <div className="col-md-6">
                  {this.state.sliderContent.map((eachItem, index) => {
                    let contentClass =
                      index !== 0 ? "paddingleft" : "paddingleft active";
                    return (
                      <div className={contentClass}>
                        <h4>{eachItem.title}</h4>
                        <p dangerouslySetInnerHTML={{
                  __html: eachItem.content
                    ? eachItem.content
                    : ""
                }}></p>
                        


                      </div>
                    );
                    
                  })}
                
                </div>
              </Row>
            </div>
          </div>

          <div
            id="cpy_section3"
            className=""
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <Row>
              {this.state.counterContent.map((eachItem, index) => {
                 return (
                <div className="col-md-4">
                  <div className="counter_div">
                    <h6>
                      <CountUp end={eachItem.count} start={0} duration={1} redraw={true}>
                        {({ countUpRef, start }) => (
                          <VisibilitySensor onChange={start} delayedCall>
                            <span ref={countUpRef} />
                          </VisibilitySensor>
                        )}
                      </CountUp>
                    </h6>
                    <p>{eachItem.title}</p>
                  </div>
                </div>

);
                    
})}

                {/* <div className="col-md-4">
                  <div className="counter_div">
                    <h6>
                      {" "}
                      <CountUp duration={1} start={0} end={999} redraw={true}>
                        {({ countUpRef, start }) => (
                          <VisibilitySensor onChange={start} delayedCall>
                            <span ref={countUpRef} />
                          </VisibilitySensor>
                        )}
                      </CountUp>
                    </h6>
                    <p>Product Sold Worldwide </p>
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="counter_div">
                    <h6>
                      {" "}
                      <CountUp duration={1} start={0} end={40} redraw={true}>
                        {({ countUpRef, start }) => (
                          <VisibilitySensor onChange={start} delayedCall>
                            <span ref={countUpRef} />
                          </VisibilitySensor>
                        )}
                      </CountUp>
                    </h6>
                    <p>Partners around the world</p>
                  </div>
                </div> */}
              </Row>
            </div>
          </div>

          <div
            id="cpy_section4"
            className=""
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section2
                    ? this.state.content.section2
                    : ""
                }}
              >
                {/* <div className='col-md-12'>
                  <h2>A Team of Industry Leaders</h2>
                  <img
                    className="img-fluid"
                    alt='A Team of Industry Leaders'
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606851155248_46234.jpg'
                  />
                  <p>
                    CIMON established its roots in South Korea, 1999. Since its foundation, CIMON has carefully constructed several award-
                    winning product lineups, including the Xpanel HMI series, PLC and PLC-S series, Xpanel Hybrid, UltimateAccess SCADA
                    software, and industrial PCs. With expanding offices in South Korea, the USA, and other parts of the globe, CIMON
                    remains adaptable and resilient as Industry 4.0 and IIoT evolve in the modern era. New products and upgrades are
                    always on the horizon, so look forward to the latest in automation innovation with CIMON.
                  </p>
                </div> */}
              </Row>
            </div>
          </div>

          <div
            id="cpy_section5"
            className=""
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <Row
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section3
                    ? this.state.content.section3
                    : ""
                }}
              >
                {/* <div className='col-md-6'>
                  <img
                    className="img-fluid"
                    alt='Compassion 
                    and Responsibility'
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1606851331836_8351.jpg'
                  />
                </div>
                <div className='col-md-6'>
                  <div className='paddingleft'>
                    <h4>Compassion <br></br> and Responsibility</h4>
                    <p>
                      Building the next generation is a heavy burden to bear,
                      and because of this, it’s important to use prudence and
                      care. CIMON uses ethical business practices and
                      strives towards a more sustainable and efficient
                      tomorrow. The future is being built in the present, and
                      instead of focusing on just the numbers, CIMON
                      realizes that what truly matters is making a more
                      prosperous, effective society. Our products, crafted with
                      ingenuity, reflect the genuine desire to make the world a
                      better place.
                    </p>
                  </div>
                </div> */}
              </Row>
            </div>
          </div>

          <div
            className="testimonial-section"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div
                className="row content"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section4
                    ? this.state.content.section4
                    : ""
                }}
              >
                {/* <div className='col-md-12 post-entry'>
                  <div className='testimonial-wrapper'>
                    <div class='test-content'>
                      <h4>
                        Industrial automation is the best way to take advantage
                        of technology in manufacturing. To become the best, we
                        must always strive to improve.
                      </h4>
                    </div>

                    <div class='cmply-details'>
                      <strong class='author-name'>
                        Che Bong An- CEO OF CIMON INC
                      </strong>
                    </div>
                  </div>
                </div> */}
              </div>
            </div>
          </div>

          <div
            id="home-section-bussiness-trust"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div
                className="row"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section5
                    ? this.state.content.section5
                    : ""
                }}
              >
                {/* <div className='col-lg-12 col-md-12 col-sm-12'>
                  <h2>Businesses Trust CIMON</h2>
                </div> */}
              </div>

              <div
                class="row"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section6
                    ? this.state.content.section6
                    : ""
                }}
              >
                {/* <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                  <img
                    className="img-fluid"
                    alt=''
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603809011620_GM_Logo.jpg'
                  />
                  &nbsp;
                </div>

                <div class='col-md-2 col-sm-1 clearfix d-none d-md-block'>
                  <img
                    class='img-fluid'
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558541108_download%20%2814%29.png'
                  />
                </div>

                <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                  <img
                    class='img-fluid'
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558469097_download%20%2811%29.png'
                  />
                </div>

                <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                  <img
                    class='img-fluid'
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558564714_download%20%2815%29.png'
                  />
                </div>

                <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                  <img
                    class='img-fluid'
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603809190016_KIA_Company_Logo.jpg'
                  />
                </div>

                <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                  <img
                    class='img-fluid'
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558518387_download%20%2813%29.png'
                  />
                </div>

                <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                  <img
                    className="img-fluid"
                    alt=''
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603808999357_Korean_Air_Company_Logo.jpg'
                  />
                </div>

                <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                  <img
                    className="img-fluid"
                    alt=''
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603808986859_Korail_Company_logo.jpg'
                  />
                </div>

                <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                  <img
                    className="img-fluid"
                    alt=''
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603808973963_Posco_Company_logo.jpg'
                  />
                </div>

                <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                  <img className="img-fluid"
                    alt=''
                    src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603808960657_Korea_Telecom_Company_Logo.jpg'
                  />
                </div> */}
              </div>

              {/* <p class='brand_para'>
                Many clients trust in CIMON for their industrial automation
                needs. Regardless of the industry, we have an integration
                solution waiting.
              </p>

              <p>
                <a href="/our-customers" class='viewmore'>View More</a>
              </p> */}
            </div>
          </div>

          <div
            id="home-section-bussiness-trust"
            className="recognition bussiness-trust-award"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div
                className="row"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section7
                    ? this.state.content.section7
                    : ""
                }}
              >
                {/* <div className='col-lg-12 col-md-12 col-sm-12'>
                  <h2>AWARDS AND DISTINCTIONS</h2>

                  <p>
                    <img
                      class='img-fluid'
                      src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598337491046_Recognition.02cded0f.png'
                    />
                  </p>
                </div> */}
              </div>
            </div>
          </div>
          <div className="about-section-eleven">
            <div className="container max_width_1080">
              <div
                className="row"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section8
                    ? this.state.content.section8
                    : ""
                }}
              >
                {/* <div class='col-md-4 first-img-section'>
                  <div class='img1'>
                    <a href='/careers'>
                      <img
                        class='img-fluid'
                        src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598337520118_career.4fc63174.png'
                      />
                    </a>
                    <div class='image-caption-overlay-center'>
                      <p>
                        <a href='/careers'>Career</a>
                      </p>
                    </div>
                  </div>


                </div>

                <div class='col-md-4 first-img-section'>
                  <div class='img1'>
                    <a href='/cimon-system-integrator-program/'>
                      <img
                        class='img-fluid'
                        src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598337540734_community.5482a427.png'
                      />
                    </a>

                    <div class='image-caption-overlay-center'>
                      <p>
                        <a href='/cimon-system-integrator-program/'>S.I Program</a>
                      </p>
                    </div>
                  </div>


                </div>

                <div class='col-md-4 first-img-section'>
                  <div class='img1'>
                    <a href='/contact'>
                      <img
                        class='img-fluid'
                        src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598337562844_contact.dd8478fa.png'
                      />
                    </a>

                    <div class='image-caption-overlay-center'>
                      <p>
                        <a href='/contact'>Contact Us</a>
                      </p>
                    </div>
                  </div>


                </div> */}
              </div>
            </div>
          </div>
          <SubFooter />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default OurCompany;
