import React from 'react';
import * as pageService from "./../../../ApiServices/page";

class SubFooter extends React.Component {
  state = {
    content : {}
  }

  componentDidMount = async () => {
    this.getContent(this.props.slug);
  }
 /* Get Page Contents */
  getContent = async (slug) => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status == 1) {
        this.setState({ content: response.data.data.content });
      } else {
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  render() {
    return (
      <div id="home-section-have-question">
        <div className="container full">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12" dangerouslySetInnerHTML={{ __html: this.state.content.section1 ? this.state.content.section1 : '' }}>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default SubFooter;