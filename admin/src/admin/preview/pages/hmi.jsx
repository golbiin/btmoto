import React, { Component } from "react";
import { Container, Row, Col, Tabs, Tab } from "react-bootstrap";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import SubFooter from "./../../../frontend/common/subFooter";
import Banner from "./../../../frontend/common/banner";
import RecommendedProducts from "./../../../frontend/common/recommendProducts";
import * as CategoriesService from "./../../../ApiServices/categories";
import * as pageService from "./../../../ApiServices/page";
import VideoV1 from "./../../../frontend/common/video-v1";
import { Helmet } from "react-helmet";
import "./../../../services/aos";
import { Link } from "react-router-dom";
import $ from "jquery";
class Hmi extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      hmicategories: [],
      products: [],
      categoryData: [],
      content: {},
      videoData: {
        title: "CIMON Xpanel: Slim. Smart. Strong.",
        sub_title: "",
        description:
          "<p>Our company has extensive knowledge and experience in the automation industry, and with that knowledge and experience, we’ve developed state-of-the-art HMI systems. </p><p>What elements should you consider before choosing an HMI? You’d want to see if there is a sleek, heavy-duty outer shell design. You’d want to check if the user interface is easy to navigate. You’d want to experience for yourself everything of which the HMI is capable. </p><p>CIMON Xpanel is the result of years of industry immersion. Carefully designed and catered to even the most detail-oriented OEM, the CIMON Xpanel has all your control and automation needs in mind.</p>",
        video: "cimon.mp4",
        showButton: true,
        buttonText: "Download XpanelDesigner",
        buttonNotes: "version 1.1 Updated 11-11-2019"
      },
      page_title: "",
      meta_tag: "",
      meta_description: ""
    };
  }

  componentDidMount = () => {
    const slug = this.props.slug;
    this.getCategorydata();
    this.getAllCategoryProducts();
    this.getContent(slug);
  };

  /* Get page content */
  getContent = async slug => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status == 1) {
        /* banner slider*/
        if (response.data.data.content.bannerSlider) {
          const banner_content = [];
          const common_silder = [];
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {};
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
            banner_content.push(Setdata);
          });
          this.setState({
            categoryData: banner_content
          });
        }
        this.setState({ content: response.data.data.content });
        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* Get banner content */
  getCategorydata = async () => {
    this.setState({ blocking: true });
    try {
      let category_slug = "hmi";
      const response = await CategoriesService.getAllCategorybySlug(
        category_slug
      );
      let hmiDetails = response.data.categoryData;
      if (hmiDetails) {
        let hmicategories = hmiDetails;
        this.setState({ hmicategories });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  /* Get recommended products */
  getAllCategoryProducts = async () => {
    this.setState({ blocking: true });
    try {
      let category_slug = "plc";
      const response = await CategoriesService.getAllCategoryProducts(
        category_slug
      );
      let productsDetails = response.data.categoryData;
      if (productsDetails) {
        let products = productsDetails;
        this.setState({ products });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  render() {
    let activetab = "";
    const {
      categoryData,
      products,
      videoData,
      wizards,
      hmicategories,
      page_title,
      meta_tag,
      meta_description
    } = this.state;
    console.log("hmicategories", hmicategories);
    if (hmicategories.length) {
      activetab = hmicategories[0].slug ? hmicategories[0].slug : "";
    }
    const images = require.context("./../../../assets/images", true);

    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <Header />
        <div
          id="category-plc"
          className="product-ipc category-plc category-hmi"
        >
          <Banner categoryInfo={categoryData} />
          <VideoV1
            section1={
              this.state.content.section1 ? this.state.content.section1 : ""
            }
            section1_sub={
              this.state.content.section1sub1
                ? this.state.content.section1sub1
                : ""
            }
            videourl={
              this.state.content.videourl
                ? this.state.content.videourl
                : "cimon.mp4"
            }
            videoInfo={videoData}
            videotype={this.state.content.video_type}
          />

          <Container
            className="sub-category-wrapper text-center"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div id="main_cat_tab">
              <Tabs defaultActiveKey="0" id="main_cat_tab">
                {hmicategories.map((data, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={index}
                      title={
                        data.category_name != null ? data.category_name : ""
                      }
                    >
                      <Row>
                        <Col className="col-md-6">
                          <div className="img-box">
                            <img
                              className="img-fluid"
                              alt={data.category_name}
                              src={data.image}
                            ></img>
                          </div>
                        </Col>
                        <Col className="col-md-6">
                          <div
                            dangerouslySetInnerHTML={{
                              __html:
                                data.descrption != null ? data.descrption : ""
                            }}
                          />
                          <div className="learn-more">
                            <Link to={"/products/hmi/" + data.slug}>
                              Learn More
                            </Link>
                          </div>
                        </Col>
                      </Row>
                    </Tab>
                  );
                })}
              </Tabs>
            </div>
            {/* <Row>

            


              {hmicategories.map((data, index) => {
                return (
                  <Col sm className="cat-list" key={index}>
                    <h3>
                      {data.category_name != null ? data.category_name : ""}
                    </h3>
                    <div className="img-box">
                      <img className="img-fluid" alt="" src={data.image}></img>
                    </div>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: data.descrption != null ? data.descrption : "",
                      }}
                    />
                    <div className="learn-more">
                      <a href={"/products/hmi/" + data.slug}>
                        <button className="btn-learn-more">LEARN MORE</button>
                      </a>
                    </div>
                  </Col>
                );
              })}
            </Row> */}
          </Container>

          <div
            className="tittle-goes"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div className="row">
                <div
                  className="col-md-12 tittle-head"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section2
                      ? this.state.content.section2
                      : ""
                  }}
                ></div>
              </div>
            </div>
          </div>
          {this.state.content.productSilder ? (
            <div
              className="slider-section"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <div
                      id="carouselSingleSlider"
                      className="carousel slide"
                      data-ride="carousel"
                    >
                      <div className="carousel-inner">
                        {this.state.content.productSilder.map(
                          (sildes, index) => {
                            return (
                              <div
                                className={
                                  index == 0
                                    ? "carousel-item active"
                                    : "carousel-item"
                                }
                              >
                                <img
                                  className="d-block w-100"
                                  src={
                                    sildes.image
                                      ? sildes.image
                                      : images(`./frontend/slider-1.jpg`)
                                  }
                                  className="img-fluid"
                                  alt="First slide"
                                ></img>
                              </div>
                            );
                          }
                        )}
                      </div>
                      <a
                        className="carousel-control-prev"
                        href="#carouselSingleSlider"
                        role="button"
                        data-slide="prev"
                      >
                        <span
                          className="carousel-control-prev-icon"
                          aria-hidden="true"
                        ></span>
                        <span className="sr-only">Previous</span>
                      </a>
                      <a
                        className="carousel-control-next"
                        href="#carouselSingleSlider"
                        role="button"
                        data-slide="next"
                      >
                        <span
                          className="carousel-control-next-icon"
                          aria-hidden="true"
                        ></span>
                        <span className="sr-only">Next</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
          <div
            className="tittile-goes-content hmi-core-feature"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div className="row">
                {/* <div className="  tittle-goes-wrapper" dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}>
              </div> */}
                <div
                  className="col-md-4 "
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section4
                      ? this.state.content.section4
                      : ""
                  }}
                ></div>
                <div
                  className="col-md-4 "
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section5
                      ? this.state.content.section5
                      : ""
                  }}
                ></div>
                <div
                  className=" col-md-4 "
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section6
                      ? this.state.content.section6
                      : ""
                  }}
                ></div>
                <div
                  className="col-md-4  "
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section7
                      ? this.state.content.section7
                      : ""
                  }}
                ></div>
                <div
                  className=" col-md-4 "
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section8
                      ? this.state.content.section8
                      : ""
                  }}
                ></div>
                <div
                  className=" col-md-4"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section9
                      ? this.state.content.section9
                      : ""
                  }}
                ></div>
              </div>
            </div>
          </div>

          {/* <div className="plc-common-section quard-core" data-aos="fade-up" data-aos-duration="2000">
            <div className="container" dangerouslySetInnerHTML={{ __html: this.state.content.section10 ? this.state.content.section10 : '' }}>
            </div>
          </div> */}

          <div
            className="cicon-container"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div className="row">
                <div
                  className="col-md-12 tittle-head text-center"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section11
                      ? this.state.content.section11
                      : ""
                  }}
                ></div>
              </div>
            </div>
          </div>

          <div
            className="tabs-section-wrapper text-center"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <Container className="max_width_1154">
              <Row>
                <div
                  className="col-md-12"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section12
                      ? this.state.content.section12
                      : ""
                  }}
                ></div>
                <Col className="col-md-12 tab_content">
                  <Tabs
                    defaultActiveKey="serial_network"
                    id="performance-tab"
                    className="performance-tab"
                  >
                    <Tab
                      eventKey="serial_network"
                      title="SERIAL NETWORK"
                      className="performance-tab-content"
                    >
                      <div className="tab-content-box">
                        <div className="tab-content-item">
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604942448164_Group%201707.png"
                            ></img>
                          </div>
                        </div>
                        <div className="tab-content-item">
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604942505734_Group%201706.png"
                            ></img>
                          </div>
                        </div>
                        {/* <div className="tab-content-item full-width">
                          <h4>CIMON Network</h4>
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src={images(`./frontend/hmi-tab-img3.png`)}
                            ></img>
                          </div>
                        </div> */}
                      </div>
                      <p>
                        Use Ethernet/IP, Modbus, Siemens S7, or other branded
                        communication protocols to connect with the Xpanel HMI.
                        With many available options, design your connectivity
                        network structure in a way that better facilitates
                        communication and task fulfillment.
                      </p>
                    </Tab>
                    <Tab
                      eventKey="ethernet_network"
                      title="ETHERNET NETWORK"
                      className="performance-tab-content"
                    >
                      <div className="tab-content-box">
                        <div className="tab-content-item full-width">
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604943071671_Ethernet%20Network-01.jpg"
                            ></img>
                          </div>
                        </div>
                      </div>
                      <p>
                        Use Ethernet/IP, Modbus, Siemens S7, or other branded
                        communication protocols to connect with the Xpanel HMI.
                        With many available options, design your connectivity
                        network structure in a way that better facilitates
                        communication and task fulfillment.
                      </p>
                    </Tab>
                    <Tab
                      eventKey="cimon_network"
                      title="CIMON NETWORK"
                      className="performance-tab-content"
                    >
                      <div className="tab-content-box">
                        <div className="tab-content-item full-width">
                          <div className="img-box">
                            <img
                              className="d-block w-100 img-fluid"
                              src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1604943403947_CIMON%20network-01.jpg"
                            ></img>
                          </div>
                        </div>
                      </div>
                      <p>
                        Use Ethernet/IP, Modbus, Siemens S7, or other branded
                        communication protocols to connect with the Xpanel HMI.
                        With many available options, design your connectivity
                        network structure in a way that better facilitates
                        communication and task fulfillment.
                      </p>
                    </Tab>
                  </Tabs>
                </Col>
              </Row>
            </Container>
          </div>
          <div
            className="tittile-goes-content tittle-goes-2 certificates-section-main"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div
                className="row tittle-goes-wrapper certificates-section"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section14
                    ? this.state.content.section14
                    : ""
                }}
              ></div>
            </div>
          </div>
          <div
            className="scada-common-icon-wrapper"
            data-aos="fade-up"
            data-aos-duration="2000"
            dangerouslySetInnerHTML={{
              __html: this.state.content.section15
                ? this.state.content.section15
                : ""
            }}
          ></div>
          <RecommendedProducts products={products} />
          <SubFooter />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default Hmi;
