import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import SubFooter from "./../../../frontend/common/subFooter";
import Banner from "./../../../frontend/common/banner";
import RecommendedProducts from "./../../../frontend/common/recommendProducts";
import * as CategoriesService from "../../../ApiServices/categories";
import * as pageService from "../../../ApiServices/page";
import VideoV1 from "./../../../frontend/common/video-v1";
import { Helmet } from "react-helmet";
import "../../../services/aos";
import { Container, Row, Col, Tabs, Tab } from "react-bootstrap";
import { Link } from "react-router-dom";

import SlickSilderCat from "./../../../frontend/common/slickSilderCat";

class Ipc extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      products: [],
      categoryData: [
        {
          title: "CIMON IPC",
          sub_title: "Industrial PC",
          description:
            "<p>An industrial PC is an important and crucial part of a business in the controls industry. Simply put, an industrial PC allows you to operate various types of industry software (such as UltimateAccess SCADA) while still maintaining the tough endurance and durability you’d expect in harsh environments.</p><br/><p>CIMON is one of the leading industrial PC manufacturers, and with good reason. With over 20 years of experience in the automation industry, CIMON puts those years of industry knowledge into the development of its high-quality products.</p>",
          image: "ipc_banner.png"
        },
        {
          title: "CIMON IPC",
          sub_title: "Industrial PC",
          description:
            "<p>An industrial PC is an important and crucial part of a business in the controls industry. Simply put, an industrial PC allows you to operate various types of industry software (such as UltimateAccess SCADA) while still maintaining the tough endurance and durability you’d expect in harsh environments.</p><br/><p>CIMON is one of the leading industrial PC manufacturers, and with good reason. With over 20 years of experience in the automation industry, CIMON puts those years of industry knowledge into the development of its high-quality products.</p>",
          image: "ipc_banner.png"
        },
        {
          title: "CIMON IPC",
          sub_title: "Industrial PC",
          description:
            "<p>An industrial PC is an important and crucial part of a business in the controls industry. Simply put, an industrial PC allows you to operate various types of industry software (such as UltimateAccess SCADA) while still maintaining the tough endurance and durability you’d expect in harsh environments.</p><br/><p>CIMON is one of the leading industrial PC manufacturers, and with good reason. With over 20 years of experience in the automation industry, CIMON puts those years of industry knowledge into the development of its high-quality products.</p>",
          image: "ipc_banner.png"
        }
      ],
      content: {},
      category_silder_data: [
        {
          title: "Efficiency",
          sub_title: "Backup and Recovery sub",
          description:
            "<p>Our IPCs are energy-efficient and produce little heat, saving money and electricity over time.</p>",
          image:
            "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1607363539529_Happy_engineer_looking_away.jpg"
        },
        {
          title: "Speed",
          sub_title: "Backup and Recovery sub",
          description:
            "<p>With a solid state disk (SSD) drive, load programs faster and store data more reliably than ever before.</p>",
          image:
            "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1607364287724_Abstract_light_with_speed.jpg"
        },
        {
          title: "Multilingual Interface",
          sub_title: "Backup and Recovery sub",
          description:
            "<p>Display your project in virtually any language with the multilingual user interface (MUI) package.</p>",
          image:
            "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605044734728_Backup.png"
        },
        {
          title: "Backup and Recovery",
          sub_title: "Backup and Recovery sub",
          description:
            "<p>Use the winclone feature for one-click backup and recovery of data.",
          image:
            "https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_11_1607364333847_Business_man_looking_at_laptop_with_headphone.jpg"
        }
      ],
      videoData: {
        title: "CIMON IPC: A Reliable Computer",
        description:
          "Able to meet the needs of industry projects large and small, CIMON IPCs are a welcome addition to your business strategy. Choose from a diverse lineup of IPC products with a broad range of capabilities, all suited for the harsh conditions of wear and tear.",
        video: "cimon.mp4",
        showButton: false
      },
      page_title: "",
      meta_tag: "",
      meta_description: "",
      categoryData1: []
    };
  }
  componentDidMount = async () => {
    const slug = this.props.slug
    this.getCategorydata();
    this.getAllCategoryProducts();
    this.getContent(slug);
  };

  /* Get page content */
  getContent = async slug => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status == 1) {
        /*banner slider */
        if (response.data.data.content.bannerSlider) {
          const banner_content = [];
          const common_silder = [];
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {};
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
            banner_content.push(Setdata);
          });
          this.setState({
            categoryData: banner_content
          });
        }
        this.setState({ content: response.data.data.content });
        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* Get banner content */
  getCategorydata = async () => {
    this.setState({ blocking: true });
    try {
      let category_slug = "ipc";
      const response = await CategoriesService.getAllCategorybySlug(
        category_slug
      );
      if (response.status == 200) {
        if (response.data.categoryData.length > 0) {
          this.setState({ categoryData1: response.data.categoryData });
          this.setState({
            message: response.data.message,
            status: response.data.status
          });
        }
      } else {
        this.setState({ blocking: false });
        this.setState({
          message: response.data.message,
          status: response.data.status
        });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  /* Get recommended products */
  getAllCategoryProducts = async () => {
    this.setState({ blocking: true });
    try {
      let category_slug = "hybrid-hmi";
      const response = await CategoriesService.getAllCategoryProducts(
        category_slug
      );
      let productsDetails = response.data.categoryData;
      if (productsDetails) {
        let products = productsDetails;
        this.setState({ products });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  render() {
    const images = require.context("../../../assets/images", true);
    const {
      categoryData,
      categoryData1,
      products,
      videoData,
      page_title,
      meta_description,
      meta_tag,
      category_silder_data
    } = this.state;

    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    console.log(this.state.content, ">>>>>>>>>>>>>>>");
    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <meta
          name="description"
          content={meta_description ? meta_description : " "}
        ></meta>
        <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        <Header />
        <div className="product-ipc">
          <div className="scada-container">
            <Banner categoryInfo={categoryData} />
            <VideoV1
              section1={
                this.state.content.section1 ? this.state.content.section1 : ""
              }
              section1_sub={
                this.state.content.section1sub1
                  ? this.state.content.section1sub1
                  : ""
              }
              videourl={
                this.state.content.videourl
                  ? this.state.content.videourl
                  : "cimon.mp4"
              }
              videoInfo={videoData}
              videotype={this.state.content.video_type}
            />
          </div>
          <div
            className="ipc-panel-pc ipc-pannel-products"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div className="row">
                <div id="main_cat_tab">
                  <Tabs
                    defaultActiveKey={categoryData1.length}
                    id="main_cat_tab"
                  >
                    {categoryData1.map((data, index) => {
                      return (
                        <Tab
                          key={index}
                          eventKey={index}
                          title={
                            data.category_name != null ? data.category_name : ""
                          }
                        >
                          <Row>
                            <Col className="col-md-6">
                              <div className="img-box">
                                <img
                                  className="img-fluid"
                                  alt={data.category_name}
                                  src={data.image}
                                ></img>
                              </div>
                            </Col>
                            <Col className="col-md-6">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html:
                                    data.descrption != null
                                      ? data.descrption
                                      : ""
                                }}
                              />
                              <div className="learn-more">
                                <Link to={"/products/ipc/" + data.slug}>
                                  Learn More
                                </Link>
                              </div>
                            </Col>
                          </Row>
                        </Tab>
                      );
                    })}
                  </Tabs>
                </div>
                {/* {categoryData1.map((catdat, index) => {
                  return (
                    <div className="col-md-4 col-sm-12" key={index}>
                      <div className="head-section">
                        <h3>
                          {catdat.category_name != null
                            ? catdat.category_name
                            : ""}
                        </h3>
                      </div>
                      <div className="img-section">
                        <img
                          src={
                            catdat.image != ""
                              ? catdat.image
                              : images(`./frontend/noImage-category.png`)
                          }
                          alt=""
                          className="img-fluid"
                        ></img>
                      </div>
                      <div
                        className="content_sectionss"
                        dangerouslySetInnerHTML={{
                          __html:
                            catdat.descrption != null ? catdat.descrption : "",
                        }}
                      />
                      <div className="learn-btn btns-learn-more">
                        <a
                          href={
                            "/products/ipc/" +
                            (catdat.slug != null ? catdat.slug : "")
                          }
                          className="learn-more"
                        >
                          LEARN MORE
                        </a>
                      </div>
                    </div>
                  );
                })} */}
              </div>
            </div>
          </div>

          <div
            className="cimon-pc cimon-ipc-new"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div
                class="row"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section2
                    ? this.state.content.section2
                    : ""
                }}
              ></div>
            </div>
          </div>

          <div
            className="tittle-goes"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div className="row justify-content-center">
                <div
                  className="col-md-12 tittle-head align-self-center"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section3
                      ? this.state.content.section3
                      : ""
                  }}
                ></div>
              </div>
            </div>
          </div>

          {this.state.content.commonSilder ? (
            <div
              className="slider-section"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <div
                      id="carouselSingleSlider"
                      className="carousel slide"
                      data-ride="carousel"
                    >
                      <div className="carousel-inner">
                        {this.state.content.commonSilder.map(
                          (sildes, index) => {
                            return (
                              <div
                                className={
                                  index == 0
                                    ? "carousel-item active"
                                    : "carousel-item"
                                }
                              >
                                <img
                                  className="d-block w-100"
                                  src={
                                    sildes.image
                                      ? sildes.image
                                      : images(`./frontend/slider-1.jpg`)
                                  }
                                  className="img-fluid"
                                  alt="First slide"
                                ></img>
                              </div>
                            );
                          }
                        )}
                      </div>
                      <a
                        className="carousel-control-prev"
                        href="#carouselSingleSlider"
                        role="button"
                        data-slide="prev"
                      >
                        <span
                          className="carousel-control-prev-icon"
                          aria-hidden="true"
                        ></span>
                        <span className="sr-only">Previous</span>
                      </a>
                      <a
                        className="carousel-control-next"
                        href="#carouselSingleSlider"
                        role="button"
                        data-slide="next"
                      >
                        <span
                          className="carousel-control-next-icon"
                          aria-hidden="true"
                        ></span>
                        <span className="sr-only">Next</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
          <div
            className="tittile-goes-content ipc-core-feature"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div className="row">
                <div
                  className="col-md-4"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section4
                      ? this.state.content.section4
                      : ""
                  }}
                ></div>
                <div
                  className="col-md-4"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section5
                      ? this.state.content.section5
                      : ""
                  }}
                ></div>
                <div
                  className="col-md-4"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section6
                      ? this.state.content.section6
                      : ""
                  }}
                ></div>
                <div
                  className="col-md-4"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section7
                      ? this.state.content.section7
                      : ""
                  }}
                ></div>
                <div
                  className="col-md-4"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section8
                      ? this.state.content.section8
                      : ""
                  }}
                ></div>
                <div
                  className="col-md-4"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section9
                      ? this.state.content.section9
                      : ""
                  }}
                ></div>
              </div>
            </div>
          </div>
          {/* <div className="plc-common-section quard-core" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container" dangerouslySetInnerHTML={{ __html: this.state.content.section10 ? this.state.content.section10 : '' }}>              
            </div>
          </div> */}

          <div
            className="tittle-goes gos-cust-1"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div className="row justify-content-center">
                <div
                  className="col-md-12 tittle-head align-self-center"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section11
                      ? this.state.content.section11
                      : ""
                  }}
                >
                  {/* <SlickSilderCat /> */}
                </div>
              </div>
            </div>
          </div>

          <SlickSilderCat categoryInfo={category_silder_data} />

          <div
            className="tittile-goes-content tittle-goes-2 dynamichtml"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div
              className="tittle-goes"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <div className="container">
                <div className="row justify-content-center">
                  <div
                    className="col-md-12 tittle-head align-self-center"
                    dangerouslySetInnerHTML={{
                      __html: this.state.content.section12
                        ? this.state.content.section12
                        : ""
                    }}
                  ></div>
                </div>
              </div>
            </div>
          </div>

          <div
            className="Certificates-grey tittle-goes-wrapper  text-center"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div
                className="row"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section14
                    ? this.state.content.section14
                    : ""
                }}
              ></div>
            </div>
          </div>

          {/* <div className="more-with-cimon" data-aos="fade-up"  data-aos-duration="2000">
            <div className="container">
              <div className="row" dangerouslySetInnerHTML={{ __html: this.state.content.section15 ? this.state.content.section15 : '' }} >                
              </div>
            </div>
          </div> */}

          <div
            className="tittle-goes cimon-ipc-contact"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div
                className="row justify-content-center"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section15
                    ? this.state.content.section15
                    : ""
                }}
              ></div>
            </div>
          </div>

          <RecommendedProducts products={products} />
          <SubFooter />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
export default Ipc;
