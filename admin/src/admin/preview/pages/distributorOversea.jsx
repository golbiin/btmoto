import React, { Component } from 'react'
import { Container, Form } from 'react-bootstrap'
import { Link } from 'react-router-dom'

import Header from "../../../frontend/common/header";
import Footer from "../../../frontend/common/footer";
import Productbanner from './../../../frontend/common/productBanner'
import Joi from 'joi-browser'
import BlockUi from 'react-block-ui'
import { connect } from 'react-redux'
import { USER_TYPES } from './../../../config.json'
import { login } from './../../../services/actions/userActions'
import * as pageService from "../../../ApiServices/page";
import { Helmet } from 'react-helmet'
import SplashBanner from './../../../frontend/common/splashBanner'
import SubFooter from "../../../frontend/common/subFooter";
import {  Row, Col } from 'react-bootstrap'


class DistributorOversea extends Component {
  constructor (...args) {
    super(...args)
    this.state = {
      data: { username: '', usertype: USER_TYPES.DISTRIBUTOR },
      errors: {},
      productBdata: {
        title: 'INTERNATIONAL DISTRIBUTOR',
        backgroundImage:
          'https://cimon-rt.s3.amazonaws.com/profile_cimon/1612213663681-modern-tower-buildings-skyscrapers-financial-district-with-cloud-sunny-day-chicago-usa%402x.png'
      },
      content: {},
      page_title: ''
    }
  }

  componentDidMount = async () => {   
    const slug = this.props.slug;
    let  slugcommon =  slug ? slug  : 'germany';
    this.getContent(slugcommon);
  }

  getContent = async (slug) => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status === 1) {

        console.log("response.data.data.content",response.data.data.content);

        console.log('before productBdata' , this.state.productBdata);
        /* Banner-Slider Contents */
        if (response.data.data.content.bannerSlider) {
          const banner_content = [];
          const common_silder = [];
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {};
            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.backgroundImage = banner.backgroundImage ? banner.backgroundImage : "https://cimon-rt.s3.amazonaws.com/profile_cimon/1612213663681-modern-tower-buildings-skyscrapers-financial-district-with-cloud-sunny-day-chicago-usa%402x.png";
            banner_content.push(Setdata);
          });
            this.setState({
              productBdata: banner_content[0],
            });
        }

        console.log('after productBdata' , this.state.productBdata);
       
        this.setState({ content: response.data.data.content });

        console.log('content productBdata' ,);
        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });

        }
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };


  render () {
    const { productBdata, page_title , content } = this.state
    const { loginProcessing, loginFailure, authUser } = this.props

    console.log('productBdata' ,productBdata);
    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : ' '}</title>
        </Helmet>
        <Header />
        <SplashBanner slug='distributeoversea' bannerInfo={productBdata} />

        <div className='container'>
          <div id='distributemain' className=''>
            <div className='container max_width_1080'>
              <Row>
                <Col className='' md={12} sm={12}>
                  <div dangerouslySetInnerHTML={{ __html: this.state.content.section1 ? this.state.content.section1 : '' }} ></div>
               
                <Row dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }} >
                {/* <Col className='' md={3} sm={3}>
                  <img className="img-fluid" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1605545088206_Hy-line_company_logo.png" alt="DistributorOversea" />
                  </Col>
                  <Col className='' sm={9} md={9}>
                 <div className="section1">
                      <h4>Hy-Line Communication
                      Products Vertriebs GmbH</h4>
                      <p>Inselkammerstr. 10
                      82008 Unterhaching
                      Germany</p>
                 </div>
                 <div className="section2">
                 <p><span>Phone:</span> <a href="tel:+49-89-614503-60">+49-89-614503-60</a></p>
                 <p><span>Fax:</span> <a href="">+49-89-6140960</a></p>
                 <p> <a className="website" href="">www.hy-line.de/wireless</a></p>
                 <p> <a className="email" href="mailto:communication@hy-line.de">communication@hy-line.de <span>(email)</span></a></p>
                 </div>
                  </Col>
                */}
               
                </Row>
                </Col>
              </Row>
            </div>
          </div>
        </div>
        <SubFooter />
        <Footer />
      </React.Fragment>
    )
  }
}
const mapStateToProps = state => ({
  authUser: state.auth.authUser,
  loginProcessing: state.auth.loginProcessing,
  loginFailure: state.auth.loginFailure
})
export default connect(mapStateToProps, {
  login
})(DistributorOversea)
