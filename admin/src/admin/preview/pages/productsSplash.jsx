import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import * as newsService from "./../../../ApiServices/news";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import SubFooter from "./../../../frontend/common/subFooter";
import { Helmet } from 'react-helmet';
import SplashBanner from "./../../../frontend/common/splashBanner";
import moment from "moment";
import { Link } from "react-router-dom";
import * as pageService from './../../../ApiServices/page';
class ProductsSplash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newsData: [],
      content: {},
      productsBanner: "",
      productBdata: {
        title: "Cimon Products--",
        subtitle: "",
        backgroundImage: "j19_bg.png",
        mainContent:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat.",
      },
    };

  }

  componentDidMount = () => {

    window.scrollTo(0, 0);
    this.getPageContents()
  };


  /*  Get Single News Details  */

  
   /* Get Page Contents */
   getPageContents = async () => {
    const slug = this.props.slug;
    try {
      const response = await pageService.getPageContent(slug)
      if (response.data.status === 1) {
        if (response.data.data.content.bannerSlider) {
          console.log('bannerSlider',response.data.data.content.bannerSlider);
          const banner_content = []
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {}
            Setdata.title = banner.heading ? banner.heading : ''
            Setdata.description = banner.subheading ? banner.subheading : ''
            Setdata.backgroundImage = banner.backgroundImage
              ? banner.backgroundImage
              : 'https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603902139033_aerial-view-tanker-oil-storage-tank-petrochemical-oil-shipping-terminal.jpg'
            banner_content.push(Setdata)
          })
 

          console.log('banner_content' ,banner_content[0] , this.state.productBdata)
          this.setState({
          
            productBdata: banner_content[0]
          })
        }
        this.setState({ content: response.data.data.content })

        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          })
        }
      }
    } catch (err) {
      this.setState({ spinner: false })
    }
  }


  render() {
    const { newsData, productBdata, date } = this.state;
    const news_slug = this.props.match.params.slug;


    const TITLE = 'Cimon Products | Automation Company - Industrial Automation';
    return (
      <React.Fragment>
        <Helmet>
          <title>{TITLE}</title>
        </Helmet>
        <Header />
        <div className="productsplash-single-wrapper">
          <SplashBanner bannerInfo={productBdata} />
          <div className="container">
            <Row dangerouslySetInnerHTML={{ __html: this.state.content.section1 ? this.state.content.section1 : '' }}>
              {/* <Col className="productnav" md={12} sm={12}>
                <ul>
                  <li><a href="#scadaid">Scada</a></li>
                  <li><a href="#hmiid">HMI</a></li>
                  <li><a href="#plcid">PLC</a></li>
                  <li><a href="#hmiplcid">HMI + PLC</a></li>
                  <li><a href="#ipcid">IPC</a></li>
                </ul>
              </Col>
          */}
            </Row>
          </div>
        </div>

        <div id="scadaid" 
        
        dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }}
        className="scada-banner-1 blackbackground">
        {/* <div className="container-fluid">
            <Row>
              <Col className="leftcontent" md={6} sm={6}>
              <div className="inner-maxwidth">
                <h2>SCADA</h2>
                <p>For powerful SCADA capability and an easy-to-use interface, choose CIMON UltimateAccess. Crafted by leading engineers in the automation industry, CIMON SCADA applications operate with your business controls solutions in mind.</p>

                <div class="splash-button-box">
                  <Link to="/introduction/scada" className="learnmore">learn more</Link>
                  <a className="download-access" href="/pdf/UltimateAccess_CIMONSCADAV3.90.pdf" target="_blank">Download UltimateAccess</a>
                </div></div>
              </Col>

              <Col md={6} sm={6}>
                <img className="img-fluid rightimage" alt="scada image" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1606421017017_297173DC-9192-41F7-A58E-650DEDEC06D4.png" />            </Col>
            </Row>
          </div> */}

        </div>

        <div id="hmiid"
        
        dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}
        className="scada-banner-1 whitebackground">
        {/* <div className="container-fluid">
            <Row>


              <Col md={6} sm={6}>
                <img className="img-fluid leftcontent " alt="hmi image" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1604002835187_XT15_robot_arm_screen%402x.png" />
              </Col>


              <Col className="rightimage" md={6} sm={6}>
                <h2>HMI</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>

                <div class="splash-button-box">
                  <Link to="/introduction/hmi" className="learnmore">learn more</Link>
                  <a className="download-access" href="/pdf/UltimateAccess_CIMONSCADAV3.90.pdf" target="_blank">Download XpanelDesigner</a>
                </div>
              </Col>


            </Row>
          </div> */}

        </div>

        <div id="plcid" 
        
        dangerouslySetInnerHTML={{ __html: this.state.content.section4 ? this.state.content.section4 : '' }}
        className="scada-banner-1 blackbackground">
        {/* <div className="container-fluid">
            <Row>
              <Col className="leftcontent" md={6} sm={6}>
              <div className="inner-maxwidth">
                <h2>PLC</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                <div class="splash-button-box">
                  <Link to="/introduction/plc" className="learnmore">learn more</Link>
                  <Link className="download-access" to="/download" >Download CICON</Link>
                </div>
                </div>
              </Col>

              <Col md={6} sm={6}>
                <img className="img-fluid rightimage" alt="plc image" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1604003033417_plcplcxp_rack%402xlatest.png" />            </Col>
            </Row>
          </div> */}

        </div>


        <div id="hmiplcid" 
          dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}
        className="scada-banner-1 whitebackground">
        {/* <div className="container-fluid">
            <Row>


              <Col md={6} sm={6}>
                <img className="img-fluid leftcontent " alt="hmi image" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_10_1606421101000_EF348B4D-B2EE-438C-8CB7-15F6FC53A5F0.png" />
              </Col>


              <Col className="rightimage" md={6} sm={6}>
                <h2>HMI + PLC</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>

                <div class="splash-button-box">
                  <Link to="/introduction/hybrid-hmi" className="learnmore">learn more</Link>
                  <a className="download-access" href="/pdf/UltimateAccess_CIMONSCADAV3.90.pdf" target="_blank">Download XpanelDesigner</a>
                  <Link className="download-access" to="/download" >Download CICON</Link>
                </div>
              </Col>


            </Row>
          </div> */}

        </div>


        <div id="ipcid" 
        
        dangerouslySetInnerHTML={{ __html: this.state.content.section6 ? this.state.content.section6 : '' }}
        className="scada-banner-1 blackbackground">
          {/* <div className="container-fluid">
            <Row>
              <Col className="leftcontent" md={6} sm={6}>
                <div className="inner-maxwidth">
                <h2>IPC</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                <div class="splash-button-box">
                  <Link to="/introduction/ipc" className="learnmore">learn more</Link>
                  </div>
                </div> 
              </Col>

              <Col md={6} sm={6}>
                <img className="img-fluid rightimage" alt="plc image" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603908433591_latestipcimage.png" />            </Col>
            </Row>
          </div> */}

        </div>


        <div className="top-arrow-wrapper">
         
          <div className="container">
            <Row>
              <Col className="top-arrow-icon" md={12} sm={12}>
              <a href="#scadaid">Back to top <i class="fa fa-arrow-up" aria-hidden="true"></i></a>
              </Col>
            </Row>
          </div>
        </div>

        <SubFooter />
        <Footer />
      </React.Fragment>
    );
  }
}

export default ProductsSplash;
