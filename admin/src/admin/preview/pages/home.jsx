import React, { Component } from "react";
import Header from "./../../../frontend/common/header";
import Footer from "./../../../frontend/common/footer";
import SubFooter from "./../../../frontend/common/subFooter";
import { Link } from "react-router-dom";
import SilckSilder from "./../../../frontend/common/slickSilder";
import * as pageService from "./../../../ApiServices/page";
import { Helmet } from "react-helmet";
import "./../../../services/aos";
import { siteUrl } from "./../../../config.json";
import * as newsService from "./../../../ApiServices/news";
import Homeslider from "./../../../frontend/common/homeSlider";

class Home extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  state = {
    content: {},
    homedata: [],
    page_title: "",
    meta_tag: "",
    meta_description: "",
    Articledata: [],
    topNews: []
  };
  scrollToMyRef = () => {
    console.log("sssss", this.myRef.current.offsetTop);
    window.scrollTo({
      top: this.myRef.current.offsetTop,
      behavior: "smooth"
    });
    //  window.scrollTo( this.myRef.current.offsetTop , behavior : 'smooth');
  };

  getContent = async slug => {
    try {
      const response = await pageService.getPageContent(slug);
      if (response.data.status == 1) {
        /* Banner-Slider Contents */
        if (response.data.data.content.bannerSlider) {
          const banner_content = [];
          response.data.data.content.bannerSlider.map((banner, index) => {
            const Setdata = {};

            Setdata.title = banner.heading ? banner.heading : "";
            Setdata.description = banner.subheading ? banner.subheading : "";
            Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
            banner_content.push(Setdata);
          });
          //console.log("this.homedata.imageSetdata", Setdata);
          this.setState({
            homedata: banner_content
          });
        }
        this.setState({ content: response.data.data.content });

        if (response.data.data.page_title) {
          this.setState({
            page_title: response.data.data.page_title
          });
        }
        if (response.data.data.meta_tag) {
          this.setState({
            meta_tag: response.data.data.meta_tag
          });
        }
        if (response.data.data.meta_desc) {
          this.setState({
            meta_description: response.data.data.meta_desc
          });
        }
      } else {
      }
    } catch (err) {
      this.setState({ spinner: false });
    }
  };
  componentDidMount = async () => {
    const slug = this.props.slug;
    this.getNews();
    this.getContent(slug);
  };

  getNews = async () => {
    this.setState({ spinner: true });
    try {
      const response = await newsService.getNewsAbout();
      if (response.status === 200) {

          console.log(response);
          const article_row = [];
          if (response.data.news.length > 0) {
        
            response.data.news.map((item, index) => {

              if(index < 3) {
                const SetNews = {};
                SetNews.title = item.title;
                SetNews.description = item.description;
                SetNews.slug = item.slug;
                SetNews.image = item.image;
                article_row.push(SetNews);
              }
            });            
          }
          this.setState({ topNews: article_row });
          const article_row1 = [];
          if (response.data.articles.length > 0) {
            
            response.data.articles.map((item, index) => {
              const SetArticle = {};
              if (index < 2) {
                SetArticle.title = item.title;
                SetArticle.description = item.description;
                SetArticle.slug = item.slug;
                article_row1.push(SetArticle);
              }
            });

          }
          this.setState({ Articledata: article_row1 });
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  render() {
    const images = require.context("../../../assets/images", true);
    const {
      page_title,
      homedata,
      Articledata,
      topNews,
      meta_tag,
      meta_description
    } = this.state;
    let newsImg = images(`./frontend/news-placeholder.png`);
    let video = "";
    video = require("../../../assets/videos/IPC_TableTopVideo_0ct4.mp4");

    return (
      <React.Fragment>
        <Helmet>
          <title>{page_title ? page_title : " "}</title>
          <meta
            name="description"
            content={meta_description ? meta_description : " "}
          ></meta>
          <meta name="keywords" content={meta_tag ? meta_tag : " "}></meta>
        </Helmet>
        <Header />
        <div className="container-fluid" id="home">
          <div className="home-video-section">
            <div className="sliderWrapper">
              <Homeslider categoryInfo={homedata} />
            </div>

            <div class="arrow_box">
              <Link
                onClick={this.scrollToMyRef}
                to="#"
                className="scroll-down-link"
              ></Link>
            </div>
          </div>

          {/* <div
            className="home-video-section"
            style={{ backgroundImage: "url(" + homedata.image + ")" }}
          >
            <div className="videoWrapper">
              <video
                width="100%"
                height="auto"
                autoplay="autoplay"
                playsinline="playsinline"
                muted="muted"
                loop="loop"
              >
                <source
                  src={siteUrl + "/images/Website.mp4"}
                  type="video/mp4"
                />
              </video>
            </div>
            <div className="photoWrapper">
              <div className="home_banner_bg"></div>
            </div>
            <div class="arrow_box">
              <Link
                onClick={this.scrollToMyRef}
                to="#"
                className="scroll-down-link"
              ></Link>
            </div>
            <div className="caption_fullwidth">
              <div className="container caption_container">
                <div className="slideshow_caption">
                  <h2
                    dangerouslySetInnerHTML={{
                      __html: this.state.homedata.title
                        ? this.state.homedata.title
                        : ""
                    }}
                  ></h2>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: this.state.homedata.description
                        ? this.state.homedata.description
                        : ""
                    }}
                  ></div>
                </div>
              </div>
            </div>
          </div> */}

          <div
            ref={this.myRef}
            className="future_auto"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container">
              <div className="middle_wrapper">
                <div
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section1
                      ? this.state.content.section1
                      : ""
                  }}
                ></div>
                <div className="iconss_section">
                  <div
                    className="single_icon_section row justify-content-between"
                    dangerouslySetInnerHTML={{
                      __html: this.state.content.section2
                        ? this.state.content.section2
                        : ""
                    }}
                  ></div>
                </div>
              </div>
            </div>
          </div>

          <div
            className="break-down-sec"
            dangerouslySetInnerHTML={{
              __html: this.state.content.section3
                ? this.state.content.section3
                : ""
            }}
          >
            {/*   <div className="container">
               <div className="break_sec_full"  dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}>        
              </div> */}

            {/* <div className="break_sec_full"  >
                <h2>BREAK DOWN OF CIMON PRODUCTS IN MARKETS</h2>

                <div class="row"

                  style={{ background: "#e5e6e7" }}

                >
                  <div class="col cbox1">
                    <div class="color_box c1"><img alt="" class="img-fluid" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAYAAACMRWrdAAAABHNCSVQICAgIfAhkiAAABT9JREFUaEPNmo2RFDcQhdURABEYIgAi8BGBIQJDBIYIjCMwF4EhAuMIzEXAEYEhAiCCpr5xS6WZ0U9LO3e1qtrauj1Jo6duvX7dGgkHN1X9JYTwyD53QwgXhUd8CCF8CyFc2+dKRPj7sCanzqSqLB4wT+0zOyUg34YQ3h0BchqYqt4PIfxuYAB3ZAPgHyLyeXbSYWBmIQC9nH3owDgAvpqx4BAwVcXd/gohHG2hFlbOHuAA6W5uYKr65y1Zqbb4Iet1gZnr/Wss596xG+oIwTzxuGYT2JmBinsFoTwTEUBWWxXYmYLKwT1uWa4F7OOZuF/NKk23LAJT1TchhN9OOCffQwjMgQJBedw5Ya7W0Pci8qzUYQfMKP1v50LemVoAxMNsDOrhefxbVQEHq/3knHekG6GA56/aCpidq/8cceoT1C8iaL6gqlgG1+V34s7r+L8M3E25Ns97sD1vW2BeF4RyF1CeZhv21dN3ss/OJRMw035Yy9M+iMgTT8fMYkgwgnxsnMMjzx5WS9oyB8YZ+NW5WBZ13xMo8/nMZV+bq16rKgs56tytzvUCbOBsxXUSQ5oB0rNBRiqomqNasloEBoMhbmsNUnhv1I0/71hodmWqyln9eXb8ZlxiyAiMRZMs1tqLnrrO8jMYkoZFuznVATEzX/NnEXnADxGYdnZsdTC3fVW1ZnGomE1h41bNNgIvKZUOTjHgslZxBORPIhKtsHugLZAYVcvRinHG8dxZcIt3AQyWIiOutWsReVz7p6puabzUdacOBsPLCMhLEXkJsN75YlJ2HdJ4UXApz3jOGhu4dcfeERgBFPtS8boAmJuVRKSkLXsW54E1YFEkM8dRwfqbiNwD2Miu7UjEGYuaEmxkcz0mxACjwIqBuePOK0VQWtjg5naxjQAjQD9t1fkq8Wg5yK2V3AQ7jgDr7jqLN2mWAnRNS5pmvCMiV9mGXJq6gajQrXl+17VS3mEEGOOGUpWOlUq5WVI3BpYaJiD5hnndQCMwLyumVIWLBxH5Z2gbrbOqojZQKmQIfOOqLPzt1sJ4QPxNVZFoHnDfReSuN45FDLgINfuY6g+Xn7MFuj3AXJcswFOBTnHME4dqxkmi02u9jCzcqY+pFCzmiXULH3i0YmvNX0QEC6ZmBILLxDuylcJXVeQbm7kaa1Yhw8YbSEDxhiSeTbphMVy3BTBpRTrP1iMWs2+AbT1g2UEL5Cw8suYqFFSy6ZIg6HHC/+reaNp7MEvWY1d5GCBJ90uMx/wRUCQNxPVSo2iol50U6+RvyQsiMI9C9x6jbr+t5szKd9uxKSswF6eIC/3X0qjkBXnNY9Ydu0AKHe4VqH3rYquCkVOhrGse5g4jVaoZMPmYWAhKV0JmETwnkgdF11ROa2Tpcd7VeZ+tK54KLI7vaskNMbU2fxUXt5Xg27SaS6ZF9dE4h8yzY+dS7R7zewLhEVaj/n8ZxXOh3h/lF8wLYaxiZraAXVgoZcS9GuMRgPI5cpkWcsZ05mnF7Lx2P3bbLpkDhUAQDVFltDZy54Kxcw0Yk0K/HjV9tAW9833BPWs5X+8OGlo+6tLAu2BPP2LcRev+oPfWAAeWg3tO4Lqg2Bnvex7n4pa4H7WX7k1PF5ipEs4chNK6uPC40Cl9rgyU6/U/F7C4GsuJjixueoEWKb01eAiYWY8gyf3YbVgPKz2feb1vGFhmPYiFmOO93vVaZ5FIpTcPRiaYBpYBxILkSIA8hT0hBhj4zYyFtqBPBrZR37GCFesdfJd0J5QNs6FLYVxKe9Nvk5YseSiwEVe56b4/AIJ2eVXuVntCAAAAAElFTkSuQmCC" />

                      <div class="text-box tbox1">
                        <h3 class="text_heading">25%<br />
Factory Automation</h3>
                      </div>
                    </div>


                  </div>

                  <div class="col cbox2">
                    <div class="color_box c2"><img alt="" class="img-fluid" src="https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1603741571125_Auto_mobile_icon.png" />
                      <div class="text-box tbox2">
                        <h3 class="text_heading">18%<br />
Automotive</h3>
                      </div></div>

                  </div>

                  <div class="col cbox3">
                    <div class="color_box c3"><img alt="" class="img-fluid" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADUAAAA1CAYAAADh5qNwAAAABHNCSVQICAgIfAhkiAAAA7ZJREFUaEPNWu1x1DAQfVsBUAFJBSQVQCogVBCogKQCuAoIFXCpIEkFSSogqYC7CggVLPPM2uM7y9bXmvHO+MeNdZKe9uvtygJHUdWXAN4COLKHv98FlrgH8Azg0Z4HEeFvF5HaWQzIewDnBqR0SgJdA7itBVgMSlUPAHwB8LEUxcT/CG4lIpuSubNBmWYIhpqZWy4NXJZpZoFS1VMAPwDQV/6XENAnEblJXTAJlGnn20ymlrrXSxG5SBkcBWWA7iqDQMpeUsYwWp7EAskkqIUBakFHgY2CWiigJGBBUAsH1AETkeOQzY6BYoTzzj9bAK9THCdjzHcRGaSWASgL29cZE6cMfTK6RH/wBvZhP9zvgDKz+zVDHjoWkUdVJQ9kJPUUsg7O3yXofVDM4J89VwSwYyKqyiRKrugpO2t0oIzLUUueQj866p+iWQNP94XnQgAOW67YB0USeea80MDeOb+q0rnJUDzlSkSa4NaAstP77bmClRDkikFRVZYarL28hD5FbT23oIiQYdxTOnMITTpT0CDxXbegfjpzO9ZCX2MnpKreJs8C81RmML0/AA5ipLNn9q5BQygzJNvGBGJaat+rKjXKotNLTgjKc9KtiLDMT5YZQvyKoDyTYZaWetryDPFXBOUVWrO11FenqtK3PHjhA0Fpsq1MDyzSUk9bXmll4wWqSkvekdAL1IWIkAxXiaq6EGovUK9S8lIMsRep9gDVZPHYhlPfqyoLyTep4wPjth7RL8jESzflwOCb6FebpyaJay44VeWNCbloqTR5qopRkGqVrj72v8o00zAK+kNxo2WBoBrux2Z/cYHoDao2AjYs3RJfTcRpOkVeJlhpOf/qKQNVQ1GSCsJU0JWF407lW2OCGxE5TN301LjKvmNXnHp1k7xoUk0k7izGq+/HTg7vjYp9yyE/Dft+5ls1hJL1EIFlXz4bILajS69dd/w61EuvaYRka8zhHnnQBZ7r1iP6yYBph13a0McjOXFn+tajV4XWmGF/Q/Sx9usWWgCbMjQxspisBs0IyrT7qR6wmoScc9KlY59EhOR3ILE7X55yTW1TuuHY/5pLvLHCNOV2fmnAJgHxNKJlg2X5pQCLAkoCNUPwiJnW2PtgUMjyqdBgyykM1963gFNAmYfO3b9N6q9o5kiO5n03HAK2AsBvkub7imwPHPMMwTHneGqObJt5cl1CubJ8asw+THMExqfm1v0WAJtAN7ma2d9bNPrlerVde5L6UJPt02/800fILtrnXkQYXd3kL5LivRrfDlYGAAAAAElFTkSuQmCC" />

                      <div class="text-box tbox3">
                        <h3 class="text_heading">15%<br />
Water Process</h3>
                      </div>
                    </div>


                  </div>

                  <div class="col cbox4">
                    <div class="color_box c4"><img alt="" class="img-fluid" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAA3CAYAAACo29JGAAAABHNCSVQICAgIfAhkiAAABGFJREFUaEPVmo1RFTEQgHcrUCpQKlAqECpQKlAqECsQKlArECoQKhAqECoQOsAK1vlu8s7cXe7dJpf33pkZRgdySb7s/96pVB5m9lJE3ojI6/DzPPwb7/QkInci8hB+blT1tvJRRGssaGaAvBeRdyICXOm4EpELVb0uXSB+bhacmQF0NhMoxYFkv4rIN1Xl/0WjCM7MDkXk+wag+hANpKqel9BlwZkZ9gMU6rfNgW2eqOpNzqZuuCCtHyIC4K7GWY4UXXBm9iFIbFdQ8b5I79hji5NwZoYaArekQRg5mgJcC7dQsNUlTwKOwi1MFce05k5VD8b+mIQLzuNnZT28FBFcOyr+rOLaxMLT1HoDuODuf1f0ikDh5XDnEtbnMPzUgsTBkN10RgoOd18jjnWg+htXhkQj9vsOpgNXSR1JgJGUK+BWhLxU1Y5X78OhjqWJbxZUQpLsS55Kvlo6DlQVL9qMFm6Gd5wFVRmyI70YLldqj3g+r/rliiLUhSWSxPYa59XAhXrsl/MAQGFTF875s6YFSPaiAPaMNjSs4KidPk48uVWohLpSZiHJKcgHVd2PJedRSXI5lwf0XG/pHDPznLVxLBrEzgNT41xVubmdjdyzAuctZwiUZBWNse5oYD70a6bGraoeAuext6nFlvb3J1XdAw47mjLS1OHpa6RskMt6FT3wKbTx+mvgAV9EvzwOiXU8j6qfdDB7KMPMCAEeUfc3IBOgr9FmBGZGTkpxG7ciSGiZ13axzOxz8HzxmrT0TuJfzKwnj4Cz7Gv590DHyazRgo6nXbNnO69CnrssODSpJ7k5Fz8brlNHZUgOVY7tsmFaGlxf3bCvtwk1789LOrFNwGHopRVx/9AEeZxFZyQOvTW40lAAQE24e1XteG0zS6qv0wHu4y2JN6UFYltesKGZeSWX2rPJKnoOpfjiV3EueSDP7STUjfTsi0MtU3vWhGvTL26rqI2XgEutlVK3TcM1Nd2qniuKJ064lERScNeq2um6zTCZJkSt4MZc+FrtrAw3KKnGbNhhMnukeys4biw3QU1JJKWW3nm14NomUdwgyol3ZPq8pG+T5tVthpwwvlzK/k4NGHqV/WQ9NY92H/NQ40FGMyLBNmuK4bxes+1RONSjypSMmvNRVdu+awxHmcINe7IVJDbozVchGS6C5LztfUqrtivXz8K90tsQx6xlByEn9SIE6cUV8qwdt/jwoDuXgisO6lsE6W+VfEc39vLxf1LPgTquyNe9Ni5OWrcowT+Ein6o8cDhPQH0xpctMjVbAXaYirWTcKGEWSrgJBjn93yHsjRAF5gLLkqrltCZviegj9nYoI7MMZTQdCUD8GQxOUt75o5+kjH28KRa9h8MSS9SLG1NeEDiOUjrtOT1WTZcpKbkfLQVNgU5+2VnMVwESRYOJMltjbSN71euUh/N5Ip8Nly8YXi3DiQpHJL12CZfQ1BlEFOpEYs/+53lUHJvLsTKTrsuWmNQnJasv+6Zv8c4OOExqnm2AAAAAElFTkSuQmCC" />
                      <div class="text-box tbox3">
                        <h3 class="text_heading">12%<br />
Building Automation</h3>
                      </div>
                    </div>


                  </div>

                  <div class="col cbox5">
                    <div class="color_box c5"><img alt="" class="img-fluid" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAYAAACMRWrdAAAABHNCSVQICAgIfAhkiAAAButJREFUaEPdmmlsVFUUx3+HLnSKlNLSikWwGheMosEVl0SicSG4VhPlkxJNhPhB/aRG4xbXTy4xUb9pjIqJGNeIn1yCUVFUBBEVBaoUENpOW20pXa75T+dN3wydvvtmpiCepJmk72z/d98595xzr1Fics6dAVwCzAGOAGamf2cDncAOYGf6tw34GvjIzLpL6YoVq8w5lwAuBq4AFqdBxFU7AHwGvAe8bWbb4irI5S8YWBrQ7cBdQG2xjuTIvw88ZGbfFKo3NjDnXDlwM3A/0FSoYQ85B7wJ3Gdmv3jwZ7HEAuacU5zI2FlxDRXB3wcsN7OX4+jwBuacuwx4FaiLY6CEvC+lAe710RkJzDlXBjySjqVIfh+jRfCsB1rMbHOUjnEddc4dDrwBXBCl6AA+7wGWmtnK8WzmBZZOEp8A5x1Ap31NaXu40sxW5RMYD9gLwK2+lg4CX5eSWL6MOSYw59wdwFMHwdm4Jn9Og9uvatkPmHNO5dCHwKS4Vg4Sv3xdbGba9zKUBcw5NxnYmq7vDpKfBZlVMtF2kBfYofIJ5qJX+p9rZkPBg8yKHcKrFWBRdaKEl6IwsLuBx6M+hBVbNrJtyDi/roE5iWpmJ6qjRAp6vqGni9beXlZ37OaxE0/x0aF26BgzS1UmKWDOuRqgFZgWpeGV3zZw+y/716Tn1c3IEp1WUcG8mvGL/tXtu7NkugYGEKBc6ljUEuVW8PxOM3s6DEx1oLJLJOUDFilYBEMMYGpYhSWzYt6bcVtfD8vWfMLve/tpG564HaHcDdFYZiycXsdzZ13o+1r2AQ3qxoNPUS262vhI+nRHK8nhYc6sa6QpHV+r/vyd3f29GdmPdm6P1BMw1FZUcHZ9Y4b/pNoGTqtXiQo/9yTZ1J3kqlnN3vqAJWa2wpxzC4AvfCVzP0UlDyURkU9c5bMTjrfWvl7+6Bt9UTE+Ral/w8xuELAHgQcKBeYrVwxfTGDKPg0CtgK43tfwt+27WLZ2NVsHhxk0tWoTRwk3xPFVFXx84dVxjZwoYGpNvPutVbt2UltZwYLp9Slj+jRFe/r3srazfUwHugf3pZKNqLl8ErVVU8bkO316PTMmV3FYWTnXNM9N8bzZ9gfXNWkiEYvOEbBNwAm+Ys9v/ZV7f1IjS2pzVnyNFVvzptam/h9F67uTdA2qvRohbcqtff8Q3tNifopSs0jANLwcSUMeFAbmwV4SlgKALREwlSCq6r3oi852Wtaspn84U296yRXKNHlSGTsuvSqu+J0ClvQppQLN33Z1cmSimsbKyWj1RB/s0qKPkOJpWyhV+3hUX1FJU9Vozbn4cE3FYXnzcakS6+SpkZVerpmlBcdYEF+qB4NYUmFcKCmm1vfoHY/GWbCfFfAppmIsVlY8RGJsvoCp87zR902/1badW9Z95cteAj6jY9E1cfUcIWBPpIehXsJrkh00V09hZVsrf/X383VSJ0Ow+e9u+txwlo5/BgYZJmsUkXmupFBZll1Eh2Nt4YwZHJOooSlRldkzvRyEQaBKwG4AXvcUSiWMJ3/dxLyaaZxfPxJTuXtWbm82nu7uwQHWd4/2YNrDFFupmOtO8nnHHmLG2FdmtkDA1GSq46v0AXcIxNg9ZvZE0LZoonrp/wTYfDP7PgC2DHjeB1hbfx9fdrSzrquTtV1J2vb20j6g/m6EegZGyyMffQHP1FD5dVSimprySi5qaGT+tOksDPVrETp3mFnqzC4Apk5ui48j+hRf/7M1FV/av4K9qxSDHW3Gii2RhjjBMGfdwlS370MvmpkWKWtK9TYQWbsUEmPhZlSJQgkjDnkmD9V4p5rZj7nANONaF2WwEGBROqOeewJ72cxuCnTljri9mk6BU30YxNe+oeGSFcVBrCnO5iSm0DJzNi1Ns6Kwq5DXJDhz2yAX2NGAKtuiW2N1Ad93dYzr0OUzZzE7VPxGeT/O82fMTOP5DI112uI9iivCkVKK6gjpWDPLmr6OBWwqoPsVx5fS+gTqutbM3srVn+/g7zjgc017JtChUqjWJRdN2faj8Y5qzwXU0kQPLkrhYnwdK83sunxiUbcGvCuS+H4VJfEDcHZwshJrxQJm59yVmrIBKpb/C/ROeoytGzt5yetCinNO4zkVyrGG6CV+C2rsdH/r0dzz5oJWLLRyumqka3eKvQNNGuTrsOFdX8NeKxYCp5tvS4CHD9DqqSV/DbjXzHQw6U2xgIUAqilVYrlvgrYEfXbam+43s43eaEKMBQELAdRmfhugtHt6IQ7kyGj+pi7jWTP7rhh9RQELG05fKFPboyu0FwG6UutDajN0o1Tx+2X4SoOPcD6ekgHLNeCc0wlp8KfRrv50cy246Kzf7WY2esJXDJIc2X8BtF+dnAMR+CsAAAAASUVORK5CYII=" />
                      <div class="text-box tbox4">
                        <h3 class="text_heading">10% Chemical,<br />
Oil Gas</h3>
                      </div>
                    </div>


                  </div>

                  <div class="col cbox6">
                    <div class="color_box c6"><img alt="" class="img-fluid" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADUAAAA1CAYAAADh5qNwAAAABHNCSVQICAgIfAhkiAAABVpJREFUaEPlmo1x1DAQhXcrACoAKgAqIKmAUAGhAqACkgqACkgqACqAVECogKQCoIJlPrHy6GzZXtl3cIBmbhInsqS3v29Xp7LFYWY3ReShiNz3D88HlS0+ich3Ebn0z4Wq8ryVoWtXcSCPROS5A1m6JEDPROTDWoCLQZnZHRF5KSLHS1FMvAe4U1W9WrJ2MyjXDGDQzK7HawfXZJpNoMzsSETeigi+8rsGgJ6q6vvohiFQrp1XOzK16Flfq+qLyORZUA7o48ogEDlLZA7R8nAukEyC2jNAGfQssFFQewooBKwKas8BdcBU9UHNZsdAEeG2lX+unT3cE5ELESG/3Y44UGDOG1UdpJYBKA/b7wILjk0BBOH3meeYEzODKn1U1bSfmZFcn4jIudOoNSAf98P9Big3u68L89AXESHsnpnZZ6c7Jw5iA5T/jf89woTMDKvgeQk4WMeDMiL2QZHBkXDrgNJkAJjDkap2RLavqby4mcH33qsq+6JB1oCttI4NM+xAOZdDS62jU3+haSTX8bYJULB5cuDdLGk3f8zzRuNBWCPtWYLKdh5d64eIIGkCAMAuzSwxdVXdCDJjoAr/+oTZ+jOWguZYvwXYed43Oy5c7lsUjc9LGipMBtMBTAJYrjUDCjOFgj12XkmYhmNyppaABUdMGs+gOAxhPDo6HyoCAdJFa4BDarPm5+9iggQWBtHweWGKrT4O8T3LoFiUxSPjWlXJNRvDNZaDA9Uv2sI8+YnUOWD+f66Meb7leewsB5uelhFONCpSYB6pO3eL6SVpVECRmzgYJgloDsyH3wGR/YM8xkETaJ+PuRExBwnfw33YisiFgGLBqO1WteRmROSEQQ+q1Smf8ncB/m6M9phZi7YOAdWSG6q0xDWD1AdmWfhcxyhqNm5mOPqdWllhZi2+dQoozIbGSWSQfzYiW3HokzLhRqNfnueJmDXww76/tljTOaBYBMeeHZm7VTbFd3YGygVnswf8NeECUNHJX1S1GiHdmTGdRJVGQM+ZH8GHQDPQlIPCPCPJ+KoFFA3HWmMyczZZCSoJZGKNuEU1aCqo/T8/7b/X1JWq3h3xGZLmrn0KghDpN17/s9Hvb8hTqXIOemvKU9tiFITsMfMclPOVsI95dcViL3k3M4qWbD3lV/Czg4Xcj/xHjhrLg/DKKgWraC9xv9YCMcrSoV5oqH/xVl62Qbk+eFGIQLbD0j1bszgFXmRUteWlPEAAgcTpLuGvrI2UqW4PfYOyNKFWQsvUQoMenpm1aOlXPeWgtlH5UvNw2FNvlXV3SjPlfFn5IgQsIb3byM55ZaPybTVBFsg9CjSAhAGDcCj2WnsUBAJ8m5+Q66eu8XBx6I2aVLqs6SZl36AcP17RTUI79P5yNylbDetHkm12ma5vsou+H/5Bm2y28eJmCaiuOFxxWzns+y204SwlainMDz/AFGkn56DA36p5ytvTVNNZS3Rnq+XLTATb6G7VeulIOFK39PfhPcCdmxk+hUlN9dLxF6R7YGZcFjA3movKvWnkYBldYNrFrQfgqH1S0x8NVm49AERgwPRyxymSTmpzpm898hsLQunUgQCJFAndgEUbSzRS2yN2P1UAa0nIS6W85r3R9sLcnW++AFiz+S7eha1Aq6pfGonczu8bsElASDD6PYp9ATYLKARqR8FjiUlWg0JtoVlNlS+tuOVbAiK/Qx7ieme7303qAYOPkSiX3A23ghsw/sgCTZrqgSPXAI4kuoSBjJ2Pa1HYOpXw7/m+X/8kXjmn+6WGi4YaICrgzNibvt/XX2yxpsbE7JQoU5/MHsqbwHzphhYSpRrrn0dMrTbnJ2hSPDjVMPsGAAAAAElFTkSuQmCC" />
                      <div class="text-box tbox5">
                        <h3 class="text_heading">20%<br />
ETC</h3>
                      </div>
                    </div>


                  </div>
                </div>


              </div> 
            </div>*/}
          </div>

          <div
            className="pixel_map"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div
                className="full_content"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section4
                    ? this.state.content.section4
                    : ""
                }}
              ></div>
            </div>
          </div>

          {/*           
          <div className="success_slider" data-aos="fade-up" data-aos-duration="2000">
            <div className="container">
              <h2>CIMON NEWS</h2>
            </div>

            <SilckSilder banners={this.state.content.newsSilder} />
            <div className="container">
              <div className="full_slidercontent">
                <div className="slider_section" dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}>
                </div>
              </div>
            </div>
          </div>
   
   
    */}

          <div
            id="home-section-bussiness-trust"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div className="row">
                <div
                  className="col-lg-12 col-md-12 col-sm-12"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section6
                      ? this.state.content.section6
                      : ""
                  }}
                ></div>
                {/* <div className="container my-4">
                  <div
                    id="logo_slider"
                    className="carousel slide carousel-multi-item"
                    data-ride="carousel"
                  >
                    <div className="carousel-inner" role="listbox">
                      <div className="carousel-item active" dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}>
                      </div>
                      <div className="carousel-item" dangerouslySetInnerHTML={{ __html: this.state.content.section7 ? this.state.content.section7 : '' }}>
                      </div>
                    </div>

                    <div className="controls-top">
                      <a
                        className="carousel-control-prev"
                        role="button"
                        href="#logo_slider"
                        data-slide="prev"
                      >
                        <i className="fa fa-chevron-left"></i>
                      </a>
                      <a
                        className="carousel-control-next"
                        role="button"
                        href="#logo_slider"
                        data-slide="next"
                      >
                        <i className="fa fa-chevron-right"></i>
                      </a>
                    </div>
                  </div>
                </div> */}
                {/* <div className="col-lg-12 col-md-12 col-sm-12" dangerouslySetInnerHTML={{ __html: this.state.content.section8 ? this.state.content.section8 : '' }}>
                </div> */}
              </div>

              <div
                className="col-md-12 col-lg-12"
                dangerouslySetInnerHTML={{
                  __html: this.state.content.section7
                    ? this.state.content.section7
                    : ""
                }}
              ></div>
            </div>
          </div>

          <div
            id="home-section-news"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div className="row">
                <h2>CIMON MEDIA</h2>
                <div className="near-heading_section col-md-12">
                  {" "}
                  <h5>NEWS</h5> <Link to="/cimon-news">View all news</Link>
                </div>
                {topNews.map((news, idx) => (
                  <div className={"col-md-4 p" + idx}>
                    <Link to={"/cimon-news/" + news.slug}>
                      <div className="single-news">
                        <img
                          className="img-fluid"
                          src={news.image ? news.image : newsImg}
                        />
                        <h3>{news.title}</h3>

                        <p>{news.description}</p>
                        <Link to={"/cimon-news/" + news.slug}>
                          View Details
                        </Link>
                      </div>
                    </Link>
                  </div>
                ))}
              </div>
            </div>
          </div>

          <div
            id="home-section-article"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div className="row">
                <div className="col-md-12 col-lg-12">
                  <div className="near-heading_section">
                    {" "}
                    <h5>Articles</h5>{" "}
                    <Link to="/cimon-news">View all articles</Link>
                  </div> 
                  {Articledata.map((news, idx) => (
                    <div className="single_article">
                      <Link to={"/cimon-news/" + news.slug}>
                        {" "}
                        <h3>{news.title}</h3>
                      </Link>
                      <p>{news.description}</p>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>

          <div
            id="home-section-join-industry"
            data-aos="fade-up"
            data-aos-duration="2000"
          >
            <div className="container max_width_1080">
              <div className="row">
                <div
                  className="col-lg-12 col-md-12 col-sm-12"
                  dangerouslySetInnerHTML={{
                    __html: this.state.content.section9
                      ? this.state.content.section9
                      : ""
                  }}
                ></div>
              </div>
            </div>
          </div>

          <SubFooter />
        </div>

        <Footer />
      </React.Fragment>
    );
  }
}

export default Home;
