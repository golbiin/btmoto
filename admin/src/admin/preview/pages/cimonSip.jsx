import React, { Component } from 'react'
import Header from './../../../frontend/common/header'
import Footer from './../../../frontend/common/footer'
import { Link } from 'react-router-dom'
import { Helmet } from 'react-helmet'

import SubFooter from './../../../frontend/common/subFooter'
import * as pageService from "./../../../ApiServices/page";

const TITLE = 'SIP | Automation Company - Industrial Automation'

class SiProgram extends Component {
  constructor (props) {
    super(props)
    this.myRef = React.createRef()
  }
  scrollToMyRef = () => window.scrollTo(0, this.myRef.current.offsetTop)
  state = {
    content: {},
    // list: [
    //   {
    //     iconId: 'imgIcon1',
    //     value: 'Heavily reduced pricing on all CIMON products',
    //     text:
    //       ''
    //   },
    //   {
    //     iconId: 'imgIcon2',
    //     value: 'License to resell CIMON products',
    //     text:
    //       ''
    //   },
    //   {
    //     iconId: 'imgIcon3',
    //     value: 'Free comprehensive training on all CIMON Hardware/Software',
    //     text:
    //       ' '
    //   },
    //   {
    //     iconId: 'imgIcon4',
    //     value:
    //       'Technical Support from CIMON Global Support via phone, e-mail, and web',
    //     text:
    //       '  '
    //   },
    //   {
    //     iconId: 'imgIcon5',
    //     value: 'Two Training seats offered each year',
    //     text:
    //       ' '
    //   },
    //   {
    //     iconId: 'imgIcon6',
    //     value: 'Access to the CIMON Online Knowledge Database and resources',
    //     text:
    //       ' '
    //   },
    //   {
    //     iconId: 'imgIcon7',
    //     value: 'Newsletters and technical bulletins',
    //     text:
    //       '  '
    //   },
    //   {
    //     iconId: 'imgIcon8',
    //     value: 'Developer briefing about upcoming updates and upgrades',
    //     text:
    //       ' '
    //   },
    //   {
    //     iconId: 'imgIcon9',
    //     value: 'Promotional eligibility in CIMON Quarterly Newsletter',
    //     text:
    //       ' '
    //   },
    //   {
    //     iconId: 'imgIcon0',
    //     value: 'Listing on our Integrator partner web page and map',
    //     text:
    //       ' '
    //   },
    //   {
    //     iconId: 'imgIcon11',
    //     value:
    //       'Lead generation and referral as based on project location, specialty and scope',
    //     text:
    //       ' '
    //   },
    //   {
    //     iconId: 'imgIcon12',
    //     value: 'Exclusive product release insights',
    //     text:
    //       ' '
    //   },
    //   {
    //     iconId: 'imgIcon13',
    //     value: 'Special discount on CIMON Starter Kits <span>(*Only 1 starter kit per company)</span>',
    //     text:
    //       ' '
    //   }
    // ]
  }
  componentDidMount = async () => { 
    var slug = this.props.slug;
     this.getContent(slug);
}


getContent = async (slug) => {

  try {
    const response = await pageService.getPageContent(slug);
    if (response.data.status == 1) {
      /* Banner-Slider Contents */
      if (response.data.data.content.bannerSlider) {
        const Setdata = {};
        response.data.data.content.bannerSlider.map((banner, index) => {
          Setdata.title = banner.heading ? banner.heading : '';
          Setdata.description = banner.subheading ? banner.subheading : '';
          Setdata.image = banner.image ? banner.image : "ipc_banner.jpg";
        });
        console.log('this.homedata.imageSetdata', Setdata);
        this.setState({
          homedata: Setdata,
        });
      }
      this.setState({ content: response.data.data.content });

      if (response.data.data.page_title) {
        this.setState({
          page_title: response.data.data.page_title
        });

      }
    } else {

    }
  } catch (err) {
    this.setState({ spinner: false });
  }
};




  render () {
    const images = require.context('./../../../assets/images/frontend', true)
    return (
      <React.Fragment>
        <Helmet>
          <title>{TITLE}</title>
        </Helmet>
        <Header />
        <div className='container-fluid'>
          <div id='sip'>
            <div className='bannerWrapper'>
              <div className='sip_banner_bg'>
                <div className='container'>
                  <div className='hedng'>
                    <h1>CIMON S.I. PROGRAM</h1>
                  </div>
                  <Link
                    onClick={this.scrollToMyRef}
                    to='#'
                    className='scroll-down-link'
                  ></Link>
                </div>
              </div>
            </div>
            <div className='container'
              dangerouslySetInnerHTML={{ __html: this.state.content.section1 ? this.state.content.section1 : '' }}
            >
              {/* <div className='top_header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center'>
                <h1>CIMON Certified System Integrator Program</h1>
                <img src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_9_1602089410697_Tech_integrator_compressed.jpg' />
                <h5>
                  Our CIMON Certified System Integrator program allows you to
                  take advantage of tremendous savings on CIMON brand hardware
                  and software as well as receive unlimited free support and
                  service benefits. Those who enroll and complete the integrator
                  certification requirements enjoy massive benefits with minimal
                  time and expense. This is the program designed to jumpstart
                  integration projects and planning in a whole new way.
                </h5>
              </div> */}
            </div>
            <div className='icon-list-container' ref={this.myRef}>
              <div className='container'>
                <div className='icon_section row'
                 dangerouslySetInnerHTML={{ __html: this.state.content.section2 ? this.state.content.section2 : '' }}
                >
                  {/* <div className='col-md-12'>
                    <h3>Features</h3>
                    <ul className='icon-list'>
                      {this.state.list.map((item, index) => (
                        <li className='iconlist_li' key={index}>
                          
                          <div className='iconlist_content_wrap'>
                            <h4 className='iconlist_title'
                            dangerouslySetInnerHTML={{
                              __html:
                              item.value != null
                                  ? item.value
                                  : "",
                            }}
                            
                            ></h4>
                           {item.text ?
                            (<p>{item.text}</p>) : '' 
                           }
                           
                          </div>
                        </li>
                      ))}
                    </ul>
                  </div> */}
                </div>
              </div>
            </div>
            <div className='sipTextblock'>
              <div className='container'>
                <div className='row' 
                  dangerouslySetInnerHTML={{ __html: this.state.content.section3 ? this.state.content.section3 : '' }}
                >
                  {/* <div className="sip_text1 col-md-12">
                    <h5>
                      CIMON is pleased to announce this exciting opportunity
                      specifically designed with the system integrator in mind.
                    </h5>
                    <p>
                      Our CIMON Certified System Integrator program allows you
                      to take advantage of tremendous savings on CIMON brand
                      hardware and software as well as receive unlimited free
                      support and service benefits. <br />
                      Those who enroll and complete the integrator certification
                      requirements enjoy massive benefits with minimal time and
                      expense. This is the program designed to jumpstart
                      integration projects and planning in a <br />
                      whole new way.
                    </p>
                  </div>
                 */}
                  {/* <hr />
                  <div className='sip_text2 col-md-12'>
                    <h5>Requirements:</h5>
                    <p>
                    To qualify, integrators must complete at least two free CIMON software training sessions and purchase a CIMON starter kit within 
60 days of enrollment. That’s it! There are no further costs or fees associated with this program!
 
                    </p>
                   
                  </div>
                  <div className='sip_text3 col-md-12'>
                    <p>
                      If you have questions or concerns, send us an email at
                      &nbsp;
                      <Link to='mailto:sales@cimoninc.com'>
                        sales@cimoninc.com
                      </Link>
                      , or get in touch with our sales team, who are always
                      happy to address your needs. We encourage you seize this
                      opportunity to receive tons of perks, discounts, and other
                      benefits you can find only at CIMON.
                    </p>
                  </div> */}
                </div>
              </div>
            </div>
{/* 
            <div
              id='home-section-bussiness-trust-Si'
              data-aos='fade-up'
              data-aos-duration='2000'
            >
              <div className='container max_width_1080'>
                <div className='row'>

                <div
                  id='logo_slider'
                  className='carousel slide carousel-multi-item'
                  data-ride='carousel'
                >
                  <div className='carousel-inner' role='listbox'>
                    <div className='carousel-item active'>
                      <div class='row'>
                        <div class='col-md-2 col-sm-1'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558445616_download%20%2810%29.png'
                          />
                        </div>

                        <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558469097_download%20%2811%29.png'
                          />
                        </div>

                        <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558491577_download%20%2812%29.png'
                          />
                        </div>

                        <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558518387_download%20%2813%29.png'
                          />
                        </div>

                        <div class='col-md-2 col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558541108_download%20%2814%29.png'
                          />
                        </div>

                        <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558564714_download%20%2815%29.png'
                          />
                        </div>
                      </div>
                    </div>
                    <div className='carousel-item'>
                      <div class='row'>
                        <div class='col-md-2 col-sm-1'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558445616_download%20%2810%29.png'
                          />
                        </div>

                        <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558469097_download%20%2811%29.png'
                          />
                        </div>

                        <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558491577_download%20%2812%29.png'
                          />
                        </div>

                        <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558518387_download%20%2813%29.png'
                          />
                        </div>

                        <div class='col-md-2 col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558541108_download%20%2814%29.png'
                          />
                        </div>

                        <div class='col-md-2  col-sm-1 clearfix d-none d-md-block'>
                          <img
                            class='img-fluid'
                            src='https://cimon-rt.s3.amazonaws.com/profile_cimon/2020_7_1598558564714_download%20%2815%29.png'
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='controls-top'>
                    <a
                      className='carousel-control-prev'
                      role='button'
                      href='#logo_slider'
                      data-slide='prev'
                    >
                      <i className='fa fa-chevron-left'></i>
                    </a>
                    <a
                      className='carousel-control-next'
                      role='button'
                      href='#logo_slider'
                      data-slide='next'
                    >
                      <i className='fa fa-chevron-right'></i>
                    </a>
                  </div>
                </div>


                </div>
              </div>
            </div> */}

            <div className='grey_bgsection'>
              <div className='container grey_cont'>
            
                <div className="row award_section"
                 dangerouslySetInnerHTML={{ __html: this.state.content.section4 ? this.state.content.section4 : '' }}
                >
                  {/* <div className="col-md-12">
                    <h3>BUSINESSES TRUST CIMON</h3>
                    <ul>
                      <li><img
                      className="img-fluid"
                      alt=""
                      src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605699938655_1605699938635-Gernal_motor_logo.png"
                    ></img></li>
                    <li><img
                      className="img-fluid"
                      alt=""
                      src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605704314105_1605704314067-logo_samsung_logo.png"
                    ></img></li>
                      <li>
                        <img
                        className="img-fluid"
                        alt=""
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605704314104_1605704314061-logo_lg_logo.png">
                        </img>
                      </li>
                      <li>
                        <img
                        className="img-fluid"
                        alt=""
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605704314102_1605704314057-logo_hyundai_logo.png">
                        </img>
                      </li>
                      <li>
                        <img
                        className="img-fluid"
                        alt=""
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605704314103_1605704314058-logo_kia_logo.png">
                        </img>
                      </li>
                      <li>
                        <img
                        className="img-fluid"
                        alt=""
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605704314092_1605704314050-logo_hankook_logo.png">
                        </img>
                      </li>
                      <li>
                        <img
                        className="img-fluid"
                        alt=""
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605704314104_1605704314061-logo_korean_air_logo.png">
                        </img>
                      </li>
                      <li>
                        <img
                        className="img-fluid"
                        alt=""
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605704314103_1605704314058-logo_korail_logo.png">
                        </img>
                      </li>
                      <li>
                        <img
                        className="img-fluid"
                        alt=""
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605704314104_1605704314062-logo_posco_logo.png">
                        </img>
                      </li>
                      <li>
                        <img
                        className="img-fluid"
                        alt=""
                        src="https://cimon-rt.s3.amazonaws.com/profile_cimon/1605704314103_1605704314060-logo_korea_telecom_logo.png">
                        </img>
                      </li>
                    </ul>
                    <p style={{'font-weight' : '300' }}>Many clients trust in CIMON for their industrial automation needs. Regardless of the industry, we have an integration solution waiting.</p>
                    <Link to="/our-customers"  
                    style={{'font-weight' : '300' }}>View More</Link>
                  </div> */}
                </div>


                <div className='row certify_section'
                 dangerouslySetInnerHTML={{ __html: this.state.content.section5 ? this.state.content.section5 : '' }}
                >
                  {/* <div className='col-md-12'>
                    <h3>Awards and Distinctions</h3>

                    <img
                      className='img-fluid'
                      alt=''
                      src='https://cimon-rt.s3.amazonaws.com/profile_cimon/1605705167956_1605705167948-awards-distinctions.png'
                    ></img>
                  </div> */}
                </div>
                {/* <div className="row customers_section">
                  <div className="col-md-12">
                    <h3>Major Customers</h3>

                    <img
                      className="img-fluid"
                      alt=""
                      src={images(`./customer_03.png`)}
                    ></img>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        </div>
        <SubFooter />
        <Footer />
      </React.Fragment>
    )
  }
}
export default SiProgram
