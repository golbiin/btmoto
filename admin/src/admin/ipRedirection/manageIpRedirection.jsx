import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as IpRedirectionService from "../../ApiServices/admin/manageIpRedirection";
import DataTable from "react-data-table-component";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import MultiSelect from "react-multi-select-component";
import moment from "moment";
import { Countries } from '../../countries';
import { States } from '../../states';
class IpRedirection extends Component {
  state = {
    id: "",
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    table_message: "",
    table_message_type: "",
    addresses: [],
    delete_status: false,
    edit_status: false,
    data_table: [],
    spinner: true,
    IpRedirectioncode: "",
    amount: "",
    discounttype: "",
    exiprydate: "", //moment(Date.now()).toDate(),
    products: [],
    Ips_selected: [],
    AllIps: '', 
    IpRedirectionstatus: "1",
    Allpages : [],
    except : false,
    AllStateIps : [],
    StateIpselected: [],
    ip : "",
    columns: [
      
      {
        name: "Location",
        selector: "country",
        sortable: true,
      },
      {
        name: "From",
        selector: "from",
        sortable: true,
      },
      {
        name: "To",
        selector: "to",
        sortable: true,
      },


      {
        name: "Status",
        selector: "status",

        cell: (row) => (
          <div className="verify_chekbox">
           <div className="custom-control custom-switch">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id={row._id}
                    defaultChecked={row.status == 1 ? true : false}
                    onChange={() => this.changeVerifyStatus(row._id, !(row.status == 1 ? true : false), row.index)}
                  />


                  <label
                    className="custom-control-label"
                    htmlFor={row._id}
                  ></label>
                </div>
          </div>
        ),
        center: true,
        hide: "sm",
      },
    
      {
        name: "Action",
        cell: (row) => (
          <div className="action_dropdown" data-id={row._id}>
            <i
              data-id={row._id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            ></i>
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >
              <a
                onClick={() => this.getContactById(row._id)}
                className="dropdown-item"
              >
                Edit
              </a>
              <a
                onClick={() => this.handleRemoveContact(row._id)}
                className="dropdown-item"
              >
                Delete
              </a>
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px", // custom width for icon button
      },
    ],
  };

  componentDidMount = () => {
    this.getAllpages();
    this.getAllIpRedirections();
  
  };

  getSelectedRedirection = async () => {
    const data_state_row = [];
    const data_selected_row = [];
    Countries.map((country_single, index) => {
     
  
      const Setdata = {};
      Setdata.value = country_single.value;
      Setdata.label = country_single.label;
      if (this.state.AllIps.indexOf(country_single.value) > -1) {
        data_selected_row.push(Setdata);
      }

      console.log('data_selected_row',this.state.AllIps , data_selected_row);
   
    });

    States.map((state_single, index) => {
     
  
      const Setdata = {};
      Setdata.value = state_single.value;
      Setdata.label = state_single.label;
      if (this.state.AllIps.indexOf(state_single.value) > -1) {
        data_state_row.push(Setdata);
      }

      
   
    });
  
    this.setState({ Ips_selected : data_selected_row  ,
      StateIpselected : data_state_row
    });
  };


  getAllpages = async () => {
    this.setState({ spinner: true ,
      data_table: []
    });
    try {
      const response = await IpRedirectionService.getAllpages();
      if (response.data.status == 1) {
       
        this.setState({ Allpages: response.data.data });
        
      } else {
        this.setState({ Allpages: [] });
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };


  changeVerifyStatus = async (id, status, index) => {

    console.log('changeVerifyStatus' , id, status, index);
    this.setState({ spinner: true });
    const response = await IpRedirectionService.updateIpRedirectionStatus(id, status);
    if (response.data.status == 1) {
      this.getAllIpRedirections();
    } else {
      this.getAllIpRedirections();
    }
 
  };
  getAllIpRedirections = async () => {
    this.setState({ spinner: true ,
      data_table: []
    });
    try {
      const response = await IpRedirectionService.getAllIpRedirections();
      if (response.data.status == 1) {
       
        const Setdata = { ...this.state.data_table };
        const data_table_row = [];
        response.data.data.map((add, index) => {
     
          const Setdata = {};
          Setdata.country = add.country.join();
          Setdata.from = add.fromtitle;
          Setdata.to = add.totitle;
          Setdata._id = add._id;
          Setdata.status = add.status == 1 ? true : false;
          data_table_row.push(Setdata);
        });      

        this.setState({
          data_table: data_table_row,
        });
        
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  schema = {
    country : Joi.array().min(1).items(Joi.string()).required()
    .error(() => {
      return {
        message: "This field is a required.",
      };
    }),   
    fromurl: Joi.alternatives().try(Joi.array().items(Joi.string()), Joi.string())
      .required()
      .error(() => {
        return {
          message: "This field is a required.",
        }; 
      }), 
      tourl: Joi.alternatives().try(Joi.array().items(Joi.string()), Joi.string())
      .required()
      .error(() => {
        return {
          message: "This field is a required.",
        };
      }),   
      ip :  Joi.string().allow(""),
      except : Joi.any().allow(""),
      id: Joi.string().allow(""),
     
  };
  validateProperty = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };
  handleChange = (input) => (event) => {
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty(input, event.target.value);
    if (errorMessage) errors[input] = errorMessage;
    else delete errors[input];
    let val = event.target.value;
    if(input == "amount") {
      val = +parseFloat(val).toFixed(2)
    }
    this.setState({ [input]: val, errors });
  };

  getContactById = async (id) => {
    const response = await IpRedirectionService.getIpRedirectionById({ id: id });
    if (response.data.status == 1) {
      this.setState({
        edit_status: true,
        submit_status: false,
        message: "",
        message_type: "",
        delete_status: false,
        fromurl: response.data.data.from,
        tourl: response.data.data.to,
        id: response.data.data._id,
        AllIps : response.data.data.country ? response.data.data.country : '',
        StateIpselected : response.data.data.state,
        except : response.data.data.except == 'true' ? true : false,
        ip : response.data.data.ip
      });
     this.getSelectedRedirection();
    }
  };

  addIpRedirection = () => {
    this.setState({
      fromurl: "",
      tourl: "",
      submit_status: false,
      message: "",
      message_type: "",
      delete_status: false,
      edit_status: false,
      except : false,
      StateIpselected : [],
      AllIps : [],
      ip : ""
    });
  };

  saveIpRedirection = async (e) => {
    e.preventDefault();
    console.log('----- saveIpRedirection',this.state.AllIps);
    const checkState = {
      country : this.state.AllIps,
      fromurl: this.state.fromurl,
      tourl: this.state.tourl,
     
    };
    const errors = { ...this.state.errors };    
    delete errors.validate;
    this.setState({ errors: {} });  
    let result = Joi.validate(checkState, this.schema); 
      

   
    console.log('result-->' , result ,checkState);
    
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;      
      errors[path] = errormessage;
      this.setState({ errors });
    } else {

      let fromtitleTitle = this.state.Allpages.filter(l => {
        return l._id.toLowerCase().match(this.state.fromurl);
      });
      let tourlTitle = this.state.Allpages.filter(l => {
        return l._id.toLowerCase().match(this.state.tourl);
      });
  
      const category_data = {
        fromurl: this.state.fromurl,
        fromtitle : fromtitleTitle[0].title,
        tourl: this.state.tourl,
        totitle : tourlTitle[0].title,
        id: this.state.id,
        country : this.state.AllIps,
        state : this.state.AllStateIps,
        except : this.state.except ,
        ip : this.state.ip
      };
      try {
        this.setState({
          submit_status: true,
          message: "",
          message_type: "",
        });
        if (this.state.edit_status === true) {         
          const response = await IpRedirectionService.updateIpRedirection(category_data);
          if (response) {
            this.setState({ submit_status: false });
            if (response.data.status === 1) {
              this.setState({
                message: response.data.message,
                message_type: "success",
                IpRedirectioncode: "",
                id: "",
                edit_status: false,
                fromurl: "",
                tourl: "",
                ip : '',
                except : false
              });
              this.getAllIpRedirections();
            } else {
              this.setState({
                message: response.data.message,
                message_type: "error",
              });
            }
          } else {
            this.setState({ submit_status: false });
            this.setState({
              message: "Something went wrong! Please try after some time",
              message_type: "error",
            });
          }
          setTimeout(() => {
            this.setState(() => ({ message: "" }));
          }, 5000);
        } else {
          const response = await IpRedirectionService.createIpRedirection(category_data);
          if (response) {
            this.setState({ submit_status: false });
            if (response.data.status === 1) {
              this.setState({
                message: response.data.message,
                message_type: "success",
                fromurl: "",
                tourl : "",
                IpRedirectioncode: "",
                except : false,
                ip : ''
              });
              this.getAllIpRedirections();
            } else {
              this.setState({
                message: response.data.message,
                message_type: "error",
              });
            }
          } else {
            this.setState({ submit_status: false });
            this.setState({
              message: "Something went wrong! Please try after some time",
              message_type: "error",
            });
          }
          setTimeout(() => {
            this.setState(() => ({ message: "" }));
          }, 5000);
        }
      } catch (err) {}
    }
  };

  handleRemoveContact = async (id) => {
    this.setState({
      delete_status: true,
    });
    const response = await IpRedirectionService.removeIpRedirection({ _id: id });
    if (response.data.status === 1) {
      let newContacts = [...this.state.data_table];
      newContacts = newContacts.filter(function (obj) {
        return obj._id !== id;
      });
      this.setState({
        table_message: response.data.message,
        table_message_type: "sucess",
      });

      this.setState({
        data_table: newContacts,
        delete_status: false,
        edit_status: false,
      });
      setTimeout(() => {
        this.setState(() => ({ table_message: "" }));
      }, 5000);
    } else {
      this.setState({
        table_message: response.data.message,
        table_message_type: "sucess",
      });
      setTimeout(() => {
        this.setState(() => ({ table_message: "" }));
      }, 5000);
    }
  };

 


  handleIpRedirectionChange = async (e) => {
    this.setState({ IpRedirectionstatus: e.target.value });
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty('IpRedirectionstatus', e.target.value);
    if (errorMessage) errors['IpRedirectionstatus'] = errorMessage;
    else delete errors['IpRedirectionstatus'];
    this.setState({ 'errors': errors });
  };

 
  userSelected = (event) => {
    const data_selected_row = [];
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value;
    });   
    this.setState({ users: data_selected_row });
    this.setState({ users_selected: event });
  };
  

    /* set selected Country code */

    setIpSelected = (event) => {
      console.log('setIpSelected', event , this.state.AllIps , this.state.Ips_selected);
      const data_selected_row = [];
      event.map((selectValues, index) => {
        data_selected_row[index] = selectValues.value;
      });
  
      this.setState({ AllIps: data_selected_row });
      
      this.setState({ Ips_selected: event });
    };

    /* select region code */

    setSateIpSelected = (event) => {
      console.log('setIpSelected', event , this.state.AllStateIps , this.state.StateIpselected);
      const data_selected_row = [];
      event.map((selectValues, index) => {
        data_selected_row[index] = selectValues.value;
      });
  
      this.setState({ AllStateIps: data_selected_row });
      
      this.setState({ StateIpselected: event });
    };


    setSateIpSelected

    handleCheckbox = (e) => {
      this.setState({ except: e.target.checked });
    };

  render() {
    let datarow = this.state.data_table;
    // const userPlaceholder = {
    //   selectSomeItems: "Users",
    //   allItemsAreSelected: "All items are selected.",
    //   selectAll: "Select All",
    // };
    // const productPlaceholder = {
    //   selectSomeItems: "Products",
    //   allItemsAreSelected: "All items are selected.",
    //   selectAll: "Select All",
    //   search: "Search",
    // };
  //  console.log('State--->', States);
    // console.log('Countries',this.state.AllIps , this.state.Ips_selected );
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4">
                  <React.Fragment>
                    <div className="categories-wrap p-4">
                      <div className="row">
                        <div className="col-md-4">
                          <div className="card">
                            <div className="card-header">
                              <strong>
                                {this.state.edit_status === true
                                  ? "Edit"
                                  : "Add"}{" "}
                                Ip Redirection
                              </strong>
                              {this.state.edit_status === true ? (
                                <button
                                  onClick={this.addIpRedirection}
                                  className="ml-4 add-cat-btn btn btn-success"
                                >
                                  + Add New
                                </button>
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="card-body coupon-add categories-add">
                        

                            <div className="form-group">
                                
                                <MultiSelect
                                    options={
                                      Countries
                                    }
                                    value={this.state.Ips_selected}
                                    onChange={this.setIpSelected} 
                                  /> 
                                {(this.state.errors.country || this.state.AllIps.length <= 0 )  ? (
                                  this.state.AllIps.length <= 0 ? (  <div className="danger">
                                  {this.state.errors.country}
                                </div> ) : ''
                                ) : (
                                  ""
                                )}
                              </div>


                            {
                              this.state.AllIps.indexOf('US') > -1  ?  
                              (<div className="form-group">
                                                            
                              <MultiSelect
                                    options={
                                      States
                                    }
                                    value={this.state.StateIpselected}
                                    onChange={this.setSateIpSelected} 
                                  />
                                {this.state.errors.states ? (
                                  <div className="danger">
                                    {this.state.errors.states}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                            ) : ''
                            }

                          

                     
                              <div className="form-group">
                              <select
                                value={this.state.fromurl}
                                className='form-group-relase'
                                onChange={this.handleChange("fromurl")}
                              >
                                <option value=''>Redirection From</option>
                                {this.state.Allpages.map(
                                    (singlepages, idx) => (
                                      <option
                                        key={singlepages._id}
                                        value={singlepages._id}
                                      >
                                        {singlepages.title}
                                      </option>
                                    )
                                  )}
                              </select>
                                {/* <input
                                  className="form-control"
                                  placeholder="Redirection From"
                                  type="text"
                                  value={this.state.fromurl}
                                  onChange={this.handleChange("fromurl")}
                                /> */}
                                {this.state.errors.fromurl ? (
                                  <div className="danger">
                                    {this.state.errors.fromurl}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>


                              <div className="form-group">
                                {/* <input
                                  className="form-control"
                                  placeholder="Redirection To"
                                  type="text"
                                  value={this.state.tourl}
                                  onChange={this.handleChange("tourl")}
                                /> */}

<select
                                value={this.state.tourl}
                                className='form-group-relase'
                                onChange={this.handleChange("tourl")}
                              >
                                <option value=''>Redirection To</option>
                                {this.state.Allpages.map(
                                    (singlepages, idx) => (
                                      <option
                                        key={singlepages._id}
                                        value={singlepages._id}
                                      >
                                        {singlepages.title}
                                      </option>
                                    )
                                  )}
                              </select>
                            
                                {this.state.errors.tourl ? (
                                  <div className="danger">
                                    {this.state.errors.tourl}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>



                              <div className="form-group">
                                  <input
                                    className="form-control"
                                    placeholder="[ Enter Ip ]"
                                    type="text"
                                    value={this.state.ip}
                                    onChange={this.handleChange("ip")}
                                  />  
                              </div>

                              <div className="form-group redirect_all_ips">
                              <input style={{'margin-right' : '5px'}}
                              type="checkbox"
                           //   onChange={this.handleChange("except")}
                              onChange={this.handleCheckbox}
                              checked={this.state.except}
                              />
                              <span>Redirect all except countries & regions listed above.</span>
                              </div>
 
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.saveIpRedirection}
                                className="save-btn btn btn-dark"
                              >
                                {this.state.submit_status === true ? (
                                  <React.Fragment>
                                    <ReactSpinner
                                      type="border"
                                      color="primary"
                                      size="1"
                                    />
                                    <span className="submitting">
                                      {this.state.edit_status === true
                                        ? "UPDATING Redirection..."
                                        : "CREATING Redirection..."}
                                    </span>
                                  </React.Fragment>
                                ) : this.state.edit_status === true ? (
                                  "UPDATE Redirection"
                                ) : (
                                  "CREATE Redirection"
                                )}
                              </button>
                              {this.state.message !== "" ? (
                                <div className={this.state.message_type}>
                                  <p>{this.state.message}</p>
                                </div>
                              ) : null}
                            </div>
                          </div>
                        </div>
                        <div className="col-md-8">
                          <div className="card">
                            <div className="card-header">
                              <strong>Ip Redirection</strong>
                            </div>
                            <div className="card-body categories-list">
                              <React.Fragment>
                                <div
                                  style={{
                                    display:
                                      this.state.spinner === true
                                        ? "flex"
                                        : "none",
                                  }}
                                  className="overlay"
                                >
                                  <ReactSpinner
                                    type="border"
                                    color="primary"
                                    size="10"
                                  />
                                </div>
                              </React.Fragment>
                              <p className="tableinformation">
                                {this.state.table_message !== "" ? (
                                  <div
                                    className={this.state.table_message_type}
                                  >
                                    <p>{this.state.table_message}</p>
                                  </div>
                                ) : null}
                              </p>
                              <DataTable
                                columns={this.state.columns}
                                data={datarow}
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                noDataComponent = {<p>There are currently no records.</p>}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default IpRedirection;
