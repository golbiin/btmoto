import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import { Link } from "react-router-dom";
import validate from "react-joi-validation";
import Joi, { join } from "joi-browser";
import * as manageUser from "../../ApiServices/admin/manageUser";

class EditProfile extends Component {
    constructor(props) {
        super(props);
    }
    state = { 
        errors: {},
        submit_status: false,
        cimonAdmin: [],
      };
    
      /* Joi validation schema */
      schema = {
        first_name: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        last_name: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        email: Joi.string()
        .required().email()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        username: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        _id: Joi.optional().label("id"),
        old_password:Joi.string().label("Old Password").default(null),
        new_password: Joi.string().label("New Password").default(null).when('old_password', {
            is: null,
            then: Joi.optional(),
            otherwise: Joi.required()
        }),
        confirm_password:  Joi.string().default(null).label('Confirm Password').valid(Joi.ref('new_password')).options({
            language: {
              any: {
                allowOnly: '!!Passwords do not match',
              }
            } 
          }).when('new_password', {
            is: null,
            then: Joi.optional(),
            otherwise: Joi.required()
        })
    
    };

  
    /*Input Handle Change */
    handleChange = (event, type = null) => {
        let cimonAdmin = { ...this.state.cimonAdmin };
        const errors = { ...this.state.errors };
        this.setState({ message: "" });
        delete errors.validate;
        let name = event.target.name; //input field  name
        let value = event.target.value; //input field value
        const errorMessage = this.validateUserdata(name, value);
        if (errorMessage) errors[name] = errorMessage;
        else delete errors[name];
        cimonAdmin[name] = value;
        this.setState({ cimonAdmin, errors });
    };
    validateUserdata = (name, value) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };

    /* Form Submit */
     handleSubmit = async () => {
        const cimonAdmin = { ...this.state.cimonAdmin };
        const errors = { ...this.state.errors };
        let result = Joi.validate(cimonAdmin, this.schema);
        if (result.error) {
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
                this.setState({
                    errors: errors  
                  })
            } else {
            if (errors.length > 0) {
                this.setState({
                    errors: errors  
                  })
            }else{
                this.setState({ submit_status: true });
                const response = await manageUser.updateProfiledata(cimonAdmin);
                if (response.data.status == 1) {
                    this.setState({
                        submitStatus: false,
                        message: response.data.message,
                        responsetype: "success",
                    });
                    setTimeout(
                        () =>
                        this.props.history.push({
                            pathname: '/admin/home',
                        }),
                        2000
                    );
                } else {
                    this.setState({
                        submitStatus: false,
                        message: response.data.message,
                        responsetype: "error",
                    });
                }
            }
        }
  };

    componentDidMount = async () => {
        const id = this.props.match.params.id;
        
        const response = await manageUser.getSingleProfile(id);
        if(response.data.status == 1){
          let newUserarray = {
            _id: response.data.data.user_data._id,
            first_name: response.data.data.user_data.first_name,
            last_name: response.data.data.user_data.last_name,
            email: response.data.data.user_data.email,
            username: response.data.data.user_data.username,
          };
          this.setState({ cimonAdmin : newUserarray });
        }else{
          this.setState({
            message: response.data.message,
            responsetype: "error",
          });
        }
    };
    render() { 
     
        return (
           
            <React.Fragment>
            <div className="container-fluid admin-body ">
                <div className="admin-row">
                    <div className="col-md-2 col-sm-12 sidebar">
                        <Adminsidebar props={this.props} />
                    </div>                    
                    <div className="col-md-10 col-sm-12  content">
                        <div className="row content-row">
                            <div className="col-md-12  header">
                                <Adminheader  props={this.props} />
                            </div>
                            <div className="col-md-12  contents  useradd-new pt-4 pb-4 pr-4" >   
                            <React.Fragment>
                            <div className="card">
                                <div className="card-header">User Details</div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label>First Name</label>
                                        <input name="first_name" 
                                        value={this.state.cimonAdmin.first_name} 
                                        onChange={(e) => this.handleChange(e)}
                                        type="text" 
                                        placeholder="First Name" 
                                        className="form-control"/>
                                        {this.state.errors.first_name ? (<div className="error text-danger">
                                        {this.state.errors.first_name}
                                        </div>
                                        ) : (
                                        ""
                                        )}
                                    </div>
                                    <div className="form-group">
                                        <label>Last Name</label>
                                        <input name="last_name" 
                                        value={this.state.cimonAdmin.last_name} 
                                        onChange={(e) => this.handleChange(e)}
                                        type="text" 
                                        placeholder="Last Name" 
                                        className="form-control"/>
                                        {this.state.errors.last_name ? (<div className="error text-danger">
                                        {this.state.errors.last_name}
                                        </div>
                                        ) : (
                                        ""
                                        )}
                                    </div>
                                    <div className="form-group">
                                        <label>User Name</label>
                                        <input name="username" 
                                        value={this.state.cimonAdmin.username} 
                                        onChange={(e) => this.handleChange(e)}
                                        type="text" 
                                        placeholder="User Name" 
                                        disabled = "disabled"
                                        className="form-control"/>
                                        {this.state.errors.username ? (<div className="error text-danger">
                                        {this.state.errors.username}
                                        </div>
                                        ) : (
                                        ""
                                        )}
                                    </div>
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input name="email" 
                                        value={this.state.cimonAdmin.email} 
                                        onChange={(e) => this.handleChange(e)}
                                        type="text" 
                                        placeholder="Email" 
                                        className="form-control"/>
                                        {this.state.errors.email ? (<div className="error text-danger">
                                        {this.state.errors.email}
                                        </div>
                                        ) : (
                                        ""
                                        )}
                                    </div>
                                    <div className="form-group">
                                        <label>Old Password</label>
                                        <input name="old_password" 
                                        onChange={(e) => this.handleChange(e)}
                                        type="password" 
                                        placeholder="Old Password" 
                                        autoComplete="new-password"
                                        className="form-control"/>
                                        {this.state.errors.old_password ? (<div className="error text-danger">
                                        {this.state.errors.old_password}
                                        </div>
                                        ) : (
                                        ""
                                        )}
                                    </div>
                                    <div className="form-group">
                                        <label>New Password</label>
                                        <input name="new_password" 
                                        onChange={(e) => this.handleChange(e)}
                                        type="password" 
                                        placeholder="New Password" 
                                        autoComplete="off"
                                        className="form-control"/>
                                        {this.state.errors.new_password ? (<div className="error text-danger">
                                        {this.state.errors.new_password}
                                        </div>
                                        ) : (
                                        ""
                                        )}
                                    </div>
                                    <div className="form-group">
                                        <label>Confirm Password</label>
                                        <input name="confirm_password" 
                                        onChange={(e) => this.handleChange(e)}
                                        type="password" 
                                        placeholder="Confirm Password" 
                                        autoComplete="off"
                                        className="form-control"/>
                                        {this.state.errors.confirm_password ? (<div className="error text-danger">
                                        {this.state.errors.confirm_password}
                                        </div>
                                        ) : (
                                        ""
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="submit">
                                <button className="submit-btn" onClick={this.handleSubmit} >Save</button>
                                <Link to="/admin/userslist" className="cancel-btn">Cancel</Link>
                            </div>
                            {this.state.message !== "" ? (
                                  <div className="form-group tableinformation"><div className={this.state.responsetype} >
                                    <p>{this.state.message}</p>
                                  </div></div>
                                ) : null}
                            </React.Fragment>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </React.Fragment> );
    }
}
 
export default EditProfile;