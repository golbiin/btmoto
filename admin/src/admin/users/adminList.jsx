import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import DeleteConfirm from "../common/deleteConfirm";
import DataTable from 'react-data-table-component';
import { Link } from "react-router-dom";
import * as manageUserService from "../../ApiServices/admin/manageUser";
import memoize from 'memoize-one';
class AdminUsersList extends Component {
    state = { 
        search:'',
        data_table : [],
        modal: false,
        spinner : true,
        selectedRows: '',
        columns : [
          {
            name: 'First Name',
          
            selector: 'first_name',
            sortable: true,
          },
          {
            name: 'Last Name',
            selector: 'last_name',
            sortable: true,
            left: true,
            hide: 'sm'
          },
          {
            name: 'Email',
            selector: 'email',
            sortable: true,
            center: true,
          },
        {
          name: '',
          cell:  row =><div className="action_dropdown" data-id={row.id}><i data-id={row.id} className="dropdown_dots"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
          <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
           
            <Link
              class="dropdown-item"
              to={"/admin/adminuser/edit/" + row.id}
              >
              Edit
            </Link>
           
            <Link
              onClick={() =>
                this.saveAndtoogle(row.id)
              } className="dropdown-item"
            >
              Delete
            </Link>
          </div>
          </div>,
          allowOverflow: false,
          button: true,
          width: '56px', // custom width for icon button
        },
        ]
    };
    componentDidMount = () => {
        this.getAllusers();
    };
    getAllusers = async () => {
      this.setState({ spinner: true});
      const response =  await manageUserService.getAlladmins();
      
      if (response.data.status == 1) {
        this.setState({ 
          users: response.data.data
        });
        const Setdata = { ...this.state.data_table };
        const data_table_row = [];
        this.setState({ spinner: false});
        response.data.data.map((item,index) => {
          const Setdata = {};
          Setdata.first_name =  item.first_name;
          Setdata.last_name =  item.last_name;
          Setdata.email =  item.email;
          Setdata.status =  item.status;
          Setdata.id =  item._id;
          data_table_row.push(Setdata);
        });
        this.setState({ 
          data_table: data_table_row
        });
      } 
    }
    searchSpace=(event)=>{
      let keyword = event.target.value;
      
      this.setState({search:keyword})
    }

    convertArrayOfObjectsToCSV  = array => {
      let result;
      const columnDelimiter = ',';
      const lineDelimiter = '\n';
      const keys = Object.keys(array[0]);
    
      result = '';
      result += keys.join(columnDelimiter);
      result += lineDelimiter;
      array.forEach(item => {
        let ctr = 0;
        keys.forEach(key => {
          if (ctr > 0) result += columnDelimiter;
          result += item[key];
          ctr++;
        });
        result += lineDelimiter;
      });
      return result;
    }

    downloadCSV = array => {
      const link = document.createElement('a');
      let csv = this.convertArrayOfObjectsToCSV(array);
      if (csv == null) return;

      const filename = 'Admin.csv';

      if (!csv.match(/^data:text\/csv/i)) {
        csv = `data:text/csv;charset=utf-8,${csv}`;
      }
      link.setAttribute('href', encodeURI(csv));
      link.setAttribute('download', filename);
      link.click();
    }

    DeletePageId = id => {
        
        const data_table = this.state.data_table.filter(i => i.id !== id);
        this.setState({data_table})
    };


    deleteUser = async () => {
      
      let id = this.state.index;
      const response = await manageUserService.deleteAdmin(id);
      if (response) {
        if (response.data.status == 1) {
          const data_table = this.state.data_table.filter(i => i.id !== id);
          this.setState({data_table})
          this.toogle();
        }
      }
    };

    saveAndtoogle = (id) => {
      this.setState({ index: id });
      this.toogle();
    };
    toogle = () => {
      let status = !this.state.modal;
      this.setState({ modal: status });
    };


    deleteAll = () => {
      const { selectedRows } = this.state;
      const rows = selectedRows.map(r => r.name);
      
      if (window.confirm(`Are you sure you want to delete:\r ${rows}?`)) {
      }
    }

    handlerowChange = state => {
      
      this.setState({ selectedRows: state.selectedRows});
      
    };

    deleteHandler = (e) => {
      const { selectedRows } = this.state;
      const rows = selectedRows.map(r => r.email);
      
      if (window.confirm(`Are you sure you want to delete:\r ${rows}?`)) {

      }
    }


    render() {
      const contextActions = memoize(deleteHandler => ( 
      <button className="danger" onClick={this.deleteHandler}>Delete</button>
      ));
        
    let rowData =  this.state.data_table,
    search = this.state.search;
    
      if(search.length > 0){
          search = search.trim().toLowerCase();
          rowData = rowData.filter(l => {
              return (l.first_name.toLowerCase().match( search ) || l.last_name.toLowerCase().match( search ) ||
              l.email.toLowerCase().match( search ));
          });
      }

    return ( <React.Fragment>
        <div className="container-fluid admin-body ">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
              <div className="col-md-12  header">
                <Adminheader  props={this.props} />
              </div>
              <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4" >   
                <div className="main_admin_page_common">
                <div className="">
                  <div className="admin_breadcum">
                    <div className="row">
                      <div className="col-md-1"><p className="page-title">Admins</p></div>
                        <div className="col-md-6">
                          <Link className="add_new_btn" to="/admin/adminadd">Add New</Link>
                          <Link className="Export_data_btn"
                          onClick={() => this.downloadCSV(rowData)} >Export Data</Link>
                        </div>
                        <div className="col-md-5">
                          <div className="searchbox">
                            <div className="commonserachform">
                              <span></span>
                              <input
                              type="text"
                              placeholder="Search"
                              onChange={(e)=>this.searchSpace(e)} 
                              name="search"
                              className="search form-control"
                              />
                              <input type="submit" className="submit_form"/>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <React.Fragment>
                      <div style={{ display: this.state.spinner === true ? 'flex justify-content-center ' : 'none' }} className="overlay text-center">
                            <ReactSpinner type="border" color="primary" size="10" />
                      </div>
                    </React.Fragment>
                  
                  <DataTable
                  columns={this.state.columns}
                  data={rowData}
                  highlightOnHover  
                  pagination
                  onSelectedRowsChange={this.handlerowChange}
                  selectableRowsVisibleOnly
                  contextActions={contextActions(this.deleteAll)}
                  noDataComponent = {<p>There are currently no records.</p>}
                  />
                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.deleteUser}
        />
      </React.Fragment> );
    }
}
 
export default AdminUsersList;