import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
//import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import { Link } from "react-router-dom";
import Uploadprofile from "../common/uploadProfileimage";
import * as manageUser from "../../ApiServices/admin/manageUser";
import { CountryDropdown } from 'react-country-region-selector';
import { Countries } from './../../countries';
import { bussinessType, industry, categories } from "../../config.json";
import BlockUi from 'react-block-ui';
class EditUser extends Component {
  constructor(props) {
    super(props);
    this.handleCheck = this.handleCheck.bind(this);
  }
  state = {
    spinner: false,
    UserTypes: [
      // { name: 'Admin', id: 'admin' },
      { name: 'Distributor', id: 'distributor' },
      { name: 'Integrator', id: 'integrator' },
      { name: 'Subscriber', id: 'subscriber' },
    ],
    cimonUser: [],
    validated: false,
    userStatus: true,
    submitStatus: false,
    errors: {},
    upload_data: "",
    show_pg: false,
    newPassword: "",
    bussiness_type: '',
    agree_newsletter: false,
    interested_products: ''
  };

  UserSchema = {
    user_name: Joi.string().allow(null).allow(''),
    email: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Email. It is not allowed to be empty.",
        };
      }),
    first_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the First name. It is not allowed to be empty.",
        };
      }),
    last_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Last name. It is not allowed to be empty.",
        };
      }),
    usertype: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the user type. It is not allowed to be empty.",
        };
      }),
    _id: Joi.optional().label("id"),
    profile_image: Joi.string().allow(null).allow(''),
    bussiness_type: Joi.string().allow(null).allow(''),
    agree_newsletter: Joi.boolean(),
    uniq_id: Joi.string().allow(null)
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the uniq id. It is not allowed to be empty.",
        };
      }),
    drivelink: Joi.optional().label("drivelink"),

    // drivelink: Joi.string().allow(null)
    //   .error(() => {
    //     return {
    //       message:
    //         "Please be sure you’ve filled in the url. It is not allowed to be empty.",
    //     };
    //   }),
    company_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Company name. It is not allowed to be empty.",
        };
      }),
    city: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the City. It is not allowed to be empty.",
        };
      }),

    zipcode: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Zipcode. It is not allowed to be empty.",
        }
      }),
    phone_number: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Phone number. It is not allowed to be empty.",
        };
      }),
    recovery_email: Joi.string().allow(null).allow(''),
    country: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the  country. It is not allowed to be empty.",
        };
      }),
    newPassword: Joi.string().allow(null).allow('')
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the NewPassword. It is not allowed to be empty.",
        };
      }),
    //newPassword: Joi.optional().label("newPassword"), 
  };

  /* handle change */
  handleChange = (event, type = null) => {
    console.log(111, event, 65666);
    let cimonUser = { ...this.state.cimonUser };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    console.log(77777, name, 5555, value);
    const errorMessage = this.validateUserdata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    cimonUser[name] = value;
    this.setState({ cimonUser, errors });
  };
  validateUserdata = (name, value) => {
    const obj = { [name]: value };
    const UserSchema = { [name]: this.UserSchema[name] };
    const { error } = Joi.validate(obj, UserSchema);
    return error ? error.details[0].message : null;
  };
  isEmpty = (obj) => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };

  bussinessType = async (e) => {
    console.log('bussiness_type', e.target.value);
    this.setState({ bussiness_type: e.target.value });
  }
  industryType = async (e) => {
    console.log('industry', e.target.value);
    this.setState({ industry: e.target.value });
  }
  /* Checkbox on Change */
  handleCheck(e) {
    this.setState({ agree_newsletter: e.target.checked });
  }
  /* Profile Upload */
  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors };
    let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      let file = value.target.files[0];
      upload_data = file;
      this.setState({ upload_data });
    }

  };


  selectCountry(val) {
    console.log('selectCountry',val.target.value);
    let cimonUser = { ...this.state.cimonUser };
    let errors = { ...this.state.errors };
    cimonUser['country'] = val.target.value ? val.target.value : "";
    if (cimonUser.country === "") {
      errors.country = " Country is required";
    } else {
      delete errors.country;
    }
    this.setState({ cimonUser, errors });
  }



  getFilepermissionById = async () => {
    // this.setState({ spinner: true });
    const response = await manageUser.getFilepermissionById();

    if (response.data.status == 1) {
      this.setState({
        userfilepermission: response.data.data[0].items,
      });

    }
    else {
      this.setState({
        userfilepermission: [],
      });
    }
    
  };

  
  componentDidMount = async () => {
    this.setState({ spinner: true });
    const id = this.props.match.params.id;
    const response = await manageUser.getSingleUser(id);
    if (response.data.status == 1) {

      console.log(353, response.data.data.user_data);
      let newUserarray = {
        _id: response.data.data.user_data._id,
        user_name: response.data.data.user_data.user_name,
        email: response.data.data.user_data.email,
        first_name: response.data.data.user_data.first_name,
        last_name: response.data.data.user_data.last_name,
        profile_image: response.data.data.user_data.profile_image,
        usertype: response.data.data.user_data.usertype,
        uniq_id: response.data.data.user_data.uniq_id,
        company_name: response.data.data.user_data.company_name,
        city: response.data.data.user_data.city,
        zipcode: response.data.data.user_data.zipcode,
        phone_number: response.data.data.user_data.phone_number,
        recovery_email: response.data.data.user_data.recovery_email,
        country: response.data.data.user_data.country,
        drivelink: response.data.data.user_data.drivelink ? response.data.data.user_data.drivelink : '',
        newPassword: this.state.newPassword,
      };

      this.setState({
        cimonUser: newUserarray,


        interested_products: response.data.data.user_data.interested_products ? response.data.data.user_data.interested_products : '',
        industry: response.data.data.user_data.industry ? response.data.data.user_data.industry : '',
        bussiness_type: response.data.data.user_data.bussiness_type ? response.data.data.user_data.bussiness_type : '',
        agree_newsletter: response.data.data.user_data.agree_newsletter ? response.data.data.user_data.agree_newsletter : 'false',
      });
    } else {
      this.setState({
        userStatus: false,
        message: response.data.message,
        responsetype: "error",
      });
    }
    this.getFilepermissionById();
    this.setState({ spinner: false });

  };



  updateUser = async () => {
    console.log(54545);
    this.setState({ submitStatus: true });
    let cimonUser = { ...this.state.cimonUser };
    const errors = { ...this.state.errors };
    if (cimonUser.country === "") {
      errors.country = " Please select your Country.";
      this.setState({ errors });
    } else {
      delete errors.country;
      this.setState({ errors });
    }

    let user_validate = Joi.validate(cimonUser, this.UserSchema);
    if (user_validate.error) {
      console.log(9999);
      if (user_validate.error) {
        let path = user_validate.error.details[0].path[0];
        let errormessage = user_validate.error.details[0].message;
        errors[path] = errormessage;
        console.log(111, errors);
      }
      errors["validate"] = "Please be sure you’ve filled all fields.";
      this.setState({ errors });
      this.setState({
        submitStatus: false,
        message: "",
        responsetype: "",
      });
      this.setState({ errors });

    } else {
      console.log(666);
      if (this.isEmpty(errors)) {
        this.setState({ spinner: true });
        if (this.state.upload_data) {
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          );

          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            let cimonUser = { ...this.state.cimonUser };
            cimonUser["profile_image"] = filepath;

            this.setState({ cimonUser });
            this.updateUserdata();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error",
            });
            this.setState({ spinner: false });
          }
        } else {
          this.updateUserdata();
        }
      }
    }
  };

  updateUserdata = async () => {
    console.log(77777);
    const cimonUser = { ...this.state.cimonUser };


    cimonUser["bussiness_type"] = this.state.bussiness_type;
    cimonUser["industry"] = this.state.industry;
    cimonUser["agree_newsletter"] = this.state.agree_newsletter;
    cimonUser["interested_products"] = this.state.interested_products;
    cimonUser["drivelink"] = this.state.userfilepermission.includes(this.state.cimonUser.usertype) ? this.state.cimonUser.drivelink : '';
    
    // cimonUser["newPassword"] = this.state.newPassword;
    this.setState({ cimonUser });
    const response = await manageUser.updateUserdata(cimonUser);
    if (response.data.status == 1) {
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "success",
      });
      this.setState({ spinner: false });
      setTimeout(() => { 
        window.location.reload();
      }, 2000);
    } else {
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "error",
      });
      this.setState({ spinner: false });
    }
  };

  generatePassword = async () => {

    let cimonUser = { ...this.state.cimonUser };

    this.setState({ show_pg: true });
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < 25; x++) {
      var i = Math.floor(Math.random() * chars.length);
      pass += chars.charAt(i);
    }

    this.setState({ newPassword: pass });
    cimonUser['newPassword'] = pass;
    this.setState({ cimonUser });
    //console.log(4444,pass);

  };
  cancelPasswordSection = async () => {
    this.setState({ show_pg: false });
  }


  render() {
    let optionUserType = this.state.UserTypes.map(v => (
      <option key={v.id} value={v.id} selected={this.state.cimonUser.usertype == v.id ? v.id : ""} >{v.name}</option>
    ));

    const { country } = this.state.cimonUser;
    const selectedProducts = this.state.interested_products.split(',');
    console.log('selectedProducts', selectedProducts);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body ">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  useradd-new pt-4 pb-4 pr-4" >
                  <React.Fragment>
                  <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className="card">
                      <div className="card-header">User Details</div>
                      <div className="card-body">
                        <div className="form-group">
                          <Uploadprofile
                            onuplaodProfile={this.onuplaodProfile}
                            value={
                              this.state.cimonUser.profile_image
                            }
                            errors={this.state.errors}
                          />
                        </div>
                        <div className="form-group">
                          <label>Email</label>
                          <input
                            type="text"
                            name="email"
                            className="form-control "
                            disabled="disabled"
                            value={this.state.cimonUser.email}
                          />
                        </div>
                        <div className="form-group">
                          <label>First Name</label>
                          <input
                            type="text"
                            name="first_name"
                            className="form-control "
                            value={this.state.cimonUser.first_name}
                            onChange={(e) => this.handleChange(e)}
                          />
                          {this.state.errors.first_name !== "" ? (
                            <div className="validate_error">
                              <p>{this.state.errors.first_name}</p>
                            </div>
                          ) : null}
                        </div>
                        <div className="form-group">
                          <label>Last Name</label>
                          <input
                            type="text"
                            name="last_name"
                            className="form-control "
                            value={this.state.cimonUser.last_name}
                            onChange={(e) => this.handleChange(e)}
                          />
                          {this.state.errors.last_name !== "" ? (
                            <div className="validate_error">
                              <p>{this.state.errors.last_name}</p>
                            </div>
                          ) : null}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">User Type</label>
                          <select name="usertype" className="form-control" onChange={(e) => this.handleChange(e)}>
                            <option value="">Select UserName Type</option>
                            {optionUserType}
                          </select>
                          {this.state.errors.usertype ? (<div className="error text-danger">
                            {this.state.errors.usertype}
                          </div>
                          ) : (
                              ""
                            )}
                        </div>
                        {(() => {
                          if ((this.state.cimonUser.usertype == 'integrator') ||
                            (this.state.cimonUser.usertype == 'distributor')) {
                            return (
                              <div className="form-group">
                                <label>Id</label>
                                <input
                                  type="text"
                                  name="uniq_id"
                                  className="form-control "
                                  disabled="disabled"
                                  value={this.state.cimonUser.uniq_id}
                                />
                              </div>
                            )
                          }
                        })()}

                        <div className="form-group">
                          <label>Company Name</label>
                          <input
                            type="text"
                            name="company_name"
                            className="form-control "
                            value={this.state.cimonUser.company_name}
                            onChange={(e) => this.handleChange(e)}
                          />
                          {this.state.errors.company_name !== "" ? (
                            <div className="validate_error">
                              <p>{this.state.errors.company_name}</p>
                            </div>
                          ) : null}
                        </div>

                        <div className="form-group">
                          <label>Country </label>


                         {/* <p>Countries</p>  */}

                          <select
                            id="country"                            
                            name="country"
                            className="profile-input"
                            // ref={register}
                             value = { country }
                           //  onChange={this.selectCountry} 

                             onChange={(e) => this.selectCountry(e)}
                            // onChange={handleProfileChange} 
                        >
                            <option key={""} value={""}>
                                Select Country
                            </option>
                            {(Countries || []).map((item) => (
                            <option key={item.value} value={item.value}>
                                {item.label || item.value}
                            </option>
                            ))}
                        </select>
                 

                        {this.state.errors.country !== "" ? (
                            <div className="validate_error">
                              <p>{this.state.errors.country}</p>
                            </div>
                          ) : null}
                          {/* <CountryDropdown className="form-control"
                            value={country}
                            onChange={(val) => this.selectCountry(val)} />

                          {this.state.errors.country !== "" ? (
                            <div className="validate_error">
                              <p>{this.state.errors.country}</p>
                            </div>
                          ) : null}
                          {/* {this.state.errors.country ? (
                                <div className="danger">
                                {this.state.errors.country}
                                </div>
                                ) : ( "" )} */}
                        </div> 

                        <div className="form-group">
                          <label>City</label>
                          <input
                            type="text"
                            name="city"
                            className="form-control "
                            value={this.state.cimonUser.city}
                            onChange={(e) => this.handleChange(e)}
                          />
                          {this.state.errors.city !== "" ? (
                            <div className="validate_error">
                              <p>{this.state.errors.city}</p>
                            </div>
                          ) : null}
                        </div>

                        <div className="form-group">
                          <label>Zipcode</label>
                          <input
                            type="text"
                            name="zipcode"
                            className="form-control "
                            value={this.state.cimonUser.zipcode}
                            onChange={(e) => this.handleChange(e)}
                          />
                          {this.state.errors.zipcode !== "" ? (
                            <div className="validate_error">
                              <p>{this.state.errors.zipcode}</p>
                            </div>
                          ) : null}
                        </div>

                        <div className="form-group">
                          <label>Phone Number</label>
                          <input
                            type="text"
                            name="phone_number"
                            className="form-control "
                            value={this.state.cimonUser.phone_number}
                            onChange={(e) => this.handleChange(e)}
                          />
                          {this.state.errors.phone_number !== "" ? (
                            <div className="validate_error">
                              <p>{this.state.errors.phone_number}</p>
                            </div>
                          ) : null}
                        </div>

                        <div className="form-group">
                          <label>Recovery Email</label>
                          <input
                            type="text"
                            name="recovery_email"
                            className="form-control"
                            value={this.state.cimonUser.recovery_email}
                            onChange={(e) => this.handleChange(e)}
                          />

                        </div>



                        <div className="form-group productlisting">
                          <div className={`register-form-group`}>
                            <label >
                              {"Which CIMON products are you interested in?"}
                            </label>
                            {(categories || []).map((item) => (
                              <span>
                                < input
                                  name="selected_products"
                                  type="checkbox"
                                  value={item.value ?? ""}
                                  label={item.label}
                                  id={item.label}
                                  checked={this.state.interested_products.includes(item.value) ? true : false}
                                  className="register-checkbox"
                                  onChange={(e) => {

                                    categories.forEach((category) => {
                                      if (category.value === e.target.value) {
                                        category.checked = e.target.checked;
                                        console.log('eeeeee', category.checked, e.target.checked, e.target.value);
                                        if (category.checked == true) {
                                          selectedProducts.push(category.value);
                                        } else {
                                          selectedProducts.pop(category.value);
                                        }
                                      }

                                    });
                                    const p = selectedProducts.join(",");
                                    this.setState({ interested_products: p });
                                    console.log('pppp', p);
                                    //     setInterestedProducts(p);
                                  }}
                                />
                                <label for={item.label} id={item.label}>{item.label}</label></span>
                            ))}
                          </div>

                        </div>



                        <div className="form-group">
                          <label>Business Type</label>
                          <select
                            id="bussiness_type"

                            className="profile-input"

                            value={this.state.bussiness_type}
                            //   defaultValue = { props.profile.country}
                            onChange={this.bussinessType}

                          >
                            <option key={""} value={""}>
                              Select Business Type
                            </option>
                            {(bussinessType || []).map((item) => (
                              <option key={item.value} value={item.value}>
                                {item.label || item.value}
                              </option>
                            ))}
                          </select>


                        </div>





                        <div className="form-group">
                          <label>Industry</label>
                          <select
                            id="industry"

                            className="profile-input"

                            value={this.state.industry}
                            //   defaultValue = { props.profile.country}
                            onChange={this.industryType}

                          >
                            <option key={""} value={""}>
                              Select Industry
                            </option>
                            {(industry || []).map((item) => (
                              <option key={item.value} value={item.value}>
                                {item.label || item.value}
                              </option>
                            ))}
                          </select>


                        </div>




                        <div className="form-group">
                          <label>New Password</label>
                          <input type="button" className="buttonbottom" value="Set New Password" onClick={this.generatePassword} />
                          <div className="passwd-group" style={{ display: this.state.show_pg === true ? 'block ' : 'none' }}>
                            <input name="newPassword" value={this.state.cimonUser.newPassword} className="form-control" type="text" size="40"
                              onChange={(e) => this.handleChange(e)}

                            />
                            {this.state.errors.newPassword !== "" ? (
                              <div className="validate_error">
                                <p>{this.state.errors.newPassword}</p>
                              </div>
                            ) : null}




                          </div>

                          {(() => {

                            if (this.state.userfilepermission) {

                              if (this.state.userfilepermission.includes(this.state.cimonUser.usertype)) {
                                return (
                                  <div className="form-group">
                                    <label>Google Drive Link</label>
                                    <input
                                      type="text"
                                      name="drivelink"
                                      className="form-control "
                                      value={this.state.cimonUser.drivelink}

                                      onChange={(e) => this.handleChange(e)}
                                    />
                                  </div>

                                )

                              }
                            }
                          })()}





                          <div style={{ 'padding-left': '20px' }} className="form-group">
                            <input
                              onChange={this.handleCheck} checked={this.state.agree_newsletter}
                              type="checkbox" id="agree_newsletter" class="form-check-input" />
                            <label title="" for="agree_newsletter" class="form-check-label">Sign me up for the newsletter to get current news, events and product updates</label>
                          </div>


                        </div>


                      </div>
                    </div>

                    <div className="submit">
                      <button
                        className="submit-btn"
                        onClick={this.updateUser}
                      >
                        Update
                            </button>
                      <Link to="/admin/userslist" className="cancel-btn">Cancel</Link>
                    </div>

                    {this.state.message !== "" ? (
                      <div className="form-group tableinformation"><div className={this.state.responsetype} >
                        <p>{this.state.message}</p>
                      </div></div>
                    ) : null}
                  </BlockUi>
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>);

  }
}

export default EditUser;
