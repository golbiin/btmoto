import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import DeleteConfirm from "../common/deleteConfirm";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import * as manageUserService from "../../ApiServices/admin/manageUser";
import memoize from "memoize-one";
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import moment from 'moment'
import { CSVLink, CSVDownload } from "react-csv";
class UsersList extends Component {
  state = {
    search: "",
    UserTypes: [
      // { name: 'Admin', id: 'admin' },
      { name: 'Distributor', id: 'distributor' },
      { name: 'Integrator', id: 'integrator' },
      { name: 'Subscriber', id: 'subscriber' },
    ],
    userfilepermission: [],
    data_table: [],
    users: [],
    modal: false,
    spinner: true,
    selectedRows: "",
    selecteduser : '',
    endDate : moment().toDate(),
    startDate :  moment().subtract(1, 'months').toDate(),
    columns: [
      {
        name: "First Name",
        selector: "first_name",
        sortable: true,
        cell: (row) => (
              <Link class="dropdown-item" to={"/admin/users/edit/" + row.id}>
                {row.first_name}
              </Link>
        ),
      },
 
      {
        name: "Last Name",
        selector: "last_name",
        sortable: true,
        left: true,
        hide: "sm",
      },
      {
        name: "Email",
        selector: "email",
        sortable: true,
        center: true,
      },
      {
        name: "Role",
        selector: "usertype",
        sortable: true,
        center: true,
      },
      {
        name: "VERIFY",
        selector: "verify",

        cell: (row) => (
          <div className={row.verify == true ? "green" : "red"}>
            {row.verify == true ? (
              "Verified"
            ) : (
                <div className="custom-control custom-switch">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id={row.id}
                    defaultChecked={row.verify}
                    onChange={() => this.changeVerifyStatus(row.id, !row.verify, row.index)}
                  />
                  <label
                    className="custom-control-label"
                    htmlFor={row.id}
                  ></label>
                </div>
              )}
          </div>
        ),
        center: true,
        hide: "sm",
      },

      {
        name: "",
        cell: (row) => (
          <div className="action_dropdown" data-id={row.id}>
            <i
              data-id={row.id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            ></i>
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >

              <Link class="dropdown-item" to={"/admin/users/edit/" + row.id}>
                Edit
              </Link>

              <Link
                onClick={() => this.saveAndtoogle(row.id)}
                className="dropdown-item"
              >
                Delete
              </Link>
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px", // custom width for icon button
      },
    ],
  };
  componentDidMount = () => {
    this.getAllusers();
    this.getFilepermissionById();
  };
  getAllusers = async () => {
    this.setState({ spinner: true });
    const response = await manageUserService.getAllusers();

    if (response.data.status == 1) {
      this.setState({
        users: response.data.data,
      });
      const Setdata = { ...this.state.data_table };
      const data_table_row = [];
     
      if(this.state.selecteduser){

        this.state.users.map((item, index) => {
          if(this.state.selecteduser == item.usertype){
            const Setdata = {};
            Setdata.index = index;
            Setdata.first_name = item.first_name;
            Setdata.last_name = item.last_name;
            Setdata.email = item.email;
            Setdata.status = item.status;
            Setdata.id = item._id;
            Setdata.admin_approve = item.admin_approve;
            Setdata.verify = item.verify;
            Setdata.usertype = item.usertype;
            data_table_row.push(Setdata);
          }
         
        });

      } else {
        this.state.users.map((item, index) => {
          console.log('manageUserService',item);
          const Setdata = {};
          Setdata.index = index;
          Setdata.first_name = item.first_name;
          Setdata.last_name = item.last_name;
          Setdata.email = item.email;
          Setdata.status = item.status;
          Setdata.id = item._id;
          Setdata.admin_approve = item.admin_approve;
          Setdata.verify = item.verify;
          Setdata.usertype = item.usertype;
          data_table_row.push(Setdata);
        });
      }

      this.setState({ spinner: false });
      this.setState({
        data_table: data_table_row,
      });
    } else {
      this.setState({ spinner: false });
    }
  };
  searchSpace = (event) => {
    let keyword = event.target.value;
    this.setState({ search: keyword });
  };

  convertArrayOfObjectsToCSV = (array) => {
    let result;
    const columnDelimiter = ",";
    const lineDelimiter = "\n";
    const keys = Object.keys(array[0]);

    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;
    array.forEach((item) => {
      let ctr = 0;
      keys.forEach((key) => {
        if (ctr > 0) result += columnDelimiter;
        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });
    return result;
  };

  downloadCSV = async (array) => {

    let  array2 = [];

    if( this.state.endDate > this.state.startDate ){

    const response = await manageUserService.exportcsv(this.state.startDate , this.state.endDate , this.state.selecteduser);

        if (response.data.status == 1) {
            if(response.data.data.length){
              array2 = response.data.data; 
              const link = document.createElement("a");
              let csv = this.convertArrayOfObjectsToCSV(array2);
              if (csv == null) return;
              const filename = "Users.csv";
              if (!csv.match(/^data:text\/csv/i)) {
              csv = `data:text/csv;charset=utf-8,${csv}`;
              }
              link.setAttribute("href", encodeURI(csv));
              link.setAttribute("download", filename);
              link.click();
            } else {
              this.setState({
                message: 'No Data Available',
                message_type: "error",
              });
            }
           
        }
  } else {
      this.setState({
        message: 'Start Date incorrect',
        message_type: "error",
      });
  }


  };

  DeletePageId = (id) => {
    const data_table = this.state.data_table.filter((i) => i.id !== id);
    this.setState({ data_table });
  };

  changeVerifyStatus = async (id, status, index) => {
    const response = await manageUserService.changeVerifyStatus(id, status);
    if (response.data.status == 1) {
      let users = [...this.state.users];
      users[index]["verify"] = status;
      this.setState(users);
      const Setdata = { ...this.state.data_table };
      const data_table_row = [];
      this.setState({ spinner: false });
      this.state.users.map((item, index) => {
        const Setdata = {};
        Setdata.index = index;
        Setdata.first_name = item.first_name;
        Setdata.last_name = item.last_name;
        Setdata.email = item.email;
        Setdata.status = item.status;
        Setdata.id = item._id;
        Setdata.admin_approve = item.admin_approve;
        Setdata.verify = item.verify;
        data_table_row.push(Setdata);
      });
      this.setState({
        data_table: data_table_row,
      });
    }
  };
  changeStatus = async (id, status) => {
    const response = await manageUserService.changeStatus(id, status);
    if (response.data.status == 1) {
      let data_table = [...this.state.data_table];
    }
  };

  deleteUser = async () => {
    let id = this.state.index;
    const response = await manageUserService.deleteUser(id);
    if (response) {
      if (response.data.status == 1) {
        const data_table = this.state.data_table.filter((i) => i.id !== id);
        this.setState({ data_table });
        this.toogle();
      }
    }
  };

  saveAndtoogle = (id) => {
    this.setState({ index: id });
    this.toogle();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };

  deleteAll = () => {
    const { selectedRows } = this.state;
    const rows = selectedRows.map((r) => r.name);
    if (window.confirm(`Are you sure you want to delete:\r ${rows}?`)) {
    }
  };

  handlerowChange = (state) => {
    console.log('state' ,state);
    this.setState({ selectedRows: state.selectedRows });
  };

  deleteHandler = (e) => {
    const { selectedRows } = this.state;
    const rows = selectedRows.map((r) => r.email);
    if (window.confirm(`Are you sure you want to delete:\r ${rows}?`)) {
    }
  };

  /* (onchange / add)    product category or primary category */

  handleCheckChieldElement = (event) => {
    let clickedValue = event.target.value;
    console.log('clickedValue', clickedValue);
    const list = [...this.state.userfilepermission, clickedValue];

    if (event.target.checked) {
      const list = [...this.state.userfilepermission, clickedValue];
      this.setState({
        userfilepermission: list,
      });
    } else {
      let userfilepermission = [...this.state.userfilepermission]; // make a separate copy of the array
      let index = userfilepermission.indexOf(event.target.value);
      if (index !== -1) {
        userfilepermission.splice(index, 1);
        this.setState({ userfilepermission: userfilepermission });
      }

    }

  };

  updatefilePermission = async () => {
    this.setState({
      message: '',
      message_type: '',
    });
    const userfilepermission = [...this.state.userfilepermission];
    const response = await manageUserService.updateUserfilepermission(userfilepermission);
    this.setState({ spinner: true });
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success",
        });
      } else {
        this.setState({
          message: response.data.message,
          message_type: "error",
        });
        this.setState({ spinner: false });
      }
    }
  };

  getFilepermissionById = async () => {
    this.setState({ spinner: true });
    const response = await manageUserService.getFilepermissionById();

    if (response.data.status == 1) {
      this.setState({
        userfilepermission: response.data.data[0].items,
      });

    }
    //  else {
    //   this.setState({
    //     userfilepermission: [],
    //   });
    // }
  };
  
  handleselecteduser = async e => {
    console.log('handleselecteduser',e);
    this.setState({ selecteduser: e.target.value });
    this.setState({ spinner: true });
    this.getAllusers();
    
  }
  
  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value })
  }
  
  handleChangeUserrole = async e => {
    this.setState({ Userrole: e.target.value })
  }
  actionHandler = e => {
    this.setState({
      message: '',
      message_type: '',
      toggleCleared: false
    })
    const { selectedRows } = this.state
    const Userids = selectedRows.map(r => r.id)
    let status = ''
    let actionvalue = this.state.contextActions_select
    if (actionvalue === '-1') {
      return
    } else if (actionvalue === '1') {
      status = 'Delete' 
      if (window.confirm(`Are you sure you want to ` + status )) {
      
        this.actionHandlerUser(Userids, actionvalue, status)
      }
    }  else if (actionvalue === '2'){
      if (this.state.Userrole == '-1') {
        return
      }
      status = 'Change Role' 
      if (window.confirm(`Are you sure you want to ` + status )) {
        
        this.actionHandlerUser(Userids, actionvalue, this.state.Userrole)
      }
    }

   
  }

  
  actionHandlerUser = async (Userids, contextActions, status) => {
    try {
      const response = await manageUserService.actionHandlerUser(
        Userids,
        contextActions,
        status
      )
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: 'success'
        }) 
        
        this.setState({
          Userrole: '-1',
          contextActions_select: '-1',
          toggleCleared: true 
        }) 
        this.getAllusers()
      } else {
        this.setState({
          message: response.data.message,
          message_type: 'error'
        })
        this.getAllusers()
      }
    } catch (err) {
      this.setState({
        message: 'Something went wrong',
        message_type: 'error'
      })
    }
  }


  
  handlepastdateChange = startDate => {
    const event = new Date();
    this.setState({
      startDate: (startDate ? startDate : moment(this.state.endDate).subtract(1, 'months').toDate())
    })
  }

  handletodateChange = endDate => {
    const event = new Date();
    this.setState({
      endDate: (endDate ? endDate : moment(this.state.startDate).add(1, 'months').toDate())
    })
  }
  render() {
    const { startDate , endDate } = this.state;
 
    const contextActions = memoize(actionHandler => (
      <div className='common_action_section'>
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value='-1'>Select</option>
          {this.state.activetab !== 'tab_a' ? (
            <option value='1'>Delete</option>
          ) : (
            ''
          ) 
          }
     
         <option value='2'>Change Role</option>
  
        </select>
        { this.state.contextActions_select === '2' ?
         ( <select 
          value={this.state.Userrole}
          onChange={this.handleChangeUserrole}
         >
          <option value='-1'>Select</option>
          { this.state.UserTypes.map(v => (
          <option key={v.id} value={v.id}   >{v.name}</option>
          ))
          }
          </select> ) : ''
        }
        <button className='danger' onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ))


    let rowData = this.state.data_table,
      search = this.state.search;
    let subscribe = this.state.users.filter((l) => {
      return (
        l.agree_newsletter === true 
      );
    });

    console.log('subscribe', subscribe , this.state.users);
    if (search.length > 0) {
      search = search.trim().toLowerCase();
      rowData = rowData.filter((l) => {
        return (
          l.first_name.toLowerCase().match(search) ||
          l.last_name.toLowerCase().match(search) ||
          l.email.toLowerCase().match(search) ||
          l.usertype.toLowerCase().match(search)
        );
      });
    }
    const { userfilepermission } = this.state;
    console.log('userfilepermission', userfilepermission);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body ">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>


                <div className="col-md-12  contents  home-inner-content ">
                <div className="row">
                     
                {this.state.message !== "" ? (
                      <div className="tableinformation" style={{'width' : '100%'}}>
                        <div className={this.state.message_type}>
                          <p>{this.state.message}</p>
                        </div>
                      </div>
                    ) : null
                    }
 </div>
                </div>

                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4">
                  <div className="main_admin_page_common">
                    
                    <div className="">
                      <div className="admin_breadcum">
                        <div className="row">
                        <div className="col-md-4">
                          </div>
                          <div className="col-md-3">
                     <div class="form-group userdropdown">
                            <div class="form-check">
                              <p>Start</p>
                     <DatePicker
                            selected={this.state.startDate}
                            selectsStart
                            dateFormat='dd/MM/yyyy'
                            onChange={this.handlepastdateChange}
                            startDate={startDate}
                            endDate={endDate}
                            maxDate={endDate}
                          />
                              </div> 
                          </div>  
                   
                          </div>
                     <div className="col-md-3">
                     <div class="form-group userdropdown">
                            <div class="form-check">
                            <p>End</p>
                     <DatePicker
                            selected={this.state.endDate}
                            selectsEnd
                            dateFormat='dd/MM/yyyy'
                            startDate={startDate}
        endDate={endDate}
        minDate={startDate}
                            onChange={this.handletodateChange}
                          />
                              </div> 
                          </div>  
                   
                          </div>
                  <div className="col-md-2">
                     <div class="form-group userdropdown">
                            <div class="form-check">
                              <select
                              
                              value={this.state.selecteduser}
                              className='form-group-relase'
                              onChange={this.handleselecteduser}
                              class="form-check-input">
                                <option value="">Sort by</option>
                                {
                                this.state.UserTypes.map((singleusertype) => (
                                <option value={singleusertype.id}>{singleusertype.name}</option>
                                ))
                                }
                              </select>
                            </div> 
                          </div>  

                  </div>

                          <div className="col-md-1">
                            <p className="page-title">Users</p>
                          </div>
                          <div className="col-md-6">
                            <Link className="add_new_btn" to="/admin/user/add">
                              Add New
                            </Link>
                            <Link
                              className="Export_data_btn"
                              onClick={() => this.downloadCSV(rowData)}
                            >
                              Export Data
                            </Link>
                          
                          </div>
                          <div className="col-md-5">
                            <div className="searchbox">
                              <div className="commonserachform">
                                <span></span>
                                <input
                                  type="text"
                                  placeholder="Search"
                                  onChange={(e) => this.searchSpace(e)}
                                  name="search"
                                  className="search form-control"
                                />
                                <input type="submit" className="submit_form" />
                              </div>
                            </div>
                          
                          </div>



                          <CSVLink
          filename={"subscribe.csv"}
          style={{'margin-left' : '140px' , 'margin-top' : '15px'}}
          className="Export_data_btn"
          data={subscribe}> Subscribe
          </CSVLink>
                          {/* <Link


                          style={{'margin-left' : '140px' , 'margin-top' : '15px'}}
                              className="Export_data_btn"
                              onClick={() => this.downloadCSV(subscribe)}
                            >
                              Subscribe
                            </Link> */}
                        </div>
                      </div>
                      <React.Fragment>
                        
                        <div
                          style={{
                            display:
                              this.state.spinner === true
                                ? "flex justify-content-center "
                                : "none",
                          }}
                          className="overlay text-center"
                        >

                         
                          <ReactSpinner
                            type="border"
                            color="primary"
                            size="10"
                          />
                        </div>
                      </React.Fragment>
                     
                      <DataTable
                       
                        columns={this.state.columns}
                        data={rowData}
                        highlightOnHover
                        pagination
                        selectableRows  
                        onSelectedRowsChange={this.handlerowChange}
                        selectableRowsVisibleOnly 
                        clearSelectedRows={this.state.toggleCleared}
                        contextActions={contextActions(this.deleteAll)}
                        noDataComponent = {<p>There are currently no records.</p>}
                      /> 
                    </div>
                  </div>
                </div>
           
                <div className="col-md-12  pb-5">
                  <div className="card-body user-permission">
                  
                    <div className="row">
                      <div className="col-md-12"><p class="page-title">File Permission</p></div>
                      {

                        this.state.UserTypes.map((singleusertype) => (
                          <div class="col-md-6">
                            <div class="form-group"><div class="form-check">
                              <input type="checkbox" id={singleusertype.id} class="form-check-input"
                                onChange={this.handleCheckChieldElement}
                                value={singleusertype.id}

                                checked={

                                  userfilepermission.length ?
                                    (userfilepermission.indexOf(
                                      singleusertype.id
                                    ) > -1
                                      ? true
                                      : false) : ''
                                }
                              />
                              <label title="" for="ControlTextarea1" class="form-check-label">{singleusertype.name}</label>
                            </div>
                            </div>
                          </div>
                        ))
                      }
                    </div>

                    <button
                      className="submit-btn"
                      onClick={this.updatefilePermission}
                    >
                      Update
                            </button>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.deleteUser}
        />
      </React.Fragment>
    );
  }
}

export default UsersList;
