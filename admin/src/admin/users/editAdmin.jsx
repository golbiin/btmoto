import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import { Link } from "react-router-dom";
import Uploadprofile from "../common/uploadProfileimage";
import * as manageUser from "../../ApiServices/admin/manageUser";
import BlockUi from 'react-block-ui';
class AdminEdit extends Component {
  state = {
    spinner: false,
    cimonUser: [],
    validated: false,
    userStatus: true,
    submitStatus: false,
    errors: {},
    upload_data: "",
   
  };

  UserSchema = {
    user_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the User name. It is not allowed to be empty.",
        };
      }),
    email: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Email. It is not allowed to be empty.",
        };
    }),
    first_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the First name. It is not allowed to be empty.",
        };
      }),
    last_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Last name. It is not allowed to be empty.",
        };
    }),
    _id: Joi.optional().label("id"),
    profile_image:Joi.string()
  };
 
 /*Input Handle Change */
  handleChange = (event, type = null) => {
    let cimonUser = { ...this.state.cimonUser };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateUserdata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    cimonUser[name] = value;
    this.setState({ cimonUser, errors });
  };
  validateUserdata = (name, value) => {
    const obj = { [name]: value };
    const UserSchema = { [name]: this.UserSchema[name] };
    const { error } = Joi.validate(obj, UserSchema);
    return error ? error.details[0].message : null;
  };

  isEmpty = (obj) => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };
  onuplaodProfile = async (value, item) => {

    let errors = { ...this.state.errors };
    let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      let file = value.target.files[0];
      upload_data = file;
      this.setState({ upload_data });
    }
 
  };


  componentDidMount = async () => {
    this.setState({ spinner: true });
    const id = this.props.match.params.id;
    const response = await manageUser.getSingleAdmin(id);
    if(response.data.status == 1){
      let newUserarray = {
        _id: response.data.data.user_data._id,
        user_name: response.data.data.user_data.username,
        email: response.data.data.user_data.email,
        first_name: response.data.data.user_data.first_name,
        last_name: response.data.data.user_data.last_name,
        profile_image: response.data.data.user_data.profile_image,
      };
      this.setState({ cimonUser : newUserarray });
    }else{
      this.setState({
        userStatus: false,
        message: response.data.message,
        responsetype: "error",
      });
    }
    this.setState({ spinner: false });
  };

  updateUser = async () => {
    this.setState({ submitStatus: true });
    let cimonUser = { ...this.state.cimonUser };
    const errors = { ...this.state.errors };
    let user_validate = Joi.validate(cimonUser, this.UserSchema);
    if (user_validate.error) {
      if (user_validate.error) {
        let path = user_validate.error.details[0].path[0];
        let errormessage = user_validate.error.details[0].message;
        errors[path] = errormessage;
      }
      errors["validate"] = "Please be sure you’ve filled all fields.";
      this.setState({ errors });
      this.setState({
        submitStatus: false,
        message: "",
        responsetype: "",
      });
      this.setState({ errors });
      
    } else {
      if (this.isEmpty(errors)) {
        
        if (this.state.upload_data) {
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          );
          
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            let cimonUser = { ...this.state.cimonUser };
            cimonUser["profile_image"] = filepath;
            this.setState({ cimonUser });
            this.updateUserdata();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error",
            });
            this.setState({ spinner: false });
          }
        } else {
        this.updateUserdata();
        }
      }
    }
  };

  updateUserdata = async () => {
    let cimonUser = { ...this.state.cimonUser };
    const response = await manageUser.updateAdmindata(cimonUser);
    if (response.data.status == 1) {
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "success",
      });
      this.setState({ spinner: false });
      setTimeout(() => { 
        window.location.reload();
      }, 2000);
    } else {
      this.setState({ spinner: false });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "error",
      });
    }
  };

  render() {
    return (
      <React.Fragment>
            <div className="container-fluid admin-body ">
                <div className="admin-row">
                    <div className="col-md-2 col-sm-12 sidebar">
                        <Adminsidebar props={this.props} />
                    </div>
                    
                    <div className="col-md-10 col-sm-12  content">
                      <div className="row content-row">
                          <div className="col-md-12  header">
                              <Adminheader  props={this.props} />
                          </div>
                          <div className="col-md-12  contents  useradd-new pt-4 pb-4 pr-4" >   
                            <React.Fragment> 
                            <BlockUi tag="div" blocking={this.state.spinner} >          
                              <div className="card">
                                <div className="card-header">User Details</div>
                                <div className="card-body">                 
                                  <div className="form-group">
                                    <Uploadprofile
                                      onuplaodProfile={this.onuplaodProfile}
                                      value={
                                        this.state.cimonUser.profile_image
                                      }
                                      errors={this.state.errors}
                                    />
                                  </div>
                                
                                  <div className="form-group">
                                    <label>UserName</label>
                                    <input
                                      type="text"
                                      name="user_name"
                                      className="form-control "
                                      disabled = "disabled"
                                      value={this.state.cimonUser.user_name}
                                      onChange={(e) => this.handleChange(e)}
                                    />
                                    {this.state.errors.user_name !== "" ? (
                                      <div className="validate_error">
                                        <p>{this.state.errors.user_name}</p>
                                      </div>
                                    ) : null}
                                </div>

                                <div className="form-group">
                                    <label>Email</label>
                                    <input
                                      type="text"
                                      name="email"
                                      className="form-control "
                                      disabled = "disabled"
                                      value={this.state.cimonUser.email}
                                      
                                    />
                                   
                                </div>

                                <div className="form-group">
                                    <label>First Name</label>
                                    <input
                                      type="text"
                                      name="first_name"
                                      className="form-control "
                                      value={this.state.cimonUser.first_name}
                                      onChange={(e) => this.handleChange(e)}
                                    />
                                    {this.state.errors.first_name !== "" ? (
                                      <div className="validate_error">
                                        <p>{this.state.errors.first_name}</p>
                                      </div>
                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <label>Last Name</label>
                                    <input
                                      type="text"
                                      name="last_name"
                                      className="form-control "
                                      value={this.state.cimonUser.last_name}
                                      onChange={(e) => this.handleChange(e)}
                                    />
                                    {this.state.errors.last_name !== "" ? (
                                      <div className="validate_error">
                                        <p>{this.state.errors.last_name}</p>
                                      </div>
                                    ) : null}
                                </div>
                               
                            </div>
                          </div>

                              <div className="submit">
                              <button
                                  className="submit-btn"
                                  onClick={this.updateUser}
                                  >
                                  Update
                                </button>
                                <Link to="/admin/userslist" className="cancel-btn">Cancel</Link>
                                {this.state.message !== "" ? (
                                      <div className="form-group tableinformation"><div className={this.state.responsetype} >
                                        <p>{this.state.message}</p>
                                      </div></div>
                                    ) : null}

                                  
                              </div>
                             
                                  
                            </BlockUi>
                            </React.Fragment>
                       </div>
                      </div>
                    </div>
                  </div>
            </div>
          </React.Fragment> );
     
  }
}

export default AdminEdit;
