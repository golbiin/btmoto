import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import { Link } from "react-router-dom";
import Joi, { join } from "joi-browser";
import * as manageUserService from "../../ApiServices/admin/manageUser";
import Uploadprofile from "../common/uploadProfileimage";
import { CountryDropdown } from 'react-country-region-selector';
import { bussinessType, industry, categories } from "../../config.json";
import BlockUi from 'react-block-ui';
class UserAdd extends Component {
    constructor(props) {
        super(props);
          this._handleImageChange = this._handleImageChange.bind(this);
          this.handleCheck = this.handleCheck.bind(this);
    }
    state = {
      spinner: false,
      UserTypes: [
        // { name: 'Admin', id: 'admin' },
        { name: 'Distributor', id: 'distributor' },
        { name: 'Integrator', id: 'integrator' },
        { name: 'Subscriber', id: 'subscriber' },
    ],



        imagePreviewUrl : '',
        file : '',
        labUser: [],
        validated: false,
        fetch_error: "",
        userStatus: true,
        bussiness_type: '',
        agree_newsletter: false,
        interested_products: '',
        data: { 
            email:"" ,
            firstName:"" ,
            lastName:"" ,
            password:"",
            usertype:"",
            company_name:"",
            city:"",
            zipcode:"",
            phone_number:"",
            recovery_email:"",
            country: "",
            drivelink: ""
          },
        dataCopy: { 
            email:"" ,
            firstName:"" ,
            lastName:"" ,
            password:"",
            usertype:"",
            company_name:"",
            city:"",
            zipcode:"",
            phone_number:"",
            recovery_email:"",
            country: "",
            drivelink: ""
          },
        errors: {},
        upload_data: "",
        submit_status: false,
        message: "",
        responsetype: "",
      };

      _handleImageChange = e => {
        let reader = new FileReader();
        let file = e.target.files[0];
        if(file){
            reader.onloadend = () => {
                this.setState({
                  file: file,
                  imagePreviewUrl: reader.result
                });
              }
              reader.readAsDataURL(file)
        }
      }
    
      /* Joi validation schema */  
        schema = {

          email: Joi.string().email().error(() => {
            return {
              message: '"Email" must be a valid email',
            };
          }),
          firstName: Joi.string()
          .required()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
          lastName:Joi.string()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
          password:Joi.string().min(8).required().label("Password"),
          usertype: Joi.string()
          .required()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
          drivelink: Joi.string().allow(null).allow('')
          .error(() => {
            return {
              message:
                "Please be sure you’ve filled in the url. It is not allowed to be empty.",
            };
          }),
          company_name: Joi.string().required()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
          country: Joi.optional().label("country").error(() => {
            return {
              message: "Please select your Country.",
            };
          }),
          city: Joi.string().required()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
          zipcode: Joi.string().required()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
          phone_number: Joi.string().required()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),

          recovery_email: Joi.string().required()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
         
      };
    
      selectCountry (val) {
        let data = { ...this.state.data };
        let errors = { ...this.state.errors };
        data['country']= val;
        if(data.country === ""){
          errors.country = " Country is required";
        } else{
            delete errors.country;
        }
        this.setState({ data, errors });
      }
      /*Input Handle Change */
      handleChange = (input) => (event) => {
        const data = { ...this.state.data };
        const errors = { ...this.state.errors };
        delete errors.validate;
        const errorMessage = this.validateProperty(input, event.target.value);
        if (errorMessage) errors[input] = errorMessage;
        else delete errors[input];
        data[input] = event.target.value;
        this.setState({ data, errors });
      };
    
    /* Joi validation call*/
      validateProperty = (name, value) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
      };
    
      onuplaodProfile = async (value, item) => {

        let errors = { ...this.state.errors };
        let upload_data = { ...this.state.upload_data };
        if (item === "errors") {
          errors["profile_image"] = value;
        } else {
          let file = value.target.files[0];
          upload_data = file;
          this.setState({ upload_data });
        }
     
      };

     /* Form Submit */
      handleSubmit = async () => {
        const errors = { ...this.state.errors };
        const data = { ...this.state.data };
        const dataCopy = { ...this.state.dataCopy };
        if(data.country === ""){
          errors.country = " Please select your Country.";
          this.setState({ errors });
      } else{
          delete errors.country;
          this.setState({ errors });
      }
        if (
          data.firstName === "" ||
          data.lastName === "" ||
          data.email === "" ||
          data.password === "" ||
          data.usertype === "" ||
          data.company_name ===""||
          data.city === "" ||
          data.zipcode === "" ||
          data.phone_number === "" ||
       //   data.recovery_email === "" ||
          data.country === ""

        ) {
          errors.validate = "Please fill in all fields prior to proceeding.";
          this.setState({ errors });
        } else {
          if (errors.length > 0) {
            errors.validate = "Please fill in all fields prior to proceeding.";
            this.setState({ errors });
            
          } else {
              try {
                delete errors.validate;
                this.setState({ spinner: true });
                if (this.state.upload_data) {
                  const response1 = await manageUserService.uploadProfile(
                    this.state.upload_data
                  );
                  
                  if (response1.data.status == 1) {
                    let filepath = response1.data.data.file_location;
                    data["profile_image"] = filepath;
                    this.setState({ data });
                    this.insertUserdata();
                  } else {
                    this.setState({
                      submitStatus: false,
                      message: response1.data.message,
                      responsetype: "error",
                    });
                    this.setState({ spinner: false });
                  }
                } else {
                  this.insertUserdata();
                }
              } catch (err) {
                return false;
              }
          }
        }
      };
      insertUserdata = async () => {
          let data = { ...this.state.data };
          data["bussiness_type"] = this.state.bussiness_type;
          data["industry"] = this.state.industry;
          data["agree_newsletter"] = this.state.agree_newsletter;
          data["interested_products"] = this.state.interested_products;
          const dataCopy = { ...this.state.dataCopy };
          const response = await manageUserService.insertUserdata(data);
          
          if (response) {
            if (response.data.status == 1) {
              this.setState({ data: dataCopy });
              this.setState({
                message: response.data.message,
                responsetype: "success",
              });
              this.setState({ spinner: false });
              setTimeout(() => { 
                window.location.reload();
              }, 2000);
            }else{
              this.setState({ spinner: false });
              this.setState({
                message: response.data.message,
                responsetype: "error",
              });
            }
            return response;
          }
      };

      getFilepermissionById = async () => {
        // this.setState({ spinner: true });
        const response = await manageUserService.getFilepermissionById();
    
        if (response.data.status == 1) {
          this.setState({
            userfilepermission: response.data.data[0].items,
          });
    
        }
        else {
          this.setState({
            userfilepermission: [],
          });
        }
      };
      
      componentDidMount = async () => {
        this.getFilepermissionById();
      };


      bussinessType = async (e) => {
        console.log('bussiness_type', e.target.value);
        this.setState({ bussiness_type: e.target.value });
      }
      industryType = async (e) => {
        console.log('industry', e.target.value);
        this.setState({ industry: e.target.value });
      }
      /* Checkbox on Change */
      handleCheck(e) {
        this.setState({ agree_newsletter: e.target.checked });
      }


    render() { 
      const selectedProducts = this.state.interested_products.split(',');
    console.log('selectedProducts', selectedProducts);

      let optionUserType = this.state.UserTypes.map(v => (
        <option key={v.id} value={v.id} >{v.name}</option>
       
      ));
    

        const images = require.context("../../assets/images/admin", true);  

        let {imagePreviewUrl} = this.state;
        let $imagePreview = <img className="img-fluid" src={images("./uploadimage.png")} />;
        if (imagePreviewUrl) {
          $imagePreview = (<img className="img-fluid" src={imagePreviewUrl} />);
        }
        const {country} = this.state.data;

        console.log('userfilepermission', this.state.userfilepermission);
        return (
           
            <React.Fragment>
            <div className="container-fluid admin-body ">
                <div className="admin-row">
                    <div className="col-md-2 col-sm-12 sidebar">
                        <Adminsidebar props={this.props} />
                    </div>
                    
                    <div className="col-md-10 col-sm-12  content">
                      <div className="row content-row">
                          <div className="col-md-12  header">
                              <Adminheader  props={this.props} />
                          </div>
                          <div className="col-md-12  contents  useradd-new pt-4 pb-4 pr-4" >   
                            <React.Fragment>   
                            <BlockUi tag="div" blocking={this.state.spinner} >
                              <div className="card">
                                <div className="card-header">User Details</div>
                               
                                <div className="card-body">
                                {this.state.errors.validate !== "" ? (
                                <div className="form-group"><div className="error text-danger">
                                  <p>{this.state.errors.validate}</p>
                                </div></div>
                              ) : null}

                                <div className="form-group thumbanail_container">
                                   <Uploadprofile
                                      onuplaodProfile={this.onuplaodProfile}
                                      value={
                                        this.state.data.profile_image
                                      }
                                      errors={this.state.errors}
                                    />
                                </div>

                                <div className="form-group">
                                  <label>Email</label>
                                  <input onChange={this.handleChange("email")} name="email" type="text" placeholder="Email" className="form-control"/>
                                  {this.state.errors.email ? (<div className="error text-danger">
                                  {this.state.errors.email}
                                  </div>
                                  ) : (
                                  ""
                                  )}
                                </div>    
                                <div className="form-group">
                                  <label>First Name</label>
                                  <input name="firstName"  onChange={this.handleChange("firstName")} type="text" placeholder="First Name" className="form-control"/>
                                  {this.state.errors.firstName ? (<div className="error text-danger">
                                            {this.state.errors.firstName}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                  <label>Last Name</label>
                                  <input name="lastName" onChange={this.handleChange("lastName")} type="text" placeholder="Last Name" className="form-control"/>
                                  {this.state.errors.lastName ? (<div className="error text-danger">
                                            {this.state.errors.lastName}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>

                                <div className="form-group">
                                  <label>Password</label>
                                  <input name="password" onChange={this.handleChange("password")}  type="password" placeholder="Password" className="form-control"/>
                                  {this.state.errors.password ? (<div className="error text-danger">
                                            {this.state.errors.password}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>

                                <div className="form-group">
                                    <label>User Type</label>
                                    <select name="usertype" onChange={this.handleChange("usertype")}>
                                    <option value="">Select User Type</option>
                                        {optionUserType}
                                    </select>
                                    {this.state.errors.usertype ? (<div className="error text-danger">
                                            {this.state.errors.usertype}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                  <label>Company Name</label>
                                  <input name="company_name" onChange={this.handleChange("company_name")} type="text" placeholder="Company Name" className="form-control"/>
                                  {this.state.errors.company_name ? (<div className="error text-danger">
                                            {this.state.errors.company_name}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                <label>Country </label>
                                <CountryDropdown className="form-control"                                          
                                value={country}
                                onChange={(val) => this.selectCountry(val)} /> 
                                {this.state.errors.country ? (
                                <div className="danger">
                                {this.state.errors.country}
                                </div>
                                ) : ( "" )}

                                
                                </div>
                                <div className="form-group">
                                  <label>City</label>
                                  <input name="city" onChange={this.handleChange("city")} type="text" placeholder="City" className="form-control"/>
                                  {this.state.errors.city ? (<div className="error text-danger">
                                            {this.state.errors.city}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                  <label>Zipcode</label>
                                  <input name="zipcode" onChange={this.handleChange("zipcode")} type="text" placeholder="Zipcode" className="form-control"/>
                                  {this.state.errors.zipcode ? (<div className="error text-danger">
                                            {this.state.errors.zipcode}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                  <label>Phone Number</label>
                                  <input name="phone_number" onChange={this.handleChange("phone_number")} type="text" placeholder="Phone Number" className="form-control"/>
                                  {this.state.errors.phone_number ? (<div className="error text-danger">
                                            {this.state.errors.phone_number}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>

                                <div className="form-group">
                                  <label>Recovery Email</label>
                                  <input onChange={this.handleChange("recovery_email")} name="recovery_email" type="text" placeholder="Recovery Email" className="form-control"/>
                                  {this.state.errors.recovery_email ? (<div className="error text-danger">
                                  {this.state.errors.recovery_email}
                                  </div>
                                  ) : (
                                  ""
                                  )}
                                </div> 


                                {(() => {

if (this.state.userfilepermission) {

  if (this.state.userfilepermission.includes(this.state.data.usertype)) {
    return (
      <div className="form-group">
        <label>Google Drive Link</label>
        <input
          type="text"
          name="drivelink"
          className="form-control "
           onChange={this.handleChange("drivelink")} 

        //  onChange={(e) => this.handleChange(e)}
        />
      </div>

    )

  }
}
})()}


<div className="form-group productlisting">
                          <div className={`register-form-group`}>
                            <label >
                              {"Which CIMON products are you interested in?"}
                            </label>
                            {(categories || []).map((item) => (
                              <span>
                                < input
                                  name="selected_products"
                                  type="checkbox"
                                  value={item.value ?? ""}
                                  label={item.label}
                                  id={item.label}
                                  checked={this.state.interested_products.includes(item.value) ? true : false}
                                  className="register-checkbox"
                                  onChange={(e) => {

                                    categories.forEach((category) => {
                                      if (category.value === e.target.value) {
                                        category.checked = e.target.checked;
                                        console.log('eeeeee', category.checked, e.target.checked, e.target.value);
                                        if (category.checked == true) {
                                          selectedProducts.push(category.value);
                                        } else {
                                          selectedProducts.pop(category.value);
                                        }
                                      }

                                    });
                                    const p = selectedProducts.join(",");
                                    this.setState({ interested_products: p });
                                    console.log('pppp', p);
                                    //     setInterestedProducts(p);
                                  }}
                                />
                                <label for={item.label} id={item.label}>{item.label}</label></span>
                            ))}
                          </div>

                        </div>



                        <div className="form-group">
                          <label>Business Type</label>
                          <select
                            id="bussiness_type"

                            className="profile-input"

                            value={this.state.bussiness_type}
                            //   defaultValue = { props.profile.country}
                            onChange={this.bussinessType}

                          >
                            <option key={""} value={""}>
                              Select Business Type
                            </option>
                            {(bussinessType || []).map((item) => (
                              <option key={item.value} value={item.value}>
                                {item.label || item.value}
                              </option>
                            ))}
                          </select>


                        </div>





                        <div className="form-group">
                          <label>Industry</label>
                          <select
                            id="industry"

                            className="profile-input"

                            value={this.state.industry}
                            //   defaultValue = { props.profile.country}
                            onChange={this.industryType}

                          >
                            <option key={""} value={""}>
                              Select Industry
                            </option>
                            {(industry || []).map((item) => (
                              <option key={item.value} value={item.value}>
                                {item.label || item.value}
                              </option>
                            ))}
                          </select>


                        </div>


  <div style={{ 'padding-left': '20px' }} className="form-group">
                            <input
                              onChange={this.handleCheck} checked={this.state.agree_newsletter}
                              type="checkbox" id="agree_newsletter" class="form-check-input" />
                            <label title="" for="agree_newsletter" class="form-check-label">Sign me up for the newsletter to get current news, events and product updates</label>
                          </div>
                            </div>
                          </div>

                              <div className="submit">
                              <button
                                  className="submit-btn"
                                  onClick={this.handleSubmit}
                                >
                                  Submit
                                </button>
                                <Link to="/admin/userslist" className="cancel-btn">Cancel</Link>
                              </div>
                              
                              {this.state.message !== "" ? (
                                      <div className="form-group"><div className={this.state.responsetype} >
                                        <p>{this.state.message}</p>
                                      </div></div>
                                    ) : null}
                            </BlockUi>
                            </React.Fragment>
                       </div>
                      </div>
                    </div>
                  </div>
            </div>
          </React.Fragment> );
    }
}
 
export default UserAdd;