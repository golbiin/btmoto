import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import { Link } from "react-router-dom";
import validate from "react-joi-validation";
import Joi, { join } from "joi-browser";

import * as manageUserService from "../../ApiServices/admin/manageUser";
import * as authServices from "../../ApiServices/authenticate";
import Uploadprofile from "../common/uploadProfileimage";
import BlockUi from 'react-block-ui';
class AdminAdd extends Component {
    constructor(props) {
        super(props);
          this._handleImageChange = this._handleImageChange.bind(this);
    }
    state = {
        spinner: false,
        imagePreviewUrl : '',
        file : '',
        labUser: [],
        validated: false,
        fetch_error: "",
        userStatus: true,
        
        data: { userName: "" , email:"" , firstName:"" , lastName:"" , password:""},
        dataCopy: { userName: "" , email:"" , firstName:"" , lastName:"" , password:""},
        errors: {},
        upload_data: "",
        submit_status: false,
        message: "",
        responsetype: "",
      };

      _handleImageChange = e => {
        let reader = new FileReader();
        let file = e.target.files[0];
        if(file){
            reader.onloadend = () => {
                this.setState({
                  file: file,
                  imagePreviewUrl: reader.result
                });
              }
              reader.readAsDataURL(file)
        }
      }
    
      /* Joi validation schema */ 
        schema = {
          userName: Joi.string()
          .required()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
          
          email: Joi.string().email().label("Email").error(() => {
            return {
              message: '"Email" must be a valid email',
            };
          }),
          firstName: Joi.string()
          .required()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
          lastName:Joi.string()
          .error(() => {
            return {
              message: "This field is Required",
            };
          }),
          password:Joi.string().min(8).label("Password")
          .required() ,
      };
   
    
     /*Input Handle Change */
      handleChange = (input) => (event) => {
        const data = { ...this.state.data };
        const errors = { ...this.state.errors };
        delete errors.validate;
        const errorMessage = this.validateProperty(input, event.target.value);
        if (errorMessage) errors[input] = errorMessage;
        else delete errors[input];
        data[input] = event.target.value;
        this.setState({ data, errors });
      };
    
/* Joi validation call*/
      validateProperty = (name, value) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
      };

/* Upload profile */
      onuplaodProfile = async (value, item) => {
        let errors = { ...this.state.errors };
        let upload_data = { ...this.state.upload_data };
        if (item === "errors") {
          errors["profile_image"] = value;
        } else {
          let file = value.target.files[0];
          upload_data = file;
          this.setState({ upload_data });
        }
     
      };

    /* Form Submit */
      handleSubmit = async () => {

        const errors = { ...this.state.errors };
        const data = { ...this.state.data };
        const dataCopy = { ...this.state.dataCopy };
        if (
          data.userName === "" ||
          data.firstName === "" ||
          data.lastName === "" ||
          data.email === "" ||
          data.password === "" 
        ) {
          errors.validate = "Please fill in all fields prior to proceeding.";
          this.setState({ errors });
        } else {
          if (errors.length > 0) {
            errors.validate = "Please fill in all fields prior to proceeding.";
            this.setState({ errors });
          } else {
              try {
                delete errors.validate;
                this.setState({ spinner: true });
                if (this.state.upload_data) {
                  const response1 = await manageUserService.uploadProfile(
                    this.state.upload_data
                  );
                  
                  if (response1.data.status == 1) {
                    let filepath = response1.data.data.file_location;
                    data["profile_image"] = filepath;
                    this.setState({ data });
                    this.insertUserdata();
                  } else {
                    this.setState({
                      submitStatus: false,
                      message: response1.data.message,
                      responsetype: "error",
                      spinner: false
                    });
                    
                  }
                } else {
                  this.insertUserdata();
                }
              } catch (err) {
                return false;
              }
          }
        }
      };
      insertUserdata = async () => {
          let data = { ...this.state.data };
          const dataCopy = { ...this.state.dataCopy };
          const response = await manageUserService.insertAdmindata(data);
          if (response) {
            if (response.data.status == 1) {
              this.setState({ data: dataCopy });
              this.setState({
                message: response.data.message,
                responsetype: "success",
              });
              this.setState({ spinner: false });
              setTimeout(() => { 
                window.location.reload();
              }, 2000);
            }else{
              this.setState({
                message: response.data.message,
                responsetype: "error",
              });
              this.setState({ spinner: false });
            }
            
            return response;
          }
      };
    render() { 
        const images = require.context("../../assets/images/admin", true);  

        let {imagePreviewUrl} = this.state;
        let $imagePreview = <img className="img-fluid" src={images("./uploadimage.png")} />;
        if (imagePreviewUrl) {
          $imagePreview = (<img className="img-fluid" src={imagePreviewUrl} />);
        }

        return (
           
            <React.Fragment>
            <div className="container-fluid admin-body ">
                <div className="admin-row">
                    <div className="col-md-2 col-sm-12 sidebar">
                        <Adminsidebar props={this.props} />
                    </div>
                    
                    <div className="col-md-10 col-sm-12  content">
                      <div className="row content-row">
                          <div className="col-md-12  header">
                              <Adminheader  props={this.props} />
                          </div>
                          <div className="col-md-12  contents  useradd-new pt-4 pb-4 pr-4" >   
                            <React.Fragment>   
                            <BlockUi tag="div" blocking={this.state.spinner} >    
                              <div className="card">
                                <div className="card-header">Admin Details</div>
                               
                                <div className="card-body">
                                {this.state.errors.validate !== "" ? (
                                <div className="form-group tableinformation"><div className="error text-danger">
                                  <p>{this.state.errors.validate}</p>
                                </div></div>
                              ) : null}

                                <div className="form-group">
                                   <Uploadprofile
                                      onuplaodProfile={this.onuplaodProfile}
                                      value={
                                        this.state.data.profile_image
                                      }
                                      errors={this.state.errors}
                                    />
                                </div>

                                 <div className="form-group">
                                  <label>Username </label>
                                  <input name="userName"  onChange={this.handleChange("userName")}  type="text" placeholder="Username *" className="form-control"/>
                                  {this.state.errors.userName ? (<div className="error text-danger">
                                            {this.state.errors.userName}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                
                                </div>  
                                <div className="form-group">
                                  <label>Email</label>
                                  <input onChange={this.handleChange("email")} name="email" type="text" placeholder="Email *" className="form-control"/>
                                  {this.state.errors.email ? (<div className="error text-danger">
                                  {this.state.errors.email}
                                  </div>
                                  ) : (
                                  ""
                                  )}
                                </div>    
                                <div className="form-group">
                                  <label>First Name</label>
                                  <input name="firstName"  onChange={this.handleChange("firstName")} type="text" placeholder="First Name *" className="form-control"/>
                                  {this.state.errors.firstName ? (<div className="error text-danger">
                                            {this.state.errors.firstName}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                <div className="form-group">
                                  <label>Last Name</label>
                                  <input name="lastName" onChange={this.handleChange("lastName")} type="text" placeholder="Last Name *" className="form-control"/>
                                  {this.state.errors.lastName ? (<div className="error text-danger">
                                            {this.state.errors.lastName}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>

                                <div className="form-group">
                                  <label>Password</label>
                                  <input name="password" onChange={this.handleChange("password")}  type="password" placeholder="Password *" className="form-control"/>
                                  {this.state.errors.password ? (<div className="error text-danger">
                                            {this.state.errors.password}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                               
                              
                            </div>
                          </div>


                              <div className="submit">
                              <button
                                  className="submit-btn"
                                  onClick={this.handleSubmit}
                                >
                                  Submit
                                </button>
                                <Link to="/admin/userslist" className="cancel-btn">Cancel</Link>
                                {this.state.message !== "" ? (
                                      <div className="form-group tableinformation"><div className={this.state.responsetype} >
                                        <p>{this.state.message}</p>
                                      </div></div>
                                    ) : null}

                              </div>
                            </BlockUi>
                            </React.Fragment>
                       </div>
                      </div>
                    </div>
                  </div>
            </div>
          </React.Fragment> );
    }
}
 
export default AdminAdd;