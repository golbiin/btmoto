import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import { Link } from "react-router-dom";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl} from "../../../src/config.json";
class ManageFooter extends Component {
  constructor(props) {
    super(props);
    this.onEditorChange1 = this.onEditorChange1.bind( this );
    this.onEditorChange2 = this.onEditorChange2.bind( this );
    this.onEditorChange3 = this.onEditorChange3.bind( this );
  }
  
    state = {
        data: { 
          col_number : "1",
          socket_number : "1",
        },
        col1_text : "",
        col2_text : "",
        col3_text : "",
        col4_text : "",
        col5_text : "",
        col1_menu : "",
        col2_menu : "",
        col3_menu : "",
        col4_menu : "",
        col5_menu : "",
        socket1_content : "",
        socket2_content : "<p>Edit Content</p>",
        socket3_content : "",
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        spinner : true,
        columnsRadio : [
          { id: 1, name: 'One'},
          { id: 2, name: 'Two'},
          { id: 3, name: 'Three'},
          { id: 4, name: 'Four'},
          { id: 5, name: 'Five'},
        ],
        socketRadio : [
          { id: 1, name: 'One'},
          { id: 2, name: 'Two'},
          { id: 3, name: 'Three'},
        ],
        allMenus: [
          { name: 'One', id: 1 },
          { name: 'Two', id: 2 },
          { name: 'Three', id: 3 },
        ],
        focused: false
    };
    
    /*  footer Editor */
    onEditorChange1( evt ) {
      if(evt.editor.getData().length){
        this.setState( {
          socket1_content: evt.editor.getData()
        });
      }
    }
    handleEditorChange = name => (changeEvent) => {
      this.setState( {
        [name]: changeEvent.target.value
      });
    }   
    onEditorChange2( evt ) {
      if(evt.editor.getData().length){
        this.setState( {
          socket2_content: evt.editor.getData()
        });
      }     
    }
    onEditorChange3( evt ) {
      if(evt.editor.getData().length){
        this.setState( {
          socket3_content: evt.editor.getData()
        });
      }
     
    }

    /*Joi validation schema */  
    schema = {
      col_number: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
      socket_number: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
    };
    /* end Joi validation schema */
    setColNumber(event) { 
      const data = { ...this.state.data };
      data["col_number"] = event.target.value;
      this.setState({
        data : data
      })
    }
    setSocketNumber(event) { 
      const data = { ...this.state.data };
      data["socket_number"] = event.target.value;
      this.setState({
        data : data
      })
    }
    handletextareaChange =  async (event) => {
      let name = event.target.name; //input field  name
      let value = event.target.value; //input field value      
      this.setState({ [event.target.name]: event.target.value });
    }

    componentDidMount = () => {
      setTimeout(() => { 
          this.getAllFooterDetails();
          this.getAllMenus();
      }, 2000);
    };

    /* Get all Menus*/
    getAllMenus = async () => {
      try {
        const response =  await settingsService.getAllMenus();
        if (response.data.status == 1) {
          this.setState({ allMenus: response.data.data});
        } 
      } catch (err) {        
      }
    }

    /* Get all Footer details */
    getAllFooterDetails = async () => {
      this.setState({ spinner: true});
      try {
        const response =  await settingsService.getAllFooterDetails();
          if (response.data.status == 1) {
            let newFooterarray = {
              col_number: response.data.data.col_number,
              socket_number: response.data.data.socket_number,
            };
            this.setState({ data : newFooterarray });
            if(response.data.data.column_content){
              this.setState({ col1_text : response.data.data.column_content.col1_content.text });
              this.setState({ col2_text : response.data.data.column_content.col2_content.text });
              this.setState({ col3_text : response.data.data.column_content.col3_content.text });
              this.setState({ col4_text : response.data.data.column_content.col4_content.text });
              this.setState({ col5_text : response.data.data.column_content.col5_content.text });
              this.setState({ col1_menu : response.data.data.column_content.col1_content.menu });
              this.setState({ col2_menu : response.data.data.column_content.col2_content.menu });
              this.setState({ col3_menu : response.data.data.column_content.col3_content.menu });
              this.setState({ col4_menu : response.data.data.column_content.col4_content.menu });
              this.setState({ col5_menu : response.data.data.column_content.col5_content.menu });
            }
            if(response.data.data.socket_content){
              this.setState({ socket1_content : response.data.data.socket_content.socket1_content });
              this.setState({ socket2_content : response.data.data.socket_content.socket2_content });
              this.setState({ socket3_content : response.data.data.socket_content.socket3_content });
            }
          } 
        this.setState({ spinner: false});
      } catch (err) {
        this.setState({ spinner: false});
      }
    }

    /* Form Submit */
    handleSubmit = async () => {
      const data = { ...this.state.data };
      const errors = { ...this.state.errors };
      let result = Joi.validate(data, this.schema);
      if (result.error) {
        
        let path = result.error.details[0].path[0];
        let errormessage = result.error.details[0].message;
        errors[path] = errormessage;
            this.setState({ errors: errors  })
      } else {
        
          if (errors.length > 0) {
              this.setState({
                  errors: errors  
                })
          }else{
            this.setState({ submit_status: true });
            this.setState({ spinner: true });

            const footer_data = {
              col_number: this.state.data.col_number,
              socket_number : this.state.data.socket_number,
            }
            footer_data["column_content"] = { 
              col1_content: {"text" : this.state.col1_text , "menu" : this.state.col1_menu },
              col2_content: {"text" : this.state.col2_text , "menu" : this.state.col2_menu },
              col3_content: {"text" : this.state.col3_text , "menu" : this.state.col3_menu },
              col4_content: {"text" : this.state.col4_text , "menu" : this.state.col4_menu },
              col5_content: {"text" : this.state.col5_text , "menu" : this.state.col5_menu },
            };
            footer_data["socket_content"] = { 
              socket1_content: this.state.socket1_content ,
              socket2_content: this.state.socket2_content ,
              socket3_content: this.state.socket3_content, 
            };
            
            const response1 = await settingsService.updateFooter(footer_data);
            if (response1.data.status == 1) {
              this.setState({ spinner: false });
              this.setState({
                  message: response1.data.message,
                  message_type: "success",
                });
                this.setState({ submit_status: false });
                setTimeout(() => { 
                  this.props.history.push({
                      pathname: "/admin/manage-footer/"
                    });

                }, 5000);
            }else {
              this.setState({ spinner: false });
              this.setState({ submit_status: false });
              this.setState({
                  message: response1.data.message,
                  message_type: "error",
                });
            }
          }
        }
    };

    render() {

        let optionTemplate = this.state.allMenus.map(v => (
          <option value={v._id}>{v.name}</option>
        ));

        var textareaValue = {};
        if ( !this.state.focused ) {
            textareaValue = {
                value: this.state.editordata
            };
        }

        
        var singleElements=[];
        for(var i=1;i<=this.state.data.col_number;i++){
          // push the component to elements!
          let name = '';
          let menu_name = '';
          let menu_val = '';
          let input_name = '';
          let value = '';
          if(i == 1){
            name = "First";
            menu_name = 'col1_menu';
            menu_val = this.state.col1_menu;
            input_name = 'col1_text';
            value = this.state.col1_text;
          }else if(i == 2){
            name = "Second"
            menu_name = 'col2_menu';
            menu_val = this.state.col2_menu;
            input_name = 'col2_text';
            value = this.state.col2_text;
          }else if(i == 3){
            name = "Third";
            menu_name = 'col3_menu';
            menu_val = this.state.col3_menu;
            input_name = 'col3_text';
            value = this.state.col3_text;
          }else if(i == 4){
            name = "Fourth";
            menu_name = 'col4_menu';
            menu_val = this.state.col4_menu;
            input_name = 'col4_text';
            value = this.state.col4_text;
          }else if(i == 5){
            name = "Fifth";
            menu_name = 'col5_menu';
            menu_val = this.state.col5_menu;
            input_name = 'col5_text';
            value = this.state.col5_text;
          }else{
            name = "";
          }

          
          singleElements.push(
            <div className="form-group row col-content-single">
              <div className="col-sm-4 ">
                  <label>{name} column content</label>
              </div>
              <div className="col-sm-8 ">
                <label>Custom Html</label>
                <textarea name={input_name} value={value} onChange={this.handletextareaChange}  className="form-control"></textarea>
                <label>Menu</label>
                <select value={menu_val} name={menu_name} onChange={this.handletextareaChange}>
                  <option value="">Select menu</option>
                  {optionTemplate}
                </select>
              </div>
            </div>
          );
        }
        return (
          <React.Fragment>
            <div className="container-fluid admin-body">
              <div className="admin-row">
                <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
                </div>
                <div className="col-md-10 col-sm-12 content">
                  <div className="row content-row">
                    <div className="col-md-12 header">
                      <Adminheader  props={this.props} />
                    </div>

                    <div className="col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4" >  
                      <React.Fragment>
                        <div style={{ display: this.state.spinner === true ? 'flex justify-content-center ' : 'none' }} className="overlay text-center">
                              <ReactSpinner type="border" color="primary" size="10" />
                        </div>
                      </React.Fragment>
                      <React.Fragment>     
                        <div className="cms-footer-content-wrap p-4">
                        <div className="row">
                        <div className="col-md-12">                     
                          <div className="card">
                            <div className="card-header">
                              <strong>Footer section options</strong>
                            </div>
                            <div className="card-body  manage-footer">
                              <div className="form-group">
                                <strong>Main subfooter settings</strong>
                              </div>
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  <label>Columns number</label>
                                </div>
                                <div className="col-sm-8">
                                  <div className="col-num-radio-wrap" onChange={this.setColNumber.bind(this)} >
                                    <input type="radio" value="1" name="col_name" checked={this.state.data.col_number === "1"} /> <span>One</span>
                                    <input type="radio" value="2" name="col_name" checked={this.state.data.col_number === "2"} /> <span>Two</span> 
                                    <input type="radio" value="3" name="col_name" checked={this.state.data.col_number === "3"} /> <span>Three</span>
                                    <input type="radio" value="4" name="col_name" checked={this.state.data.col_number === "4"} /> <span>Four</span>
                                    <input type="radio" value="5" name="col_name" checked={this.state.data.col_number === "5"} /> <span>Five</span>
                                                                        
                                  </div>
                                </div>
                              </div>

                              <div className="col-num-content-wrap">
                                {singleElements}
                              </div>

                              <div className="form-group">
                                <strong>Main Socket settings</strong>
                              </div>                             
                              <div className="col-num-content-wrap">
                                <div className="form-group row col-content-single">
                                  <div className="col-sm-4 ">
                                      <label>First column content</label>
                                  </div>
                                  <div className="col-sm-8 ">                                    
                                      <CKEditor
                                        data={this.state.socket1_content}
                                        config={ {
                                          extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                          allowedContent: true,
                                          filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                        } }
                                        onInit = { 
                                          editor => {        
                                          } 
                                        }
                                        onChange={this.onEditorChange1}
                                      />                                     
                                  </div>
                                </div>

                                <div className="form-group row col-content-single">
                                  <div className="col-sm-4 ">
                                      <label>Second column content</label>
                                  </div>
                                  <div className="col-sm-8 ">
                                   
                                      <CKEditor
                                        data={this.state.socket2_content}
                                        config={ {
                                          extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                          allowedContent: true,
                                          filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                        } }
                                        onInit = { 
                                          editor => {        
                                          } 
                                        }
                                        onChange={this.onEditorChange2}
                                      />                                    
                                  </div>
                                </div>
                                <div className="form-group row col-content-single">
                                  <div className="col-sm-4 ">
                                      <label>Third column content</label>
                                  </div>
                                  <div className="col-sm-8 ">                          
                                      <CKEditor
                                        data={this.state.socket3_content}
                                        config={ {
                                          extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                          allowedContent: true,
                                          filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                        } }
                                        onInit = { 
                                          editor => {        
                                          } 
                                        }
                                        onChange={this.onEditorChange3}
                                      />
                                     
                                  </div>
                                </div>
                              </div>

                              <button
                              disabled={this.state.submit_status === true ? "disabled" : false}
                              onClick={this.handleSubmit} className="btn btn-info">
                              {this.state.submit_status ? (
                                    <ReactSpinner type="border" color="dark" size="1" />
                                ) : (
                                    ""
                                )}
                              Save Changes
                              </button>
  
                              {this.state.message !== "" ? (
                            <p className="tableinformation">
                            <div className={this.state.message_type}>
                            <p>{this.state.message}</p>
                            </div>
                            </p>
                            ) : null
                          } 
                            </div>
                          </div>
                        </div>
                        </div>
                        </div>
                        
                        </React.Fragment>     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        );
      }
    }
    export default ManageFooter;
    