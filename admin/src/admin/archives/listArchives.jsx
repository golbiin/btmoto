import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import DeleteConfirm from "../common/deleteConfirm";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import * as settingsService from "../../ApiServices/admin/settings";
import ReactSpinner from "react-bootstrap-spinner";
import memoize from "memoize-one";
import TrashConfirm from "../common/trashConfirm";

class ListArchives extends Component {
  state = {
    confirm: {
      name: "Delete",
      message: "Are you sure you want to delete this page? ",
      action: "delete"
    },
    typeFilterValue: "",
    dateFilterValue: "",
    dateFilterOptions: [],
    typeFilterOptions: [],
    search: "",
    modal_trash: false,
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    spinner: true,
    contextActions_select: "-1",
    activetab: "tab_a",
    columns: [
      {
        name: "Title",
        selector: "title",
        sortable: true,
        cell: row => (
          <div>
            <Link to={"/admin/archives/edit/" + row._id}>{row.title}</Link>
          </div>
        )
      },
      {
        name: "Categories",
        selector: "related_cat",
        sortable: true,
        cell: row => (
          <div>
            <Link to={"/admin/archives/edit/" + row._id}>
              {row.related_cat}
            </Link>
          </div>
        )
      },

      {
        name: "",
        cell: row => (
          <div className="action_dropdown" data-id={row._id}>
            <i
              data-id={row._id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            />
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >
              <Link
                to={"/admin/archives/edit/" + row._id}
                className="dropdown-item"
              >
                Edit Page
              </Link>
              <Link
                onClick={() => this.saveAndtoogle(row._id, "delete")}
                className="dropdown-item"
              >
                Delete
              </Link>

              {row.page_status != 0 ? (
                <Link
                  onClick={() => this.saveAndtoogletrash(row._id, "trash")}
                  className="dropdown-item"
                >
                  Trash
                </Link>
              ) : (
                ""
              )}
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px" // custom width for icon button
      }
    ]
  };

  componentDidMount = () => {
    //   this.getDateFilterOptions();
    this.getTypeFilterOptions();
    this.getAllArchives();
  };
  DeletePageId = id => {
    const data_table = this.state.data_table.filter(i => i.id !== id);
    this.setState({ data_table });
  };
  getDateFilterOptions = async () => {
    //  this.setState({ spinner: true });
    try {
      const response = await settingsService.getArchiveDateFilterOptions();
      console.log(431, response);
      if (response.data.status === 1) {
        this.setState({
          dateFilterOptions: response.data.options[0].date
        });
        // this.setState({ spinner: false });
      } else {
        this.setState({ dateFilterOptions: [] });
      }
    } catch (err) {
      // this.setState({ spinner: false });
    }
  };

  /* Get all types*/
  getTypeFilterOptions = async () => {
    //this.setState({ spinner: true });
    try {
      const response = await settingsService.getArchiveTypeFilterOptions();
      console.log(43, response);
      if (response.data.status === 1) {
        if (response.data.options != null) {
          this.setState({ typeFilterOptions: response.data.options });
        }
        //  this.setState({ spinner: false });
      } else {
        this.setState({ typeFilterOptions: [] });
      }
    } catch (err) {
      // this.setState({ spinner: false });
    }
  };

  /*  Get All Archives  */
  getAllArchives = async () => {
    this.setState({ spinner: true });
    // this.setState({processing})
    try {
      const filter = {
        date: this.state.dateFilterValue,
        type: this.state.typeFilterValue
      };
      const response = await settingsService.getAllArchives(filter);
      const data_table_row = [];
      const data_table_rowTrash = [];
      const data_table_rowDraft = [];

      if (response.data.status === 1) {
        response.data.data.map((news, index) => {
          console.log("news", news);
          if (news.page_status == 0) {
            const SetdataTrash = {};
            SetdataTrash.title = news.title;
            SetdataTrash.slug = news.slug;
            SetdataTrash._id = news._id;
            SetdataTrash.releasedate = news.releasedate;
            SetdataTrash.description = news.description;
            SetdataTrash.related_cat = news.related_cat;
            SetdataTrash.page_status = news.page_status;
            SetdataTrash.file_url = news.file_url;
            SetdataTrash.version = news.version;
            data_table_rowTrash.push(SetdataTrash);
          } else if (news.page_status == 1) {
            const Setdata = {};
            Setdata.title = news.title;
            Setdata.slug = news.slug;
            Setdata._id = news._id;
            Setdata.parent_id = news.parent_id;
            Setdata.description = news.description;
            Setdata.related_cat = news.related_cat;
            Setdata.page_status = news.page_status;
            Setdata.file_url = news.file_url;
            Setdata.version = news.version;
            Setdata.releasedate = news.releasedate;
            data_table_row.push(Setdata);
          } else if (news.page_status == 2) {
            const SetdataDraft = {};
            SetdataDraft.title = news.title;
            SetdataDraft.slug = news.slug;
            SetdataDraft._id = news._id;
            SetdataDraft.parent_id = news.parent_id;
            SetdataDraft.description = news.description;
            SetdataDraft.related_cat = news.related_cat;
            SetdataDraft.page_status = news.page_status;
            SetdataDraft.file_url = news.file_url;
            SetdataDraft.version = news.version;
            SetdataDraft.releasedate = news.releasedate;
            data_table_rowDraft.push(SetdataDraft);
          }
        });
        this.setState({
          data_table: data_table_row,
          dataTrash: data_table_rowTrash,
          dataDraft: data_table_rowDraft
        });
        this.setState({ spinner: false });
      }
      this.setState({
        data_table: data_table_row,
        dataTrash: data_table_rowTrash,
        dataDraft: data_table_rowDraft
      });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  searchSpace = event => {
    let keyword = event.target.value;
    this.setState({ search: keyword });
  };

  doAction = action => {
    switch (action) {
      case "trash":
        this.trashArchives();
        break;
      case "delete":
        this.deleteArchives();
        break;
      default:
    }
  };

  trashArchives = async () => {
    let id = this.state.index;
    const response = await settingsService.trashArchives(id);
    const data_table = this.state.data_table.filter(i => i._id !== id);
    this.setState({ data_table });
    if (response) {
      if (response.data.status == 1) {
        this.getAllArchives();
        this.toogleTrash();
        // setTimeout(() => {
        //   this.setState({
        //    // message: response.data.message,
        //   //  message_type: 'success'
        //   })

        // }, 2000);
      } else {
        this.setState({
          //  message: response.data.message,
          // message_type: 'error'
        });
      }
    }
  };
  /* Delete Archives */
  deleteArchives = async () => {
    let id = this.state.index;
    const response = await settingsService.deleteArchives(id);
    if (response) {
      if (response.data.status == 1) {
        console.log(3344, response.data.message);
        const data_table = this.state.data_table.filter(i => i._id !== id);
        const dataTrash = this.state.dataTrash.filter(i => i._id !== id);
        const dataDraft = this.state.dataDraft.filter(i => i._id !== id);
        this.setState({
          data_table: data_table,
          dataTrash: dataTrash,
          dataDraft: dataDraft
        });
        this.toogle();

        // setTimeout(() => {
        // //  this.setState(() => ({ message:  response.data.message,  message_type: 'success'}))
        // }, 1000);
      } else {
        // this.setState({
        //   message: response.data.message,
        //   message_type: 'error'
        // })
      }
    }
  };
  saveAndtoogletrash = id => {
    this.setState({ index: id });
    this.toogleTrash();
  };
  toogleTrash = () => {
    let status = !this.state.modal_trash;
    this.setState({ modal_trash: status });
  };
  saveAndtoogle = (id, action) => {
    switch (action) {
      case "trash":
        this.setState({
          confirm: {
            name: "Trash",
            message: "Are you sure you want to move the item to trash? ",
            action: action
          }
        });
        break;
      case "delete":
        this.setState({
          confirm: {
            name: "Delete",
            message: "Are you sure you'd like to delete this item?",
            action: action
          }
        });
        break;
      default:
        this.setState({
          confirm: {
            name: "Delete",
            message: "Are you sure you'd like to delete this item?",
            action: action
          }
        });
    }
    this.setState({ index: id });
    this.toogle();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };
  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows });
  };
  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value });
  };

  actionHandler = e => {
    this.setState({
      message: "",
      message_type: ""
    });
    const { selectedRows } = this.state;
    const archiveids = selectedRows.map(r => r._id);
    let status = "";
    let actionvalue = this.state.contextActions_select;

    if (actionvalue === "-1") {
      return;
    } else if (actionvalue === "0") {
      status = "Trash";
    } else if (actionvalue === "1") {
      status = "Published";
    } else if (actionvalue === "2") {
      status = "Draft";
    } else if (actionvalue === "3") {
      status = "Delete Trash";
    }
    if (window.confirm(`Are you sure you want to move ` + status)) {
      this.setState({ toggleCleared: !this.state.toggleCleared });
      this.actionHandlerArchives(archiveids, actionvalue, status);
    }
  };

  actionHandlerArchives = async (archiveids, contextActions, status) => {
    try {
      const response = await settingsService.actionHandlerArchives(
        archiveids,
        contextActions,
        status
      );
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success"
        });

        this.getAllArchives();
      } else {
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
        this.getAllArchives();
      }
    } catch (err) {
      this.setState({
        message: "Something went wrong",
        message_type: "error"
      });
    }
  };
  handleSearchByDate = event => {
    let keyword = event.target.value;
    this.setState({ dateFilterValue: keyword });
    this.setState(
      {
        dateFilterValue: keyword
      },
      () => {
        this.getAllArchives();
      }
    );
  };
  handleSearchByType = event => {
    let keyword = event.target.value;
    this.setState({ typeFilterValue: keyword });
    this.setState(
      {
        typeFilterValue: keyword
      },
      () => {
        this.getAllArchives();
      }
    );
  };

  tabActive = async tab => {
    this.setState({
      message: "",
      message_type: ""
    });
    this.setState({ activetab: tab });
  };

  render() {
    let rowData = this.state.data_table;
    let search = this.state.search;
    let rowDataTrash = this.state.dataTrash;
    let rowDataDraft = this.state.dataDraft;
    const contextActions = memoize(actionHandler => (
      <div className="common_action_section">
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value="-1">Select</option>
          {this.state.activetab !== "tab_a" ? (
            <option value="1">Published</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_b" ? (
            <option value="0">Trash</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_c" ? (
            <option value="2">Draft</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_a" &&
          this.state.activetab !== "tab_c" ? (
            <option value="3">Delete Trash</option>
          ) : (
            ""
          )}
        </select>
        <button className="danger" onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ));

    let dateFilterOptionHtml = this.state.dateFilterOptions.map(dateObj => (
      <option
        key={dateObj.month + "/" + dateObj.year}
        value={dateObj.month + "/" + dateObj.year}
      >
        {dateObj.month + "/" + dateObj.year}
      </option>
    ));

    // let typeFilterOptionHtml = this.state.typeFilterOptions.map(typeObj => (
    //   <option key={typeObj._id} value={typeObj._id}>
    //     {typeObj._id}
    //   </option>
    // ));

    let typeFilterOptionHtml = this.state.typeFilterOptions.map(typeObj => {
      if (typeObj._id !== "")
        return (
          <option key={typeObj._id} value={typeObj._id}>
            {typeObj._id}
          </option>
        );
    });

    if (search.length > 0) {
      search = search.trim().toLowerCase();
      rowData = rowData.filter(l => {
        return l.title.toLowerCase().match(search);
      });
    }

    return (
      <React.Fragment>
        <div className="container-fluid admin-body archive_sec">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4">
                  <div className="main_admin_page_common">
                    <div className="">
                      <div className="admin_breadcum">
                        <div className="row">
                          {/* <div className='col-md-1'>
                            <p className='page-title'>Archives</p>
                          </div> */}
                          <div className="col-md-3">
                            <Link
                              className="add_new_btn archive_btn"
                              to="/admin/archives/add"
                            >
                              Add New
                            </Link>
                          </div>
                          <div className="col-md-3">
                            <div className="bulkaction">
                              <div className="commonserachform">
                                <select
                                  className="form-control"
                                  name="date_filter"
                                  onChange={e => this.handleSearchByType(e)}
                                >
                                  <option value="">All Types</option>
                                  {typeFilterOptionHtml}
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            {/* <div className="bulkaction">
                              <div className="commonserachform">
                                <select
                                  className="form-control"
                                  name="date_filter"
                                  onChange={e => this.handleSearchByDate(e)}
                                >
                                  <option value="">All Dates</option>
                                {dateFilterOptionHtml}
                                </select>
                              </div>
                            </div> */}
                          </div>

                          <div className="col-md-3">
                            <div className="searchbox">
                              <div className="commonserachform">
                                <span />
                                <input
                                  type="text"
                                  placeholder="Search"
                                  onChange={e => this.searchSpace(e)}
                                  name="search"
                                  className="search form-control"
                                />
                                <input type="submit" className="submit_form" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      {this.state.message !== "" ? (
                        <p className="tableinformation">
                          <div className={this.state.message_type}>
                            <p>{this.state.message}</p>
                          </div>
                        </p>
                      ) : null}
                      <div className="faq-list-table-wrapper">
                        {this.state.spinner === true ? (
                          <React.Fragment>
                            <div
                              style={{
                                display:
                                  this.state.spinner === true
                                    ? "flex justify-content-center "
                                    : "none"
                              }}
                              className="overlay text-center"
                            >
                              <ReactSpinner
                                type="border"
                                color="primary"
                                size="10"
                              />
                            </div>
                          </React.Fragment>
                        ) : (
                          <React.Fragment>
                            <ul className="nav nav-pills nav-fill">
                              <li
                                className={
                                  this.state.activetab === "tab_a"
                                    ? "active"
                                    : ""
                                }
                              >
                                <a
                                  href="#tab_a"
                                  data-toggle="pill"
                                  onClick={() => this.tabActive("tab_a")}
                                >
                                  All ({this.state.data_table.length})
                                </a>
                              </li>

                              <li
                                className={
                                  this.state.activetab === "tab_b"
                                    ? "active"
                                    : ""
                                }
                              >
                                <a
                                  href="#tab_b"
                                  data-toggle="pill"
                                  onClick={() => this.tabActive("tab_b")}
                                >
                                  Trash ({this.state.dataTrash.length})
                                </a>
                              </li>

                              <li
                                className={
                                  this.state.activetab === "tab_c"
                                    ? "active"
                                    : ""
                                }
                              >
                                <a
                                  href="#tab_c"
                                  data-toggle="pill"
                                  onClick={() => this.tabActive("tab_c")}
                                >
                                  Draft ({this.state.dataDraft.length})
                                </a>
                              </li>
                            </ul>

                            <div className="tab-content">
                              <div className="tab-pane active" id="tab_a">
                                <DataTable
                                  columns={this.state.columns}
                                  data={rowData}
                                  selectableRows
                                  highlightOnHover
                                  pagination
                                  selectableRowsVisibleOnly
                                  clearSelectedRows={this.state.toggleCleared}
                                  onSelectedRowsChange={this.handlerowChange}
                                  contextActions={contextActions(
                                    this.deleteAll
                                  )}
                                  noDataComponent = {<p>There are currently no records.</p>}
                                />
                              </div>
                              <div className="tab-pane" id="tab_b">
                                <DataTable
                                  columns={this.state.columns}
                                  data={rowDataTrash}
                                  selectableRows
                                  highlightOnHover
                                  pagination
                                  selectableRowsVisibleOnly
                                  clearSelectedRows={this.state.toggleCleared}
                                  onSelectedRowsChange={this.handlerowChange}
                                  contextActions={contextActions(
                                    this.deleteAll
                                  )}
                                  noDataComponent = {<p>There are currently no records.</p>}
                                />
                              </div>
                              <div className="tab-pane" id="tab_c">
                                <DataTable
                                  columns={this.state.columns}
                                  data={rowDataDraft}
                                  selectableRows
                                  highlightOnHover
                                  pagination
                                  selectableRowsVisibleOnly
                                  clearSelectedRows={this.state.toggleCleared}
                                  onSelectedRowsChange={this.handlerowChange}
                                  contextActions={contextActions(
                                    this.deleteAll
                                  )}
                                  noDataComponent = {<p>There are currently no records.</p>}
                                />
                              </div>
                            </div>
                          </React.Fragment>
                        )}
                      </div>{" "}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          name={this.state.confirm.name}
          message={this.state.confirm.message}
          action={this.state.confirm.action}
          deleteUser={this.doAction}
          toogle={this.toogle}
        />
        <TrashConfirm
          modal={this.state.modal_trash}
          toogle={this.toogleTrash}
          trash_action={this.trashArchives}
        />
      </React.Fragment>
    );
  }
}

export default ListArchives;
