import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import DeleteConfirm from "../common/deleteConfirm";
import DataTable from 'react-data-table-component';
import { Link } from "react-router-dom";
import * as contactService from "../../ApiServices/admin/manageContact";
import ReactSpinner from "react-bootstrap-spinner";

class AdminManageSubscribers extends Component {
    state = { 
        search: '',
        data_table : [],
        spinner : true,
        columns : [
          {
            name: 'Full Name',
            selector: 'fullname',
            sortable: true,
            cell: row => <div>{row.fullname}</div>,
          },
          {
            name: 'Country',
            selector: 'country',
            left: true,
            hide: 'sm',
            width: 300,
          },          
          {
            name: 'Phone',
            selector: 'phone',
            sortable: true,
            left: true,
            hide: 'sm'
          },

          {
            name: 'Email',
            selector: 'email',
            sortable: true,
            left: true,
            hide: 'sm'
          },
          {
            name: 'Comments',
            selector: 'comments',
            sortable: true,
            left: true,
            hide: 'sm'
          },        
          {
            name: '',
            cell:  row =><div className="action_dropdown" data-id={row._id}><i data-id={row._id} className="dropdown_dots"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
            <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
            <Link
              onClick={() =>
                this.saveAndtoogle(row._id)
              } className="dropdown-item"
            >
              Delete
            </Link>
            </div>
            </div>,
            allowOverflow: false,
            button: true,
            width: '56px', // custom width for icon button
        },
        ]

    };


    componentDidMount = () => {
        this.getAllSubcriptions();
    };

    DeletePageId = id => {
      
      const data_table = this.state.data_table.filter(i => i.id !== id);
      this.setState({data_table})
    };

    getAllSubcriptions = async () => {
      this.setState({ spinner: true});
        try {
          const response =  await contactService.getAllSubcriptions();
            if (response.data.status === 1) {              
              const data_table_row = [];
              response.data.data.map((news,index) => {
              const Setdata = {};
              Setdata.fullname =  news.fullname;
              Setdata.country =  news.country;
              Setdata._id =  news._id;
              Setdata.email = news.email;
              Setdata.comments = news.comments;
              Setdata.phone = news.phone;
              data_table_row.push(Setdata);
              });
              this.setState({ 
              data_table: data_table_row
              });
              
            } 
          this.setState({ spinner: false});
        } catch (err) {
          this.setState({ spinner: false});
        }
      } 

    searchSpace=(event)=>{
        let keyword = event.target.value;
        this.setState({search:keyword})
    }

    deleteSubscription = async () => {
      let id = this.state.index;
      const response = await contactService.deleteSubcription(id);
      if (response) {
        if (response.data.status === 1) {
          const data_table = this.state.data_table.filter(i => i._id !== id);
          this.setState({data_table})
          this.toogle();
        }
      }
    };

    saveAndtoogle = (id) => {
      this.setState({ index: id });
      this.toogle();
    };
    toogle = () => {
      let status = !this.state.modal;
      this.setState({ modal: status });
    };
   
    render() { 
        let rowData =  this.state.data_table;
        let search = this.state.search;
        if(search.length > 0){
           search = search.trim().toLowerCase();
           rowData = rowData.filter(l => {
               return (l.fullname.toLowerCase().match( search ));
           });
       }

        return ( <React.Fragment>
            <div className="container-fluid admin-body ">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
            <div className="row content-row">
                <div className="col-md-12  header">
                <Adminheader  props={this.props} />
                </div>
                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4" >   
                <div className="main_admin_page_common">
                <div className="">
                <div className="admin_breadcum">
                    <div className="row">
                    <div className="col-md-1"><p className="page-title">Subscriptions</p></div>
                    <div className="col-md-6">
                    </div>
                    <div className="col-md-5">
                    <div className="searchbox">
                        <div className="commonserachform">
                        <span></span>
                        <input
                        type="text"
                        placeholder="Search" onChange={(e)=>this.searchSpace(e)} 
                        name="search"
                        className="search form-control"
                        />
                        <input type="submit" className="submit_form"/>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
                <div className="faq-list-table-wrapper">
                <React.Fragment>
                  <div style={{ display: this.state.spinner === true ? 'flex justify-content-center ' : 'none' }} className="overlay text-center">
                        <ReactSpinner type="border" color="primary" size="10" />
                  </div>
                </React.Fragment>
                <DataTable
                columns={this.state.columns}
                data={rowData}
                selectableRows
                highlightOnHover
                pagination
                selectableRowsVisibleOnly 
                noDataComponent = {<p>There are currently no records.</p>}
                />
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>                
            </div>
            </div>
            <DeleteConfirm
              modal={this.state.modal}
              toogle={this.toogle}
              deleteUser={this.deleteSubscription}
            />
            </React.Fragment> );
    }
}
 
export default AdminManageSubscribers;