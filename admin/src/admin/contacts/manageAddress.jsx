import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as contactService from "../../ApiServices/admin/manageContact";
import DataTable from 'react-data-table-component';

class AdminManageAddress extends Component {
    state = {
      id:"",
      name: "",
      address1: "",
      address2: "",
      phone: "",
      email: "",
      latitude: "",
      longitude: "",
      attributes: [],
      errors: {},
      submit_status: false,
      message: "",
      message_type: "",
      table_message: "",
      table_message_type: "",
      addresses: [],
      delete_status: false,
      edit_status: false,
      data_table : [],
      spinner : true,
      columns : [
        {
          name: 'Name',
          selector: 'name',
          sortable: true,
        },
        {
          name: 'Address 1',
          selector: 'address1',
          sortable: true,
        },
      {
        name: 'Action',
        cell:  row =><div className="action_dropdown" data-id={row._id}><i data-id={row._id} className="dropdown_dots"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
        <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
          <a  onClick={ () =>this.getContactById(row._id) } className="dropdown-item">
                Edit
          </a>
          <a  onClick={ () =>this.handleRemoveContact(row._id) } className="dropdown-item">
                Delete
          </a>
        </div>
        </div>,
        allowOverflow: false,
        button: true,
        width: '56px', // custom width for icon button
    },
    ]
    };

    componentDidMount = () => {
      this.getAllAddress();
    };
    getAllAddress = async () => {
      this.setState({ spinner: true});
        try {
          const response =  await contactService.getAllContacts();
          if (response.data.status == 1) {
            this.setState({ addresses: response.data.data});
            const Setdata = { ...this.state.data_table };
            const data_table_row = [];
            response.data.data.map((add,index) => {
              const Setdata = {};
              Setdata.name =  add.name;
              Setdata.address1 =  add.address1;
              Setdata.address2 =  add.address2;
              Setdata._id =  add._id;
              Setdata.phone = add.phone;
              Setdata.email = add.email;
              Setdata.latitude = add.latitude;
              Setdata.longitude = add.longitude;
              data_table_row.push(Setdata);
            });
            this.setState({ 
            data_table: data_table_row
            });
            
          } 
          this.setState({ spinner: false});
        } catch (err) {
            this.setState({ spinner: false});
        }
    }

    schema = {
      name: Joi.string().required().error(() => {
        return {
          message: 'This field is required.',
        };
      }),
      address1: Joi.string().required().error(() => {
        return {
          message: 'This field is required.',
        };
      }),
      address2: Joi.string().required().error(() => {
        return {
          message: 'This field is required.',
        };
      }),
      phone: Joi.string(),
      email: Joi.string().email(),
      latitude: Joi.string(),
      longitude: Joi.string(),
    };

    validateProperty = (name, value) => {
      const obj = { [name]: value };
      const schema = { [name]: this.schema[name] };
      const { error } = Joi.validate(obj, schema);
      return error ? error.details[0].message : null;
    };

    handleChange = (input) => event => {
      const errors = { ...this.state.errors };
      delete errors.validate;
      const errorMessage = this.validateProperty(input, event.target.value);
      if (errorMessage) errors[input] = errorMessage;
      else delete errors[input];
      this.setState({ [input]: event.target.value, errors });
    }
  
    getContactById = async (id) => {
      const response =  await contactService.getContactById({id: id});
      if (response.data.status == 1) {
          this.setState({ 
              edit_status: true,
              submit_status: false,
              message: "",
              message_type: "",
              delete_status: false,
              name: response.data.data.name,
              address1: response.data.data.address1,
              address2: response.data.data.address2,
              email : response.data.data.email,
              phone : response.data.data.phone,
              latitude : response.data.data.latitude,
              longitude : response.data.data.longitude,
              id : response.data.data._id,
          });
      } 
    }
    
    addContact = () => {
      this.setState({ 
        name: "",
        address1: "",
        address2: "",
        phone: "",
        email: "",
        latitude: "",
        longitude: "",
        attributes: [],
        errors: {},
        submit_status: false,
        message: "",
        message_type: "",
        delete_status: false,
        edit_status: false,
        descrption: ""
      })
    }
    saveContact = async (e) => {
      
      e.preventDefault()
      const checkState = {  
            name: this.state.name,
            address1: this.state.address1,
            address2: this.state.address2,
       };
      const errors = { ...this.state.errors };
      let result = Joi.validate(checkState, this.schema);
      if (result.error) {
          let path = result.error.details[0].path[0];
          let errormessage = result.error.details[0].message;
          errors[path] = errormessage;
          this.setState({ errors });
      } else {
        const category_data = {
          name: this.state.name,
          address1: this.state.address1,
          address2: this.state.address2,
          email : this.state.email,
          phone : this.state.phone,
          latitude : this.state.latitude,
          longitude : this.state.longitude,
          id : this.state.id,
        }
        try {
          this.setState({ 
            submit_status: true,
            message: "",
            message_type: "" 
          });
        if(this.state.edit_status === true){
          const response = await contactService.updateContact(category_data);
          if (response) {
            this.setState({ submit_status: false });
             if(response.data.status === 1){
              this.setState({
                message: response.data.message,
                message_type: "success",
                name: "",
                address1: "",
                address2: "",
                email: "",
                phone: "",
                latitude: "",
                longitude: "",
                id: "",
                edit_status: false
              });
              this.getAllAddress();
             } else {
              this.setState({
                message: response.data.message,
                message_type: "error",
              });
             }
          } else {
            this.setState({ submit_status: false });
            this.setState({
              message: "Something went wrong! Please try after some time",
              message_type: "error",
            });
          }

        }else{
          const response = await contactService.createContact(category_data);
          if (response) {
            this.setState({ submit_status: false });
             if(response.data.status === 1){
              this.setState({
                message: response.data.message,
                message_type: "success",
                name: "",
                address1: "",
                address2: "",
                email: "",
                phone: "",
                latitude: "",
                longitude: ""
              });
              this.getAllAddress();
             } else {
              this.setState({
                message: response.data.message,
                message_type: "error",
              });
             }
          } else {
            this.setState({ submit_status: false });
            this.setState({
              message: "Something went wrong! Please try after some time",
              message_type: "error",
            });
          }
          setTimeout(() => { 
            this.setState(() => ({message: ''}))
          }, 5000);
        }
      
        } catch (err) {
          
        }
     }      
    }
    handleRemoveContact = async (id ) => {
      this.setState({ 
        delete_status: true,
      });
      const response =  await contactService.removeContact({ _id: id });
      if (response.data.status === 1) {
          let newContacts = [...this.state.data_table];
          newContacts = newContacts.filter(function( obj ) {
               return obj._id !== id;
          });
          this.setState({
            table_message: response.data.message,
            table_message_type: "sucess",
          });

          this.setState({ data_table :newContacts, delete_status: false, edit_status: false });
          setTimeout(() => { 
            this.setState(() => ({table_message: ''}))
          }, 5000);
      } else {
        this.setState({
          table_message: response.data.message,
          table_message_type: "sucess",
        });
        setTimeout(() => { 
          this.setState(() => ({table_message: ''}))
        }, 5000);
      }
    };

    render() {
        
        let datarow =  this.state.data_table;
        return (
          <React.Fragment>
            <div className="container-fluid admin-body">
              <div className="admin-row">
                <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
                </div>
                <div className="col-md-10 col-sm-12 content">
                  <div className="row content-row">
                    <div className="col-md-12 header">
                      <Adminheader  props={this.props} />
                    </div>
                    <div className="col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4" >  
                      <React.Fragment>  
                        <div className="categories-wrap p-4">
                          <div className="row">
                            <div className="col-md-4">
                              <div className="card">
                                <div className="card-header">
                                  <strong>{this.state.edit_status === true ? 'Edit' : 'Add'} Address</strong>
                                  {this.state.edit_status === true ? 
                                  <button  onClick={this.addContact}
                                   className="ml-4 add-cat-btn btn btn-success">+ Add New</button> : ''}
                                </div>
                                <div className="card-body categories-add">
                                  <div className="form-group">
                                    <input
                                    className="form-control"
                                    placeholder="Name*"
                                    type="text"
                                    value={this.state.name}
                                    onChange={this.handleChange('name')}
                                    />
                                    {this.state.errors.name ? (<div className="danger">{this.state.errors.name}</div>) : ( "")}
                                  </div>
                                  <div className="form-group">
                                    <textarea  onChange={this.handleChange('address1')}  
                                    placeholder="Address 1*"
                                    value={this.state.address1}    className="form-control"></textarea>
                                    {this.state.errors.address1 ? (<div className="danger">{this.state.errors.address1}</div>) : ( "")}
                                  </div>
                                  <div className="form-group">
                                    <textarea  onChange={this.handleChange('address2')}
                                    placeholder="Address 2*" 
                                    value={this.state.address2}   className="form-control"></textarea>
                                    {this.state.errors.address2 ? (<div className="danger">{this.state.errors.address2}</div>) : ( "")}
                                  </div>
                                  <div className="form-group">
                                    <input
                                    className="form-control"
                                    placeholder="Phone"
                                    type="text"
                                    value={this.state.phone}
                                    onChange={this.handleChange('phone')}
                                    />
                                    {this.state.errors.phone ? (<div className="danger">{this.state.errors.phone}</div>) : ( "")}
                                  </div>
                                  <div className="form-group">
                                    <input
                                    className="form-control"
                                    placeholder="Email"
                                    type="text"
                                    value={this.state.email}
                                    onChange={this.handleChange('email')}
                                    />
                                    {this.state.errors.email ? (<div className="danger">{this.state.errors.email}</div>) : ( "")}
                                  </div>
                                  <div className="form-group">
                                    <input
                                    className="form-control"
                                    placeholder="Latitude"
                                    type="text"
                                    value={this.state.latitude}
                                    onChange={this.handleChange('latitude')}
                                    />
                                    {this.state.errors.latitude ? (<div className="danger">{this.state.errors.latitude}</div>) : ( "")}
                                  </div>
                                  <div className="form-group">
                                    <input
                                    className="form-control"
                                    placeholder="Longitude"
                                    type="text"
                                    value={this.state.longitude}
                                    onChange={this.handleChange('longitude')}
                                    />
                                    {this.state.errors.longitude ? (<div className="danger">{this.state.errors.longitude}</div>) : ( "")}
                                  </div>
                                  <button 
                                    disabled={this.state.submit_status === true ? "disabled" : false}
                                    onClick={this.saveContact}
                                    className="save-btn btn btn-dark">
                                    {
                                      this.state.submit_status === true ? (
                                        <React.Fragment>
                                        <ReactSpinner type="border" color="primary" size="1" />
                                        <span className="submitting">
                                        {this.state.edit_status === true ? 'UPDATING ADDRESS...' : 'CREATING ADDRESS...'}
                                         </span>
                                        </React.Fragment>
                                    ) : (
                                      this.state.edit_status === true ? 'UPDATE ADDRESS' : 'CREATE ADDRESS'
                                    )
                                    }
                                  </button>
                                  {this.state.message !== "" ? (
                                <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                                </div>
                                ) : null}
                                </div>                                
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="card">
                              <div className="card-header">
                                <strong>Cimon Address</strong>
                              </div>
                              <div className="card-body categories-list">
                              <React.Fragment>
                              <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                                    <ReactSpinner type="border" color="primary" size="10" />
                              </div>
                            </React.Fragment>
                              <p className="tableinformation">
                              {this.state.table_message !== "" ? (
                                <div className={this.state.table_message_type}>
                                <p>{this.state.table_message}</p>
                                </div>
                                ) : null
                              }                                
                              </p>
                              <DataTable 
                                  columns={this.state.columns}
                                  data={datarow}                                  
                                  highlightOnHover
                                  pagination
                                  selectableRowsVisibleOnly
                                  noDataComponent = {<p>There are currently no records.</p>}
                              />                             
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>                        
                        </React.Fragment>     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        );
      }
    }
    
    export default AdminManageAddress;
    