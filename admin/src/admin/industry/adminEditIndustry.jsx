import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import * as Industry from "../../ApiServices/admin/industry";
import * as manageUser from "../../ApiServices/admin/manageUser";
import CKEditor from "ckeditor4-react";
import { apiUrl ,siteUrl } from "../../../src/config.json";
import BlockUi from 'react-block-ui';
class AdminEditIndustry extends Component {
  constructor(props) {
    super(props);
    this.handleCheck = this.handleCheck.bind(this);

    this.handleEditorChange = this.handleEditorChange.bind(this);
    this.onEditorChange = this.onEditorChange.bind(this);
    this.onRelatedContentChange = this.onRelatedContentChange.bind(this);
  }
  state = {
    spinner: false,
    checkval: "",
    profile_image: "",
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    upload_data: "",
    cimonIndustry: [],
    validated: false,
    industryStatus: true,
    focused: false
  };

  onEditorChange(evt) {
    const cimonIndustry = { ...this.state.cimonIndustry };
    cimonIndustry["content"] = evt.editor.getData();
    this.setState({
      cimonIndustry: cimonIndustry
    });
  }

  onRelatedContentChange(evt) {
    const cimonIndustry = { ...this.state.cimonIndustry };
    cimonIndustry["relatedcontent"] = evt.editor.getData();
    this.setState({
      cimonIndustry: cimonIndustry
    });
  }

  handleEditorChange(changeEvent) {
    const cimonIndustry = { ...this.state.cimonIndustry };
    cimonIndustry["content"] = changeEvent.target.value;
    this.setState({
      cimonIndustry: cimonIndustry
    });
  }

  onclassicEdit = async (value, item) => {
    if (item === "editorContent") {
      const cimonIndustry = { ...this.state.cimonIndustry };
      cimonIndustry["content"] = value.getData();
      this.setState({
        cimonIndustry: cimonIndustry
      });
    }
  };

  /* Checkbox on Change */
  handleCheck(e) {
    this.setState({ checkval: e.target.checked });
  }

  /*Joi validation schema*/

  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    slug: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    description: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    _id: Joi.optional().label("id"),
    profile_image: Joi.allow(null),
    content: Joi.allow(null),
    relatedcontent: Joi.allow(null)
  };

  /* Input Handle Change */
  handleChange = (event, type = null) => {
    let cimonIndustry = { ...this.state.cimonIndustry };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateIndustrydata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    cimonIndustry[name] = value;
    this.setState({ cimonIndustry, errors });
  };
  validateIndustrydata = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Get single Industry */
  componentDidMount = async () => {
    this.setState({spinner:true});
    const id = this.props.match.params.id;
    const response = await Industry.getSingleIndustry(id);
    if (response.data.status == 1) {
      let newIndustryarray = {
        _id: response.data.data.industry_data._id,
        title: response.data.data.industry_data.title,
        slug: response.data.data.industry_data.slug,
        profile_image: response.data.data.industry_data.image,
        description: response.data.data.industry_data.description,
        content: response.data.data.industry_data.content,
        relatedcontent: response.data.data.industry_data.relatedcontent
      };
      setTimeout(() => {
        this.setState({ cimonIndustry: newIndustryarray });
        let checkvalue =
          response.data.data.industry_data.is_top_industry == "1"
            ? true
            : false;
        this.setState({ checkval: checkvalue });
        this.setState({spinner:false});
      }, 2000);
    } else {
      this.setState({
        industryStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
      this.setState({spinner:false});
    }
  };

  /* Form Submit */
  handleSubmit = async () => {
    const cimonIndustry = { ...this.state.cimonIndustry };
    const errors = { ...this.state.errors };
    let result = Joi.validate(cimonIndustry, this.schema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });

        if (this.state.upload_data) {
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            cimonIndustry["profile_image"] = filepath;
            this.setState({ cimonIndustry });
            this.updateIndustryData();
          } else {
            
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }
        } else {
          this.updateIndustryData();
        }
      }
    }
  };

  /* Update Industry */
  updateIndustryData = async () => {
    const cimonIndustry = { ...this.state.cimonIndustry };
    cimonIndustry["checked"] = this.state.checkval == true ? "1" : "0";
    this.setState({ cimonIndustry });
    const response = await Industry.updateIndustry(cimonIndustry);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
  };
  /* Upload Image */
  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors };
    let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      let file = value.target.files[0];
      upload_data = file;
      this.setState({ upload_data: upload_data });
    }
  };
  render() {
    let checkedCond = this.state.cimonIndustry.checked;
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents addpage-form-wrap Industry-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                    <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className="row addpage-form-wrap">
                      <div className="col-lg-8 col-md-12">
                        <div className="form-group">
                          <label htmlFor="">Update Industry</label>
                          <input
                            name="title"
                            value={this.state.cimonIndustry.title}
                            onChange={e => this.handleChange(e)}
                            type="text"
                            placeholder="Add title *"
                            className="form-control"
                          />
                          {this.state.errors.title ? (
                            <div className="error text-danger">
                              {this.state.errors.title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Slug</label>
                          <input
                            name="slug"
                            value={this.state.cimonIndustry.slug}
                            onChange={e => this.handleChange(e)}
                            type="text"
                            placeholder="slug *"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Short Description</label>
                          <textarea
                            name="description"
                            onChange={e => this.handleChange(e)}
                            value={this.state.cimonIndustry.description}
                            className="form-control"
                          ></textarea>
                          {this.state.errors.description ? (
                            <div className="error text-danger">
                              {this.state.errors.description}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-12">
                              <label htmlFor="">Content</label>
                              <CKEditor
                                data={this.state.cimonIndustry.content}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorChange}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-12">
                              <label htmlFor="">Related Content</label>
                              <CKEditor
                                data={this.state.cimonIndustry.relatedcontent}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onRelatedContentChange}
                              />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-4 col-md-12 ">
                        {/* <div className="form-group form-group-inline checkbox-div">
                                        <label htmlFor="">Is top Industry?</label>
                                        <input type="checkbox"  onChange={this.handleCheck}  checked={this.state.checkval}/>
                                    </div> */}
                        <div className="form-group thumbanail_container">
                          <Uploadprofile
                            onuplaodProfile={this.onuplaodProfile}
                            value={this.state.cimonIndustry.profile_image}
                            errors={this.state.errors}
                          />
                        </div>
                        <div className="faq-sideinputs">
                          <div className="faq-btns form-btn-wrap">
                            <div className="update_btn input-group-btn float-right">
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="btn btn-info"
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                                Update
                              </button>
                            </div>
                          </div>
                          {this.state.message !== "" ? (
                            <div className="tableinformation">
                              <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                              </div>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                    </BlockUi>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminEditIndustry;
