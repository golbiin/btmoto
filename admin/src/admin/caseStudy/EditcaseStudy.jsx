import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import ReactSpinner from 'react-bootstrap-spinner'
import Joi, { join } from 'joi-browser'
import Uploadprofile from '../common/uploadProfileimage'
import * as settingsService from '../../ApiServices/admin/manageCasestudy'
import * as manageUser from '../../ApiServices/admin/manageUser'
import CKEditor from 'ckeditor4-react'
import { apiUrl ,siteUrl} from '../../../src/config.json'
import * as manageProducts from '../../ApiServices/admin/products'
import MultiSelect from 'react-multi-select-component'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import moment from 'moment'
import BlockUi from 'react-block-ui';
class EditCaseStudy extends Component {
  constructor (props) {
    super(props)
    this.handleCheck = this.handleCheck.bind(this)

    this.handleEditorChange = this.handleEditorChange.bind(this)
    this.onEditorChange = this.onEditorChange.bind(this)
    this.onRelatedContentChange = this.onRelatedContentChange.bind(this)
  }
  state = {
    spinner: false,
    checkval: '',
    profile_image: '',
    errors: {},
    submit_status: false,
    message: '',
    message_type: '',
    upload_data: '',
    cimonCasestudy: [],
    validated: false,
    CasestudyStatus: true,
    focused: false,
    related_products: [],
    related_products_options: [],
    related_products_selected: [],

    related_cat: [],
    related_cat_options: [],
    related_cat_selected: [],
    releasedate: '',
    date: ''
  }

  onEditorChange (evt) {
    const cimonCasestudy = { ...this.state.cimonCasestudy }
    cimonCasestudy['content'] = evt.editor.getData()
    this.setState({
      cimonCasestudy: cimonCasestudy
    })
  }

  onRelatedContentChange (evt) {
    const cimonCasestudy = { ...this.state.cimonCasestudy }
    cimonCasestudy['relatedcontent'] = evt.editor.getData()
    this.setState({
      cimonCasestudy: cimonCasestudy
    })
  }

  handleEditorChange (changeEvent) {
    const cimonCasestudy = { ...this.state.cimonCasestudy }
    cimonCasestudy['content'] = changeEvent.target.value
    this.setState({
      cimonCasestudy: cimonCasestudy
    })
  }

  onclassicEdit = async (value, item) => {
    if (item === 'editorContent') {
      const cimonCasestudy = { ...this.state.cimonCasestudy }
      cimonCasestudy['content'] = value.getData()
      this.setState({
        cimonCasestudy: cimonCasestudy
      })
    }
  }

  /* Checkbox on Change */
  handleCheck (e) {
    this.setState({ checkval: e.target.checked })
  }

  /*Joi validation schema*/
  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    sub_title: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    slug: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    description: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is Required'
        }
      }),
    _id: Joi.optional().label('id'),
    profile_image: Joi.allow(null),
    content: Joi.allow(null),
    relatedcontent: Joi.allow(null),
    data_order: Joi.number()
    .required()
    .error(() => {
      return {
        message: "This field is a required field and must be a number."
      };
    })
  }

  /* Input Handle Change */
  handleChange = (event, type = null) => {
    let cimonCasestudy = { ...this.state.cimonCasestudy }
    const errors = { ...this.state.errors }
    this.setState({ message: '' })
    delete errors.validate
    let name = event.target.name //input field  name
    let value = event.target.value //input field value
    const errorMessage = this.validateCasestudydata(name, value)
    if (errorMessage) errors[name] = errorMessage
    else delete errors[name]
    cimonCasestudy[name] = value
    this.setState({ cimonCasestudy, errors })
  }
  validateCasestudydata = (name, value) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)
    return error ? error.details[0].message : null
  }

  setSelected = event => {
    const data_selected_row = []
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value
    })

    this.setState({ related_products: data_selected_row })
    this.setState({ related_products_selected: event })
  }
  // getAllProductsIdsandNames = async () => {
  //   const response = await manageProducts.getAllProductsIdsandNames();
  //   if (response.data.status == 1) {
  //     const Setdata = { ...this.state.related_products };
  //     const data_related_row = [];
  //     const data_selected_row = [];
  //     response.data.data.map((Products, index) => {
  //       const Setdata = {};
  //       Setdata.value = Products._id;
  //       Setdata.label = Products.product_name;

  //       if (this.state.related_products) {
  //         if (this.state.related_products.indexOf(Products._id) > -1) {
  //           data_selected_row.push(Setdata);
  //         }
  //       }

  //       data_related_row.push(Setdata);
  //     });
  //     console.log('data_related_row', data_selected_row, this.state.related_products);
  //     this.setState({ related_products_options: data_related_row });
  //     this.setState({ related_products_selected: data_selected_row });
  //   }
  // };

  setcatSelected = event => {
    const data_selected_row = []
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value
    })

    this.setState({ related_cat: data_selected_row })
    this.setState({ related_cat_selected: event })
  }

  // get cats ids& name
  // getAllCatsIdsandNames = async () => {
  //   const response = await settingsService.getAllCatsIdsandNames();
  //   if (response.data.status == 1) {
  //     const Setdata = { ...this.state.related_cat };
  //     const data_related_row = [];
  //     const data_selected_row = [];
  //     response.data.data.map((Cats, index) => {
  //       const Setdata = {};
  //       Setdata.value = Cats._id;
  //       Setdata.label = Cats.category_name;

  //       if (this.state.related_cat) {
  //         if (this.state.related_cat.indexOf(Cats._id) > -1) {
  //           data_selected_row.push(Setdata);
  //         }
  //       }

  //       data_related_row.push(Setdata);
  //     });
  //     console.log('related_cat', data_selected_row, this.state.related_cat);
  //     this.setState({ related_cat_options: data_related_row });
  //     this.setState({ related_cat_selected: data_selected_row });
  //   }
  // };

  getIndustryMenu = async () => {
    const response = await settingsService.getIndustryMenu()
    if (response.data.status == 1) {
      const Setdata = { ...this.state.related_cat }
      const data_related_row = []
      const data_selected_row = []

      response.data.data.map((Cats, index) => {
        console.log('elveee', Cats)
        const Setdata = {}
        Setdata.value = Cats.url
        Setdata.label = Cats.name

        if (this.state.related_cat) {
          if (this.state.related_cat.indexOf(Cats.url) > -1) {
            data_selected_row.push(Setdata)
          }
        }

        data_related_row.push(Setdata)
      })
      console.log('related_cat', data_selected_row, this.state.related_cat)
      this.setState({ related_cat_options: data_related_row })
      this.setState({ related_cat_selected: data_selected_row })
    }
  }

  getProductMenu = async () => {
    const response = await settingsService.getProductMenu()
    if (response.data.status == 1) {
      const Setdata = { ...this.state.related_products }
      const data_related_row = []
      const data_selected_row = []

      response.data.data.map((Cats, index) => {
        console.log('elveee', Cats)
        const Setdata = {}
        Setdata.value = Cats.url
        Setdata.label = Cats.name

        if (this.state.related_products) {
          if (this.state.related_products.indexOf(Cats.url) > -1) {
            data_selected_row.push(Setdata)
          }
        }

        data_related_row.push(Setdata)
      })
      console.log('related_cat', data_selected_row, this.state.related_cat)
      this.setState({ related_products_options: data_related_row })
      this.setState({ related_products_selected: data_selected_row })
    }
  }

  handleDateChange = async e => {
    this.setState({ releasedate: e.target.value })
  }

  handledateChange = date => {
    const event = new Date();
    this.setState({
      date: (date ? date : moment(event).toDate())
    })
  }

  /* Get single Casestudy */
  componentDidMount = async () => {
    this.setState({spinner:true});
    const id = this.props.match.params.id
    const response = await settingsService.getSingleCasestudy(id)
    if (response.data.status == 1) {
      let newCasestudyarray = {
        _id: response.data.data.Casestudy_data._id,
        title: response.data.data.Casestudy_data.title,
        sub_title: response.data.data.Casestudy_data.sub_title
          ? response.data.data.Casestudy_data.sub_title
          : '',
        slug: response.data.data.Casestudy_data.slug,
        profile_image: response.data.data.Casestudy_data.image,
        description: response.data.data.Casestudy_data.description,
        content: response.data.data.Casestudy_data.content,
        relatedcontent: response.data.data.Casestudy_data.relatedcontent,
        data_order : response.data.data.Casestudy_data.data_order
      }
      this.setState({
        related_products: response.data.data.Casestudy_data.related_products
      })
      this.setState({
        related_cat: response.data.data.Casestudy_data.related_cat
      })
      this.setState({
        releasedate: response.data.data.Casestudy_data.releasedate
      })
      this.setState({
        date: moment(response.data.data.Casestudy_data.date).toDate()
      })

      setTimeout(() => {
        this.setState({ cimonCasestudy: newCasestudyarray })
        let checkvalue =
          response.data.data.Casestudy_data.is_top_case_study == '1'
            ? true
            : false
        this.setState({ checkval: checkvalue });
        this.setState({spinner:false});
      }, 2000)
      //  this.getAllProductsIdsandNames();
      //    this.getAllCatsIdsandNames();
      this.getIndustryMenu()
      this.getProductMenu()
    } else {
      this.setState({
        CasestudyStatus: false,
        message: response.data.message,
        responsetype: 'error'
      })
      this.setState({spinner:false});
    }
  }

  /* Form Submit */
  handleSubmit = async () => {
    const cimonCasestudy = { ...this.state.cimonCasestudy }
    const errors = { ...this.state.errors }
    let result = Joi.validate(cimonCasestudy, this.schema)
    if (result.error) {
      let path = result.error.details[0].path[0]
      let errormessage = result.error.details[0].message
      errors[path] = errormessage
      this.setState({
        errors: errors
      })
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        })
      } else {
        this.setState({ submit_status: true })
        this.setState({ spinner: true })

        
        if (this.state.upload_data) {
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          )
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location
            cimonCasestudy['profile_image'] = filepath
            this.setState({ cimonCasestudy })
            this.updateCasestudyData()
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: 'error'
            })
            this.setState({ spinner: false });
          }
        } else {
          this.updateCasestudyData()
        }
      }
    }
  }

  /* Update Casestudy */
  updateCasestudyData = async () => {
    const cimonCasestudy = { ...this.state.cimonCasestudy }
    cimonCasestudy['checked'] = this.state.checkval == true ? '1' : '0'
    cimonCasestudy['related_products'] = this.state.related_products
    cimonCasestudy['related_cat'] = this.state.related_cat
    cimonCasestudy['releasedate'] = this.state.releasedate
    cimonCasestudy['date'] = this.state.date.toISOString()
    this.setState({ cimonCasestudy })
    const response = await settingsService.updateCasestudy(cimonCasestudy)
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false })
        this.setState({
          message: response.data.message,
          message_type: 'success'
        })
        this.setState({ submit_status: false })
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false })
        this.setState({ submit_status: false })
        this.setState({
          message: response.data.message,
          message_type: 'error'
        })
      }
    }
  }
  /* Upload Image */
  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors }
    let upload_data = { ...this.state.upload_data }
    if (item === 'errors') {
      errors['profile_image'] = value
    } else {
      let file = value.target.files[0]
      upload_data = file
      this.setState({ upload_data: upload_data })
    }
  }
  render () {
    let checkedCond = this.state.cimonCasestudy.checked

    const minOffset = 0
    const maxOffset = 25
    const thisYear = new Date().getFullYear() + 5
    const options = []
    for (let i = minOffset; i <= maxOffset; i++) {
      const year = thisYear - i
      options.push(<option value={year}>{year}</option>)
    }
    return (
      <React.Fragment>
        <div className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents addpage-form-wrap Casestudy-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4'>
                  <div className='addpage-form'>
                    <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className='row addpage-form-wrap'>
                      <div className='col-lg-8 col-md-12'>
                        <div className='form-group'>
                          <label htmlFor=''>Update Case Study</label>
                          <input
                            name='title'
                            value={this.state.cimonCasestudy.title}
                            onChange={e => this.handleChange(e)}
                            type='text'
                            placeholder='Add title *'
                            className='form-control'
                          />
                          {this.state.errors.title ? (
                            <div className='error text-danger'>
                              {this.state.errors.title}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>

                        <div className='form-group'>
                          <label htmlFor=''>Slug</label>
                          <input
                            name='slug'
                            value={this.state.cimonCasestudy.slug}
                            onChange={e => this.handleChange(e)}
                            type='text'
                            placeholder='slug *'
                            className='form-control'
                          />
                          {this.state.errors.slug ? (
                            <div className='error text-danger'>
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>

                        <div className='form-group'>
                          <label htmlFor=''>Sub title</label>
                          <input
                            name='sub_title'
                            value={this.state.cimonCasestudy.sub_title}
                            onChange={e => this.handleChange(e)}
                            type='text'
                            placeholder='Sub title *'
                            className='form-control'
                          />
                          {this.state.errors.sub_title ? (
                            <div className='error text-danger'>
                              {this.state.errors.sub_title}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>

                        <div className='form-group'>
                          <label htmlFor=''>Short Description</label>
                          <textarea
                            name='description'
                            onChange={e => this.handleChange(e)}
                            value={this.state.cimonCasestudy.description}
                            className='form-control'
                          ></textarea>
                          {this.state.errors.description ? (
                            <div className='error text-danger'>
                              {this.state.errors.description}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>
                        <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <label htmlFor=''>Content</label>
                              <CKEditor
                                data={this.state.cimonCasestudy.content}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  image2_alignClasses : [ 'align-left', 'align-center', 'align-right' ],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + '/admin/upload/imageupload-new'
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorChange}
                              />
                            </div>
                          </div>
                        </div>

                        <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <label htmlFor=''>Related Content</label>
                              <CKEditor
                                data={this.state.cimonCasestudy.relatedcontent}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + '/admin/upload/imageupload-new'
                                }}
                                onInit={editor => {}}
                                onChange={this.onRelatedContentChange}
                              />
                            </div>
                          </div>
                        </div>

                        <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Select Products</h5>

                              <MultiSelect
                                options={this.state.related_products_options}
                                value={this.state.related_products_selected}
                                onChange={this.setSelected} //  labelledBy={"Select"}
                              />
                            </div>{' '}
                          </div>{' '}
                        </div>

                        <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Select Category</h5>

                              <MultiSelect
                                options={this.state.related_cat_options}
                                value={this.state.related_cat_selected}
                                onChange={this.setcatSelected} //  labelledBy={"Select"}
                              />
                            </div>{' '}
                          </div>{' '}
                        </div>

                        <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Release Date</h5>

                              <select
                                value={this.state.releasedate}
                                className='form-group-relase'
                                onChange={this.handleDateChange}
                              >
                                <option value=''>Release Date</option>
                                {options}
                              </select>
                            </div>{' '}
                          </div>{' '}
                        </div>
                  
                        <div className='form-group'>
                          <label htmlFor=''>Case Study Order</label>
                          <input
                            name='data_order'
                            value={this.state.cimonCasestudy.data_order}
                            onChange={e => this.handleChange(e)}
                            type='text'
                            placeholder='Add Case Study Order *'
                            className='form-control'
                          />
                          {this.state.errors.data_order ? (
                            <div className='error text-danger'>
                              {this.state.errors.data_order}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>

                  
                      </div>

                      <div className='col-lg-4 col-md-12 '>
                        <div className='form-group form-group-inline checkbox-div'>
                          <label htmlFor=''>Feature this Case Study</label>
                          <input 
                            type='checkbox'  
                            onChange={this.handleCheck}
                            checked={this.state.checkval}
                          />
                        </div>
                        <div className='form-group thumbanail_container'>
                          <Uploadprofile
                            onuplaodProfile={this.onuplaodProfile}
                            value={this.state.cimonCasestudy.profile_image}
                            errors={this.state.errors}
                          />
                        </div>

                        <div className='form-group thumbanail_container'>
                          <label>Published on</label>
                          <DatePicker
                            selected={this.state.date}
                            timeInputLabel='Time:'
                            dateFormat='MMMM d, yyyy h:mm aa'
                            showTimeInput
                            onChange={this.handledateChange}
                          />
                        </div>

                        <div className='faq-sideinputs'>
                          <div className='faq-btns form-btn-wrap'>
                            <div className='update_btn input-group-btn float-right'>
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? 'disabled'
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className='btn btn-info'
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type='border'
                                    color='dark'
                                    size='1'
                                  />
                                ) : (
                                  ''
                                )}
                                Update
                              </button>
                            </div>
                          </div>
                          {this.state.message !== '' ? (
                            <div className='tableinformation'>
                              <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                              </div>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                    </BlockUi>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default EditCaseStudy
