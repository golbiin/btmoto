import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import validate from "react-joi-validation";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import * as settingsService from "../../ApiServices/admin/distributorResources";
import * as manageUser from "../../ApiServices/admin/manageUser";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl} from "../../../src/config.json";
import * as manageCasestudy from '../../ApiServices/admin/manageCasestudy'
import MultiSelect from 'react-multi-select-component'
import DatePicker from 'react-datepicker'
import moment from "moment";
import * as manageProducts from "../../ApiServices/admin/products";
import 'react-datepicker/dist/react-datepicker.css'
import BlockUi from 'react-block-ui';
import Moment from 'moment';
class EditDistributorResources  extends Component {
    constructor(props) {
        super(props);
        this.handleCheck = this.handleCheck.bind(this);
        this.handleEditorChange = this.handleEditorChange.bind( this );
        this.onEditorChange = this.onEditorChange.bind( this );
        this.uploadSingleFiles = this.uploadSingleFiles.bind(this);
    }
    state = { 
        checkval: "",
        profile_image:"",
        errors: {},
        submit_status: false,
        message : "",
        message_type: "",
        upload_data: "",
        cimonNews: [],
        validated: false,
        newsStatus: true,
        releasedate: '',
        focused: false,
        related_products: [],
        related_products_options: [],
        related_products_selected: [],
        related_cat : '',
        multiple_upload_status :false,
        multiple_file_array :[],
        CategoryOptions : []
    
    }

    onEditorChange( evt ) {
      const cimonNews = { ...this.state.cimonNews };
      cimonNews["content"] = evt.editor.getData()
      this.setState({
        cimonNews : cimonNews
      })
    }
  
    handleEditorChange( changeEvent ) {
      const cimonNews = { ...this.state.cimonNews };
      cimonNews["content"] = changeEvent.target.value
      this.setState({
        cimonNews : cimonNews
      })
      
    }

    onclassicEdit = async (value, item) => {
      if (item === "editorContent") {
          const cimonNews = { ...this.state.cimonNews };
          cimonNews["content"] = value.getData();
          this.setState({
            cimonNews : cimonNews
          })
      } 
    };

    /* Checkbox on Chnage*/
    handleCheck(e){
        this.setState({ checkval : e.target.checked });
      }
    
    /*Joi validation schema */  
    schema = {
        title: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        slug: Joi.string()
        .required()
        .error(() => {
          return {
            message: "This field is Required",
          };
        }),
        
        // description: Joi.string()
        // .required()
        // .error(() => {
        //   return {
        //     message: "This field is Required",
        //   };
        // }),
        _id: Joi.optional().label("id"),
       // profile_image:Joi.allow(null),
        content:Joi.allow(null),
        version : Joi.optional().label("version"),
        file_url : Joi.optional().label("file_url"),
        data_order: Joi.number()
          .required()
          .error(() => {
            return {
              message: "This field is a required field and must be a number"
            };
        })
    
    };

    /*Input Handile Change */
    handleChange = (event, type = null) => {
        let cimonNews = { ...this.state.cimonNews };
        const errors = { ...this.state.errors };
        this.setState({ message: "" });
        delete errors.validate;
        let name = event.target.name; //input field  name
        let value = event.target.value; //input field value
        const errorMessage = this.validateNewsdata(name, value);
        if (errorMessage) errors[name] = errorMessage;
        else delete errors[name];
        cimonNews[name] = value;
        this.setState({ cimonNews, errors });
    };
    validateNewsdata = (name, value) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };
    
    componentDidMount = async () => {
        this.setState({ spinner: true });
        const id = this.props.match.params.id;
        const response = await settingsService.getSingleDistributors(id);
        console.log(10,response);
        let cimonNews = { ...this.state.cimonNews };
        if(response.data.status == 1){
          let newNewsarray = {
            _id: response.data.data.news_data._id,
            title: response.data.data.news_data.title,
            slug: response.data.data.news_data.slug,
            file_url: response.data.data.news_data.file_url,
            // profile_image: response.data.data.news_data.image,
            //description: response.data.data.news_data.description,
            content : response.data.data.news_data.content,
            version : response.data.data.news_data.version,
           //releasedate: moment(response.data.data.news_data.releasedate).toDate(),
           data_order : response.data.data.news_data.data_order,
          };

          if (response.data.data.news_data.uploadFiles ) {

            if(response.data.data.news_data.uploadFiles.archiveFiles.length){
              this.setState({
                multiple_file_array:
                   (response.data.data.news_data.uploadFiles.archiveFiles == '[]' || response.data.data.news_data.uploadFiles.archiveFiles.length <=0) ?  [] :  JSON.parse(response.data.data.news_data.uploadFiles.archiveFiles),
              });
            }  
           
          }
          this.setState({
            related_products: response.data.data.news_data.related_products ,
            related_cat : response.data.data.news_data.related_cat,
            releasedate: moment(response.data.data.news_data.releasedate).toDate(),
           
          })
            console.log(444,this.state.cimonNews);
          setTimeout(() => { 
            
            this.setState({ cimonNews : newNewsarray });
            let checkvalue =  ((response.data.data.news_data.is_top_news == '1') ? true : false);
            this.setState({ checkval : checkvalue });
            this.setState({ spinner: false });
          }, 2000);
          
        }else{
          this.setState({
            newsStatus: false,
            message: response.data.message,
            responsetype: "error",
          });
          this.setState({ spinner: false });
        } 
        
        this.getProductMenu();
        this.getAllCategoryOptions();
    };



    getAllCategoryOptions = async () => {
      const response = await settingsService.getAllCategoryOptions()
      if (response.data.status == 1) {
        console.log('getAllCategoryOptions', response.data.data)
        this.setState({
          CategoryOptions: response.data.data  
        })
      }
    }
      
     /* Form  Submit */
    handleSubmit = async () => {
      console.log(123,'sdf');
        const cimonNews = { ...this.state.cimonNews };
        const errors = { ...this.state.errors };
        let result = Joi.validate(cimonNews, this.schema);
        if (result.error) {
            let path = result.error.details[0].path[0];
            let errormessage = result.error.details[0].message;
            errors[path] = errormessage;
                this.setState({
                    errors: errors  
                  })
            } else {
              
            if (errors.length > 0) {
                this.setState({
                    errors: errors  
                  })
            }else{
                this.setState({ submit_status: true });
                this.setState({ spinner: true });
                
            

                  this.updateDistributorsData();
                  
            }
        }
    };

    updateDistributorsData = async () => {
      let newDate ="";
      if(this.state.releasedate !="")
        newDate = Moment(new Date(this.state.releasedate)).format('YYYY-MM-DD');
        const cimonNews = { ...this.state.cimonNews };
       // cimonNews["checked"] = (this.state.checkval == true ? '1' : '0');
        cimonNews['related_products'] = this.state.related_products;
        cimonNews['related_cat'] = this.state.related_cat;
        cimonNews["releasedate"] = newDate;
        cimonNews["uploadFiles"] = {
          archiveFiles: this.state.multiple_file_array ,
        };

     
        console.log(43343,this.state.releasedate);
        this.setState({ cimonNews });
        const response = await settingsService.updateDistributors(cimonNews);
        if (response) {
            if(response.data.status === 1){
                this.setState({ spinner: false });
                this.setState({
                    message: response.data.message,
                    message_type: "success",
                  });
                  this.setState({ submit_status: false });
                  setTimeout(() => { 
                    window.location.reload();
                  }, 2000);
            } else {
                this.setState({ spinner: false });
                this.setState({ submit_status: false });
                this.setState({
                    message: response.data.message,
                    message_type: "error",
                  });
            }
        }
    }

    // onuplaodProfile = async (value, item) => {
    //     let errors = { ...this.state.errors };
    //     let upload_data = { ...this.state.upload_data };
    //     if (item === "errors") {
    //       errors["profile_image"] = value;
    //     } else {
    //       let file = value.target.files[0];
    //       upload_data = file;
    //       this.setState({ upload_data : upload_data });
    //     }
    // };





/* upload single file upload */      
uploadSingleFiles  = async (e) =>{
  this.setState({ upload_status: true });
  const response1 = await manageProducts.uploadProductImages(
    e.target.files
  ); 
  console.log(222,response1);
  if (response1.data.status == 1) {
    response1.data.data.file_location.map((item, key) => {
      this.setState({ productfileArray: item.Location});
      let cimonNews = { ...this.state.cimonNews };
      cimonNews['file_url'] =  item.Location;
      this.setState({ cimonNews: cimonNews});      
    });    
    this.setState({ upload_status: false });
     } else {
    this.setState({
      submitStatus: false,
      message: response1.data.message,
      responsetype: "error",
    });
    this.setState({ upload_status: false });
  }
}

 /* Multiple file upload */

 uploadMultipleFiles = async (e) => {
  this.setState({ multiple_upload_status: true });
  const response1 = await manageProducts.uploadProductImages(e.target.files);
  if (response1.data.status == 1) {
    response1.data.data.file_location.map((item, key) =>
      this.setState({
        multiple_file_array: this.state.multiple_file_array.concat({
          file_name: item.Location,
          file_url: item.Location,
        }),
      })
    );
    this.setState({ multiple_upload_status: false });
  } else {
    this.setState({
      submitStatus: false,
      message: response1.data.message,
      responsetype: "error",
    });
    this.setState({ multiple_upload_status: false });
  }
};

handleRemoveDistributorImg(i) {
  const filteredvalue = this.state.multiple_file_array.filter(
    (s, sidx) => i !== sidx
  );
  if (filteredvalue) {
    this.setState({
      multiple_file_array: filteredvalue,
    });
  }
}

    getProductMenu = async () => {
      const response = await manageCasestudy.getProductMenu()
      if (response.data.status == 1) {
        const Setdata = { ...this.state.related_products }
        const data_related_row = []
        const data_selected_row = []
  
        response.data.data.map((Cats, index) => {
          console.log('elveee', Cats)
          const Setdata = {}
          Setdata.value = Cats.url
          Setdata.label = Cats.name
  
          if (this.state.related_products) {
            if (this.state.related_products.indexOf(Cats.url) > -1) {
              data_selected_row.push(Setdata)
            }
          }
  
          data_related_row.push(Setdata)
        })
        console.log('related_cat', data_selected_row, this.state.related_cat)
        this.setState({ related_products_options: data_related_row })
        this.setState({ related_products_selected: data_selected_row })
      }
    }
  
    setSelected = event => {
      const data_selected_row = []
      event.map((selectValues, index) => {
        data_selected_row[index] = selectValues.value
      })
  
      this.setState({ related_products: data_selected_row })
      this.setState({ related_products_selected: event })
    }

    handledateChange = (date) => {
      this.setState({
        releasedate: date,
      });
      console.log(7777,this.state.releasedate);
    };
    handleselectcat = async e => {
      this.setState({ related_cat: e.target.value })
    }

    onEditorChange( evt ) {
      const cimonNews = { ...this.state.cimonNews };
      cimonNews["content"] = evt.editor.getData()
      this.setState({
        cimonNews : cimonNews
      })
    }
 
    render() { 
      const images = require.context("../../assets/images/admin", true); 
        let checkedCond  = this.state.cimonNews.checked;
        const { CategoryOptions } = this.state;
        const minOffset = 0
        const maxOffset = 25
        const thisYear = new Date().getFullYear() + 5
        const options = []
        for (let i = minOffset; i <= maxOffset; i++) {
          const year = thisYear - i
          options.push(<option value={year}>{year}</option>)
        }
console.log(666,this.state.multiple_file_array)
        return ( <React.Fragment>
            <div className="container-fluid admin-body">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div>   
            <div className="col-md-10 col-sm-12  content">
                <div className="row content-row">
                    <div className="col-md-12  header">
                        <Adminheader props={this.props} />
                    </div>
                    <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4" >  
                    <div className="addpage-form">
                      <BlockUi tag="div" blocking={this.state.spinner} >
                        <div  className="row addpage-form-wrap">
                            <div className="col-lg-8 col-md-12">                                
                                                             
                                <div className="form-group">
                                    <label htmlFor="">Update</label>
                                    <input name="title" 
                                    value={this.state.cimonNews.title} 
                                    onChange={(e) => this.handleChange(e)}
                                    type="text"
                                    placeholder="Add title *" 
                                    className="form-control"/>
                                    {this.state.errors.title ? (<div className="error text-danger">
                                            {this.state.errors.title}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>
                                 <div className="form-group">
                                    <label htmlFor="">Slug</label>
                                    <input name="slug" 
                                    value={this.state.cimonNews.slug} 
                                    onChange={(e) => this.handleChange(e)}
                                    type="text" placeholder="slug *" className="form-control"/>
                                    {this.state.errors.slug ? (<div className="error text-danger">
                                            {this.state.errors.slug}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>


                                <div className="form-group">
                                    <label htmlFor="">Version</label>
                                    <input name="version" 
                                    value={this.state.cimonNews.version} 
                                    onChange={(e) => this.handleChange(e)}
                                    type="text" placeholder="version" className="form-control"/>
                                    {/* {this.state.errors.version ? (<div className="error text-danger">
                                            {this.state.errors.version}
                                            </div>
                                        ) : (
                                            ""
                                        )} */}
                                </div>

                                <div className="gallery-single">        
                                <div className="form-group">
                                    <label>File</label>
                                    <input type="file" className="form-control"  onChange={this.uploadSingleFiles}  />
                                    {this.state.upload_status === true ? (
                                      <ReactSpinner type="border" color="dark" size="1" />
                                    ) : (
                                     ""
                                    )} 
                                
                              
                                <a href={this.state.cimonNews.file_url?this.state.cimonNews.file_url:""}>{this.state.cimonNews.file_url?this.state.cimonNews.file_url:""}</a>  
                                  </div> 
                              </div> 
                              <div className="form-group">
                                        <label>Multiple File</label>
                                        <input
                                          type="file"
                                          className="form-control"
                                          onChange={this.uploadMultipleFiles}
                                          multiple
                                        />
                                        {this.state.multiple_upload_status ===
                                        true ? (
                                          <ReactSpinner
                                            type="border"
                                            color="dark"
                                            size="1"
                                          />
                                        ) : (
                                          ""
                                        )}






                             <div className="multi-preview">
                                        {
                                        
                                        this.state.multiple_file_array.length > 0 ?(
                                        
                                        this.state.multiple_file_array.map(
                                          (files, index) => (
                                            <div
                                              key={index}
                                              className="single-img"
                                            >
                                             
                                              <a 
                                                href={files.file_url}
                                                id={index}
                                                alt="..."
                                                width="80px"
                                                height="70px"
                                              > { files.file_url }</a>
                                                        <span
                                                value={index}
                                                onClick={() =>
                                                  this.handleRemoveDistributorImg(
                                                    index
                                                  )
                                                }
                                              >
                                                <img
                                                  src={images(`./close.jpg`)}
                                                  className="img-fluid "
                                                  alt="customer"
                                                ></img>
                                              </span>
                                      
                                            </div>
                                          )
                                        )
                                        ) : ''
                                      }
                                      </div>
                                      </div>
                                
                                      <div className="form-group">
                                      <label>Content</label>
                                  <div className="row">
                                    <div className="col-md-12">

                                      <CKEditor
                                          data={this.state.cimonNews.content}
                                          config={ {
                                            extraPlugins: ["justify" , "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                              allowedContent: true,
                                              filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                              } }

                                          onInit = { 
                                              editor => {          
                                              } 
                                          }
                                          onChange={this.onEditorChange}
                                      />
                                 </div>
                                 </div>
                                 </div>


                                {/* <div className="form-group">
                                    <label htmlFor="">Short Description</label>
                                    <textarea name="description"  
                                    onChange={(e) => this.handleChange(e)}
                                    value={this.state.cimonNews.description}
                                    className="form-control">
                                    </textarea>
                                    {this.state.errors.description ? (<div className="error text-danger">
                                            {this.state.errors.description}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div> */}

                                {/* <div className="form-group">
                                  <div className="row">
                                    <div className="col-md-12">
                                      <label htmlFor="">Content</label>
                                      <CKEditor
                                          data={this.state.cimonNews.content}
                                          config={ {
                                            extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                              allowedContent: true,
                                              filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                              } }

                                          onInit = { 
                                              editor => {          
                                              } 
                                          }
                                          onChange={this.onEditorChange}
                                      />
                                    </div> 
                                </div>
                                </div> */}




                                <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Select Products</h5>

                              <MultiSelect
                                options={this.state.related_products_options}
                                value={this.state.related_products_selected}
                                onChange={this.setSelected} //  labelledBy={"Select"}
                              />
                            </div>{' '}
                          </div>{' '}
                        </div>


                        <div className='form-group'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Select Category</h5>

                                    <select
                                     value={this.state.related_cat}
                                     className='form-group-relase'
                                     onChange={this.handleselectcat}
                                    >

                                    <option value="">Category</option>
                                    {
                                      CategoryOptions.length > 0 ? (
                                          CategoryOptions.map((data, index) => {
                                          return (
                                            <>
                                            <option key={data._id} value={data._id}>{data.title}</option>
                                            </>);
                                      })) : ""
                                    }
                                      {/* <option value="">Category</option>
                                      <option value="catalogs">Catalogs</option>
                                      <option value="certificates">Certificates</option>
                                      <option value="software">CIMON Software</option>
                                      <option value="marketing">Marketing Materials</option>
                                      <option value="manual">Product Manuals</option>
                                      <option value="videos">Videos</option> */}
                                      
                                    </select>        
                                          
                            </div> 
                          </div> 
                        </div>



                        <div className='form-group relDate'>
                          <div className='row'>
                            <div className='col-md-12'>
                              <h5>Release Date</h5>

                              {}
                                <DatePicker
                                  selected={this.state.releasedate}
                                  placeholderText="Release Date *"
                                  maxDate={new Date()}                                  
                                  dateFormat="dd-MM-yyyy"                                  
                                  onChange={this.handledateChange}
                                  // className= {form-control}
                                />
                            </div> 
                          </div> 
                        </div>
                  
                            <div className="form-group">
                                    <label htmlFor=""> Order</label>
                                    <input name="data_order" 
                                    value={this.state.cimonNews.data_order} 
                                    onChange={(e) => this.handleChange(e)}
                                    type="text"
                                    placeholder="Add  Order *" 
                                    className="form-control"/>
                                    {this.state.errors.data_order ? (<div className="error text-danger">
                                            {this.state.errors.data_order}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                </div>


                            </div>

                            <div className="col-lg-8 col-md-12 ">                           
                                    {/* <div className="form-group form-group-inline checkbox-div">
                                        <label htmlFor="">Is top news?</label>
                                        <input type="checkbox"  onChange={this.handleCheck}  checked={this.state.checkval}/>
                                    </div>
                                  */}
                                 
                                 
                                    {/* <div className="form-group thumbanail_container">
                                        <Uploadprofile
                                        onuplaodProfile={this.onuplaodProfile}
                                        value={
                                        this.state.cimonNews.profile_image
                                        }
                                        errors={this.state.errors}
                                        />
                                    </div> */}
                                    <div className="faq-sideinputs">
                                    <div className="faq-btns form-btn-wrap">   
                                      <div className="update_btn input-group-btn float-right"> 
                                        <button
                                        disabled={this.state.submit_status === true ? "disabled" : false}
                                        onClick={this.handleSubmit} className="btn btn-info">
                                        {this.state.submit_status ? (
                                        <ReactSpinner type="border" color="dark" size="1" />
                                        ) : (
                                        ""
                                        )}
                                        Update
                                        </button>
                                
                                      </div>
                                    </div>

                                    {this.state.message !== "" ? (
                                    <div className="tableinformation">
                                      <div className={this.state.message_type}>
                                      <p>{this.state.message}</p>
                                      </div>
                                    </div>
                                    ) : null
                                    } 
                                </div>
                            </div>
                        </div>
                      </BlockUi>
                     </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
          </React.Fragment> );
    }
}
 
export default EditDistributorResources;