import React, { Component } from "react";
import Adminheader from "./common/adminHeader";
import Adminsidebar from "./common/adminSidebar";
import AdminGraph from "./adminGraph";
import * as manageUserService from "../ApiServices/admin/manageUser";
import { CURRENCY_SYMBOL } from "../../src/config.json";
class AdminDashboard extends Component {
  state = {
    customers: 0,
    CurrentMonthCount: 0,
    LastMonthCount: 0,
    graphvalues: {},
    MonthlyRevenue: 0,
    DevliveryTotalCount: 0,
  };

  componentDidMount = () => {
    this.getAllusers();
  };

  getAllusers = async () => {
    this.setState({ spinner: true });
    const response = await manageUserService.getDashboardDetails();

    if (response.data.status == 1) {
      this.setState({
        UserCount: response.data.data.UserCount,
        AdminCount: response.data.data.AdminCount,
        OrderCount: response.data.data.OrderCount,
        LastMonthCount: response.data.data.LastMonthCount,
        CurrentMonthCount: response.data.data.CurrentMonthCount,
        graphvalues: response.data.data,
        LastMonthOrderCount: response.data.data.LastMonthOrderCount,
        MonthlyRevenue: response.data.data.MonthlyRevenue,
        DevliveryLastmonthCount: response.data.data.DevliveryLastmonthCount,
        DevliveryTotalCount: response.data.data.DevliveryTotalCount,
      });
    }
  };

  render() {
    const images = require.context("../assets/images", true);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4">
                  <div className="row">
                    <div className="col-xl-8 col-lg-12 col-md-12 col-sm-12 home-box-left">
                      <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 single-box-wrap">
                          <div className="card single-box-inner">
                            <div className="card-body text-center">
                              <div className="img-wrap  ">
                                <img
                                  src={images(`./frontend/customers.png`)}
                                  className="img-fluid "
                                  alt="customer"
                                ></img>
                              </div>
                              <div className="box-name text-xs text-uppercase  pt-3 pb-3 text-white">
                                CUSTOMERS
                              </div>
                              <div className="tot-count font-weight-bold text-uppercase   text-white">
                                {this.state.UserCount}
                              </div>

                              <div className="count-rate  pt-3  text-white">
                                Increased by{" "}
                                {this.state.CurrentMonthCount
                                  ? this.state.UserCount
                                    ? (
                                        Number(
                                          (
                                            this.state.CurrentMonthCount /
                                            this.state.UserCount
                                          ).toFixed(2)
                                        ) * 100
                                      ).toFixed(0)
                                    : 0
                                  : 0}
                                %
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3  single-box-wrap">
                          <div className="card single-box-inner">
                            <div className="card-body text-center">
                              <div className="img-wrap ">
                                <img
                                  src={images(`./frontend/orders.png`)}
                                  className="img-fluid "
                                  alt="orders"
                                ></img>
                              </div>
                              <div className="box-name text-xs text-uppercase  pt-3 pb-3 text-white">
                                ORDERS
                              </div>
                              <div className="tot-count font-weight-bold text-uppercase   text-white">
                                {this.state.OrderCount}
                              </div>

                              <div className="count-rate  pt-3  text-white">
                                Increased by{" "}
                                {this.state.LastMonthOrderCount
                                  ? this.state.OrderCount
                                    ? Number(
                                        (
                                          this.state.LastMonthOrderCount /
                                          this.state.OrderCount
                                        ).toFixed(2)
                                      ) * 100
                                    : 0
                                  : 0}
                                %
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3  single-box-wrap">
                          <div className="card single-box-inner">
                            <div className="card-body text-center">
                              <div className="img-wrap ">
                                <img
                                  src={images(`./frontend/delivery.png`)}
                                  className="img-fluid  "
                                  alt="delivery"
                                ></img>
                              </div>
                              <div className="box-name text-xs text-uppercase  pt-3 pb-3 text-white">
                                DELIVERY
                              </div>
                              <div className="tot-count font-weight-bold text-uppercase   text-white">
                                {this.state.DevliveryTotalCount}
                              </div>
                              <div className="count-rate  pt-3  text-white">
                                Increased by{" "}
                                {this.state.DevliveryLastmonthCount
                                  ? this.state.DevliveryTotalCount
                                    ? Number(
                                        (
                                          this.state.DevliveryLastmonthCount /
                                          this.state.DevliveryTotalCount
                                        ).toFixed(2)
                                      ).toFixed(0) * 100
                                    : 0
                                  : 0}
                                %
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3  single-box-wrap">
                          <div className="card single-box-inner">
                            <div className="card-body text-center">
                              <div className="img-wrap ">
                                <img
                                  src={images(`./frontend/users.png`)}
                                  className="img-fluid  "
                                  alt="users"
                                ></img>
                              </div>
                              <div className="box-name text-xs text-uppercase  pt-3 pb-3 text-white">
                                USERS
                              </div>
                              <div className="tot-count font-weight-bold text-uppercase   text-white">
                                {this.state.UserCount?Number(this.state.UserCount + this.state.AdminCount):0 }
                              </div>
                              <div className="count-rate  pt-3  text-white">
                                Increased by{" "}
                                {this.state.CurrentMonthCount
                                  ? this.state.UserCount
                                    ? (
                                        Number(
                                          (
                                            this.state.CurrentMonthCount /
                                            this.state.UserCount
                                          ).toFixed(2)
                                        ) * 100
                                      ).toFixed(0)
                                    : 0
                                  : 0}
                                %
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-4 col-lg-12 col-md-12 col-sm-12  home-box-right">
                      <div className="row invoice-wrap">
                        <div className="col-md-12 invoice-head ">
                          <div>Invoices</div>
                        </div>
                        <div className="col-md-12 invoice-content bg-white   text-center">
                          <div className="row">
                            <div className="col-xs-12 col-sm-6 col-md-6 single">
                              <div className="due-name">DUE</div>
                              <div className="due-rate">$1,890</div>
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-6 single">
                              <div className="due-name">OVERDUE</div>
                              <div className="due-rate">$1,890</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row monthly-revenue-wrap text-white">
                        <div className="col-md-12 revenue-head ">
                          <div>Monthly Revenue</div>
                        </div>
                        <div className="col-md-12 revenue-content ">
                          <div className="row ">
                            <div className="col-xs-12 col-sm-6 col-md-6 single ">
                              <div className="rev-name">GROWTH</div>
                              <div className="rev-rate">
                                {CURRENCY_SYMBOL +
                                  Number(this.state.MonthlyRevenue).toFixed(0)}
                              </div>
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-6 single">
                              <img
                                src={images(`./frontend/revenue.png`)}
                                className="img-fluid  "
                              ></img>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <AdminGraph props={this.state.graphvalues} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminDashboard;
