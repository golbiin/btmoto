import React, { Component } from "react";
import Adminheader from "./../common/adminHeader";
import Adminsidebar from "./../common/adminSidebar";
import {
  ReactFormBuilder,
  ReactFormGenerator,
  ElementStore
} from "react-form-builder2";
import "react-form-builder2/dist/app.css";
import * as cformService from "../../ApiServices/admin/cformService";
import ReactSpinner from "react-bootstrap-spinner";
import { apiUrl } from "./../../config.json";
class EditCform extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form_name: "",
      data: [],
      dataurl: "",
      errors: {},
      submit_status: false,
      message: "",
      message_type: "",
      previewVisible: false,
      shortPreviewVisible: false,
      roPreviewVisible: false
    };

    const update = this._onChange.bind(this);
    ElementStore.subscribe(state => update(state.data));
  }

  /* Get single form */
  componentDidMount = async () => {
    this.setState({ spinner: true });
    const id = this.props.match.params.id;
    const response = await cformService.getSingleCForm(id);
    if (response) {
      if (response.data.status === 1) {
        console.log("Form details: ", response.data.data);
        this.setState({
          data: response.data.data.content,
          form_name: response.data.data.form_name,
          dataurl: apiUrl + "/admin/cforms/content?form_id=" + id
          //   message: response.data.message,
          //   message_type: "alert alert-success alert-dismissible fade show"
        });
      } else {
        this.setState({
          message: response.data.message,
          message_type: "alert alert-danger alert-dismissible fade show"
        });
      }
    }
  };

  /* Input Handle Change */
  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    if (input.value === "") {
      errors[input.name] = "Please add form name.";
      this.setState({ errors });
      return false;
    }
    this.setState({ form_name: input.value });
  };

  savePreview = async () => {
    if (this.state.form_name === "") {
      this.setState({
        message: "Please add form name.",
        message_type: "text-danger"
      });
      return false;
    }

    if (this.state.data.length === 0) {
      this.setState({
        message: "Please add element for form",
        message_type: "text-danger"
      });
      return false;
    }

    const cform_data = {
      _id: this.props.match.params.id,
      form_name: this.state.form_name,
      content: this.state.data
    };

    this.setState({ submit_status: true, message: "" });

    const response = await cformService.editCForms(cform_data);
    //console.log('Cform response: ', response)
    if (response) {
      this.setState({ submit_status: false });
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "text-success"
        });
      } else {
        this.setState({
          message: response.data.message,
          message_type: "text-danger"
        });
      }
    }
  };

  showPreview() {
    this.setState({
      previewVisible: true
    });
  }

  showShortPreview() {
    this.setState({
      shortPreviewVisible: true
    });
  }

  showRoPreview() {
    this.setState({
      roPreviewVisible: true
    });
  }

  closePreview() {
    this.setState({
      previewVisible: false,
      shortPreviewVisible: false,
      roPreviewVisible: false
    });
  }

  _onChange(data) {
    this.setState({
      data
    });
  }

  render() {
    let modalClass = "modal";
    if (this.state.previewVisible) {
      modalClass += " show d-block";
    }

    let shortModalClass = "modal short-modal";
    if (this.state.shortPreviewVisible) {
      shortModalClass += " show d-block";
    }

    let roModalClass = "modal ro-modal";
    if (this.state.roPreviewVisible) {
      roModalClass += " show d-block";
    }

    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
              </div>

              <div
                className="editContent"
                style={{ fontFamily: "Avenir Roman" }}
              >
                <div className="row">
                  <div className="col-md-12 pt-4">
                    <h4 className="float-left">Edit Form</h4>
                  </div>
                  {this.state.message !== "" ? (
                    <div className="col-md-12">
                      <div role="alert" className={this.state.message_type}>
                        {this.state.message}
                      </div>
                    </div>
                  ) : (
                    ""
                  )}

                  <div className="col-md-12">
                    <button
                      className="btn btn-success float-right"
                      disabled={
                        this.state.submit_status === true ? "disabled" : false
                      }
                      onClick={this.savePreview.bind(this)}
                    >
                      {this.state.submit_status ? (
                        <ReactSpinner type="border" color="dark" size="1" />
                      ) : (
                        ""
                      )}
                      Update Form
                    </button>

                    <button
                      className="btn btn-primary float-right"
                      style={{ marginRight: "10px" }}
                      onClick={this.showPreview.bind(this)}
                    >
                      Preview Form
                    </button>
                  </div>
                  <div className="col-md-12 mb-4">
                    <div className="form-group">
                      <label htmlFor="">Form Name</label>
                      <input
                        name="title"
                        value={this.state.form_name}
                        onChange={this.handleChange}
                        type="text"
                        placeholder="Add cimon form name"
                        className="form-control"
                      />
                      {this.state.errors.title ? (
                        <div className="error text-danger">
                          {this.state.errors.form_name}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                  <div className="col-12">
                    {this.state.data.length > 0 ? (
                      <ReactFormBuilder url={this.state.dataurl} />
                    ) : (
                      ""
                    )}
                  </div>
                </div>
                <div
                  className="clearfix mt-4"
                  style={{ margin: "10px", width: "70%" }}
                >
                  {this.state.previewVisible && (
                    <>
                      <div class="modal-backdrop fade show"></div>
                      <div className={modalClass}>
                        <div className="modal-dialog modal-lg">
                          <div
                            className="modal-content p-3"
                            style={{ background: "#f7f7f7" }}
                          >
                            <ReactFormGenerator
                              download_path=""
                              back_action=""
                              back_name="Back"
                              answer_data={{}}
                              action_name="Save"
                              form_action=""
                              form_method="POST"
                              data={this.state.data}
                              hide_actions={true}
                            />

                            {/* <ViewCform id={this.props.match.params.id} /> */}

                            <div className="modal-footer">
                              <button
                                type="button"
                                className="btn btn-default"
                                data-dismiss="modal"
                                onClick={this.closePreview.bind(this)}
                              >
                                Close
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default EditCform;
