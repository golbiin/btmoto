import React from "react";
import { ReactFormGenerator, ElementStore } from "react-form-builder2";
import * as cformService from "../../ApiServices/admin/cformService";
import BlockUi from "react-block-ui";
import ReactSpinner from "react-bootstrap-spinner";

export default class BuilderPreview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form_name: "",
      data: [],
      errors: {},
      submit_status: false,
      message: "",
      message_type: "",
      previewVisible: false,
      shortPreviewVisible: false,
      roPreviewVisible: false
    };

    const update = this._onChange.bind(this);
    ElementStore.subscribe(state => update(state.data));
  }

  /* Input Handle Change */
  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    if (input.value === "") {
      errors[input.name] = "Please add form name.";
      this.setState({ errors });
      return false;
    }
    this.setState({ form_name: input.value });
  };

  savePreview = async () => {
    this.setState({ message: "", message_type: "" });

    if (this.state.form_name === "") {
      this.setState({
        message: "Please add form name.",
        message_type: "text-danger"
      });
      return false;
    }

    if (this.state.data.length === 0) {
      this.setState({
        message: "Please add elements for form.",
        message_type: "text-danger"
      });
      return false;
    }

    this.setState({ submit_status: true });

    const cform_data = {
      form_name: this.state.form_name,
      content: this.state.data
    };

    const response = await cformService.createCForms(cform_data);
    //console.log('Cform response: ', response)
    if (response) {
      this.setState({ submit_status: false });
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "text-success"
        });

        // this.props.history.push({
        //   pathname: "/admin/list-cforms",
        // });
      } else {
        this.setState({
          message: response.data.message,
          message_type: "text-danger"
        });
      }
    }
  };

  showPreview() {
    this.setState({
      previewVisible: true
    });
  }

  showShortPreview() {
    this.setState({
      shortPreviewVisible: true
    });
  }

  showRoPreview() {
    this.setState({
      roPreviewVisible: true
    });
  }

  closePreview() {
    this.setState({
      previewVisible: false,
      shortPreviewVisible: false,
      roPreviewVisible: false
    });
  }

  _onChange(data) {
    this.setState({
      data
    });
  }

  render() {
    let modalClass = "modal";
    if (this.state.previewVisible) {
      modalClass += " show d-block";
    }

    let shortModalClass = "modal short-modal";
    if (this.state.shortPreviewVisible) {
      shortModalClass += " show d-block";
    }

    let roModalClass = "modal ro-modal";
    if (this.state.roPreviewVisible) {
      roModalClass += " show d-block";
    }

    return (
      <>
        <div className="row">
          <div className="col-md-12 pt-4">
            <h4 className="float-left">Create a Form</h4>
          </div>
          {this.state.message !== "" ? (
            <div className="col-md-12">
              <div role="alert" className={this.state.message_type}>
                {this.state.message}
              </div>
            </div>
          ) : (
            ""
          )}

          <div className="col-md-12">
            <button
              className="btn btn-success float-right"
              disabled={this.state.submit_status === true ? "disabled" : false}
              onClick={this.savePreview.bind(this)}
            >
              {this.state.submit_status ? (
                <ReactSpinner type="border" color="dark" size="1" />
              ) : (
                ""
              )}
              Save Form
            </button>
            {this.state.data.length > 0 ? (
              <button
                className="btn btn-primary float-right"
                style={{ marginRight: "10px" }}
                onClick={this.showPreview.bind(this)}
              >
                Preview Form
              </button>
            ) : (
              ""
            )}
          </div>
          <div className="col-md-12">
            <div className="form-group">
              <label htmlFor="">Form Name</label>
              <input
                name="title"
                onChange={this.handleChange}
                type="text"
                placeholder="Enter form name"
                className="form-control"
              />
              {this.state.errors.title ? (
                <div className="error text-danger">
                  {this.state.errors.form_name}
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
        <div className="clearfix mt-4" style={{ margin: "10px", width: "70%" }}>
          {this.state.previewVisible && (
            <>
              <div class="modal-backdrop fade show"></div>
              <div className={modalClass}>
                <div className="modal-dialog modal-lg">
                  <div
                    className="modal-content p-3"
                    style={{ background: "#f7f7f7" }}
                  >
                    <ReactFormGenerator
                      download_path=""
                      back_action=""
                      back_name="Back"
                      answer_data={{}}
                      action_name="Save"
                      form_action=""
                      form_method="POST"
                      hide_actions={true}
                      data={this.state.data}
                    />

                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-default"
                        data-dismiss="modal"
                        onClick={this.closePreview.bind(this)}
                      >
                        Close
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </>
    );
  }
}
