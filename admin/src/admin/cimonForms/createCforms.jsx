import React, { Component } from "react";
import Adminheader from "./../common/adminHeader";
import Adminsidebar from "./../common/adminSidebar";
import { ReactFormBuilder, ReactFormGenerator  } from 'react-form-builder2';
import 'react-form-builder2/dist/app.css';
import BuilderPreview from "./builderPreview";
class CreateCforms extends Component {
  state = {};

  componentDidMount = () => {
    
  };


  render() {
  

    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12" style={{ fontFamily: "Avenir Roman" }}>
                <BuilderPreview />
                <ReactFormBuilder />
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CreateCforms;
