import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import AdminGraph from '../adminGraph'
import { Line, Bar } from 'react-chartjs-2'
import * as Settingservice from '../../ApiServices/admin/settings'
import { CURRENCY_SYMBOL } from '../../config.json'
class AdminReport extends Component {
  state = {
    activetab: 'tab_a',
    order_cnt : 0,
    sales_static: {
      labels: [
        'Jan',
        'Feb',
        'March',
        'April',
        'May',
        'June',
        'July',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
      ],
      datasets: [
        {
          label: new Date().getFullYear() + ' gross sales in this period',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(177,212,234,0.5)',
          borderColor: '#b1d4ea',
          borderCapStyle: 'butt',
          pointBackgroundColor: '#b1d4ea',
          data: [100, 200, 300,100, 100, 200, 100, 300, 400, 300, 500, 50]
        },
        {
          label: new Date().getFullYear() - 1 + ' net sales in this period',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(52,152,219,0.5)',
          borderColor: '#3498db',
          borderCapStyle: 'butt',
          pointBackgroundColor: '#3498db',
          data: [150, 100, 50, 0, 250, 0, 0, 0, 0, 0, 0, 0]
        },
        {
          label: 'Order',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(219,225,227,0.5)',
          borderColor: '#dbe1e3',
          borderCapStyle: 'butt',
          pointBackgroundColor: '#dbe1e3',
          data: [50, 100, 20, 0, 250, 0, 0, 70, 0, 0, 0, 0]
        },
        {
          label: new Date().getFullYear() - 1 + ' Delivery',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(219,225,227,0.5)',
          borderColor: '#dbe1e3',
          borderCapStyle: 'butt',
          pointBackgroundColor: '#dbe1e3',
          data: [50, 100, 20, 0, 250, 0, 0, 70, 0, 0, 0, 0]
        },
       
        {
          label: new Date().getFullYear() - 1 + ' Refund',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(231,76,60,0.5)',
          borderColor: '#e74c3c',
          borderCapStyle: 'butt',
          pointBackgroundColor: '#e74c3c',
          data: [50, 100, 0, 0, 250, 0, 0, 0, 0, 8, 80, 0]
        },
        {
          label: new Date().getFullYear() - 1 + ' Shipping',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(92,196,136,0.5)',
          borderColor: '#5cc488',
          borderCapStyle: 'butt',
          pointBackgroundColor: '#5cc488',
          data: [50, 100, 0, 0, 250, 0, 50, 0, 50, 0, 60, 0]
        }
      ]
    },

    tabData: [
      { name: 'Tab 1', isActive: true },
      { name: 'Tab 2', isActive: false },
      { name: 'Tab 3', isActive: false }
    ]
  }
  componentDidMount = () => {
    this.getReport();
  }
  tabActive = async tab => {
    this.setState({ activetab: tab })
    this.getReport();
  };

  getReport = async () => {
    try {
      const response = await Settingservice.getReport();
        if (response.data.status == 1) {
          this.setState({ order_cnt: response.data.data.LastMonthOrderCount })
          const sales_static = { ...this.state.sales_static };
           sales_static["datasets"][2] ["data"] = response.data.data.OrderGraphdata;
          this.setState({ sales_static  : sales_static  
          });
        }
      } catch (err) {
      }
  };

  render () {
    const images = require.context('../../assets/images', true)
    return (
      <React.Fragment>
        <div className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents  adminreport pt-4 pb-4 pr-4'>
                  <div className='row'>
                    <div className='col-xs-12 col-lg-12 col-md-12 col-sm-12 home-box-left'>
                      <div className='row'>
                        <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                          <div className='adminreport_inner'>
                            <ul className='nav nav-pills nav-fill'>
                              <li
                                className={
                                  this.state.activetab === 'tab_a'
                                    ? 'active'
                                    : ''
                                }
                              >
                                <a
                                  href='#tab_a'
                                  data-toggle='pill'
                                  onClick={() => this.tabActive('tab_a')}
                                >
                                  Orders
                                </a>
                              </li>
                              <li
                                className={
                                  this.state.activetab === 'tab_b'
                                    ? 'active'
                                    : ''
                                }
                              >
                                <a
                                  href='#tab_b'
                                  data-toggle='pill'
                                  onClick={() => this.tabActive('tab_b')}
                                >
                                  Customers
                                </a>
                              </li>
                            </ul>
                            <div class='tab-content'>
                              {/* Tab 1 */}
                              <div className='tab-pane active' id='tab_a'>
                                <div className='edit-details-head pb-4'>
                                  <ul className='nav nav-pills nav-fill'>
                                    <li
                                      className={
                                        this.state.activetab === 'tab_c'
                                          ? 'active'
                                          : ''
                                      }
                                    >
                                      <a
                                        href='#tab_c'
                                        data-toggle='pill'
                                        onClick={() => this.tabActive('tab_c')}
                                      >
                                        Year
                                      </a>
                                    </li>
                                    <li
                                      className={
                                        this.state.activetab === 'tab_d'
                                          ? 'active'
                                          : ''
                                      }
                                    >
                                      <a
                                        href='#tab_d'
                                        data-toggle='pill'
                                        onClick={() => this.tabActive('tab_d')}
                                      >
                                        Last month
                                      </a>

                                      <a
                                        href='#tab_e'
                                        data-toggle='pill'
                                        onClick={() => this.tabActive('tab_e')}
                                      >
                                        This month
                                      </a>

                                      <a
                                        href='#tab_e'
                                        data-toggle='pill'
                                        onClick={() => this.tabActive('tab_e')}
                                      >
                                        Last 7 days
                                      </a>
                                    </li>
                                  </ul>

                                  <div className='row common_row'>
                                    <div className='col-xs-12 col-lg-3 col-md-3 col-sm-3 home-box-left'>
                                      <div class='chart-sidebar'>
                                        <ul class='chart-legend'>
                                          <li
                                            style={{
                                              'border-color': '#b1d4ea'
                                            }}
                                            class='highlight_series tips'
                                            data-series='6'
                                          >
                                            <strong>
                                              <span class='Price-amount amount'>
                                                <span class='Price-currencySymbol'>
                                                  $
                                                </span>
                                                0.00
                                              </span>
                                            </strong>
                                            gross sales in this period
                                          </li>
                                          <li
                                            style={{
                                              'border-color': '#3498db'
                                            }}
                                            class='highlight_series tips'
                                            data-series='7'
                                          >
                                            <strong>
                                              <span class='Price-amount amount'>
                                                <span class='Price-currencySymbol'>
                                                  $
                                                </span>
                                                0.00
                                              </span>
                                            </strong>
                                            net sales in this period
                                          </li>
                                          <li
                                            style={{
                                              'border-color': '#dbe1e3'
                                            }}
                                            class='highlight_series '
                                            data-series='1'
                                            data-tip=''
                                          >
                                            <strong>{this.state.order_cnt}</strong> orders placed
                                          </li>
                                          <li
                                            style={{
                                              'border-color': '#ecf0f1'
                                            }}
                                            class='highlight_series '
                                            data-series='0'
                                            data-tip=''
                                          >
                                            <strong>0</strong> items purchased
                                          </li>
                                          <li
                                            style={{
                                              'border-color': '#e74c3c'
                                            }}
                                            class='highlight_series '
                                            data-series='8'
                                            data-tip=''
                                          >
                                            <strong>
                                              <span class='Price-amount amount'>
                                                <span class='Price-currencySymbol'>
                                                  $
                                                </span>
                                                0.00
                                              </span>
                                            </strong>
                                            refunded 0 orders (0 items)
                                          </li>
                                          <li
                                            style={{
                                              'border-color': '#5cc488'
                                            }}
                                            class='highlight_series '
                                            data-series='5'
                                            data-tip=''
                                          >
                                            <strong>
                                              <span class='Price-amount amount'>
                                                <span class='Price-currencySymbol'>
                                                  $
                                                </span>
                                                0.00
                                              </span>
                                            </strong>
                                            charged for shipping
                                          </li>
                                          <li
                                            style={{
                                              'border-color': '#f1c40f'
                                            }}
                                            class='highlight_series '
                                            data-series='4'
                                            data-tip=''
                                          >
                                            <strong>
                                              <span class='Price-amount amount'>
                                                <span class='Price-currencySymbol'>
                                                  $
                                                </span>
                                                0.00
                                              </span>
                                            </strong>
                                            worth of coupons used
                                          </li>
                                        </ul>
                                        <ul class='chart-widgets' />
                                      </div>
                                    </div>
                                    <div className='col-xs-12 col-lg-9 col-md-9 col-sm-9 home-box-right'>
                                      <div className='row'>
                                        <Line
                                          data={this.state.sales_static}
                                          options={{
                                            title: {
                                              display: false,
                                              fontSize: 20
                                            },
                                            legend: {
                                              display: true,
                                              position: 'top',
                                              labels: {
                                                usePointStyle: true,
                                                boxWidth: 16
                                              }
                                            },
                                            scales: {
                                              xAxes: [
                                                {
                                                  gridLines: {
                                                    display: false
                                                  }
                                                }
                                              ]
                                            }
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              {/* End Tab 1 */}

                              {/* Tab 2 */}
                              <div className='tab-pane' id='tab_b'>
                                <div className='tab-icon' />
                                <div className='edit-details-head pb-4'>
                                  <Line
                                    data={this.state.sales_static}
                                    options={{
                                      title: {
                                        display: false,
                                        fontSize: 20
                                      },
                                      legend: {
                                        display: false,
                                        position: 'top',
                                        labels: {
                                          usePointStyle: true,
                                          boxWidth: 16
                                        }
                                      },
                                      scales: {
                                        xAxes: [
                                          {
                                            gridLines: {
                                              display: false
                                            }
                                          }
                                        ]
                                      }
                                    }}
                                  />
                                </div>
                              </div>
                              {/* End Tab 2 */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default AdminReport
