import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import { Link } from 'react-router-dom'
import validate from 'react-joi-validation'
import Joi, { join } from 'joi-browser'
import * as AttributeService from '../../ApiServices/admin/attributeService'
import ReactSpinner from 'react-bootstrap-spinner'
import MultiSelect from 'react-multi-select-component'
class EditCateogry extends Component {
  constructor (props) {
    super(props)
    this.UpdateEditorContent = this.UpdateEditorContent.bind(this)
    this.onEditorChange = this.onEditorChange.bind(this)
    this.onEditorChange2 = this.onEditorChange2.bind(this)
  }
  state = {
    data: { userName: '', password: '' },
    errors: {},
    parent_category: 0,
    submit_status: false,
    name: '',
    parentcategories: [],
    slug: '',
    descrption: '',
    short_description: '',
    spinner: true,
    content: [],
    page_type: '',
    page_layout: '',
    profile_image: '',
    upload_data: '',
    page_id_name: [],
    url: '',
    banner_sub_description: '',
    banner_main_description: '',
    manualUrl: '',
    downloadUrl: '',
    related_cat_options: [],
    related_cat_selected: [],
    related_cat: []
  }

  onEditorChange (evt) {
    this.setState({
      descrption: evt.editor.getData()
    })
  }

  onEditorChange2 (evt) {
    this.setState({
      short_description: evt.editor.getData()
    })
  }

  /*****************INPUT HANDLE CHANGE **************/

  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors }
    const data = { ...this.state.data }
    const errorMessage = this.validateProperty(input)

    if (errorMessage) errors[input.name] = errorMessage
    else delete errors[input.name]
    data[input.name] = input.value
    this.setState({ data, errors })
  }

  validateProperty = ({ name, value }) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)
    return error ? error.details[0].message : null
  }

  /*Select Handle Change*/
  handleParentCatChange = async e => {

  
    this.setState({
      parent_category: e.target.value
    })

    if(e.target.value == '0'){
      this.setState({
        related_cat_selected: []
      })
      
    }
    
  }
  handletextareaChange = async e => {
    this.setState({
      short_description: e.target.value
    })
  }

  componentDidMount = () => {
    this.getAllParentAttributes()
    setTimeout(() => {
      this.getAttributeBySlug()
      this.getAllCategory()
    }, 2000)
  }

  /* Get all catogeries */
  getAllCategory = async () => {
    this.setState({ spinner: true })
    try {
      const response = await AttributeService.getAllCategory()
      if (response.data.status == 1) {
        const data_related_row = []
        const data_selected_row = []

        response.data.data.map((Cats, index) => {
          const Setdata = {}
          Setdata.value = Cats._id
          Setdata.label = Cats.category_name

          if (this.state.related_cat) {
            if (this.state.related_cat.indexOf(Cats._id) > -1) {
              data_selected_row.push(Setdata)
            }
          }

          data_related_row.push(Setdata)
        })
        console.log('related_cat', data_selected_row, this.state.related_cat)
        this.setState({ related_cat_options: data_related_row })
        this.setState({ related_cat_selected: data_selected_row })
      }

      this.setState({ spinner: false })
    } catch (err) {
      this.setState({ spinner: false })
    }
  }

  /* Get all Parent Category */
  getAllParentAttributes = async () => {
    const response = await AttributeService.getAllParentAttributes()
    if (response.data.status == 1) {
      this.setState({
        parentcategories: response.data.data
      })
    }
  }

  /* Get Attribute by slug */
  getAttributeBySlug = async () => {
    const category_slug = this.props.match.params.slug
    const response = await AttributeService.getAttributeBySlug({
      slug: category_slug
    })
    if (response.data.status == 1) {
      this.setState({
        submit_status: false,
        message: '',
        message_type: '',
        delete_status: false,
        name: response.data.data.name,
        slug: response.data.data.slug,
        parent_category: response.data.data.parent_id,
        _id: response.data.data._id,
        related_cat: response.data.data.category,
        ordernumber: response.data.data.order
      })

      this.setState({ spinner: false })
    } else {
      this.props.history.push({
        pathname: '/admin/settings/manage-categories'
      })
      this.setState({ spinner: false })
    }
  }

  /**Joi validation schema*/
  schema = {
    name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Category name. It is not allowed to be empty.'
        }
      }),
    slug: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Slug. It is not allowed to be empty.'
        }
      }),
      ordernumber :Joi.number()
      .required()
      .error(() => {
        return {
          message: "This field is a required field and must be a number."
        };
    })
  }

  validateProperty = (name, value) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)
    return error ? error.details[0].message : null
  }

  handleChange = input => event => {
    const errors = { ...this.state.errors }
    delete errors.validate
    const errorMessage = this.validateProperty(input, event.target.value)
    if (errorMessage) errors[input] = errorMessage
    else delete errors[input]
    this.setState({ [input]: event.target.value, errors })
  }

  /* Update React Editor Content */
  UpdateEditorContent = data => {
    // set on Parent  React editor content
    const updateddata = data
    this.setState({ content: updateddata })
  }

  /*From submit  */
  saveCategories = async e => {
    e.preventDefault()
    const errors = { ...this.state.errors }
    const checkState = {
      name: this.state.name,
      slug: this.state.slug,
      ordernumber : this.state.ordernumber
    }
    let result = Joi.validate(checkState, this.schema)
    if (!result.error) {
      try {
        this.setState({
          submit_status: true,
          message: '',
          message_type: ''
        })
        this.setState({ spinner: true })
        this.updateCatData()
      } catch (err) {
        this.setState({ activepage: false })
        this.setState({ submit_status: false })
        this.setState({
          message: 'Something went wrong! Please try after some time',
          message_type: 'error'
        })
      }
    }
  }
  /*   handle Update Cateogry  */
  updateCatData = async () => {
    const data_selected_row = []
    this.state.related_cat_selected.map((Cats, index) => {
      data_selected_row.push(Cats.value)
    })

    const category_data = {
      name: this.state.name,
      _id: this.state._id,
      parent_category: this.state.parent_category,
      category: data_selected_row,
      slug: this.state.slug,
      ordernumber : this.state.ordernumber
    }

    try {
      const response = await AttributeService.updateAttribute(category_data)

      if (response) {
        this.setState({ submit_status: false })
        if (response.data.status === 1) {
          this.setState({
            message: response.data.message,
            message_type: 'success'
          })

          setTimeout(
            () =>
              this.props.history.push({
                pathname: '/admin/settings/manage-attributes'
              }),
            2000
          )
          this.setState({ spinner: false })
        } else {
          this.setState({ spinner: false })
          this.setState({
            message: response.data.message,
            message_type: 'error'
          })
        }
      }
    } catch (err) {
      this.setState({
        message: 'something Went wrong!',
        message_type: 'error'
      })
    }
  }

  setcatSelected = event => {
    const data_selected_row = []
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value
    })

    this.setState({ related_cat: data_selected_row })
    this.setState({ related_cat_selected: event })
  }

  render () {
    const feteched = this.state.content
    return (
      <React.Fragment>
        <div className='container-fluid admin-body '>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>

            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents  useradd-new pt-4 pb-4 pr-4'>
                  <React.Fragment>
                    <div
                      style={{
                        display: this.state.spinner === true ? 'flex' : 'none'
                      }}
                      className='overlay'
                    >
                      <ReactSpinner type='border' color='primary' size='10' />
                    </div>
                  </React.Fragment>
                  <React.Fragment>
                    <div className='card'>
                      <div className='card-header'>Edit Attribute</div>
                      <div className='card-body'>
                        <p className='tableinformation'></p>
                        <div className='form-group'>
                          <label>Attribute Name *</label>
                          <input
                            value={this.state.name}
                            type='text'
                            placeholder='Attribute Name'
                            onChange={this.handleChange('name')}
                            className='form-control'
                          />
                          {this.state.errors.name ? (
                            <div className='danger'>
                              {this.state.errors.name}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>
                        <div className='form-group'>
                          <label>Attribute Slug *</label>
                          <input
                            
                            value={this.state.slug}
                            onChange={this.handleChange('slug')}
                            type='text'
                            placeholder='Attribute Slug'
                            className='form-control'
                          />
                          {this.state.errors.slug ? (
                            <div className='danger'>
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>

                        <div className='form-group'>
                          <label>Parent Attribute</label>
                          <select
                            className='form-control'
                            value={this.state.parent_category}
                            onChange={this.handleParentCatChange}
                          >
                            <option value='0'>None</option>
                            {this.state.parentcategories.map(
                              (cat_parent, idx) => (
                                <option
                                  key={cat_parent.name}
                                  value={cat_parent._id}
                                >
                                  {cat_parent.name}
                                </option>
                              )
                            )}
                          </select>
                        </div>

                      
                          <div className='form-group'>
                            <div className='row'>
                              <div className='col-md-6'>
                                <label>Select Category</label>

                                <MultiSelect
                                  options={this.state.related_cat_options}
                                  value={this.state.related_cat_selected}
                                  onChange={this.setcatSelected} //  labelledBy={"Select"}
                                />
                              </div>
                            </div>
                          </div>
                        



                          <div className="form-group">
                                <label>Attribute Order</label>
                                <input
                                
                                  className="form-control"
                                  type="number"
                                  value={this.state.ordernumber}
                                  onChange={this.handleChange('ordernumber')}
                                />
                                {this.state.errors.ordernumber ? (<div className="danger">{this.state.errors.ordernumber}</div>) : ("")}
                              </div>



                      </div>
                    </div>

                    <div className='submit'>
                      <button
                        className='submit-btn'
                        disabled={
                          this.state.submit_status === true ? 'disabled' : false
                        }
                        onClick={this.saveCategories}
                      >
                        {this.state.submit_status === true ? (
                          <React.Fragment>
                            <ReactSpinner
                              type='border'
                              color='primary'
                              size='1'
                            />
                          </React.Fragment>
                        ) : (
                          'Save'
                        )}
                      </button>
                      <Link
                        disabled={
                          this.state.submit_status === true ? 'disabled' : false
                        }
                        to='/admin/settings/manage-attributes'
                        className='cancel-btn'
                      >
                        Cancel
                      </Link>
                    </div>
                    {this.state.table_message !== '' ? (
                      <div className={this.state.message_type}>
                        <p>{this.state.message}</p>
                      </div>
                    ) : null}
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default EditCateogry
