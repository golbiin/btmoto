import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import ReactSpinner from 'react-bootstrap-spinner'
import Joi from 'joi-browser'
import * as AttributeService from '../../ApiServices/admin/attributeService'
import { Link } from 'react-router-dom'
import DataTable from 'react-data-table-component'

import MultiSelect from 'react-multi-select-component'
class ManageAttributes extends Component {
  constructor (props) {
    super(props)
    this.onEditorChange = this.onEditorChange.bind(this)
    this.onEditorChange2 = this.onEditorChange2.bind(this)
  }

  state = {
    name: '',
    descrption: '',
    parent_category: 0,
    slug: '',
    attributes: [],
    errors: {},
    submit_status: false,
    message: '',
    message_type: '',
    table_message: '',
    table_message_type: '',
    categories: [],
    parentcategories: [],
    delete_status: false,
    edit_status: false,
    data_table: [],
    spinner: true,
    page_type: '',
    page_layout: '',
    page_id_name: '',
    profile_image: '',
    upload_data: '',
    testdata: '',
    related_cat_options: [],
    related_cat_selected: [],
    columns: [
      {
        name: 'Attribute Name',
        sortable: false,
        cell: row => (
          <p className={'catogery_sec_loop parent_id_' + row.parent_id}>
            {row.name}{' '}
          </p>
        )
      },
      {
        name: 'Slug',
        selector: 'slug',
        sortable: true
      },

      {
        name: 'Order',
        selector: 'order',
        sortable: true
      },

      {
        name: 'Action',
        cell: row => (
          <div className='action_dropdown' data-id={row._id}>
            <i
              data-id={row._id}
              className='dropdown_dots'
              data-toggle='dropdown'
              aria-haspopup='true'
              aria-expanded='false'
            ></i>
            <div
              className='dropdown-menu dropdown-menu-center'
              aria-labelledby='dropdownMenuButton'
            >
              <Link
                to={'/admin/settings/manage-attributes/' + row._id}
                className='dropdown-item'
              >
                Edit Attribute
              </Link>
              <a
                onClick={() => this.handleRemoveAttribute(row._id)}
                className='dropdown-item'
              >
                Delete
              </a>
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: '56px' // custom width for icon button
      }
    ]
  }

  /* onchange ckeditor Description */
  onEditorChange (evt) {
    this.setState({
      descrption: evt.editor.getData()
    })
  }
  onEditorChange2 (evt) {
    this.setState({
      short_description: evt.editor.getData()
    })
  }

  componentDidMount = () => {
    this.getAllAttributes()
    this.getAllParentAttributes()
    this.getAllCategory()
  }

  /* Get all catogeries */
  getAllCategory = async () => {
    this.setState({ spinner: true })
    try {
      const response = await AttributeService.getAllCategory()
      if (response.data.status == 1) {
        const data_related_row = []
        const data_selected_row = []

        response.data.data.map((Cats, index) => {
          const Setdata = {}
          Setdata.value = Cats._id
          Setdata.label = Cats.category_name

          if (this.state.related_cat) {
            if (this.state.related_cat.indexOf(Cats._id) > -1) {
              data_selected_row.push(Setdata)
            }
          }

          data_related_row.push(Setdata)
        })
        console.log('related_cat', data_selected_row, this.state.related_cat)
        this.setState({ related_cat_options: data_related_row })
        this.setState({ related_cat_selected: data_selected_row })
      }

      this.setState({ spinner: false })
    } catch (err) {
      this.setState({ spinner: false })
    }
  }

  /* Get all catogeries */
  getAllAttributes = async () => {
    this.setState({ spinner: true })
    try {
      const response = await AttributeService.getAllAttributes()
      if (response.data.status == 1) {
        this.setState({ categories: response.data.data })
        const Setdata = { ...this.state.data_table }
        const data_table_row = []
        response.data.data.map((cat, index) => {
          const Setdata = {}
          Setdata.name = cat.name
          Setdata.slug = cat.slug
          Setdata.order = cat.order
          Setdata._id = cat._id
          Setdata.parent_id = cat.parent_id
          Setdata.descrption = cat.descrption
          Setdata.updatedata = '0'
          data_table_row.push(Setdata)
        })
        this.setState({
          data_table: data_table_row
        })
      }

      this.setState({ spinner: false })
    } catch (err) {
      this.setState({ spinner: false })
    }
  }

  /* Get all Parent Category  */
  getAllParentAttributes = async () => {
    try {
      const response = await AttributeService.getAllParentAttributes()
      if (response.data.status == 1) {
        this.setState({
          parentcategories: response.data.data
        })
      }
    } catch (err) {
      this.setState({
        message: 'Something went wrong!',
        responsetype: 'error'
      })
    }
  }

  /* initialize  Category fields    */
  addCategrory = () => {
    this.setState({
      name: '',
      parent_category: 0,
      slug: '',
      attributes: [],
      errors: {},
      submit_status: false,
      message: '',
      message_type: '',
      delete_status: false,
      edit_status: false
    })
  }

  /*Joi validation schema*/
  schema = {
    name: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is a required field.'
        }
      }),
    slug: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is a required field.'
        }
      }),
    attributes: Joi.optional().label('Attributes'),
    profile_image: Joi.allow(null),
    ordernumber: Joi.number()
      .required()
      .error(() => {
        return {
          message: 'This field is a required field and must be a number.'
        }
      })
  }

  validateProperty = (name, value) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)
    return error ? error.details[0].message : null
  }

  handleChange = input => event => {
    const errors = { ...this.state.errors }
    delete errors.validate
    const errorMessage = this.validateProperty(input, event.target.value)
    if (errorMessage) errors[input] = errorMessage
    else delete errors[input]
    this.setState({ [input]: event.target.value, errors })
  }

  /*   Set parent category on select change   */
  handleParentCatChange = async e => {
    this.setState({
      parent_category: e.target.value
    })

    if (e.target.value == '0') {
      this.setState({
        related_cat_selected: []
      })
    }
  }

  /*   handle page type and Pagelayout  */
  handlePageType = async e => {
    this.setState({
      page_type: e.target.value,
      page_layout: ''
    })
  }
  handlePageLayout = async e => {
    this.setState({
      page_layout: e.target.value
    })
  }

  /*   handle textarea change */
  handletextareaChange = async e => {
    this.setState({
      short_description: e.target.value
    })
  }

  /* Handle  add category */
  saveCategories = async e => {
    e.preventDefault()
    const checkState = {
      name: this.state.name,
      slug: this.state.slug,
      ordernumber: this.state.ordernumber
    }
    const errors = { ...this.state.errors }
    let result = Joi.validate(checkState, this.schema)
    if (result.error) {
      let path = result.error.details[0].path[0]
      let errormessage = result.error.details[0].message
      errors[path] = errormessage
      this.setState({ errors })
    } else {
      const category_data = {
        name: this.state.name,
        slug: this.state.slug,
        parent_category: this.state.parent_category
      }

      try {
        this.setState({
          submit_status: true,
          message: '',
          message_type: ''
        })

        if (this.state.edit_status === true) {
          this.setState({
            submitStatus: false,
            message: '',
            responsetype: 'error'
          })
        } else {
          this.addCatData()
        }
      } catch (err) {
        this.setState({
          submitStatus: false,
          message: 'Something went wrong! Please try after some time',
          responsetype: 'error'
        })
      }
    }
  }

  addCatData = async () => {
    // converting data pair to ids
    const data_selected_row = []
    this.state.related_cat_selected.map((Cats, index) => {
      data_selected_row.push(Cats.value)
    })

    const category_data = {
      name: this.state.name,
      slug: this.state.slug,
      parent_category: this.state.parent_category,
      category: data_selected_row,
      ordernumber: this.state.ordernumber
    }

    try {
      const response = await AttributeService.createAttributes(category_data)
      if (response) {
        this.setState({ submit_status: false })
        if (response.data.status === 1) {
          this.setState({
            message: response.data.message,
            message_type: 'success',
            name: '',
            slug: '',
            parent_category: 0,
            related_cat_selected: [],
            ordernumber: 0
          })
          this.getAllAttributes()
          this.getAllParentAttributes()
        } else {
          this.setState({
            message: response.data.message,
            message_type: 'error'
          })
        }
      } else {
        this.setState({ submit_status: false })
        this.setState({
          message: 'Something went wrong! Please try after some time',
          message_type: 'error'
        })
      }
    } catch (err) {
      this.setState({
        submitStatus: false,
        message: 'Something went wrong! Please try after some time',
        responsetype: 'error'
      })
    }
  }
  /* Handle remove category */
  handleRemoveAttribute = async id => {
    if (window.confirm(`Are you sure you’d like to delete this?`)) {
      this.setState({
        delete_status: true
      })
      try {
        const response = await AttributeService.removeAttributes({ _id: id })
        if (response.data.status === 1) {
          let newCategories = [...this.state.data_table]
          newCategories = newCategories.filter(function (obj) {
            return obj._id !== id
          })
          this.setState({
            table_message: response.data.message,
            table_message_type: 'sucess'
          })
          this.setState({
            data_table: newCategories,
            delete_status: false,
            edit_status: false
          })
          setTimeout(() => {
            this.setState(() => ({ table_message: '' }))
          }, 5000)
        } else {
          this.setState({
            table_message: response.data.message,
            table_message_type: 'sucess'
          })
          setTimeout(() => {
            this.setState(() => ({ table_message: '' }))
          }, 5000)
        }
      } catch (err) {
        this.setState({
          table_message: 'Something went wrong!',
          table_message_type: 'error'
        })
      }
    }
  }

  setcatSelected = event => {
    const data_selected_row = []
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value
    })

    this.setState({ related_cat: data_selected_row })
    this.setState({ related_cat_selected: event })
  }

  render () {
    console.log(' this.state.parent_category ', this.state.parent_category)
    let datarow = this.state.data_table
    return (
      <React.Fragment>
        <div className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12 content'>
              <div className='row content-row'>
                <div className='col-md-12 header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4'>
                  <React.Fragment>
                    <div className='categories-wrap p-4'>
                      <div className='row'>
                        <div className='col-md-4'>
                          <div className='card'>
                            <div className='card-header'>
                              <strong>
                                {this.state.edit_status === true
                                  ? 'Edit'
                                  : 'Add'}{' '}
                                Attribute
                              </strong>
                              {this.state.edit_status === true ? (
                                <button
                                  onClick={this.addCategrory}
                                  className='ml-4 add-cat-btn btn btn-success'
                                >
                                  + Add New
                                </button>
                              ) : (
                                ''
                              )}
                            </div>
                            <div className='card-body categories-add'>
                              <div className='form-group'>
                                <label>Enter Attribute Name*</label>
                                <input
                                  className='form-control'
                                  type='text'
                                  value={this.state.name}
                                  onChange={this.handleChange('name')}
                                />
                                {this.state.errors.name ? (
                                  <div className='danger'>
                                    {this.state.errors.name}
                                  </div>
                                ) : (
                                  ''
                                )}
                              </div>
                              <div className='form-group'>
                                <label>
                                  Enter Attribute Slug* <i>(must be unique)</i>
                                </label>
                                <input
                                  disabled={
                                    this.state.edit_status === true
                                      ? 'disabled'
                                      : false
                                  }
                                  className='form-control'
                                  type='text'
                                  value={this.state.slug}
                                  onChange={this.handleChange('slug')}
                                />
                                {this.state.errors.slug ? (
                                  <div className='danger'>
                                    {this.state.errors.slug}
                                  </div>
                                ) : (
                                  ''
                                )}
                              </div>
                              <div className='form-group'>
                                <label>Parent Attribute</label>
                                <select
                                  className='form-control'
                                  value={this.state.parent_category}
                                  onChange={this.handleParentCatChange}
                                >
                                  <option value='0'>None</option>
                                  {this.state.parentcategories.map(
                                    (cat_parent, idx) => (
                                      <option
                                        key={cat_parent._id}
                                        value={cat_parent._id}
                                      >
                                        {cat_parent.name}
                                      </option>
                                    )
                                  )}
                                </select>
                              </div>

                              <div className='form-group'>
                                <div className='row'>
                                  <div className='col-md-12'>
                                    <label>Select Category</label>

                                    <MultiSelect
                                      options={this.state.related_cat_options}
                                      value={this.state.related_cat_selected}
                                      onChange={this.setcatSelected} //  labelledBy={"Select"}
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className='form-group'>
                                <label>Attribute Order</label>
                                <input
                                  className='form-control'
                                  type='number'
                                  value={this.state.ordernumber}
                                  onChange={this.handleChange('ordernumber')}
                                />
                                {this.state.errors.ordernumber ? (
                                  <div className='danger'>
                                    {this.state.errors.ordernumber}
                                  </div>
                                ) : (
                                  ''
                                )}
                              </div>

                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? 'disabled'
                                    : false
                                }
                                onClick={this.saveCategories}
                                className='save-btn btn btn-dark'
                              >
                                {this.state.submit_status === true ? (
                                  <React.Fragment>
                                    <ReactSpinner
                                      type='border'
                                      color='primary'
                                      size='1'
                                    />
                                    <span className='submitting'>
                                      {this.state.edit_status === true
                                        ? 'UPDATING Attribute...'
                                        : 'CREATING Attribute...'}
                                    </span>
                                  </React.Fragment>
                                ) : this.state.edit_status === true ? (
                                  'UPDATE Attribute'
                                ) : (
                                  'CREATE Attribute'
                                )}
                              </button>
                              {this.state.message !== '' ? (
                                <div className={this.state.message_type}>
                                  <p>{this.state.message}</p>
                                </div>
                              ) : null}
                              <p className='tableinformation'>
                                {this.state.table_message !== '' ? (
                                  <div
                                    className={this.state.table_message_type}
                                  >
                                    <p>{this.state.table_message}</p>
                                  </div>
                                ) : null}
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className='col-md-8'>
                          <div className='card'>
                            <div className='card-header'>
                              <strong>Cimon Attributes</strong>
                            </div>
                            <div className='card-body categories-list'>
                              <React.Fragment>
                                <div
                                  style={{
                                    display:
                                      this.state.spinner === true
                                        ? 'flex'
                                        : 'none'
                                  }}
                                  className='overlay'
                                >
                                  <ReactSpinner
                                    type='border'
                                    color='primary'
                                    size='10'
                                  />
                                </div>
                              </React.Fragment>

                              <DataTable
                                columns={this.state.columns}
                                data={this.state.data_table}
                                paginationPerPage={30}
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                noDataComponent={
                                  <p>There are currently no records.</p>
                                }
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default ManageAttributes
