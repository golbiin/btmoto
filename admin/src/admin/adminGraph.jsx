import React, { Component } from "react";
import {Line, Bar} from 'react-chartjs-2';
import {Doughnut} from 'react-chartjs-2';
import {Chart} from 'chart.js';
import * as manageUserService from "../ApiServices/admin/manageUser";

class AdminGraph extends Component {
    
    state = { 
    UserWeekOrderCount : 0,
    CurrentMonthOrderCount : 0,
    UserWeekCount : 0,
    CurrentMonthCount: 0,
    UserCount : 0,
    bar_chart: {
        labels : [new Date().getFullYear()],
        datasets : [
            {
            label : "Ongoing",
            data : [100],
            backgroundColor : [
                "#11cdef"
            ]
            },
            {
            label : "Pending",
            data : [38],
            backgroundColor : [
                "#FEB969",
            ]
            },
            {
            label : "Success",
            data : [78],
            backgroundColor : [
                "#2DCE98"
            ]
            },
            {
            label : "Canceled",
            data : [58],
            backgroundColor : [
            "#F53C56"
            ]
            }
    
            
        ],
        
        },



        doughnut_chart: {
            showChart: true,
                
                labels: ['US'],
                datasets: [
                    {
                   
                    borderColor: ['transparent','transparent','transparent','transparent'],
                    backgroundColor: [
                        '#F53C56',
                        '#FEB969',
                        '#11CDEF',
                        '#2DCE98'
                    ],
                    hoverBackgroundColor: [
                    '#F53C56',
                    '#FEB969',
                    '#11CDEF',
                    '#2DCE98'
                    ],
                    data: [0],
                
                    }
                ]
            },  





        line_graph: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul','Aug','Sep' , 'Oct','Nov','Dec'],
            datasets: [
                {
                label: (new Date().getFullYear()) + ' Report',
                fill: true,
                backgroundColor: 'rgba(153,223,253,0.8)',
                borderColor: 'transparent',
                pointBackgroundColor: '#6DBCDB',
                data: [0, 0, 0, 0, 0, 0, 0, 0]
                }, {
                label: (new Date().getFullYear() - 1) +' Report',
                fill: true,
                backgroundColor: 'rgba(255,192,192,0.8)',
                borderColor: 'transparent',
                pointBackgroundColor: '#F68D91',
                data: [0, 0, 0, 0, 0, 0, 0, 0]
                }
            ]
            },
            
            user_static: {
                labels: ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July','Aug' , 'Sep' , 'Oct','Nov','Dec'],
                datasets: [
                    {
                    label: new Date().getFullYear() + ' Users',
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(109,188,219,0.5)',
                    borderColor: '#6DBCDB',
                    borderCapStyle: 'butt',
                    pointBackgroundColor: '#fff',
                    data: [0,0,0,0,0,0,0,0,0,0,0,0]
                    } 
                ]
                } ,


            sales_static: {
                labels: ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July','Aug' , 'Sep' , 'Oct','Nov','Dec'],
                datasets: [
                        {
                        label: (new Date().getFullYear()) +' Report',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: 'rgba(255,165,0,0.5)',
                        borderColor: '#FFA500',
                        borderCapStyle: 'butt',
                        pointBackgroundColor: '#FFA500',
                        data: [0, 0, 0, 0, 0, 0, 0 , 0,0, 0, 0, 0]
                        } ,
                        { 
                            label: (new Date().getFullYear()-1) + ' Report',
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: 'rgba(57,163,255,0.5)',
                            borderColor: '#39A3E1',
                            borderCapStyle: 'butt',
                            pointBackgroundColor: '#39A3E1',
                            data: [0, 0, 0, 0, 0, 0, 0 , 0,0, 0, 0, 0]
                        }   
                    ]
                }

     }

    componentDidMount = () => {
        this.getAllusers();
    };
     getAllusers = async () => {
        this.setState({ spinner: true});
        const response =  await manageUserService.getDashboardDetails();
        if (response.data.status == 1) {
            let user_static = { ...this.state.user_static };
            let User_static = [0,0,0,0,0,0,0,0,0,0,0,0];
                    if( response.data.data.UserGraphdata){
                        for(let i=1; i<=12;i++){
                            response.data.data.UserGraphdata.forEach(function(item){
                                if(item._id === i){ 
                                        User_static[i-1]=item.count;
                                        return;
                                } 
                            });
                        }
                        user_static["datasets"][0] ["data"] = User_static;
                        this.setState({ user_static  : user_static });
                    }
                    let sales_static_current = [0,0,0,0,0,0,0,0,0,0,0,0];
                    let sales_static_Preview = [0,0,0,0,0,0,0,0,0,0,0,0];
                    if(response.data.data.SalesGraphDataCurrentYear ||  response.data.data.SalesGraphDataPrevieousYear){
                        for(let i=1; i<=12;i++){
                            response.data.data.SalesGraphDataCurrentYear.forEach(function(item1){
                                if(item1._id === i){
                                    sales_static_current[i-1]=item1.count;
                                    return;
                                }
                            });
                            response.data.data.SalesGraphDataPrevieousYear.forEach(function(item2){
                                if(item2._id === i){
                                    sales_static_Preview[i-1]=item2.count;
                                    return;
                                }
                            });
                        }

                        const sales_static = { ...this.state.sales_static };
                        const line_graph = {...this.state.line_graph};                        
                        line_graph["datasets"][0] ["data"] = sales_static["datasets"][0] ["data"] = sales_static_current;
                        line_graph["datasets"][1] ["data"] = sales_static["datasets"][1] ["data"] = sales_static_Preview;
                        this.setState({ sales_static  : sales_static ,
                            line_graph : line_graph
                        });                             
                }

                if(response.data.data.ProjectGraphData){
                    const bar_chart = {...this.state.bar_chart};
                    bar_chart["datasets"] = response.data.data.ProjectGraphData;
                    this.setState({  
                        bar_chart : bar_chart
                    });
                }



          
          this.setState({ 
            UserCount: response.data.data.UserCount,
            LastMonthCount :  response.data.data.LastMonthCount,
            CurrentMonthCount : response.data.data.CurrentMonthCount,
            UserWeekCount : response.data.data.UserWeekCount,
          });
         
        } 
        


        const response2 =  await manageUserService.getDungraphDetails();
        if (response2.data.status == 1) {
            
            if(response2.data.data.CountryGraphData){
                let label = [];
                let values = [];
                response2.data.data.CountryGraphData.forEach(function(item2){
                    label.push(item2._id);
                    values.push(item2.total)
                });
               
                const doughnut_chart = {...this.state.doughnut_chart};
               
                doughnut_chart["datasets"][0] ["data"]  = values;
                doughnut_chart["labels"] = label;         
             
                this.setState({  
                    doughnut_chart : doughnut_chart,
                    CurrentMonthOrderCount : response2.data.data.CurrentMonthOrderCount,
                    UserWeekOrderCount : response2.data.data.UserWeekOrderCount
                });
            }
            
        }
      }
    render() { 
        const images = require.context("../assets/images", true);
        return ( <React.Fragment>

    {/* Bar Graph */}
    <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12 pt-4 pb-4 card-single">
        <div class="card card-nav-tabs">
            <h4 class="card-header card-header-info">Projects<i className="dropdown_dots"></i></h4>
            <div class="card-body">
            <Bar
            data={this.state.bar_chart}
            options={{
          
                legend: {
                    display: true,
                    position:"right",
                    legendMarkerType: "circle",
                    labels: {
                        fontColor: '#707070',
                        fontSize: 10,
                        fontFamily: "font-family: 'Montserrat', sans-serif;font-weight:400",
                        usePointStyle: true,
                        boxWidth: 16,
                        padding: 10
                     
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                      
                        }
                    }]
                }
            }
            }


            />
            </div>
        </div>
    </div>
    {/* End Bar Graph */}


    {/* Doughnut chart */}

    <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12 pt-4 pb-4 card-single">
        <div class="card card-nav-tabs">
            <h4 class="card-header card-header-info">Earnings<i className="dropdown_dots"></i></h4>
            <div class="card-body">
            <Doughnut
                            data={this.state.doughnut_chart}
                            options={{
                                title:{
                                },
                                legendMarkerType: "circle",
                                legend:{
                                    display:true,
                                    position:'right',
                                    labels: {
                                        usePointStyle: true,
                                        boxWidth: 16,
                                        fontSize:15,
                                        fontFamily: 'Avenir Medium',
                                        fontColor: '#002658',
                                        boxWidth: 20,
                                        circular:true
                                    }
                                }
                            }}
                        />
            </div>
        </div>
    </div>
    {/* End Doughnut chart */}

            
            

 <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12 pt-4 pb-4 card-single">
        <div class="card card-nav-tabs">
            <h4 class="card-header card-header-info">Recent Report<i className="dropdown_dots"></i></h4>
            <div class="card-body">
            <Line
            data={this.state.line_graph}
            options={{
            title:{
            },
            legend:{
                display: true,
                position:"top",
                labels: {
                    usePointStyle: true,
                    boxWidth: 16,
                }
            }
            }}
            />
            </div>
        </div>
    </div>




<div className="col-md-6 col-lg-6 col-sm-6 col-xs-12 pb-4 card-single">
        <div class="card card-nav-tabs">
            <h4 class="card-header card-header-info">User Statistics<i className="dropdown_dots"></i></h4>
            <div class="card-body">
            <Line
            data={this.state.user_static}
            options={{
            title:{
            },
            legend:{
                display: false,
                position:"top",
                labels: {
                    usePointStyle: true,
                    boxWidth: 16,
                }
            },
            scales: {
                xAxes: [{
                    gridLines: {
                      display: false,
                    },
                  }],
              } 
              
            }}
            />
    <div className="row graph_bottom">
        <div className="main_box_section">
          
            <div className="box1 boxs">
            <p>Weekly Users</p>
            <p>{this.state.UserWeekCount}</p></div>
            <div className="box2 boxs">
            <p>Monthly Users</p>
            <p>{this.state.CurrentMonthCount}</p></div>
            <div className="box3 boxs">            
            <p>Trend</p>
            <p>
                <img src={images(`./admin/ic_trending_up.png`)} className="img-fluid " alt="customer" ></img>
            </p>
            </div>  
          
        </div>
    </div>
            </div>
        </div>
    </div>


<div className="col-md-6 col-lg-6 col-sm-6 col-xs-12 pb-4 card-single">
        <div class="card card-nav-tabs">
            <h4 class="card-header card-header-info">Sales Comparison<i className="dropdown_dots"></i></h4>
            <div class="card-body">
            <Line
            data={this.state.sales_static}
            options={{
            title:{
                   display:false,
                   fontSize:20
            },
            legend:{
                display: false,
                position:"top",
                labels: {
                    usePointStyle: true,
                    boxWidth: 16,
                }
            },
            scales: {
                xAxes: [{
                    gridLines: {
                      display: false,
                    },
                  }],
              },
            }}
            />
        <div className="row graph_bottom">
            <div className="main_box_section">

            <div className="box1 boxs">
                <p>Weekly Orders</p>
                <p>{this.state.UserWeekOrderCount}</p></div>
                <div className="box2 boxs">
                <p>Monthly Orders</p>
                <p>{this.state.CurrentMonthOrderCount}</p></div>
                <div className="box3 boxs">
                <p>Trend</p>
                <p>
                    <img src={images(`./admin/ic_trending_up.png`)} className="img-fluid " alt="customer" ></img>

                 </p></div>  

            </div>
        </div>
            </div>
        </div>
    </div>
            </React.Fragment>);
    }






















}
 
export default AdminGraph;