import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import { Link } from "react-router-dom";
import DataTable from 'react-data-table-component';
import Uploadprofile from "../common/uploadProfileimage";
import * as manageUser from "../../ApiServices/admin/manageUser";
import CKEditor from 'ckeditor4-react';
import { apiUrl ,siteUrl} from "../../../src/config.json";
import CategoryLink from "../../../src/admin/common/categoryLink";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css"; 
class ManageCategories extends Component {
  constructor(props) {
    super(props);
    this.onEditorChange = this.onEditorChange.bind(this);
    this.onEditorChange2 = this.onEditorChange2.bind(this);
    this.UploadprofileElement = React.createRef();
  }

  state = {
    category_name: "",
    descrption: "",
    parent_category: 0,
    slug: "",
    attributes: [],
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    table_message: "",
    table_message_type: "",
    categories: [],
    parentcategories: [],
    delete_status: false,
    edit_status: false,
    data_table: [],
    spinner: true,
    page_type: "",
    page_layout: "",
    page_id_name: "",
    profile_image: "",
    upload_data: "",
    testdata: "",
    order : '',
    columns: [
      {
        name: 'Category Name',
        sortable: false,
        cell: row => <p className={'catogery_sec_loop parent_id_' + row.parent_id}>{row.category_name}  </p>
      },
      {
        name: 'Slug',
        selector: 'slug',
        sortable: true,
      },

      {
        name: 'Descrption',
        cell: row => row.descrption.replace(/(<([^>]+)>)/ig, ''),
        selector: 'descrption',
        sortable: true,
        left: true,
        hide: 'sm'
      },

      {
        name: 'Order',
        selector: 'order',
        sortable: true,
        width: '80px'
      },

      {
        name: 'Action',
        cell: row => <div className="action_dropdown" data-id={row._id}><i data-id={row._id} className="dropdown_dots" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
          <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
            <Link to={"/admin/settings/manage-categories/" + row.slug} className="dropdown-item">
              Edit Category
        </Link>
            <a onClick={() => this.handleRemoveCategory(row._id)} className="dropdown-item">
              Delete
        </a>
          </div>
        </div>,
        allowOverflow: false,
        button: true,
        width: '56px', // custom width for icon button
      },

    ],
    cropped:"",
    submitcrop:false,
    src: null,
    crop: {
      unit: '%',
      width: 50,
      height: 50
    },
  };
  onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () =>
        this.setState({ src: reader.result })
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  _crop() {
    // image in dataUrl
    this.cropper.getCroppedCanvas().toBlob(blob => {
      if (!blob) {
        //reject(new Error('Canvas is empty'));
        console.error('Canvas is empty');
        return;
      } 
      const reader = new FileReader();
      reader.readAsArrayBuffer(blob);
      
      const blobFile = new File([blob], 'image.jpg'); 
      this.setState({ upload_data: blobFile });  
      window.URL.revokeObjectURL(this.fileUrl);
      this.fileUrl = window.URL.createObjectURL(blob);
      console.log(this.fileUrl);
      this.UploadprofileElement.current.setImage(this.fileUrl);
      this.setState({ cropped: this.fileUrl });  
    } );
  }
  cropImage = e => { 
    this.setState({ submitcrop:  true  });
  }
  onCropperInit(cropper) {
      this.cropper = cropper;
  }
 /* onchange ckeditor Description */
  onEditorChange(evt) {
    this.setState({
      descrption: evt.editor.getData()
    })
  }
  onEditorChange2(evt) {
    this.setState({
      short_description: evt.editor.getData()
    })
  }


  componentDidMount = () => {
    this.getAllCategories();
    this.getAllParentCategory();
    this.getAllPagesIdsandNames();
  };


/* Get all catogeries */
  getAllCategories = async () => {
    this.setState({ spinner: true });
    try {
      const response = await settingsService.getAllCategories();
      if (response.data.status == 1) {
        this.setState({ categories: response.data.data });
        const Setdata = { ...this.state.data_table };
        const data_table_row = [];
        response.data.data.map((cat, index) => {
          const Setdata = {};
          Setdata.category_name = cat.category_name;
          Setdata.slug = cat.slug;
          Setdata.order = cat.order;
          Setdata._id = cat._id;
          Setdata.parent_id = cat.parent_id;
          Setdata.descrption = cat.descrption;
          Setdata.updatedata = '0';
          data_table_row.push(Setdata);
        });
        this.setState({
          data_table: data_table_row
        });
      }

      this.setState({ spinner: false });
    } catch (err) {

      this.setState({ spinner: false });
    }
  }

  /* Get all Parent Category  */
  getAllParentCategory = async () => {
    try {
        const response = await settingsService.getAllParentCategory();
        if (response.data.status == 1) {
          this.setState({
            parentcategories: response.data.data
          });
        }
    } catch (err) {
      this.setState({
        message: 'Something went wrong!',
        responsetype: "error",
      });     
    }
  }

/* Get all pages ids and names */
  getAllPagesIdsandNames = async () => {

    try {
      const response = await settingsService.getAllPagesIdsandNames();
      if (response.data.status == 1) {
        this.setState({
          page_id_name: response.data.data
        });
      }
    } catch (err) {
      this.setState({
        message: 'Something went wrong!',
        responsetype: "error",
      });
    }
  }

/* initialize  Category fields    */
  addCategrory = () => {
    this.setState({
      category_name: "",
      parent_category: 0,
      slug: "",
      attributes: [],
      errors: {},
      submit_status: false,
      message: "",
      message_type: "",
      delete_status: false,
      edit_status: false,
      descrption: "",
      short_description: ""
    })
  }
 

  /*Joi validation schema*/  
  schema = {
    category_name: Joi.string().required().error(() => {
      return {
        message: 'This field is a required field.',
      };
    }),
    slug: Joi.string().required().error(() => {
      return {
        message: 'This field is a required field.',
      };
    }),
    attributes: Joi.optional().label("Attributes"),
    profile_image: Joi.allow(null),
    order: 
    Joi.alternatives(
      Joi.number().allow("").allow(0),
      Joi.string().allow("").allow(0)
  )
  };

  validateProperty = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  handleChange = (input) => event => {
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty(input, event.target.value);
    if (errorMessage) errors[input] = errorMessage;
    else delete errors[input];
    this.setState({ [input]: event.target.value, errors });
  }

/*   Set parent category on select change   */
  handleParentCatChange = async (e) => {
    this.setState({
      parent_category: e.target.value
    });
  }

/*   handle page type and Pagelayout  */
  handlePageType = async (e) => {
    this.setState({
      page_type: e.target.value,
      page_layout: ""
    });

  }
  handlePageLayout = async (e) => {
    this.setState({
      page_layout: e.target.value
    });
  }

/*   handle textarea change */
  handletextareaChange = async (e) => {
    this.setState({
      short_description: e.target.value
    });
  }

/* Handle  add category */
  saveCategories = async (e) => {
    e.preventDefault()
    const checkState = {
      category_name: this.state.category_name,
      slug: this.state.slug
    };
    const errors = { ...this.state.errors };
    let result = Joi.validate(checkState, this.schema);
    if (result.error) {

      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });


    } else {

      const category_data = {
        category_name: this.state.category_name,
        slug: this.state.slug, 
        parent_category: this.state.parent_category,
        descrption: this.state.descrption,
        short_description: this.state.short_description,
        page_type: this.state.page_type,
        page_layout: this.state.page_layout,
        profile_image: this.state.profile_image,
        order : this.state.order
      }


      try {
        this.setState({
          submit_status: true,
          message: "",
          message_type: ""
        });
         
        if (this.state.edit_status === true) {
            this.setState({
              submitStatus: false,
              message: '',
              responsetype: "error",
            });
        } else {

          if (this.state.upload_data) {
            const response1 = await manageUser.uploadProfile(
              this.state.upload_data
            );
         
            if (response1.data.status == 1) {
              let filepath = response1.data.data.file_location;
              this.setState({ profile_image: filepath });
              this.addCatData();

            } else {
              
              this.setState({
                submitStatus: false,
                message: response1.data.message,
                responsetype: "error",
              });
            }
          } else {
            this.addCatData();
          }

          setTimeout(() => {
            this.setState(() => ({ message: '' }))
          }, 5000);

        }
      } catch (err) {
        this.setState({
          submitStatus: false,
          message: 'Something went wrong! Please try after some time',
          responsetype: "error",
        });
      }
    }
  }

  addCatData = async () => {
    const category_data = {
      category_name: this.state.category_name,
      slug: this.state.slug,
      parent_category: this.state.parent_category,
      descrption: this.state.descrption,
      short_description: this.state.short_description,
      page_type: this.state.page_type,
      page_layout: this.state.page_layout,
      profile_image: this.state.profile_image,
      order : this.state.order
    }

    try {

    const response = await settingsService.createCategory(category_data);
    if (response) {
      this.setState({ submit_status: false });
      if (response.data.status === 1) {
        this.setState({
          message: response.data.message,
          message_type: "success",
          category_name: "",
          slug: "",
          parent_category: 0,
          descrption: "",
          short_description : '',
          page_type: "",
          page_layout: "",
          order : ''
        });
        this.getAllCategories();
        this.getAllParentCategory();
      } else {
        this.setState({
          message: response.data.message,
          message_type: "error",
        });
      }
    } else {
      this.setState({ submit_status: false });
      this.setState({
        message: "Something went wrong! Please try after some time",
        message_type: "error",
      });
    }


  } catch (err) {
    this.setState({
      submitStatus: false,
      message: 'Something went wrong! Please try after some time',
      responsetype: "error",
    });
  }
  }
  /* Handle remove category */
  handleRemoveCategory = async (id) => {

    if (window.confirm(`Are you sure you’d like to delete this?`)) {

    this.setState({
      delete_status: true,
    });
    try {
    const response = await settingsService.removeCategory({ _id: id });
    if (response.data.status === 1) {
      let newCategories = [...this.state.data_table];
      newCategories = newCategories.filter(function (obj) {
        return obj._id !== id;
      });
      this.setState({
        table_message: response.data.message,
        table_message_type: "sucess",
      });
      this.setState({ data_table: newCategories, delete_status: false, edit_status: false });
      setTimeout(() => {
        this.setState(() => ({ table_message: '' }))
      }, 5000);
    } else {
      this.setState({
        table_message: response.data.message,
        table_message_type: "sucess",
      });
      setTimeout(() => {
        this.setState(() => ({ table_message: '' }))
      }, 5000);

    }

  } catch (err) {
    this.setState({
      table_message: 'Something went wrong!',
      table_message_type: "error",
    });
  }
    }
  };


 /* Handle Upload Category image  */
  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors };
      let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      if (value.target.files && value.target.files.length > 0) {
        const reader = new FileReader();
        reader.addEventListener('load', () =>
          this.setState({ src: reader.result })
        );
        reader.readAsDataURL(value.target.files[0]);
      }
      let file = value.target.files[0];
      upload_data = file;
      this.setState({ upload_data: upload_data });
    }
  };

  render() {
    let datarow = this.state.data_table;
    const { crop, croppedImageUrl, src,submitcrop } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4" >
                  <React.Fragment>
                    <div className="categories-wrap p-4">
                      <div className="row">
                        <div className="col-md-4">
                          <div className="card">
                            <div className="card-header">
                              <strong>{this.state.edit_status === true ? 'Edit' : 'Add'} Category</strong>
                              {this.state.edit_status === true ?
                                <button onClick={this.addCategrory}
                                  className="ml-4 add-cat-btn btn btn-success">+ Add New</button> : ''}
                            </div>
                            <div className="card-body categories-add">

                            

                              <div className="form-group">
                                <label>Enter Category Name*</label>
                                <input
                                  className="form-control"
                                  type="text"
                                  value={this.state.category_name}
                                  onChange={this.handleChange('category_name')}
                                />
                                {this.state.errors.category_name ? (<div className="danger">{this.state.errors.category_name}</div>) : ("")}
                              </div>


                              <div className="form-group">
                                <label>Enter Category Slug* <i>(must be unique)</i></label>
                                <input
                                  disabled={this.state.edit_status === true ? "disabled" : false}
                                  className="form-control"
                                  type="text"
                                  value={this.state.slug}
                                  onChange={this.handleChange('slug')}
                                />
                                {this.state.errors.slug ? (<div className="danger">{this.state.errors.slug}</div>) : ("")}
                              </div>





                              <div className="form-group">
                          <label htmlFor="">Product Order</label>
                          <input
                            name="order"
                            onChange={this.handleChange('order')}
                            value={this.state.order}
                            type="number"
                          //  min = "0"
                          //  placeholder="Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.order ? (
                            <div className="error text-danger">
                              {this.state.errors.order}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>




                              <div className="form-group">
                                <label>Parent Category</label>
                                <select className="form-control" value={this.state.parent_category} onChange={this.handleParentCatChange}>
                                  <option value="0">None</option>
                                  {this.state.parentcategories.map((cat_parent, idx) => (
                                    <option key={cat_parent._id} value={cat_parent._id}>{cat_parent.category_name}</option>
                                  ))}
                                </select>
                              </div>
                              {/* <div className="form-group">
                                <label>Short Description</label>
                                <textarea value={this.state.short_description} onChange={this.handletextareaChange} className="form-control"></textarea>
                              </div> */}

                                <div className="form-group">
                                <label>Short Description</label>
                                <CKEditor
                                  data=""
                                   onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                    extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                    allowedContent: true,
                                    filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                  }}
                                  onInit={
                                    editor => {
                                    }
                                  }
                                  onChange={this.onEditorChange2}
                                />                              
                              </div>
                              <div className="form-group">
                                <label>Description</label>
                                <CKEditor
                                  data=""
                                   onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                    extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                    allowedContent: true,
                                    filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                                  }}
                                  onInit={
                                    editor => {
                                    }
                                  }
                                  onChange={this.onEditorChange}
                                />                              
                              </div>
                              <div className="form-group thumbanail_container">
                                <label>Image</label>
                                <Uploadprofile   ref={this.UploadprofileElement}
                                  onuplaodProfile={this.onuplaodProfile}
                                  value={
                                    this.state.profile_image
                                  }
                                  errors={this.state.errors}
                                />
                                
                                {src && !submitcrop && (
                            <div class="cropscreen">
                              <div class="cropheader">
                            <label>Crop Image</label>
                            <button onClick={this.cropImage} class="btn btn-info">Crop Image</button>
                            </div>
                            <Cropper
                                src={src}
                                style={{height: 500, width: '100%'}}
                                // Cropper.js options
                                initialAspectRatio={1 / 1}
                                guides={false}
                                crop={this._crop.bind(this)}
                                onInitialized={this.onCropperInit.bind(this)}
                            /> 
                              </div>
                            )}
                              </div>
                              <div className="form-group">
                                <label>Page Type</label>
                                <select className="form-control" value={this.state.page_type} onChange={this.handlePageType}>
                                  <option value="">None</option>
                                  <option value="product">Product List</option>
                                  <option value="cms">Cms</option>
                                </select>
                              </div>
                              {(() => {
                                if (this.state.page_type) {
                                  return <div className="form-group">
                                    <label>Select Layout</label>
                                    {(() => {
                                      if (this.state.page_type === 'product') {
                                        return <select className="form-control" value={this.state.page_layout} onChange={this.handlePageLayout}>
                                          <option value="">None</option>
                                          <option value="layout_1">Layout 1</option>
                                          <option value="layout_2">Layout 2</option>
                                        </select>;
                                      } else if (this.state.page_type === 'cms') {
                                        return <select className="form-control" value={this.state.page_layout} onChange={this.handlePageLayout}>
                                          <option value="">None</option>
                                          {this.state.page_id_name.map((pages, idx) => (
                                            <option value={pages._id}>{pages.title}</option>
                                          ))}

                                        </select>;
                                      }

                                    })()}
                                  </div>;
                                }
                              })()}                             
                              <button
                                disabled={this.state.submit_status === true ? "disabled" : false}
                                onClick={this.saveCategories}
                                className="save-btn btn btn-dark">
                                {
                                  this.state.submit_status === true ? (
                                    <React.Fragment>
                                      <ReactSpinner type="border" color="primary" size="1" />
                                      <span className="submitting">
                                        {this.state.edit_status === true ? 'UPDATING CATEGORY...' : 'CREATING CATEGORY...'}
                                      </span>
                                    </React.Fragment>
                                  ) : (
                                      this.state.edit_status === true ? 'UPDATE CATEGORY' : 'CREATE CATEGORY'
                                    )
                                }
                              </button>
                              {this.state.message !== "" ? (
                                <div className={this.state.message_type}>
                                  <p>{this.state.message}</p>
                                </div>
                              ) : null}
                          <p className="tableinformation">

                          {this.state.table_message !== "" ? (
                          <div className={this.state.table_message_type}>
                          <p>{this.state.table_message}</p>
                          </div>
                          ) : null
                          }
                          </p>
                                

                            </div>

                          </div>
                        </div>
                        <div className="col-md-8">
                          <div className="card">
                            <div className="card-header">
                              <strong>Cimon Categories</strong>
                            </div>
                            <div className="card-body categories-list">
                              <React.Fragment>
                                <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                                  <ReactSpinner type="border" color="primary" size="10" />
                                </div>
                              </React.Fragment>
                             
                              <DataTable
                                columns={this.state.columns}
                                data={this.state.data_table}
                                paginationPerPage={30}
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                noDataComponent = {<p>There are currently no records.</p>}
                              />                          
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </React.Fragment>
                </div>

              </div>
            </div>

          </div>
        </div>

      </React.Fragment>
    );

  }
}

export default ManageCategories;
