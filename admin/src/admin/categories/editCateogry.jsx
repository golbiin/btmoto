import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import { Link } from "react-router-dom";
import validate from "react-joi-validation";
import Joi, { join } from "joi-browser";
import * as settingsService from "../../ApiServices/admin/settings";
import ReactSpinner from "react-bootstrap-spinner";
import ReactEditor from '../reactEditor/reactEditor';
import * as manageUser from "../../ApiServices/admin/manageUser";
import Uploadprofile from "../common/uploadProfileimage";
import CKEditor from 'ckeditor4-react';
import { apiUrl, siteUrl } from "../../config.json";
import CategoryLink from "../common/categoryLink";
import * as manageProducts from "../../ApiServices/admin/products";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css"; 
class EditCateogry extends Component {
  constructor(props) {
    super(props);
    this.UpdateEditorContent = this.UpdateEditorContent.bind(this);
    this.onEditorChange = this.onEditorChange.bind(this);
    this.onEditorChange2 = this.onEditorChange2.bind(this);
    this.UploadprofileElement = React.createRef();
  }
  state = {
    data: { userName: "", password: "" },
    errors: {},
    parent_category: 0,
    submit_status: false,
    category_name: "",
    parentcategories: [],
    slug: "",
    descrption: "",
    short_description: "",
    spinner: true,
    content: [],
    page_type: "",
    page_layout: "",
    profile_image: "",
    upload_data: "",
    page_id_name: [],
    url: '',
    banner_sub_description: '',
    banner_main_description:'',
    manualUrl:"",
    downloadUrl :"",
    cropped:"",
    submitcrop:false,
    src: null,
    order : '',
    crop: {
      unit: '%',
      width: 50,
      height: 50 
    },
  };
  onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () =>
        this.setState({ src: reader.result })
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  _crop() {
    // image in dataUrl
    this.cropper.getCroppedCanvas().toBlob(blob => {
      if (!blob) {
        //reject(new Error('Canvas is empty'));
        console.error('Canvas is empty');
        return;
      } 
      const reader = new FileReader();
      reader.readAsArrayBuffer(blob);
      
      const blobFile = new File([blob], 'image.jpg'); 
      this.setState({ upload_data: blobFile });  
      window.URL.revokeObjectURL(this.fileUrl);
      this.fileUrl = window.URL.createObjectURL(blob);
      console.log(this.fileUrl);
      this.UploadprofileElement.current.setImage(this.fileUrl);
      this.setState({ cropped: this.fileUrl });  
    } );
  }
  cropImage = e => { 
    this.setState({ submitcrop:  true  });
  }
  onCropperInit(cropper) {
      this.cropper = cropper;
  }

  onEditorChange(evt) {
    this.setState({
      descrption: evt.editor.getData()
    })
  }

  
  onEditorChange2(evt) {
    this.setState({
      short_description: evt.editor.getData()
    })
  }


  /*****************INPUT HANDLE CHANGE **************/

  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
 
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });

  };
 
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;

  };

 /*Select Handle Change*/
  handleParentCatChange = async (e) => {
    this.setState({
      parent_category: e.target.value
    });
  }
  handletextareaChange = async (e) => {
    this.setState({
      short_description: e.target.value
    });
  }

  componentDidMount = () => {
    this.getAllParentCategory();
    setTimeout(() => {
      this.getCategoryBySlug();
    }, 2000);
    this.getAllPagesIdsandNames();
  };

/* Get all Parent Category */
  getAllParentCategory = async () => {
    const response = await settingsService.getAllParentCategory();
    if (response.data.status == 1) {
      this.setState({
        parentcategories: response.data.data,
      });

    }
  }

  /* Get Category  by slug */
  getCategoryBySlug = async () => {
    const category_slug = this.props.match.params.slug;
    const response = await settingsService.getCategoryBySlug({ slug: category_slug });
    if (response.data.status == 1) {
      this.setState({
        submit_status: false,
        message: "",
        message_type: "",
        delete_status: false,
        category_name: response.data.data.category_name,
        slug: response.data.data.slug,
        descrption: response.data.data.descrption,
        short_description: response.data.data.short_description,
        manualUrl: response.data.data.manualUrl?response.data.data.manualUrl:"",
        downloadUrl: response.data.data.downloadUrl?response.data.data.downloadUrl:"",
        parent_category: response.data.data.parent_id,
        content: response.data.data.content,
        page_layout: response.data.data.page_layout,
        page_type: response.data.data.page_type,
        profile_image: response.data.data.image,
        order :  response.data.data.order,
        _id: response.data.data._id
      });

      if (response.data.data.hasOwnProperty('banner_sub_description')) {
        let banner_sub_description = response.data.data.banner_sub_description ? response.data.data.banner_sub_description : ''
        this.setState({ banner_sub_description: banner_sub_description });
      }

      if (response.data.data.hasOwnProperty('banner_main_description')) {
        let banner_main_description = response.data.data.banner_main_description ? response.data.data.banner_main_description : ''
        this.setState({ banner_main_description: banner_main_description });
      }

      


      this.GetCurrentCatUrl(response.data.data._id);
      this.setState({ spinner: false });

    } else {
      this.props.history.push({
        pathname: "/admin/settings/manage-categories"
      });
      this.setState({ spinner: false });
    }
  }

  /**Joi validation schema*/
  schema = {
    category_name: Joi.string().required().error(() => {
      return {
        message: 'Please be sure you’ve filled in the Category name. It is not allowed to be empty.',
      };
    }),
    slug: Joi.string().required().error(() => {
      return {
        message: 'Please be sure you’ve filled in the Slug. It is not allowed to be empty.',
      };
    }),
    banner_sub_description: Joi.string().allow(""),
    banner_main_description : Joi.string().allow(""),
    manualUrl : Joi.string().allow(""),
    downloadUrl : Joi.string().allow(""),
    profile_image: Joi.allow(null),
    order: Joi.alternatives(Joi.number().allow("").allow(0),
      Joi.string().allow("").allow(0))


  };

  validateProperty = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  handleChange = (input) => event => {
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty(input, event.target.value);
    if (errorMessage) errors[input] = errorMessage;
    else delete errors[input];
    this.setState({ [input]: event.target.value, errors });
  }

/* Update React Editor Content */
  UpdateEditorContent = data => {
    // set on Parent  React editor content
    const updateddata = data;
    this.setState({ content: updateddata });
  }

  /*From submit  */
  saveCategories = async (e) => {
    e.preventDefault()
    const errors = { ...this.state.errors };
    const checkState = {
      category_name: this.state.category_name,
      slug: this.state.slug,
    };
    let result = Joi.validate(checkState, this.schema);
    if (!result.error) {
      try {
        this.setState({
          submit_status: true,
          message: "",
          message_type: ""
        });
        this.setState({ spinner: true });
        if (this.state.upload_data) {
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            this.setState({ profile_image: filepath });
            this.updateCatData();
          } else {
           
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error",
            });
          }
        } else {
          this.updateCatData();
        }
      } catch (err) {
        this.setState({ activepage: false });
        this.setState({ submit_status: false });
        this.setState({
          message: "Something went wrong! Please try after some time",
          message_type: "error",
        });
      }
    }

  }
  /*   handle Update Cateogry  */
  updateCatData = async () => {
    const category_data = {
      category_name: this.state.category_name,
      slug: this.state.slug,
      parent_category: this.state.parent_category,
      descrption: this.state.descrption,
      content: this.state.content,
      page_type: this.state.page_type,
      page_layout: this.state.page_layout,
      profile_image: this.state.profile_image,
      banner_sub_description: this.state.banner_sub_description, 
      manualUrl: this.state.manualUrl, 
      downloadUrl: this.state.downloadUrl, 
      banner_main_description : this.state.banner_main_description,
      short_description : this.state.short_description,
      order : this.state.order
    }


    try {
    const response = await settingsService.updateCategory(category_data);

    if (response) {
      this.setState({ submit_status: false });
      if (response.data.status === 1) {
        this.setState({
          message: response.data.message,
          message_type: "success",
        });
       
        
        setTimeout(
          () =>
            this.props.history.push({
              pathname: '/admin/settings/manage-categories',
            }),
          2000
        );
        this.setState({ spinner: false });

        
      } else {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "error",
        });
        
      }
    }

  } catch (err) {

    this.setState({
      message: 'something Went wrong!',
      message_type: "error",
    });
  }

  }


  /*   handle page type and Pagelayout  */
  handlePageType = async (e) => {
    this.setState({
      page_type: e.target.value,
      page_layout: ""
    });

  }
  handlePageLayout = async (e) => {
    this.setState({
      page_layout: e.target.value
    });
  }

  /*  Get all PAges Ids and names  */
  getAllPagesIdsandNames = async () => {
    const response = await settingsService.getAllPagesIdsandNames();
    if (response.data.status == 1) {
      if (response.data.data) {
        this.setState({
          page_id_name: response.data.data
        });
      }
    }
  }

  /* Handle Upload Category image  */
  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors };
    let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      if (value.target.files && value.target.files.length > 0) {
        const reader = new FileReader();
        reader.addEventListener('load', () =>
          this.setState({ src: reader.result })
        );
        reader.readAsDataURL(value.target.files[0]);
      }
      let file = value.target.files[0];
      upload_data = file;
      this.setState({ upload_data: upload_data });
    }
  };

  /* get Current Category url   */
  GetCurrentCatUrl = async (Cat_id) => {
    let url = '';
    this.setState({ submit_status: true });
    const cimonProducts = { ...this.state.cimonProducts };
    const response = await manageProducts.getCurrentproductUrl(Cat_id, 'category');
    if (response.data.status == 1) {
      this.setState({ submit_status: false });
      if (response.data.data !== '/null') {
        if (response.data.data.indexOf('introduction') < 0) {
          url = response.data.data + '/'
          this.setState({
            url: '/product' + url
          })
        } else {
          url = response.data.data + '/'
          this.setState({
            url: url
          })
        }

      } else {
        url = '/'
        this.setState({
          url: '/product' + url
        })
      }

    }
  }


  render() {
     const feteched = this.state.content;
     const { crop, croppedImageUrl, src,submitcrop } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid admin-body ">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  useradd-new pt-4 pb-4 pr-4" >
                  <React.Fragment>
                    <div style={{ display: this.state.spinner === true ? 'flex' : 'none' }} className="overlay">
                      <ReactSpinner type="border" color="primary" size="10" />
                    </div>
                  </React.Fragment>
                  <React.Fragment>
                    <div className="card">
                      <div className="card-header">Edit Category</div>
                      <div className="card-body">
                        <p className="tableinformation">
                         
                        </p>
                        <div className="form-group">
                          <label>Category Name *</label>
                          <input
                            value={this.state.category_name}
                            type="text" placeholder="Category Name"
                            onChange={this.handleChange('category_name')}
                            className="form-control" />
                          {this.state.errors.category_name ?
                            (<div className="danger">{this.state.errors.category_name}</div>) : ("")}
                        </div>
                        <div className="form-group">
                          <label>Category Slug *</label>
                          <input disabled
                            value={this.state.slug}
                            onChange={this.handleChange('slug')}
                            type="text" placeholder="Category Slug" className="form-control" />
                          {this.state.errors.slug ?
                            (<div className="danger">{this.state.errors.slug}</div>) : ("")}
                        </div>
                        <div className="form-group">
                          <label>Url</label>
                          <input disabled
                            value={siteUrl + this.state.url}
                            className="form-control"
                          />
                        </div>


                        
                        <div className="form-group">
                          <label htmlFor="">Product Order</label>
                          <input
                            name="order"
                            onChange={this.handleChange('order')}
                            value={this.state.order}
                            type="number"
                          //  min = "0"
                          //  placeholder="Meta Description"
                            className="form-control"
                          />
                          {this.state.errors.order ? (
                            <div className="error text-danger">
                              {this.state.errors.order}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>




                        <div className="form-group">
                          <label>Parent Category</label>
                          <select className="form-control" value={this.state.parent_category} onChange={this.handleParentCatChange}>
                            <option value="0">None</option>
                            {this.state.parentcategories.map((cat_parent, idx) => (
                              <option key={cat_parent.category_name} value={cat_parent._id}>{cat_parent.category_name}</option>
                            ))}
                          </select>
                        </div>


                        <div className="form-group">
                          <label htmlFor="">Banner Main Text</label>
                          <input value={this.state.banner_main_description}
                            onChange={this.handleChange('banner_main_description')}
                            type="text" placeholder="Sub Text" className="form-control" />
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Banner Sub Text</label>
                          <input value={this.state.banner_sub_description}
                            onChange={this.handleChange('banner_sub_description')}
                            type="text" placeholder="Sub Text" className="form-control" />
                        </div>

                        {/* <div className="form-group">
                          <label>Short Description</label>
                          <textarea value={this.state.short_description} onChange={this.handletextareaChange} className="form-control"></textarea>
                        </div> */}

                          <div className="form-group">
                          <label>Short Description</label>
                          <CKEditor
                            data={((this.state.short_description != null) ? this.state.short_description : "")}
                             onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                              extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                              allowedContent: true,
                              filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                            }}

                            onInit={
                              editor => {
                              }
                            }
                            onChange={this.onEditorChange2}
                          />
                          
                        </div>

                        <div className="form-group">
                          <label>Description</label>
                          <CKEditor
                            data={((this.state.descrption != null) ? this.state.descrption : "")}
                             onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                              extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                              allowedContent: true,
                              filebrowserImageUploadUrl: apiUrl + '/admin/upload/imageupload-new'
                            }}

                            onInit={
                              editor => {
                              }
                            }
                            onChange={this.onEditorChange}
                          />
                          
                        </div>

                        <div className="form-group thumbanail_container">
                          <Uploadprofile  ref={this.UploadprofileElement}
                            onuplaodProfile={this.onuplaodProfile}
                            value={
                              this.state.profile_image
                            }
                            errors={this.state.errors}
                          />

                            {src && !submitcrop && (
                            <div class="cropscreen">
                              <div class="cropheader">
                            <label>Crop Image</label>
                            <button onClick={this.cropImage} class="btn btn-info">Crop Image</button>
                            </div>
                            <Cropper
                                src={src}
                                style={{height: 500, width: '100%'}}
                                // Cropper.js options
                                initialAspectRatio={1 / 1}
                                guides={false}
                                crop={this._crop.bind(this)}
                                onInitialized={this.onCropperInit.bind(this)}
                            /> 
                              </div>
                            )}
                        </div>

                        <div className="form-group">
                          <label>Page Type</label>
                          <select className="form-control" value={this.state.page_type} onChange={this.handlePageType}>
                            <option value="">None</option>
                            <option value="product">Product List</option>
                            <option value="cms">Cms</option>
                          </select>
                        </div>


                        {(() => {
                          if (this.state.page_type) {
                            return <div className="form-group">
                              <label>Select Layout</label>
                              {(() => {
                                if (this.state.page_type === 'product') {
                                  return <select className="form-control" value={this.state.page_layout} onChange={this.handlePageLayout}>
                                    <option value="">None</option>
                                    <option value="layout_1">Layout 1</option>
                                    <option value="layout_2">Layout 2</option>
                                  </select>;
                                } else if (this.state.page_type === 'cms') {
                                  return <select className="form-control" value={this.state.page_layout} onChange={this.handlePageLayout}>
                                    <option value="">None</option>
                                    {
                                      this.state.page_id_name.map((pages, idx) => (
                                        <option value={pages._id}>{pages.title}</option>
                                      ))
                                    }

                                  </select>;
                                }

                              })()}
                            </div>;
                          }
                        })()}
                        <div className="form-group">
                          <label htmlFor="">Manual URL</label>
                          <input value={this.state.manualUrl}
                            onChange={this.handleChange('manualUrl')}
                            type="text" placeholder="Manual URL" className="form-control" />
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Download URL</label>
                          <input value={this.state.downloadUrl}
                            onChange={this.handleChange('downloadUrl')}
                            type="text" placeholder="Download URL" className="form-control" />
                        </div>
                        
                      </div>
                    </div>
                     
                    <div className="submit">
                      <button className="submit-btn"
                        disabled={this.state.submit_status === true ? "disabled" : false}
                        onClick={this.saveCategories}
                      >
                        {
                          this.state.submit_status === true ? (
                            <React.Fragment>
                              <ReactSpinner type="border" color="primary" size="1" />

                            </React.Fragment>
                          ) : 'Save'
                        }
                      </button>
                      <Link
                        disabled={this.state.submit_status === true ? "disabled" : false}

                        to="/admin/settings/manage-categories" className="cancel-btn">Cancel</Link>
                    </div>
                    {this.state.table_message !== "" ? (
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          ) : null
                          }
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>);
  }
}

export default EditCateogry;