import React, { Component } from "react";
class UploadVideo extends Component {
  state = {
    uploading: false,
    video_url: "",
  };
  uplaodHandle = async (event) => {
    event.preventDefault();
    let files = event.target.files[0];
    if (files) {
      if (this.checkMimeType(event)) {
        let video_url = URL.createObjectURL(event.target.files[0]);
        
        this.setState({ video_url: video_url });
        this.props.onuplaodVideo(event, "video_url");
      }
    }
  };
  checkMimeType = (event) => {
    let files = event.target.files[0];
    
    const types = ["video/mp4","video/ogg"];
    let error = "";
    
    if (!types.includes(files.type)) {
      error = files.type + " is not a supported format\n";
      this.props.onuplaodVideo(error, "errors");
    } else {
      return true;
    }
  };  
  componentDidMount = async () => {     
  }

  render() {
    
    return (
      <React.Fragment>
        <div className="video_uploadclass">
            { 
              <React.Fragment>
                <input
                  type= "file"
                  name= "image"
                  accept= "video/mp4,video/ogg"
                  onChange={this.uplaodHandle}
                />
                <a href={this.props.value}>{this.props.value}</a>
              </React.Fragment>
            }
        </div>
      </React.Fragment>
    );
  }
}

export default UploadVideo;
