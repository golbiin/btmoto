import React, { Component } from "react";
class UploadThumb extends Component {
  state = {
    uploading: false,
    profile_image_thumb: "",
  };

  uplaodHandle = async (event) => {
    event.preventDefault();
    let files = event.target.files[0];
    if (files) {
      if (this.checkMimeType(event)) {
        let profile_image_thumb = URL.createObjectURL(event.target.files[0]);
        this.setState({ profile_image_thumb: profile_image_thumb });
        this.props.onUploadThumbnail(event, "profile_image_thumb");
      }
    }
  };
  checkMimeType = (event) => {
    let files = event.target.files[0];
    const types = ["image/png", "image/jpeg", "image/gif", "image/jpg"];
    let error = "";
    if (!types.includes(files.type)) {
      error = files.type + " is not a supported format\n";
      this.props.onUploadThumbnail(error, "errors");
    } else {
      return true;
    }
  };

  render() {
    const images = require.context("../../assets/images/admin", true);
    let id = this.props.id ? this.props.id : 'imagethum'; 
    return (
      <React.Fragment>
        <div className="profile_up">
          <label htmlFor={id}>
            {this.state.profile_image_thumb ? (
              <React.Fragment>
                <input
                  type="file"
                  name="imagethum"
                  id={id}
                  accept= "image/png, image/jpeg, image/gif, image/jpg "
                  onChange={this.uplaodHandle}
                  className="profile_img"
                />
                <img src={this.state.profile_image_thumb} className="img-fluid upload_img" />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <input
                  type="file"
                  name="imagethum"
                  id={id}
                  accept= "image/png, image/jpeg, image/gif, image/jpg "
                  onChange={this.uplaodHandle}
                  className="profile_img"
                />
                <img
                  src={
                    this.props.value
                      ? this.props.value
                      : images(`./upload-image.png`)
                  }
                  className="img-fluid pro_img"
                />
              </React.Fragment>
            )}
             <span className="upload-img-label">
            <i className="fa fa-camera" aria-hidden="true"></i>
          </span>
          </label>
        </div>
      </React.Fragment>
    );
  }
}

export default UploadThumb;
