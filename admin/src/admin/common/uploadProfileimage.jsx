import React, { Component } from "react";
class Uploadprofile extends Component {
  state = {
    uploading: false,
    profile_image: "",
  };
  setImage =(image) =>{
    this.setState({ profile_image: image });
  }
  uplaodHandle = async (event) => {
    event.preventDefault();
    let files = event.target.files[0];
    if (files) {
      if (this.checkMimeType(event)) {
        let profile_image = URL.createObjectURL(event.target.files[0]);
        this.setState({ profile_image: profile_image });
        this.props.onuplaodProfile(event, "profile_image");
      }
    }
  };
  checkMimeType = (event) => {
    let files = event.target.files[0];
    const types = ["image/png", "image/jpeg", "image/gif", "image/jpg"];
    let error = "";
    if (!types.includes(files.type)) {
      error = files.type + " is not a supported format\n";
      this.props.onuplaodProfile(error, "errors");
    } else {
      return true;
    }
  };

  render() {
    const images = require.context("../../assets/images/admin", true);
  //  console.log('elveee--->',this.props);
    return (
      <React.Fragment>
        <div className="profile_up">
          <label htmlFor="image">
            {this.state.profile_image ? (
              <React.Fragment>
                <input
                  type="file"
                  name="image"
                  id="image"
                  accept= "image/png, image/jpeg, image/gif, image/jpg "
                  onChange={this.uplaodHandle}
                  className="profile_img"
                />
                <img src={this.state.profile_image} className="img-fluid upload_img" />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <input
                  type="file"
                  name="image"
                  id="image"
                  accept= "image/png, image/jpeg, image/gif, image/jpg "
                  onChange={this.uplaodHandle}
                  className="profile_img"
                />
                <img
                  src={
                    this.props.value
                      ? this.props.value
                      : images(`./upload-image.png`)
                  }
                  className="img-fluid pro_img"
                />
              </React.Fragment>
            )}
             <span className="upload-img-label">
            <i className="fa fa-camera" aria-hidden="true"></i>
          </span>
          </label>
        </div>
      </React.Fragment>
    );
  }
}

export default Uploadprofile;
