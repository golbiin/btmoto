import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import { slide as Menu } from "react-burger-menu";
import * as authServices from "../../ApiServices/admin/login";
class Adminsidebar extends Component {
  state = {
    show: false,
    menuopen: false,
  };
  hideElement = () => {
    let cart_open = !this.state.menuopen;
    this.setState({ menuopen: cart_open }); 
  };
  isMenuOpen = (state) => {
    if (state.isOpen === false) {
      let menushow = "cart-on";
      if (this.state.slidemenu === "cart-on") {
        menushow = "cart-off";
      }
      this.setState({ slidemenu: menushow });
    }
  };
  shouldDisableOverlayClick = () => {
    return false;
  };
  toogleDropdown = () => {
    let toogled = !this.state.show;
    this.setState({ show: toogled, showSettings: false });
  };
  toogleSettingsDropdown = () => {
    let toogled = !this.state.showSettings;
    this.setState({ showSettings: toogled, show: false });
  };
  toogleDropdownCforms = () => {
    let toogled = !this.state.formSettings;
    this.setState({ formSettings: toogled, show: false });
  };
  
  toogleSettingsubDropdown = () => {
    let toogled = !this.state.showSettingsub;
    this.setState({ showSettingsub: toogled, show: false });
  };

  toogleContactDropdown = () => {
    let toogled = !this.state.showContact;
    this.setState({ showContact: toogled, show: false });
  };
  toogleUsersDropdown = () => {
    let toogled = !this.state.showUsers;
    this.setState({ showUsers: toogled, show: false });
  };
  toogleShippingDropdown = () => {
    let toogled = !this.state.showShipping;
    this.setState({ showShipping: toogled, show: false });
  };

  toogleEcommerceDropdown = () => {
    let toogled = !this.state.showEcommerce;
    this.setState({ showEcommerce: toogled, show: false });
  };

  toogleArticleDropdown = () => {
    let toogled = !this.state.showArticle;
    this.setState({ showArticle: toogled, show: false });
  };

  
  toogleDistributorDropdown = () => {
    let toogled = !this.state.showDistributor;
    this.setState({ showDistributor: toogled, show: false });
  };


  toogleIntegratorDropdown = () => {
    let toogled = !this.state.showIntegrator;
    this.setState({ showIntegrator: toogled, show: false });
  };


  toogleintributorDropdown = () => {
    let toogled = !this.state.showDistributor;
    this.setState({ showDistributor: toogled, show: false });
  };



  toogleCareerDropdown = () => {
    let toogled = !this.state.showCareer;
    this.setState({ showCareer: toogled, show: false });
  };

  toogleRedirectionDropdown = () => {
    let toogled = !this.state.showredirection;
    this.setState({ showredirection: toogled, show: false });
  };

  hideMenu = () => {
    this.setState({ menuopen: false });
  };
  Signout = () => {
    authServices.logout();
    this.props.props.history.push({
      pathname: "/admin/login",
    });
  };

  activeClass = (path) => {
    if (
      this.props.props.match.isExact &&
      this.props.props.match.path === path
    ) {
      return true;
    }
    return false;
  };

  render(props) {
    if (this.props.props.parent === "cms") {
      if (!this.state.show) {
        this.setState({show:true});
        
      }
    }

    const images = require.context("../../assets/images/admin", true);
    return (
      <React.Fragment>
        <div className="leftbar big-screen">
          <div className="logo-section">
            <div className="logo">
              <img
                src={images("./logo.png")}
                alt="logo"
                style={{ width: "150px" }}
              />
            </div>
            <div className="menubar"></div>
          </div>
          <div className="menu">
            <ul className="list ">
              <li
                className={
                  "list-item dashboard " +
                  (this.activeClass("/admin/home") ? "navigation--active" : "")
                }
              >
                <Link to="/admin/home">Dashboard</Link>
              </li>
              <li
                className={
                  (this.state.show
                    ? "list-item pages show-dropdown"
                    : "list-item pages hide-dropdown") +
                  (this.activeClass("/admin/pages")
                    ? " navigation--active"
                    : "") +
                  (this.activeClass("/admin/faq") ? " navigation--active" : "")
                }
              >
                <Link to="#" className="dropdown" onClick={this.toogleDropdown}>
                  CMS
                </Link>
                <div
                  className={
                    this.state.show
                      ? "pages-dropdown sub-item"
                      : "pages-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/pages">
                    Pages
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/faq">
                    Faq
                  </NavLink>

                  <NavLink activeClassName="active" to="/admin/contact/address">
                  Contact address
                  </NavLink>
                </div>
              </li>
              {/* <li
                className={
                  this.state.showContact
                    ? "list-item  pages show-dropdown"
                    : "list-item  pages hide-dropdown"
                }
              >
                <Link to="#" className="dropdown" onClick={this.toogleContactDropdown}>
                  Contacts
                </Link>
                <div
                  className={
                    this.state.showContact
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/contact/address">
                    Address
                  </NavLink>
                  <NavLink
                    activeClassName="active"
                    to="/admin/contact/subscriptions"
                  >
                    Subscriptions
                  </NavLink>
                </div>
              </li> */}
              <li
                className={
                  this.state.showUsers
                    ? "list-item  pages show-dropdown profile_user"
                    : "list-item  pages hide-dropdown profile_user"
                }
              >
                <Link className="dropdown" onClick={this.toogleUsersDropdown}>
                  Users
                </Link>
                <div
                  className={
                    this.state.showUsers
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/adminusers">
                    Admins
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/userslist">
                    Users
                  </NavLink>
                </div>
              </li>
              <li
                className={
                  this.state.showEcommerce
                    ? "list-item   show-dropdown side_bar_store"
                    : "list-item   hide-dropdown side_bar_store"
                }
              >
                <Link className="dropdown" onClick={this.toogleEcommerceDropdown}>
                  E-commerce
                </Link>
                <div
                  className={
                    this.state.showEcommerce
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <ul className="adminecommerce">
                    <li className="list-item side_bar_coupon">
                      <Link to="/admin/coupons">Coupons</Link>
                    </li>
                    <li
                      className={
                        "list-item orders " +
                        (this.activeClass("/admin/orders")
                          ? "navigation--active"
                          : "")
                      }
                    >
                      <Link to="/admin/orders">Orders</Link>
                    </li>
                    <li
                      className={
                        "list-item products " +
                        (this.activeClass("/admin/products")
                          ? "navigation--active"
                          : "")
                      }
                    >
                    <NavLink activeClassName="active" to="/admin/products">
                      Products
                    </NavLink>
                    </li>



                    <li
                      className={
                        "list-item products " +
                        (this.activeClass("/admin/settings/manage-attributes")
                          ? "navigation--active"
                          : "")
                      }
                    >
                      <NavLink activeClassName="active" to="/admin/settings/manage-attributes">
                      Attributes
                </NavLink>
                    </li>



                    <li
                      className={
                        this.state.showSettingsub
                          ? "list-item have-dropdown settings show-dropdown"
                          : "list-item have-dropdown settings hide-dropdown"
                      }
                    >
                    <Link
                    className="dropdown"
                    onClick={this.toogleSettingsubDropdown}
                    >
                    Settings
                    </Link>
                      <div
                        className={
                          this.state.showSettingsub
                            ? "users-dropdown sub-item"
                            : "users-dropdown sub-item disable"
                        }
                      >
                      <Link to="/admin/settings/manage-categories">
                      Manage Categories
                      </Link>
                        <Link to="/admin/stripesettings">Stripe</Link>
                      </div>
                    </li>
                    <li
                      className={
                        this.state.showShipping
                          ? "list-item have-dropdown  pages show-dropdown profile_user"
                          : "list-item  have-dropdown  pages hide-dropdown profile_user"
                      }
                    >
                      <Link
                        className="dropdown"
                        onClick={this.toogleShippingDropdown}
                      >
                        Shipping Methods
                    </Link>
                      <div
                        className={
                          this.state.showShipping
                            ? "users-dropdown sub-item"
                            : "users-dropdown sub-item disable"
                        }
                      >
                        <NavLink activeClassName="active" to="/admin/tax">
                          Tax
                    </NavLink>
                        <NavLink activeClassName="active" to="/admin/shipping">
                          UPS
                    </NavLink>
                      </div>
                    </li>
                  </ul>
                </div>
              </li>
              <li
                className={
                  "list-item pages " +
                  (this.activeClass("/admin/resources")
                    ? "navigation--active"
                    : "")
                }
              >
                <NavLink activeClassName="active" to="/admin/resources">
                  Resources
                </NavLink>
              </li>

              <li className="list-item dashboard">
                <Link to="/admin/uploads">Media</Link>
              </li>
              <li className="list-item side_bar_news">
                <Link to="/admin/archives">Archives</Link>
              </li>
  
             {/*  <li
                className={
                  this.state.showDistributor
                    ? "list-item  pages show-dropdown  "
                    : "list-item  pages hide-dropdown  "
                }
              >
                <Link className="dropdown" onClick={this.toogleDistributorDropdown}>
                Distributor Resources
                </Link>
                <div
                  className={
                    this.state.showDistributor
                      ? "users-dropdown sub-item" 
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/distributor-resources">
                  Distributor Resources
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/distributor-category">
                  Category
                  </NavLink>  

                   
                </div>
              </li>









              <li
                className={
                  this.state.showIntegrator
                    ? "list-item  pages show-dropdown  "
                    : "list-item  pages hide-dropdown  "
                }
              >
                <Link className="dropdown" onClick={this.toogleIntegratorDropdown}>
                Integrator Resources
                </Link>
                <div
                  className={
                    this.state.showIntegrator
                      ? "users-dropdown sub-item" 
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/integrator-resources">
                  Integrator Resources
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/integrator-category">
                  Category
                  </NavLink>  

                   
                </div>
              </li>
 */}



              {/* <li className="list-item side_bar_news">
                <Link to="/admin/integrator-resources">Integrator Resources</Link>
              </li> */}

              
              <li className="list-item side_bar_news">
                <Link to="/admin/news">News</Link>
              </li>

              {/* <li className="list-item side_bar_news">
                <Link to="/admin/articles">Articles</Link>
              </li> */}


              <li
                className={
                  this.state.showArticle
                    ? "list-item  pages show-dropdown  "
                    : "list-item  pages hide-dropdown  "
                }
              >
                <Link className="dropdown" onClick={this.toogleArticleDropdown}>
                Articles
                </Link>
                <div
                  className={
                    this.state.showArticle
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/articles">
                  Articles
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/source">
                  software
                  </NavLink>  

                   
                </div>
              </li>


              <li className="list-item side_bar_news">
                <Link to="/admin/casestudy">Case Study</Link>
              </li>
              {/* <li className="list-item side_bar_news">
                <Link to="/admin/international-distributor">International Distributor</Link>
              </li> */}

              <li className="list-item side_bar_news">
                <Link to="/admin/industry">Industries</Link>
              </li>
          

              
              <li
                className={
                  this.state.showUsers
                    ? "list-item  pages show-dropdown side_bar_news"
                    : "list-item  pages hide-dropdown side_bar_news"
                }
              >
                <Link className="dropdown" onClick={this.toogleUsersDropdown}>
                Live Trainings
                </Link>
                <div
                  className={
                    this.state.showUsers
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                   <NavLink activeClassName="active" to="/admin/attendee">
                   Users
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/live-training">
                  Live Trainings
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/event-locations">
                    Locations
                  </NavLink>
                </div>
              </li>

              {/* <li className="list-item side_bar_news">
                <Link to="/admin/live-training">Live Training</Link>
              </li> */}
              <li className="list-item side_bar_news">
                <Link to="/admin/video-training">Video Training</Link>
              </li>
              {/* <li className="list-item side_bar_careeer">
                <Link to="/admin/redirection">Redirection</Link>
              </li> */}

              <li
                className={
                  this.state.showredirection
                    ? "list-item  pages show-dropdown  "
                    : "list-item  pages hide-dropdown  "
                }
              >
                <Link className="dropdown" onClick={this.toogleRedirectionDropdown}>
                Redirection
                </Link>
                <div
                  className={
                    this.state.showredirection
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/redirection">
                  Redirection
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/ipredirection">
                  iP Redirection
                  </NavLink>  

                   
                </div>
              </li>








              <li
                className={
                  this.state.showCareer
                    ? "list-item  pages show-dropdown  "
                    : "list-item  pages hide-dropdown  "
                }
              >
                <Link className="dropdown" onClick={this.toogleCareerDropdown}>
                Careers
                </Link>
                <div
                  className={
                    this.state.showCareer
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/careers">
                  Careers
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/careers/category">
                  Category
                  </NavLink>  

                   
                </div>
              </li>





              <li className="list-item side_bar_store">
                <Link to="/admin/storelocator">Store Locator</Link>
              </li>             
              <li
                className={
                  this.state.showSettings
                    ? "list-item settings show-dropdown"
                    : "list-item settings hide-dropdown"
                }
              >
                <Link
                  className="dropdown"
                  onClick={this.toogleSettingsDropdown}
                >
                  Settings
                </Link>
                <div
                  className={
                    this.state.showSettings
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                 
                  <Link to="/admin/menus">Menus</Link>
                  <Link to="/admin/manage-profile">Profile</Link>
                  <Link to="/admin/manage-footer">Footer</Link>
                  <Link to="/admin/language">Language</Link>
                  <Link to="/admin/feedback">Feedback</Link>
                  <Link to="/admin/facebooksettings">Facebook</Link>
                  <Link to="/admin/linkedinsettings">LinkedIn</Link>
                  <Link to="/admin/manage-slider">Slider Option</Link>
                  
                </div>
              </li>
              <li
                className={
                  this.state.showUsers
                    ? "list-item  pages show-dropdown side_bar_news"
                    : "list-item  pages hide-dropdown side_bar_news"
                }
              >
                <Link className="dropdown " onClick={this.toogleDropdownCforms}>
                Btmoto Forms  
                </Link>
                <div
                  className={
                    this.state.formSettings
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/add-cforms">
                    Add New
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/list-cforms">
                    List
                  </NavLink>
                </div>
              </li>
            </ul>
     
          </div>
          <div className="signout">
            <i className="fa fa-sign-out" aria-hidden="true"></i>
            <Link to="#" onClick={this.Signout}>
              Sign out
            </Link>
          </div>
          <div className="sidebar_content">
            <p className="copyright">© 2020 BTMOTO All Rights Reserved</p>
          </div>
        </div>
        <div className="leftbar small-screen">
          <div className="logo-section">
            <div className="logo">
              <img src={images("./logo.png")} alt="logo" />
            </div>
            <div className="menubar" onClick={this.hideElement}></div>
          </div>
          <div className="menu">
            <div
              id={
                this.state.menuopen === true
                  ? "admin-menu-on"
                  : "admin-menu-off"
              }
            >
              <span className="close new" onClick={this.hideMenu}></span>
              <Menu
                isOpen={this.state.menuopen}
                onStateChange={this.isMenuOpen}
                right
                disableOverlayClick={() => this.shouldDisableOverlayClick}
                width={"380px"}
              >
                     <ul className="list ">
              <li
                className={
                  "list-item dashboard " +
                  (this.activeClass("/admin/home") ? "navigation--active" : "")
                }
              >
                <Link to="/admin/home">Dashboard</Link>
              </li>
              <li
                className={
                  (this.state.show
                    ? "list-item pages show-dropdown"
                    : "list-item pages hide-dropdown") +
                  (this.activeClass("/admin/pages")
                    ? " navigation--active"
                    : "") +
                  (this.activeClass("/admin/faq") ? " navigation--active" : "")
                }
              >
                <Link to="#" className="dropdown" onClick={this.toogleDropdown}>
                  CMS
                </Link>
                <div
                  className={
                    this.state.show
                      ? "pages-dropdown sub-item"
                      : "pages-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/pages">
                    Pages
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/faq">
                    Faq
                  </NavLink>
                </div>
              </li>
              <li
                className={
                  this.state.showContact
                    ? "list-item  pages show-dropdown"
                    : "list-item  pages hide-dropdown"
                }
              >
                <Link to="#" className="dropdown" onClick={this.toogleContactDropdown}>
                  Contacts
                </Link>
                <div
                  className={
                    this.state.showContact
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/contact/address">
                    Address
                  </NavLink>
                  <NavLink
                    activeClassName="active"
                    to="/admin/contact/subscriptions"
                  >
                    Subscriptions
                  </NavLink>
                </div>
              </li>
              <li
                className={
                  this.state.showUsers
                    ? "list-item  pages show-dropdown profile_user"
                    : "list-item  pages hide-dropdown profile_user"
                }
              >
                <Link className="dropdown" onClick={this.toogleUsersDropdown}>
                  Users
                </Link>
                <div
                  className={
                    this.state.showUsers
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/adminusers">
                    Admins
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/userslist">
                    Users
                  </NavLink>
                </div>
              </li>






              <li
                className={
                  this.state.showEcommerce
                    ? "list-item   show-dropdown side_bar_store"
                    : "list-item   hide-dropdown side_bar_store"
                }
              >
                <Link className="dropdown" onClick={this.toogleEcommerceDropdown}>
                  E-commerce
                </Link>
                <div
                  className={
                    this.state.showEcommerce
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >

                  <ul className="adminecommerce">

                    <li className="list-item side_bar_coupon">
                      <Link to="/admin/coupons">Coupons</Link>
                    </li>

                    <li
                      className={
                        "list-item orders " +
                        (this.activeClass("/admin/orders")
                          ? "navigation--active"
                          : "")
                      }
                    >
                      <Link to="/admin/orders">Orders</Link>
                    </li>


                    <li
                      className={
                        "list-item products " +
                        (this.activeClass("/admin/products")
                          ? "navigation--active"
                          : "")
                      }
                    >
                      <NavLink activeClassName="active" to="/admin/products">
                        Products
                </NavLink>
                    </li>

                    <li
                      className={
                        "list-item products " +
                        (this.activeClass("/admin/settings/manage-attributes")
                          ? "navigation--active"
                          : "")
                      }
                    >
                      <NavLink activeClassName="active" to="/admin/settings/manage-attributes">
                      Attributes
                </NavLink>
                    </li>


                    <li
                      className={
                        this.state.showSettingsub
                          ? "list-item have-dropdown settings show-dropdown"
                          : "list-item have-dropdown settings hide-dropdown"
                      }
                    >
                      <Link
                        className="dropdown"
                        onClick={this.toogleSettingsubDropdown}
                      >
                        Settings
                </Link>
                      <div
                        className={
                          this.state.showSettingsub
                            ? "users-dropdown sub-item"
                            : "users-dropdown sub-item disable"
                        }
                      >
                        <Link to="/admin/settings/manage-categories">
                          Manage Categories
                  </Link>

                        <Link to="/admin/stripesettings">Stripe</Link>
                      </div>
                    </li>
                    <li
                      className={
                        this.state.showShipping
                          ? "list-item have-dropdown  pages show-dropdown profile_user"
                          : "list-item  have-dropdown  pages hide-dropdown profile_user"
                      }
                    >
                      <Link
                        className="dropdown"
                        onClick={this.toogleShippingDropdown}
                      >
                        Shipping Methods
                    </Link>
                      <div
                        className={
                          this.state.showShipping
                            ? "users-dropdown sub-item"
                            : "users-dropdown sub-item disable"
                        }
                      >
                        <NavLink activeClassName="active" to="/admin/tax">
                          Tax
                    </NavLink>
                        <NavLink activeClassName="active" to="/admin/shipping">
                          UPS
                    </NavLink>
                      </div>
                    </li>
                  </ul>
                </div>
              </li>          
              <li
                className={
                  "list-item pages " +
                  (this.activeClass("/admin/resources")
                    ? "navigation--active"
                    : "")
                }
              >
                <NavLink activeClassName="active" to="/admin/resources">
                  Resources
                </NavLink>
              </li>
              <li className="list-item side_bar_news">
                <Link to="/admin/news">Articles</Link>
              </li>
              <li className="list-item side_bar_careeer">
                <Link to="/admin/careers">Careers</Link>
              </li>
              <li className="list-item side_bar_store">
                <Link to="/admin/storelocator">Store Locator</Link>
              </li>

              <li className="list-item calendar">
                <Link>Calendar</Link>
              </li>
              <li className="list-item chat_message">
                <Link>Chat & Messages</Link>
              </li>
              <li
                className={
                  this.state.showSettings
                    ? "list-item settings show-dropdown"
                    : "list-item settings hide-dropdown"
                }
              >
                <Link
                  className="dropdown"
                  onClick={this.toogleSettingsDropdown}
                >
                  Settings
                </Link>
                <div
                  className={
                    this.state.showSettings
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <Link to="/admin/menus">Menus</Link>
                  <Link to="/admin/language">Language</Link>
                  <Link to="/admin/manage-profile">Profile</Link>
                  <Link to="/admin/manage-footer">Footer</Link>
                </div>
              </li>

              <li
                className={
                  this.state.showUsers
                    ? "list-item  pages show-dropdown side_bar_news"
                    : "list-item  pages hide-dropdown side_bar_news"
                }
              >
                <Link className="dropdown " onClick={this.toogleDropdownCforms}>
                  Btmoto Forms  
                </Link>
                <div
                  className={
                    this.state.showCform
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <NavLink activeClassName="active" to="/admin/cforms">
                    Add New
                  </NavLink>
                  <NavLink activeClassName="active" to="/admin/cforms">
                    List
                  </NavLink>
                </div>
              </li>
            </ul>            
              </Menu>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Adminsidebar;
