import { apiUrl ,siteUrl} from "../../../src/config.json";
import * as settingsService from "../../ApiServices/admin/settings";
import { Component } from 'react';
const axios = require("axios").default;
class  MyUploadAdapter extends Component {
    constructor( loader , Allfiles ) {
        super(loader , Allfiles);
        this.loader = loader;
        this.files = Allfiles;
        this.upload = this.upload.bind(this);
        this.abort = this.abort.bind(this)
        this.state = {
          files: Allfiles,
        };
      }
     
_initListeners = (resolve, reject, file) => {  
        const data = new FormData();
        data.append( 'token', localStorage.getItem('admin_token'));
        data.append( '1233', 'asdasdsadsad' );
        data.append( 'url', 'dswc' ); 
        this.loader.file
        .then( uploadedFile => {
            return new Promise( ( resolve, reject ) => {
              data.append( 'uploadfile', uploadedFile ); 
                  if(resolve && file){
                    axios( {
                      url: apiUrl + '/admin/upload/imageupload',
                      method: 'post',
                      data,
                  } ).then( response => {
                    
                      if ( response.data.result === 'success' ) {
                          resolve( {
                              default: response.data.url
                          } );
                      } else {
                          reject( response.data.message );
                      }
                  } ).catch( response => {
                      reject( 'Upload failed' );
                  } );
              }
            });
        } );
  }  
     upload = async ()  => {
       return new Promise((resolve, reject ,file) => {
          const category_data = {
            image_id : this.loader.id,
            base64image: this.loader._reader._reader.result,
            typeOption: 'upload_image',           
          }  
          settingsService.uploadimageEditor(category_data)
          .then( uploadedFile => {
            if(uploadedFile.data.uploaded === 1){
              resolve({ default: uploadedFile.data.url });
            } else {
              resolve({ default: this.loader._reader._reader.result });
            }
            
          });
          });
      }    
      abort() {
      }
}

export default MyUploadAdapter;