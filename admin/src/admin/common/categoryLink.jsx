import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as manageProducts from "../../ApiServices/admin/products";
class CategoryLink extends Component {
    state = { 
        url : ''
    }
    componentDidMount = () => {
        this.getCurrentproductUrl(this.props.catid ,this.props.type);
    };
    getCurrentproductUrl = async (Cat_id , types ) => {
        let url = '';
        this.setState({ submit_status: true });
        const cimonProducts = { ...this.state.cimonProducts };
        const response =  await manageProducts.getCurrentproductUrl(Cat_id, types);
        if (response.data.status == 1) {
          this.setState({ submit_status: false });
  
          if(response.data.data !== '/null'){

            if(response.data.data.indexOf('introduction') < 0){ 
                url = response.data.data +'/'
                this.setState({
                  url : '/product' + url
                }) 
            } else {
                url = response.data.data +'/'
                this.setState({
                  url :  url
                })
            }
         
          } else {
            url = '/'
            this.setState({
              url :    '/product' + url
            })
          }
        
        }
      }
      render() { 
        return (
            <React.Fragment>
                <Link key={this.props.catid} target="_blank" className="dropdown-item" to={this.state.url}>View</Link> 
            </React.Fragment>
        );
    }
}


export default CategoryLink;