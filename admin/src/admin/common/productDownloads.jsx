import React, { Component } from "react";
class ProductDownloads extends Component {
  state = {
    uploading: false,
    profile_image: "",
  };
  uplaodHandle = async (event) => {
    event.preventDefault();
    let files = event.target.files[0];
    if (files) {
      if (this.checkMimeType(event)) {
        let profile_image = URL.createObjectURL(event.target.files[0]);
        this.setState({ profile_image: profile_image });
        this.props.onuplaodProfile(event, "profile_image");
      }
    }
  };
  checkMimeType = (event) => {
    let files = event.target.files[0];
    const types = ["application/pdf","application/msword" ,"application/octet-stream","application/zip" , "application/x-zip-compressed"];
    let error = "";
    console.log('checkMimeType files.type',files.type);
    if (!types.includes(files.type)) {
      error = files.type + " is not a supported format\n";
      console.log('checkMimeType',error);
      this.props.onuplaodProfile(error, "errors");
    } else {
      return true;
    }
  };
  render() {
    const images = require.context("../../assets/images/admin", true);
    return (
      <React.Fragment>
        <div className="profile_up">
            { 
              <React.Fragment>
                <input
                  type= "file"
                  name= "image"
                  accept= "application/msword, application/pdf , application/octet-stream , application/zip"
                  onChange={this.uplaodHandle}
                />
                <a href={this.props.value}>{this.props.value}</a>
              </React.Fragment>
            }
        </div>
      </React.Fragment>
    );
  }
}

export default ProductDownloads;
