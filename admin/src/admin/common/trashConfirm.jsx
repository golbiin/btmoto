import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
const TrashConfirm = props => {
  let modal = props.modal;
  const toggle = () => {
    modal = false;
    props.toogle();
  };
  const doSometing = () => {
    props.trash_action();
  };
  return (
    <div>
      <Modal isOpen={modal} toggle={toggle} className="delete-popup">
        <ModalHeader toggle={toggle}>
          {props.name ? props.name : "Trash"} Confirmation
        </ModalHeader>
        <ModalBody>
          {props.message
            ? props.message
            : "Are you sure you'd like to trash this item?"}
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={doSometing}>
            {props.name ? props.name : "Trash"}
          </Button>{" "}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default TrashConfirm;
