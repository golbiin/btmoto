import React, { Component } from "react";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import MyUploadAdapter from "./MyUploadAdapter";
class ClassicEditorContent extends Component {
  state = {
    editorContent: "",
  };
  onCashange  = data => {  
    this.setState({
        editorContent : data.getData()
    })
    this.props.onclassicEdit(data, "editorContent");
 }

  render() {
    const editorConfiguration = {       
       toolbar: [  
                'heading','bold', 'italic', 'link', 
                'fontSize', 'fontFamily', 'FontColor', 'fontBackgroundColor', 
                'bulletedList', 'numberedList', '|' ,
                'insertTable', 'tableColumn', 'tableRow', "mergeTableCells", '|', 
                "imageTextAlternative", "imageUpload",  "mediaEmbed", 
                "imageStyle:full", "imageStyle:side",
        ],
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
            ]
        },
        FontColor: {
          colors: [
              {
                  color: 'hsl(0, 0%, 0%)',
                  label: 'Black'
              },
              {
                  color: 'hsl(0, 0%, 30%)',
                  label: 'Dim grey'
              },
              {
                  color: 'hsl(0, 0%, 60%)',
                  label: 'Grey'
              },
              {
                  color: 'hsl(0, 0%, 90%)',
                  label: 'Light grey'
              },
              {
                  color: 'hsl(0, 0%, 100%)',
                  label: 'White',
                  hasBorder: true
              },

          ]
        },
        fontBackgroundColor: {
            colors: [
                {
                    color: 'hsl(0, 75%, 60%)',
                    label: 'Red'
                },
                {
                    color: 'hsl(30, 75%, 60%)',
                    label: 'Orange'
                },
                {
                    color: 'hsl(60, 75%, 60%)',
                    label: 'Yellow'
                },
                {
                    color: 'hsl(90, 75%, 60%)',
                    label: 'Light green'
                },
                {
                    color: 'hsl(120, 75%, 60%)',
                    label: 'Green'
                },
            ]
        },
        fontFamily: {
          options: [
          ],
          supportAllValues: true
        },
    };    
    return (
      <React.Fragment>
        <div className="row">
        <div className="col-md-12">
          <CKEditor
            editor = { ClassicEditor  }
            data = {
              this.props.value
                ? this.props.value
                : ""
            }
            config={ editorConfiguration }
            onInit = { editor => { 
              editor.plugins.get( 'FileRepository' ).createUploadAdapter = function( loader ) { 
              
              
              return new MyUploadAdapter( loader  , loader.file ); 
              };
          } }
          onChange = { ( event, editor ) => { 
            this.onCashange( editor );
          }}
        
          onFocus = { editor => { 
          } }  
          />
        </div>        
        </div>
      </React.Fragment>
    );
  }
}

export default ClassicEditorContent;
