import React, { Component } from "react";
import { Button, Modal, Tabs, Tab, Form } from 'react-bootstrap';
import * as mediaUploads from "../../ApiServices/admin/mediaFiles";
import * as settingsService from "../../ApiServices/admin/settings";

import { apiUrl, siteUrl } from "../../../src/config.json";
import ReactSpinner from 'react-bootstrap-spinner'
import * as manageProducts from "../../ApiServices/admin/products";
class CategoryShortcut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //  modal: false,
      // name: "",
      message : '',
      message_type:'',
      _id : '',
      categories_list: [],
      categories: [],
    };
  }
  getAllProductCategories = async () => {
    const response = await settingsService.getAllProductCategories();
    if (response.data.status == 1) {
      if (response.data.data) {
        this.setState({
          categories_list: response.data.data,
          //   categories : ['5ecfabcaede3a52210ebbe61' ,'5ecface6c4b76c3670148d6d','12311321']
        });
      }
    }
  };


  componentDidMount = async () => {
    this.getAllProductCategories();
    setTimeout(() => {
      this.getProductSingle(this.props.value);
    }, 1800);
  };


  /* Get single products */
  getProductSingle = async (id) => {
    
    this.setState({_id:id });

    const response = await manageProducts.getSingleProduct(id);
    if (response.data.status == 1) {
      var img_url = ""; // featured image
      var img_name = "";
      if (response.data.data.product_data !== null) {
      
        let newProductsarray = {
          _id: response.data.data.product_data._id,
          product_name: response.data.data.product_data.product_name,
          slug: response.data.data.product_data.slug,
         };
     
        this.setState({ cimonProducts: newProductsarray });
        
        const categories = response.data.data.product_data.categories;
        if (categories) {
          const categoriesvalue = this.state.categories_list
            .filter((s, sidx) => categories.indexOf(s._id) >= 0)
            .map(function (s) {
              return s._id;
            });
          this.setState({ categories: categoriesvalue });
        }
        const primary_category =
          response.data.data.product_data.primary_category;
        if (primary_category) {
          this.setState({ primary_category: primary_category });
          this.getCurrentproductUrl(primary_category)
        }


      }
    }
  };



  handleCheckChieldElement = (event) => {
    let clickedValue = event.target.value;

    if (event.target.checked) {
      const list = [...this.state.categories, clickedValue];

      if (list.indexOf(this.state.primary_category) < 0) {
        // setting Primary Category default option

        this.getCurrentproductUrl(list[0]);
        this.setState({
          primary_category: list[0],
        });
      }

      this.setState({
        categories: list,
      });
    } else {
      let categories = [...this.state.categories]; // make a separate copy of the array
      let index = categories.indexOf(event.target.value);
      if (index !== -1) {
        categories.splice(index, 1);
        this.setState({ categories: categories });
      }
      if (categories.indexOf(this.state.primary_category) < 0) {
        // setting Primary Category default option
        this.getCurrentproductUrl(categories[0]);
        this.setState({
          primary_category: categories[0],
        });
      }
    }
  };


  
    /* Form Submit */
    handleSubmit = async () => {
     
      console.log('categories_list', this.state.categories, this.state.url , this.state.primary_category);
  
      this.setState({ submit_status: true });
    //  cimonProducts["primary_category"] = this.state.primary_category;
 
      const checkState = {
        categories:  this.state.categories,
        _id: this.state._id ,
        url : this.state.url,
        primary_category : this.state.primary_category,
        slug : this.state.cimonProducts.slug
      };

      const response = await manageProducts.updateCategoryShort(checkState);
      if (response) {
        if (response.data.status === 1) {
          this.setState({ spinner: false });
          this.setState({
            message: response.data.message,
            message_type: "success",
          });
          this.setState({ submit_status: false });
          this.triggerParentUpdate();
        } else {
          this.setState({ spinner: false });
          this.setState({ submit_status: false });
          this.setState({
            message: response.data.message,
            message_type: "error",
          });
        }
      }
  };

  SetPrimaryCat = (Cat_id) => {
    this.setState({ primary_category: Cat_id });
    this.getCurrentproductUrl(Cat_id);
  };

  triggerParentUpdate = () => {
    
    this.props.triggerParentUpdate();
  }

  getCurrentproductUrl = async (Cat_id) => {
    let url = "";
    this.setState({ submit_status: true });
    const cimonProducts = { ...this.state.cimonProducts };
    const response = await manageProducts.getCurrentproductUrl(Cat_id);
    if (response.data.status == 1) {
      this.setState({ submit_status: false });

      if (response.data.data !== "/null") {
        url = this.checkurl(response.data.data);
        this.setState({
          url: url,
        });
      } else {
        url = "/product/";
        this.setState({
          url: url,
        });
      }
    }
  };

   /* set product url on admin side (main category and sub category)*/

   checkurl = (url) => {
    let productUrl;
    if (url.indexOf("scada") !== -1) {
      url = "/introduction/scada";
      productUrl = siteUrl + url;
    } else {
      url = "/product" + url + "/";
      productUrl = siteUrl + url + this.state.cimonProducts.slug;
    }

    this.setState({ productUrl: productUrl });
    return url;
  };

  render() {
   const { show, showTitleEdit, modalClose, show_value } = this.props;
    const newrowId = this.props.newrowId;

    return (

      
      <p>
        <h3>Categories</h3>
        {this.state.categories_list.map((categorysingle) => (
          <div
            key={categorysingle._id}
            slug={categorysingle.slug}
            className={
              "checkboxloop parent_" +
              categorysingle.parent_id
            }
          >
            <input
              key={categorysingle._id}
              onChange={this.handleCheckChieldElement}
              checked={
                this.state.categories.indexOf(
                  categorysingle._id
                ) > -1
                  ? true
                  : false
              }
              value={categorysingle._id}
              type="checkbox"
            />{" "}
            {categorysingle.category_name}
            {(() => {
              if (
                this.state.categories.indexOf(
                  categorysingle._id
                ) > -1 &&
                this.state.primary_category ===
                categorysingle._id
              ) {
                return (
                  <a
                    onClick={() =>
                      this.SetPrimaryCat(categorysingle._id)
                    }
                    className="Primat_cat"
                  >
                    Primary
                  </a>
                );
              } else if (
                this.state.categories[0] ===
                categorysingle._id &&
                this.state.primary_category === ""
              ) {
                return (
                  <a
                    onClick={() =>
                      this.SetPrimaryCat(categorysingle._id)
                    }
                    className="Primat_cat"
                  >
                    Primary
                  </a>
                );
              } else if (
                this.state.categories.indexOf(
                  categorysingle._id
                ) > -1
              ) {
                return (
                  <a
                    onClick={() =>
                      this.SetPrimaryCat(categorysingle._id)
                    }
                    className="Primat_cat"
                  >
                    Make Primary
                  </a>
                );
              }
            })()}
          </div>
        ))}


        <div >
          <button
            disabled={
              this.state.submit_status === true
                ? "disabled"
                : false
            }
            onClick={this.handleSubmit}
            className="btn btn-info"
          >
            {this.state.submit_status ? (
              <ReactSpinner
                type="border"
                color="dark"
                size="1"
              />
            ) : (
                ""
              )}
                                Update
                              </button>
                              {this.state.message !== "" ? (
                          <p className="tableinformation">
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          </p>
                        ) : null}
        </div>
      </p>


    );

  }
}

export default CategoryShortcut;





