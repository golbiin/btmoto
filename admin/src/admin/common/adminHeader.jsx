import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as authServices from "../../ApiServices/admin/login";
class Adminheader extends Component {
  state = {
    title : 'Dashboard',
    username : '',
    _id: ''
  };
  Signout = () => {
    authServices.logout();
     
  };

  componentDidMount = () => {
    this.getCurrentUser();
    if(this.props.props.title){
      this.setState({ title: this.props.props.title});
    }
  };

  getCurrentUser = async () => {
    const response =  await authServices.getCurrentUser();
      if(response){
        if(response.email){
          const response1 =  await authServices.getAdminDetails(response.email);
          if(response1){
             
            if(response1.data.data.hasOwnProperty('username')){
                this.setState({ username: response1.data.data.username});
                this.setState({ _id: response1.data.data._id});
            }   
          }
        }
      }
  };
  render(props) {
    let pagetitle = 'Dashboard';
    if (this.state.title) {
      pagetitle = this.state.title;
    }
    const images = require.context("../../assets/images/admin", true);
    return (
      <React.Fragment>
        <div className="row header-container">
          <div className="col-md-12 tob-bar">
            <div className="inner_box_header">
              <p>Welcome</p>
              <div className="breadcum">
               { pagetitle }
              </div>
            </div>
            <div className="actions">
              <div className="dropdown">
                <a className="profile dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                  <img src={images("./profile_user.png")} alt="profile-img"  />
                  <span>{this.state.username}</span>
                </a>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <Link to ={{ pathname: '/admin/profile/'+this.state._id, state: { id: '1'} }} className="dropdown-item">
                  Edit My Profile
                  </Link>
                  <Link to="#" onClick={this.Signout} className="dropdown-item">
                  Log Out
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Adminheader;
