import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
const DeleteConfirm = (props) => { 
  let modal = props.modal;
  const toggle = () => {
    modal = false;
    props.toogle();
  };
  const doSometing = () => {
    props.deleteUser(props.action);
  };
  return (
    <div>
      <Modal isOpen={modal} toggle={toggle} className="delete-popup">
        <ModalHeader toggle={toggle}>{(props.name ? props.name : "Delete")} Confirmation</ModalHeader>
        <ModalBody>{(props.message ? props.message : "Are you sure you'd like to delete this item?")}</ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={doSometing}>
          {(props.name ? props.name : "Delete")}
          </Button>{" "}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default DeleteConfirm;
