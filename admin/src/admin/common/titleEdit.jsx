import React, { Component } from "react";
import { Button, Modal, Tabs, Tab, Form } from 'react-bootstrap';
import * as mediaUploads from "../../ApiServices/admin/mediaFiles";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import Moment from 'react-moment';
import 'react-image-crop/lib/ReactCrop.scss';
import ReactCrop from 'react-image-crop';
class TitleEdit extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      src: null,
      crop: {
        unit: '%',
        width: 30,
        aspect: 16 / 9,
      },
      copyFileUrl: '', 
      copied: false,
      modalInputName: "",
      newrowId:"",
      file_name:"",
      submitStatus: false,
      spinner: true,
      message: "",
      message1: "",
      responsetype:"",
      file_details: {
        originalname: '',
        encoding: '',
        mimetype: '',
        filename: '',
        size: ''
      },
      file_info: {
        file_url: '',
        file_name: '',
        created_on: '',
        file_type: '',
        
      }
    };
  }

  componentDidMount() {
    const newrowId= this.props.newrowId;
    this.setState({newrowId: newrowId });
    console.log(2222,newrowId);
    this.getMedia(newrowId);

    const reader = new FileReader()
    reader.addEventListener(
      'load',
      () =>
        this.setState({
          src: reader.result,
        }),
      false
    )
  //  reader.readAsDataURL(this.state.src)
   // this.onSelectFile(this.state.imgUrl);
   this.setState({ src:this.state.imgUrl })
  }
  /*Crop section */
  // onSelectFile = e => {
  //   if (this.state.imgUrl) {
  //     const reader = new FileReader();
  //     reader.addEventListener('load', () =>
  //       this.setState({ src: reader.result })
  //     );
  //     reader.readAsDataURL(this.state.imgUrl);
  //   }
  // };
  onSelectFile = e => {
    console.log(45646,e.target.files[0]);
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader()
      reader.addEventListener(
        'load',
        () =>
          this.setState({
            src: reader.result,
          }),
        false
      )
      reader.readAsDataURL(e.target.files[0])
    }
  }
  // If you setState the crop in here you should return false.
  onImageLoaded = image => {
    this.imageRef = image;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    // You could also use percentCrop:
    // this.setState({ crop: percentCrop });
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        'newFile.jpeg'
      );
      this.setState({ croppedImageUrl });
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          //reject(new Error('Canvas is empty'));
          console.error('Canvas is empty');
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = window.URL.createObjectURL(blob);
        resolve(this.fileUrl);
      }, 'image/jpeg');
    });
  }
  /*crop section */


  handleChange(e) {
    console.log(5555,e);
    const target = e.target;
    const file_name = target.name;
    const value = target.value;
    console.log(5555,target,file_name,value);
    this.setState({
      file_name: value
    });
  }


  /* Get all files*/
  getMedia = async (newrowId) => {
    console.log(1111,newrowId,333,this.state.newrowId)
    this.setState({ blocking: true });
    try {
      const response = await mediaUploads.getNameById(newrowId);
      if (response.status === 200) {


        this.setState({file_details: response.data.file_details})
        this.setState({file_info: response.data.singleDetails });
        this.setState({ file_name: response.data.singleDetails.file_name });       
        this.setState({ copyFileUrl: response.data.singleDetails.file_url });       
     
        this.setState({ blocking: false });
      } else {
        // this.props.history.push({
        //    pathname: "/career-center",
        // });
      }
    } catch (err) {
      this.setState({ blocking: false });
    }
  };

  handleSubmit = async (e) => {
   
    const errors = { ...this.state.errors };
    const filename = this.state.file_name;

    if (filename) {
      this.setState({ spinner: true });
      const data = {
        file_name: this.state.file_name,
        id :this.state.newrowId
      }
      try{  
        this.setState({ submit_status: true });
        const response = await mediaUploads.updateTitle(data);
        
        if (response.data.status === 1) {
          this.setState({
              submitStatus: false,
              message: response.data.message,
              responsetype: "success",
              spinner: false,
                file_name:""
          });     
        } else {
          this.setState({
              submitStatus: false,
              spinner: false,
              message: response.data.message,
              responsetype: "error",
          });
        }
        setTimeout(() => { 
          this.setState(() => ({message: ''}))
        }, 3000);
        window.location.reload(false);

      } catch (err) {
        this.setState({
            message: 'something went wrong',
            responsetype: "error",
            spinner: false ,
            submit_status : false
        });
      }
    }    
  };
  getImage = (row) => {

    console.log(row);
    const images = require.context("../../assets/images", true);
    let imgUrl = images(`./admin/document.png`);
    if (row.file_type === 'image' ) {
      imgUrl = (row.file_url);
    } else if ( row.file_type == 'audio') {
      imgUrl = images(`./admin/audio.png`)
    } else if ( row.file_type == 'video') {
      imgUrl = images(`./admin/video.png`)
    } else if ( row.file_type == 'document') {
      imgUrl = images(`./admin/document.png`)
    } else if ( row.file_type == 'archive') {
      imgUrl = images(`./admin/archive.png`)
    }

    return (<img className='img-fluid media-cimon' src={imgUrl} />)
  }
  onCopy = () => {
    this.setState({copied: true});
  };
  render() {
    const { show,showTitleEdit,modalClose } = this.props;
    const newrowId= this.props.newrowId;
    const { crop, croppedImageUrl, src } = this.state;
    return (
      <div className="titleBox">
        <Modal
            show={show}   
            onHide={modalClose}      
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"  
            centered
            dialogClassName="contact-distibutor-model"
            className="media-edit-modal"
          // onHide={onHide}
          >
            <Modal.Header closeButton>
              <h3 className="contact_head"><label>Media Title</label></h3>
            </Modal.Header>
            <Modal.Body>
              <div id="TitleEdit" >
                <div className="container">
                  <div className="modal-body">
                    <div className="row">
                      <div className="col-md-5 media-edit-left">

                        {this.getImage(this.state.file_info)}
                        {/* {this.state.file_details.mimetype} */}
                        <div className="App">
        <div>
          {/* <input type="file"  accept="image/*" onChange={this.onSelectFile} /> */}
        </div>
        {src && (
          <ReactCrop
            src={src}
            crop={crop}
            ruleOfThirds
            onImageLoaded={this.onImageLoaded}
            onComplete={this.onCropComplete}
            onChange={this.onCropChange}
          />
        )}
        {croppedImageUrl && (
          <img alt="Crop" style={{ maxWidth: '100%' }} src={croppedImageUrl} />
        )}
      </div>
                      </div>
                      <div className="col-md-7">
                      <div className="details">
                        <h4 className="screen-reader-text">Details</h4>
                        <div className="uploaded"><strong>Uploaded on: </strong>                         
                        <Moment format="D MMM YYYY">
                        {this.state.file_info.created_on} 
                        </Moment>
                        </div>
                        {/* {this.state.file_details.mimetype} */}
                        <div className="filename"><strong>File name:</strong> {this.state.file_info.file_name}</div>
                        {this.state.file_details.mimetype != '' ?
                        <div className="file-type" ngIf="this.state.file_details" ><strong>File type:</strong> {this.state.file_details.mimetype}</div> : '' }
                        {this.state.file_details.size ? 
                        <div className="file-size" ><strong>File size:</strong> {this.state.file_details.size } KB</div> : '' }
                      </div>
                        <form>
                          <div className="form-group"><label>Name:</label>
                            <input type="text"
                              value={this.state.file_name?this.state.file_name:""}
                              name="modalInputName"
                              onChange={e => this.handleChange(e)}
                              className="form-control"
                              required
                            />     
                          </div>
                          <div className="form-group"><label>File Url:</label>
                            <input type="text"
                              value={this.state.copyFileUrl}                            
                              className="form-control"
                              disabled
                            /> 
                            <CopyToClipboard
                              onCopy={this.onCopy}
                              options={{message: 'Whoa!'}}
                              text={this.state.copyFileUrl}>
                              <button className="copy-to-btn" type="button" >Copy URL to clipboard</button>
                            </CopyToClipboard>   
                            <p class="success-copy">{ this.state.copied ? 'Copied': ''}</p> 
                          </div>
                        </form>
                      </div>
                    </div>                    
                  </div>                  
                </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <div className="row">
              <div className="col-md-12">
              {this.state.message !== "" ? (
                <div className="tableinformation"><div className={this.state.responsetype} >
                    <p>{this.state.message}</p>
                </div></div>
              ) : null} 
              </div>
            </div>
            <button type="button" class="btn btn-outline-dark" id="submitedits" onClick={e => this.handleSubmit(e)} >Save changes</button>           
          </Modal.Footer>
        </Modal>           
      </div>
    );
	}
}

export default TitleEdit;

  



