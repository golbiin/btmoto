import React, { Component } from "react";
import ReactSpinner from "react-bootstrap-spinner";
class MultipleUploadImages extends Component {
  state = {
    uploading: false,
    video_url: ""
  };

  uplaodHandle = async event => {
    event.preventDefault();
    let files = event.target.files[0];
    if (files) {
      let video_url = URL.createObjectURL(event.target.files[0]);
      this.setState({ video_url: video_url });
      this.props.onMultipleupload(event, "video_url", this.props.idx);
    }
  };
  checkMimeType = event => {
    let files = event.target.files[0];
    const types = ["video/mp4", "video/ogg"];
    let error = "";
    if (!types.includes(files.type)) {
      error = files.type + " is not a supported format\n";
      this.props.onMultipleupload(error, "errors", this.props.idx);
    } else {
      return true;
    }
  };

  componentDidMount = async () => {};

  render() {
    return (
      <React.Fragment>
        <div className="video_uploadclass">
          {
            <React.Fragment>
              {this.props.value ? (
                this.props.value
                  .split(/[#?]/)[0]
                  .split(".")
                  .pop()
                  .trim() !== "mp4" ? (
                  <img
                    alt="multi upload image preview"
                    width="150"
                    className="img-fluid"
                    src={this.props.value}
                  />
                ) : (
                  <React.Fragment>
                    <a href={this.props.value} target="_blank">
                      {this.props.value}
                    </a>
                    <br />
                  </React.Fragment>
                )
              ) : (
                ""
              )}
              <input type="file" name="image" onChange={this.uplaodHandle} />
            </React.Fragment>
          }
          {this.props.imagestatus === true ? (
            <ReactSpinner type="border" color="dark" size="1" />
          ) : (
            ""
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default MultipleUploadImages;
