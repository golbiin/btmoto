import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import ReactSpinner from 'react-bootstrap-spinner'
import Joi from 'joi-browser'
import * as settingsService from '../../ApiServices/admin/settings'
import DataTable from 'react-data-table-component'
import { CSVLink, CSVDownload } from 'react-csv'
class FeedBack extends Component {
  constructor (props) {
    super(props)
  }
  state = {
    editorContent: '',
    checked: false,
    id: '',

    language: '',
    code: '',
    url: '',
    languagestatus: '',

    errors: {},
    submit_status: false,
    message: '',
    message_type: '',
    focused: false,
    addresses: [],
    data_table: [],
    table_message: '',
    table_message_type: '',
    delete_status: false,
    edit_status: false,
    spinner: true,
    search: '',
    columns: [
      {
        name: 'user',
        selector: 'users',
        sortable: true,
        width: '20%'
      },

      {
        name: 'category',
        selector: 'feedback',
        sortable: true,
        width: '20%'
      },
      {
        name: 'description',
        selector: 'description',
        sortable: true,
        width: '40%'
      },
      {
        name: 'review',
        selector: 'review',
        width: '10%',
        sortable: true
      },

      {
        name: 'Action',
        width: '10%',
        cell: row => (
          <div id={row._id} className='action_dropdown' data-id={row._id}>
            <i
              data-id={row._id}
              className='dropdown_dots'
              data-toggle='dropdown'
              aria-haspopup='true'
              aria-expanded='false'
            ></i>
            <div
              className='dropdown-menu dropdown-menu-center'
              aria-labelledby='dropdownMenuButton'
            >
              {/* <a
                onClick={() => this.getLanguageById(row._id)}
                className="dropdown-item"
              >
                Edit
              </a> */}
              <a
                onClick={() => this.handleRemoveFeedback(row._id)}
                className='dropdown-item'
              >
                Delete
              </a>
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: '56px' // custom width for icon button
      }
    ]
  }

  /* Joi validation schema */
  schema = {
    email: Joi.string()
      .required()
      .error(() => {
        return {
          message: 'This field is a required field.'
        }
      }),
 
  }

  componentDidMount = () => {
    this.getAllFeedBack()
    this.getFeedbackEmail()
  }

  /*Get all Languages */
  getAllFeedBack = async () => {
    this.setState({ spinner: true })
    try {
      const response = await settingsService.getAllFeedBack()
      console.log('get all languages', response)
      if (response.data.status == 1) {
        this.setState({ addresses: response.data.data })
        const Setdata = { ...this.state.data_table }
        const data_table_row = []
        response.data.data.map((feedback, index) => {
          const Setdata = {}
          Setdata.users = feedback.users.email
          Setdata.feedback = feedback.feedback
          Setdata.description = feedback.description
          Setdata.review = feedback.review
          Setdata._id = feedback._id
          data_table_row.push(Setdata)
        })
        this.setState({
          data_table: data_table_row
        })
      }
      this.setState({ spinner: false })
    } catch (err) {
      this.setState({ spinner: false })
    }
  }

  /* get language by id */
  getLanguageById = async id => {
    const response = await settingsService.getLanguageById({ id: id })
    if (response.data.status == 1) {
      this.setState({
        edit_status: true,
        submit_status: false,
        message: '',
        message_type: '',
        delete_status: false,
        language: response.data.data.language,
        code: response.data.data.code,
        url: response.data.data.url,
        languagestatus: response.data.data.languagestatus,
        id: response.data.data._id
      })
    }
  }

  addLanguage = () => {
    this.setState({
      language: '',
      code: '',
      url: '',
      languagestatus: '1',
      attributes: [],
      errors: {},
      submit_status: false,
      message: '',
      message_type: '',
      delete_status: false,
      edit_status: false,
      descrption: ''
    })
  }

  /*delete languages */
  handleRemoveFeedback = async id => {
    this.setState({
      delete_status: true
    })
    const response = await settingsService.removeFeedback({ _id: id })
    console.log('removeLanguage', response)
    if (response.data.status === 1) {
      let newLang = [...this.state.data_table]
      newLang = newLang.filter(function (obj) {
        return obj._id !== id
      })
      this.setState({
        table_message: response.data.message,
        table_message_type: 'sucess'
      })

      this.setState({
        data_table: newLang,
        delete_status: false,
        edit_status: false
      })
      setTimeout(() => {
        this.setState(() => ({ table_message: '' }))
      }, 5000)
    } else {
      this.setState({
        table_message: response.data.message,
        table_message_type: 'sucess'
      })
      setTimeout(() => {
        this.setState(() => ({ table_message: '' }))
      }, 5000)
    }
  }



    /*Get all Languages */
    getFeedbackEmail = async () => {
      this.setState({ spinner: true })
      try {
        const response = await settingsService.getFeedbackEmail()
        console.log('get all languages', response)
        if (response.data.status == 1) {
        
          this.setState({ email:   response.data.data[0].value })
          
        }
         
        this.setState({ spinner: false })
      } catch (err) {
        this.setState({ spinner: false })
      }
    }
  
  /* Input Handle Change */
  handleChange = input => event => {
    const errors = { ...this.state.errors }
    delete errors.validate
    const errorMessage = this.validateProperty(input, event.target.value)
    if (errorMessage) errors[input] = errorMessage
    else delete errors[input]
    let value = event.target.value
    this.setState({ [input]: value, errors })
  }

  /*Joi Validation Call*/
  validateProperty = (name, value) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)
    return error ? error.details[0].message : null
  }

  /* Form Submit */
  handleSubmit = async e => {
    e.preventDefault()
    const checkState = {
      email: this.state.email,
    }
    const errors = { ...this.state.errors }
    this.setState({ errors: {} })
    let result = Joi.validate(checkState, this.schema)

    console.log('Current result ', result, checkState)
    if (result.error) {
      let path = result.error.details[0].path[0]
      let errormessage = result.error.details[0].message
      errors[path] = errormessage
      this.setState({ errors: errors })
    } else {
       
      try {
        this.setState({
          submit_status: true,
          message: '',
          message_type: ''
        })
       

        const response = await settingsService.updatefeedbacksettings(checkState)
         
        if (response) {
          this.setState({ submit_status: false })
          if (response.data.status === 1) {
            this.setState({
              message: response.data.message,
              message_type: 'success',
              edit_status: false
            })
           
          } else {
            this.setState({
              message: response.data.message,
              message_type: 'error'
            })
          }
        } else {
          this.setState({ submit_status: false })
          this.setState({
            message: 'Something went wrong! Please try after some time',
            message_type: 'error'
          })
        }


      } catch (err) {}
    }
  }

  /* Select languagestatus */
  handleLanguageStatus = async e => {
    this.setState({ languagestatus: e.target.value })
    const errors = { ...this.state.errors }
    delete errors.validate
    const errorMessage = this.validateProperty('languagestatus', e.target.value)
    if (errorMessage) errors['languagestatus'] = errorMessage
    else delete errors['languagestatus']
    this.setState({ errors: errors })
  }

  searchSpace = event => {
    let keyword = event.target.value

    this.setState({ search: keyword })
  }

  render () {
    let datarow = this.state.data_table

    let search = this.state.search

    if (search.length > 0) {
      search = search.trim().toLowerCase()
      console.log('search', search, datarow)
      datarow = datarow.filter(l => {
        return (
          l.users.toLowerCase().match(search) ||
          l.feedback.toLowerCase().match(search) ||
          l.description.toLowerCase().match(search) ||
          l.review.toLowerCase().match(search)
        )
      })
    }

    return (
      <React.Fragment>
        <div className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents main_admin_page_common  useradd-new pt-2 pb-4 pr-4'>
                  <div className='form-group menu_container' style={{'padding-bottom' : '0px' , 'margin-bottom' : '0px'}}>
                    <label>Email</label>
                    <div className='create_menu_container'>
                      <input
                        className='form-control'
                        type='text'
                        value={this.state.email}
                        onChange={this.handleChange('email')}
                      />
                      <button onClick={this.handleSubmit}>Update</button>

                      {this.state.errors.email ? (
                        <div className='danger'>{this.state.errors.email}</div>
                      ) : (
                        ''
                      )}
                    </div>
                    <div className="tableinformation">
                                {this.state.message !== "" ? (
                                  <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                  </div>
                                ) : null}
                              </div>
                  </div>

                  <div className='categories-wrap p-4'>
                    <div className=' admin_breadcum'>
                      <div
                        className='row'
                        style={{
                          width: '100%',
                          background: 'transparent',
                          'margin-bottom': '25px',
                          'margin-top': '20px'
                        }}
                      >
                        <div className='col-md-4'>
                          <CSVLink
                            filename={'feedback.csv'}
                            style={{
                              'margin-left': '25px',
                              'margin-top': '15px'
                            }}
                            className='Export_data_btn'
                            data={datarow}
                          >
                            
                            EXPORT
                          </CSVLink>
                        </div>
                        <div className='col-md-8'>
                          <div className='searchbox'>
                            <div className='commonserachform'>
                              <span></span>
                              <input
                                type='text'
                                placeholder='Search'
                                onChange={e => this.searchSpace(e)}
                                name='search'
                                className='search form-control'
                              />
                              <input type='submit' className='submit_form' />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className='col-md-12'>
                        <div className='card'>
                          <div className='card-header'>
                            <strong>Feedback</strong>
                          </div>
                          <div className='card-body categories-list  new_search'>
                            <React.Fragment>
                              <div
                                style={{
                                  display:
                                    this.state.spinner === true
                                      ? 'flex'
                                      : 'none'
                                }}
                                className='overlay'
                              >
                                <ReactSpinner
                                  type='border'
                                  color='primary'
                                  size='10'
                                />
                              </div>
                            </React.Fragment>
                            <p className='tableinformation'>
                              {this.state.table_message !== '' ? (
                                <div className={this.state.table_message_type}>
                                  <p>{this.state.table_message}</p>
                                </div>
                              ) : null}
                            </p>
                            <DataTable
                              columns={this.state.columns}
                              data={datarow}
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              noDataComponent={
                                <p>There are currently no records.</p>
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default FeedBack
