import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import * as settingsService from "../../ApiServices/admin/liveTraining";
import { apiUrl ,siteUrl} from "../../config.json";
import BlockUi from 'react-block-ui';
class adminEditEventLocations extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    spinner: false,
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    eventLocation: {
      location_name: "",
      description: "",
      address: "",
      latitude: "",
      longitude: ""
    },
    validated: false,
    locationStatus: true,
    focused: false
  };

  /*Joi validation schema*/

  schema = {
    _id: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    location_name: Joi.string().allow(""),
    // .required()
    // .error(() => {
    //   return {
    //     message: "This field is a required field.",
    //   };
    // }),
    description: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),

    address: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field"
        };
      }),
    latitude: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field"
        };
      }),
    longitude: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field"
        };
      })
  };

  /* Input Handle Change */
  handleChange = (event, type = null) => {
    let eventLocation = { ...this.state.eventLocation };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateEventLocation(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    eventLocation[name] = value;
    this.setState({ eventLocation, errors });
  };
  validateEventLocation = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Get single Training */
  componentDidMount = async () => {
    this.setState({spinner:true});
    const id = this.props.match.params.id;
    const response = await settingsService.getSingleEventLocation(id);
    if (response.data.status == 1) {
      let eventLocation = {
        _id: response.data.data.data._id,
        location_name: response.data.data.data.location_name,
        description: response.data.data.data.description,
        address: response.data.data.data.address,
        latitude: response.data.data.data.latitude,
        longitude: response.data.data.data.longitude
      };
      this.setState({ eventLocation: eventLocation });
    } else {
      this.setState({
        locationStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
    this.setState({spinner:false});
  };

  /* Form Submit */
  handleSubmit = async () => {
    const eventLocation = { ...this.state.eventLocation };
    const errors = { ...this.state.errors };

    let result = Joi.validate(eventLocation, this.schema);
    console.log(result);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });
        this.updateEventLocation();
      }
    }
  };

  /* Update News */
  updateEventLocation = async () => {
    const eventLocation = { ...this.state.eventLocation };
    this.setState({ eventLocation });
    const response = await settingsService.updateEventLocation(eventLocation);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                    <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className="row addpage-form-wrap">
                      <div className="col-lg-8 col-md-12">
                        <div className="form-group">
                          <label htmlFor="">Update</label>
                          <input
                            name="location_name"
                            value={this.state.eventLocation.location_name}
                            onChange={e => this.handleChange(e)}
                            type="text"
                            placeholder="Location Name *"
                            className="form-control"
                          />
                          {this.state.errors.location_name ? (
                            <div className="error text-danger">
                              {this.state.errors.location_name}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Description</label>
                          <textarea
                            name="description"
                            onChange={this.handleChange}
                            value={this.state.eventLocation.description}
                            className="form-control"
                          ></textarea>
                          {this.state.errors.description ? (
                            <div className="error text-danger">
                              {this.state.errors.description}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Address</label>
                          <textarea
                            name="address"
                            onChange={this.handleChange}
                            value={this.state.eventLocation.address}
                            className="form-control"
                          ></textarea>
                          {this.state.errors.address ? (
                            <div className="error text-danger">
                              {this.state.errors.address}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor="">Latitude</label>
                          <input
                            name="latitude"
                            onChange={this.handleChange}
                            value={this.state.eventLocation.latitude}
                            type="text"
                            placeholder="Latitude *"
                            className="form-control"
                          />
                          {this.state.errors.latitude ? (
                            <div className="error text-danger">
                              {this.state.errors.latitude}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Longitude</label>
                          <input
                            name="longitude"
                            onChange={this.handleChange}
                            value={this.state.eventLocation.longitude}
                            type="text"
                            placeholder="Longitude *"
                            className="form-control"
                          />
                          {this.state.errors.longitude ? (
                            <div className="error text-danger">
                              {this.state.errors.longitude}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>

                      <div className="col-lg-4 col-md-12 ">
                        {/* <div className="form-group form-group-inline checkbox-div">
                                        <label htmlFor="">Is top news?</label>
                                        <input type="checkbox"  onChange={this.handleCheck}  checked={this.state.checkval}/>
                                    </div> */}

                        <div className="faq-sideinputs">
                          <div className="faq-btns form-btn-wrap">
                            <div className="update_btn input-group-btn float-right">
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="btn btn-info"
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                                Update
                              </button>
                            </div>
                          </div>
                          {this.state.message !== "" ? (
                            <div className="tableinformation">
                              <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                              </div>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                    </BlockUi>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
      
    );
  }
}

export default adminEditEventLocations;
