import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import DeleteConfirm from "../common/deleteConfirm";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import * as settingsService from "../../ApiServices/admin/liveTraining";
import ReactSpinner from "react-bootstrap-spinner";
import memoize from "memoize-one";
import TrashConfirm from "../common/trashConfirm";

class AdminListEventLocations extends Component {
  state = {
    confirm: {
      name: "Delete",
      message: "Are you sure you want to delete this page? ",
      action: "delete"
    },
    search: "",
    modal_trash: false,
    data_table: [],
    dataTrash: [],
    dataDraft: [],
    eventLocations: [],
    spinner: true,
    contextActions_select: "-1",
    activetab: "tab_a",
    columns: [
      {
        name: "Location Name",
        selector: "location_name",
        sortable: true,
        cell: row => (
          <div>
            <Link to={"/admin/event-locations/edit/" + row._id}>
              {row.location_name}
            </Link>
          </div>
        )
      },
      {
        name: "",
        cell: row => (
          <div className="action_dropdown" data-id={row._id}>
            <i
              data-id={row._id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            />
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >
              <Link
                to={"/admin/event-locations/edit/" + row._id}
                className="dropdown-item"
              >
                Edit Page
              </Link>
              <Link
                onClick={() => this.saveAndtoogle(row._id, "delete")}
                className="dropdown-item"
              >
                Delete
              </Link>
              {row.page_status != 0 ? (
                <Link
                  onClick={() => this.saveAndtoogle(row._id, "trash")}
                  className="dropdown-item"
                >
                  Trash
                </Link>
              ) : (
                ""
              )}
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px" // custom width for icon button
      }
    ]
  };

  componentDidMount = () => {
    this.getAllEventLocation();
  };
  DeletePageId = id => {
    const data_table = this.state.data_table.filter(i => i.id !== id);
    this.setState({ data_table });
  };

  /* Get all News */
  getAllEventLocation = async () => {
    this.setState({ spinner: true });
    try {
      const response = await settingsService.getAllEventLocation();
      const data_table_row = [];
      const data_table_rowTrash = [];
      const data_table_rowDraft = [];
      if (response.data.status === 1) {
        response.data.data.map((eventLocation, index) => {
          if (eventLocation.page_status == 0) {
            const SetdataTrash = {};
            SetdataTrash.location_name = eventLocation.location_name;
            SetdataTrash.slug = eventLocation.slug;
            SetdataTrash._id = eventLocation._id;
            SetdataTrash.address = eventLocation.address;
            SetdataTrash.description = eventLocation.description;
            SetdataTrash.latitude = eventLocation.latitude;
            SetdataTrash.longitude = eventLocation.longitude;
            SetdataTrash.page_status = eventLocation.page_status;
            data_table_rowTrash.push(SetdataTrash);
          } else if (eventLocation.page_status == 1) {
            const Setdata = {};
            Setdata.location_name = eventLocation.location_name;
            Setdata.slug = eventLocation.slug;
            Setdata._id = eventLocation._id;
            Setdata.address = eventLocation.address;
            Setdata.description = eventLocation.description;
            Setdata.latitude = eventLocation.latitude;
            Setdata.longitude = eventLocation.longitude;
            Setdata.page_status = eventLocation.page_status;
            data_table_row.push(Setdata);
          } else if (eventLocation.page_status == 2) {
            const SetdataDraft = {};
            SetdataDraft.location_name = eventLocation.title;
            SetdataDraft.slug = eventLocation.slug;
            SetdataDraft._id = eventLocation._id;
            SetdataDraft.address = eventLocation.address;
            SetdataDraft.description = eventLocation.description;
            SetdataDraft.latitude = eventLocation.latitude;
            SetdataDraft.longitude = eventLocation.longitude;
            SetdataDraft.page_status = eventLocation.page_status;
            data_table_rowDraft.push(SetdataDraft);
          }
        });
      }

      console.log(data_table_row);
      this.setState({
        data_table: data_table_row,
        dataTrash: data_table_rowTrash,
        dataDraft: data_table_rowDraft
      });
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
      this.setState({
        data_table: [],
        dataTrash: [],
        dataDraft: []
      });
    }
  };

  searchSpace = event => {
    let keyword = event.target.value;
    this.setState({ search: keyword });
  };

  doAction = action => {
    switch (action) {
      case "trash":
        this.trashEventLocation();
        break;
      case "delete":
        this.deleteEventLocation();
        break;
      default:
    }
  };
  trashEventLocation = async () => {
    let id = this.state.index;
    const response = await settingsService.trashEventLocation(id);
    if (response) {
      if (response.data.status == 1) {
        this.getAllEventLocation();
        this.toogle();
      }
    }
  };

  deleteEventLocation = async () => {
    let id = this.state.index;
    const response = await settingsService.deleteEventLocation(id);
    if (response) {
      if (response.data.status == 1) {
        const data_table = this.state.data_table.filter(i => i._id !== id);
        const dataTrash = this.state.dataTrash.filter(i => i._id !== id);
        const dataDraft = this.state.dataDraft.filter(i => i._id !== id);
        this.setState({
          data_table: data_table,
          dataTrash: dataTrash,
          dataDraft: dataDraft
        });

        this.toogle();
      }
    }
  };

  saveAndtoogle = (id, action) => {
    switch (action) {
      case "trash":
        this.setState({
          confirm: {
            name: "Trash",
            message: "Are you sure you want to move the item to trash? ",
            action: action
          }
        });
        break;
      case "delete":
        this.setState({
          confirm: {
            name: "Delete",
            message: "Are you sure you'd like to delete this item?",
            action: action
          }
        });
        break;
      default:
        this.setState({
          confirm: {
            name: "Delete",
            message: "Are you sure you'd like to delete this item?",
            action: action
          }
        });
    }
    this.setState({ index: id });
    this.toogle();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };

  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows });
  };
  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value });
  };

  actionHandler = e => {
    this.setState({
      message: "",
      message_type: ""
    });
    const { selectedRows } = this.state;
    const ids = selectedRows.map(r => r._id);
    let status = "";
    let actionvalue = this.state.contextActions_select;
    if (actionvalue === "-1") {
      return;
    } else if (actionvalue === "0") {
      status = "Trash";
    } else if (actionvalue === "1") {
      status = "Published";
    } else if (actionvalue === "2") {
      status = "Draft";
    } else if (actionvalue === "3") {
      status = "Delete trash";
    }
    if (window.confirm(`Are you sure you want to move ` + status)) {
      this.setState({ toggleCleared: !this.state.toggleCleared });
      this.actionHandlerEventLocation(ids, actionvalue, status);
    }
  };

  actionHandlerEventLocation = async (ids, contextActions, status) => {
    try {
      const response = await settingsService.actionHandlerEventLocation(
        ids,
        contextActions,
        status
      );
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success"
        });

        this.getAllEventLocation();
      } else {
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
        this.getAllEventLocation();
      }
    } catch (err) {
      this.setState({
        message: "Something went wrong",
        message_type: "error"
      });
    }
  };

  tabActive = async tab => {
    this.setState({
      message: "",
      message_type: ""
    });
    this.setState({ activetab: tab });
  };

  render() {
    let rowData = this.state.data_table;
    let search = this.state.search;
    let rowDataTrash = this.state.dataTrash;
    let rowDataDraft = this.state.dataDraft;
    const contextActions = memoize(actionHandler => (
      <div className="common_action_section">
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value="-1">Select</option>
          {this.state.activetab !== "tab_a" ? (
            <option value="1">Published</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_b" ? (
            <option value="0">Trash</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_c" ? (
            <option value="2">Draft</option>
          ) : (
            ""
          )}
          {this.state.activetab !== "tab_a" &&
          this.state.activetab !== "tab_c" ? (
            <option value="3">Delete Trash</option>
          ) : (
            ""
          )}
        </select>
        <button className="danger" onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ));
    if (search.length > 0) {
      search = search.trim().toLowerCase();
      rowData = rowData.filter(l => {
        return l.location_name.toLowerCase().match(search);
      });
    }

    return (
      <React.Fragment>
        <div className="container-fluid admin-body ">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4">
                  <div className="main_admin_page_common">
                    <div className="">
                      <div className="admin_breadcum">
                        <div className="row">
                          <div className="col-md-1">
                            <p className="page-title">News</p>
                          </div>
                          <div className="col-md-6">
                            <Link
                              className="add_new_btn"
                              to="/admin/event-locations/add"
                            >
                              Add New
                            </Link>
                          </div>
                          <div className="col-md-5">
                            <div className="searchbox">
                              <div className="commonserachform">
                                <span />
                                <input
                                  type="text"
                                  placeholder="Search"
                                  onChange={e => this.searchSpace(e)}
                                  name="search"
                                  className="search form-control"
                                />
                                <input type="submit" className="submit_form" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="faq-list-table-wrapper">
                        <React.Fragment>
                          <div
                            style={{
                              display:
                                this.state.spinner === true
                                  ? "flex justify-content-center "
                                  : "none"
                            }}
                            className="overlay text-center"
                          >
                            <ReactSpinner
                              type="border"
                              color="primary"
                              size="10"
                            />
                          </div>
                        </React.Fragment>
                        {this.state.message !== "" ? (
                          <div className="tableinformation">
                            <div className={this.state.message_type}>
                              <p>{this.state.message}</p>
                            </div>
                          </div>
                        ) : null}
                        <ul className="nav nav-pills nav-fill">
                          <li
                            className={
                              this.state.activetab === "tab_a" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_a"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_a")}
                            >
                              All ({this.state.data_table.length})
                            </a>
                          </li>
                          <li
                            className={
                              this.state.activetab === "tab_b" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_b"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_b")}
                            >
                              Trash ({this.state.dataTrash.length})
                            </a>
                          </li>

                          <li
                            className={
                              this.state.activetab === "tab_c" ? "active" : ""
                            }
                          >
                            <a
                              href="#tab_c"
                              data-toggle="pill"
                              onClick={() => this.tabActive("tab_c")}
                            >
                              Draft ({this.state.dataDraft.length})
                            </a>
                          </li>
                        </ul>
                        <div className="tab-content">
                          <div className="tab-pane active" id="tab_a">
                            <DataTable
                              columns={this.state.columns}
                              data={rowData}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                          <div className="tab-pane" id="tab_b">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataTrash}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                          <div className="tab-pane" id="tab_c">
                            <DataTable
                              columns={this.state.columns}
                              data={rowDataDraft}
                              selectableRows
                              highlightOnHover
                              pagination
                              selectableRowsVisibleOnly
                              clearSelectedRows={this.state.toggleCleared}
                              onSelectedRowsChange={this.handlerowChange}
                              contextActions={contextActions(this.deleteAll)}
                              noDataComponent = {<p>There are currently no records.</p>}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          name={this.state.confirm.name}
          message={this.state.confirm.message}
          action={this.state.confirm.action}
          deleteUser={this.doAction}
        />
      </React.Fragment>
    );
  }
}

export default AdminListEventLocations;
