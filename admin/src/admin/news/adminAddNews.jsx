import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import UploadThumb from "../common/uploadThumbimage";
import * as settingsService from "../../ApiServices/admin/settings";
import * as manageUser from "../../ApiServices/admin/manageUser";
import CKEditor from "ckeditor4-react";
import { apiUrl ,siteUrl} from "../../../src/config.json";
import BlockUi from 'react-block-ui';
class AdminAddNews extends Component {
  constructor(props) {
    super(props);
    this.handleCheck = this.handleCheck.bind(this);
    this.onEditorChange = this.onEditorChange.bind(this);
  }
  state = {
    spinner: false,
    editorContent: "",
    checked: false,
    data: {
      title: "",
      slug: "",
      description: "",
      meta_tag : '',
      meta_desc :'',
      news_order: ""
    },
    profile_image: "",
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    upload_data: "",
    upload_data_thumb:"",
    editordata: "",
    focused: false,
    profile_image_thumb :''
  };

  onEditorChange(evt) {
    this.setState({
      editordata: evt.editor.getData()
    });
  }

  onclassicEdit = async (value, item) => {
    if (item === "editorContent") {
      this.setState({
        editorContent: value.getData()
      });
    }
  };

  /* Checkbox on change */
  handleCheck(e) {
    this.setState({
      checked: e.target.checked
    });
  }

  /* Joi validation schema */

  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    slug: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),

      meta_tag :  Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    
      meta_desc : Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),

    description: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),
    profile_image: Joi.allow(null),
    news_order: Joi.number()
    .required()
    .error(() => {
      return {
        message: "This field is a required field."
      };
    })
  };
  /* Input Handle Change */
  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);

    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  /*Joi Validation Call*/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /* Form Submit */
  handleSubmit = async () => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    let result = Joi.validate(data, this.schema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });

        if (this.state.upload_data && this.state.upload_data_thumb) {


          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            this.setState({ profile_image: filepath });
                  const response2 = await manageUser.uploadProfile(
                    this.state.upload_data_thumb
                  );
                  if (response2.data.status == 1) {
                    let filepath = response2.data.data.file_location;
                    this.setState({ profile_image_thumb: filepath })
                    this.updateNewsData();
                  } else {
                    this.setState({
                      submitStatus: false,
                      message: response2.data.message,
                      responsetype: "error"
                    });
                    this.setState({ spinner: false });
                  }
          //  this.updateArticlesData();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }
        
        } else if (this.state.upload_data){
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            this.setState({ profile_image: filepath });
            this.updateNewsData();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }
        } else if(this.state.upload_data_thumb){
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data_thumb
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            this.setState({ profile_image_thumb: filepath })
            this.updateNewsData();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }
        } else {
          this.updateNewsData();
        }
        // if (this.state.upload_data) {
        //   const response1 = await manageUser.uploadProfile(
        //     this.state.upload_data
        //   );
        //   if (response1.data.status == 1) {
        //     let filepath = response1.data.data.file_location;
        //     this.setState({ profile_image: filepath });
        //     this.updateNewsData();
        //   } else {
        //     this.setState({
        //       submitStatus: false,
        //       message: response1.data.message,
        //       responsetype: "error"
        //     });
        //     this.setState({ spinner: false });
        //   }
        // } else {
        //   this.updateNewsData();
        // }
      }
    }
  };
  
  /* Add news */
  updateNewsData = async () => {
    const news_data = {
      title: this.state.data.title,
      slug: this.state.data.slug,
      description: this.state.data.description,
      profile_image: this.state.profile_image,
      profile_image_thumb : this.state.profile_image_thumb,
      checked: this.state.checked == true ? "1" : "0",
      content: this.state.editordata,
      meta_tag: this.state.data.meta_tag,
      meta_desc: this.state.data.meta_desc,
      news_order: this.state.data.news_order,
    };
    const response = await settingsService.createNews(news_data);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
         // window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
  };
  /* upload image */
  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors };

    let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      let file = value.target.files[0];
      upload_data = file;
      this.setState({ upload_data: upload_data });
    }
  };


     /* Upload Image thumbnail */
     onUploadThumbnail = async (value, item) => {
      console.log('onUploadThumbnail',value);
  
      let errors = { ...this.state.errors };
      let upload_data_thumb = { ...this.state.upload_data_thumb };
      if (item === "errors") {
        errors["profile_image_thumb"] = value;
      } else {  
        let file = value.target.files[0];
        console.log('UploadThumbnail file',file);
        upload_data_thumb = file;
        this.setState({ upload_data_thumb: upload_data_thumb });
      }
    };


  render() {
    let checkedCond = this.state.checked;
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents addpage-form-wrap news-add-wrap addfaq-from-wrap home-inner-content pt-4 pb-4 pr-4">
                  <div className="addpage-form">
                  <BlockUi tag="div" blocking={this.state.spinner} >
                    <div className="row addpage-form-wrap">
                      <div className="col-lg-8 col-md-12">
                        <div className="form-group">
                          <label htmlFor="">Add New News</label>
                          <input
                            name="title"
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Add title *"
                            className="form-control"
                          />
                          {this.state.errors.title ? (
                            <div className="error text-danger">
                              {this.state.errors.title}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Slug</label>
                          <input
                            name="slug"
                            onChange={this.handleChange}
                            value={this.state.slug}
                            type="text"
                            placeholder="slug *"
                            className="form-control"
                          />
                          {this.state.errors.slug ? (
                            <div className="error text-danger">
                              {this.state.errors.slug}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>







                        <div className="form-group">
                          <label htmlFor="">  Meta tag</label>
                          <input
                            name="meta_tag"
                            onChange={this.handleChange}
                            value={this.state.meta_tag}
                            type="text"
                            placeholder="Meta tag *"
                            className="form-control"
                          />
                         
                          {this.state.errors.meta_tag ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_tag}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div className="form-group">
                          <label htmlFor=""> Meta Description</label>
                          <input
                            name="meta_desc"
                            onChange={this.handleChange}
                            value={this.state.meta_desc}
                            type="text"
                            placeholder="Meta Description *"
                            className="form-control"
                          />
                          {this.state.errors.meta_desc ? (
                            <div className="error text-danger">
                              {this.state.errors.meta_desc}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>



                        <div className="form-group">
                          <label htmlFor="">Short Description</label>
                          <textarea
                            name="description"
                            onChange={this.handleChange}
                            value={this.state.description}
                            className="form-control"
                          ></textarea>
                          {this.state.errors.description ? (
                            <div className="error text-danger">
                              {this.state.errors.description}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-12">
                              <label htmlFor="">Content</label>
                              <CKEditor
                                data={this.state.editordata}
                                 onBeforeLoad={CKEDITOR => {
                                    CKEDITOR.plugins.addExternal('simplebutton', siteUrl + '/plugins/simplebutton/', 'plugin.js');
                                  }
                                  }
                                  config={{
                                  extraPlugins: ["justify" ,"simplebutton", "font",  "colorbutton" ,"colordialog" , "image2" ,"tableresize"],
                                  allowedContent: true,
                                  filebrowserImageUploadUrl:
                                    apiUrl + "/admin/upload/imageupload-new"
                                }}
                                onInit={editor => {}}
                                onChange={this.onEditorChange}
                                style={{
                                  float: "left",
                                  width: "99%"
                                }}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="form-group">
                          <label htmlFor="">News Order</label>
                          <input
                            name="news_order"
                            onChange={this.handleChange}
                            value={this.state.news_order}
                            type="text"
                            placeholder="News Order *"
                            className="form-control"
                          />
                          {this.state.errors.news_order ? (
                            <div className="error text-danger">
                              {this.state.errors.news_order}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>


                      </div>
                      <div className="col-lg-4 col-md-12 ">
                        <div className="form-group form-group-inline checkbox-div">
                          <label htmlFor="">Top Article</label>
                          <input
                            type="checkbox"
                            onChange={this.handleCheck}
                            checked={checkedCond}
                          />
                        </div>
                        <div className="form-group thumbanail_container">
                          <Uploadprofile
                            onuplaodProfile={this.onuplaodProfile}
                            value={this.state.profile_image}
                            errors={this.state.errors}
                          />
                        </div>

                        <div className="form-group  thumbanail_container">
                                <label>Upload Thumbnail</label>
                          <UploadThumb
                           onUploadThumbnail={this.onUploadThumbnail}
                           value={this.state.profile_image_thumb}
                           errors={this.state.errors}
                          />
                        </div>


                        <div className="faq-sideinputs">
                          <div className="faq-btns form-btn-wrap">
                            <div className="float-left"></div>
                            <div className="update_btn input-group-btn float-right">
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.handleSubmit}
                                className="btn btn-info"
                              >
                                {this.state.submit_status ? (
                                  <ReactSpinner
                                    type="border"
                                    color="dark"
                                    size="1"
                                  />
                                ) : (
                                  ""
                                )}
                                Publish
                              </button>
                            </div>
                          </div>
                          {this.state.message !== "" ? (
                            <div className="tableinformation">
                              <div className={this.state.message_type}>
                                <p>{this.state.message}</p>
                              </div>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </BlockUi>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminAddNews;
