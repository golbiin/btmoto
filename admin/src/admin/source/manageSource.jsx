import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi from "joi-browser";
import * as SourceService from "../../ApiServices/admin/manageSource";
import DataTable from "react-data-table-component";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import MultiSelect from "react-multi-select-component";
import moment from "moment";
class AdminSource extends Component {
  state = {
    id: "",
    errors: {},
    submit_status: false,
    message: "",
    message_type: "",
    table_message: "",
    table_message_type: "",
    addresses: [],
    delete_status: false,
    edit_status: false,
    data_table: [],
    spinner: true,
    Sourcecode: "",
    amount: "",
    discounttype: "",
    exiprydate: "", //moment(Date.now()).toDate(),
    products: [],
    products_selected: [],
    products_value: [],
    users: [],
    users_value: [],
    users_selected: [],
    guestusers: "",
    Sourcestatus: "1",

    columns: [
      {
        name: "Title",
        selector: "title",
        sortable: true,
      },
    
      {
        name: "Action",
        cell: (row) => (
          <div className="action_dropdown" data-id={row._id}>
            <i
              data-id={row._id}
              className="dropdown_dots"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            ></i>
            <div
              className="dropdown-menu dropdown-menu-center"
              aria-labelledby="dropdownMenuButton"
            >
              <a
                onClick={() => this.getContactById(row._id)}
                className="dropdown-item"
              >
                Edit
              </a>
              <a
                onClick={() => this.handleRemoveContact(row._id)}
                className="dropdown-item"
              >
                Delete
              </a>
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: "56px", // custom width for icon button
      },
    ],
  };

  componentDidMount = () => {
    this.getAllSources();
     
  };
  getAllSources = async () => {
    this.setState({ spinner: true });
    try {
      const response = await SourceService.getAllSources();
      if (response.data.status == 1) {
        this.setState({ addresses: response.data.data });
        const Setdata = { ...this.state.data_table };
        const data_table_row = [];
        response.data.data.map((add, index) => {
          const Setdata = {};
          Setdata.title = add.title;
          Setdata._id = add._id;
          data_table_row.push(Setdata);
        });      

        this.setState({
          data_table: data_table_row,
        });
        
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  schema = {
    title: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field.",
        };
      }), 
    id: Joi.string().allow(""),
  };
  validateProperty = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };
  handleChange = (input) => (event) => {
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty(input, event.target.value);
    if (errorMessage) errors[input] = errorMessage;
    else delete errors[input];
    let val = event.target.value;
    if(input == "amount") {
      val = +parseFloat(val).toFixed(2)
    }
    this.setState({ [input]: val, errors });
  };

  getContactById = async (id) => {
    const response = await SourceService.getSourceById({ id: id });
    if (response.data.status == 1) {
      this.setState({
        edit_status: true,
        submit_status: false,
        message: "",
        message_type: "",
        delete_status: false,
        title: response.data.data.title,
        id: response.data.data._id,
      });
      
    }
  };

  addSource = () => {
    this.setState({
      title: "",
     
      submit_status: false,
      message: "",
      message_type: "",
      delete_status: false,
      edit_status: false,
    });
  };

  saveSource = async (e) => {
    e.preventDefault();
    const checkState = {
      title: this.state.title,
    };
    const errors = { ...this.state.errors };    
    this.setState({ errors: {} });  
    let result = Joi.validate(checkState, this.schema);    
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;      
      errors[path] = errormessage;
      this.setState({ errors });
    } else {
      const category_data = {
        title: this.state.title,
        id: this.state.id,
      };
      try {
        this.setState({
          submit_status: true,
          message: "",
          message_type: "",
        });
        if (this.state.edit_status === true) {         
          const response = await SourceService.updateSource(category_data);
          if (response) {
            this.setState({ submit_status: false });
            if (response.data.status === 1) {
              this.setState({
                message: response.data.message,
                message_type: "success",
                Sourcecode: "",
                id: "",
                edit_status: false,
              });
              this.getAllSources();
            } else {
              this.setState({
                message: response.data.message,
                message_type: "error",
              });
            }
          } else {
            this.setState({ submit_status: false });
            this.setState({
              message: "Something went wrong! Please try after some time",
              message_type: "error",
            });
          }
          setTimeout(() => {
            this.setState(() => ({ message: "" }));
          }, 5000);
        } else {
          const response = await SourceService.createSource(category_data);
          if (response) {
            this.setState({ submit_status: false });
            if (response.data.status === 1) {
              this.setState({
                message: response.data.message,
                message_type: "success",
                title: "",
                
              });
              this.getAllSources();
            } else {
              this.setState({
                message: response.data.message,
                message_type: "error",
              });
            }
          } else {
            this.setState({ submit_status: false });
            this.setState({
              message: "Something went wrong! Please try after some time",
              message_type: "error",
            });
          }
          setTimeout(() => {
            this.setState(() => ({ message: "" }));
          }, 5000);
        }
      } catch (err) {}
    }
  };

  handleRemoveContact = async (id) => {
    this.setState({
      delete_status: true,
    });
    const response = await SourceService.removeSource({ _id: id });
    if (response.data.status === 1) {
      let newContacts = [...this.state.data_table];
      newContacts = newContacts.filter(function (obj) {
        return obj._id !== id;
      });
      this.setState({
        table_message: response.data.message,
        table_message_type: "sucess",
      });

      this.setState({
        data_table: newContacts,
        delete_status: false,
        edit_status: false,
      });
      setTimeout(() => {
        this.setState(() => ({ table_message: "" }));
      }, 5000);
    } else {
      this.setState({
        table_message: response.data.message,
        table_message_type: "sucess",
      });
      setTimeout(() => {
        this.setState(() => ({ table_message: "" }));
      }, 5000);
    }
  };

  handledateChange = (date) => {
    this.setState({
      exiprydate: date,
    });
    if(date) {
      const errors = { ...this.state.errors };
      delete errors.validate;
      const errorMessage = this.validateProperty('exiprydate', date);
      if (errorMessage) errors['exiprydate'] = errorMessage;
      else delete errors['exiprydate'];
      this.setState({ 'errors': errors });
    }
  };

  handlediscountChange = async (e) => {
    this.setState({ discounttype: e.target.value });
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty('discounttype', e.target.value);
    if (errorMessage) errors['discounttype'] = errorMessage;
    else delete errors['discounttype'];
    this.setState({ 'errors': errors });
  };

  handleSourceChange = async (e) => {
    this.setState({ Sourcestatus: e.target.value });
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty('Sourcestatus', e.target.value);
    if (errorMessage) errors['Sourcestatus'] = errorMessage;
    else delete errors['Sourcestatus'];
    this.setState({ 'errors': errors });
  };

 
  userSelected = (event) => {
    const data_selected_row = [];
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value;
    });   
    this.setState({ users: data_selected_row });
    this.setState({ users_selected: event });
  };
  setSelected = (event) => {
    const data_selected_row = [];
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value;
    });
    this.setState({ products: data_selected_row });
    this.setState({ products_selected: event });
  };

  render() {
    let datarow = this.state.data_table;
    // const userPlaceholder = {
    //   selectSomeItems: "Users",
    //   allItemsAreSelected: "All items are selected.",
    //   selectAll: "Select All",
    // };
    // const productPlaceholder = {
    //   selectSomeItems: "Products",
    //   allItemsAreSelected: "All items are selected.",
    //   selectAll: "Select All",
    //   search: "Search",
    // };
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  main_admin_page_common  useradd-new pt-2 pb-4 pr-4">
                  <React.Fragment>
                    <div className="categories-wrap p-4">
                      <div className="row">
                        <div className="col-md-4">
                          <div className="card">
                            <div className="card-header">
                              <strong>
                                {this.state.edit_status === true
                                  ? "Edit"
                                  : "Add"}{" "}
                                Software
                              </strong>
                              {this.state.edit_status === true ? (
                                <button
                                  onClick={this.addSource}
                                  className="ml-4 add-cat-btn btn btn-success"
                                >
                                  + Add New
                                </button>
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="card-body coupon-add categories-add">
                        

                              <div className="form-group">
                                <input
                                  className="form-control"
                                  placeholder="Software title"
                                  type="text"
                                  value={this.state.title}
                                  onChange={this.handleChange("title")}
                                />
                                {this.state.errors.title ? (
                                  <div className="danger">
                                    {this.state.errors.title}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
 
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={this.saveSource}
                                className="save-btn btn btn-dark"
                              >
                                {this.state.submit_status === true ? (
                                  <React.Fragment>
                                    <ReactSpinner
                                      type="border"
                                      color="primary"
                                      size="1"
                                    />
                                    <span className="submitting">
                                      {this.state.edit_status === true
                                        ? "UPDATING SOFTWARE..."
                                        : "CREATING SOFTWARE..."}
                                    </span>
                                  </React.Fragment>
                                ) : this.state.edit_status === true ? (
                                  "UPDATE SOFTWARE"
                                ) : (
                                  "CREATE SOFTWARE"
                                )}
                              </button>
                              {this.state.message !== "" ? (
                                <div className={this.state.message_type}>
                                  <p>{this.state.message}</p>
                                </div>
                              ) : null}
                            </div>
                          </div>
                        </div>
                        <div className="col-md-8">
                          <div className="card">
                            <div className="card-header">
                              <strong>SOFTWARE</strong>
                            </div>
                            <div className="card-body categories-list">
                              <React.Fragment>
                                <div
                                  style={{
                                    display:
                                      this.state.spinner === true
                                        ? "flex"
                                        : "none",
                                  }}
                                  className="overlay"
                                >
                                  <ReactSpinner
                                    type="border"
                                    color="primary"
                                    size="10"
                                  />
                                </div>
                              </React.Fragment>
                              <p className="tableinformation">
                                {this.state.table_message !== "" ? (
                                  <div
                                    className={this.state.table_message_type}
                                  >
                                    <p>{this.state.table_message}</p>
                                  </div>
                                ) : null}
                              </p>
                              <DataTable
                                columns={this.state.columns}
                                data={datarow}
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                noDataComponent = {<p>There are currently no records.</p>}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminSource;
