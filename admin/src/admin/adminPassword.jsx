import React, { Component } from "react";
import { Link } from "react-router-dom";
import Joi from "joi-browser";
import * as authServices from "../ApiServices/admin/login";
import ReactSpinner from "react-bootstrap-spinner";
class AdminPassword extends Component {
  state = {
    data: { username: ""  , password : "", repassword : "" ,_id : "" },
    id : '',
    errors: {},
    submit_status: false,
  };
/* Joi validation schema */
  schema = {
    _id: Joi.string(),
    username: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your username/email",
        };
      }),
      password: Joi.string()
        .required()
        .error(() => {
        return {
          message: "Please enter your password",
        };
        }),


        repassword: Joi.any().valid(Joi.ref('password')).required().options({ 
          language: { any: { allowOnly: 'must match password' } } }).label('Repassword')

       
  };

  /* on chnage input save data */
  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;

    delete errors.validate;
    this.setState({ data, errors });
    delete errors.validate;

  };
 /* Joi validation call*/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  ForgotSubmit = async () => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    delete errors.validate;
    let result = Joi.validate(data, this.schema);
    this.setState({
      message: "",
      message_type: "",
    });
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
    } else {
      this.setState({ submit_status: true });
      try {
        const response = await authServices.UpdatePassword(data);
        if (response) {
          this.setState({
            message: response.data.message,
            message_type: response.data.type,
            submit_status: false,
          });
        }
      } catch (err) {}
    }
  };

  componentDidMount = async () => {
    const id = this.props.match.params.id;
    if(id){
      const response = await authServices.verfiyUserWithId(id);
      if(response.data.status ==1){
        const data = { ...this.state.data };
        data['_id'] = id;
        this.setState({ data, data });
      } else {
        this.props.history.push({
          pathname: "/admin/login/"
        });
      }

    }
  
  }
  render() {
   let id=  this.state.id;
    return (
      <React.Fragment>
        <div className="container-fluid">
          <div id="admin_login">
            <div className="loginbox">
              <h1>Change Password</h1>
              <div className="login-form">
                {this.state.message !== "" ? (
                  <div className={this.state.message_type}>
                    <p>{this.state.message}</p>
                  </div>
                ) : null}

 <div><div className="form-group">
<span class="username"></span>
<input
  type="text"
  placeholder="Username or email"
  className="form-control field"
  name="username"
  onChange={this.handleChange}
  value={this.state.data.username}
/>
{this.state.errors.username ? (
  <div className="danger">{this.state.errors.username}</div>
) : (
  ""
)}
</div>
     <div className="form-group">
                  <span class="password"></span>
                 
                  <input
                    type="password"
                    placeholder="Password"
                    className="form-control field"
                    name="password"
                    ref="password"
                    onChange={this.handleChange}
                    value={this.state.data.password}
                  />
                  {this.state.errors.password ? (
                    <div className="danger">{this.state.errors.password}</div>
                  ) : (
                    ""
                  )}
                </div>

                 <div className="form-group">
                  <span class="password"></span>
                 
                  <input
                    type="password"
                    placeholder="Re-password"
                    className="form-control field"
                    name="repassword"
                    ref="confirmPassword" 
                    onChange={this.handleChange}
                    value={this.state.data.repassword}
                  />
                  {
                    
                    (this.state.errors.repassword  && (this.state.data.repassword !== this.state.data.password)) ? (
                    <div className="danger">{this.state.errors.repassword}</div>
                  ) : (
                    ""
                  )  
                  
                  }
                </div>

<button className="admin-login-btn" onClick={this.ForgotSubmit}>
{this.state.submit_status ? (
  <ReactSpinner type="border" color="dark" size="1" />
) : (
  ""
)}
Submit
</button>  <Link style={
{
  'text-align': 'center',
  'display': 'inline-block',
  'width': '100%',
  'margin-top': '15px',
  'color': '#11cdef'
}

} to="/admin/login">Login</Link></div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminPassword;
