import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import OrderItems from "../orders/OrderItems";
import { Link } from "react-router-dom";
import Joi, { join } from "joi-browser";
import * as OrdersApi from "../../ApiServices/admin/orders";
import * as manageUser from "../../ApiServices/admin/manageUser";
import * as manageProducts from "../../ApiServices/admin/products";
import { CURRENCY_SYMBOL } from "../../config.json";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import ReactSpinner from "react-bootstrap-spinner";
import {
  CountryDropdown,
  RegionDropdown,
} from "react-country-region-selector";
class AdminOrderDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activetab: "tab_a",
      tabData: [
        { name: "Tab 1", isActive: true },
        { name: "Tab 2", isActive: false },
        { name: "Tab 3", isActive: false },
      ],
      data: {},
      user_data: {},
      errors: {},
      order_id: "",
      order_notes: [],
      product: [],
      tax: {},
      shipping: {},
      total: {},
      shipping_details: {},
      address: [],
      city: "",
      country: "",
      state: "",
      zipcode: "",
      phone: "",
      date: "",
      paymentid: "",
      page_status: "",
      status: "",
      invoice_url: "",
      upload_status: false,
      ship_status: false,
      refund: {},
      refundamount: 0,
      refundamount_array: [],
      shipping_method: "",
      trackingNumber: "",
    };
  }

  tabActive = async (tab) => {
    this.setState({ activetab: tab });
  };

  /*Joi validation schema*/
  schema = {
    storename: Joi.string(),
    order_details: Joi.allow(null),
    payment_details: Joi.allow(null),
    shipping_details: Joi.allow(null),
    page_status: Joi.allow(null),
    _id: Joi.optional().label("id"),
    payment_details: Joi.allow(null),
    status: Joi.string(),
    user_id: Joi.string(),
    __v: Joi.allow(null),
    zipcode: Joi.string(),
    created_on: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required",
        };
      }),
  };

  /*Joi Validation Call */
  validateProperty = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  /*Input Handle Change */
  handleChange = (input) => (event) => {
    const errors = { ...this.state.errors };
    delete errors.validate;
    const errorMessage = this.validateProperty(input, event.target.value);
    if (errorMessage) errors[input] = errorMessage;
    else delete errors[input];
    this.setState({ [input]: event.target.value, errors });
  };

  /* Form Submit */
  handleSubmit = async () => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    this.setState({
      submit_status: false,
      message_confirm: "",
      message_type: "",
    });
    const checkState = {
      created_on: this.state.date.toISOString(),
      status: this.state.status,
      zipcode: this.state.zipcode,
    };
    let result = Joi.validate(checkState, this.schema);
      if (result.error) {
      this.setState({ submit_status: true });
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors,
      });
    } else {
      if (errors.length > 0) {
        this.setState({ submit_status: true });
        this.setState({
          errors: errors,
        });
      } else {
        const OrderId = this.props.match.params.id;
        const response = await OrdersApi.updateOrder(OrderId, checkState);
        if (response.data.status == 1) {
          this.setState({
            message: response.data.message,
            message_type: "success",
          });
        } else {
          this.setState({
            message: response.data.message,
            message_type: "error",
          });
        }
        this.getCurrentOders(OrderId);
        this.setState({ submit_status: true });
        setTimeout(() => {
          this.setState({
            message: "",
            message_type: "",
          });
        }, 2000);
      }
    }
  };
/* set on refund value from OrderItems */
  UpdateRefund = async (amount, refundarray) => {
    this.setState({
      refundamount: amount,
      refundamount_array: refundarray,
    });  
  };

  handleGenerateInvoice = async () => {
    this.setState({
      upload_status: true,
      message_confirm: "",
      message_type: "",
    });
    const data = { ...this.state.data };
    const response = await OrdersApi.generateInvoice(data);

    if (response.data.status == 1) {
      this.setState({
        invoice_url: response.data.url,
        upload_status: false,
      });
    } else {
      this.setState({
        invoice_url: "",
        upload_status: false,
      });
    }
  };

  componentDidMount = () => {
    const OrderId = this.props.match.params.id;
    this.getCurrentOders(OrderId);
    this.getCurrentOdersnotes(OrderId);
  };

  /* Get single products details */
  getproductdetails = async (product) => {
    const response = await manageProducts.getSingleProduct(product.product_id);
    if (response.data.status == 1) { 
      return <p>{product.product_name}</p>;
    }
  };

  handledateChange = (date) => {
    this.setState({
      date: date,
    });
  };

  /*Order Status Change */
  getCurrentOdersnotes = async (OrderId) => {
    try {
      const response = await OrdersApi.getCurrentOdersnotes(OrderId);
      if (response.data.status == 1) {  
        this.setState({
          order_notes: response.data.data,
        });
      }
    } catch (err) {}
  };
/* Get Current Order details */
  getCurrentOders = async (OrderId) => {
     this.setState({
            message_confirm: '',
            message_type: "",
      });
    try {
      const response = await OrdersApi.getCurrentOders(OrderId);
      if (response.data.status == 1) {
        if (response.data.data[0]) {
          this.setState({
            date: moment(response.data.data[0].created_on).toDate(),
          });
          this.setState({
            data: response.data.data[0],
            page_status: response.data.data[0].page_status,
            status: response.data.data[0].status,
          });
          if (response.data.data[0].order_details.product) {
            this.setState({
              product: response.data.data[0].order_details.product,
            });
          }

          if (response.data.data[0].order_details.tax) {
            this.setState({ tax: response.data.data[0].order_details.tax });
          }
          if (response.data.data[0].order_details.total) {
            this.setState({ total: response.data.data[0].order_details.total });
          }

          if (response.data.data[0].shipping_details) {
            this.setState({
              shipping_details: response.data.data[0].shipping_details,
              shipping_method: response.data.data[0].shipping_method,
            });
          }

          if (response.data.data[0].shipping_details.address) {
            this.setState({
              address: response.data.data[0].shipping_details,
              city: response.data.data[0].shipping_details.city,
              country: response.data.data[0].shipping_details.country,
              state: response.data.data[0].shipping_details.state,
              zipcode: response.data.data[0].shipping_details.zipcode,
              phone: response.data.data[0].shipping_details.phone,
            });
          }
          if (response.data.data[0].payment_details.id) {
            this.setState({
              paymentid: response.data.data[0].payment_details.id,
            });
          }
          if (response.data.data[0].invoice_url) {
            this.setState({
              invoice_url: response.data.data[0].invoice_url,
            });
          }
          if (response.data.data[0].order_details.refund) {
            this.setState({
              refund: response.data.data[0].order_details.refund,
            });
          }
          const response2 = await manageUser.getSingleUser(
            response.data.data[0].user_id
          );
          if (response2.data.status == 1) {
            this.setState({ user_data: response2.data.data.user_data });
          }
          let TrackingNumber = "";
          if (response.data.data[0].hasOwnProperty("shipmentresults")) {
            response.data.data[0].shipmentresults.PackageResults.PackageResults.forEach(
              function (item) {
              TrackingNumber = TrackingNumber + item.TrackingNumber[0] + ", ";
              }
            );            
          }
          this.setState({
            trackingNumber: TrackingNumber,
          });
        }
      }
    } catch (err) {}
  };

  movetoTrash = async () => {
    const OrderId = this.props.match.params.id;
    const response = await OrdersApi.trashOrder(OrderId);
    if (response) {
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success",
        });
      }
      setTimeout(() => {
        this.setState({
          message: "",
          message_type: "",
        });
        this.getCurrentOders(OrderId);
      }, 2000);
    }
  };

  movetoPublish = async () => {
    const OrderId = this.props.match.params.id;
    const response = await OrdersApi.movetoPublish(OrderId);
    if (response) {
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: "success",
        });
        this.getCurrentOders(OrderId);
      }
      setTimeout(() => {
        this.setState({
          message: "",
          message_type: "",
        });
        this.getCurrentOders(OrderId);
      }, 2000);
    }
  };

  handlestatusChange = async (e) => {
    this.setState({ status: e.target.value });
  };
  shippingconfirm = async (e) => {
    this.setState({
      ship_status: true,
      message_confirm: "",
      message_type: "",
    });
    const OrderId = this.props.match.params.id;
    try {
      const response = await OrdersApi.shippingConfirm(OrderId);
      if (response) {
        
        if (response.data.status == 1) {
          this.setState({
            message_confirm: response.data.message,
            message_type: "success",
            ship_status: false,
            trackingNumber : '   '
          });
          this.getCurrentOders(OrderId);
        } else {
          this.setState({
            message_confirm: response.data.message,
            message_type: "error",
            ship_status: false,
          });
        }
      }
    } catch (err) {
      this.setState({
        message_confirm: "",
        message_type: "",
        ship_status: false,
      });
    }
  };
  render() {       
    const images = require.context("../../assets/images/admin", true);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
              <div className="row content-row">
                <div className="col-md-12  header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4">
                  <div className="order-summary-details-wrapper  bg-white">
                    <div className="inner-col head-wrap">
                      <h2 className="bg-white">Order Summary</h2>
                    </div>
                    <div className="form-group row details-wrap-div">
                      <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                        Order ID:
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                        {this.state.data.order_no}
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                        Status :
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                        {this.state.status ? this.state.status : "-"}
                      </div>

                      <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                        Customer:
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                        {this.state.user_data.first_name +
                          "  " +
                          this.state.user_data.last_name}
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                        Email:
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                        {this.state.user_data.email
                          ? this.state.user_data.email
                          : "-"}
                      </div>

                      <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                        Date Added:
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                        {this.state.data.created_on
                          ? moment(this.state.data.created_on).format(
                              "DD-MM-YYYY HH:mm:ss"
                            )
                          : "-"}
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                        Order Total :
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                        {this.state.total.total
                          ? CURRENCY_SYMBOL + this.state.total.total
                          : "-"}
                      </div>

                      <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                        Shipping Method:
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                        {
                          this.state.shipping_method.service
                            ? this.state.shipping_method.service
                            : "-"
                        }
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                        Billing Email:
                      </div>
                      <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                        {this.state.shipping_details.email
                          ? this.state.shipping_details.email
                          : ""}
                      </div>
                    </div>
                  </div>
                  <div className=" order-summary-tab-wrapper bg-white">
                    <div className="col-xl-12 col-md-12 col-sm-12  p-0">
                      <ul className="nav nav-pills nav-fill">
                        <li
                          className={
                            this.state.activetab === "tab_a" ? "active" : ""
                          }
                        >
                          <a
                            href="#tab_a"
                            data-toggle="pill"
                            onClick={() => this.tabActive("tab_a")}
                          >
                            Order Details
                          </a>
                        </li>
                        <li
                          className={
                            this.state.activetab === "tab_b" ? "active" : ""
                          }
                        >
                          <a
                            href="#tab_b"
                            data-toggle="pill"
                            onClick={() => this.tabActive("tab_b")}
                          >
                            Shipping Address
                          </a>
                        </li>
                        <li
                          className={
                            this.state.activetab === "tab_d" ? "active" : ""
                          }
                        >
                          <a
                            href="#tab_d"
                            data-toggle="pill"
                            onClick={() => this.tabActive("tab_d")}
                          >
                            Status & Comments
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div className="col-xl-12 col-md-12 col-sm-12 p-0">
                      <div className="tab-content">
                        <div className="tab-pane active" id="tab_a">
                          <div className="tab-icon" />
                          <div className="edit-details-head pb-4">
                            Edit Order Details
                          </div>
                          <div className="edit-details-wrap">
                            <div className="form-group row">
                              <label
                                htmlFor="inputOrderID"
                                className="col-sm-2 col-form-label"
                              >
                                Order ID:
                              </label>
                              <div className="col-sm-3">
                                <input
                                  type="text"
                                  className="form-control"
                                  disabled
                                  id="inputOrderID"
                                  value={this.state.data.order_no}
                                />
                              </div>
                              <div className="col-sm-2 col-form-label" />
                              <label
                                htmlFor="inputstorename"
                                className="col-sm-2 col-form-label"
                              >
                                Payment Id:
                              </label>
                              <div className="col-sm-3">
                                <input
                                  type="text"
                                  className="form-control"
                                  id="inputstorename"
                                  value={this.state.paymentid}
                                  placeholder="Payment Id"
                                  disabled
                                />
                              </div>
                            </div>
                            <div className="form-group row">
                              <label
                                htmlFor="inputInvoiceID"
                                className="col-sm-2 col-form-label"
                              >
                                Invoice ID:
                              </label>
                              <div className="col-sm-3">
                                {!this.state.invoice_url ? (
                                  <button
                                    title=" Generate Tracking number  and then click Generate Invoice Id "
                                    onClick={this.handleGenerateInvoice}
                                    type="button"
                                    className="btn bgenerate"
                                    disabled={
                                      !this.state.trackingNumber ? true : false
                                    }
                                  >
                                    Generate
                                    {this.state.upload_status === true ? (
                                      <ReactSpinner
                                        type="border"
                                        color="dark"
                                        size="1"
                                      />
                                    ) : (
                                      ""
                                    )}
                                  </button>
                                ) : (
                                  <a
                                    target="_blank"
                                    href={this.state.invoice_url}
                                    type="button"
                                    className="btn bgenerate"
                                  >
                                    Download
                                  </a>
                                )}
                              </div>

                              <div className="col-sm-2 col-form-label" />
                              <label
                                htmlFor="dateadded"
                                className="col-sm-2 col-form-label"
                              >
                                Order Date:
                              </label>
                              <div className="col-sm-3">
                                <DatePicker
                                  selected={this.state.date}
                                  timeInputLabel="Time:"
                                  dateFormat="MMMM d, yyyy h:mm aa"
                                  showTimeInput
                                  onChange={this.handledateChange}
                                />

                                {this.state.errors.created_on ? (
                                  <div className="error text-danger">
                                    {this.state.errors.created_on}
                                  </div>
                                ) : (
                                  ""
                                )}
                               
                              </div>
                            </div>

                            <div className="form-group row">
                              <label
                                htmlFor="inputCustomer"
                                className="col-sm-2 col-form-label"
                              >
                                Customer:
                              </label>
                              <div className="col-sm-3">
                                <div className="customer-link">
                                  <Link
                                    className="mr-2"
                                    to={
                                      "/admin/users/edit/" +
                                      this.state.data.user_id
                                    }
                                  >
                                    {this.state.user_data.first_name +
                                      "  " +
                                      this.state.user_data.last_name}
                                  </Link>
                                </div>
                              </div>
                              <div className="col-sm-2 col-form-label" />
                              <label
                                htmlFor="inputCustomer"
                                className="col-sm-2 col-form-label"
                              >
                                Status :
                              </label>
                              <div className="col-sm-3">
                                <select
                                  value={this.state.status}
                                  onChange={this.handlestatusChange}
                                >
                                  <option value="Cancelled">Cancelled</option>
                                  <option value="Completed">Completed</option>
                                  <option value="Delivery">Delivery</option>
                                  <option value="Ongoing">Ongoing</option>
                                  <option value="Processing">Processing</option>
                                  <option value="Refund">Refund</option>
                                  <option value="succeeded">Succeeded</option>
                                </select>
                                {this.state.errors.status ? (
                                  <div className="error text-danger">
                                    {this.state.errors.status}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                            </div>

                            {this.state.status != "Cancelled" ? (
                              <div className="form-group row">
                                <label
                                  htmlFor="inputInvoiceID"
                                  className="col-sm-2 col-form-label"
                                >
                                  Tracking Number :
                                </label>
                                <div className="col-sm-3">
                                  {this.state.trackingNumber == "" ? (
                                    <button
                                      className="btn bgenerate"
                                      onClick={this.shippingconfirm}
                                    >
                                      Generate
                                      {this.state.ship_status === true ? (
                                        <ReactSpinner
                                          type="border"
                                          color="dark"
                                          size="1"
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </button>
                                  ) : (
                                    <p>
                                      {this.state.trackingNumber.replace(
                                        /,\s*$/,
                                        ""
                                      )}
                                    </p>
                                  )}

                                  <div>
                                    {" "}
                                    {this.state.message_confirm !== "" ? (
                                      <div className="tableinformation">
                                        <div
                                          className={this.state.message_type}
                                        >
                                          <p>{this.state.message_confirm}</p>
                                        </div>
                                      </div>
                                    ) : null}{" "}
                                  </div>
                                </div>
                              </div>
                            ) : (
                              ""
                            )}
                          </div>

                          <OrderItems
                            triggerParentUpdate={this.UpdateRefund}
                            orders={this.state.data}
                            total={this.state.total}
                            shipping={this.state.shipping_method}
                            product={this.state.product}
                            paymentid={this.state.paymentid}
                            orderid={this.props.match.params.id}
                            refund={this.state.refund}
                            status={this.state.status}
                          />
                        </div>

                        <div className="tab-pane" id="tab_b">
                          <div className="tab-icon" />

                          <div className="form-group row details-wrap-div">
                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label> First Name:</label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              {this.state.shipping_details.first_name
                                ? this.state.shipping_details.first_name
                                : "-"}
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label>Last Name:</label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              {this.state.shipping_details.last_name
                                ? this.state.shipping_details.last_name
                                : "-"}
                            </div>

                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label> Email: </label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              {this.state.shipping_details.email
                                ? this.state.shipping_details.email
                                : "-"}
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label> Company (optional): </label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              {this.state.shipping_details.company
                                ? this.state.shipping_details.company
                                : "-"}
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label> Address:</label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              {this.state.shipping_details.address},{" "}
                              {this.state.shipping_details.appartment}
                            </div>

                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label> City:</label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              {this.state.shipping_details.city
                                ? this.state.shipping_details.city
                                : "-"}
                            </div>

                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label>Country:</label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              <CountryDropdown
                                className="form-control"
                                as="select"
                                value={this.state.shipping_details.country}
                                valueType="short"
                                disabled
                              />
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label> Region:</label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              <RegionDropdown
                                className="form-control"
                                blankOptionLabel="Select Region"
                                defaultOptionLabel="Select Region"
                                valueType="short"
                                countryValueType="short"
                                country={this.state.shipping_details.country}
                                value={this.state.shipping_details.region}
                                disabled
                              />
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label> ZIP Code:</label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              <input
                                type="text"
                                onChange={this.handleChange("zipcode")}
                                name="zipcode"
                                className="form-control"
                                id="inputstorename"
                                value={this.state.zipcode}
                                placeholder="Zipcode"
                              />
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 title-div">
                              <label>Phone (optional):</label>
                            </div>
                            <div className="col-lg-3 col-md-6 col-xs-6 detail-content-div">
                              {this.state.shipping_details.phone
                                ? this.state.shipping_details.phone
                                : "-"}
                            </div>
                          </div>
                        </div>
                        <div className="tab-pane note_content_tab" id="tab_d">
                          <div className="tab-icon " />
                          {this.state.order_notes.map((notes, nos) => (
                            <div key={nos}>
                              <div className="note_content">
                                <p style={{ margin: "0px" }}>
                                  {notes.message ? notes.message : notes.status}
                                </p>
                              </div>
                              <p style={{ marginTop: "1rem" }}>
                                {notes.created_on
                                  ? moment(notes.created_on).format(
                                      "DD-MM-YYYY HH:mm:ss"
                                    )
                                  : ""}
                              </p>
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-12 col-md-12 col-sm-12 p-0 mt-3">
                      <div className="order-details-btns form-inline form float-right">
                        <div className="float-left">
                          {this.state.page_status == 1 ? (
                            <Link onClick={this.movetoTrash} to="#">
                              Move to Trash
                            </Link>
                          ) : (
                            <Link
                              style={{ color: "#0050a8" }}
                              onClick={this.movetoPublish}
                              to="#"
                            >
                              Publish
                            </Link>
                          )}
                        </div>
                        <div className="float-right input-group-btn">
                          <button onClick={this.handleSubmit} className="btn">
                            Save Changes 
                          </button>
                        </div>
                      </div>
                    </div>

                    <div
                      style={{ textAlign: "center" }}
                      className="col-xl-12 col-md-12 col-sm-12 p-15 mt-3"
                    >
                      {this.state.message !== "" ? (
                        <div className="tableinformation">
                          <div className={this.state.message_type}>
                            <p>{this.state.message}</p>
                          </div>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminOrderDetails;
