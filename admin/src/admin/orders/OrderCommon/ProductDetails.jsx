import React, { Component } from 'react';
import * as manageProducts from "../../../ApiServices/admin/products";
import { Link } from 'react-router-dom';
class ProductDetails extends Component {
    state = { 
        product_image : ''
    }
    /* Get single products details */
    getProductSingle = async (id) => {
        const response =  await manageProducts.getSingleProduct(id);
        if(response.data.status == 1){
            if(response.data.data.product_data !== null)
            {
                this.setState({ product_image: response.data.data.product_data.images.featured_image.image_url });
            }     
        }
        else{            
        }  
      }
    componentDidMount = () => {
        this.getProductSingle(this.props.productid);
    };
    render() { 
       
        return (
            <React.Fragment>
               <p><Link to={'/admin/product/edit/'+ this.props.productid }><img src={this.state.product_image
                          ?this.state.product_image
                          : ""} />{this.props.productname}</Link>
                          </p>
            </React.Fragment>
        );
    }
}


export default ProductDetails;