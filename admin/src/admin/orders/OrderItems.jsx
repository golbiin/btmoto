import React, { Component } from 'react'
import ProductDetails from '../orders/OrderCommon/ProductDetails'
import { CURRENCY_SYMBOL } from '../../config.json'
import Joi from 'joi-browser'
import * as OrdersApi from '../../ApiServices/admin/orders'
import moment from 'moment'
import ReactSpinner from "react-bootstrap-spinner";
class OrderItems extends Component {
  constructor(props) {
    super(props);
    this.handleCheck = this.handleCheck.bind(this);

  }
  state = {
    refund_btn: false,
    total_amount: 0,
    refund_amount: 0,
    refund_note: '',
    refund_array: [],
    removed_product_array: [],
    amount_refunded: 0,
    payment_id: '',
    orderid: '',
    removestock_products: [],
    quanity: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    total: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    submit_status: false,
    errors: {},
    checked: false,
    lineSubtotal: 0,
    tax: 0,
    shipping_refund_amount: 0,
    tax_refund: 0,
    tax_refund_amount: 0,
    refunded_shipping_amt: 0,
    refunded_tax_amt: 0,
    removed_product_qty: 0,
    totalproduct_qty: 0,
    orders : {
      order_details : {},
      tax : { tax_name : '' } ,
      coupon_details : {}
    }
  }
   /* Joi validation schema */
  schema = {
    total_amount: Joi.number()
      .positive()
      .required()
      .error(() => {
        return {
          message: 'It is not allowed to be empty.'
        }
      }),
    refund_amount: Joi.number()
      .positive()
      .required()
      .error(() => {
        return {
          message: 'It is not allowed to be empty.'
        }
      }),
    refund_note: Joi.string()
      .error(() => {
        return {
          message: 'Please be sure you’ve filled in the note'
        }
      }),
    shipping_refund_amount: Joi.number()
      .positive()
      .allow(0)
      .error(() => {
        return {
          message: 'It is not allowed to be empty.'
        }
      }),
    tax_refund_amount: Joi.number()
      .positive()
      .allow(0)
      .error(() => {
        return {
          message: 'It is not allowed to be empty.'
        }
      }),
    removestock_products: Joi.any().allow(null),
    payment_id: Joi.string().allow(null),
    orderid: Joi.any().allow(null),
    quanity: Joi.any().allow(null),
    restockitems: Joi.any().allow(null),
  }
  validateProperty = (name, value) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)
    return error ? error.details[0].message : null
  }
  handleCheck(e) {
    this.setState({
      checked: e.target.checked
    })    
  }
/*Input Handle Change */
  handleChange = input => event => {
    const errors = { ...this.state.errors }
    delete errors.validate
    const errorMessage = this.validateProperty(input, event.target.value)
    if (errorMessage) errors[input] = errorMessage
    else delete errors[input]
    this.setState({ [input]: event.target.value, errors })
    if (input === 'refund_amount') {
      this.setState({
        quanity: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      })
      this.setState({ total: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] })
    }
  }

  enableRefund = () => {
    this.setState({ refund_btn: true })
  }
  cancelRefund = () => {
    this.setState({ refund_btn: false })
  }
  componentWillReceiveProps() {
    let itemsarray = [];
    let totalproduct_qty = 0;
    this.props.product.map((attribute, sidx) => {
      totalproduct_qty = totalproduct_qty * 1 + attribute.quantity * 1
      const Setdata = {}
      Setdata.count = 0
      Setdata._id = ''
      Setdata.amount = 0
      itemsarray.push(Setdata)
    })
    this.intialRemovedItemsfunction(this.props.refund);
    this.setState({
      total_amount: this.props.total.total ? this.props.total.total : 0,
      payment_id: this.props.paymentid ? this.props.paymentid : '',
      orderid: this.props.orderid,
      refund_array: this.props.refund ? this.props.refund : 0,
      removestock_products: itemsarray ? itemsarray : [],
      totalproduct_qty: totalproduct_qty ? totalproduct_qty : 0,
      tax: this.props.total.tax_total ? this.props.total.tax_total : 0,
      orders : this.props.orders
    })
    this.getAmount_Refunded(this.props.refund, this.props.total.total);
    this.lineSubtotal();
  }

  intialRemovedItemsfunction(refundarray) {
    let Removeditems = [];
    if (refundarray.length) {
      refundarray.map(
        (singlerefund, idx) =>
          Removeditems.push(singlerefund)
      )
    }
    this.getRemovedItems(Removeditems);
  }
/* Get Removed Order Items */
  getRemovedItems = async (Removeditems) => { 
    if (Removeditems.length > 0) {
      var holder = {};
      Removeditems.forEach(function (valuesare) {
        if (valuesare) {
          if (valuesare.removedstock) {
            valuesare.removedstock.forEach(function (removedstocksingle) {
              if ((removedstocksingle.count * 1) > 0) {
                if (removedstocksingle._id !== '') {
                  if (holder.hasOwnProperty(removedstocksingle._id)) {
                    holder[removedstocksingle._id] = holder[removedstocksingle._id] + removedstocksingle.count * 1;
                  } else {
                    holder[removedstocksingle._id] = removedstocksingle.count * 1;
                  }
                }
              }
            })
          }
        }       
      })
    }
    var obj2 = [];
    if (holder) {
      for (var prop in holder) {
        obj2.push({ _id: prop, count: holder[prop] });
      }
    }
    this.setState({
      removed_product_array: obj2
    });
  }
  getAmount_Refunded = async (refund_array = [], total_amount = 0) => {
    let refundvalue = 0
    let refunded_shipping_amt = 0
    let refunded_tax_amt = 0
    let removed_product_qty = 0

    if (refund_array.length > 0) {
      refund_array.map(
        (singlerefund, idx) =>
          (
            (refundvalue = refundvalue + (singlerefund.other.amount * 1) / 100),
            (refunded_shipping_amt = refunded_shipping_amt + (singlerefund.shipping * 1)),
            (refunded_tax_amt = refunded_tax_amt + (singlerefund.tax * 1)),
            (removed_product_qty = removed_product_qty + (singlerefund.removedstock[0].count * 1))
          )
      )
    }  
    this.setState({
      amount_refunded: (refundvalue).toFixed(2),
      refunded_shipping_amt: (refunded_shipping_amt).toFixed(2),
      refunded_tax_amt: (refunded_tax_amt).toFixed(2),
      removed_product_qty: removed_product_qty
    })
  }
  getRefundArray = async () => {
    const response = await OrdersApi.getRefundArray(this.props.orderid);
    if (response.data.status === 1) {
      this.intialRemovedItemsfunction(response.data.data)
    }
  };

  RefundviaStripe = async () => {
    this.setState({ submit_status: true })
    const checkState = {
      payment_id: this.state.payment_id,
      refund_amount: this.state.refund_amount,
      orderid: this.state.orderid,
      total_amount: this.state.total_amount,
      removestock_products: this.state.removestock_products.filter(l => {
        return (
          l.count != 0
        )
      }),
      restockitems: this.state.checked,
      shipping_refund_amount: this.state.shipping_refund_amount,
      tax_refund_amount: this.state.tax_refund_amount
    }
    const errors = { ...this.state.errors }
    delete errors.validate
    let result = Joi.validate(checkState, this.schema)   
    if (result.error) {
      this.setState({ submit_status: false })
      let path = result.error.details[0].path[0]
      let errormessage = result.error.details[0].message
      errors[path] = errormessage
      this.setState({
        errors: errors
      })
    } else {
      const amount_refunded = { ...this.state.refund_array }
      if (
        window.confirm(
          `Are you sure you wish to process this refund? This action cannot be undone.`
        )
      ) {
        const response = await OrdersApi.refundOrder(checkState)
        if (response.data.status === 1) {
          this.setState({ submit_status: false })
          const newvalue = response.data.data         
          this.setState({ refund_array: [...this.state.refund_array, newvalue] })
          this.getAmount_Refunded(this.state.refund_array, this.props.total.total)
          this.getRefundArray()
          this.setState({
            message: response.data.message,
            message_type: 'success',
            refund_amount: 0,
            refund_note: '',
            quanity: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            total: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            products: [],
            shipping_refund_amount: 0,
          })
        } else {
          this.setState({ submit_status: false })
          this.setState({
            message: response.data.message,
            message_type: 'error'
          })
        }
        setTimeout(() => {
          this.setState({
            message: '',
            message_type: '',
            refund_btn: false
          })
        }, 6000)
      }
      this.setState({
        submit_status: false
      })
    }
  }

  handleQuanityChange = (idx, price, qunatity, product_id) => evt => {
    let valuequnaity = evt.target.value > qunatity ? qunatity : evt.target.value
    const quanity = this.state.quanity
    const total = this.state.total
    const removestock_products = this.state.removestock_products
    let totalqunatity = 1;
    let shipping_refund = 0;
    let tax_refund = 0;
    quanity[idx] = valuequnaity * 1 > 0 ? valuequnaity * 1 : 0
    total[idx] = valuequnaity * price > 0 ? valuequnaity * price : 0
    let totalrefundamount = 0
    total.map((totalamount, i) => {
      totalrefundamount = totalrefundamount * 1 + totalamount * 1
    })
    const stock_products = removestock_products.map((attribute, sidx) => {
      if (idx == sidx) {
        attribute.count = valuequnaity > 0 ? valuequnaity : 0
        attribute._id = product_id
        attribute.amount = valuequnaity * price > 0 ? valuequnaity * price : 0
        return attribute
      } else {
        return attribute
      }
    })

    /* Shipping refund calculation  */
    let totalproduct_qty = this.state.totalproduct_qty; // Current order product qunatity
    let rm_qty = this.state.removed_product_qty; // Reomved  product qunatity
    totalqunatity = (totalproduct_qty - rm_qty)
    shipping_refund =
      (totalqunatity > 0 ? (
        (
          (this.props.total.shipping_total ? this.props.total.shipping_total : 0)  // 1 quanity price
          / (totalproduct_qty > 0 ? totalproduct_qty : 1)) *
        (quanity.reduce((totalqunatity, Qty) => totalqunatity + Qty, 0) // current qunaity
        )
      ) : 0)
    tax_refund =
      (totalqunatity > 0 ? (
        (
          (this.props.total.tax_total ? this.props.total.tax_total : 0)  // 1 quanity price
          / (totalproduct_qty > 0 ? totalproduct_qty : 1)) *
        (quanity.reduce((totalqunatity, Qty) => totalqunatity + Qty, 0) // current qunaity
        )
      ) : 0)
    /* Shipping refund calculation  */
    this.setState({
      quanity: quanity,
      total: total,
      refund_amount: (totalrefundamount + shipping_refund + tax_refund).toFixed(2),
      removestock_products: stock_products,
      shipping_refund_amount: (shipping_refund).toFixed(2),
      tax_refund_amount: (tax_refund).toFixed(2)
    })
  }

  handletotalChange = idx => evt => {
    const total = this.state.total
    total[idx] = evt.target.value
    let totalrefundamount = 0
    total.map((totalamount, i) => {
      totalrefundamount = totalrefundamount * 1 + totalamount * 1
    })
    this.setState({
      total: total,
      refund_amount: totalrefundamount
    })
  }

  getcurrentRemovedQunaitybyId = (productid) => {
    let rowData = this.state.removed_product_array.filter(l => {
      return (
        l._id.match(productid)
      )
    });
    if (rowData.length > 0) {
      return (rowData[0].count ? rowData[0].count : 0)
    } else {
      return 0;
    }
  }

  lineSubtotal = () => {
    let lineSubtotal = 0;
    this.props.product.map((products, idx) => (
      lineSubtotal += ((products.price * 1) * (products.quantity * 1))
    ))
    this.setState({
      lineSubtotal: lineSubtotal
    })
  }

  render() { 
    return (
      <React.Fragment>
        <div className='edit-details-wrap order_list'>
          <div className='form-group row product_list'>
            <div className='single_order row head'>
              <div className='col-md-6'>Item</div>
              <div className='col-md-2'>Cost</div>
              <div className='col-md-2'>Qty</div>
              <div className='col-md-2'>Total</div>
            </div>

            {this.props.product.map((products, idx) => (
              <div key={idx} className='single_order row body'>
                <div className='col-md-6'>
                  <ProductDetails
                    productid={products.product_id}
                    productname={products.product_name}
                  />
                </div>
                <div className='col-md-2'>
                  <div>
                    {CURRENCY_SYMBOL + products.price}
                  </div>
                  <p style={{ 'marginBottom': '0px', 'color': 'red' }}></p>
                </div>
                <div className='col-md-2'>
                  {' '}
                  x {products.quantity}
                  {this.state.refund_btn === true ? (

                    <div>
                      <input
                        type='number'
                        placeholder='Quanity'
                        min='0'
                        max={products.quantity}
                        onChange={this.handleQuanityChange(
                          idx,
                          products.price,
                          (products.quantity - (

                            this.getcurrentRemovedQunaitybyId(products.product_id)
                          )),
                          products.product_id
                        )}
                        value={this.state.quanity[idx]}
                        className='form-control'
                      />

                    </div>
                  ) : (
                      ''
                    )}
                  {this.state.amount_refunded ?
                    <p style={{ 'marginBottom': '0px', 'color': 'red' }}>- x {
                      this.getcurrentRemovedQunaitybyId(products.product_id)
                    }</p> : ''
                  }
                </div>
                <div className='col-md-2'>
                  {CURRENCY_SYMBOL + (products.price * products.quantity)}
                  {this.state.refund_btn === true ? (
                    <div>
                      <input
                        type='text'
                        disabled
                        onChange={this.handletotalChange(idx)}
                        placeholder='Quanity'
                        value={this.state.total[idx]}
                        className='form-control'
                      />

                    </div>
                  ) : (
                      ''
                    )}

                  {this.state.amount_refunded ?
                    <p style={{ 'marginBottom': '0px', 'color': 'red' }}>{
                      CURRENCY_SYMBOL + (this.getcurrentRemovedQunaitybyId(products.product_id) * products.price)}</p> : ''
                  }
                </div>
              </div>
            ))}
          </div>
          {this.state.refund_array.length > 0 ? (
            <div className='refund_loop_conatiner'>
              {this.state.refund_array.map((singlerefund, idx) => (
                <div className='refund_loop row shipping  '>
                  <div className='col-md-10'>
                    <p>
                      Refund #{singlerefund.other.id} -{' '}
                      {moment
                        .unix(singlerefund.other.created)
                        .format('DD-MM-YYYY HH:mm:ss')}{' '}
                      by admin{' '}
                    </p>
                  </div>
                  <div className='col-md-2 red'>
                    <p style={{ 'margin-bottom': '0px', 'color': 'red' }}>
                      -
                      {singlerefund.other.amount > 0
                        ? CURRENCY_SYMBOL + singlerefund.other.amount / 100
                        : 0}
                    </p>
                  </div>
                </div>
              ))}{' '}
            </div>
          ) : (
              ''
            )}


          {this.props.total ? (
            <div className='single_order row shipping'>
              <div className="rownew">
                <div className='col-md-10'>
                  <p>Items Subtotal:</p>
                </div>
                <div className='col-md-2'>
                  <p>
                    {CURRENCY_SYMBOL + this.state.lineSubtotal}</p>
                </div>

              </div>
              <div className="rownew">
                <div className='col-md-10'>
                  <p>Shipping : <b><i>{ this.props.shipping.service ? this.props.shipping.service : ''}</i></b></p>                  
                </div>
                <div className='col-md-2'>
                  <p>{CURRENCY_SYMBOL + this.props.total.shipping_total}
                    {
                      this.state.refund_btn === true ? (
                        <span style={{ 'color': 'red' }}> - {this.state.refunded_shipping_amt}</span>
                      ) : ''
                    }
                  </p>
                  {this.state.refund_btn === true ? (
                    <input
                      type='number'
                      className='form-control'
                      disabled
                      onChange={this.handleChange('shipping_refund_amount')}
                      value={this.state.shipping_refund_amount * 1}
                    />
                  ) : ''
                  }
                  {this.state.errors.shipping_refund_amount !== '' ? (
                    <div className='validate_error'>
                      <p>{this.state.errors.shipping_refund_amount}</p>
                    </div>
                  ) : null}
                </div>
              </div>      
              <div className="rownew">
                <div className='col-md-10'>
                  <p>Tax : {
                   this.state.orders.tax ? 
                    (<b><i>{ this.state.orders.tax.tax_name}</i></b>) : ''
                  }              
                  </p>
                </div>
                <div className='col-md-2'>
                  <p>{CURRENCY_SYMBOL + this.state.tax}
                    {this.state.refund_btn === true ? (
                      <span style={{ 'color': 'red' }}>
                        - {this.state.refunded_tax_amt}</span>
                    ) : ''
                    }
                  </p>
                  {this.state.refund_btn === true ? (
                    <input
                      type='number'
                      className='form-control'
                      disabled
                      value={(this.state.tax_refund_amount * 1).toFixed(2)}
                    />
                  ) : ''
                  }
                </div>
              </div>
            {(this.props.total.discount !== '' && this.props.total.discount > 0 ) ?  
             ( <div className="rownew">
                <div className='col-md-10'>
                <p>Coupon : 
                  <b><i>{ this.state.orders.coupon_details.couponcode ? this.state.orders.coupon_details.couponcode : ''}</i></b>
                </p>                 
                </div>
                <div className='col-md-2'>
                  <p style={{ color: 'red' }}>{CURRENCY_SYMBOL + this.props.total.discount}  </p>
                </div>
              </div> ) : ''
              }
              <div className="rownew">
                <div className='col-md-10'>
                  <p>Refund :</p>  
                </div>
                <div className='col-md-2'>
                  <p style={{ color: 'red' }}>{CURRENCY_SYMBOL + this.state.amount_refunded}  </p>
                </div>
              </div>                     
              <div className="rownew">
                <div className='col-md-10'>
                  <p>
                    <b>Order Total</b>
                  </p>
                </div>
                <div className='col-md-2'>
                  <p>{CURRENCY_SYMBOL + ((this.props.total.total * 1).toFixed(2) - (this.state.amount_refunded * 1).toFixed(2)).toFixed(2)}</p>
                </div>
              </div>
            </div>
          ) : (
              ''
            )}
          <div
            className={
              this.state.refund_btn == false
                ? 'single_order row refunde_conatiner'
                : 'single_order row refunde_conatiner  bgm_refund'
            }
          >
            {(this.props.status !== 'Cancelled' && this.props.status !== 'Delivery') ? (
              this.state.refund_btn === false ? (
                <button
                  onClick={this.enableRefund}
                  className='primary refund_btn'
                  disabled={
                    this.state.submit_status === true ? 'disabled' : false
                  }
                >
                  Refund
                </button>
              ) : (
                  <div className='refund_container'>
                    <div className='order_single_items'>
                      <p>Restock refunded items:	</p>
                      <p  >
                        <input type="checkbox" style={{
                          'width': 'auto',
                          'margin-top': '4px'
                        }} onChange={this.handleCheck} checked={this.state.checked} />
                      </p>
                    </div>

                    <div className='order_single_items'>
                      <p>Amount already refunded: </p>
                      <p style={{ color: 'red' }}>
                        - {CURRENCY_SYMBOL + this.state.amount_refunded * 1}
                      </p>
                    </div>
                    <div className='order_single_items'>
                      <p>Total available to refund: </p>
                      <p>
                        {
                          (this.state.total_amount * 1 -
                            this.state.amount_refunded * 1).toFixed(2)}
                      </p>
                    </div>
                    <div className='order_single_items'>
                      <p>Refund amount: </p>
                      <p>
                        <input
                          type='number'
                          className='form-control'
                          onChange={this.handleChange('refund_amount')}
                          disabled
                          value={this.state.refund_amount * 1}
                        />
                        {this.state.errors.refund_amount !== '' &&
                          this.state.refund_amount * 1 <= 0 ? (
                            <div className='validate_error'>
                              <p>{this.state.errors.refund_amount}</p>
                            </div>
                          ) : null}
                      </p>
                    </div>
                    <div className='order_single_button'>
                      <button
                        onClick={this.cancelRefund}
                        className='primary cancel refund_btn'
                      >
                        Cancel
                  </button>
                      {this.props.total.total * 1 -
                        this.state.amount_refunded * 1 >=
                        this.state.refund_amount ? (
                          <button
                            onClick={this.RefundviaStripe}
                            disabled={
                              this.state.submit_status === true ? 'disabled' : ''
                            }
                            className='primary refundviastripe refund_btn'
                          >
                            Refund via Stripe
                            {this.state.submit_status === true ? (
                              <ReactSpinner type="border" color="dark" size="1" />
                            ) : (
                                ""
                              )}
                          </button>
                        ) : (
                          ''
                        )}
                    </div>
                    {this.state.message ? (
                      <div className='order_single_button'>
                        {this.state.message !== '' ? (
                          <p style={{ 'text-align': 'center' }} className='tableinformation'>
                            <div style={{ 'text-align': 'center' }} className={this.state.message_type}>
                              <p style={{ 'text-align': 'center' }}>{this.state.message}</p>
                            </div>
                          </p>
                        ) : null}
                      </div>
                    ) : (
                        ''
                      )

                    }
                  </div>
                )

            ) : ''

            }
          </div>
        </div>
      
      </React.Fragment>
    )
  }
}

export default OrderItems
