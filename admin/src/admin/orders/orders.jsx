import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import DeleteConfirm from '../common/deleteConfirm'
import * as OrdersApi from '../../ApiServices/admin/orders'
import memoize from 'memoize-one'
import ReactSpinner from 'react-bootstrap-spinner'
import { CSVLink, CSVDownload } from "react-csv";
class Orders extends Component {
  state = {
    activetab: 'tab_a',
    tabData: [
      { name: 'Tab 1', isActive: true },
      { name: 'Tab 2', isActive: false },
      { name: 'Tab 3', isActive: false }
    ],
    search: '',
    data: [],
    dataTrash: [],
    contextActions_select: '0',
    contextActionsTrashselect: 'untrash',
    toggleCleared: false,
    spinner: true,
    message: '',
    message_type: '',
    columns: [
      {
        name: 'ORDER ID',
        cell: row => (
          <Link
            to={{ pathname: '/admin/orders/' + row.id, state: { id: row.id } }}
          >
            {row.order_no}
          </Link>
        ),
        allowOverflow: true,
        button: true,
        width: '35px', // custom width for icon button
        hide: 'sm'
      },
      {
        name: 'PRODUCT',
        cell: row => (
          <Link
            className='product_div'
            to={{ pathname: '/admin/orders/' + row.id, state: { id: row.id } }}
          >
            <p>{row.title}</p>
          </Link>
        ),
        selector: 'title',
        allowOverflow: true,
        sortable: true,
      },
      {
        name: 'Email',
        selector: 'email',
        sortable: true,
        cell: row => <div className='order_email'>{row.email}</div>,
        allowOverflow: false,
        left: true
      },
      {
        name: 'Date',
        selector: 'date',
        sortable: true,
        left: true,
        hide: 'sm',
        maxWidth : '110px'
      },
      {
        name: 'QUANTITY',
        selector: 'qunatity',
        center: true,
        maxWidth : '30px'
      },

      {
        name: 'STATUS',
        selector: 'status',
        cell: row => (
          <i className={'order_status ' + row.status}>{row.status}</i>
        ),
        left: true,
        hide: 'sm',
        maxWidth : '30px'
      },
      {
        name: 'PRICE',
        selector: 'price',
        sortable: true,
        left: true,
        hide: 'sm',
        maxWidth : '30px'
      },

      {
        name: '',
        cell: row => (
          <div className='action_dropdown' data-id={row.id}>
            <i
              data-id={row.id}
              className='dropdown_dots'
              data-toggle='dropdown'
              aria-haspopup='true'
              aria-expanded='false'
            />
            <div
              className='dropdown-menu dropdown-menu-center'
              aria-labelledby='dropdownMenuButton'
            >
              <Link to={ '/admin/orders/' + row.id}                  
                  className='dropdown-item'
                >
                  Edit Order
              </Link> 
              {row.page_status == 1  ? (
                <Link
                  onClick={() => this.saveAndtoogle(row.id)}
                  className='dropdown-item'
                >
                  Trash
                </Link>
              ) : (
                <Link
                  onClick={() => this.deleteAndtoogle(row.id)}
                  className='dropdown-item'
                >
                  Delete
                </Link>
              )}
            </div>
          </div>
        ),
        allowOverflow: false,
        button: true,
        width: '30px', // custom width for icon button,
        maxWidth : '56px'
      }
    ]
  }

  removecomma (Items ,key){
    if(key == 'title'){
      return Items.replace(',','');
    } else {
      return Items;
    }   
  }

  convertArrayOfObjectsToCSV = array => {
    let result
    const columnDelimiter = ','
    const lineDelimiter = '\n'
    const keys = Object.keys(array[0])
    result = ''
    result += keys.join(columnDelimiter)
    result += lineDelimiter   
    array.forEach(item => {
      let ctr = 0
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter
        result += this.removecomma(item[key],key);
        ctr++
      })
      result += lineDelimiter
    })
    return result
  }

  downloadCSV = array => {
    const link = document.createElement('a')
    let csv = this.convertArrayOfObjectsToCSV(array)
    if (csv == null) return
    const filename = 'Order.csv'
    if (!csv.match(/^data:text\/csv/i)) {
      csv = `data:text/csv;charset=utf-8,${csv}`
    }
    link.setAttribute('href', encodeURI(csv))
    link.setAttribute('download', filename)
    link.click()
  }
  tabActive = async tab => {
    this.setState({ activetab: tab })
  }

  /* Trash */
  saveAndtoogle = id => {
    this.setState({ index: id })
    this.toogle()
  }
  toogle = () => {
    let status = !this.state.modal
    this.setState({ modal: status })
  }
  trashOrder = async () => {
    let id = this.state.index
    const response = await OrdersApi.trashOrder(id)
    if (response) {
      if (response.data.status == 1) {
        const data = this.state.data.filter(i => i.id !== id)
        this.setState({ data })
        this.toogle()
        this.getAllOrders()
      }
    }
  }

  /* Delete */
  deleteAndtoogle = id => {
    this.setState({ index: id })
    this.toogleDelete()
  }
  toogleDelete = () => {
    let status = !this.state.modalDelete
    this.setState({ modalDelete: status })
  }

  deleteOrder = async () => {
    let id = this.state.index
    const response = await OrdersApi.deleteOrder(id)
    if (response) {
      if (response.data.status == 1) {
        const dataTrash = this.state.data.filter(i => i.id !== id)
        this.setState({ dataTrash })
        this.toogleDelete()
        this.getAllOrders()
      }
    }
  }

  searchSpace = event => {
    let keyword = event.target.value
    this.setState({ search: keyword })
  }
  componentDidMount = () => {
    this.getAllOrders()
  }
/* Get all Orders */
  getAllOrders = async () => {
    try {
      this.setState({ spinner: true })
      const response = await OrdersApi.getAllOrders()
      if (response.data.status == 1) {
        const data_table_row = []
        const data_table_row_trash = []
        this.setState({ spinner: false })
        response.data.data.map((item, index) => {
          if (item.page_status == 0) {
            let product_id =''
            let quantity = 0
            let product_name = ''
            let removed_product_qty = 0
            const SetdataTrash = {}
            SetdataTrash.id = item._id
            SetdataTrash.status = item.status
            let date = new Date(item.created_on)
            let year = date.getFullYear()
            let month = date.getMonth() + 1
            let dt = date.getDate()
            if (dt < 10) {
              dt = '0' + dt
            }
            if (month < 10) {
              month = '0' + month
            }
            SetdataTrash.date = dt + '-' + month + '-' + year
            item.order_details.product.map((item1, index) => {
              if (item1.product_name) {
                product_name += item1.product_name + ' ,'

                if (item1.quantity) {
                  quantity =
                    parseInt(quantity, 10) + parseInt(item1.quantity, 10)
                }  

                if (item1.product_id) {
                  product_id = item1.product_id ; 
  
                }
              }          
            })
            if ( item.order_details.refund.length > 0) { // removed quanity
              item.order_details.refund.map(
                (singlerefund, idx) =>
                  (
                    (removed_product_qty = removed_product_qty + (singlerefund.removedstock[0].count * 1))
        
                  )
              )
        
            }

            SetdataTrash.title = product_name.replace(/.$/,"")
            SetdataTrash.qunatity = (quantity - removed_product_qty)
            SetdataTrash.email = item.shipping_details.email
            SetdataTrash.price = item.order_details.total.total
            SetdataTrash.page_status = item.page_status
            SetdataTrash.order_no =  item.order_no ? item.order_no : '' 
            SetdataTrash.product_id = product_id
            data_table_row_trash.push(SetdataTrash)
          } else {
            let product_id =""
            let quantity =  0
            let refundvalue = 0
            let product_name = ''
            let removed_product_qty = 0
            const Setdata = {}
            Setdata.id = item._id
            Setdata.status = item.status
            let date = new Date(item.created_on)
            let year = date.getFullYear()
            let month = date.getMonth() + 1
            let dt = date.getDate()
            if (dt < 10) {
              dt = '0' + dt
            }
            if (month < 10) {
              month = '0' + month
            }
            Setdata.date = dt + '-' + month + '-' + year
            item.order_details.product.map((item1, index) => {
              if (item1.product_name) {
                product_name += item1.product_name + ', '
                if (item1.quantity) {
                  quantity =
                    parseInt(quantity, 10) + parseInt(item1.quantity, 10)
                }
                if (item1.product_id) {
                  product_id = item1.product_id ; 
                }
              }
            })
            refundvalue = 
          ( item.order_details.refund.length > 0 ?
            ( item.order_details.refund.reduce((refundvalue, Qty) => refundvalue * 1 + (Qty.other.amount * 1) / 100, 0)
            ) : 0
          )        
          if ( item.order_details.refund.length > 0) { // removed quanity
            item.order_details.refund.map(
              (singlerefund, idx) =>
                (
                  (removed_product_qty = removed_product_qty + (singlerefund.removedstock[0].count * 1))
      
                )
            )
      
          }           
            Setdata.product_id = product_id;
            Setdata.title = product_name.replace(/, $/,"")
            Setdata.qunatity = (quantity - removed_product_qty)
            Setdata.email = item.shipping_details.email
            Setdata.price =  (item.order_details.total.total - refundvalue).toFixed(2)
            Setdata.page_status = item.page_status
            Setdata.order_no =  item.order_no ? item.order_no : '' 
            data_table_row.push(Setdata)
          }
        })
        this.setState({ dataTrash: data_table_row_trash })
        this.setState({ data: data_table_row })
        this.setState({ spinner: false })
      } else {
        this.setState({ spinner: false })
      }
    } catch (err) {
      this.setState({ spinner: false })
    }
  }

  actionHandlerOrder = async (Orderids, contextActions, status) => {
    try {
      const response = await OrdersApi.actionHandlerOrder(
        Orderids,
        contextActions,
        status
      )
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: 'success'
        })

        this.getAllOrders()
      } else {
        this.setState({
          message: response.data.message,
          message_type: 'error'
        })
        this.getAllOrders()
      }
    } catch (err) {
      this.setState({
        message: 'Something went wrong',
        message_type: 'error'
      })
    }
  }

  actionTrashHandlerOrder = async (Orderids, contextActions) => {
    try {
      const response = await OrdersApi.actionTrashHandlerOrder(
        Orderids,
        contextActions
      )
      if (response.data.status == 1) {
        this.setState({
          message: response.data.message,
          message_type: 'success'
        })
        this.getAllOrders()
      } else {
        this.setState({
          message: response.data.message,
          message_type: 'error'
        })
        this.getAllOrders()
      }
    } catch (err) {
      this.setState({
        message: 'Something went wrong',
        message_type: 'error'
      })
    }
  }

  actionHandler = e => {
    const { selectedRows } = this.state
    const Orderids = selectedRows.map(r => r.id)
    let status = ''

    let actionvalue = this.state.contextActions_select
    if (actionvalue === '0') {
      status = 'Trash'
    } else if (actionvalue === '1') {
      status = 'Done'
    } else if (actionvalue === '2') {
    }
    if (
      window.confirm(
        `Are you sure you want to move ` + status + ` :\r ${Orderids}?`
      )
    ) {
      this.setState({ toggleCleared: !this.state.toggleCleared })
      this.actionHandlerOrder(
        Orderids,
        this.state.contextActions_select,
        status
      )
    }
  }

  actionTRashHandler = e => {
    const { selectedRows } = this.state
    const Orderids = selectedRows.map(r => r.id)
    let status = ''
    let actionvalue = this.state.contextActionsTrashselect
    if (
      window.confirm(
        `Are you sure you want to move ` + actionvalue + ` :\r ${Orderids}?`
      )
    ) {
      this.setState({ toggleCleared: !this.state.toggleCleared })
      this.actionTrashHandlerOrder(
        Orderids,
        this.state.contextActionsTrashselect
      )
    }
  }

  handlerowChange = state => {
    this.setState({ selectedRows: state.selectedRows }) 
  }
  handlebackorderChange = async e => {
    this.setState({ contextActions_select: e.target.value })
  }
  handlecontextActionsTrash = async e => {
    this.setState({ contextActionsTrashselect: e.target.value })
  }
  render () {
    const contextActions = memoize(actionHandler => (
      <div className='common_action_section'>
        <select
          value={this.state.contextActions_select}
          onChange={this.handlebackorderChange}
        >
          <option value='0'>Trash</option>
        </select>
        <button className='danger' onClick={this.actionHandler}>
          Apply
        </button>
      </div>
    ))

    const contextActionsTrash = memoize(actionHandler => (
      <div className='common_action_section'>
        <select
          value={this.state.contextActionsTrashselect}
          onChange={this.handlecontextActionsTrash}
        >
          <option value='untrash'>Restore</option>
          <option value='delete'>Delete Permanently</option>
        </select>
        <button className='danger' onClick={this.actionTRashHandler}>
          Apply
        </button>
      </div>
    ))

    let rowData = this.state.data
    let rowDataTrash = this.state.dataTrash
    let search = this.state.search
    if (search.length > 0) {
      search = search.trim().toLowerCase()
      rowData = rowData.filter(l => {
        return (
          l.title.toLowerCase().match(search) ||
          l.status.toLowerCase().match(search) ||
          l.email.toLowerCase().match(search)
        )
      })
      rowDataTrash = rowDataTrash.filter(l => {
        return (
          l.title.toLowerCase().match(search) ||
          l.status.toLowerCase().match(search) ||
          l.email.toLowerCase().match(search)
        )
      })
    }

    return (
      <React.Fragment>
        <div className='container-fluid admin-body '>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12  content'>
              <div className='row content-row'>
                <div className='col-md-12  header'>
                  <Adminheader props={this.props} />
                </div>
                <div className='col-md-12  contents  home-inner-content pt-4 pb-4 pr-4'>
                  <div className='main_admin_page_common'>
                    <div className=''>
                      <div className='admin_breadcum'>
                        <div className='row'>
                          <div className='col-md-1'>
                            <p className='page-title'>Orders</p>
                          </div>
                          <div className='col-md-6'>
                            <CSVLink
                              filename={"Order.csv"}
                              className="Export_data_btn"
                              data={rowData}> Export Data
                            </CSVLink>
                          </div>
                          <div className='col-md-5'>
                            <div className='searchbox'>
                              <div className='commonserachform'>
                                <span />
                                <input
                                  type='text'
                                  placeholder='Search'
                                  onChange={e => this.searchSpace(e)}
                                  name='search'
                                  className='search form-control'
                                />
                                <input type='submit' className='submit_form' />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className=' order-summary-tab-wrapper bg-white'>                   
                        <div className='col-xl-12 col-md-12 col-sm-12  p-0'>
                          <ul className='nav nav-pills nav-fill'>
                            <li
                              className={
                                this.state.activetab === 'tab_a' ? 'active' : ''
                              }
                            >
                              <a
                                href='#tab_a'
                                data-toggle='pill'
                                onClick={() => this.tabActive('tab_a')}
                              >
                                All ({rowData.length})
                              </a>
                            </li>

                            <li
                              className={
                                this.state.activetab === 'tab_b' ? 'active' : ''
                              }
                            >
                              <a
                                href='#tab_b'
                                data-toggle='pill'
                                onClick={() => this.tabActive('tab_b')}
                              >
                                Trash ({rowDataTrash.length})
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div
                          style={{
                            display:
                              this.state.spinner === true
                                ? 'flex justify-content-center '
                                : 'none'
                          }}
                          className='overlay text-center'
                        >
                          <ReactSpinner
                            type='border'
                            color='primary'
                            size='10'
                          />
                        </div>
                        <div className='col-xl-12 col-md-12 col-sm-12 p-0'>
                          <div className='tab-content'>
                            {this.state.message !== '' ? (
                              <p className='tableinformation'>
                                <div className={this.state.message_type}>
                                  <p>{this.state.message}</p>
                                </div>
                              </p>
                            ) : null}
                            <div className='tab-pane active' id='tab_a'>
                              <DataTable
                                columns={this.state.columns}
                                data={rowData}
                                selectableRows
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                clearSelectedRows={this.state.toggleCleared}
                                onSelectedRowsChange={this.handlerowChange}
                                contextActions={contextActions(this.deleteAll)}
                                paginationPerPage = {30}
                                paginationRowsPerPageOptions = {[30, 50, 75, 100]}
                                noDataComponent = {<p>There are currently no records.</p>}
                                
                              />
                            </div>

                            <div className='tab-pane' id='tab_b'>
                              <DataTable
                                columns={this.state.columns}
                                data={rowDataTrash}
                                selectableRows  
                                highlightOnHover
                                pagination
                                selectableRowsVisibleOnly
                                clearSelectedRows={this.state.toggleCleared}
                                onSelectedRowsChange={this.handlerowChange}
                                contextActions={contextActionsTrash(
                                  this.deleteAll
                                )}
                                paginationPerPage = {30}
                                paginationRowsPerPageOptions = {[30, 50, 75, 100]}
                                noDataComponent = {<p>There are currently no records.</p>}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.trashOrder}
          message='Are you sure want to trash?'
          name='Trash'
        />

        <DeleteConfirm
          modal={this.state.modalDelete}
          toogle={this.toogleDelete}
          deleteUser={this.deleteOrder}
        />
      </React.Fragment>
    )
  }
}

export default Orders
