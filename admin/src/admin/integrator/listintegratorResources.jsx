import React, { Component } from 'react';
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import { Link } from "react-router-dom";
import DataTable from 'react-data-table-component';
import ReactSpinner from "react-bootstrap-spinner";
import DeleteConfirm from "../common/deleteConfirm";
import * as manageIntegrator from "../../ApiServices/admin/integrator";

class ListIntegratorResources extends Component {
    state = { 
        search: '',
        data_table : [],
        spinner : true,
        columns : [
          {
            name: 'Resource Category',
            selector: 'resource_category',
            sortable: true,
            cell: row => <div><a href={row._id} >{row.resource_category}</a></div>,
          },
        
          {
            name: 'File Name',
            selector: 'filename',
            left: true,
            hide: 'sm',
            width: 300,
          },
          {
            name: 'Version',
            selector: 'version',
            left: true,
            hide: 'sm',
            width: 300,
          },
          {
            name: 'Resource Type',
            selector: 'resource_type',
            left: true,
            hide: 'sm',
            width: 300,
          },
          
      
          {
            name: '',
            cell:  row =><div className="action_dropdown" data-id={row._id}><i data-id={row._id} className="dropdown_dots"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
            <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
            <Link  to={"/admin/resources/edit/" + row._id} className="dropdown-item">
                  Edit Page
            </Link>
            <Link
              onClick={() =>
                this.saveAndtoogle(row._id)
              } className="dropdown-item"
            >
              Delete
            </Link>
            </div>
            </div>,
            allowOverflow: false,
            button: true,
            width: '56px', // custom width for icon button
        },
        ]

    };


    componentDidMount = () => {
        this.getAllIntegratorResources();
    };

    DeletePageId = id => { 
        const data_table = this.state.data_table.filter(i => i.id !== id);
        this.setState({data_table})
      };

      /* Get all Integrator and Distributor Resources*/
      getAllIntegratorResources = async () => {
        this.setState({ spinner: true});
          try {
            const response =  await manageIntegrator.getAllIntegratorResources();
              if (response.data.status === 1) {
                const data_table_row = [];
                response.data.data.map((news,index) => {
                const Setdata = {};
                Setdata.filename =  news.filename;
                Setdata.version =  news.version;
                Setdata._id =  news._id;
                Setdata.resource_category = news.resource_category;
                Setdata.resource_type = news.resource_type;
                data_table_row.push(Setdata);
                });
                this.setState({ 
                data_table: data_table_row
                });                
              } 
            this.setState({ spinner: false});
          } catch (err) {
            this.setState({ spinner: false});
          }
        }

    searchSpace=(event)=>{
        let keyword = event.target.value;
        this.setState({search:keyword})
    }
    /* Deleted list Integrator and Distributor Resources*/
    deleteIntegrator = async () => { 
        let id = this.state.index;
        const response = await manageIntegrator.deleteIntegrator(id);
        if (response) {
          if (response.data.status === 1) {
            const data_table = this.state.data_table.filter(i => i._id !== id);
            this.setState({data_table})
            this.toogle();
          }
        }
      };

    saveAndtoogle = (id) => {
      this.setState({ index: id });
      this.toogle();
    };
    toogle = () => {
      let status = !this.state.modal;
      this.setState({ modal: status });
    };
   
    render() { 
        let rowData =  this.state.data_table;
        let search = this.state.search;
        if(search.length > 0){
           search = search.trim().toLowerCase();
           rowData = rowData.filter(l => {
               return (l.resource_category.toLowerCase().match( search ) ||
                l.filename.toLowerCase().match( search ) || 
                l.version.toLowerCase().match( search )|| 
                l.resource_type.toLowerCase().match( search )  );
           });
       }
        return ( <React.Fragment>
            <div className="container-fluid admin-body ">
            <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
                  <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12  content">
            <div className="row content-row">
                <div className="col-md-12  header">
                <Adminheader  props={this.props} />
                </div>
                <div className="col-md-12  contents  home-inner-content pt-4 pb-4 pr-4" >   
                <div className="main_admin_page_common">
                <div className="">
                <div className="admin_breadcum">
                    <div className="row">
                    <div className="col-md-1"><p className="page-title">Resource</p></div>
                    <div className="col-md-6">
                        <Link className="add_new_btn" to="/admin/resources/add">Add New</Link>
                    </div>
                    <div className="col-md-5">
                    <div className="searchbox">
                        <div className="commonserachform">
                        <span></span>
                        <input
                        type="text"
                        placeholder="Search" onChange={(e)=>this.searchSpace(e)} 
                        name="search"
                        className="search form-control"
                        />
                        <input type="submit" className="submit_form"/>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
                <div className="faq-list-table-wrapper">
                <React.Fragment>
                  <div style={{ display: this.state.spinner === true ? 'flex justify-content-center ' : 'none' }} className="overlay text-center">
                        <ReactSpinner type="border" color="primary" size="10" />
                  </div>
                </React.Fragment>
                <DataTable
                columns={this.state.columns}
                data={rowData}
                highlightOnHover
                pagination
                selectableRowsVisibleOnly
                noDataComponent = {<p>There are currently no records.</p>}
                />
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>                
            </div>
            </div>
            <DeleteConfirm
              modal={this.state.modal}
              toogle={this.toogle}
              deleteUser={this.deleteIntegrator}
            />
            </React.Fragment> );
    }
}
 
export default ListIntegratorResources;