import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import ReactSpinner from "react-bootstrap-spinner";
import Joi, { join } from "joi-browser";
import * as manageProducts from "../../ApiServices/admin/products";
import * as manageIntegrator from "../../ApiServices/admin/integrator";
import BlockUi from 'react-block-ui';
class IntegratorResources extends Component {
  constructor(props) {
    super(props);
    this.uploadMultipleFiles = this.uploadMultipleFiles.bind(this);
  }

  state = {

    file_url: "",

    uploading: false,
    profile_image: "",
    data: {
      category_id: "",
      product_id: "",
      resource_category: "",
      filename: "",
      version: "",
      resource_type: ""
    },

    upload_status: false,
    blocking: false,
    upload_status: false,
    parent_category: "",
    parentcategories: [],
    productcategories: [],
    soft_upload_status: false,
    docum_upload_status: false,
    upload_data: "",
    message: "",
    message_type: "",
    spinner: false,
    errors: {}
  };

  /*Joi validation schema*/
  schema = {
    filename: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),

    version: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is a required field."
        };
      }),

    file_url: Joi.string(),
    resource_type: Joi.string(),
    category_id: Joi.optional().label("category_id"),
    product_id: Joi.optional().label("product_id"),
    resource_category: Joi.optional().label("resource_category")
  };

  handleParentCatChange = async e => {
    let data = { ...this.state.data };
    let errors = { ...this.state.errors };
    data["category_id"] = e.target.value;
    this.setState({ data: data }, () => {
      this.getAllProductsCat();
    });
    delete errors.category_id;
  };

  /* Resources handle Change */
  handleChange = async ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];

    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  /*Joi Validation Call*/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  componentDidMount = () => {
    this.getAllCategory();
    this.getAllProductsCat();
  };

  /* Get all categories for integrator,distributor resources */
  getAllCategory = async () => {
    const response = await manageProducts.getAllCategory();
    if (response.data.status == 1) {
      this.setState({ parentcategories: response.data.data });
    }
  };

  /* Get all Products for integrator,distributor resources */
  getAllProductsCat = async () => {
    this.setState({ spinner: true });
    try {
      const response = await manageProducts.getAllProductsCat(
        this.state.data.category_id
      );
      if (response.data.status == 1) {
        this.setState({
          productcategories: response.data.data
        });
      }

      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };
  /* upload multiple file upload */
  uploadMultipleFiles = async e => {
    this.setState({ upload_status: true });
    const response1 = await manageProducts.uploadProductImages(e.target.files);

    if (response1.data.status == 1) {
      response1.data.data.file_location.map((item, key) =>
        this.setState({
          productfileArray: item.Location
        })
      );

      this.setState({ upload_status: false });
    } else {
      this.setState({
        submitStatus: false,
        message: response1.data.message,
        responsetype: "error"
      });
      this.setState({ upload_status: false });
    }
  };

  /* Form Submit */
  handleSubmit = async () => {
    const data = {
      product_id: this.state.data.product_id,
      category_id: this.state.data.category_id,
      resource_category: this.state.data.resource_category,
      filename: this.state.data.filename,
      version: this.state.data.version,
      resource_type: this.state.data.resource_type
    };
    const errors = { ...this.state.errors };
    if (data.category_id === "") {
      errors.category_id = "Please choose a category.";
      this.setState({
        errors: errors
      });
    } else {
      delete errors.category_id;
      this.setState({ errors });
    }
    if (data.product_id === "") {
      errors.product_id = "Please select a product.";
      this.setState({
        errors: errors
      });
    } else {
      delete errors.product_id;
      this.setState({ errors });
    }
    if (data.resource_category === "") {
      errors.resource_category = "Please choose a Resource Category.";
      this.setState({
        errors: errors
      });
    } else {
      delete errors.resource_category;
      this.setState({ errors });
    }

    if (data.resource_type === "") {
      errors.resource_type = "This field is a required field.";
      this.setState({
        errors: errors
      });
    } else {
      delete errors.resource_type;
      this.setState({ errors });
    }
    if (data.version === "") {
      errors.version = "This field is a required field.";
      this.setState({
        errors: errors
      });
    } else {
      delete errors.version;
      this.setState({ errors });
    }
    let result = Joi.validate(data, this.schema);

    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });
        if (this.state.upload_data) {
          const response1 = await manageProducts.uploadFeaturedImage(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            this.setState({ file_url: filepath });
            this.setState({ upload_data: "" });
            this.updateProductData();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }
        } else {
          
          this.updateProductData();
        }
      }
    }
  };
  /* add Integrator and Distributor Resources*/
  updateProductData = async () => {
    const product_data = {
      category_id: this.state.data.category_id,
      product_id: this.state.data.product_id,
      resource_category: this.state.data.resource_category,
      file_url: this.state.productfileArray,
      filename: this.state.data.filename,
      version: this.state.data.version,
      resource_type: this.state.data.resource_type
    };
    const response = await manageIntegrator.createIntegrator(product_data);
    if (response) {
      if (response.data.status === 1) {
        this.setState({ spinner: false });
        this.setState({
          message: response.data.message,
          message_type: "success"
        });
        this.setState({ submit_status: false });
        setTimeout(() => { 
          window.location.reload();
        }, 2000);
      } else {
        this.setState({ spinner: false });
        this.setState({ submit_status: false });
        this.setState({
          message: response.data.message,
          message_type: "error"
        });
      }
    }
  };

  /* Select Resource Type*/
  handleResourceType = async e => {
    const data = { ...this.state.data };
    data["resource_type"] = e.target.value;
    this.setState({ data: data });
  };

  /* Select Resource Category*/
  handleResourceCategory = async e => {
    const data = { ...this.state.data };
    data["resource_category"] = e.target.value;
    this.setState({ data: data });
  };

  render() {
    const images = require.context("../../assets/images/admin", true);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>

            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader props={this.props} />
                </div>
                
                <div className="col-md-12  contents  main_admin_page_common  useradd-new integrator pt-2 pb-4 pr-4">
                  <React.Fragment>
                    <div className="categories-wrap p-4">
                      <BlockUi tag="div" blocking={this.state.spinner} >
                      <div className="row">
                        <div className="col-md-6">
                          <div className="card">
                            <div className="card-header">
                              <strong>Add Integrator Resources</strong>
                            </div>
                            <div className="card-body categories-add shipping-add">
                              <div className="form-group">
                                <label>Categories *</label>
                                <select
                                  name="category_id"
                                  value={this.state.data.category_id}
                                  className="form-control"
                                  onChange={this.handleParentCatChange}
                                >
                                  <option value="0">None</option>
                                  {this.state.parentcategories.map(
                                    (cat_parent, idx) => (
                                      <option
                                        key={cat_parent._id}
                                        value={cat_parent._id}
                                      >
                                        {cat_parent.category_name}
                                      </option>
                                    )
                                  )}
                                </select>

                                {this.state.errors.category_id ? (
                                  <div className="error text-danger">
                                    {this.state.errors.category_id}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="form-group">
                                <label>Products *</label>
                                <select
                                  className="form-control"
                                  name="product_id"
                                  onChange={this.handleChange}
                                >
                                  <option value="0">None</option>
                                  {this.state.productcategories.map(
                                    (cat_product, idx) => (
                                      <option
                                        key={cat_product._id}
                                        value={cat_product._id}
                                      >
                                        {cat_product.product_name}
                                      </option>
                                    )
                                  )}
                                </select>
                                {this.state.errors.product_id ? (
                                  <div className="error text-danger">
                                    {this.state.errors.product_id}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="form-group">
                                <label>Resource Category *</label>
                                <select
                                  className="form-control"
                                  name="resource_category"
                                  onChange={this.handleResourceCategory}
                                >
                                  <option value="0">None</option>
                                  <option value="image">Image</option>
                                  <option value="software">Software</option>
                                  <option value="document">Document</option>
                                </select>
                                {this.state.errors.resource_category ? (
                                  <div className="error text-danger">
                                    {this.state.errors.resource_category}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="gallery-single">
                                <div className="form-group">
                                  <label>File</label>
                                  <input
                                    type="file"
                                    className="form-control"
                                    onChange={this.uploadMultipleFiles}
                                  />
                                  {this.state.upload_status === true ? (
                                    <ReactSpinner
                                      type="border"
                                      color="dark"
                                      size="1"
                                    />
                                  ) : (
                                    ""
                                  )}
                                </div>
                              </div>
                              <div className="form-group">
                                <label>File Name *</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="filename"
                                  onChange={this.handleChange}
                                ></input>

                                {this.state.errors.filename ? (
                                  <div className="error text-danger">
                                    {this.state.errors.filename}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="form-group">
                                <label>Version *</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="version"
                                  onChange={this.handleChange}
                                ></input>
                                {this.state.errors.version ? (
                                  <div className="error text-danger">
                                    {this.state.errors.version}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="form-group">
                                <label>Resource Type *</label>
                                <select
                                  className="form-control"
                                  name="resource_type"
                                  onChange={this.handleResourceType}
                                >
                                  <option value="0">None</option>
                                  <option value="integrator">Integrator</option>
                                  <option value="distributor">
                                    Distributor
                                  </option>
                                </select>
                                {this.state.errors.resource_type ? (
                                  <div className="error text-danger">
                                    {this.state.errors.resource_type}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <button
                                disabled={
                                  this.state.submit_status === true
                                    ? "disabled"
                                    : false
                                }
                                onClick={() => {
                                  this.handleSubmit();
                                }}
                                className="save-btn btn btn-dark"
                              >
                                {this.state.submit_status === true ? (
                                  <React.Fragment>
                                    <ReactSpinner
                                      type="border"
                                      color="primary"
                                      size="1"
                                    />
                                  </React.Fragment>
                                ) : (
                                  ""
                                )}
                                Save
                              </button>
                              {this.state.message !== "" ? (
                                <p className="tableinformation">
                                  <div className={this.state.message_type}>
                                    <p>{this.state.message}</p>
                                  </div>
                                </p>
                              ) : null}
                            </div>
                          </div>
                        </div>
                      </div>
                      </BlockUi>
                    </div>
                  </React.Fragment>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default IntegratorResources;
