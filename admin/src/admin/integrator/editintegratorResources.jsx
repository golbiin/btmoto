import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import { EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { Link } from "react-router-dom";
import { convertToRaw } from "draft-js";
import ReactSpinner from "react-bootstrap-spinner";
import validate from "react-joi-validation";
import Joi, { join } from "joi-browser";
import Uploadprofile from "../common/uploadProfileimage";
import * as settingsService from "../../ApiServices/admin/settings";
import * as manageUser from "../../ApiServices/admin/manageUser";
import * as manageIntegrator from "../../ApiServices/admin/integrator";
import * as manageProducts from "../../ApiServices/admin/products";
import BlockUi from 'react-block-ui';
class EditIntegratorResources extends Component {
  state = {
    parentcategories: [],
    productcategories: [],
    profile_image: "",

    ResourceCategory: [
      { name: "Imange", id: "image" },
      { name: "Software", id: "software" },
      { name: "Document", id: "document" }
    ],

    ResourceType: [
      { name: "Integrator", id: "integrator" },
      { name: "Distributor", id: "distributor" }
    ],

    cimonUser: [],
    validated: false,
    userStatus: true,
    submitStatus: false,
    errors: {},
    upload_data: "",
    value: "",
    spinner: false
  };
  /*Joi validation schema*/

  UserSchema = {
    product_id: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    category_id: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),
    filename: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),

    version: Joi.string()
      .required()
      .error(() => {
        return {
          message: "This field is Required"
        };
      }),

    profile_image: Joi.allow(null),
    resource_type: Joi.string(),
    resource_category: Joi.string(),
    _id: Joi.optional().label("id")
  };

  handleChange = (event, type = null) => {
    let cimonUser = { ...this.state.cimonUser };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateUserdata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    cimonUser[name] = value;
    this.setState({ cimonUser, errors });
  };
  validateUserdata = (name, value) => {
    const obj = { [name]: value };
    const UserSchema = { [name]: this.UserSchema[name] };
    const { error } = Joi.validate(obj, UserSchema);
    return error ? error.details[0].message : null;
  };

  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };

  /* upload multiple file upload */

  uploadMultipleFiles = async e => {
    this.setState({ upload_status: true });
    const response1 = await manageProducts.uploadProductImages(e.target.files);
    if (response1.data.status == 1) {
      response1.data.data.file_location.map((item, key) => {
        this.setState({ productfileArray: item.Location });
        let cimonUser = { ...this.state.cimonUser };
        cimonUser["file_url"] = item.Location;
        this.setState({ cimonUser: cimonUser });
      });
      this.setState({ upload_status: false });
    } else {
      this.setState({
        submitStatus: false,
        message: response1.data.message,
        responsetype: "error"
      });
      this.setState({ upload_status: false });
    }
  };

  handleRemoveProductImg(i) {
    const filteredvalue = this.state.productfileArray.filter(
      (s, sidx) => i !== sidx
    );
    if (filteredvalue) {
      this.setState({
        productfileArray: filteredvalue
      });
    }
  }
  /*Get single integrator,distributor resources */
  componentDidMount = async () => {
    this.setState({spinner:true});
    this.getAllCategory();
    const id = this.props.match.params.id;
    const response = await manageIntegrator.getSingleIntegrator(id);
    if (response.data.status == 1) {
      let newUserarray = {
        _id: response.data.data.user_data._id,
        category_id: response.data.data.user_data.category_id,
        product_id: response.data.data.user_data.product_id,
        resource_category: response.data.data.user_data.resource_category,
        filename: response.data.data.user_data.filename,
        file_url: response.data.data.user_data.file_url,
        resource_type: response.data.data.user_data.resource_type,
        version: response.data.data.user_data.version
      };

      this.setState({ cimonUser: newUserarray });
      this.getAllProductsCat(response.data.data.user_data.category_id);
      this.setState({spinner:false});
    } else {
      this.setState({
        userStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
      this.setState({spinner:false});
    }
  };

  handleParentCatChange = async e => {
    const data = { ...this.state.data };
    let cat_id = e.target.value;
    data["category_id"] = cat_id;
    this.setState({ data: data }, () => {
      this.getAllProductsCat(cat_id);
    });
  };

  handleProductChange = async e => {
    const cimonUser = { ...this.state.cimonUser };
    cimonUser["product_id"] = e.target.value;
    this.setState({ cimonUser: cimonUser });
  };

  /* Select all category */
  getAllCategory = async () => {
    const response = await manageProducts.getAllCategory();
    if (response.data.status == 1) {
      this.setState({ parentcategories: response.data.data });
    }
  };

  /* Select all products  */
  getAllProductsCat = async category_id => {
    this.setState({ spinner: true });
    try {
      const response = await manageProducts.getAllProductsCat(category_id);
      if (response.data.status == 1) {
        this.setState({
          productcategories: response.data.data
        });
      }
      this.setState({ spinner: false });
    } catch (err) {
      this.setState({ spinner: false });
    }
  };

  /* Form Submit */
  updateResource = async () => {
    const cimonUser = { ...this.state.cimonUser };
    const errors = { ...this.state.errors };
    let result = Joi.validate(cimonUser, this.UserSchema);
    if (result.error) {
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({
        errors: errors
      });
    } else {
      if (errors.length > 0) {
        this.setState({
          errors: errors
        });
      } else {
        this.setState({ submit_status: true });
        this.setState({ spinner: true });
        if (this.state.upload_data) {
          // check for featured images
          const response1 = await manageProducts.uploadFeaturedImage(
            this.state.upload_data
          );
          if (response1.data.status == 1) {
            let filepath = response1.data.data.file_location;
            this.setState({ file_url: filepath });

            this.setState({ upload_data: "" });
            this.updateResource();
          } else {
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
            this.setState({ spinner: false });
          }
        } else {
          
          this.updateResource();
        }
      }
    }
  };
  /* Update Integrator and Distributor Resources */
  updateResource = async () => {
    let cimonUser = { ...this.state.cimonUser };
    const response = await manageIntegrator.updateResource(cimonUser);

    if (response.data.status == 1) {
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "success"
      });
      setTimeout(() => { 
        window.location.reload();
      }, 2000);
    } else {
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
    this.setState({ spinner: true });
  };

  render() {
    const images = require.context("../../assets/images/admin", true);
    let optionResourceCategory = this.state.ResourceCategory.map(v => (
      <option
        key={v.id}
        value={v.id}
        selected={this.state.cimonUser.resource_category == v.id ? v.id : ""}
      >
        {v.name}
      </option>
    ));
    let optionResourceType = this.state.ResourceType.map(v => (
      <option
        key={v.id}
        value={v.id}
        selected={this.state.cimonUser.resource_type == v.id ? v.id : ""}
      >
        {v.name}
      </option>
    ));
    let categoryDetails = this.state.parentcategories.map(cat_parent => (
      <option
        key={cat_parent._id}
        value={cat_parent._id}
        selected={
          this.state.cimonUser.category_id == cat_parent._id
            ? cat_parent._id
            : ""
        }
      >
        {cat_parent.category_name}
      </option>
    ));
    let productDetails = this.state.productcategories.map(v => (
      <option
        key={v._id}
        value={v._id}
        selected={this.state.cimonUser.product_id == v._id ? v._id : ""}
      >
        {v.product_name}
      </option>
    ));

    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader props={this.props} />
                </div>
                <div className="col-md-12  contents  main_admin_page_common  useradd-new integrator pt-2 pb-4 pr-4">
                  <React.Fragment>
                    <div className="categories-wrap p-4">
                      <BlockUi tag="div" blocking={this.state.spinner} >
                      <div className="row">
                        <div className="col-md-6">
                          <div className="card">
                            <div className="card-header">
                              <strong>Edit Integrator Resources</strong>
                            </div>
                            <div className="card-body categories-add shipping-add">
                              <div className="form-group">
                                <label>Categories</label>
                                <select
                                  name="category_id"
                                  className="form-control"
                                  onChange={e => this.handleParentCatChange(e)}
                                >
                                  <option value="">Select Category</option>
                                  {categoryDetails}
                                </select>
                              </div>
                              <div className="form-group">
                                <label>Products</label>
                                <select
                                  className="form-control"
                                  name="product_id"
                                  category_id={this.state.cimonUser.category_id}
                                  value={this.state.cimonUser.product_id}
                                  onChange={e => this.handleProductChange(e)}
                                >
                                  <option value="">Select Products</option>
                                  {productDetails}
                                </select>
                              </div>
                              <div className="form-group">
                                <label>Resource Category</label>
                                <select
                                  className="form-control"
                                  name="resource_category"
                                  onChange={e => this.handleChange(e)}
                                >
                                  <option value="">
                                    Select Resource Category
                                  </option>
                                  {optionResourceCategory}
                                </select>
                              </div>
                              <div className="gallery-single">
                                <div className="form-group">
                                  <label>File</label>
                                  <input
                                    type="file"
                                    className="form-control"
                                    onChange={this.uploadMultipleFiles}
                                  />
                                  {this.state.upload_status === true ? (
                                    <ReactSpinner
                                      type="border"
                                      color="dark"
                                      size="1"
                                    />
                                  ) : (
                                    ""
                                  )}
                                </div>
                                {this.state.cimonUser.resource_category ==
                                "image" ? (
                                  <div className="multi-preview">
                                    <div className="single-img">
                                      <img
                                        src={this.state.cimonUser.file_url}
                                        alt="..."
                                        width="80px"
                                        height="70px"
                                      />
                                    </div>
                                  </div>
                                ) : (
                                  <a href={this.state.cimonUser.file_url}>
                                    {this.state.cimonUser.file_url}
                                  </a>
                                )}
                              </div>
                              <div className="form-group">
                                <label>File Name</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="filename"
                                  value={this.state.cimonUser.filename}
                                  onChange={e => this.handleChange(e)}
                                ></input>
                              </div>
                              <div className="form-group">
                                <label>Version</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="version"
                                  value={this.state.cimonUser.version}
                                  onChange={e => this.handleChange(e)}
                                ></input>
                              </div>
                              <div className="form-group">
                                <label>Resource Type</label>
                                <select
                                  className="form-control"
                                  name="resource_type"
                                  onChange={e => this.handleChange(e)}
                                >
                                  <option value="">Select Resource Type</option>
                                  {optionResourceType}
                                </select>
                              </div>
                              <div className="submit">
                                <button
                                  className="submit-btn"
                                  onClick={this.updateResource}
                                >
                                  Update
                                </button>
                                {this.state.message !== "" ? (
                                  <div className="form-group tableinformation">
                                    <div className={this.state.responsetype}>
                                      <p>{this.state.message}</p>
                                    </div>
                                  </div>
                                ) : null}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      </BlockUi>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default EditIntegratorResources;
