import { apiUrl } from "../config.json";
const axios = require("axios").default;

/* Get Footer */
export function getFooter() {
   return axios.post(apiUrl + "/footerColumns/footer");
}

/* Get Footer Columns */
export function getAllFooterclms() {
   return axios.post(apiUrl + "/footerColumns/getAllFooterclms");
 
}

/* Get Socket Columns */
export function getAllSocketclms() {
   return axios.post(apiUrl + "/footerColumns/getAllSocketclms");

}