import { apiUrl } from "../config.json";
const axios = require("axios").default;

export function checkout(id, shippingDetails, cartProducts, cartTotal, cartId, couponDetails, token, authUser) {
  return axios.post(apiUrl + "/checkout/payment", {
    id:id,
    shippingDetails: shippingDetails,
    products: cartProducts,
    total: cartTotal,
    cartId: cartId,
    couponDetails,
    token,
    authUser
  });
}

export function getOrder(id) {
  return axios.post(apiUrl + "/checkout/getOrder", {
    id:id,
    token: localStorage.getItem('user_token')
  });
}

export function upsValidateAddress(shipping, cartProducts, cartTotal) {
  return axios.post(apiUrl + "/checkout/upsValidateAddress", {shipping, cartProducts, cartTotal});
}


export function applyCoupon(coupon_code, cartId, token) {
  return axios.post(apiUrl + "/cart/applyCoupon", {
    cartId: cartId,
    coupon_code: coupon_code,
    token: token
  });
}

export function getAvailableShippingMethod() {
  return axios.post(apiUrl + "/cart/getAvailableShippingMethod", {
  });
}