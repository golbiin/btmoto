import { apiUrl } from "../config.json";
const axios = require("axios").default;

/* List all Jobs */
export function getJobs() {
  return axios.post(apiUrl + "/careers/getAlljobs");
}

 /* Get Job Description */
export function getsinglejobs(slug) {
  return axios.post(apiUrl + "/careers/getsinglejobs", {
    slug: slug,
  });
}

/* Search Jobs */
export function searchJobs(data) {
  return axios.post(apiUrl + "/careers/searchJobs", {
    data: data,
 
  });
}
