import { apiUrl } from "../config.json";
const axios = require("axios").default;
export function getStoreDetailsBySlug(slug) {
    return axios.post(apiUrl + "/storeLocators/getStoreDetailsBySlug", {
        slug: slug,
    });
}
export function searchStoreLocation(slug) {
    return axios.post(apiUrl + "/storeLocators/searchStoreLocation", {
        slug: slug,
    });
}
