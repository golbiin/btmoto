import { apiUrl } from "../config.json";
const tokenKey = "user_token";
const axios = require("axios").default;
/* Get all products based on category */
export function getAllProducts(categories) {
  return axios.post(apiUrl + "/products/getAllProducts", {
    categories: categories
  });
}

/*Get recommended products */
export function getAllProductsCategories() {
  return axios.post(apiUrl + "/products/getAllProductsCategories", {});
}

/*Get single product details */
export function getSingleproduct(slug) {
  return axios.post(apiUrl + "/products/getSingleproduct", {
    slug: slug
  });
}

export function getRelatedproducts(slug) {
  return axios.post(apiUrl + "/products/getRelatedproducts", {
    slug: slug
  });
}

export function getAllScadaproducts(slug) {
  return axios.post(apiUrl + "/products/getAllScadaproducts", {
    slug: slug
  });
}

export function checkAddToCart(product, quantity) {
  return axios.post(apiUrl + "/products/checkAddToCart", {
    product: product,
    quantity: quantity
  });
}

export function checkAddToCartMultiple(products) {
  return axios.post(apiUrl + "/products/checkAddToCartMultiple", {
    products: products
  });
}

export function getCartProducts(id, token = "", coupon = "") {
  return axios.post(apiUrl + "/cart/getCartItems", {
    id: id,
    coupon_code: coupon,
    token: token
  });
}

/* Get all products for intergrator distributor resources*/
export function getAllProductsIntegrator() {
  return axios.post(apiUrl + "/products/getAllProductsIntegrator");
}

/*Get all categories for integrator,distributor resources*/
export function getAllCategoryIntegrator() {
  return axios.post(apiUrl + "/products/getAllCategoryIntegrator");
}

export function getAllIntegrator(category) {
  return axios.post(apiUrl + "/products/getAllIntegrator", {
    token: localStorage.getItem(tokenKey),
    category: category
  });
}

export function searchFilename(data) {
  return axios.post(apiUrl + "/product/searchFilename", {
    data: data
  });
}
