import { apiUrl } from "../config.json";
const axios = require("axios").default;

/*Get Page Contents */
export function getPageContent(slug) { 
  return axios.post(apiUrl + "/pages/getPageContent", {
    slug: slug,
  });

}


export function getPreviewContent(slug) { 
  return axios.post(apiUrl + "/pages/getPreviewContent", {
    slug: slug,
  });

}
