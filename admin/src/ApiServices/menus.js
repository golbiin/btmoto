import { apiUrl } from "../config.json";
const axios = require("axios").default;


/* Get Header Menus */
export function getHeaderMenus() {
  return axios.post(apiUrl + "/menus/getHeaderMenus");
}
