import { apiUrl } from "../config.json";
const axios = require("axios").default;
const tokenKey = "user_token";


export function getAllOrders(data) {
    return axios.post(apiUrl + "/admin/orders/getAllOrders", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  export function getUserOrders() {
    return axios.post(apiUrl + "/order/getUserOrders", {
      token: localStorage.getItem(tokenKey),
    });
  }
  
  export function cancelOrders(order_id) {
    return axios.post(apiUrl + "/order/cancelOrder", {
      order_id: order_id,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  
  export function movetoPublish(order_id) {
    return axios.post(apiUrl + "/admin/orders/movetoPublish", {
      order_id: order_id,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  
  
  export function deleteOrder(order_id) {
    return axios.post(apiUrl + "/admin/orders/deleteOrder", {
      order_id: order_id,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  export function getCurrentOders(data) {
    return axios.post(apiUrl + "/admin/orders/getCurrentOders", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  
  export function getCurrentOdersnotes(data) {
    return axios.post(apiUrl + "/admin/orders/getCurrentOdersnotes", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  
  export function generateInvoice(data) {
    return axios.post(apiUrl + "/admin/orders/generateInvoice", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  
  
  export function updateOrder(order_id,data) {
    return axios.post(apiUrl + "/admin/orders/updateOrder", {
      data: data,
      _id: order_id,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  
  export function actionHandlerOrder(orderids,action,status) {
    return axios.post(apiUrl + "/admin/orders/actionHandlerOrder", {
      orderids: orderids,
      action : action,
      status : status,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  export function actionTrashHandlerOrder(orderids,action,status) {
    return axios.post(apiUrl + "/admin/orders/actionTrashHandlerOrder", {
      orderids: orderids,
      action : action,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  export function refundOrder(data) {
    return axios.post(apiUrl + "/admin/orders/refundOrder", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  export function getRefundArray(data) {
    return axios.post(apiUrl + "/admin/orders/getRefundArray", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  
  export function shippingConfirm(data) {
  
    return axios.post(apiUrl + "/admin/orders/shippingConfirm", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
  }
  
  