import { apiUrl } from "../config.json";
const axios = require("axios").default;


/* Forgot Password */
export function forgotPassword(data) {
  return axios.post(apiUrl + "/login/forgotPassword", {
    data: data
  });
}
/* Reset Password */
export function resetPassword(data, user_id) {
  return axios.post(apiUrl + "/login/resetPassword", {
    data: data,
    user_id: user_id
  });
}

/* Forgot Recovery Email */
export function forgotRecoveryEmail(data) {
  return axios.post(apiUrl + "/login/forgotRecoveryEmail", {
    data: data
  });
}

/*Get all languages */
export function getAlllanguage() {
  return axios.post(apiUrl + "/login/getAlllanguage");
}