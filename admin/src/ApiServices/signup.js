import { apiUrl } from "../config.json";
import getCurrentUser from "./authenticate";
const axios = require("axios").default;

/* user signup */
export function signupUser(users) {
  return axios.post(apiUrl + "/user/signup", {
    account_type: users.accountType,
    first_name: users.firstName,
    last_name: users.lastName,
    email: users.email,
    password: users.password,
  });
}

/*create profile */
export function createProfile(data) {
  let token = localStorage.getItem("userToken");
  return axios.post(apiUrl + "/user/createprofile", {
    token: token,
    profileData: data,
  });
}
