import { apiUrl } from "../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

/*Get all Contact */
export function getcontact() {
  return axios.post(apiUrl + "/contact/getAllcontact");
}
/* Create Contact Details */
export function createContact(data) {
  return axios.post(apiUrl + "/contact/createContact", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}