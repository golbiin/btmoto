import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function createTraining(data) {
  return axios.post(apiUrl + "/admin/videotraining/createVideoTraining", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

/*  Get all types of Training  */
export function getAllTraining(filter) {
  return axios.post(apiUrl + "/admin/videotraining/getAllVideoTraining", {
    filter: filter,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateTraining(data) {
  return axios.post(apiUrl + "/admin/videotraining/updateVideoTraining", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getDateFilterOptions() {
  return axios.post(apiUrl + "/admin/videotraining/getDateFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}
export function getTypeFilterOptions() {
  return axios.post(apiUrl + "/admin/videotraining/getTypeFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteTraining(id) {
  return axios.post(apiUrl + "/admin/videotraining/deleteVideoTraining", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function trashTraining(id) {
  return axios.post(apiUrl + "/admin/videotraining/trashVideoTraining", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function getSingleTraining(id) {
  return axios.post(apiUrl + "/admin/videotraining/getSingleVideoTraining", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function actionHandlerTraining(ids, action, status) {
  return axios.post(apiUrl + "/admin/videotraining/actionHandlerTraining", {
    ids: ids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}
