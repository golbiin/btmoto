import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function getAllIpRedirections() {
  return axios.post(apiUrl + "/admin/ipredirection/getAllRedirection", {
    token: localStorage.getItem(tokenKey),
  });
}

export function createIpRedirection(data) {
  
  return axios.post(apiUrl + "/admin/ipredirection/createRedirection", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 

export function getIpRedirectionById(data) {
  return axios.post(apiUrl + "/admin/ipredirection/getRedirectionById", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
export function removeIpRedirection(data) {
  return axios.post(apiUrl + "/admin/ipredirection/removeRedirection", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}



export function updateIpRedirection(data) {
  return axios.post(apiUrl + "/admin/ipredirection/updateRedirection", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

export function updateIpRedirectionStatus(id,status) {
  return axios.post(apiUrl + "/admin/ipredirection/updateRedirectionStatus", {
    _id: id,
    status: status,
    token: localStorage.getItem(tokenKey),
  });
}


export function getAllpages() {
  return axios.post(apiUrl + "/admin/ipredirection/getAllpages", {
    token: localStorage.getItem(tokenKey),
  });
}




 