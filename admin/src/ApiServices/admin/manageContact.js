import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function getAllContacts() {
  return axios.post(apiUrl + "/admin/contacts/getAllAddress", {
    token: localStorage.getItem(tokenKey),
  });
}

export function createContact(data) {
  
  return axios.post(apiUrl + "/admin/contacts/createContact", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

export function removeContact(data) {
  return axios.post(apiUrl + "/admin/contacts/removeContact", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

export function getContactById(data) {
  return axios.post(apiUrl + "/admin/contacts/getContactById", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

export function updateContact(data) {
  return axios.post(apiUrl + "/admin/contacts/updateContact", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

export function getAllSubcriptions() {
  return axios.post(apiUrl + "/admin/contacts/getAllSubcriptions", {
    token: localStorage.getItem(tokenKey),
  });
}

export function deleteSubcription(id) {
  return axios.post(apiUrl + "/admin/contacts/removeSubscription", {
    id: id,
    token: localStorage.getItem(tokenKey),
  });
}
