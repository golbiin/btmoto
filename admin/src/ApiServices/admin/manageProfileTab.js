import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function createProfileTab(data) {
  return axios.post(apiUrl + "/admin/tab/createProfileTab", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllTabs() {
  return axios.post(apiUrl + "/admin/tab/getAllTabs", {
    token: localStorage.getItem(tokenKey)
  });
}

export function updateProfileTab(data) {
  return axios.post(apiUrl + "/admin/tab/updateProfileTab", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteProfileTab(data) {
  return axios.post(apiUrl + "/admin/tab/deleteProfileTab", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
