import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function createCForms(data) {
  return axios.post(apiUrl + "/admin/cforms/create", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllCForms(filter) {
  return axios.post(apiUrl + "/admin/cforms/list", {
    filter,
    token: localStorage.getItem(tokenKey)
  });
}

export function getCFormsDateFilterOptions() {
  return axios.post(apiUrl + "/admin/cforms/listByDate", {
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteCForms(id) {
  return axios.post(apiUrl + "/admin/cforms/delete", {
    form_id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function trashCForms(id) {
  return axios.post(apiUrl + "/admin/cforms/trash", {
    form_id: id,
    token: localStorage.getItem(tokenKey)
  });
}
export function actionHandlerCForms(ids, action, status) {
  return axios.post(apiUrl + "/admin/cforms/actionHandler", {
    formids: ids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}
export function getSingleCForm(id) {
  return axios.post(apiUrl + "/admin/cforms/fetchDetails", {
    form_id: id,
    token: localStorage.getItem(tokenKey)
  });
}
export function editCForms(data) {
  return axios.post(apiUrl + "/admin/cforms/edit", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
