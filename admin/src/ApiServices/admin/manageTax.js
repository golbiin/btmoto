import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function getAllTax() {
  return axios.post(apiUrl + "/admin/tax/getAllTax", {
    token: localStorage.getItem(tokenKey),
  });
}




export function getTaxById(data) {
  return axios.post(apiUrl + "/admin/tax/getTaxById", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}


export function removeTax(data) {
  return axios.post(apiUrl + "/admin/tax/removeTax", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}


export function updateTax(data) {
  return axios.post(apiUrl + "/admin/tax/updateTax", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}


export function createTax(data) {
  return axios.post(apiUrl + "/admin/tax/createTax", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
