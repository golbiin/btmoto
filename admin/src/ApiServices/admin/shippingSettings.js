import {apiUrl} from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function addShippingData(data) {
  return axios.post(apiUrl + "/admin/shipping_settings/addShippingData", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

export function getShippingData() {
  return axios.post(apiUrl + "/admin/shipping_settings/getShippingData", {
    token: localStorage.getItem(tokenKey),
  });
}

export function updateShippingData(data) {
  return axios.post(apiUrl + "/admin/shipping_settings/updateShippingData", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
  export function addShippingMethod(data) {
    return axios.post(apiUrl + "/admin/shipping_settings/addShippingMethod", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });

  }

  export function updateShippingMethod(data) {
    return axios.post(apiUrl + "/admin/shipping_settings/updateShippingMethod", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });

  }
  export function getShippingMethod() {
    return axios.post(apiUrl + "/admin/shipping_settings/getShippingMethod", {
      token: localStorage.getItem(tokenKey),
    });
  }