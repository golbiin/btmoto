import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function createIntegrator(data) {
    
    return axios.post(apiUrl + "/admin/integrator/createIntegrator", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
   }

   export function getAllIntegratorResources() {
    return axios.post(apiUrl + "/admin/integrator/getAllIntegratorResources", {
      token: localStorage.getItem(tokenKey),
    });
   }
   export function deleteIntegrator(id) {
    return axios.post(apiUrl + "/admin/integrator/deleteIntegrator", {
      id: id,
      token: localStorage.getItem(tokenKey),
    });
  }

  export function getSingleIntegrator(user_id) {
    return axios.post(apiUrl + "/admin/integrator/getSingleIntegrator", {
      user_id: user_id,
      token: localStorage.getItem(tokenKey),
    });
  }

  export function updateResourcedata(cimonUser) {
    
    return axios.post(apiUrl + "/admin/integrator/updateResourcedata", {
      data: cimonUser,
      token: localStorage.getItem(tokenKey),
    });
  }

  export function updateResource(cimonUser) {
    
    return axios.post(apiUrl + "/admin/integrator/updateResource", {
      data: cimonUser,
      token: localStorage.getItem(tokenKey),
    });
  }
