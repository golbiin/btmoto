import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

/********************************************************** */


/*     Create Attributes  */
export function createAttributes(data) {
  return axios.post(apiUrl + "/admin/attribute/createAttributes", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}


/*  Get All Attributes  */
export function getAllAttributes() {
  return axios.post(apiUrl + "/admin/attribute/getAllAttributes", {
    token: localStorage.getItem(tokenKey)
  });
}



/*  Get all Parent Attributes */
export function getAllParentAttributes(data) {
  return axios.post(apiUrl + "/admin/attribute/getAllParentAttributes", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}


/* Remove Attributes */
export function removeAttributes(data) {
  return axios.post(apiUrl + "/admin/attribute/removeAttributes", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}



/* Remove getAttributeBySlug */
export function getAttributeBySlug(data) {
  return axios.post(apiUrl + "/admin/attribute/getAttributeBySlug", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}


/* update Attribute */

export function updateAttribute(data) {
  return axios.post(apiUrl + "/admin/attribute/updateAttribute", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}




/* get all Categories */

export function getAllCategory() {
  return axios.post(apiUrl + "/admin/attribute/getAllCategory", {
    token: localStorage.getItem(tokenKey)
  });
}



/********************************************************** */
