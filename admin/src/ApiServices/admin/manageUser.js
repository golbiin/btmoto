import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function getSingleProfile(user_id) {
  return axios.post(apiUrl + "/admin/manage/users/getSingleProfile", {
    user_id: user_id,
    token: localStorage.getItem(tokenKey),
  });
}

export function updateProfiledata(cimonAdmin) {
  
  return axios.post(apiUrl + "/admin/manage/users/updateProfile", {
    data: cimonAdmin,
    token: localStorage.getItem(tokenKey),
  });
}




export function insertAdmindata(users) {
  return axios.post(apiUrl + "/admin/manage/users/insertAdmin", {
     userName: users.userName,
     firstName: users.firstName,
     lastName: users.lastName,
     email: users.email,
     password: users.password,
     profile_image: users.profile_image,
   });
 }

export function getAlladmins(data) {
  return axios.post(apiUrl + "/admin/manage/users/getAlladmins", {
    token: localStorage.getItem(tokenKey),
  });
}

export function getSingleAdmin(user_id) {
  return axios.post(apiUrl + "/admin/manage/users/getSingleAdmin", {
    user_id: user_id,
    token: localStorage.getItem(tokenKey),
  });
}

export function updateAdmindata(cimonUser) {
  
  return axios.post(apiUrl + "/admin/manage/users/updateAdmin", {
    data: cimonUser,
    token: localStorage.getItem(tokenKey),
  });
}

export function deleteAdmin(user_id) {
  return axios.post(apiUrl + "/admin/manage/users/deleteAdmin", {
    user_id: user_id,
    token: localStorage.getItem(tokenKey),
  });
}

export function getAllusers(data) {
  return axios.post(apiUrl + "/admin/manage/users/getAllUser", {
    token: localStorage.getItem(tokenKey),
  });
}
export function changeStatus(id, status) {
  return axios.post(apiUrl + "/admin/manage/users/changeStatus", {
    user_id: id,
    status: status,
    token: localStorage.getItem(tokenKey),
  });
}
export function changeVerifyStatus(id, status) {
  return axios.post(apiUrl + "/admin/manage/users/changeVerifyStatus", {
    user_id: id,
    status: status,
    token: localStorage.getItem(tokenKey),
  });
}

export function deleteUser(user_id) {
  return axios.post(apiUrl + "/admin/manage/users/deleteUser", {
    user_id: user_id,
    token: localStorage.getItem(tokenKey),
  });
}
export function getCount(type) {
  return axios.post(apiUrl + "/admin/manage/users/getCount", {
    type: type,
    token: localStorage.getItem(tokenKey),
  });
}

export function getSingleUser(user_id) {
  return axios.post(apiUrl + "/admin/manage/users/getSingleUser", {
    user_id: user_id,
    token: localStorage.getItem(tokenKey),
  });
}

export function updateUserdata(cimonUser) {
  
  return axios.post(apiUrl + "/admin/manage/users/updateUser", {
    data: cimonUser,
    token: localStorage.getItem(tokenKey),
  });
}

export function insertUserdata(users) {
  console.log('------>',users);
 return axios.post(apiUrl + "/admin/manage/users/insertUser", {
    userName: users.userName,
    firstName: users.firstName,
    lastName: users.lastName,
    email: users.email,
    password: users.password,
    profile_image: users.profile_image,
     usertype: users.usertype,
     company_name: users.company_name,
     country :users.country,
     city: users.city,
     zipcode: users.zipcode,
     phone_number: users.phone_number,
     recovery_email: users.recovery_email,
     drivelink : users.drivelink,
     bussiness_type : users.bussiness_type,
     industry : users.industry,
     agree_newsletter : users.agree_newsletter,
     interested_products : users.interested_products
  });
}

export function uploadProfile(upload_data) {
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  upload.append("file", upload_data);
  return axios.post(apiUrl + "/admin/upload/uploadData/" + token, upload, {});
}

export function getDashboardDetails() {
  return axios.post(apiUrl + "/admin/manage/users/getDashboardDetails", {
    token: localStorage.getItem(tokenKey),
  });
}


export function getDungraphDetails() {
  return axios.post(apiUrl + "/admin/manage/users/getDungraphDetails", {
    token: localStorage.getItem(tokenKey),
  });
}

export function updateUserfilepermission(userfilepermission) {
  
  return axios.post(apiUrl + "/admin/manage/users/updateFilePermission", {
    data: userfilepermission,
    token: localStorage.getItem(tokenKey),
  });
}

export function getFilepermissionById(userfilepermission) {
  
  return axios.post(apiUrl + "/admin/manage/users/getFilepermissionById", {
    data: userfilepermission,
    token: localStorage.getItem(tokenKey),
  });
}

export function exportcsv(start ,end , role) {
  
  return axios.post(apiUrl + "/admin/manage/users/exportcsv", {
    start: start,
    end: end,
    role: role,
    token: localStorage.getItem(tokenKey),
  });
}



export function actionHandlerUser(userids,action,status) {
  return axios.post(apiUrl + "/admin/manage/users/actionHandlerUser", {
       userids: userids,
        action : action,
        status : status,
        token: localStorage.getItem(tokenKey),
  });
}

