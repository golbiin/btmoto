import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function getAllCategorys() {
  return axios.post(apiUrl + "/admin/category/getAllCategory", {
    token: localStorage.getItem(tokenKey),
  });
}

export function createCategory(data) {
  
  return axios.post(apiUrl + "/admin/category/createCategory", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 

export function getCategoryById(data) {
  return axios.post(apiUrl + "/admin/category/getCategoryById", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
export function removeCategory(data) {
  return axios.post(apiUrl + "/admin/category/removeCategory", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}



export function updateCategory(data) {
  return axios.post(apiUrl + "/admin/category/updateCategory", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 
export function getAllCatsIdsandNames() {
  return axios.post(apiUrl + "/admin/settings/getAllParentCategory", {});
}
