import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function getAllStoreLocation() {
  return axios.post(apiUrl + "/admin/storelocation/getAllStoreLocation", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getStoreDetailsbyId(data) {
  return axios.post(apiUrl + "/admin/storelocation/getStoreDetailsbyId", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function createStore(data) {
  return axios.post(apiUrl + "/admin/storelocation/createStore", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateStoreLocator(data) {
  return axios.post(apiUrl + "/admin/storelocation/updateStoreLocator", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function uploadFeaturedImage(upload_data) {
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  upload.append("file", upload_data);
  return axios.post(
    apiUrl + "/admin/product-upload/uploadFeaturedImage/" + token,
    upload,
    {}
  );
}

export function deleteStore(data) {
  return axios.post(apiUrl + "/admin/storelocation/deleteStore", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function trashStore(data) {
  return axios.post(apiUrl + "/admin/storelocation/trashStore", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function actionHandlerstore(ids, action, status) {
  return axios.post(apiUrl + "/admin/storelocation/actionHandlerstore", {
    ids: ids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}
