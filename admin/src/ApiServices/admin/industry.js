import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

/********************************************************** */

export function getAllIndustry() {
  return axios.post(apiUrl + "/admin/industry/getAllIndustry", {
    token: localStorage.getItem(tokenKey)
  });
}

export function updateIndustry(data) {
  return axios.post(apiUrl + "/admin/industry/updateIndustry", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function createIndustry(data) {
  return axios.post(apiUrl + "/admin/industry/createIndustry", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteIndustry(news_id) {
  return axios.post(apiUrl + "/admin/industry/deleteIndustry", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}
export function trashIndustry(news_id) {
  return axios.post(apiUrl + "/admin/industry/trashIndustry", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function getSingleIndustry(news_id) {
  return axios.post(apiUrl + "/admin/industry/getSingleIndustry", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function actionHandlerIndustry(newsids, action, status) {
  return axios.post(apiUrl + "/admin/industry/actionHandlerIndustry", {
    newsids: newsids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}
/********************************************************** */
