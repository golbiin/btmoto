import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function getDateFilterOptions() {
  return axios.post(apiUrl + "/admin/livetraining/getDateFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}
export function getTypeFilterOptions() {
  return axios.post(apiUrl + "/admin/livetraining/getTypeFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}

/*  Get all types of Training  */
export function getAllTraining(filter) {
  return axios.post(apiUrl + "/admin/livetraining/getAllTraining", {
    filter: filter,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateTraining(data) {
  return axios.post(apiUrl + "/admin/livetraining/updateTraining", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function createTraining(data) {
  return axios.post(apiUrl + "/admin/livetraining/createTraining", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteTraining(id) {
  return axios.post(apiUrl + "/admin/livetraining/deleteTraining", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function trashTraining(id) {
  return axios.post(apiUrl + "/admin/livetraining/trashTraining", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}


export function duplicateTraining(id) {   

  return axios.post(apiUrl + "/admin/livetraining/duplicateTraining", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });

}


export function getSingleTraining(id) {
  return axios.post(apiUrl + "/admin/livetraining/getSingleTraining", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function actionHandlerTraining(newsids, action, status) {
  return axios.post(apiUrl + "/admin/livetraining/actionHandlerTraining", {
    newsids: newsids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

export function createEventLocation(data) {
  return axios.post(apiUrl + "/admin/livetraining/createEventLocation", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllEventLocation() {
  return axios.post(apiUrl + "/admin/livetraining/getAllEventLocation", {
    token: localStorage.getItem(tokenKey)
  });
}
export function getAllActiveEventLocation(status) {
  return axios.post(apiUrl + "/admin/livetraining/getAllActiveEventLocation", {
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteEventLocation(id) {
  return axios.post(apiUrl + "/admin/livetraining/deleteEventLocation", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}
export function trashEventLocation(id) {
  return axios.post(apiUrl + "/admin/livetraining/trashEventLocation", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function actionHandlerEventLocation(ids, action, status) {
  return axios.post(apiUrl + "/admin/livetraining/actionHandlerEventLocation", {
    ids: ids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

export function getSingleEventLocation(id) {
  return axios.post(apiUrl + "/admin/livetraining/getSingleEventLocation", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateEventLocation(data) {
  return axios.post(apiUrl + "/admin/livetraining/updateEventLocation", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}


/* get all Published FeedBack */
export function getAllTrainingPublished() {
  return axios.post(apiUrl + "/admin/livetraining/getAllTrainingPublished", {
    token: localStorage.getItem(tokenKey)
  });
}

/* Get all getEventTranining */


export function getEventTranining() {
  return axios.post(apiUrl + "/admin/livetraining/getEventTranining", {
    token: localStorage.getItem(tokenKey)
  });
}
/* Update Event TRaining */

export function updateEventTraining(data) {
  return axios.post(apiUrl + "/admin/livetraining/updateEventTraining", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

/* Update Event TRaining */

/* User Event Training */ 

export function getUserTraining() {
  return axios.post(apiUrl + "/admin/livetraining/getUserTraining", {
    token: localStorage.getItem(tokenKey)
  });
}
/* End User TRaiing */


/* --------------Get User Traning------------------- */
export function getUsersByTraningId(TraningId) {
  console.log('getUsersByTraningId');
  return axios.post(apiUrl + "/admin/livetraining/getUsersByTraningId", {
    data: TraningId,
    token: localStorage.getItem(tokenKey)
  });
}

/* --------------End Get User Traning------------------- */