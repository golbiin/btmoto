import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function createFaq(data) {
  return axios.post(apiUrl + "/admin/faq/createFaq", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getCategoryParent() {
  return axios.post(apiUrl + "/admin/faq/getCategoryParent", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllFaq() {
  return axios.post(apiUrl + "/admin/faq/getAllFaq", {
    token: localStorage.getItem(tokenKey)
  });
}

export function updateFaq(data) {
  return axios.post(apiUrl + "/admin/faq/updatefaq", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteFaq(faq_id) {
  return axios.post(apiUrl + "/admin/faq/deleteFaq", {
    faq_id: faq_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function trashFaq(faq_id) {
  return axios.post(apiUrl + "/admin/faq/trashFaq", {
    faq_id: faq_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function getSingleFaq(faq_id) {
  return axios.post(apiUrl + "/admin/faq/getSinglefaq", {
    faq_id: faq_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function actionHandlerCasestudy(case_id, action, status) {
  console.log(case_id);
  return axios.post(apiUrl + "/admin/faq/actionHandlerCasestudy", {
    case_id: case_id,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}
