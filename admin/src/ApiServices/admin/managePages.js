import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function deletePage(page_id) {
  return axios.post(apiUrl + "/admin/manage/pages/deletePage", {
    page_id: page_id,
    token: localStorage.getItem(tokenKey),
  });
}
export function duplicatePage(page_id) {
  return axios.post(apiUrl + "/admin/manage/pages/duplicatePage", {
    page_id: page_id,
    token: localStorage.getItem(tokenKey),
  });
}


export function getAllpages(data) {
  return axios.post(apiUrl + "/admin/manage/pages/getAllpages", {
    token: localStorage.getItem(tokenKey),
  });
}

export function insertPagedata(pages) {
  
 return axios.post(apiUrl + "/admin/manage/pages/insertPages", {
    data: pages,
    token: localStorage.getItem(tokenKey),
  });
}


export function getSinglePage(slug) {
  return axios.post(apiUrl + "/admin/manage/pages/getSinglePage", {
    slug: slug,
    token: localStorage.getItem(tokenKey),
  });
}
export function getSinglePageById(slug) {
  return axios.post(apiUrl + "/admin/manage/pages/getSinglePageById", {
    slug: slug,
    token: localStorage.getItem(tokenKey),
  });
}

export function getSinglePageByIdNew(id) {
  return axios.post(apiUrl + "/admin/manage/pages/getSinglePageByIdNew", {
    id: id,
    token: localStorage.getItem(tokenKey),
  });
} 
export function updatePagedata(cimonPages) {
  
  return axios.post(apiUrl + "/admin/manage/pages/updatePage", {
    data: cimonPages,
    token: localStorage.getItem(tokenKey),
  });
}

export function updatePagedatabyId(pageData) {
  
  return axios.post(apiUrl + "/admin/manage/pages/updatePagedatabyId", {
    data: pageData,
    token: localStorage.getItem(tokenKey),
  });
}

export function previewPagedata(cimonPages) {
  
  return axios.post(apiUrl + "/admin/manage/pages/previewPage", {
    data: cimonPages,
    token: localStorage.getItem(tokenKey),
  });
}

export function uploadPageimage(file) {
  
  const upload = new FormData();
  upload.append("file", file);
  
  return axios.post(apiUrl + "/admin/manage/pages/uploadPageImage/", {
    upload: upload,
  }); 
}


export function uploadVideo(upload_data) {
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  upload.append("file", upload_data);

  //return axios.post(apiUrl + "/admin/mediaFiles/uploadFiles/" + token, upload, {});
  return axios.post(apiUrl + "/admin/manage/pages/uploadVideo/" + token, upload, {});
}



export function actionHandlerPage(newsids,action,status) {
  return axios.post(apiUrl + "/admin/manage/pages/actionHandlerPage", {
        newsids: newsids,
        action : action,
        status : status,
        token: localStorage.getItem(tokenKey),
  });
}