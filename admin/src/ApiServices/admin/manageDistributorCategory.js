import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function getAllDistributors() {
  return axios.post(apiUrl + "/admin/distributor_category/getAllDistributor", {
    token: localStorage.getItem(tokenKey),
  });
}

export function createDistributor(data) {
  
  return axios.post(apiUrl + "/admin/distributor_category/createDistributor", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 

export function getDistributorById(data) {
  return axios.post(apiUrl + "/admin/distributor_category/getDistributorById", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
export function removeDistributor(data) {
  return axios.post(apiUrl + "/admin/distributor_category/removeDistributor", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}



export function updateDistributor(data) {
  return axios.post(apiUrl + "/admin/distributor_category/updateDistributor", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 
export function getAllCatsIdsandNames() {
  return axios.post(apiUrl + "/admin/settings/getAllParentCategory", {});
}
