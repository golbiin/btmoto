import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function getAllRedirections() {
  return axios.post(apiUrl + "/admin/redirection/getAllRedirection", {
    token: localStorage.getItem(tokenKey),
  });
}

export function createRedirection(data) {
  
  return axios.post(apiUrl + "/admin/redirection/createRedirection", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 

export function getRedirectionById(data) {
  return axios.post(apiUrl + "/admin/redirection/getRedirectionById", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
export function removeRedirection(data) {
  return axios.post(apiUrl + "/admin/redirection/removeRedirection", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}



export function updateRedirection(data) {
  return axios.post(apiUrl + "/admin/redirection/updateRedirection", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

export function updateRedirectionStatus(id,status) {
  return axios.post(apiUrl + "/admin/redirection/updateRedirectionStatus", {
    _id: id,
    status: status,
    token: localStorage.getItem(tokenKey),
  });
}


 