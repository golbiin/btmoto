import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function getAllCoupons() {
  return axios.post(apiUrl + "/admin/coupons/getAllCoupons", {
    token: localStorage.getItem(tokenKey),
  });
}

export function createCoupon(data) {
  
  return axios.post(apiUrl + "/admin/coupons/createCoupon", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

export function getAllUsersIdsandNames() {
    return axios.post(apiUrl + "/admin/coupons/getAllUsersIdsandNames", {
      token: localStorage.getItem(tokenKey),
    });
 }

export function getCouponById(data) {
  return axios.post(apiUrl + "/admin/coupons/getCouponById", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
export function removeCoupon(data) {
  return axios.post(apiUrl + "/admin/coupons/removeCoupon", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}



export function updateCoupon(data) {
  return axios.post(apiUrl + "/admin/coupons/updateCoupon", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 
