import {apiUrl} from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function uploadFiles(upload_data) {

    let token = localStorage.getItem(tokenKey);
    const upload = new FormData();
    for(var x = 0; x < upload_data.length; x++) {
      upload.append('file', upload_data[x])
    }

    console.log('upload_data upload' , upload_data ,upload);


    return axios.post(apiUrl + "/admin/mediaFiles/uploadFiles/" + token, upload, {});

    
    
    /* const res = axios.post(apiUrl + "/admin/mediaFiles/uploadFiles/" + token, upload, {timeout: 500000000}).then(response =>{
      console.log("response", response);
      return response;
    }).catch(err=>{
      console.log("err",err);
    });

    console.log("res", res);
    return res; */
  
  }
  

  export function saveFiles(data) {
  
    return axios.post(apiUrl + "/admin/mediaFiles/saveFiles", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
   }

   export function getAllfiles(filter) {
    return axios.post(apiUrl + "/admin/mediaFiles/getAllfiles", {
      filter,
      token: localStorage.getItem(tokenKey),
    });
   }
   export function getDateFilterOptions() {
    return axios.post(apiUrl + "/admin/mediaFiles/getDateFilterOptions", {
      token: localStorage.getItem(tokenKey),
    });
   }
   export function getTypeFilterOptions() {
    return axios.post(apiUrl + "/admin/mediaFiles/getTypeFilterOptions", {
      token: localStorage.getItem(tokenKey),
    });
   }
 
  export function deleteEntry(id) {
    return axios.post(apiUrl + "/admin/mediaFiles/deleteEntry", {
      id: id,
      token: localStorage.getItem(tokenKey),
    });
  }
  export function bulkDelete(id) {
    return axios.post(apiUrl + "/admin/mediaFiles/bulkDelete", {
      id: id,
      token: localStorage.getItem(tokenKey),
    });
  }

  export function getNameById(id) {
    return axios.post(apiUrl + "/admin/mediaFiles/getNameById", {
      id: id,
      token: localStorage.getItem(tokenKey),
    });
   }
 
   export function updateTitle(data) 
   {
    return axios.post(apiUrl + "/admin/mediaFiles/updateTitle", {
      data: data,
      token: localStorage.getItem(tokenKey),
    });
  }