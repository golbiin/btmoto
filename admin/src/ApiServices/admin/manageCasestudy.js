import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function getAllCasestudy() {
  return axios.post(apiUrl + "/admin/casestudy/getAllcasestudy", {
    token: localStorage.getItem(tokenKey)
  });
}

export function updateCasestudy(data) {
  return axios.post(apiUrl + "/admin/casestudy/updatecasestudy", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function createCasestudy(data) {
  return axios.post(apiUrl + "/admin/casestudy/createcasestudy", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteCasestudy(case_id) {
  return axios.post(apiUrl + "/admin/casestudy/deletecasestudy", {
    case_id: case_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function trashCasestudy(case_id) {
  return axios.post(apiUrl + "/admin/casestudy/trashcasestudy", {
    case_id: case_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function getSingleCasestudy(case_id) {
  return axios.post(apiUrl + "/admin/casestudy/getSinglecasestudy", {
    case_id: case_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function actionHandlerCasestudy(case_id, action, status) {
  return axios.post(apiUrl + "/admin/casestudy/actionHandlerCasestudy", {
    case_id: case_id,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllCatsIdsandNames() {
  return axios.post(apiUrl + "/admin/casestudy/getAllCatsIdsandNames", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getIndustryMenu() {
  return axios.post(apiUrl + "/admin/casestudy/getIndustryMenu", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getProductMenu() {
  return axios.post(apiUrl + "/admin/casestudy/getProductMenu", {
    token: localStorage.getItem(tokenKey)
  });
}
