import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

/***************************************** Distributors ************************************************* */
/* Create Distributors */
export function createDistributors(data) {
  return axios.post(apiUrl + "/admin/integrator-resources/createDistributors", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
/* Get all Distributors */
export function getAllDistributors(filter) {
  return axios.post(apiUrl + "/admin/integrator-resources/getAllDistributors", {
    filter: filter,
    token: localStorage.getItem(tokenKey)
  });
}

/* Delete Distributors */
export function deleteDistributors(news_id) {
  return axios.post(apiUrl + "/admin/integrator-resources/deleteDistributors", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}
/* Trash archives */
export function trashDistributors(id) {
  return axios.post(apiUrl + "/admin/integrator-resources/trashDistributors", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

/* Get Single Distributors*/
export function getSingleDistributors(news_id) {
  return axios.post(apiUrl + "/admin/integrator-resources/getSingleDistributors", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}
/* Update Distributors */
export function updateDistributors(data) {
  return axios.post(apiUrl + "/admin/integrator-resources/updateDistributors", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
/*Action handler archives*/
export function actionHandlerDistributors(archiveids, action, status) {
  return axios.post(apiUrl + "/admin/integrator-resources/actionHandlerDistributors", {
    archiveids: archiveids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

/*date filter archive */

export function getDistributorDateFilterOptions() {
  return axios.post(apiUrl + "/admin/integrator-resources/getDistributorDateFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}
/*category filter */

export function getDistributorTypeFilterOptions() {
  return axios.post(apiUrl + "/admin/integrator-resources/getDistributorTypeFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}


/* get All category Options */

export function getAllCategoryOptions() {
  console.log('getAllCategoryOptions 2');
  return axios.post(apiUrl + "/admin/integrator_category/getAllCategoryOptions", {
    token: localStorage.getItem(tokenKey)
  });
}