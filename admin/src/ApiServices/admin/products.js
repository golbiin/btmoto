import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function uploadFeaturedImage(upload_data) {
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  upload.append("file", upload_data);
  return axios.post(
    apiUrl + "/admin/product-upload/uploadFeaturedImage/" + token,
    upload,
    {}
  );
}

export function uploadProductImages(upload_data) {
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  for (var x = 0; x < upload_data.length; x++) {
    upload.append("file", upload_data[x]);
  }

  return axios.post(
    apiUrl + "/admin/product-upload/uploadData/" + token,
    upload,
    {}
  );
}

export function createProduct(data) {
  return axios.post(apiUrl + "/admin/product/createProduct", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllProducts() {
  return axios.post(apiUrl + "/admin/product/getAllProducts", {
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteProduct(product_id) {
  return axios.post(apiUrl + "/admin/product/deleteProduct", {
    product_id: product_id,
    token: localStorage.getItem(tokenKey)
  });
}
export function trashProduct(product_id) {
  return axios.post(apiUrl + "/admin/product/trashProduct", {
    product_id: product_id,
    token: localStorage.getItem(tokenKey)
  });
}
export function duplicateProduct(product_id) {
  return axios.post(apiUrl + "/admin/product/duplicateProduct", {
    product_id: product_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllProductsIdsandNames() {
  return axios.post(apiUrl + "/admin/product/getAllProductsIdsandNames", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getListCatNames(cat_ids) {
  return axios.post(apiUrl + "/admin/product/getListCatNames", {
    token: localStorage.getItem(tokenKey),
    cat_ids: cat_ids
  });
}

export function getSingleProduct(p_id) {
  return axios.post(apiUrl + "/admin/product/getSingleProduct", {
    p_id: p_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateProduct(data) {
  return axios.post(apiUrl + "/admin/product/updateProduct", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function getCurrentproductUrl(cat_id, location = "product") {
  return axios.post(apiUrl + "/admin/product/getCurrentproductUrl", {
    cat_id: cat_id,
    location: location,
    token: localStorage.getItem(tokenKey)
  });
}

export function actionHandlerProduct(productids, action, status) {
  return axios.post(apiUrl + "/admin/product/actionHandlerProduct", {
    productids: productids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllProductsCat(category) {
  return axios.post(apiUrl + "/admin/product/getAllProductsCat", {
    token: localStorage.getItem(tokenKey),
    category: category
  });
}

export function getAllCategory(data) {
  return axios.post(apiUrl + "/admin/product/getAllCategory", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateCategoryShort(data) {
  return axios.post(apiUrl + "/admin/product/updateCategoryShort", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
