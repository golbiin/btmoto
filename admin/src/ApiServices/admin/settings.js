import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

/*     Create Category  */
export function createCategory(data) {
  return axios.post(apiUrl + "/admin/settings/createCategory", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateCategory(data) {
  return axios.post(apiUrl + "/admin/settings/updateCategory", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

/*  Get all Parent Catogery */
export function getAllParentCategory(data) {
  return axios.post(apiUrl + "/admin/settings/getAllParentCategory", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllCategory(data) {
  return axios.post(apiUrl + "/admin/settings/getAllCategory", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function checkSlugExist(data) {
  return axios.post(apiUrl + "/admin/settings/checkSlugExist", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

/*  Get All Categories  */
export function getAllCategories() {
  return axios.post(apiUrl + "/admin/settings/getAllCategories", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllProductCategories() {
  return axios.post(apiUrl + "/admin/settings/getAllProductCategories", {
    token: localStorage.getItem(tokenKey)
  });
}
export function getselctedproductCatogery() {
  return axios.post(apiUrl + "/admin/settings/getselctedproductCatogery", {
    token: localStorage.getItem(tokenKey)
  });
}

/* Remove Category */
export function removeCategory(data) {
  return axios.post(apiUrl + "/admin/settings/removeCategory", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getCategoryBySlug(data) {
  return axios.post(apiUrl + "/admin/settings/getCategoryBySlug", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getItemBySlug(data) {
  return axios.post(apiUrl + "/admin/settings/getItemBySlug", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

/********************************************************** */

export function getAllNews(filter) {
  return axios.post(apiUrl + "/admin/settings/getAllNews", {
    filter,
    token: localStorage.getItem(tokenKey)
  });
}

export function getDateFilterOptions() {
  return axios.post(apiUrl + "/admin/settings/getDateFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}

export function updateNews(data) {
  return axios.post(apiUrl + "/admin/settings/updateNews", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function createNews(data) {
  return axios.post(apiUrl + "/admin/settings/createNews", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteNews(news_id) {
  return axios.post(apiUrl + "/admin/settings/deleteNews", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function trasheNews(news_id) {
  return axios.post(apiUrl + "/admin/settings/trashNews", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function getSingleNews(news_id) {
  return axios.post(apiUrl + "/admin/settings/getSingleNews", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function actionHandlerNews(newsids, action, status) {
  return axios.post(apiUrl + "/admin/settings/actionHandlerNews", {
    newsids: newsids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

/********************************************************** */

export function createArticles(data) {
  return axios.post(apiUrl + "/admin/settings/createArticles", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function getAllArticles(filter) {
  return axios.post(apiUrl + "/admin/settings/getAllArticles", {
    filter,
    token: localStorage.getItem(tokenKey)
  });
}
export function getArticleDateFilterOptions() {
  return axios.post(apiUrl + "/admin/settings/getArticleDateFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteArticles(news_id) {
  return axios.post(apiUrl + "/admin/settings/deleteArticles", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function trashArticles(news_id) {
  return axios.post(apiUrl + "/admin/settings/trashArticles", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}
export function actionHandlerArticles(newsids, action, status) {
  return axios.post(apiUrl + "/admin/settings/actionHandlerArticles", {
    newsids: newsids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}
export function getSingleArticles(news_id) {
  return axios.post(apiUrl + "/admin/settings/getSingleArticles", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateArticles(data) {
  return axios.post(apiUrl + "/admin/settings/updateArticles", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

/********************************************************** */

export function getAllCareers() {
  return axios.post(apiUrl + "/admin/settings/getAllCareers", {
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteCareer(id) {
  return axios.post(apiUrl + "/admin/settings/deleteCareer", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function trashCareer(id) {
  return axios.post(apiUrl + "/admin/settings/trashCareer", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function actionHandlercareer(ids, action, status) {
  return axios.post(apiUrl + "/admin/settings/actionHandlercareer", {
    ids: ids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

export function createCareer(data) {
  return axios.post(apiUrl + "/admin/settings/createCareer", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getSingleCareer(id) {
  return axios.post(apiUrl + "/admin/settings/getSingleCareer", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateCareer(data) {
  return axios.post(apiUrl + "/admin/settings/updateCareer", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function uploadimageEditor(data) {
  return axios.post(apiUrl + "/admin/upload/imageupload", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

/*   Get All Pages Ids and Names */
export function getAllPagesIdsandNames(data) {
  return axios.post(apiUrl + "/admin/settings/getAllPagesIdsandNames", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

/****************** Footer *****************/

export function updateFooter(data) {
  return axios.post(apiUrl + "/admin/settings/updateFooter", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllFooterDetails(data) {
  return axios.post(apiUrl + "/admin/settings/getAllFooterDetails", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getFooterMenus() {
  return axios.post(apiUrl + "/admin/settings/getFooterMenus", {
    token: localStorage.getItem(tokenKey)
  });
}

/****************** Menu *****************/

export function createMenu(data) {
  return axios.post(apiUrl + "/admin/settings/createMenu", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllMenus() {
  return axios.post(apiUrl + "/admin/settings/getAllMenus", {
    token: localStorage.getItem(tokenKey)
  });
}

export function updateMenu(data) {
  return axios.post(apiUrl + "/admin/settings/updateMenu", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteMenu(data) {
  return axios.post(apiUrl + "/admin/settings/deleteMenu", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

/****************** Menu ****************/

/*stripe */

export function updateStripe(data) {
  return axios.post(apiUrl + "/admin/settings/updateStripe", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function getStripe() {
  return axios.post(apiUrl + "/admin/settings/getStripe", {
    token: localStorage.getItem(tokenKey)
  });
}

export function createorder() {
  return axios.post(apiUrl + "/admin/orders/createOrder", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getReport() {
  return axios.post(apiUrl + "/admin/settings/getReport", {
    token: localStorage.getItem(tokenKey)
  });
}


/*************Facebook api*************** */
export function getFacebook() {
  return axios.post(apiUrl + "/admin/settings/getFacebook", {
    token: localStorage.getItem(tokenKey)
  });
}
export function updateFacebook(data) {
  return axios.post(apiUrl + "/admin/settings/updateFacebook", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
/*************Linkedin api*************** */
export function updateLinkedinCred(data) {
  return axios.post(apiUrl + "/admin/settings/updateLinkedinCred", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
} 
export function getLinkedInData() {
  return axios.post(apiUrl + "/admin/settings/getLinkedindata", {
    token: localStorage.getItem(tokenKey)
  });
}
export function getLinkedIn(data) {
  return axios.post(apiUrl + "/admin/settings/getLinkedin", {
    data: data
  });
}
export function updateLinkedIn(data) {
  return axios.post(apiUrl + "/admin/settings/updateLinkedin", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
/***************************************** Archives ************************************************* */
/* Create Archives */
export function createArchives(data) {
  return axios.post(apiUrl + "/admin/settings/createArchives", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
/* Get all Archives */
export function getAllArchives(filter) {
  return axios.post(apiUrl + "/admin/settings/getAllArchives", {
    filter: filter,
    token: localStorage.getItem(tokenKey)
  });
}

/* Delete Archives */
export function deleteArchives(news_id) {
  return axios.post(apiUrl + "/admin/settings/deleteArchives", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}
/* Trash archives */
export function trashArchives(id) {
  return axios.post(apiUrl + "/admin/settings/trashArchives", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}

/* Get Single Archives*/
export function getSingleArchives(news_id) {
  return axios.post(apiUrl + "/admin/settings/getSingleArchives", {
    news_id: news_id,
    token: localStorage.getItem(tokenKey)
  });
}
/* Update Archives */
export function updateArchives(data) {
  return axios.post(apiUrl + "/admin/settings/updateArchives", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
/*Action handler archives*/
export function actionHandlerArchives(archiveids, action, status) {
  return axios.post(apiUrl + "/admin/settings/actionHandlerArchives", {
    archiveids: archiveids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

/*date filter archive */

export function getArchiveDateFilterOptions() {
  return axios.post(apiUrl + "/admin/settings/getArchiveDateFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}
/*category filter */

export function getArchiveTypeFilterOptions() {
  return axios.post(apiUrl + "/admin/settings/getArchiveTypeFilterOptions", {
    token: localStorage.getItem(tokenKey)
  });
}

/* Create Languages */
export function createLanguages(data) {
  return axios.post(apiUrl + "/admin/settings/createLanguages", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
/* get all Languages */
export function getAllLanguages() {
  return axios.post(apiUrl + "/admin/settings/getAllLanguages", {
    token: localStorage.getItem(tokenKey)
  });
}
/* get Languages id */
export function getLanguageById(data) {
  return axios.post(apiUrl + "/admin/settings/getLanguageById", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
/* Delete Languages */
export function removeLanguage(data) {
  return axios.post(apiUrl + "/admin/settings/removeLanguage", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
/* Update Languages */
export function updateLanguage(data) {
  return axios.post(apiUrl + "/admin/settings/updateLanguage", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

// create intenatinal distributor
export function createInternationalDistributor(data) {
  return axios.post(apiUrl + "/admin/settings/createInternationalDistributor", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllInternationalDistributor() {
  return axios.post(apiUrl + "/admin/settings/getAllInternationalDistributor", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getSingleInternationalDistributor(id) {
  return axios.post(
    apiUrl + "/admin/settings/getSingleInternationalDistributor",
    {
      id: id,
      token: localStorage.getItem(tokenKey)
    }
  );
}

export function deleteInternationalDistributor(id) {
  return axios.post(apiUrl + "/admin/settings/deleteInternationalDistributor", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}
export function trashInternationalDistributor(id) {
  return axios.post(apiUrl + "/admin/settings/trashInternationalDistributor", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}
export function updateInternationalDistributor(data) {
  return axios.post(apiUrl + "/admin/settings/updateInternationalDistributor", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function actionHandlerId(newsids, action, status) {
  return axios.post(apiUrl + "/admin/settings/actionHandlerId", {
    ids: newsids,
    action: action,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}


/* get all FeedBack */
export function getAllFeedBack() {
  return axios.post(apiUrl + "/admin/settings/getAllFeedBack", {
    token: localStorage.getItem(tokenKey)
  });
}




 
/* Delete Languages */
export function removeFeedback(data) {
  return axios.post(apiUrl + "/admin/settings/removeFeedback", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}


/* createfeedbacksettings */


export function updatefeedbacksettings(data) {
  return axios.post(apiUrl + "/admin/settings/updatefeedbacksettings", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}


/* get feedback email */

export function getFeedbackEmail(data) {
  return axios.post(apiUrl + "/admin/settings/getFeedbackEmail", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}



/* update updateSettngs */
export function updateSettngs(data) {
  return axios.post(apiUrl + "/admin/settings/updateSettngs", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}


/* get updateSettngs */
export function getSliderSettngs() {
  console.log('getSliderSettngs');
  return axios.post(apiUrl + "/admin/settings/getSliderSettngs", {
    token: localStorage.getItem(tokenKey)
  });
}

