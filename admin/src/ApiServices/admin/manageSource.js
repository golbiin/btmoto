import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function getAllSources() {
  return axios.post(apiUrl + "/admin/source/getAllSource", {
    token: localStorage.getItem(tokenKey),
  });
}

export function createSource(data) {
  
  return axios.post(apiUrl + "/admin/source/createSource", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 

export function getSourceById(data) {
  return axios.post(apiUrl + "/admin/source/getSourceById", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
export function removeSource(data) {
  return axios.post(apiUrl + "/admin/source/removeSource", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}



export function updateSource(data) {
  return axios.post(apiUrl + "/admin/source/updateSource", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 
export function getAllCatsIdsandNames() {
  return axios.post(apiUrl + "/admin/settings/getAllParentCategory", {});
}
