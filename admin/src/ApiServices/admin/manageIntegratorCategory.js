import { apiUrl } from "../../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;


export function getAllIntegrators() {
  return axios.post(apiUrl + "/admin/integrator_category/getAllIntegrator", {
    token: localStorage.getItem(tokenKey),
  });
}

export function createIntegrator(data) {
  
  return axios.post(apiUrl + "/admin/integrator_category/createIntegrator", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 

export function getIntegratorById(data) {
  return axios.post(apiUrl + "/admin/integrator_category/getIntegratorById", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
export function removeIntegrator(data) {
  return axios.post(apiUrl + "/admin/integrator_category/removeIntegrator", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}



export function updateIntegrator(data) {
  return axios.post(apiUrl + "/admin/integrator_category/updateIntegrator", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}

 
export function getAllCatsIdsandNames() {
  return axios.post(apiUrl + "/admin/settings/getAllParentCategory", {});
}
