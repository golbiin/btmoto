import { apiUrl } from "../config.json";
import jwtDecode from "jwt-decode";

import { USER_TYPES } from './../config.json';
const axios = require("axios").default;
const tokenKey = "user_token";
const rememberKey = "user_remember_token";

export function authenticate(users) {
  return axios.post(apiUrl + "/login/authenticate", {
    username: users.username,
    password: users.password,
    usertype: users.usertype? users.usertype: USER_TYPES.SUBSCRIBER,
    cartId: users.cartId
  });
}
export function loginWithJwt(jwt, remember) {
  localStorage.setItem(tokenKey, jwt);
  if (remember) {
    localStorage.setItem(rememberKey, jwt);
  }
  return true;
}

export function logout() {
  localStorage.removeItem(rememberKey);
  localStorage.removeItem(tokenKey);
}
export function getUserToken() {
  try {
    const jwt = localStorage.getItem(tokenKey);
    return (jwt);
  } catch (ex) {
    return null;
  }
}
export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem(tokenKey);
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}

export function getUserDetails(email) {
  return axios.post(apiUrl + "/login/getUserDetails", {
    email: email,
    token: localStorage.getItem(tokenKey),
  });
}

export function register(data) {
  return axios.post(apiUrl + "/login/register", {
    data: data,
  });
}

export function verifyToken(token) {
  return axios.post(apiUrl + "/login/verifyToken", {
    token: token,
  });
}

export function updateProfile(data) {
  return axios.post(apiUrl + "/login/updateProfile", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
export function updateRecoveyEmail(data) {
  return axios.post(apiUrl + "/login/updateRecoveryEmail", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}


export function uploadProfile(upload_data) {
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  upload.append("file", upload_data);
  return axios.post(apiUrl + "/admin/upload/uploadData/" + token, upload, {});
}

export function updateUserdata(profile) {
  
  return axios.post(apiUrl + "/login/updateUser", {
    data: profile,
    token: localStorage.getItem(tokenKey),
  });
}





