import { apiUrl } from "../config.json";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function getAllCategorybySlug(slug) {
  return axios.post(apiUrl + "/categories/getAllCategoryBySlug", {
    slug: slug,
  });
}
/* Recommended products ipc,plc,scada,hybrid-hmi,hmi page */
export function getAllCategoryProducts(slug) {
  return axios.post(apiUrl + "/categories/getAllCategoryProducts", {
    slug: slug,
  });
}
export function getAllCategory(data) {
  return axios.post(apiUrl + "/categories/getAllCategory", {
    data: data,
    token: localStorage.getItem(tokenKey),
  });
}
/*PLC product sub categories */
export function getAllCmCategories(slug) {
  return axios.post(apiUrl + "/categories/getAllCmCategories", {
    slug: slug,
  });
}
/*Get banner details for products page */
export function getBannerDetails(slug) {
  return axios.post(apiUrl + "/categories/getproductBannerData", {
    slug: slug,
  });
}
