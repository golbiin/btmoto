import { apiUrl } from "../config.json";
const axios = require("axios").default;

/*  Get all types of News  */
export function getNews() {
  return axios.post(apiUrl + "/news/getAllNews", {});
}

/*  Get the Latest News  */
export function getLatestNews(pageNumber) {
  return axios.post(apiUrl + "/news/getLatestNews", {
    page: pageNumber
  });
}

/*  Get Single News Details  */
export function getNewsBySlug(slug) {
  return axios.post(apiUrl + "/news/getNewsBySlug", {
    slug: slug,
  });
}

/* Site Search Functionality */
export function getSearchResults(slug) {
  return axios.post(apiUrl + "/login/search", {
    slug: slug,
  });
}

/*  Get all types of Archives  */
export function getArchives() {
  return axios.post(apiUrl + "/news/getArchives", {});
}
/*  Get Single Archive Details  */
export function getArchiveBySlug(slug) {
  return axios.post(apiUrl + "/news/getArchiveBySlug", {
    slug: slug,
  });
}

/*  Get all types of News  */
export function getNewsAbout() {
  return axios.post(apiUrl + "/news/getAllNewsAbout", {});
}