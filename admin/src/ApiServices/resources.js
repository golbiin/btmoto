import { apiUrl } from "../config.json";
const axios = require("axios").default;

/* Get all Integrator and Distributor Resources */
export function getResources(pid, cid, s, token) {
  return axios.post(apiUrl + "/resources/getAllresources", {
    pid, cid, s, token
  });
}