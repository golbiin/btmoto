import 'react-app-polyfill/ie9';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
//import "./assets/fonts/fontawesome.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/style/blockui.scss";
import "./assets/style/common.scss";
import "./assets/style/admin.scss";
import "./assets/style/dashboard.scss";
import "./assets/style/custom1.scss";
import "./assets/style/cimon.scss";
import "jquery/dist/jquery.min.js";
import "bootstrap/dist/js/bootstrap.min.js";
import "./assets/style/custom2.scss";
import "../node_modules/alertifyjs/build/css/alertify.css";
import "../node_modules/alertifyjs/build/css/themes/semantic.css";
import "aos/dist/aos.css";
import "aos/dist/aos.js";

 if (process.env.NODE_ENV !== 'production') {
   console.log('Looks like we are in development mode!');
 }

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
