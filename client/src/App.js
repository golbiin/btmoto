import React, { Component, Suspense, lazy } from "react";
import {
  Route,
  Redirect,
  Switch,
  BrowserRouter as Router
} from "react-router-dom";

import "./App.css";

const Home = lazy(() => import("./components/home"));
const Resources = lazy(() => import("./components/resources"));

class App extends Component {
  state = {};

  /*********************sitelock submission**********************/
  lockSubmit = e => {
    e.preventDefault();
    const password = document.getElementById("password").value;
    if (password === "Btmoto2021") {
      localStorage.setItem("sitelock", true);
      window.location = "/";
    }
  };
  render() {
    const siteLock = localStorage.getItem("sitelock") === "true";
    const images = require.context("./assets/images", true);

    if (siteLock === false) {
      return (
        <React.Fragment>
          <form
            style={{
              marginLeft: "30%",
              marginRight: "30%",
              marginTop: 50
            }}
            id="sitelock"
            onSubmit={this.lockSubmit}
          >
            <div className="form-group">
              <img
                className="logoImg"
                style={{
                  padding: "5px 0"
                }}
                alt=""
                src={images(`./logo.png`).default}
              ></img>
            </div>
            <div className="form-group">
              <label> Please Login To Continue </label>
              <input type="password" className="form-control" id="password" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <Router>
            <Suspense
              fallback={
                <div
                  style={{
                    height: "100vh",
                    display: "flex",
                    position: "absolute",
                    background: "#fff",
                    zIndex: "999",
                    width: "100%",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  Loading...
                </div>
              }
            >
              <Switch>
                <Route
                  // path="/home"
                  exact
                  path="/"
                  render={props => (
                    <Home {...props} component={Home} title="Home - BTMOTO" />
                  )}
                />

                {/*    <Route path="/not-found" component={NotFound} /> */}
                <Redirect from="/home" exact to="/" />

                <Route
                  path="/resources"
                  render={props => (
                    <Resources
                      {...props}
                      component={Resources}
                      title="Resources - BTMOTO"
                    />
                  )}
                />

                <Redirect to="/not-found" />
              </Switch>
            </Suspense>
          </Router>
        </React.Fragment>
      );
    }
  }
}

export default App;
