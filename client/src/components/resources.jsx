import Footer from "./common/footer";
import Header from "./common/header";
import BannerCarousel from "./elements/bannerCarousel";
import FaqAccordian from "./elements/accordian";

const images = require.context("./../assets/images", true);
const videos = require.context("./../assets/videos", true);
const Resources = () => {
    return (
        <div id="resources">
          <Header />
     <div className="breadcrumbs">
     <div className="container">
    <a href="#" className="previous"> Back</a>
    <a href="#" className="current-page">Resources</a>
  </div>
     </div>
          <div className="about-section"> 
            <div className="container">
            <div className="set-bg">
              <div className="row">
                <div className="col-12 col-lg-12">
                    <h2>About The Manufacturers</h2>
                     <p>Select your make and model to see available downloads and how to guides. 
                         This will encompass instructions, software, owners manuals and any other
                          relevant information for your motorcycle. Please note, this page is constantly 
                          being updated so check back for most recent instructions and software for your motorcycle.</p>
                </div>
                        <form>
                        <div className="row justify-content-md-center justify-content-sm-center">   
                        <div className="col-lg-4  col-md-5 col-sm-6 col-xs-6 select-left">
                        <label  className="form-label">Select Manufactureres</label>
                        <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
                        <option selected>Select One</option>
                        <option value="1">Aprilia</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                        </select>
                   
                        </div>
                        <div className="col-lg-4  col-md-5 col-sm-6 col-xs-6 select-right">
                        <label className="form-label">Select Model</label>
                        <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
                        <option selected>Select One</option>
                        <option value="1">TUONO X / RSV4 X (2021)</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                        </select>
                        </div>
                        <div className="col-lg-3  col-md-3 col-sm-6 col-xs-6 button-wrapper">
                        <button type="submit" className="btn btn-primary">find</button>
                        </div>
                       </div>
                        </form>
              </div>
              </div> 
            
            </div>
          </div>
             <div className="selected-section">

             <div className="container">

             <div className="row img-content">
                <div className="col-md-6 col-sm-12 col-xs-12 ">
                <div className="imgsec">
              <img
              className="sel-img"
              src={images("./selected-img.png").default}
              />
                </div>

                </div>


                <div className="col-md-6 col-sm-12 col-xs-12 con-width">
                <div className="content-sec">
                    <h2>APRILIA TUONO X / RSV4 X (2021)</h2>
                    <p>
                    Sed magna purus fermentum eu tincidunt eu varius ut felis 
                    curabitur vestibulum aliquam leo. Pellentesque ut neque. 
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices 
                    posuere cubilia curae. In ac dui quis mi consectetuer lacinia.
                     Nam adipiscing.
                    </p>

                </div>


                </div>


                </div>
              

        <div className="doc-section">
          
           <div className="row">
                <div className="col-md-8 col-sm-12 doc-left">
                  <div className="doc-single">

                  <h4>Documents</h4>
                <div className="document-outer">
                <h5>BrenTuning Moto Handheld Instructions</h5>
                <a className="download" href="#">Download</a>
                </div>
                <div className="document-outer">
                <h5>BrenTuning Moto Flasher+ Troubleshooting Guide</h5>
                <a className="download" href="#">Download</a>
                </div>
                <div className="document-outer">
                <h5>BrenTuning Moto Handheld Instructions</h5>
                <a className="download" href="#">Download</a>
                </div>
              

                  </div>
        
          

                </div>


                <div className="col-md-4 col-sm-12 video-right">
                <div className="content-sec">
                  <h4>Videos</h4>
                    <div className="videos">

                  <video className="sel-video" width="189" height="122" controls>
                  <source src={videos("./videoplayback.mp4").default} type="video/mp4"/>
                  </video>
                    
    
                      <div className="video-title">
                        How To Install 
                        Handheld Tuner
                      </div>
                    </div>
                    <div className="videos">
                    <video className="sel-video" width="189" height="122" controls>
                  <source src={videos("./videoplayback.mp4").default} type="video/mp4"/>
                  </video>

                      <div className="video-title">
                        How To Install 
                        Handheld Tuner
                      </div>
                    </div>

                </div>


                </div>

      </div>
      </div>   
      <div className="link-section">

      <div className="row">
          <div className="col-md-8 col-sm-12 doc-left">

      <div className="link-single">

          <h4>Links</h4>
          <div className="document-outer">
          <h5>BrenTuning Moto Handheld Software Download</h5>
          <a className="go" href="#">Go</a>
          </div>
          <div className="document-outer">
          <h5>BrenTuning Moto Handheld Software Download (Backup Link)</h5>
          <a className="go" href="#">Go</a>
          </div>
          <div className="document-outer">
          <h5>TeamViewer Remote Access (For Troubleshooting)</h5>
          <a className="go" href="#">Go</a>
          </div>


</div>

       </div>
       </div>
      </div>
      
             
    

            </div>

             </div>
               

               <div className="hr">
                  <div className="container">

                    
                  </div>

               </div>
          
           <div className="accordian-section">
           <div className="container">
              <div className="row">
                <div className="col-12 col-lg-12">
                    <h3>FAQ - Product Question</h3>

                    <FaqAccordian />
                    </div>
                    </div>
   
                    </div>
          

           </div>
          <Footer />
        </div>  
    );
}

export default Resources;