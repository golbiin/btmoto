import Footer from "./common/footer";
import Header from "./common/header";
import BannerCarousel from "./elements/bannerCarousel";
import SpecialSlider from "./elements/specialSlider";
import HomeSharing from "./elements/homeSharing";

const images = require.context("./../assets/images", true);
const Home = () => {
  return (
    <div id="home">
      <Header />
      <BannerCarousel />
      <div className="accessories-section">
        <div className="container">
          <div className="row">
            <div className="col-12 col-lg-4 accessories-box tuners">
              Handheld tuners
            </div>
            <div className="col-12 col-lg-4 accessories-box accessories">
              parts
            </div>
            <div className="col-12 col-lg-4 accessories-box apparel">
              accessories & apparel
            </div>
          </div>
        </div>
      </div>
      <SpecialSlider />
      <HomeSharing />
      <Footer />
    </div>
  );
};

export default Home;
