import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
const images = require.context("./../../assets/images", true);
const BannerCarousel = () => {
  return (
    <div className="banner-carousel">
      <div className="banner-search-area">
        <div className="banner-search-area-inner">
          <div className="select-text">Select Your Motorcycle</div>
          <div className="select-make">
            <select>
              <option value="">Make</option>
            </select>
          </div>
          <div className="select-model">
            <select>
              <option value="">Model</option>
            </select>
          </div>
          <div className="select-year">
            <select>
              <option value="">Year</option>
            </select>
          </div>
          <div className="select-go">
            <button>GO</button>
          </div>
        </div>
      </div>

      <Carousel showThumbs={false} showStatus={false}>
        <div>
          <img src={images("./banner1.jpg").default} />
          <div className="banner-content">
            <h2>We provide</h2>
            <h1>
              SERVICE & <span>TUNING</span>
            </h1>
            <p>
              Custom dyno calibration and ECU flashing services for your
              motorcycle ...
            </p>
            <a className="btmoto-btn">Read More</a>
          </div>
        </div>
        <div>
          <img src={images("./banner1.jpg").default} />
          <div className="banner-content">
            <h2>We provide</h2>
            <h1>
              SERVICE & <span>TUNING</span>
            </h1>
            <p>
              Custom dyno calibration and ECU flashing services for your
              motorcycle ...
            </p>
            <a className="btmoto-btn">Read More</a>
          </div>
        </div>
        <div>
          <img src={images("./banner1.jpg").default} />
          <div className="banner-content">
            <h2>We provide</h2>
            <h1>
              SERVICE & <span>TUNING</span>
            </h1>
            <p>
              Custom dyno calibration and ECU flashing services for your
              motorcycle ...
            </p>
            <a className="btmoto-btn">Read More</a>
          </div>
        </div>
        <div>
          <img src={images("./banner1.jpg").default} />
          <div className="banner-content">
            <h2>We provide</h2>
            <h1>
              SERVICE & <span>TUNING</span>
            </h1>
            <p>
              Custom dyno calibration and ECU flashing services for your
              motorcycle ...
            </p>
            <a className="btmoto-btn">Read More</a>
          </div>
        </div>
      </Carousel>
    </div>
  );
};
export default BannerCarousel;
