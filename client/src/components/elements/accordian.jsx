const images = require.context("./../../assets/images", true);
const Accordian = () =>{
    return (
        <div className="accordion accordion-flush" id="accordionFlushExample">
        <div className="accordion-item">
          <h2 className="accordion-header" id="flush-headingOne">
            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
            In hac habitasse platea dictumst Etiam sit amet orci eget?
            </button>
          </h2>
          <div id="flush-collapseOne" className="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
            <div className="accordion-body">
            I was surprised when I first saw the gum. I didn’t expect the unusual shape.I think that each piece could be larger. I have a small mouth and I chew 2 or 3 at a time. The gum’s texture is very soft. I found it sort of strange.
             It sort of felt like some of it dissolved in your mouth after chewing for a while.</div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="flush-headingTwo">
            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
            Fusce risus nisl viverra et tempor et pretium in sapien Ut non?
            </button>
          </h2>
          <div id="flush-collapseTwo" className="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
            <div className="accordion-body">I was surprised when I first saw the gum. I didn’t expect the unusual shape.I think that each piece could be larger. I have a small mouth and I chew 2 or 3 at a time. The gum’s texture is very soft. I found it sort of strange. It sort of felt like some of it dissolved in your mouth after chewing for a while.</div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="flush-headingThree">
            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
            Cras id dui. Donec posuere vulputate arcu Phasellus blandit?
            </button>
          </h2>
          <div id="flush-collapseThree" className="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
            <div className="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="flush-headingFour">
            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
            How long will it take for my order to arrive?
            </button>
          </h2>
          <div id="flush-collapseFour" className="accordion-collapse collapse" aria-labelledby="flush-collapseFour" data-bs-parent="#accordionFlushExample">
            <div className="accordion-body">I was surprised when I first saw the gum. I didn’t expect the unusual shape.I think that each piece could be larger. I have a small mouth and I chew 2 or 3 at a time. The gum’s texture is very soft. I found it sort of strange. 
            It sort of felt like some of it dissolved in your mouth after chewing for a while.</div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="flush-headingFive">
            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
            Nunc nulla Suspendisse nisl elit rhoncus eget elementum?
            </button>
          </h2>
          <div id="flush-collapseFive" className="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
            <div className="accordion-body">I was surprised when I first saw the gum. I didn’t expect the unusual shape.I think that each piece could be larger. I have a small mouth and I chew 2 or 3 at a time. The gum’s texture is very soft. I found it sort of strange. 
            It sort of felt like some of it dissolved in your mouth after chewing for a while.</div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="flush-headingsix">
            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapsesix" aria-expanded="false" aria-controls="flush-collapsesix">
            Can I create a custom Assortment?
            </button>
          </h2>
          <div id="flush-collapsesix" className="accordion-collapse collapse" aria-labelledby="flush-headingsix" data-bs-parent="#accordionFlushExample">
            <div className="accordion-body">I was surprised when I first saw the gum. I didn’t expect the unusual shape.I think that each piece could be larger. I have a small mouth and I chew 2 or 3 at a time. The gum’s texture is very soft. I found it sort of strange. 
            It sort of felt like some of it dissolved in your mouth after chewing for a while.</div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="flush-headingSeven">
            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
            Ut varius tincidunt libero In auctor lobortis lacus Vestibu?
            </button>
          </h2>
          <div id="flush-collapseSeven" className="accordion-collapse collapse" aria-labelledby="flush-headingSeven" data-bs-parent="#accordionFlushExample">
            <div className="accordion-body">I was surprised when I first saw the gum. I didn’t expect the unusual shape.I think that each piece could be larger. I have a small mouth and I chew 2 or 3 at a time. The gum’s texture is very soft. I found it sort of strange. 
            It sort of felt like some of it dissolved in your mouth after chewing for a while.</div>
          </div>
        </div>
      </div>
    );
};
export default Accordian;