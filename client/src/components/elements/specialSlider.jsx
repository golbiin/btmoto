import React, { useState } from "react";
const images = require.context("./../../assets/images", true);
const SpecialSlider = () => {
  const [title, setTitle] = useState("Solutions");
  const [isClass1, setClass1] = useState(true);
  const [isClass2, setClass2] = useState(false);
  const [isClass3, setClass3] = useState(false);

  const toggle = title => {
    setTitle(title);
    if (title === "Solutions") {
      setClass1(true);
      setClass2(false);
      setClass3(false);
    }
    if (title === "Racing") {
      setClass1(false);
      setClass2(true);
      setClass3(false);
    }
    if (title === "Maintenance") {
      setClass1(false);
      setClass2(false);
      setClass3(true);
    }
  };

  return (
    <div className="special-slider">
      <div className="invert-text left">
        <img
          className="invert-text-img"
          src={images("./wwd-left.png").default}
        />
      </div>
      <div className="invert-text right">
        <img
          className="invert-text-img"
          src={images("./wwd-right.png").default}
        />
      </div>
      <div className="container">
        <div className="titles">
          <a
            className={isClass1 ? "active" : ""}
            onClick={() => toggle("Solutions")}
          >
            Solutions
          </a>
          <a
            className={isClass2 ? "active" : ""}
            onClick={() => toggle("Racing")}
          >
            Racing
          </a>
          <a
            className={isClass3 ? "active" : ""}
            onClick={() => toggle("Maintenance")}
          >
            Maintenance
          </a>
        </div>
        <div className="slider-container">
          <div className="slider-content">
            <h1>{title}</h1>
            <a className="btmoto-btn">Let’s Go</a>
          </div>
        </div>
      </div>
    </div>
  );
};
export default SpecialSlider;
