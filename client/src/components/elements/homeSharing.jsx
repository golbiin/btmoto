const images = require.context("./../../assets/images", true);
const HomeSharing = () => {
  return (
    <div className="home-sharing-section">
      <div className="sharing-inner">
        <div className="if-feed-wrap">
          <div className="ig-item ig-title">
            <div className="share-title">
              # Sharing
              <br /> 14 ways to <br />
              love more.
            </div>
            <div className="share-sub-title">FOLLOW US ON INSTAGRAM</div>
            <div className="share-nav">
              <span className="left">
                <img src={images("./icons/share-left.png").default} />
              </span>
              <span className="right">
                <img src={images("./icons/share-right.png").default} />
              </span>
            </div>
          </div>
          <div className="ig-item ig-1">
            <div className="share-img-item">
              <img src={images("./share-img-1.png").default} />
              <div className="ig-share-count">534</div>
              <div className="ig-feed-title">S1000RR Handheld Giveway?</div>
              <div className="ig-feed-exrept">
                While our focus is on development with new bikes, ...
              </div>
            </div>
          </div>
          <div className="ig-item ig-2">
            <div className="share-img-item">
              <img src={images("./share-img-2.png").default} />
              <div className="ig-share-count">678</div>
              <div className="ig-feed-title">2022 BMW models are starting </div>
              <div className="ig-feed-exrept">
                While our focus is on development with new bikes, ...
              </div>
            </div>
          </div>
          <div className="ig-item ig-3">
            <div className="share-img-item">
              <img src={images("./share-img-3.png").default} />
              <div className="ig-share-count">345</div>
              <div className="ig-feed-title">Flashing more and more 2020+ </div>
              <div className="ig-feed-exrept">
                While our focus is on development with new bikes, ...
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default HomeSharing;
