import React, { Component } from "react";
import { Link } from "react-router-dom";
class DeviceHeader extends Component {
  openNav = () => {
    document.getElementById("deviceSidebar").style.width = "320px";
  };

  closeNav = () => {
    document.getElementById("deviceSidebar").style.width = "0";
  };
  render() {
    const images = require.context("./../../assets/images", true);
    return (
      <div className="device-header">
        <div className="device-log-wrap">
          <div className="mob-logo">
            <img src={images("./btm-logo.png").default} />
          </div>
          <div className="mob-breadcrumb">
            <div className="mob-head-cart">
              <span className="mob-cart-icon">
                <img src={images("./icons/cart-icon.png").default} />
              </span>
              <span className="mob-cart-count">2</span>
            </div>

            <div className="menu-btn" onClick={this.openNav}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="30"
                height="30"
                fill="currentColor"
                class="bi bi-justify"
                viewBox="0 0 16 16"
              >
                <path
                  fill-rule="evenodd"
                  d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"
                />
              </svg>
            </div>
          </div>
        </div>

        <div id="deviceSidebar" className="menu-sidebar">
          <div className="menu-close" onClick={this.closeNav}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              class="bi bi-x-lg"
              viewBox="0 0 16 16"
            >
              <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z" />
            </svg>
          </div>

          <div className="menu-inner-logo">
            <img src={images("./btm-logo.png").default} />
          </div>
          <div className="mob-header-search">
            <input
              type="text"
              placeholder="Search Shop"
              className="search-input"
            />
            <button className="search-btn">
              {" "}
              <img src={images("./icons/search-icon.png").default} />
            </button>
          </div>
          <div className="menu-return-text">
            <a className="returns">
              <i className="left-arrow"></i>
              RETURNS UP TO 30 DAYS
              <i className="right-arrow"></i>
            </a>
          </div>
          <div className="menu-list">
            <Link className="bt-nav-link" onClick={this.closeNav} to="/">
              Shop
            </Link>
            <Link
              className="bt-nav-link"
              onClick={this.closeNav}
              to="/resoources"
            >
              Resources
            </Link>
            <Link className="bt-nav-link" onClick={this.closeNav} to="/">
              News
            </Link>
            <Link className="bt-nav-link" onClick={this.closeNav} to="/">
              Contact
            </Link>
          </div>
          <div className="menu-extra">
            <Link className="bt-nav-link user" onClick={this.closeNav} to="/">
              LOGIN
            </Link>
            <Link className="bt-nav-link map" onClick={this.closeNav} to="/">
              US
            </Link>
            <Link className="bt-nav-link share" onClick={this.closeNav} to="/">
              SHARE
            </Link>
          </div>
          <div className="dealer">
            <a class="locator">Dealer Locator</a>
          </div>
        </div>
      </div>
    );
  }
}
export default DeviceHeader;
