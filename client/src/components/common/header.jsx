import DeviceHeader from "./deviceHeader";
const images = require.context("./../../assets/images", true);
const Header = () => {
  return (
    <>
      <div className="header large-sreen-header">
        <div className="header-inner">
          <div className="row">
            <div className="col-12 col-sm-4 links">
              <a className="locator">Dealer Locator</a>
            </div>
            <div className="col-12 col-sm-4 links middle">
              <a className="returns">
                <i className="left-arrow"></i>
                RETURNS UP TO 30 DAYS
                <i className="right-arrow"></i>
              </a>
            </div>
            <div className="col-12 col-sm-4 links last">
              <a className="right-buttons user">LOGIN</a>
              <a className="right-buttons map">US</a>
              <a className="right-buttons share">SHARE</a>
            </div>
          </div>
          <div className="row">
            <div className="col logo">
              <img src={images("./btm-logo.png").default} />
            </div>
          </div>
          <div className="row menu-bar">
            <div className="col-12 col-sm-3 logo-box">
              <div className="logo-items">
                <img src={images("./btm-vector.png").default} />
                <img src={images("./icons/dropdown.png").default} />
              </div>
            </div>
            <div className="col-12 col-sm-6 menu-box">
              <nav className="navbar navbar-expand-lg">
                <button
                  className="navbar-toggler"
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarNav"
                  aria-controls="navbarNav"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                  <ul className="navbar-nav">
                    <li className="nav-item active">
                      <a className="nav-link" href="#">
                        SHOP
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#">
                        RESOURCES
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#">
                        NEWS
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#">
                        CONTACT
                      </a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
            <div className="col-12 col-sm-3 search-cart-box">
              <div className="head-search">
                <input
                  type="text"
                  placeholder="Search Shop"
                  className="search-input"
                />
                <button className="search-btn">
                  {" "}
                  <img src={images("./icons/search-icon.png").default} />
                </button>
              </div>
              <div className="head-cart">
                <span className="cart-icon">
                  <img src={images("./icons/cart-icon.png").default} />
                </span>
                <span className="cart-count">2</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <DeviceHeader />
    </>
  );
};
export default Header;
