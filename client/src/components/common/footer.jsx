const images = require.context("./../../assets/images", true);
const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <div className="row">
          <div className="col-12 col-sm-3 icons">
            <img src={images("./icons/foot-icon-1.png").default} />
            <h3>FREE RETURNS</h3>
            <p>Within 30 days of receipt</p>
            <div className="separator"></div>
          </div>
          <div className="col-12 col-sm-3 icons">
            <img src={images("./icons/foot-icon-2.png").default} />
            <h3>2 YEAR WARRANTY</h3>
            <p>On all products</p>
            <div className="separator"></div>
          </div>
          <div className="col-12 col-sm-3 icons">
            <img src={images("./icons/foot-icon-3.png").default} />
            <h3>CRASH POLICY</h3>
            <p>Support if you fall</p>
            <div className="separator"></div>
          </div>
          <div className="col-12 col-sm-3 icons">
            <img src={images("./icons/foot-icon-4.png").default} />
            <h3>SECURE PAYMENT</h3>
            <p>Payment processed in secure environment</p>
            <div className="separator"></div>
          </div>
        </div>
        <div className="row second-section">
          <div className="col-12 col-sm-3 links">
            <ul className="main-links">
              <li>
                <a>Home</a>
              </li>
              <li>
                <a>ABOUT US</a>
              </li>
              <li>
                <a>News</a>
              </li>
            </ul>
          </div>
          <div className="col-12 col-sm-3 links">
            <ul className="main-links">
              <li>
                <a>Resources </a>
              </li>
              <li>
                <a>PARTNER</a>
              </li>
            </ul>
          </div>
          <div className="col-12 col-sm-3 links">
            <h3>SHOP</h3>
            <div className="links-row">
              <ul className="sub-links">
                <li>
                  <a>Aprilia</a>
                </li>
                <li>
                  <a>Ducati</a>
                </li>
                <li>
                  <a>KTM</a>
                </li>
              </ul>
              <ul className="sub-links">
                <li>
                  <a>BMW</a>
                </li>
                <li>
                  <a>Honda</a>
                </li>
                <li>
                  <a>Yamaha</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-12 col-sm-3 links">
            <h3>Newsletter</h3>
            <p>Please subscribe to our Newsletter</p>
            <div className="newsletter">
              <input type="text" name="name" placeholder="email address" />
              <button>Sign up</button>
            </div>
          </div>
        </div>

        <div className="row socket">
          <div className="col-9 cpr">
            <div className="copy-right">
              © BT MOTO | BT MOTO® IS A REGISTERED TRADEMARK
            </div>
          </div>
          <div className="col-3 soc">
            <div className="follow">
              <h3>Follow us</h3>
              <div className="social">
                <a href="#" className="social-links">
                  <img src={images("./icons/fb-w.png").default} />
                </a>
                <a href="#" className="social-links">
                  <img src={images("./icons/twt.png").default} />
                </a>
                <a href="#" className="social-links">
                  <img src={images("./icons/insta.png").default} />
                </a>
                <a href="#" className="social-links">
                  <img src={images("./icons/yt.png").default} />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Footer;
